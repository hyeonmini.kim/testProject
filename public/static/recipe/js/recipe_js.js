$(document).ready(function(){

    /*sticy tab*/
    let tab_ul = document.getElementsByClassName("tab_ul")[0];
    let tab_li = tab_ul.getElementsByTagName("li");
    let active_bar = document.getElementsByClassName("active_bar")[0];

    for(let i=0; i < tab_li.length; i++){
        tab_li[i].addEventListener("click", function(){
            tab_ul.getElementsByClassName("active")[0].classList.remove("active");
            tab_li[i].classList.add("active");
        
            active_bar.style.left = `calc(calc(100% / 3) * ${i})`;
        });

        tab_li[i].addEventListener("keydown", function(){
            tab_ul.getElementsByClassName("active")[0].classList.remove("active");
            tab_li[i].classList.add("active");
        
            active_bar.style.left = `calc(calc(100% / 3) * ${i})`;
        });

    }

    /*영양소 특성 팝업 tab*/
    let calori_tab = document.getElementsByClassName("calori_tab")[0];
    let calori_tab_li = calori_tab.getElementsByTagName("li");
    let activeBar = document.getElementsByClassName("activeBar")[0];
    let contentWrap = document.getElementsByClassName("content_wrap")[0];
    let tabContent = document.getElementsByClassName("tab_content");

    for(let i=0; i < calori_tab_li.length; i++){
        calori_tab_li[i].addEventListener("click", function(){
            calori_tab.getElementsByClassName("active")[0].classList.remove("active");
            calori_tab_li[i].classList.add("active");
            contentWrap.getElementsByClassName("active")[0].classList.remove("active");
            tabContent[i].classList.add("active");
        
            activeBar.style.left = `calc(calc(100% / 4) * ${i})`;
        });

        calori_tab_li[i].addEventListener("keydown", function(){
            calori_tab.getElementsByClassName("active")[0].classList.remove("active");
            calori_tab_li[i].classList.add("active");
            contentWrap.getElementsByClassName("active")[0].classList.remove("active");
            tabContent[i].classList.add("active");
        
            activeBar.style.left = `calc(calc(100% / 4) * ${i})`;
        });
    }


    /*영양소 특성 클릭시 팝업 */
    $("#c_charter").click(function(){
        $(".back_dim").addClass('active');
        $(".calori_char").addClass('active');
        $(".calori_char").children('.header_fixed').addClass('active');;
    });
    $(".calori_char").find('button').click(function(){
        $(".back_dim").removeClass('active');
        $(".calori_char").removeClass('active');
        $(".calori_char").children('.header_fixed').removeClass('active');
    });
    
    /*영양정보 출처 클릭시 팝업 */
    $("#c_resource").click(function(){
        $(".back_dim").addClass('active');
        $(".calori_resource").addClass('active');
    });
    $(".calori_resource").find('button').click(function(){
        $(".back_dim").removeClass('active');
        $(".calori_resource").removeClass('active');
    });


    /*메인비주얼 클릭시 팝업 */
    $(".main_visual").find('.slide_area').click(function(){
        $(".back_dim").addClass('active');
        $(".main_visual_popup").addClass('active');
    });
    $(".main_visual_popup").children('.close_btn').click(function(){
        $(".back_dim").removeClass('active');
        $(".main_visual_popup").removeClass('active');
    });

    /*알레르기 유발재료 클릭시 팝업 */
    $(".con1 > a").click(function(){
        $(".back_dim").addClass('active');
        $(".alergy_popup").addClass('active');
        $(".alergy_popup").children('.header_fixed').addClass('active');
    });
    $(".alergy_popup").find('button').click(function(){
        $(".back_dim").removeClass('active');
        $(".alergy_popup").removeClass('active');
        $(".alergy_popup").children('.header_fixed').removeClass('active');
    });
   

    
    // $(".sticky_tab_wrap").find(".hidden_header").hide();
    // $("body").on('mousewheel',function(e){ 
    //     var wheel = e.originalEvent.wheelDelta; 

    //     if(0 < wheel){  //스크롤 올릴때 
    //         $(".hidden_header").show();
          
    //     } else { //스크롤  내릴때 
    //         $(".hidden_header").hide();
    //     } 
    
    // });


    $(".top_scroll_btn button").click(function(){
        
        $(".body_right").animate({scrollTop : 0}, 300);
    });


    $(".slide_area").each(function(){
        $(".slide_area").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: ($(this).children('div').length < 2) ? false : true,
            arrows:false,
            
        });
    });

    $(".popup_slide_area").each(function(){
        $(".popup_slide_area").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: ($(this).children('div').length < 2) ? false : true,
            arrows:false,
            
        });
    });
    
    $(".slide-calori").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows:false,
    });



    //tooltip
    $(".tooltip").hide();
    $("#tooltip_opener").click(function(){
        $(".tooltip").fadeToggle();
        
        if( !$(".tooltip").is(":visible") ) {
            $(".tooltip").fadeIn();
        }
    });

    
    //dialoge
    $(".dialoge").hide();
    $(".profile_area, .reveiw_icon, .bottom_fixed_btn > button").click(function(){
        $(".dialoge").show();
    });
    $(".dialoge").find('button:nth-child(1)').click(function(){
        $(".dialoge").hide();
    });
    $(".dialoge").find('button:nth-child(2)').click(function(){
        // $(".dialoge").show();
    });


    //건강노트
    var bodyTxt = $(".note_body_txt p");
    var hidden_btn = $(".n_hidden_btn");
    
    bodyTxt.each(function(){
        $(this).outerHeight();
        hidden_btn.hide();

        if($(this).outerHeight() > 70) {
            $(this).addClass('hidden');
            hidden_btn.show();

            hidden_btn.click(function(){
                $(this).toggleClass('active');
                bodyTxt.toggleClass('hidden');

            });
        } else {
            $(this).removeClass('hidden');
            hidden_btn.hide();
        }
    });

    //해쉬태그
    var tag_open = $(".tag_open");
    var tag_close = $(".tag_close");
    var tag_wrap_p = $('.tag_wrap > p'); 

    tag_close.hide();
    tag_wrap_p.not('.default').hide();

    tag_open.click(function(){
        tag_wrap_p.not('.default').show();
        $(this).hide();
        tag_close.show();
    });
    tag_close.click(function(){
        tag_wrap_p.not('.default').hide();
        $(this).hide();
        tag_open.show();
    });




});


//tooltip
$(document).mousedown(function( e ){
    if( $(".tooltip").is(":visible") ) {
    $(".tooltip").each(function(){
        var l_position = $(this).offset();
        l_position.right = parseInt(l_position.left) + ($(this).width());
        l_position.bottom = parseInt(l_position.top) + parseInt($(this).height());

        if( ( l_position.left <= e.pageX && e.pageX <= l_position.right )
        && ( l_position.top <= e.pageY && e.pageY <= l_position.bottom ) ) {
        } else {
        $(this).fadeOut();
        }
    });
    }
});

