import {RecoilEnv} from "recoil";

export const ENV = process.env.ENV

export const HOST_API_KEY = process.env.HOST_API_KEY || '';

export const IS_MOCK = process.env.IS_MOCK

export const WEB_VERSION = process.env.VERSION

// LAYOUT
export const BEFU = {
  B_MARGIN: 20,
}

export const HEADER = {
  H_MOBILE: 58,
  H_MAIN_DESKTOP: 88,
  H_DASHBOARD_DESKTOP: 92,
  H_DASHBOARD_DESKTOP_OFFSET: 92 - 32,
};

export const NAV = {
  W_BASE: 260,
  W_DASHBOARD: 280,
  W_DASHBOARD_MINI: 88,
  //
  H_DASHBOARD_ITEM: 48,
  H_DASHBOARD_ITEM_SUB: 36,
  //
  H_DASHBOARD_ITEM_HORIZONTAL: 32,
};

// Recoil Duplicate_Atom_Key Warning 제거
RecoilEnv.RECOIL_DUPLICATE_ATOM_KEY_CHECKING_ENABLED = false