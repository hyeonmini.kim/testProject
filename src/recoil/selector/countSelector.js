import {countState} from "../atom/count";
import {selector} from "recoil";

const countSelector = selector({
  key: 'countSelector',
  get: ({get}) => {
    return get(countState) * 2
  },
  set: ({set}, newValue) => {
    set(countState, newValue)
  }
});

export {countSelector}



