import {selector} from "recoil";
import {stackNavigation, stackPreFetching} from "../../atom/common/stackNavigation";

export const stackNavigationSelector = selector({
  key: 'stackNavigationSelector',
  get: ({get}) => {
    return get(stackNavigation)
  },
  set: ({set}, newValue) => {
    set(stackNavigation, newValue)
  },
});

export const stackPreFetchingSelector = selector({
  key: 'stackPreFetchingSelector',
  get: ({get}) => {
    return get(stackPreFetching)
  },
  set: ({set}, newValue) => {
    set(stackPreFetching, newValue)
  },
});