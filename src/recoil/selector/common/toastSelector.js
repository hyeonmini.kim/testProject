import {selector} from "recoil";
import {toastMessageAtom} from "../../atom/common/toast";

export const toastMessageSelector = selector({
  key: 'toastMessageSelector',
  get: ({get}) => {
    return get(toastMessageAtom)
  },
  set: ({set}, newValue) => {
    set(toastMessageAtom, newValue)
  },
});