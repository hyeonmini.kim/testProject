import {selector} from "recoil";
import {iFrameOpen, iFrameUrl} from "../../atom/common/iFrame";

export const iFrameUrlSelector = selector({
  key: 'iFrameUrlSelector',
  get: ({get}) => {
    return get(iFrameUrl)
  },
  set: ({set}, newValue) => {
    set(iFrameUrl, newValue)
  },
});

export const iFrameOpenSelector = selector({
  key: 'iFrameOpenSelector',
  get: ({get}) => {
    return get(iFrameOpen)
  },
  set: ({set}, newValue) => {
    set(iFrameOpen, newValue)
  },
});