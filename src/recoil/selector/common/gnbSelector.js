import {gnbActiveTab} from "../../atom/common/gnb";
import {selector} from "recoil";

export const gnbActiveTabSelector = selector({
  key: 'gnbActiveTabSelector',
  get: ({get}) => {
    return get(gnbActiveTab)
  },
  set: ({set}, newValue) => {
    set(gnbActiveTab, newValue)
  },
});