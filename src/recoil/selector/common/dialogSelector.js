import {selector} from "recoil";
import {dialogAtom, dialogShowDialog, showFullScreenMessageDialog, showMembershipDialog} from "../../atom/common/dialog";

export const dialogSelector = selector({
  key: 'dialogSelector',
  get: ({get}) => {
    return get(dialogAtom)
  },
  set: ({set}, newValue) => {
    set(dialogAtom, newValue)
  },
});

export const dialogShowDialogSelector = selector({
  key: 'dialogShowDialogSelector',
  get: ({get}) => {
    return get(dialogShowDialog)
  },
  set: ({set}, newValue) => {
    set(dialogShowDialog, newValue)
  },
});

export const showFullScreenMessageDialogSelector = selector({
  key: 'showFullScreenMessageDialogSelector',
  get: ({get}) => {
    return get(showFullScreenMessageDialog)
  },
  set: ({set}, newValue) => {
    set(showFullScreenMessageDialog, newValue)
  },
});

export const showMembershipDialogSelector = selector({
  key: 'showMembershipDialogSelector',
  get: ({get}) => {
    return get(showMembershipDialog)
  },
  set: ({set}, newValue) => {
    set(showMembershipDialog, newValue)
  },
})
