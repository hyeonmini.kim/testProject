import {selector} from "recoil";
import {env, isMock, versionCurrent, versionNew} from "../../atom/common/version";

export const versionCurrentSelector = selector({
  key: 'versionCurrentSelector',
  get: ({get}) => {
    return get(versionCurrent)
  },
  set: ({set}, newValue) => {
    set(versionCurrent, newValue)
  },
});

export const versionNewSelector = selector({
  key: 'versionNewSelector',
  get: ({get}) => {
    return get(versionNew)
  },
  set: ({set}, newValue) => {
    set(versionNew, newValue)
  },
});

export const isMockSelector = selector({
  key: 'isMockSelector',
  get: ({get}) => {
    return get(isMock)
  },
  set: ({set}, newValue) => {
    set(isMock, newValue)
  },
});

export const envSelector = selector({
  key: 'envSelector',
  get: ({get}) => {
    return get(env)
  },
  set: ({set}, newValue) => {
    set(env, newValue)
  },
});