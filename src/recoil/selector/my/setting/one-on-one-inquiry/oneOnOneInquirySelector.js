import {DefaultValue, selector} from "recoil";
import {ONE_ON_ONE_INQUIRY} from "../../../../../constant/my/Setting";
import {
  settingOneOnOneInquiryAttachmentFile,
  settingOneOnOneInquiryBody,
  settingOneOnOneInquiryCategory,
  settingOneOnOneInquiryIsTrue,
  settingOneOnOneInquiryKey,
  settingOneOnOneInquiryTitle
} from "../../../../atom/my/setting/one-on-one-inquiry/oneOnOneInquiry";

export const settingOneOnOneInquirySelector = selector({
  key: 'settingOneOnOneInquirySelector',
  get: ({get}) => {
    const oneOnOneInquiry = {...ONE_ON_ONE_INQUIRY}
    oneOnOneInquiry.key = get(settingOneOnOneInquiryKey)
    oneOnOneInquiry.title = get(settingOneOnOneInquiryTitle)
    oneOnOneInquiry.body = get(settingOneOnOneInquiryBody)
    oneOnOneInquiry.attachmentFile = get(settingOneOnOneInquiryAttachmentFile)
    oneOnOneInquiry.category = get(settingOneOnOneInquiryCategory)
    return oneOnOneInquiry
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(settingOneOnOneInquiryKey)
      reset(settingOneOnOneInquiryTitle)
      reset(settingOneOnOneInquiryBody)
      reset(settingOneOnOneInquiryAttachmentFile)
      reset(settingOneOnOneInquiryCategory)
    } else {
      set(settingOneOnOneInquiryKey, newValue?.key)
      set(settingOneOnOneInquiryTitle, newValue?.title)
      set(settingOneOnOneInquiryBody, newValue?.body)
      set(settingOneOnOneInquiryAttachmentFile, newValue?.attachmentFile)
      set(settingOneOnOneInquiryCategory, newValue?.category)
    }
  },
});

export const settingOneOnOneInquiryKeySelector = selector({
  key: 'settingOneOnOneInquiryKeySelector',
  get: ({get}) => {
    return get(settingOneOnOneInquiryKey)
  },
  set: ({set}, newValue) => {
    set(settingOneOnOneInquiryKey, newValue)
  },
});

export const settingOneOnOneInquiryTitleSelector = selector({
  key: 'settingOneOnOneInquiryTitleSelector',
  get: ({get}) => {
    return get(settingOneOnOneInquiryTitle)
  },
  set: ({set}, newValue) => {
    set(settingOneOnOneInquiryTitle, newValue)
  },
});

export const settingOneOnOneInquiryBodySelector = selector({
  key: 'settingOneOnOneInquiryBodySelector',
  get: ({get}) => {
    return get(settingOneOnOneInquiryBody)
  },
  set: ({set}, newValue) => {
    set(settingOneOnOneInquiryBody, newValue)
  },
});
export const settingOneOnOneInquiryAttachmentFileSelector = selector({
  key: 'settingOneOnOneInquiryAttachmentFileSelector',
  get: ({get}) => {
    return get(settingOneOnOneInquiryAttachmentFile)
  },
  set: ({set}, newValue) => {
    set(settingOneOnOneInquiryAttachmentFile, newValue)
  },
});
export const settingOneOnOneInquiryCategorySelector = selector({
  key: 'settingOneOnOneInquiryCategorySelector',
  get: ({get}) => {
    return get(settingOneOnOneInquiryCategory)
  },
  set: ({set}, newValue) => {
    set(settingOneOnOneInquiryCategory, newValue)
  },
});
export const settingOneOnOneInquiryIsTrueSelector = selector({
  key: 'settingOneOnOneInquiryIsTrueSelector',
  get: ({get}) => {
    return get(settingOneOnOneInquiryIsTrue)
  },
  set: ({set}, newValue) => {
    set(settingOneOnOneInquiryIsTrue, newValue)
  },
});
