import {DefaultValue, selector} from "recoil";
import {MyBaby, MyData} from "../../../constant/my/MyData";
import {
  isChangePhoneNumber,
  myAccountKey,
  myBabies,
  myBabyAllergyIngredients,
  myBabyBirthdate,
  myBabyConcerns,
  myBabyGender,
  myBabyHeight,
  myBabyIsProfilePictureUrlDeleted,
  myBabyKey,
  myBabyNickname,
  myBabyParentKey,
  myBabyProfilePictureThumbnailOrder,
  myBabyProfilePictureUrl,
  myBabyWeight,
  myBirthDay,
  myBriefBio, myCall,
  myEmail,
  myEmailReceiveAgreementModifiedDatetime,
  myGrade, myIsCall,
  myIsEmailReceiveAgreed,
  myIsMarketingInfoPushNotifSet,
  myIsPostCensorshipResultPushNotifSet,
  myIsProfilePictureUrlDeleted,
  myIsTermsCollectingPersonalDataMarketingAgreed,
  myIsTextMessageReciveAgreed,
  myKey,
  myMarketingInfoPushNotifSettingModifiedDatetime,
  myNickname,
  myPhoneNumber,
  myProfilePictureUrl,
  myProfileSelectedBabyKey,
  mySocialMediaUrl,
  myTextMessageReciveAgreementModifiedDatetime,
  myType,
  tempMyAccountKey,
  tempMyBabies,
  tempMyBabyAllergyIngredients,
  tempMyBabyBirthdate,
  tempMyBabyConcerns,
  tempMyBabyGender,
  tempMyBabyHeight,
  tempMyBabyKey,
  tempMyBabyNickname,
  tempMyBabyParentKey,
  tempMyBabyProfilePictureThumbnailOrder,
  tempMyBabyProfilePictureUrl,
  tempMyBabyWeight,
  tempMyBirthDay,
  tempMyBriefBio,
  tempMyEmail,
  tempMyEmailReceiveAgreementModifiedDatetime,
  tempMyGrade,
  tempMyIsEmailReceiveAgreed,
  tempMyIsMarketingInfoPushNotifSet,
  tempMyIsPostCensorshipResultPushNotifSet,
  tempMyIsTermsCollectingPersonalDataMarketingAgreed,
  tempMyIsTextMessageReciveAgreed,
  tempMyKey,
  tempMyMarketingInfoPushNotifSettingModifiedDatetime,
  tempMyNickname,
  tempMyPhoneNumber,
  tempMyProfilePictureUrl,
  tempMyProfileSelectedBabyKey,
  tempMySocialMediaUrl,
  tempMyTextMessageReciveAgreementModifiedDatetime,
  tempMyType
} from "../../atom/my/myData";

export const myDataSelector = selector({
  key: 'myDataSelector',
  get: ({get}) => {
    const myData = {...MyData}
    myData.profilePictureUrl = get(myProfilePictureUrl)
    myData.marketingInfoPushNotifSettingModifiedDatetime = get(myMarketingInfoPushNotifSettingModifiedDatetime)
    myData.birthDay = get(myBirthDay)
    myData.profileSelectedBabyKey = get(myProfileSelectedBabyKey)
    myData.briefBio = get(myBriefBio)
    myData.babies = get(myBabies)
    myData.type = get(myType)
    myData.emailReceiveAgreementModifiedDatetime = get(myEmailReceiveAgreementModifiedDatetime)
    myData.isTermsCollectingPersonalDataMarketingAgreed = get(myIsTermsCollectingPersonalDataMarketingAgreed)
    myData.accountKey = get(myAccountKey)
    myData.isEmailReceiveAgreed = get(myIsEmailReceiveAgreed)
    myData.phoneNumber = get(myPhoneNumber)
    myData.socialMediaUrl = get(mySocialMediaUrl)
    myData.isPostCensorshipResultPushNotifSet = get(myIsPostCensorshipResultPushNotifSet)
    myData.grade = get(myGrade)
    myData.nickname = get(myNickname)
    myData.textMessageReciveAgreementModifiedDatetime = get(myTextMessageReciveAgreementModifiedDatetime)
    myData.isMarketingInfoPushNotifSet = get(myIsMarketingInfoPushNotifSet)
    myData.isTextMessageReciveAgreed = get(myIsTextMessageReciveAgreed)
    myData.key = get(myKey)
    myData.email = get(myEmail)
    return myData
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(myProfilePictureUrl)
      reset(myMarketingInfoPushNotifSettingModifiedDatetime)
      reset(myBirthDay)
      reset(myProfileSelectedBabyKey)
      reset(myBriefBio)
      reset(myBabies)
      reset(myType)
      reset(myEmailReceiveAgreementModifiedDatetime)
      reset(myIsTermsCollectingPersonalDataMarketingAgreed)
      reset(myAccountKey)
      reset(myIsEmailReceiveAgreed)
      reset(myPhoneNumber)
      reset(mySocialMediaUrl)
      reset(myIsPostCensorshipResultPushNotifSet)
      reset(myGrade)
      reset(myNickname)
      reset(myTextMessageReciveAgreementModifiedDatetime)
      reset(myIsMarketingInfoPushNotifSet)
      reset(myIsTextMessageReciveAgreed)
      reset(myKey)
      reset(myEmail)
    } else {
      set(myProfilePictureUrl, newValue.profilePictureUrl)
      set(myMarketingInfoPushNotifSettingModifiedDatetime, newValue.marketingInfoPushNotifSettingModifiedDatetime)
      set(myBirthDay, newValue.birthDay)
      set(myProfileSelectedBabyKey, newValue.profileSelectedBabyKey)
      set(myBriefBio, newValue.briefBio)
      set(myBabies, newValue.babies)
      set(myType, newValue.type)
      set(myEmailReceiveAgreementModifiedDatetime, newValue.emailReceiveAgreementModifiedDatetime)
      set(myIsTermsCollectingPersonalDataMarketingAgreed, newValue.isTermsCollectingPersonalDataMarketingAgreed)
      set(myAccountKey, newValue.accountKey)
      set(myIsEmailReceiveAgreed, newValue.isEmailReceiveAgreed)
      set(myPhoneNumber, newValue.phoneNumber)
      set(mySocialMediaUrl, newValue.socialMediaUrl)
      set(myIsPostCensorshipResultPushNotifSet, newValue.isPostCensorshipResultPushNotifSet)
      set(myGrade, newValue.grade)
      set(myNickname, newValue.nickname)
      set(myTextMessageReciveAgreementModifiedDatetime, newValue.textMessageReciveAgreementModifiedDatetime)
      set(myIsMarketingInfoPushNotifSet, newValue.isMarketingInfoPushNotifSet)
      set(myIsTextMessageReciveAgreed, newValue.isTextMessageReciveAgreed)
      set(myKey, newValue.key)
      set(myEmail, newValue.email)
    }
  },
});

export const myBabyDataSelector = selector({
  key: 'myBabyDataSelector',
  get: ({get}) => {
    const myBabyData = {...MyBaby}
    myBabyData.key = get(myBabyKey)
    myBabyData.parentKey = get(myBabyParentKey)
    myBabyData.nickname = get(myBabyNickname)
    myBabyData.birthdate = get(myBabyBirthdate)
    myBabyData.height = get(myBabyHeight)
    myBabyData.weight = get(myBabyWeight)
    myBabyData.gender = get(myBabyGender)
    myBabyData.profilePictureUrl = get(myBabyProfilePictureUrl)
    myBabyData.profilePictureThumbnailOrder = get(myBabyProfilePictureThumbnailOrder)
    myBabyData.allergyIngredients = get(myBabyAllergyIngredients)
    myBabyData.concerns = get(myBabyConcerns)
    return myBabyData
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(myBabyKey)
      reset(myBabyParentKey)
      reset(myBabyNickname)
      reset(myBabyBirthdate)
      reset(myBabyHeight)
      reset(myBabyWeight)
      reset(myBabyGender)
      reset(myBabyProfilePictureUrl)
      reset(myBabyProfilePictureThumbnailOrder)
      reset(myBabyAllergyIngredients)
      reset(myBabyConcerns)
    } else {
      set(myBabyKey, newValue.key)
      set(myBabyParentKey, newValue.parentKey)
      set(myBabyNickname, newValue.nickname)
      set(myBabyBirthdate, newValue.birthdate)
      set(myBabyHeight, newValue.height)
      set(myBabyWeight, newValue.weight)
      set(myBabyGender, newValue.gender)
      set(myBabyProfilePictureUrl, newValue.profilePictureUrl)
      set(myBabyProfilePictureThumbnailOrder, newValue.profilePictureThumbnailOrder)
      set(myBabyAllergyIngredients, newValue.allergyIngredients)
      set(myBabyConcerns, newValue.concerns)
    }
  },
});

export const tempMyDataSelector = selector({
  key: 'tempMyDataSelector',
  get: ({get}) => {
    const myData = {...MyData}
    myData.profilePictureUrl = get(tempMyProfilePictureUrl)
    myData.marketingInfoPushNotifSettingModifiedDatetime = get(tempMyMarketingInfoPushNotifSettingModifiedDatetime)
    myData.birthDay = get(tempMyBirthDay)
    myData.profileSelectedBabyKey = get(tempMyProfileSelectedBabyKey)
    myData.briefBio = get(tempMyBriefBio)
    myData.babies = get(tempMyBabies)
    myData.type = get(tempMyType)
    myData.emailReceiveAgreementModifiedDatetime = get(tempMyEmailReceiveAgreementModifiedDatetime)
    myData.isTermsCollectingPersonalDataMarketingAgreed = get(tempMyIsTermsCollectingPersonalDataMarketingAgreed)
    myData.accountKey = get(tempMyAccountKey)
    myData.isEmailReceiveAgreed = get(tempMyIsEmailReceiveAgreed)
    myData.phoneNumber = get(tempMyPhoneNumber)
    myData.socialMediaUrl = get(tempMySocialMediaUrl)
    myData.isPostCensorshipResultPushNotifSet = get(tempMyIsPostCensorshipResultPushNotifSet)
    myData.grade = get(tempMyGrade)
    myData.nickname = get(tempMyNickname)
    myData.textMessageReciveAgreementModifiedDatetime = get(tempMyTextMessageReciveAgreementModifiedDatetime)
    myData.isMarketingInfoPushNotifSet = get(tempMyIsMarketingInfoPushNotifSet)
    myData.isTextMessageReciveAgreed = get(tempMyIsTextMessageReciveAgreed)
    myData.key = get(tempMyKey)
    myData.email = get(tempMyEmail)
    return myData
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(tempMyProfilePictureUrl)
      reset(tempMyMarketingInfoPushNotifSettingModifiedDatetime)
      reset(tempMyBirthDay)
      reset(tempMyProfileSelectedBabyKey)
      reset(tempMyBriefBio)
      reset(tempMyBabies)
      reset(tempMyType)
      reset(tempMyEmailReceiveAgreementModifiedDatetime)
      reset(tempMyIsTermsCollectingPersonalDataMarketingAgreed)
      reset(tempMyAccountKey)
      reset(tempMyIsEmailReceiveAgreed)
      reset(tempMyPhoneNumber)
      reset(tempMySocialMediaUrl)
      reset(tempMyIsPostCensorshipResultPushNotifSet)
      reset(tempMyGrade)
      reset(tempMyNickname)
      reset(tempMyTextMessageReciveAgreementModifiedDatetime)
      reset(tempMyIsMarketingInfoPushNotifSet)
      reset(tempMyIsTextMessageReciveAgreed)
      reset(tempMyKey)
      reset(tempMyEmail)
    } else {
      set(tempMyProfilePictureUrl, newValue.profilePictureUrl)
      set(tempMyMarketingInfoPushNotifSettingModifiedDatetime, newValue.marketingInfoPushNotifSettingModifiedDatetime)
      set(tempMyBirthDay, newValue.birthDay)
      set(tempMyProfileSelectedBabyKey, newValue.profileSelectedBabyKey)
      set(tempMyBriefBio, newValue.briefBio)
      set(tempMyBabies, newValue.babies)
      set(tempMyType, newValue.type)
      set(tempMyEmailReceiveAgreementModifiedDatetime, newValue.emailReceiveAgreementModifiedDatetime)
      set(tempMyIsTermsCollectingPersonalDataMarketingAgreed, newValue.isTermsCollectingPersonalDataMarketingAgreed)
      set(tempMyAccountKey, newValue.accountKey)
      set(tempMyIsEmailReceiveAgreed, newValue.isEmailReceiveAgreed)
      set(tempMyPhoneNumber, newValue.phoneNumber)
      set(tempMySocialMediaUrl, newValue.socialMediaUrl)
      set(tempMyIsPostCensorshipResultPushNotifSet, newValue.isPostCensorshipResultPushNotifSet)
      set(tempMyGrade, newValue.grade)
      set(tempMyNickname, newValue.nickname)
      set(tempMyTextMessageReciveAgreementModifiedDatetime, newValue.textMessageReciveAgreementModifiedDatetime)
      set(tempMyIsMarketingInfoPushNotifSet, newValue.isMarketingInfoPushNotifSet)
      set(tempMyIsTextMessageReciveAgreed, newValue.isTextMessageReciveAgreed)
      set(tempMyKey, newValue.key)
      set(tempMyEmail, newValue.email)
    }
  },
});

export const tempMyBabyDataSelector = selector({
  key: 'tempMyBabyDataSelector',
  get: ({get}) => {
    const myBabyData = {...MyBaby}
    myBabyData.key = get(tempMyBabyKey)
    myBabyData.parentKey = get(tempMyBabyParentKey)
    myBabyData.nickname = get(tempMyBabyNickname)
    myBabyData.birthdate = get(tempMyBabyBirthdate)
    myBabyData.height = get(tempMyBabyHeight)
    myBabyData.weight = get(tempMyBabyWeight)
    myBabyData.gender = get(tempMyBabyGender)
    myBabyData.profilePictureUrl = get(tempMyBabyProfilePictureUrl)
    myBabyData.profilePictureThumbnailOrder = get(tempMyBabyProfilePictureThumbnailOrder)
    myBabyData.allergyIngredients = get(tempMyBabyAllergyIngredients)
    myBabyData.concerns = get(tempMyBabyConcerns)
    return myBabyData
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(tempMyBabyKey)
      reset(tempMyBabyParentKey)
      reset(tempMyBabyNickname)
      reset(tempMyBabyBirthdate)
      reset(tempMyBabyHeight)
      reset(tempMyBabyWeight)
      reset(tempMyBabyGender)
      reset(tempMyBabyProfilePictureUrl)
      reset(tempMyBabyProfilePictureThumbnailOrder)
      reset(tempMyBabyAllergyIngredients)
      reset(tempMyBabyConcerns)
    } else {
      set(tempMyBabyKey, newValue.key)
      set(tempMyBabyParentKey, newValue.parentKey)
      set(tempMyBabyNickname, newValue.nickname)
      set(tempMyBabyBirthdate, newValue.birthdate)
      set(tempMyBabyHeight, newValue.height)
      set(tempMyBabyWeight, newValue.weight)
      set(tempMyBabyGender, newValue.gender)
      set(tempMyBabyProfilePictureUrl, newValue.profilePictureUrl)
      set(tempMyBabyProfilePictureThumbnailOrder, newValue.profilePictureThumbnailOrder)
      set(tempMyBabyAllergyIngredients, newValue.allergyIngredients)
      set(tempMyBabyConcerns, newValue.concerns)
    }
  },
});

export const myProfilePictureUrlSelector = selector({
  key: 'myProfilePictureUrlSelector',
  get: ({get}) => {
    return get(myProfilePictureUrl)
  },
  set: ({set}, newValue) => {
    set(myProfilePictureUrl, newValue)
  },
});

export const myMarketingInfoPushNotifSettingModifiedDatetimeSelector = selector({
  key: 'myMarketingInfoPushNotifSettingModifiedDatetimeSelector',
  get: ({get}) => {
    return get(myMarketingInfoPushNotifSettingModifiedDatetime)
  },
  set: ({set}, newValue) => {
    set(myMarketingInfoPushNotifSettingModifiedDatetime, newValue)
  },
});

export const myBirthDaySelector = selector({
  key: 'myBirthDaySelector',
  get: ({get}) => {
    return get(myBirthDay)
  },
  set: ({set}, newValue) => {
    set(myBirthDay, newValue)
  },
});

export const myProfileSelectedBabyKeySelector = selector({
  key: 'myProfileSelectedBabyKeySelector',
  get: ({get}) => {
    return get(myProfileSelectedBabyKey)
  },
  set: ({set}, newValue) => {
    set(myProfileSelectedBabyKey, newValue)
  },
});

export const myBriefBioSelector = selector({
  key: 'myBriefBioSelector',
  get: ({get}) => {
    return get(myBriefBio)
  },
  set: ({set}, newValue) => {
    set(myBriefBio, newValue)
  },
});

export const myBabiesSelector = selector({
  key: 'myBabiesSelector',
  get: ({get}) => {
    return get(myBabies)
  },
  set: ({set}, newValue) => {
    set(myBabies, newValue)
  },
});

export const myTypeSelector = selector({
  key: 'myTypeSelector',
  get: ({get}) => {
    return get(myType)
  },
  set: ({set}, newValue) => {
    set(myType, newValue)
  },
});

export const myEmailReceiveAgreementModifiedDatetimeSelector = selector({
  key: 'myEmailReceiveAgreementModifiedDatetimeSelector',
  get: ({get}) => {
    return get(myEmailReceiveAgreementModifiedDatetime)
  },
  set: ({set}, newValue) => {
    set(myEmailReceiveAgreementModifiedDatetime, newValue)
  },
});

export const myIsTermsCollectingPersonalDataMarketingAgreedSelector = selector({
  key: 'myIsTermsCollectingPersonalDataMarketingAgreedSelector',
  get: ({get}) => {
    return get(myIsTermsCollectingPersonalDataMarketingAgreed)
  },
  set: ({set}, newValue) => {
    set(myIsTermsCollectingPersonalDataMarketingAgreed, newValue)
  },
});

export const myAccountKeySelector = selector({
  key: 'myAccountKeySelector',
  get: ({get}) => {
    return get(myAccountKey)
  },
  set: ({set}, newValue) => {
    set(myAccountKey, newValue)
  },
});

export const myIsEmailReceiveAgreedSelector = selector({
  key: 'myIsEmailReceiveAgreedSelector',
  get: ({get}) => {
    return get(myIsEmailReceiveAgreed)
  },
  set: ({set}, newValue) => {
    set(myIsEmailReceiveAgreed, newValue)
  },
});

export const myPhoneNumberSelector = selector({
  key: 'myPhoneNumberSelector',
  get: ({get}) => {
    return get(myPhoneNumber)
  },
  set: ({set}, newValue) => {
    set(myPhoneNumber, newValue)
  },
});

export const mySocialMediaUrlSelector = selector({
  key: 'mySocialMediaUrlSelector',
  get: ({get}) => {
    return get(mySocialMediaUrl)
  },
  set: ({set}, newValue) => {
    set(mySocialMediaUrl, newValue)
  },
});

export const myIsPostCensorshipResultPushNotifSetSelector = selector({
  key: 'myIsPostCensorshipResultPushNotifSetSelector',
  get: ({get}) => {
    return get(myIsPostCensorshipResultPushNotifSet)
  },
  set: ({set}, newValue) => {
    set(myIsPostCensorshipResultPushNotifSet, newValue)
  },
});

export const myGradeSelector = selector({
  key: 'myGradeSelector',
  get: ({get}) => {
    return get(myGrade)
  },
  set: ({set}, newValue) => {
    set(myGrade, newValue)
  },
});

export const myNicknameSelector = selector({
  key: 'myNicknameSelector',
  get: ({get}) => {
    return get(myNickname)
  },
  set: ({set}, newValue) => {
    set(myNickname, newValue)
  },
});

export const myTextMessageReciveAgreementModifiedDatetimeSelector = selector({
  key: 'myTextMessageReciveAgreementModifiedDatetimeSelector',
  get: ({get}) => {
    return get(myTextMessageReciveAgreementModifiedDatetime)
  },
  set: ({set}, newValue) => {
    set(myTextMessageReciveAgreementModifiedDatetime, newValue)
  },
});

export const myIsMarketingInfoPushNotifSetSelector = selector({
  key: 'myIsMarketingInfoPushNotifSetSelector',
  get: ({get}) => {
    return get(myIsMarketingInfoPushNotifSet)
  },
  set: ({set}, newValue) => {
    set(myIsMarketingInfoPushNotifSet, newValue)
  },
});

export const myIsTextMessageReciveAgreedSelector = selector({
  key: 'myIsTextMessageReciveAgreedSelector',
  get: ({get}) => {
    return get(myIsTextMessageReciveAgreed)
  },
  set: ({set}, newValue) => {
    set(myIsTextMessageReciveAgreed, newValue)
  },
});

export const myKeySelector = selector({
  key: 'myKeySelector',
  get: ({get}) => {
    return get(myKey)
  },
  set: ({set}, newValue) => {
    set(myKey, newValue)
  },
});

export const myEmailSelector = selector({
  key: 'myEmailSelector',
  get: ({get}) => {
    return get(myEmail)
  },
  set: ({set}, newValue) => {
    set(myEmail, newValue)
  },
});

export const myIsProfilePictureUrlDeletedSelector = selector({
  key: 'myIsProfilePictureUrlDeletedSelector',
  get: ({get}) => {
    return get(myIsProfilePictureUrlDeleted)
  },
  set: ({set}, newValue) => {
    set(myIsProfilePictureUrlDeleted, newValue)
  },
});

export const myBabyKeySelector = selector({
  key: 'myBabyKeySelector',
  get: ({get}) => {
    return get(myBabyKey)
  },
  set: ({set}, newValue) => {
    set(myBabyKey, newValue)
  },
});

export const myBabyParentKeySelector = selector({
  key: 'myBabyParentKeySelector',
  get: ({get}) => {
    return get(myBabyParentKey)
  },
  set: ({set}, newValue) => {
    set(myBabyParentKey, newValue)
  },
});

export const myBabyNicknameSelector = selector({
  key: 'myBabyNicknameSelector',
  get: ({get}) => {
    return get(myBabyNickname)
  },
  set: ({set}, newValue) => {
    set(myBabyNickname, newValue)
  },
});

export const myBabyBirthdateSelector = selector({
  key: 'myBabyBirthdateSelector',
  get: ({get}) => {
    return get(myBabyBirthdate)
  },
  set: ({set}, newValue) => {
    set(myBabyBirthdate, newValue)
  },
});

export const myBabyHeightSelector = selector({
  key: 'myBabyHeightSelector',
  get: ({get}) => {
    return get(myBabyHeight)
  },
  set: ({set}, newValue) => {
    set(myBabyHeight, newValue)
  },
});

export const myBabyWeightSelector = selector({
  key: 'myBabyWeightSelector',
  get: ({get}) => {
    return get(myBabyWeight)
  },
  set: ({set}, newValue) => {
    set(myBabyWeight, newValue)
  },
});

export const myBabyGenderSelector = selector({
  key: 'myBabyGenderSelector',
  get: ({get}) => {
    return get(myBabyGender)
  },
  set: ({set}, newValue) => {
    set(myBabyGender, newValue)
  },
});

export const myBabyProfilePictureUrlSelector = selector({
  key: 'myBabyProfilePictureUrlSelector',
  get: ({get}) => {
    return get(myBabyProfilePictureUrl)
  },
  set: ({set}, newValue) => {
    set(myBabyProfilePictureUrl, newValue)
  },
});

export const myBabyProfilePictureThumbnailOrderSelector = selector({
  key: 'myBabyProfilePictureThumbnailOrderSelector',
  get: ({get}) => {
    return get(myBabyProfilePictureThumbnailOrder)
  },
  set: ({set}, newValue) => {
    set(myBabyProfilePictureThumbnailOrder, newValue)
  },
});

export const myBabyAllergyIngredientsSelector = selector({
  key: 'myBabyAllergyIngredientsSelector',
  get: ({get}) => {
    return get(myBabyAllergyIngredients)
  },
  set: ({set}, newValue) => {
    set(myBabyAllergyIngredients, newValue)
  },
});

export const myBabyConcernsSelector = selector({
  key: 'myBabyConcernsSelector',
  get: ({get}) => {
    return get(myBabyConcerns)
  },
  set: ({set}, newValue) => {
    set(myBabyConcerns, newValue)
  },
});

export const myBabyIsProfilePictureUrlDeletedSelector = selector({
  key: 'myBabyIsProfilePictureUrlDeletedSelector',
  get: ({get}) => {
    return get(myBabyIsProfilePictureUrlDeleted)
  },
  set: ({set}, newValue) => {
    set(myBabyIsProfilePictureUrlDeleted, newValue)
  },
});

export const tempMyProfilePictureUrlSelector = selector({
  key: 'tempMyProfilePictureUrlSelector',
  get: ({get}) => {
    return get(tempMyProfilePictureUrl)
  },
  set: ({set}, newValue) => {
    set(tempMyProfilePictureUrl, newValue)
  },
});

export const isChangePhoneNumberSelector = selector({
  key: 'isChangePhoneNumberSelector',
  get: ({get}) => {
    return get(isChangePhoneNumber)
  },
  set: ({set}, newValue) => {
    set(isChangePhoneNumber, newValue)
  },
})

export const tempMyMarketingInfoPushNotifSettingModifiedDatetimeSelector = selector({
  key: 'tempMyMarketingInfoPushNotifSettingModifiedDatetimeSelector',
  get: ({get}) => {
    return get(tempMyMarketingInfoPushNotifSettingModifiedDatetime)
  },
  set: ({set}, newValue) => {
    set(tempMyMarketingInfoPushNotifSettingModifiedDatetime, newValue)
  },
});

export const tempMyBirthDaySelector = selector({
  key: 'tempMyBirthDaySelector',
  get: ({get}) => {
    return get(tempMyBirthDay)
  },
  set: ({set}, newValue) => {
    set(tempMyBirthDay, newValue)
  },
});

export const tempMyProfileSelectedBabyKeySelector = selector({
  key: 'tempMyProfileSelectedBabyKeySelector',
  get: ({get}) => {
    return get(tempMyProfileSelectedBabyKey)
  },
  set: ({set}, newValue) => {
    set(tempMyProfileSelectedBabyKey, newValue)
  },
});

export const tempMyBriefBioSelector = selector({
  key: 'tempMyBriefBioSelector',
  get: ({get}) => {
    return get(tempMyBriefBio)
  },
  set: ({set}, newValue) => {
    set(tempMyBriefBio, newValue)
  },
});

export const tempMyBabiesSelector = selector({
  key: 'tempMyBabiesSelector',
  get: ({get}) => {
    return get(tempMyBabies)
  },
  set: ({set}, newValue) => {
    set(tempMyBabies, newValue)
  },
});

export const tempMyTypeSelector = selector({
  key: 'tempMyTypeSelector',
  get: ({get}) => {
    return get(tempMyType)
  },
  set: ({set}, newValue) => {
    set(tempMyType, newValue)
  },
});

export const tempMyEmailReceiveAgreementModifiedDatetimeSelector = selector({
  key: 'tempMyEmailReceiveAgreementModifiedDatetimeSelector',
  get: ({get}) => {
    return get(tempMyEmailReceiveAgreementModifiedDatetime)
  },
  set: ({set}, newValue) => {
    set(tempMyEmailReceiveAgreementModifiedDatetime, newValue)
  },
});

export const tempMyIsTermsCollectingPersonalDataMarketingAgreedSelector = selector({
  key: 'tempMyIsTermsCollectingPersonalDataMarketingAgreedSelector',
  get: ({get}) => {
    return get(tempMyIsTermsCollectingPersonalDataMarketingAgreed)
  },
  set: ({set}, newValue) => {
    set(tempMyIsTermsCollectingPersonalDataMarketingAgreed, newValue)
  },
});

export const tempMyAccountKeySelector = selector({
  key: 'tempMyAccountKeySelector',
  get: ({get}) => {
    return get(tempMyAccountKey)
  },
  set: ({set}, newValue) => {
    set(tempMyAccountKey, newValue)
  },
});

export const tempMyIsEmailReceiveAgreedSelector = selector({
  key: 'tempMyIsEmailReceiveAgreedSelector',
  get: ({get}) => {
    return get(tempMyIsEmailReceiveAgreed)
  },
  set: ({set}, newValue) => {
    set(tempMyIsEmailReceiveAgreed, newValue)
  },
});

export const tempMyPhoneNumberSelector = selector({
  key: 'tempMyPhoneNumberSelector',
  get: ({get}) => {
    return get(tempMyPhoneNumber)
  },
  set: ({set}, newValue) => {
    set(tempMyPhoneNumber, newValue)
  },
});

export const tempMySocialMediaUrlSelector = selector({
  key: 'tempMySocialMediaUrlSelector',
  get: ({get}) => {
    return get(tempMySocialMediaUrl)
  },
  set: ({set}, newValue) => {
    set(tempMySocialMediaUrl, newValue)
  },
});

export const tempMyIsPostCensorshipResultPushNotifSetSelector = selector({
  key: 'tempMyIsPostCensorshipResultPushNotifSetSelector',
  get: ({get}) => {
    return get(tempMyIsPostCensorshipResultPushNotifSet)
  },
  set: ({set}, newValue) => {
    set(tempMyIsPostCensorshipResultPushNotifSet, newValue)
  },
});

export const tempMyGradeSelector = selector({
  key: 'tempMyGradeSelector',
  get: ({get}) => {
    return get(tempMyGrade)
  },
  set: ({set}, newValue) => {
    set(tempMyGrade, newValue)
  },
});

export const tempMyNicknameSelector = selector({
  key: 'tempMyNicknameSelector',
  get: ({get}) => {
    return get(tempMyNickname)
  },
  set: ({set}, newValue) => {
    set(tempMyNickname, newValue)
  },
});

export const tempMyTextMessageReciveAgreementModifiedDatetimeSelector = selector({
  key: 'tempMyTextMessageReciveAgreementModifiedDatetimeSelector',
  get: ({get}) => {
    return get(tempMyTextMessageReciveAgreementModifiedDatetime)
  },
  set: ({set}, newValue) => {
    set(tempMyTextMessageReciveAgreementModifiedDatetime, newValue)
  },
});

export const tempMyIsMarketingInfoPushNotifSetSelector = selector({
  key: 'tempMyIsMarketingInfoPushNotifSetSelector',
  get: ({get}) => {
    return get(tempMyIsMarketingInfoPushNotifSet)
  },
  set: ({set}, newValue) => {
    set(tempMyIsMarketingInfoPushNotifSet, newValue)
  },
});

export const tempMyIsTextMessageReciveAgreedSelector = selector({
  key: 'tempMyIsTextMessageReciveAgreedSelector',
  get: ({get}) => {
    return get(tempMyIsTextMessageReciveAgreed)
  },
  set: ({set}, newValue) => {
    set(tempMyIsTextMessageReciveAgreed, newValue)
  },
});

export const tempMyKeySelector = selector({
  key: 'tempMyKeySelector',
  get: ({get}) => {
    return get(tempMyKey)
  },
  set: ({set}, newValue) => {
    set(tempMyKey, newValue)
  },
});

export const tempMyEmailSelector = selector({
  key: 'tempMyEmailSelector',
  get: ({get}) => {
    return get(tempMyEmail)
  },
  set: ({set}, newValue) => {
    set(tempMyEmail, newValue)
  },
});

export const tempMyBabyKeySelector = selector({
  key: 'tempMyBabyKeySelector',
  get: ({get}) => {
    return get(tempMyBabyKey)
  },
  set: ({set}, newValue) => {
    set(tempMyBabyKey, newValue)
  },
});

export const tempMyBabyParentKeySelector = selector({
  key: 'tempMyBabyParentKeySelector',
  get: ({get}) => {
    return get(tempMyBabyParentKey)
  },
  set: ({set}, newValue) => {
    set(tempMyBabyParentKey, newValue)
  },
});

export const tempMyBabyNicknameSelector = selector({
  key: 'tempMyBabyNicknameSelector',
  get: ({get}) => {
    return get(tempMyBabyNickname)
  },
  set: ({set}, newValue) => {
    set(tempMyBabyNickname, newValue)
  },
});

export const tempMyBabyBirthdateSelector = selector({
  key: 'tempMyBabyBirthdateSelector',
  get: ({get}) => {
    return get(tempMyBabyBirthdate)
  },
  set: ({set}, newValue) => {
    set(tempMyBabyBirthdate, newValue)
  },
});

export const tempMyBabyHeightSelector = selector({
  key: 'tempMyBabyHeightSelector',
  get: ({get}) => {
    return get(tempMyBabyHeight)
  },
  set: ({set}, newValue) => {
    set(tempMyBabyHeight, newValue)
  },
});

export const tempMyBabyWeightSelector = selector({
  key: 'tempMyBabyWeightSelector',
  get: ({get}) => {
    return get(tempMyBabyWeight)
  },
  set: ({set}, newValue) => {
    set(tempMyBabyWeight, newValue)
  },
});

export const tempMyBabyGenderSelector = selector({
  key: 'tempMyBabyGenderSelector',
  get: ({get}) => {
    return get(tempMyBabyGender)
  },
  set: ({set}, newValue) => {
    set(tempMyBabyGender, newValue)
  },
});

export const tempMyBabyProfilePictureUrlSelector = selector({
  key: 'tempMyBabyProfilePictureUrlSelector',
  get: ({get}) => {
    return get(tempMyBabyProfilePictureUrl)
  },
  set: ({set}, newValue) => {
    set(tempMyBabyProfilePictureUrl, newValue)
  },
});

export const tempMyBabyProfilePictureThumbnailOrderSelector = selector({
  key: 'tempMyBabyProfilePictureThumbnailOrderSelector',
  get: ({get}) => {
    return get(tempMyBabyProfilePictureThumbnailOrder)
  },
  set: ({set}, newValue) => {
    set(tempMyBabyProfilePictureThumbnailOrder, newValue)
  },
});

export const tempMyBabyAllergyIngredientsSelector = selector({
  key: 'tempMyBabyAllergyIngredientsSelector',
  get: ({get}) => {
    return get(tempMyBabyAllergyIngredients)
  },
  set: ({set}, newValue) => {
    set(tempMyBabyAllergyIngredients, newValue)
  },
});

export const tempMyBabyConcernsSelector = selector({
  key: 'tempMyBabyConcernsSelector',
  get: ({get}) => {
    return get(tempMyBabyConcerns)
  },
  set: ({set}, newValue) => {
    set(tempMyBabyConcerns, newValue)
  },
});

export const myCallSelector = selector({
  key: 'myCallSelector',
  get: ({get}) => {
    return get(myCall)
  },
  set: ({set}, newValue) => {
    set(myCall, newValue)
  },
});