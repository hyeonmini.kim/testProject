import {selector} from "recoil";
import {
  licenseUrl,
  settingAgreeTermsDetail,
  settingIsAutoLogin,
  settingIsCall, settingIsChangedOneOnOneInquiries,
  settingIsEventInfo,
  settingIsRecipeResult,
  settingIsScreenLock,
  settingShowAgreeTerms,
  settingShowFAQ,
  settingShowFAQDetail,
  settingShowLicense,
  settingShowNotice,
  settingShowNoticeDetail,
  settingShowOneOnOneInquiries,
  settingShowOneOnOneInquiriesDetail,
  settingShowOneOnOneInquiriesPopup,
  settingShowPersonalTerms,
  settingShowPush,
  settingShowRegistOneOnOneInquiry,
  settingShowServiceTerms,
  settingShowSetting,
  settingShowTerms,
  settingShowVersion
} from "../../atom/my/setting";

export const settingShowPushSelector = selector({
  key: 'settingShowPushSelector',
  get: ({get}) => {
    return get(settingShowPush)
  },
  set: ({set}, newValue) => {
    set(settingShowPush, newValue)
  },
});

export const settingIsAutoLoginSelector = selector({
  key: 'settingIsAutoLoginSelector',
  get: ({get}) => {
    return get(settingIsAutoLogin)
  },
  set: ({set}, newValue) => {
    set(settingIsAutoLogin, newValue)
  },
});

export const settingIsScreenLockSelector = selector({
  key: 'settingIsScreenLockSelector',
  get: ({get}) => {
    return get(settingIsScreenLock)
  },
  set: ({set}, newValue) => {
    set(settingIsScreenLock, newValue)
  },
});

export const settingShowNoticeSelector = selector({
  key: 'settingShowNoticeSelector',
  get: ({get}) => {
    return get(settingShowNotice)
  },
  set: ({set}, newValue) => {
    set(settingShowNotice, newValue)
  },
});

export const settingShowTermsSelector = selector({
  key: 'settingShowTermsSelector',
  get: ({get}) => {
    return get(settingShowTerms)
  },
  set: ({set}, newValue) => {
    set(settingShowTerms, newValue)
  },
});

export const settingShowLicenseSelector = selector({
  key: 'settingShowLicenseSelector',
  get: ({get}) => {
    return get(settingShowLicense)
  },
  set: ({set}, newValue) => {
    set(settingShowLicense, newValue)
  },
});

export const settingShowFAQSelector = selector({
  key: 'settingShowFAQSelector',
  get: ({get}) => {
    return get(settingShowFAQ)
  },
  set: ({set}, newValue) => {
    set(settingShowFAQ, newValue)
  },
});

export const settingShowVersionSelector = selector({
  key: 'settingShowVersionSelector',
  get: ({get}) => {
    return get(settingShowVersion)
  },
  set: ({set}, newValue) => {
    set(settingShowVersion, newValue)
  },
});

export const settingShowOneOnOneInquiriesSelector = selector({
  key: 'settingShowOneOnOneInquiriesSelector',
  get: ({get}) => {
    return get(settingShowOneOnOneInquiries)
  },
  set: ({set}, newValue) => {
    set(settingShowOneOnOneInquiries, newValue)
  },
});

export const settingIsEventInfoSelector = selector({
  key: 'settingIsEventInfoSelector',
  get: ({get}) => {
    return get(settingIsEventInfo)
  },
  set: ({set}, newValue) => {
    set(settingIsEventInfo, newValue)
  },
});

export const settingIsRecipeResultSelector = selector({
  key: 'settingIsRecipeResultSelector',
  get: ({get}) => {
    return get(settingIsRecipeResult)
  },
  set: ({set}, newValue) => {
    set(settingIsRecipeResult, newValue)
  },
});

export const settingShowSettingSelector = selector({
  key: 'settingShowSettingSelector',
  get: ({get}) => {
    return get(settingShowSetting)
  },
  set: ({set}, newValue) => {
    set(settingShowSetting, newValue)
  },
});

export const settingShowNoticeDetailSelector = selector({
  key: 'settingShowNoticeDetailSelector',
  get: ({get}) => {
    return get(settingShowNoticeDetail)
  },
  set: ({set}, newValue) => {
    set(settingShowNoticeDetail, newValue)
  },
});

export const settingShowServiceTermsSelector = selector({
  key: 'settingShowServiceTermsSelector',
  get: ({get}) => {
    return get(settingShowServiceTerms)
  },
  set: ({set}, newValue) => {
    set(settingShowServiceTerms, newValue)
  },
});

export const settingShowPersonalTermsSelector = selector({
  key: 'settingShowPersonalTermsSelector',
  get: ({get}) => {
    return get(settingShowPersonalTerms)
  },
  set: ({set}, newValue) => {
    set(settingShowPersonalTerms, newValue)
  },
});

export const settingShowAgreeTermsSelector = selector({
  key: 'settingShowAgreeTermsSelector',
  get: ({get}) => {
    return get(settingShowAgreeTerms)
  },
  set: ({set}, newValue) => {
    set(settingShowAgreeTerms, newValue)
  },
});

export const settingShowFAQDetailSelector = selector({
  key: 'settingShowFAQDetailSelector',
  get: ({get}) => {
    return get(settingShowFAQDetail)
  },
  set: ({set}, newValue) => {
    set(settingShowFAQDetail, newValue)
  },
});

export const settingIsCallSelector = selector({
  key: 'settingIsCallSelector',
  get: ({get}) => {
    return get(settingIsCall)
  },
  set: ({set}, newValue) => {
    set(settingIsCall, newValue)
  },
});

export const settingAgreeTermsDetailSelector = selector({
  key: 'settingAgreeTermsDetailSelector',
  get: ({get}) => {
    return get(settingAgreeTermsDetail)
  },
  set: ({set}, newValue) => {
    set(settingAgreeTermsDetail, newValue)
  },
})

export const settingShowOneOnOneInquiriesDetailSelector = selector({
  key: 'settingShowOneOnOneInquiriesDetailSelector',
  get: ({get}) => {
    return get(settingShowOneOnOneInquiriesDetail)
  },
  set: ({set}, newValue) => {
    set(settingShowOneOnOneInquiriesDetail, newValue)
  },
})

export const settingShowOneOnOneInquiriesPopupSelector = selector({
  key: 'settingShowOneOnOneInquiriesPopupSelector',
  get: ({get}) => {
    return get(settingShowOneOnOneInquiriesPopup)
  },
  set: ({set}, newValue) => {
    set(settingShowOneOnOneInquiriesPopup, newValue)
  },
})

export const settingShowRegistOneOnOneInquirySelector = selector({
  key: 'settingShowRegistOneOnOneInquirySelector',
  get: ({get}) => {
    return get(settingShowRegistOneOnOneInquiry)
  },
  set: ({set}, newValue) => {
    set(settingShowRegistOneOnOneInquiry, newValue)
  },
})

export const settingIsChangedOneOnOneInquiriesSelector = selector({
  key: 'settingIsChangedOneOnOneInquiriesSelector',
  get: ({get}) => {
    return get(settingIsChangedOneOnOneInquiries)
  },
  set: ({set}, newValue) => {
    set(settingIsChangedOneOnOneInquiries, newValue)
  },
})

export const licenseUrlSelector = selector({
  key: 'licenseUrlSelector',
  get: ({get}) => {
    return get(licenseUrl)
  },
  set: ({set}, newValue) => {
    set(licenseUrl, newValue)
  },
})
