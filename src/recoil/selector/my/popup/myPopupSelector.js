import {selector} from "recoil";
import {
  myCallbackAddSns,
  myOpenAddChildren,
  myOpenAddChildrenBtnMargin,
  myOpenAddChildrenBtnPosition,
  myOpenAddChildrenBtnText,
  myOpenAddChildrenPage,
  myOpenChangePhone,
  myOpenChangePw,
  myOpenChildren,
  myOpenMomStar,
  myOpenMyInformationPopup,
  myOpenResultAdd,
  myOpenSns,
  myOpenSnsDetail,
  myOpenWithdrawMembership
} from "../../../atom/my/popup/myPopup";

export const myOpenMyInformationPopupSelector = selector({
  key: 'myOpenMyInformationPopupSelector',
  get: ({get}) => {
    return get(myOpenMyInformationPopup)
  },
  set: ({set}, newValue) => {
    set(myOpenMyInformationPopup, newValue)
  },
});

export const myOpenMomStarSelector = selector({
  key: 'myOpenMomStarSelector',
  get: ({get}) => {
    return get(myOpenMomStar)
  },
  set: ({set}, newValue) => {
    set(myOpenMomStar, newValue)
  },
});

export const myOpenChildrenSelector = selector({
  key: 'myOpenChildrenSelector',
  get: ({get}) => {
    return get(myOpenChildren)
  },
  set: ({set}, newValue) => {
    set(myOpenChildren, newValue)
  },
});

export const myOpenAddChildrenSelector = selector({
  key: 'myOpenAddChildrenSelector',
  get: ({get}) => {
    return get(myOpenAddChildren)
  },
  set: ({set}, newValue) => {
    set(myOpenAddChildren, newValue)
  },
});

export const myOpenAddChildrenPageSelector = selector({
  key: 'myOpenAddChildrenPageSelector',
  get: ({get}) => {
    return get(myOpenAddChildrenPage)
  },
  set: ({set}, newValue) => {
    set(myOpenAddChildrenPage, newValue)
  },
});

export const myOpenAddChildrenBtnPositionSelector = selector({
  key: 'myOpenAddChildrenBtnPositionSelector',
  get: ({get}) => {
    return get(myOpenAddChildrenBtnPosition)
  },
  set: ({set}, newValue) => {
    set(myOpenAddChildrenBtnPosition, newValue)
  },
});

export const myOpenAddChildrenBtnMarginSelector = selector({
  key: 'myOpenAddChildrenBtnMarginSelector',
  get: ({get}) => {
    return get(myOpenAddChildrenBtnMargin)
  },
  set: ({set}, newValue) => {
    set(myOpenAddChildrenBtnMargin, newValue)
  },
});

export const myOpenAddChildrenBtnTextSelector = selector({
  key: 'myOpenAddChildrenBtnTextSelector',
  get: ({get}) => {
    return get(myOpenAddChildrenBtnText)
  },
  set: ({set}, newValue) => {
    set(myOpenAddChildrenBtnText, newValue)
  },
});

export const myOpenResultAddSelector = selector({
  key: 'myOpenResultAddSelector',
  get: ({get}) => {
    return get(myOpenResultAdd)
  },
  set: ({set}, newValue) => {
    set(myOpenResultAdd, newValue)
  },
});

export const myOpenSnsSelector = selector({
  key: 'myOpenSnsSelector',
  get: ({get}) => {
    return get(myOpenSns)
  },
  set: ({set}, newValue) => {
    set(myOpenSns, newValue)
  },
});

export const myOpenChangePwSelector = selector({
  key: 'myOpenChangePwSelector',
  get: ({get}) => {
    return get(myOpenChangePw)
  },
  set: ({set}, newValue) => {
    set(myOpenChangePw, newValue)
  },
});

export const myOpenChangePhoneSelector = selector({
  key: 'myOpenChangePhoneSelector',
  get: ({get}) => {
    return get(myOpenChangePhone)
  },
  set: ({set}, newValue) => {
    set(myOpenChangePhone, newValue)
  },
});

export const myOpenWithdrawMembershipSelector = selector({
  key: 'myOpenWithdrawMembershipSelector',
  get: ({get}) => {
    return get(myOpenWithdrawMembership)
  },
  set: ({set}, newValue) => {
    set(myOpenWithdrawMembership, newValue)
  },
});

export const myOpenSnsDetailSelector = selector({
  key: 'myOpenSnsDetailSelector',
  get: ({get}) => {
    return get(myOpenSnsDetail)
  },
  set: ({set}, newValue) => {
    set(myOpenSnsDetail, newValue)
  },
});

export const myCallbackAddSnsSelector = selector({
  key: 'myCallbackAddSnsSelector',
  get: ({get}) => {
    return get(myCallbackAddSns)
  },
  set: ({set}, newValue) => {
    set(myCallbackAddSns, newValue)
  },
});