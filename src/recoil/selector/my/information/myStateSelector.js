import {selector} from "recoil";
import {
  myActiveTab,
  myIsExpandedAccordion,
  myIsSelectedFilter,
  myMoreSearchScrap,
  myMoreSearchWrite,
  myPageScrap,
  myPageWrite,
  myScrapCount,
  myScrapList,
  mySelectedFilter,
  myWriteCount,
  myWriteList
} from "../../../atom/my/information/myState";

export const myIsExpandedAccordionSelector = selector({
  key: 'myIsExpandedAccordionSelector',
  get: ({get}) => {
    return get(myIsExpandedAccordion)
  },
  set: ({set}, newValue) => {
    set(myIsExpandedAccordion, newValue)
  },
});

export const myActiveTabSelector = selector({
  key: 'myActiveTabSelector',
  get: ({get}) => {
    return get(myActiveTab)
  },
  set: ({set}, newValue) => {
    set(myActiveTab, newValue)
  },
});

export const mySelectedFilterSelector = selector({
  key: 'mySelectedFilterSelector',
  get: ({get}) => {
    return get(mySelectedFilter)
  },
  set: ({set}, newValue) => {
    set(mySelectedFilter, newValue)
  },
});

export const myPageWriteSelector = selector({
  key: 'myPageWriteSelector',
  get: ({get}) => {
    return get(myPageWrite)
  },
  set: ({set}, newValue) => {
    set(myPageWrite, newValue)
  },
});

export const myPageScrapSelector = selector({
  key: 'myPageScrapSelector',
  get: ({get}) => {
    return get(myPageScrap)
  },
  set: ({set}, newValue) => {
    set(myPageScrap, newValue)
  },
});

export const myMoreSearchWriteSelector = selector({
  key: 'myMoreSearchWriteSelector',
  get: ({get}) => {
    return get(myMoreSearchWrite)
  },
  set: ({set}, newValue) => {
    set(myMoreSearchWrite, newValue)
  },
});

export const myMoreSearchScrapSelector = selector({
  key: 'myMoreSearchScrapSelector',
  get: ({get}) => {
    return get(myMoreSearchScrap)
  },
  set: ({set}, newValue) => {
    set(myMoreSearchScrap, newValue)
  },
});

export const myWriteListSelector = selector({
  key: 'myWriteListSelector',
  get: ({get}) => {
    return get(myWriteList)
  },
  set: ({set}, newValue) => {
    set(myWriteList, newValue)
  },
});

export const myScrapListSelector = selector({
  key: 'myScrapListSelector',
  get: ({get}) => {
    return get(myScrapList)
  },
  set: ({set}, newValue) => {
    set(myScrapList, newValue)
  },
});

export const myWriteCountSelector = selector({
  key: 'myWriteCountSelector',
  get: ({get}) => {
    return get(myWriteCount)
  },
  set: ({set}, newValue) => {
    set(myWriteCount, newValue)
  },
});

export const myScrapCountSelector = selector({
  key: 'myScrapCountSelector',
  get: ({get}) => {
    return get(myScrapCount)
  },
  set: ({set}, newValue) => {
    set(myScrapCount, newValue)
  },
});

export const myIsSelectedFilterSelector = selector({
  key: 'myIsSelectedFilterSelector',
  get: ({get}) => {
    return get(myIsSelectedFilter)
  },
  set: ({set}, newValue) => {
    set(myIsSelectedFilter, newValue)
  },
});