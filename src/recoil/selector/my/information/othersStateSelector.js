import {selector} from "recoil";
import {
  othersActiveTab,
  othersMoreSearchScrap,
  othersMoreSearchWrite,
  othersPageScrap,
  othersPageWrite,
  othersScrapCount,
  othersScrapList,
  othersWriteCount,
  othersWriteList
} from "../../../atom/my/information/othersState";

export const othersActiveTabSelector = selector({
  key: 'othersActiveTabSelector',
  get: ({get}) => {
    return get(othersActiveTab)
  },
  set: ({set}, newValue) => {
    set(othersActiveTab, newValue)
  },
});

export const othersPageWriteSelector = selector({
  key: 'othersPageWriteSelector',
  get: ({get}) => {
    return get(othersPageWrite)
  },
  set: ({set}, newValue) => {
    set(othersPageWrite, newValue)
  },
});

export const othersPageScrapSelector = selector({
  key: 'othersPageScrapSelector',
  get: ({get}) => {
    return get(othersPageScrap)
  },
  set: ({set}, newValue) => {
    set(othersPageScrap, newValue)
  },
});

export const othersMoreSearchWriteSelector = selector({
  key: 'othersMoreSearchWriteSelector',
  get: ({get}) => {
    return get(othersMoreSearchWrite)
  },
  set: ({set}, newValue) => {
    set(othersMoreSearchWrite, newValue)
  },
});

export const othersMoreSearchScrapSelector = selector({
  key: 'othersMoreSearchScrapSelector',
  get: ({get}) => {
    return get(othersMoreSearchScrap)
  },
  set: ({set}, newValue) => {
    set(othersMoreSearchScrap, newValue)
  },
});

export const othersWriteListSelector = selector({
  key: 'othersWriteListSelector',
  get: ({get}) => {
    return get(othersWriteList)
  },
  set: ({set}, newValue) => {
    set(othersWriteList, newValue)
  },
});

export const othersScrapListSelector = selector({
  key: 'othersScrapListSelector',
  get: ({get}) => {
    return get(othersScrapList)
  },
  set: ({set}, newValue) => {
    set(othersScrapList, newValue)
  },
});

export const othersWriteCountSelector = selector({
  key: 'othersWriteCountSelector',
  get: ({get}) => {
    return get(othersWriteCount)
  },
  set: ({set}, newValue) => {
    set(othersWriteCount, newValue)
  },
});

export const othersScrapCountSelector = selector({
  key: 'othersScrapCountSelector',
  get: ({get}) => {
    return get(othersScrapCount)
  },
  set: ({set}, newValue) => {
    set(othersScrapCount, newValue)
  },
});
