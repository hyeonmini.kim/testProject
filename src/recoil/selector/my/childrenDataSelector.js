import {selector} from "recoil";
import {
  childrenAllergyIngredients,
  childrenAllergyIngredientsList,
  childrenConcerns,
  childrenConcernsList
} from "../../atom/my/childrenData";

export const childrenAllergyIngredientsListSelector = selector({
  key: 'childrenAllergyIngredientsListSelector',
  get: ({get}) => {
    return get(childrenAllergyIngredientsList)
  },
  set: ({set}, newValue) => {
    set(childrenAllergyIngredientsList, newValue)
  },
})

export const childrenAllergyIngredientsSelector = selector({
  key: 'childrenAllergyIngredientsSelector',
  get: ({get}) => {
    return get(childrenAllergyIngredients)
  },
  set: ({set}, newValue) => {
    set(childrenAllergyIngredients, newValue)
  },
})

export const childrenConcernsListSelector = selector({
  key: 'childrenConcernsListSelector',
  get: ({get}) => {
    return get(childrenConcernsList)
  },
  set: ({set}, newValue) => {
    set(childrenConcernsList, newValue)
  },
})

export const childrenConcernsSelector = selector({
  key: 'childrenConcernsSelector',
  get: ({get}) => {
    return get(childrenConcerns)
  },
  set: ({set}, newValue) => {
    set(childrenConcerns, newValue)
  },
})