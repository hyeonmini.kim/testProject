import {DefaultValue, selector} from "recoil";
import {MyData} from "../../../constant/my/MyData";
import {
  othersAccountKey,
  othersBabies,
  othersBirthDay,
  othersBriefBio,
  othersEmail,
  othersEmailReceiveAgreementModifiedDatetime,
  othersGrade,
  othersIsEmailReceiveAgreed,
  othersIsMarketingInfoPushNotifSet,
  othersIsPostCensorshipResultPushNotifSet,
  othersIsTermsCollectingPersonalDataMarketingAgreed,
  othersIsTextMessageReciveAgreed,
  othersKey,
  othersMarketingInfoPushNotifSettingModifiedDatetime,
  othersNickname,
  othersPhoneNumber,
  othersProfilePictureUrl,
  othersProfileSelectedBabyKey,
  othersSocialMediaUrl,
  othersTextMessageReciveAgreementModifiedDatetime,
  othersType
} from "../../atom/my/othersData";

export const othersDataSelector = selector({
  key: 'othersDataSelector',
  get: ({get}) => {
    const myData = {...MyData}
    myData.profilePictureUrl = get(othersProfilePictureUrl)
    myData.marketingInfoPushNotifSettingModifiedDatetime = get(othersMarketingInfoPushNotifSettingModifiedDatetime)
    myData.birthDay = get(othersBirthDay)
    myData.profileSelectedBabyKey = get(othersProfileSelectedBabyKey)
    myData.briefBio = get(othersBriefBio)
    myData.babies = get(othersBabies)
    myData.type = get(othersType)
    myData.emailReceiveAgreementModifiedDatetime = get(othersEmailReceiveAgreementModifiedDatetime)
    myData.isTermsCollectingPersonalDataMarketingAgreed = get(othersIsTermsCollectingPersonalDataMarketingAgreed)
    myData.accountKey = get(othersAccountKey)
    myData.isEmailReceiveAgreed = get(othersIsEmailReceiveAgreed)
    myData.phoneNumber = get(othersPhoneNumber)
    myData.socialMediaUrl = get(othersSocialMediaUrl)
    myData.isPostCensorshipResultPushNotifSet = get(othersIsPostCensorshipResultPushNotifSet)
    myData.grade = get(othersGrade)
    myData.nickname = get(othersNickname)
    myData.textMessageReciveAgreementModifiedDatetime = get(othersTextMessageReciveAgreementModifiedDatetime)
    myData.isMarketingInfoPushNotifSet = get(othersIsMarketingInfoPushNotifSet)
    myData.isTextMessageReciveAgreed = get(othersIsTextMessageReciveAgreed)
    myData.key = get(othersKey)
    myData.email = get(othersEmail)
    return myData
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(othersProfilePictureUrl)
      reset(othersMarketingInfoPushNotifSettingModifiedDatetime)
      reset(othersBirthDay)
      reset(othersProfileSelectedBabyKey)
      reset(othersBriefBio)
      reset(othersBabies)
      reset(othersType)
      reset(othersEmailReceiveAgreementModifiedDatetime)
      reset(othersIsTermsCollectingPersonalDataMarketingAgreed)
      reset(othersAccountKey)
      reset(othersIsEmailReceiveAgreed)
      reset(othersPhoneNumber)
      reset(othersSocialMediaUrl)
      reset(othersIsPostCensorshipResultPushNotifSet)
      reset(othersGrade)
      reset(othersNickname)
      reset(othersTextMessageReciveAgreementModifiedDatetime)
      reset(othersIsMarketingInfoPushNotifSet)
      reset(othersIsTextMessageReciveAgreed)
      reset(othersKey)
      reset(othersEmail)
    } else {
      set(othersProfilePictureUrl, newValue.profilePictureUrl)
      set(othersMarketingInfoPushNotifSettingModifiedDatetime, newValue.marketingInfoPushNotifSettingModifiedDatetime)
      set(othersBirthDay, newValue.birthDay)
      set(othersProfileSelectedBabyKey, newValue.profileSelectedBabyKey)
      set(othersBriefBio, newValue.briefBio)
      set(othersBabies, newValue.babies)
      set(othersType, newValue.type)
      set(othersEmailReceiveAgreementModifiedDatetime, newValue.emailReceiveAgreementModifiedDatetime)
      set(othersIsTermsCollectingPersonalDataMarketingAgreed, newValue.isTermsCollectingPersonalDataMarketingAgreed)
      set(othersAccountKey, newValue.accountKey)
      set(othersIsEmailReceiveAgreed, newValue.isEmailReceiveAgreed)
      set(othersPhoneNumber, newValue.phoneNumber)
      set(othersSocialMediaUrl, newValue.socialMediaUrl)
      set(othersIsPostCensorshipResultPushNotifSet, newValue.isPostCensorshipResultPushNotifSet)
      set(othersGrade, newValue.grade)
      set(othersNickname, newValue.nickname)
      set(othersTextMessageReciveAgreementModifiedDatetime, newValue.textMessageReciveAgreementModifiedDatetime)
      set(othersIsMarketingInfoPushNotifSet, newValue.isMarketingInfoPushNotifSet)
      set(othersIsTextMessageReciveAgreed, newValue.isTextMessageReciveAgreed)
      set(othersKey, newValue.key)
      set(othersEmail, newValue.email)
    }
  },
});

export const othersProfilePictureUrlSelector = selector({
  key: 'othersProfilePictureUrlSelector',
  get: ({get}) => {
    return get(othersProfilePictureUrl)
  },
  set: ({set}, newValue) => {
    set(othersProfilePictureUrl, newValue)
  },
});

export const othersMarketingInfoPushNotifSettingModifiedDatetimeSelector = selector({
  key: 'othersMarketingInfoPushNotifSettingModifiedDatetimeSelector',
  get: ({get}) => {
    return get(othersMarketingInfoPushNotifSettingModifiedDatetime)
  },
  set: ({set}, newValue) => {
    set(othersMarketingInfoPushNotifSettingModifiedDatetime, newValue)
  },
});

export const othersBirthDaySelector = selector({
  key: 'othersBirthDaySelector',
  get: ({get}) => {
    return get(othersBirthDay)
  },
  set: ({set}, newValue) => {
    set(othersBirthDay, newValue)
  },
});

export const othersProfileSelectedBabyKeySelector = selector({
  key: 'othersProfileSelectedBabyKeySelector',
  get: ({get}) => {
    return get(othersProfileSelectedBabyKey)
  },
  set: ({set}, newValue) => {
    set(othersProfileSelectedBabyKey, newValue)
  },
});

export const othersBriefBioSelector = selector({
  key: 'othersBriefBioSelector',
  get: ({get}) => {
    return get(othersBriefBio)
  },
  set: ({set}, newValue) => {
    set(othersBriefBio, newValue)
  },
});

export const othersBabiesSelector = selector({
  key: 'othersBabiesSelector',
  get: ({get}) => {
    return get(othersBabies)
  },
  set: ({set}, newValue) => {
    set(othersBabies, newValue)
  },
});

export const othersTypeSelector = selector({
  key: 'othersTypeSelector',
  get: ({get}) => {
    return get(othersType)
  },
  set: ({set}, newValue) => {
    set(othersType, newValue)
  },
});

export const othersEmailReceiveAgreementModifiedDatetimeSelector = selector({
  key: 'othersEmailReceiveAgreementModifiedDatetimeSelector',
  get: ({get}) => {
    return get(othersEmailReceiveAgreementModifiedDatetime)
  },
  set: ({set}, newValue) => {
    set(othersEmailReceiveAgreementModifiedDatetime, newValue)
  },
});

export const othersIsTermsCollectingPersonalDataMarketingAgreedSelector = selector({
  key: 'othersIsTermsCollectingPersonalDataMarketingAgreedSelector',
  get: ({get}) => {
    return get(othersIsTermsCollectingPersonalDataMarketingAgreed)
  },
  set: ({set}, newValue) => {
    set(othersIsTermsCollectingPersonalDataMarketingAgreed, newValue)
  },
});

export const othersAccountKeySelector = selector({
  key: 'othersAccountKeySelector',
  get: ({get}) => {
    return get(othersAccountKey)
  },
  set: ({set}, newValue) => {
    set(othersAccountKey, newValue)
  },
});

export const othersIsEmailReceiveAgreedSelector = selector({
  key: 'othersIsEmailReceiveAgreedSelector',
  get: ({get}) => {
    return get(othersIsEmailReceiveAgreed)
  },
  set: ({set}, newValue) => {
    set(othersIsEmailReceiveAgreed, newValue)
  },
});

export const othersPhoneNumberSelector = selector({
  key: 'othersPhoneNumberSelector',
  get: ({get}) => {
    return get(othersPhoneNumber)
  },
  set: ({set}, newValue) => {
    set(othersPhoneNumber, newValue)
  },
});

export const othersSocialMediaUrlSelector = selector({
  key: 'othersSocialMediaUrlSelector',
  get: ({get}) => {
    return get(othersSocialMediaUrl)
  },
  set: ({set}, newValue) => {
    set(othersSocialMediaUrl, newValue)
  },
});

export const othersIsPostCensorshipResultPushNotifSetSelector = selector({
  key: 'othersIsPostCensorshipResultPushNotifSetSelector',
  get: ({get}) => {
    return get(othersIsPostCensorshipResultPushNotifSet)
  },
  set: ({set}, newValue) => {
    set(othersIsPostCensorshipResultPushNotifSet, newValue)
  },
});

export const othersGradeSelector = selector({
  key: 'othersGradeSelector',
  get: ({get}) => {
    return get(othersGrade)
  },
  set: ({set}, newValue) => {
    set(othersGrade, newValue)
  },
});

export const othersNicknameSelector = selector({
  key: 'othersNicknameSelector',
  get: ({get}) => {
    return get(othersNickname)
  },
  set: ({set}, newValue) => {
    set(othersNickname, newValue)
  },
});

export const othersTextMessageReciveAgreementModifiedDatetimeSelector = selector({
  key: 'othersTextMessageReciveAgreementModifiedDatetimeSelector',
  get: ({get}) => {
    return get(othersTextMessageReciveAgreementModifiedDatetime)
  },
  set: ({set}, newValue) => {
    set(othersTextMessageReciveAgreementModifiedDatetime, newValue)
  },
});

export const othersIsMarketingInfoPushNotifSetSelector = selector({
  key: 'othersIsMarketingInfoPushNotifSetSelector',
  get: ({get}) => {
    return get(othersIsMarketingInfoPushNotifSet)
  },
  set: ({set}, newValue) => {
    set(othersIsMarketingInfoPushNotifSet, newValue)
  },
});

export const othersIsTextMessageReciveAgreedSelector = selector({
  key: 'othersIsTextMessageReciveAgreedSelector',
  get: ({get}) => {
    return get(othersIsTextMessageReciveAgreed)
  },
  set: ({set}, newValue) => {
    set(othersIsTextMessageReciveAgreed, newValue)
  },
});

export const othersKeySelector = selector({
  key: 'othersKeySelector',
  get: ({get}) => {
    return get(othersKey)
  },
  set: ({set}, newValue) => {
    set(othersKey, newValue)
  },
});

export const othersEmailSelector = selector({
  key: 'othersEmailSelector',
  get: ({get}) => {
    return get(othersEmail)
  },
  set: ({set}, newValue) => {
    set(othersEmail, newValue)
  },
});
