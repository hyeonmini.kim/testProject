import {DefaultValue, selector} from "recoil";
import {
  forMyPageEntry,
  memberInformation,
  tempUserAvatar,
  tempUserBirth,
  tempUserChildren,
  tempUserEmail,
  tempUserGender,
  tempUserIntroduction,
  tempUserIsMomStar,
  tempUserKey,
  tempUserLevel,
  tempUserName,
  tempUserNickname,
  tempUserPassword,
  tempUserPhone,
  tempUserSnsAccount,
  tempUserSnsLink,
  userAvatar,
  userBirth,
  userChildren,
  userEmail,
  userGender,
  userIntroduction,
  userIsMomStar,
  userKey,
  userLevel,
  userName,
  userNickname,
  userPassword,
  userPhone,
  userSnsAccount,
  userSnsLink
} from "../../atom/auth/user";
import {USER} from "../../../constant/common/User";

export const userSelector = selector({
  key: 'userSelector',
  get: ({get}) => {
    const user = {...USER}
    user.user_key = get(userKey)
    user.name = get(userName)
    user.phone = get(userPhone)
    user.birth = get(userBirth)
    user.gender = get(userGender)
    user.email = get(userEmail)
    user.password = get(userPassword)
    user.nickname = get(userNickname)
    user.avatar = get(userAvatar)
    user.isMomStar = get(userIsMomStar)
    user.children = get(userChildren)
    user.level = get(userLevel)
    user.snsLink = get(userSnsLink)
    user.introduction = get(userIntroduction)
    user.snsAccount = get(userSnsAccount)
    return user
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(userKey)
      reset(userName)
      reset(userPhone)
      reset(userBirth)
      reset(userGender)
      reset(userEmail)
      reset(userPassword)
      reset(userNickname)
      reset(userAvatar)
      reset(userIsMomStar)
      reset(userChildren)
      reset(userLevel)
      reset(userSnsLink)
      reset(userIntroduction)
      reset(userSnsAccount)
    } else {
      set(userKey, newValue.user_key)
      set(userName, newValue.name)
      set(userPhone, newValue.phone)
      set(userBirth, newValue.birth)
      set(userGender, newValue.gender)
      set(userEmail, newValue.email)
      set(userPassword, newValue.password)
      set(userNickname, newValue.nickname)
      set(userAvatar, newValue.avatar)
      set(userIsMomStar, newValue.isMomStar)
      set(userChildren, newValue.children)
      set(userLevel, newValue.level)
      set(userSnsLink, newValue.snsLink)
      set(userIntroduction, newValue.introduction)
      set(userSnsAccount, newValue.snsAccount)
    }
  },
});

export const tempUserSelector = selector({
  key: 'tempUserSelector',
  get: ({get}) => {
    const user = {...USER}
    user.userKey = get(tempUserKey)
    user.name = get(tempUserName)
    user.phone = get(tempUserPhone)
    user.birth = get(tempUserBirth)
    user.gender = get(tempUserGender)
    user.email = get(tempUserEmail)
    user.password = get(tempUserPassword)
    user.nickname = get(tempUserNickname)
    user.avatar = get(tempUserAvatar)
    user.isMomStar = get(tempUserIsMomStar)
    user.children = get(tempUserChildren)
    user.level = get(tempUserLevel)
    user.snsLink = get(tempUserSnsLink)
    user.introduction = get(tempUserIntroduction)
    user.snsAccount = get(tempUserSnsAccount)
    return user
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(tempUserKey)
      reset(tempUserName)
      reset(tempUserPhone)
      reset(tempUserBirth)
      reset(tempUserGender)
      reset(tempUserEmail)
      reset(tempUserPassword)
      reset(tempUserNickname)
      reset(tempUserAvatar)
      reset(tempUserIsMomStar)
      reset(tempUserChildren)
      reset(tempUserLevel)
      reset(tempUserSnsLink)
      reset(tempUserIntroduction)
      reset(tempUserSnsAccount)
    } else {
      set(tempUserKey, newValue.userKey)
      set(tempUserName, newValue.name)
      set(tempUserPhone, newValue.phone)
      set(tempUserBirth, newValue.birth)
      set(tempUserGender, newValue.gender)
      set(tempUserEmail, newValue.email)
      set(tempUserPassword, newValue.password)
      set(tempUserNickname, newValue.nickname)
      set(tempUserAvatar, newValue.avatar)
      set(tempUserIsMomStar, newValue.isMomStar)
      set(tempUserChildren, newValue.children)
      set(tempUserLevel, newValue.level)
      set(tempUserSnsLink, newValue.snsLink)
      set(tempUserIntroduction, newValue.introduction)
      set(tempUserSnsAccount, newValue.snsAccount)
    }
  },
});

export const memberInformationSelector = selector({
  key: 'memberInformationSelector',
  get: ({get}) => {
    return get(memberInformation)
  },
  set: ({set}, newValue) => {
    set(memberInformation, newValue)
  },
})

export const forMyPageEntrySelector = selector({
  key: 'forMyPageEntrySelector',
  get: ({get}) => {
    return get(forMyPageEntry)
  },
  set: ({set}, newValue) => {
    set(forMyPageEntry, newValue)
  },
})

export const userKeySelector = selector({
  key: 'userKeySelector',
  get: ({get}) => {
    return get(userKey)
  },
  set: ({set}, newValue) => {
    set(userKey, newValue)
  },
});

export const userNameSelector = selector({
  key: 'userNameSelector',
  get: ({get}) => {
    return get(userName)
  },
  set: ({set}, newValue) => {
    set(userName, newValue)
  },
});

export const userPhoneSelector = selector({
  key: 'userPhoneSelector',
  get: ({get}) => {
    return get(userPhone)
  },
  set: ({set}, newValue) => {
    set(userPhone, newValue)
  },
});

export const userBirthSelector = selector({
  key: 'userBirthSelector',
  get: ({get}) => {
    return get(userBirth)
  },
  set: ({set}, newValue) => {
    set(userBirth, newValue)
  },
});

export const userGenderSelector = selector({
  key: 'userGenderSelector',
  get: ({get}) => {
    return get(userGender)
  },
  set: ({set}, newValue) => {
    set(userGender, newValue)
  },
});

export const userEmailSelector = selector({
  key: 'userEmailSelector',
  get: ({get}) => {
    return get(userEmail)
  },
  set: ({set}, newValue) => {
    set(userEmail, newValue)
  },
});

export const userPasswordSelector = selector({
  key: 'userPasswordSelector',
  get: ({get}) => {
    return get(userPassword)
  },
  set: ({set}, newValue) => {
    set(userPassword, newValue)
  },
});

export const userNicknameSelector = selector({
  key: 'userNicknameSelector',
  get: ({get}) => {
    return get(userNickname)
  },
  set: ({set}, newValue) => {
    set(userNickname, newValue)
  },
});

export const userAvatarSelector = selector({
  key: 'userAvatarSelector',
  get: ({get}) => {
    return get(userAvatar)
  },
  set: ({set}, newValue) => {
    set(userAvatar, newValue)
  },
});

export const userIsMomStarSelector = selector({
  key: 'userIsMomStarSelector',
  get: ({get}) => {
    return get(userIsMomStar)
  },
  set: ({set}, newValue) => {
    set(userIsMomStar, newValue)
  },
});

export const userChildrenSelector = selector({
  key: 'userChildrenSelector',
  get: ({get}) => {
    return get(userChildren)
  },
  set: ({set}, newValue) => {
    set(userChildren, newValue)
  },
});

export const userLevelSelector = selector({
  key: 'userLevelSelector',
  get: ({get}) => {
    return get(userLevel)
  },
  set: ({set}, newValue) => {
    set(userLevel, newValue)
  },
});

export const userSnsLinkSelector = selector({
  key: 'userSnsLinkSelector',
  get: ({get}) => {
    return get(userSnsLink)
  },
  set: ({set}, newValue) => {
    set(userSnsLink, newValue)
  },
});

export const userIntroductionSelector = selector({
  key: 'userIntroductionSelector',
  get: ({get}) => {
    return get(userIntroduction)
  },
  set: ({set}, newValue) => {
    set(userIntroduction, newValue)
  },
});

export const userSnsAccountSelector = selector({
  key: 'userSnsAccountSelector',
  get: ({get}) => {
    return get(userSnsAccount)
  },
  set: ({set}, newValue) => {
    set(userSnsAccount, newValue)
  },
});
