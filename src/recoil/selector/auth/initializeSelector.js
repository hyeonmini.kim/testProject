import {selector} from "recoil";
import {isInitAxios, isInitEnv, isInitialized} from "../../atom/auth/isInitialized";

export const initializeSelector = selector({
  key: 'initializeSelector',
  get: ({get}) => {
    return get(isInitialized)
  },
  set: ({set}, newValue) => {
    set(isInitialized, newValue)
  },
});

export const initAxiosSelector = selector({
  key: 'initAxiosSelector',
  get: ({get}) => {
    return get(isInitAxios)
  },
  set: ({set}, newValue) => {
    set(isInitAxios, newValue)
  },
});

export const initEnvSelector = selector({
  key: 'initEnvSelector',
  get: ({get}) => {
    return get(isInitEnv)
  },
  set: ({set}, newValue) => {
    set(isInitEnv, newValue)
  },
});