import {selector} from "recoil";
import {accessToken, refreshToken, socialAccountId, transactionSeqNumber} from "../../atom/auth/accessToken";

export const accessTokenSelector = selector({
  key: 'accessTokenSelector',
  get: ({get}) => {
    return get(accessToken)
  },
  set: ({set}, newValue) => {
    set(accessToken, newValue)
  },
})

export const refreshTokenSelector = selector({
  key: 'refreshTokenSelector',
  get: ({get}) => {
    return get(refreshToken)
  },
  set: ({set}, newValue) => {
    set(refreshToken, newValue)
  },
})

export const socialAccountIdSelector = selector({
  key: 'socialAccountIdSelector',
  get: ({get}) => {
    return get(socialAccountId)
  },
  set: ({set}, newValue) => {
    set(socialAccountId, newValue)
  },
})

export const transactionSeqNumberSelector = selector({
  key: 'transactionSeqNumberSelector',
  get: ({get}) => {
    return get(transactionSeqNumber)
  },
  set: ({set}, newValue) => {
    set(transactionSeqNumber, newValue)
  },
})
