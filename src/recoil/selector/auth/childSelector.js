import {DefaultValue, selector} from "recoil";
import {CHILD} from "../../../constant/common/User";
import {
  childAllergy,
  childAvatar,
  childBirth,
  childGender,
  childHeight,
  childId,
  childKeyword,
  childNickname,
  childWeight
} from "../../atom/auth/child";

export const childSelector = selector({
  key: 'childSelector',
  get: ({get}) => {
    const child = {...CHILD}
    child.id = get(childId)
    child.nickname = get(childNickname)
    child.gender = get(childGender)
    child.birth = get(childBirth)
    child.weight = get(childWeight)
    child.height = get(childHeight)
    child.allergy = get(childAllergy)
    child.keyword = get(childKeyword)
    child.avatar = get(childAvatar)
    return child
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(childId)
      reset(childNickname)
      reset(childGender)
      reset(childBirth)
      reset(childWeight)
      reset(childHeight)
      reset(childAllergy)
      reset(childKeyword)
      reset(childAvatar)
    } else {
      set(childId, newValue.id)
      set(childNickname, newValue.nickname)
      set(childGender, newValue.gender)
      set(childBirth, newValue.birth)
      set(childWeight, newValue.weight)
      set(childHeight, newValue.height)
      set(childAllergy, newValue.allergy)
      set(childKeyword, newValue.keyword)
      set(childAvatar, newValue.avatar)
    }
  },
});

export const childIdSelector = selector({
  key: 'childIdSelector',
  get: ({get}) => {
    return get(childId)
  },
  set: ({set}, newValue) => {
    set(childId, newValue)
  },
});

export const childNicknameSelector = selector({
  key: 'childNicknameSelector',
  get: ({get}) => {
    return get(childNickname)
  },
  set: ({set}, newValue) => {
    set(childNickname, newValue)
  },
});

export const childGenderSelector = selector({
  key: 'childGenderSelector',
  get: ({get}) => {
    return get(childGender)
  },
  set: ({set}, newValue) => {
    set(childGender, newValue)
  },
});

export const childBirthSelector = selector({
  key: 'childBirthSelector',
  get: ({get}) => {
    return get(childBirth)
  },
  set: ({set}, newValue) => {
    set(childBirth, newValue)
  },
});

export const childWeightSelector = selector({
  key: 'childWeightSelector',
  get: ({get}) => {
    return get(childWeight)
  },
  set: ({set}, newValue) => {
    set(childWeight, newValue)
  },
});

export const childHeightSelector = selector({
  key: 'childHeightSelector',
  get: ({get}) => {
    return get(childHeight)
  },
  set: ({set}, newValue) => {
    set(childHeight, newValue)
  },
});

export const childAllergySelector = selector({
  key: 'childAllergySelector',
  get: ({get}) => {
    return get(childAllergy)
  },
  set: ({set}, newValue) => {
    set(childAllergy, newValue)
  },
});

export const childKeywordSelector = selector({
  key: 'childKeywordSelector',
  get: ({get}) => {
    return get(childKeyword)
  },
  set: ({set}, newValue) => {
    set(childKeyword, newValue)
  },
});

export const childAvatarSelector = selector({
  key: 'childAvatarSelector',
  get: ({get}) => {
    return get(childAvatar)
  },
  set: ({set}, newValue) => {
    set(childAvatar, newValue)
  },
});
