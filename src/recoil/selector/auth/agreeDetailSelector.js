import {selector} from "recoil";
import {commonAgreeOpen, commonIsLoaded} from "../../atom/auth/agreeDetail";

export const commonAgreeOpenSelector = selector({
  key: 'commonAgreeOpenSelector',
  get: ({get}) => {
    return get(commonAgreeOpen)
  },
  set: ({set}, newValue) => {
    set(commonAgreeOpen, newValue)
  },
});

export const commonIsLoadedSelector = selector({
  key: 'commonIsLoadedSelector',
  get: ({get}) => {
    return get(commonIsLoaded)
  },
  set: ({set}, newValue) => {
    set(commonIsLoaded, newValue)
  },
});