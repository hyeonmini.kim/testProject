import {selector} from "recoil";
import {isAuthenticated} from "../../atom/auth/isAuthenticated";

export const authenticateSelector = selector({
  key: 'authenticateSelector',
  get: ({get}) => {
    return get(isAuthenticated)
  },
  set: ({set}, newValue) => {
    set(isAuthenticated, newValue)
  },
});