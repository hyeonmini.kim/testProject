import {DefaultValue, selector} from "recoil";
import {IDENTIFICATION_QA_INFORM} from "../../../constant/common/User";
import {
  firstAnswerState,
  firstQuestionCodeState,
  firstQuestionTextState,
  questionNumState,
  secondAnswerState,
  secondQuestionCodeState,
  secondQuestionTextState
} from "../../atom/sign-up/auth";

export const identificationQASelector = selector({
  key: 'identificationQASelector',
  get: ({get}) => {
    const child = {...IDENTIFICATION_QA_INFORM}
    child.questionNum = get(questionNumSelector)
    child.firstQuestionCode = get(firstQuestionCodeSelector)
    child.secondQuestionCode = get(secondQuestionCodeSelector)
    child.firstQuestionText = get(firstQuestionTextSelector)
    child.secondQuestionText = get(secondQuestionTextSelector)
    child.firstAnswer = get(firstAnswerSelector)
    child.secondAnswer = get(secondAnswerSelector)
    return child
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(questionNumSelector)
      reset(firstQuestionCodeSelector)
      reset(secondQuestionCodeSelector)
      reset(firstQuestionTextSelector)
      reset(secondQuestionTextSelector)
      reset(firstAnswerSelector)
      reset(secondAnswerSelector)
    } else {
      set(questionNumSelector, newValue.questionNum)
      set(firstQuestionCodeSelector, newValue.firstQuestionCode)
      set(secondQuestionCodeSelector, newValue.secondQuestionCode)
      set(firstQuestionTextSelector, newValue.firstQuestionText)
      set(secondQuestionTextSelector, newValue.secondQuestionText)
      set(firstAnswerSelector, newValue.firstAnswer)
      set(secondAnswerSelector, newValue.secondAnswer)
    }
  },
});

export const questionNumSelector = selector({
  key: 'questionNumSelector',
  get: ({get}) => {
    return get(questionNumState)
  },
  set: ({set}, newValue) => {
    set(questionNumState, newValue)
  },
});

export const firstQuestionCodeSelector = selector({
  key: 'firstQuestionCodeSelector',
  get: ({get}) => {
    return get(firstQuestionCodeState)
  },
  set: ({set}, newValue) => {
    set(firstQuestionCodeState, newValue)
  },
});

export const secondQuestionCodeSelector = selector({
  key: 'secondQuestionCodeSelector',
  get: ({get}) => {
    return get(secondQuestionCodeState)
  },
  set: ({set}, newValue) => {
    set(secondQuestionCodeState, newValue)
  },
});

export const firstQuestionTextSelector = selector({
  key: 'firstQuestionTextSelector',
  get: ({get}) => {
    return get(firstQuestionTextState)
  },
  set: ({set}, newValue) => {
    set(firstQuestionTextState, newValue)
  },
});

export const secondQuestionTextSelector = selector({
  key: 'secondQuestionTextSelector',
  get: ({get}) => {
    return get(secondQuestionTextState)
  },
  set: ({set}, newValue) => {
    set(secondQuestionTextState, newValue)
  },
});

export const firstAnswerSelector = selector({
  key: 'firstAnswerSelector',
  get: ({get}) => {
    return get(firstAnswerState)
  },
  set: ({set}, newValue) => {
    set(firstAnswerState, newValue)
  },
});

export const secondAnswerSelector = selector({
  key: 'secondAnswerSelector',
  get: ({get}) => {
    return get(secondAnswerState)
  },
  set: ({set}, newValue) => {
    set(secondAnswerState, newValue)
  },
});
