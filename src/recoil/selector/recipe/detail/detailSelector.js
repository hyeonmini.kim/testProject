import {selector} from "recoil";
import {
  recipeDetailPopupDialogOpenStatus,
  recipeDetailPopupDialogType,
  recipeDetailSlideIndex,
  recipeDetailUpdateCount
} from "../../../atom/recipe/detail/detail";

export const recipeDetailPopupDialogOpenStatusSelector = selector({
  key: 'recipeDetailPopupDialogOpenStatusSelector',
  get: ({get}) => {
    return get(recipeDetailPopupDialogOpenStatus)
  },
  set: ({set}, newValue) => {
    set(recipeDetailPopupDialogOpenStatus, newValue)
  },
});

export const recipeDetailPopupDialogTypeSelector = selector({
  key: 'recipeDetailPopupDialogTypeSelector',
  get: ({get}) => {
    return get(recipeDetailPopupDialogType)
  },
  set: ({set}, newValue) => {
    set(recipeDetailPopupDialogType, newValue)
  },
});

export const recipeDetailSlideIndexSelector = selector({
  key: 'recipeDetailSlideIndexSelector',
  get: ({get}) => {
    return get(recipeDetailSlideIndex)
  },
  set: ({set}, newValue) => {
    set(recipeDetailSlideIndex, newValue)
  },
});

export const recipeDetailUpdateCountSelector = selector({
  key: 'recipeDetailUpdateCountSelector',
  get: ({get}) => {
    return get(recipeDetailUpdateCount)
  },
  set: ({set}, newValue) => {
    set(recipeDetailUpdateCount, newValue)
  },
});