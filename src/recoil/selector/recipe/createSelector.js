import {
  createActiveStep,
  createEmpty,
  createOrderDeleteImages,
  createOrderImages,
  createRecipeBasicMaterials,
  createRecipeCategory,
  createRecipeCookTime,
  createRecipeDeleteImages,
  createRecipeDesc,
  createRecipeDifficulty,
  createRecipeHealthNote,
  createRecipeImages,
  createRecipeKey,
  createRecipeMaterialType,
  createRecipeMonthlyAge,
  createRecipeName,
  createRecipeQuantity,
  createRecipeSeasoningMaterials,
  createRecipeSteps,
  createReturnMessage,
  createShowMaterialAdd,
  createShowMaterialEdit,
  createShowMaterialInput,
  createShowMeasurementUnit,
  createShowPreview,
  createShowReturn,
  createTag,
  createTemporaryRecipe,
} from "../../atom/recipe/create";
import {DefaultValue, selector} from "recoil";
import {MATERIAL_TYPE} from "../../../constant/recipe/Material";
import {RECIPE} from "../../../constant/recipe/Recipe";

export const createRecipeSelector = selector({
  key: 'createRecipeSelector',
  get: ({get}) => {
    const recipe = {...RECIPE}
    recipe.recipe_key = get(createRecipeKey)
    recipe.recipe_name = get(createRecipeName)
    recipe.recipe_desc = get(createRecipeDesc)
    recipe.recipe_category = get(createRecipeCategory)
    recipe.recipe_lead_time = get(createRecipeCookTime)
    recipe.recipe_level = get(createRecipeDifficulty)
    recipe.recipe_babyfood_step = get(createRecipeMonthlyAge)
    recipe.recipe_health_note = get(createRecipeHealthNote)
    recipe.recipe_servings = get(createRecipeQuantity)
    recipe.recipe_ingredient_list = [...get(createRecipeBasicMaterials), ...get(createRecipeSeasoningMaterials)]
    recipe.recipe_order_list = get(createRecipeSteps)
    recipe.recipe_reject_msg = get(createReturnMessage)
    recipe.recipe_tag_desc = get(createTag)
    return recipe
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(createRecipeKey)
      reset(createActiveStep)
      reset(createRecipeName)
      reset(createRecipeDesc)
      reset(createRecipeCategory)
      reset(createRecipeImages)
      reset(createRecipeCookTime)
      reset(createRecipeDifficulty)
      reset(createRecipeMonthlyAge)
      reset(createRecipeHealthNote)
      reset(createRecipeQuantity)
      reset(createRecipeBasicMaterials)
      reset(createRecipeSeasoningMaterials)
      reset(createRecipeMaterialType)
      reset(createRecipeSteps)
      reset(createReturnMessage)
      reset(createTag)
      reset(createRecipeDeleteImages)
      reset(createOrderDeleteImages)
    } else {
      set(createRecipeKey, newValue?.recipe_key)
      set(createRecipeName, newValue?.recipe_name)
      set(createRecipeDesc, newValue?.recipe_desc)
      set(createRecipeCategory, newValue?.recipe_category)
      set(createRecipeCookTime, newValue?.recipe_lead_time)
      set(createRecipeDifficulty, newValue?.recipe_level)
      set(createRecipeMonthlyAge, newValue?.recipe_babyfood_step)
      set(createRecipeHealthNote, newValue?.recipe_health_note)
      set(createRecipeImages, newValue?.image_file_list?.map((file, index) => {
        return {
          index: index,
          ...file,
        }
      }))
      set(createRecipeQuantity, newValue?.recipe_servings ? newValue?.recipe_servings : 1)
      set(createReturnMessage, newValue?.recipe_reject_msg)
      const basicMaterials = newValue?.recipe_ingredient_list?.filter((ingredient) => ingredient?.recipe_ingredient_category === MATERIAL_TYPE.BASIC)
      const seasoningMaterials = newValue?.recipe_ingredient_list?.filter((ingredient) => ingredient?.recipe_ingredient_category === MATERIAL_TYPE.SEASONING)
      set(createRecipeBasicMaterials, basicMaterials?.length ? basicMaterials : [])
      set(createRecipeSeasoningMaterials, seasoningMaterials?.length ? seasoningMaterials : [])
      set(createRecipeSteps, newValue?.recipe_order_list?.map((file, index) => {
        return {
          index: index,
          ...file,
        }
      }))
      set(createTag, newValue?.recipe_tag_list?.map((tag) => tag?.recipe_tag_desc).join(','))
    }
  },
});

export const createRecipeKeySelector = selector({
  key: 'createRecipeKeySelector',
  get: ({get}) => {
    return get(createRecipeKey)
  },
  set: ({set}, newValue) => {
    set(createRecipeKey, newValue)
  },
});

export const createRecipeNameSelector = selector({
  key: 'createRecipeNameSelector',
  get: ({get}) => {
    return get(createRecipeName)
  },
  set: ({set}, newValue) => {
    set(createRecipeName, newValue)
  },
});

export const createRecipeDescSelector = selector({
  key: 'createRecipeDescSelector',
  get: ({get}) => {
    return get(createRecipeDesc)
  },
  set: ({set}, newValue) => {
    set(createRecipeDesc, newValue)
  },
});

export const createRecipeCategorySelector = selector({
  key: 'createRecipeCategorySelector',
  get: ({get}) => {
    return get(createRecipeCategory)
  },
  set: ({set}, newValue) => {
    set(createRecipeCategory, newValue)
  },
});

export const createRecipeImagesSelector = selector({
  key: 'createRecipeImagesSelector',
  get: ({get}) => {
    return get(createRecipeImages)
  },
  set: ({set}, newValue) => {
    set(createRecipeImages, newValue)
  },
});

export const createRecipeDeleteImagesSelector = selector({
  key: 'createRecipeDeleteImagesSelector',
  get: ({get}) => {
    return get(createRecipeDeleteImages)
  },
  set: ({set}, newValue) => {
    set(createRecipeDeleteImages, newValue)
  },
});

export const createRecipeCookTimeSelector = selector({
  key: 'createRecipeCookTimeSelector',
  get: ({get}) => {
    return get(createRecipeCookTime)
  },
  set: ({set}, newValue) => {
    set(createRecipeCookTime, newValue)
  },
});

export const createRecipeDifficultySelector = selector({
  key: 'createRecipeDifficultySelector',
  get: ({get}) => {
    return get(createRecipeDifficulty)
  },
  set: ({set}, newValue) => {
    set(createRecipeDifficulty, newValue)
  },
});

export const createRecipeMonthlyAgeSelector = selector({
  key: 'createRecipeMonthlyAgeSelector',
  get: ({get}) => {
    return get(createRecipeMonthlyAge)
  },
  set: ({set}, newValue) => {
    set(createRecipeMonthlyAge, newValue)
  },
});

export const createRecipeHealthNoteSelector = selector({
  key: 'createRecipeHealthNoteSelector',
  get: ({get}) => {
    return get(createRecipeHealthNote)
  },
  set: ({set}, newValue) => {
    set(createRecipeHealthNote, newValue)
  },
});

export const createRecipeQuantitySelector = selector({
  key: 'createRecipeQuantitySelector',
  get: ({get}) => {
    return get(createRecipeQuantity)
  },
  set: ({set}, newValue) => {
    set(createRecipeQuantity, newValue)
  },
});

export const createRecipeBasicMaterialsSelector = selector({
  key: 'createRecipeBasicMaterialsSelector',
  get: ({get}) => {
    return get(createRecipeBasicMaterials)
  },
  set: ({set}, newValue) => {
    set(createRecipeBasicMaterials, newValue)
  },
});

export const createRecipeSeasoningMaterialsSelector = selector({
  key: 'createRecipeSeasoningMaterialsSelector',
  get: ({get}) => {
    return get(createRecipeSeasoningMaterials)
  },
  set: ({set}, newValue) => {
    set(createRecipeSeasoningMaterials, newValue)
  },
});

export const createRecipeMaterialTypeSelector = selector({
  key: 'createRecipeMaterialTypeSelector',
  get: ({get}) => {
    return get(createRecipeMaterialType)
  },
  set: ({set}, newValue) => {
    set(createRecipeMaterialType, newValue)
  },
});

export const createRecipeMaterialsSelector = selector({
  key: 'createRecipeMaterialsSelector',
  get: ({get}) => {
    const type = get(createRecipeMaterialType)
    switch (type) {
      case MATERIAL_TYPE.BASIC:
      default:
        return get(createRecipeBasicMaterials)
      case MATERIAL_TYPE.SEASONING:
        return get(createRecipeSeasoningMaterials)
    }
    return get(createRecipeBasicMaterials)
  },
  set: ({get, set}, newValue) => {
    const type = get(createRecipeMaterialType)
    switch (type) {
      case MATERIAL_TYPE.BASIC:
      default:
        set(createRecipeBasicMaterials, newValue)
        break;
      case MATERIAL_TYPE.SEASONING:
        set(createRecipeSeasoningMaterials, newValue)
        break;
    }
  },
});

export const createRecipeStepsSelector = selector({
  key: 'createRecipeStepsSelector',
  get: ({get}) => {
    return get(createRecipeSteps)
  },
  set: ({set}, newValue) => {
    set(createRecipeSteps, newValue)
  },
});

export const createTemporaryRecipeSelector = selector({
  key: 'createTemporaryRecipeSelector',
  get: ({get}) => {
    return get(createTemporaryRecipe)
  },
  set: ({set}, newValue) => {
    set(createTemporaryRecipe, newValue)
  },
});

export const createShowMaterialEditSelector = selector({
  key: 'createShowMaterialEditSelector',
  get: ({get}) => {
    return get(createShowMaterialEdit)
  },
  set: ({set}, newValue) => {
    set(createShowMaterialEdit, newValue)
  },
});

export const createShowMaterialAddSelector = selector({
  key: 'createShowMaterialAddSelector',
  get: ({get}) => {
    return get(createShowMaterialAdd)
  },
  set: ({set}, newValue) => {
    set(createShowMaterialAdd, newValue)
  },
});

export const createShowMaterialInputSelector = selector({
  key: 'createShowMaterialInputSelector',
  get: ({get}) => {
    return get(createShowMaterialInput)
  },
  set: ({set}, newValue) => {
    set(createShowMaterialInput, newValue)
  },
});

export const createShowMeasurementUnitSelector = selector({
  key: 'createShowMeasurementUnitSelector',
  get: ({get}) => {
    return get(createShowMeasurementUnit)
  },
  set: ({set}, newValue) => {
    set(createShowMeasurementUnit, newValue)
  },
});

export const createShowPreviewSelector = selector({
  key: 'createShowPreviewSelector',
  get: ({get}) => {
    return get(createShowPreview)
  },
  set: ({set}, newValue) => {
    set(createShowPreview, newValue)
  },
});

export const createActiveStepSelector = selector({
  key: 'createActiveStepSelector',
  get: ({get}) => {
    return get(createActiveStep)
  },
  set: ({set}, newValue) => {
    set(createActiveStep, newValue)
  },
});

export const createShowReturnSelector = selector({
  key: 'createShowReturnSelector',
  get: ({get}) => {
    return get(createShowReturn)
  },
  set: ({set}, newValue) => {
    set(createShowReturn, newValue)
  },
});

export const createReturnMessageSelector = selector({
  key: 'createReturnMessageSelector',
  get: ({get}) => {
    return get(createReturnMessage)
  },
  set: ({set}, newValue) => {
    set(createReturnMessage, newValue)
  },
});

export const createTagSelector = selector({
  key: 'createTagSelector',
  get: ({get}) => {
    return get(createTag)
  },
  set: ({set}, newValue) => {
    set(createTag, newValue)
  },
});

export const createEmptySelector = selector({
  key: 'createEmptySelector',
  get: ({get}) => {
    return get(createEmpty)
  },
  set: ({set}, newValue) => {
    set(createEmpty, newValue)
  },
});

export const createOrderImagesSelector = selector({
  key: 'createOrderImagesSelector',
  get: ({get}) => {
    return get(createOrderImages)
  },
  set: ({set}, newValue) => {
    set(createOrderImages, newValue)
  },
});

export const createOrderDeleteImagesSelector = selector({
  key: 'createOrderDeleteImagesSelector',
  get: ({get}) => {
    return get(createOrderDeleteImages)
  },
  set: ({set}, newValue) => {
    set(createOrderDeleteImages, newValue)
  },
});