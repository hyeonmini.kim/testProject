import {DefaultValue, selector} from "recoil";
import {
  searchActiveTab,
  searchAgeGroup,
  searchCall, searchCategory,
  searchFetchItems,
  searchIsAvoid,
  searchIsCall,
  searchIsHome,
  searchIsSearch,
  searchIsTitle,
  searchRecipeResult,
  searchRecipeSearch,
  searchShowDetail,
  searchShowFilter
} from "../../atom/recipe/search";
import {RECIPE_RESULT_STATUS, RECIPE_SEARCH_AGE_GROUP_TYPE, RECIPE_SEARCH_HOME_HEALTH} from "../../../constant/recipe/Search";

const SEARCH = {
  search: '',
  result: RECIPE_RESULT_STATUS.INIT,
  isCall: null,
  isTitle: false,
  isSearch: false,
  category: false,
  isAvoid: false,
  ageGroup: RECIPE_SEARCH_AGE_GROUP_TYPE.ALL,
  activeTab: RECIPE_SEARCH_HOME_HEALTH.NAME,
  showFilter: true,
  fetchItems: [],
}

export const searchRecipeSelector = selector({
  key: 'searchRecipeSelector',
  get: ({get}) => {
    const search = {...SEARCH}
    search.search = get(searchRecipeSearch)
    search.result = get(searchRecipeResult)
    search.isCall = get(searchIsCall)
    search.isTitle = get(searchIsTitle)
    search.isSearch = get(searchIsSearch)
    search.category = get(searchCategory)
    search.isAvoid = get(searchIsAvoid)
    search.ageGroup = get(searchAgeGroup)
    search.activeTab = get(searchActiveTab)
    search.showFilter = get(searchShowFilter)
    search.fetchItems = get(searchFetchItems)
    return search
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(searchRecipeSearch)
      reset(searchRecipeResult)
      reset(searchIsCall)
      reset(searchIsTitle)
      reset(searchIsSearch)
      reset(searchCategory)
      reset(searchIsAvoid)
      reset(searchAgeGroup)
      reset(searchActiveTab)
      reset(searchShowFilter)
      reset(searchFetchItems)
    } else {
      set(searchRecipeSearch, newValue?.search)
      set(searchRecipeResult, newValue?.result)
      set(searchIsCall, newValue?.isCall)
      set(searchIsTitle, newValue?.isTitle)
      set(searchAgeGroup, newValue?.ageGroup)
      set(searchActiveTab, newValue?.activeTab)
      set(searchFetchItems, newValue?.items)
      set(searchIsSearch, newValue?.isSearch)
      set(searchIsAvoid, newValue?.isAvoid)
      set(searchCategory, newValue?.category)
      set(searchShowFilter, newValue?.showFilter)
    }
  },
});

export const searchRecipeSearchSelector = selector({
  key: 'searchRecipeSearchSelector',
  get: ({get}) => {
    return get(searchRecipeSearch)
  },
  set: ({set}, newValue) => {
    set(searchRecipeSearch, newValue)
  },
});

export const searchRecipeResultSelector = selector({
  key: 'searchRecipeResultSelector',
  get: ({get}) => {
    return get(searchRecipeResult)
  },
  set: ({set}, newValue) => {
    set(searchRecipeResult, newValue)
  },
});

export const searchIsTitleSelector = selector({
  key: 'searchIsTitleSelector',
  get: ({get}) => {
    return get(searchIsTitle)
  },
  set: ({set}, newValue) => {
    set(searchIsTitle, newValue)
  },
});

export const searchCallSelector = selector({
  key: 'searchCallSelector',
  get: ({get}) => {
    return get(searchCall)
  },
  set: ({set}, newValue) => {
    set(searchCall, newValue)
  },
});

export const searchAgeGroupSelector = selector({
  key: 'searchAgeGroupSelector',
  get: ({get}) => {
    return get(searchAgeGroup)
  },
  set: ({set}, newValue) => {
    set(searchAgeGroup, newValue)
  },
});

export const searchShowDetailSelector = selector({
  key: 'searchShowDetailSelector',
  get: ({get}) => {
    return get(searchShowDetail)
  },
  set: ({set}, newValue) => {
    set(searchShowDetail, newValue)
  },
});

export const searchIsHomeSelector = selector({
  key: 'searchIsHomeSelector',
  get: ({get}) => {
    return get(searchIsHome)
  },
  set: ({set}, newValue) => {
    set(searchIsHome, newValue)
  },
});

export const searchIsAvoidSelector = selector({
  key: 'searchIsAvoidSelector',
  get: ({get}) => {
    return get(searchIsAvoid)
  },
  set: ({set}, newValue) => {
    set(searchIsAvoid, newValue)
  },
});

export const searchIsSearchSelector = selector({
  key: 'searchIsSearchSelector',
  get: ({get}) => {
    return get(searchIsSearch)
  },
  set: ({set}, newValue) => {
    set(searchIsSearch, newValue)
  },
});

export const searchCategorySelector = selector({
  key: 'searchCategorySelector',
  get: ({get}) => {
    return get(searchCategory)
  },
  set: ({set}, newValue) => {
    set(searchCategory, newValue)
  },
});

export const searchActiveTabSelector = selector({
  key: 'searchActiveTabSelector',
  get: ({get}) => {
    return get(searchActiveTab)
  },
  set: ({set}, newValue) => {
    set(searchActiveTab, newValue)
  },
});

export const searchIsCallSelector = selector({
  key: 'searchIsCallSelector',
  get: ({get}) => {
    return get(searchIsCall)
  },
  set: ({set}, newValue) => {
    set(searchIsCall, newValue)
  },
});

export const searchShowFilterSelector = selector({
  key: 'searchShowFilterSelector',
  get: ({get}) => {
    return get(searchShowFilter)
  },
  set: ({set}, newValue) => {
    set(searchShowFilter, newValue)
  },
});

export const searchFetchItemsSelector = selector({
  key: 'searchFetchItemsSelector',
  get: ({get}) => {
    return get(searchFetchItems)
  },
  set: ({set}, newValue) => {
    set(searchFetchItems, newValue)
  },
});