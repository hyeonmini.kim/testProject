import {selector} from "recoil";
import {
  labBabyFoodActiveStep,
  labBabyFoodList,
  labBabyFoodReasonList,
  labBabyFoodSelectBrandCd,
  labBabyFoodSelectBrandName,
  labBabyFoodSelectReasonList,
  labCommentCheck,
  labCommentCommentKey,
  labCommentFetch,
  labCommentItem,
  labCommentOrder,
  labCommentPostKey,
  labCommentShowCommentModify,
  labCommentShowCommentView,
  labIsBabyFoodEtcSelected,
  labIsBottomButtonDisabled,
  labIsItemSelected,
  labIsNutritionSUR03EtcSelected,
  labIsNutritionSUR04EtcSelected,
  labIsNutritionSUR05EtcSelected,
  labIsSUR03Selected,
  labIsSUR04Selected,
  labIsSUR05Selected,
  labIsSUR06Selected,
  labNutritionActiveStep,
  labNutritionActiveStepList,
  labNutritionSelectedLength,
  labNutritionSelectList,
  labNutritionSUR03List,
  labNutritionSUR03ReasonList,
  labNutritionSUR03SelectBrandCd,
  labNutritionSUR03SelectBrandName,
  labNutritionSUR03SelectReasonList,
  labNutritionSUR04List,
  labNutritionSUR04ReasonList,
  labNutritionSUR04SelectBrandCd,
  labNutritionSUR04SelectBrandName,
  labNutritionSUR04SelectReasonList,
  labNutritionSUR05List,
  labNutritionSUR05ReasonList,
  labNutritionSUR05SelectBrandCd,
  labNutritionSUR05SelectBrandName,
  labNutritionSUR05SelectReasonList,
  labRecommendList,
  labScore,
  labSelectedSurveyType
} from "../../atom/lab/lab";

export const labSelectedSurveyTypeSelector = selector({
  key: 'labSelectedSurveyTypeSelector',
  get: ({get}) => {
    return get(labSelectedSurveyType)
  },
  set: ({set}, newValue) => {
    set(labSelectedSurveyType, newValue)
  },
});

export const labBabyFoodActiveStepSelector = selector({
  key: 'labBabyFoodActiveStepSelector',
  get: ({get}) => {
    return get(labBabyFoodActiveStep)
  },
  set: ({set}, newValue) => {
    set(labBabyFoodActiveStep, newValue)
  },
});

export const labNutritionActiveStepSelector = selector({
  key: 'labNutritionActiveStepSelector',
  get: ({get}) => {
    return get(labNutritionActiveStep)
  },
  set: ({set}, newValue) => {
    set(labNutritionActiveStep, newValue)
  },
});

export const labNutritionActiveStepListSelector = selector({
  key: 'labNutritionActiveStepListSelector',
  get: ({get}) => {
    return get(labNutritionActiveStepList)
  },
  set: ({set}, newValue) => {
    set(labNutritionActiveStepList, newValue)
  },
});

export const labIsBottomButtonDisabledSelector = selector({
  key: 'labIsBottomButtonDisabledSelector',
  get: ({get}) => {
    return get(labIsBottomButtonDisabled)
  },
  set: ({set}, newValue) => {
    set(labIsBottomButtonDisabled, newValue)
  },
});

export const labNutritionSelectListSelector = selector({
  key: 'labNutritionSelectListSelector',
  get: ({get}) => {
    return get(labNutritionSelectList)
  },
  set: ({set}, newValue) => {
    set(labNutritionSelectList, newValue)
  },
});

export const labNutritionSelectedLengthSelector = selector({
  key: 'labNutritionSelectedLengthSelector',
  get: ({get}) => {
    return get(labNutritionSelectedLength)
  },
  set: ({set}, newValue) => {
    set(labNutritionSelectedLength, newValue)
  },
});

export const labIsSUR03SelectedSelector = selector({
  key: 'labIsSUR03SelectedSelector',
  get: ({get}) => {
    return get(labIsSUR03Selected)
  },
  set: ({set}, newValue) => {
    set(labIsSUR03Selected, newValue)
  },
});

export const labIsSUR04SelectedSelector = selector({
  key: 'labIsSUR04SelectedSelector',
  get: ({get}) => {
    return get(labIsSUR04Selected)
  },
  set: ({set}, newValue) => {
    set(labIsSUR04Selected, newValue)
  },
});

export const labIsSUR05SelectedSelector = selector({
  key: 'labIsSUR05SelectedSelector',
  get: ({get}) => {
    return get(labIsSUR05Selected)
  },
  set: ({set}, newValue) => {
    set(labIsSUR05Selected, newValue)
  },
});

export const labIsSUR06SelectedSelector = selector({
  key: 'labIsSUR06SelectedSelector',
  get: ({get}) => {
    return get(labIsSUR06Selected)
  },
  set: ({set}, newValue) => {
    set(labIsSUR06Selected, newValue)
  },
});

export const labBabyFoodListSelector = selector({
  key: 'labBabyFoodListSelector',
  get: ({get}) => {
    return get(labBabyFoodList)
  },
  set: ({set}, newValue) => {
    set(labBabyFoodList, newValue)
  },
});

export const labIsBabyFoodEtcSelectedSelector = selector({
  key: 'labIsBabyFoodEtcSelectedSelector',
  get: ({get}) => {
    return get(labIsBabyFoodEtcSelected)
  },
  set: ({set}, newValue) => {
    set(labIsBabyFoodEtcSelected, newValue)
  },
});

export const labIsNutritionSUR03EtcSelectedSelector = selector({
  key: 'labIsNutritionSUR03EtcSelectedSelector',
  get: ({get}) => {
    return get(labIsNutritionSUR03EtcSelected)
  },
  set: ({set}, newValue) => {
    set(labIsNutritionSUR03EtcSelected, newValue)
  },
});

export const labIsNutritionSUR04EtcSelectedSelector = selector({
  key: 'labIsNutritionSUR04EtcSelectedSelector',
  get: ({get}) => {
    return get(labIsNutritionSUR04EtcSelected)
  },
  set: ({set}, newValue) => {
    set(labIsNutritionSUR04EtcSelected, newValue)
  },
});

export const labIsNutritionSUR05EtcSelectedSelector = selector({
  key: 'labIsNutritionSUR05EtcSelectedSelector',
  get: ({get}) => {
    return get(labIsNutritionSUR05EtcSelected)
  },
  set: ({set}, newValue) => {
    set(labIsNutritionSUR05EtcSelected, newValue)
  },
});

export const labNutritionSUR03ListSelector = selector({
  key: 'labNutritionSUR03ListSelector',
  get: ({get}) => {
    return get(labNutritionSUR03List)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR03List, newValue)
  },
});

export const labNutritionSUR04ListSelector = selector({
  key: 'labNutritionSUR04ListSelector',
  get: ({get}) => {
    return get(labNutritionSUR04List)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR04List, newValue)
  },
});

export const labNutritionSUR05ListSelector = selector({
  key: 'labNutritionSUR05ListSelector',
  get: ({get}) => {
    return get(labNutritionSUR05List)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR05List, newValue)
  },
});

export const labBabyFoodReasonListSelector = selector({
  key: 'labBabyFoodReasonListSelector',
  get: ({get}) => {
    return get(labBabyFoodReasonList)
  },
  set: ({set}, newValue) => {
    set(labBabyFoodReasonList, newValue)
  },
});

export const labNutritionSUR03ReasonListSelector = selector({
  key: 'labNutritionSUR03ReasonListSelector',
  get: ({get}) => {
    return get(labNutritionSUR03ReasonList)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR03ReasonList, newValue)
  },
});

export const labNutritionSUR04ReasonListSelector = selector({
  key: 'labNutritionSUR04ReasonListSelector',
  get: ({get}) => {
    return get(labNutritionSUR04ReasonList)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR04ReasonList, newValue)
  },
});

export const labNutritionSUR05ReasonListSelector = selector({
  key: 'labNutritionSUR05ReasonListSelector',
  get: ({get}) => {
    return get(labNutritionSUR05ReasonList)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR05ReasonList, newValue)
  },
});

export const labBabyFoodSelectBrandCdSelector = selector({
  key: 'labBabyFoodSelectBrandCdSelector',
  get: ({get}) => {
    return get(labBabyFoodSelectBrandCd)
  },
  set: ({set}, newValue) => {
    set(labBabyFoodSelectBrandCd, newValue)
  },
});

export const labNutritionSUR03SelectBrandCdSelector = selector({
  key: 'labNutritionSUR03SelectBrandCdSelector',
  get: ({get}) => {
    return get(labNutritionSUR03SelectBrandCd)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR03SelectBrandCd, newValue)
  },
});

export const labNutritionSUR04SelectBrandCdSelector = selector({
  key: 'labNutritionSUR04SelectBrandCdSelector',
  get: ({get}) => {
    return get(labNutritionSUR04SelectBrandCd)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR04SelectBrandCd, newValue)
  },
});

export const labNutritionSUR05SelectBrandCdSelector = selector({
  key: 'labNutritionSUR05SelectBrandCdSelector',
  get: ({get}) => {
    return get(labNutritionSUR05SelectBrandCd)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR05SelectBrandCd, newValue)
  },
});

export const labBabyFoodSelectBrandNameSelector = selector({
  key: 'labBabyFoodSelectBrandNameSelector',
  get: ({get}) => {
    return get(labBabyFoodSelectBrandName)
  },
  set: ({set}, newValue) => {
    set(labBabyFoodSelectBrandName, newValue)
  },
});

export const labNutritionSUR03SelectBrandNameSelector = selector({
  key: 'labNutritionSUR03SelectBrandNameSelector',
  get: ({get}) => {
    return get(labNutritionSUR03SelectBrandName)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR03SelectBrandName, newValue)
  },
});

export const labNutritionSUR04SelectBrandNameSelector = selector({
  key: 'labNutritionSUR04SelectBrandNameSelector',
  get: ({get}) => {
    return get(labNutritionSUR04SelectBrandName)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR04SelectBrandName, newValue)
  },
});

export const labNutritionSUR05SelectBrandNameSelector = selector({
  key: 'labNutritionSUR05SelectBrandNameSelector',
  get: ({get}) => {
    return get(labNutritionSUR05SelectBrandName)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR05SelectBrandName, newValue)
  },
});

export const labBabyFoodSelectReasonListSelector = selector({
  key: 'labBabyFoodSelectReasonListSelector',
  get: ({get}) => {
    return get(labBabyFoodSelectReasonList)
  },
  set: ({set}, newValue) => {
    set(labBabyFoodSelectReasonList, newValue)
  },
});

export const labNutritionSUR03SelectReasonListSelector = selector({
  key: 'labNutritionSUR03SelectReasonListSelector',
  get: ({get}) => {
    return get(labNutritionSUR03SelectReasonList)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR03SelectReasonList, newValue)
  },
});

export const labNutritionSUR04SelectReasonListSelector = selector({
  key: 'labNutritionSUR04SelectReasonListSelector',
  get: ({get}) => {
    return get(labNutritionSUR04SelectReasonList)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR04SelectReasonList, newValue)
  },
});

export const labNutritionSUR05SelectReasonListSelector = selector({
  key: 'labNutritionSUR05SelectReasonListSelector',
  get: ({get}) => {
    return get(labNutritionSUR05SelectReasonList)
  },
  set: ({set}, newValue) => {
    set(labNutritionSUR05SelectReasonList, newValue)
  },
});

export const labCommentShowCommentViewSelector = selector({
  key: 'labCommentShowCommentViewSelector',
  get: ({get}) => {
    return get(labCommentShowCommentView)
  },
  set: ({set}, newValue) => {
    set(labCommentShowCommentView, newValue)
  },
});

export const labCommentShowCommentModifySelector = selector({
  key: 'labCommentShowCommentModifySelector',
  get: ({get}) => {
    return get(labCommentShowCommentModify)
  },
  set: ({set}, newValue) => {
    set(labCommentShowCommentModify, newValue)
  },
});

export const labRecommendListSelector = selector({
  key: 'labRecommendListSelector',
  get: ({get}) => {
    return get(labRecommendList)
  },
  set: ({set}, newValue) => {
    set(labRecommendList, newValue)
  },
});

export const labIsItemSelectedSelector = selector({
  key: 'labIsItemSelectedSelector',
  get: ({get}) => {
    return get(labIsItemSelected)
  },
  set: ({set}, newValue) => {
    set(labIsItemSelected, newValue)
  },
});

export const labScoreSelector = selector({
  key: 'labScoreSelector',
  get: ({get}) => {
    return get(labScore)
  },
  set: ({set}, newValue) => {
    set(labScore, newValue)
  },
});

export const labCommentItemSelector = selector({
  key: 'labCommentItemSelector',
  get: ({get}) => {
    return get(labCommentItem)
  },
  set: ({set}, newValue) => {
    set(labCommentItem, newValue)
  },
});

export const labCommentFetchSelector = selector({
  key: 'labCommentFetchSelector',
  get: ({get}) => {
    return get(labCommentFetch)
  },
  set: ({set}, newValue) => {
    set(labCommentFetch, newValue)
  },
});

export const labCommentCheckSelector = selector({
  key: 'labCommentCheckSelector',
  get: ({get}) => {
    return get(labCommentCheck)
  },
  set: ({set}, newValue) => {
    set(labCommentCheck, newValue)
  },
});

export const labCommentOrderSelector = selector({
  key: 'labCommentOrderSelector',
  get: ({get}) => {
    return get(labCommentOrder)
  },
  set: ({set}, newValue) => {
    set(labCommentOrder, newValue)
  },
});

export const labCommentPostKeySelector = selector({
  key: 'labCommentPostKeySelector',
  get: ({get}) => {
    return get(labCommentPostKey)
  },
  set: ({set}, newValue) => {
    set(labCommentPostKey, newValue)
  },
});

export const labCommentCommentKeySelector = selector({
  key: 'labCommentCommentKeySelector',
  get: ({get}) => {
    return get(labCommentCommentKey)
  },
  set: ({set}, newValue) => {
    set(labCommentCommentKey, newValue)
  },
});