import {DefaultValue, selector} from "recoil";
import {SIGN_UP_INFORM_ID, SIGN_UP_INFORM_SNS} from "../../../constant/sign-up/SignUpInform";
import {
  authAllergyState,
  authBirthdayState,
  authChildAllergyState,
  authChildBirthState,
  authChildGenderState,
  authChildHeightState,
  authChildKeywordState,
  authChildNicknameState,
  authChildWeightState,
  authCiState,
  authConfirmPasswordState,
  authEmailAgreedState,
  authEmailState,
  authFindIdAnswerState,
  authFindIdQuestionState,
  authGenderState,
  authIdentificationQAState,
  authIdState,
  authIsPhoneIdentityVerificationUsageAgreedState,
  authKeywordState,
  authMarketingPushState,
  authNameState,
  authNicknameState,
  authPasswordState,
  authPersonalMarketingAgreedState,
  authPhoneNumberState,
  authResetPasswordAnswer1State,
  authResetPasswordAnswer2State,
  authResetPasswordQuestion1State,
  authResetPasswordQuestion2State,
  authSmsAgreedState,
  authUuidState,
  signUpType
} from "../../atom/sign-up/auth";
import {ADD_CHILDREN} from "../../../constant/my/AddChildren";

export const signUpIdSelector = selector({
  key: 'signUpIdSelector',
  get: ({get}) => {
    const inform = {...SIGN_UP_INFORM_ID}
    inform.isPhoneIdentityVerificationUsageAgreed = get(authIsPhoneIdentityVerificationUsageAgreedSelector)
    inform.uuid = get(authUuidState)
    inform.ci = get(authCiState)
    inform.name = get(authNameState)
    inform.phoneNumber = get(authPhoneNumberState)
    inform.birthDay = get(authBirthdayState)
    inform.gender = get(authGenderState)
    inform.id = get(authIdState)
    inform.password = get(authPasswordState)
    inform.email = get(authEmailState)
    inform.nickname = get(authNicknameState)
    inform.identityVerificationQas = get(authIdentificationQASelector)
    inform.babies = [get(addChildrenSelector)]
    inform.isTermsCollectingPersonalDataMarketingAgreed = get(authPersonalMarketingAgreedState)
    inform.isTextMessageReciveAgreed = get(authSmsAgreedState)
    inform.isEmailReceiveAgreed = get(authEmailAgreedState)
    inform.isMarketingInfoPushNotifSet = get(authMarketingPushState)
    return inform
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(authIsPhoneIdentityVerificationUsageAgreedSelector)
      reset(authUuidState)
      reset(authCiState)
      reset(authNameState)
      reset(authPhoneNumberState)
      reset(authBirthdayState)
      reset(authGenderState)
      reset(authIdState)
      reset(authPasswordState)
      reset(authEmailState)
      reset(authNicknameState)
      reset(authIdentificationQASelector)
      reset(addChildrenSelector)
      reset(authPersonalMarketingAgreedState)
      reset(authSmsAgreedState)
      reset(authEmailAgreedState)
      reset(authMarketingPushState)
    } else {
      set(authIsPhoneIdentityVerificationUsageAgreedSelector, newValue.isPhoneIdentityVerificationUsageAgreed)
      set(authUuidState, newValue.uuid)
      set(authCiState, newValue.ci)
      set(authNameState, newValue.name)
      set(authPhoneNumberState, newValue.phoneNumber)
      set(authBirthdayState, newValue.birthDay)
      set(authGenderState, newValue.gender)
      set(authIdState, newValue.id)
      set(authPasswordState, newValue.password)
      set(authEmailState, newValue.email)
      set(authNicknameState, newValue.nickname)
      set(authIdentificationQASelector, newValue.identityVerificationQas)
      set(addChildrenSelector, newValue.babies)
      set(authPersonalMarketingAgreedState, newValue.isTermsCollectingPersonalDataMarketingAgreed)
      set(authSmsAgreedState, newValue.isTextMessageReciveAgreed)
      set(authEmailAgreedState, newValue.isEmailReceiveAgreed)
      set(authMarketingPushState, newValue.isMarketingInfoPushNotifSet)
    }
  },
})

export const signUpSnsSelector = selector({
  key: 'signUpSnsSelector',
  get: ({get}) => {
    const inform = {...SIGN_UP_INFORM_SNS}
    inform.uuid = get(authUuidState)
    inform.ci = get(authCiState)
    inform.name = get(authNameState)
    inform.phoneNumber = get(authPhoneNumberState)
    inform.birthDay = get(authBirthdayState)
    inform.gender = get(authGenderState)
    inform.email = get(authEmailState)
    inform.nickname = get(authNicknameState)
    inform.babies = [get(addChildrenSelector)]
    inform.isTermsCollectingPersonalDataMarketingAgreed = get(authPersonalMarketingAgreedState)
    inform.isTextMessageReciveAgreed = get(authSmsAgreedState)
    inform.isEmailReceiveAgreed = get(authEmailAgreedState)
    inform.isMarketingInfoPushNotifSet = get(authMarketingPushState)
    return inform
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(authUuidState)
      reset(authCiState)
      reset(authNameState)
      reset(authPhoneNumberState)
      reset(authBirthdayState)
      reset(authGenderState)
      reset(authEmailState)
      reset(authNicknameState)
      reset(addChildrenSelector)
      reset(authPersonalMarketingAgreedState)
      reset(authSmsAgreedState)
      reset(authEmailAgreedState)
      reset(authMarketingPushState)
    } else {
      set(authUuidState, newValue.uuid)
      set(authCiState, newValue.ci)
      set(authNameState, newValue.name)
      set(authPhoneNumberState, newValue.phoneNumber)
      set(authBirthdayState, newValue.birthDay)
      set(authGenderState, newValue.gender)
      set(authEmailState, newValue.email)
      set(authNicknameState, newValue.nickname)
      set(addChildrenSelector, newValue.babies)
      set(authPersonalMarketingAgreedState, newValue.isTermsCollectingPersonalDataMarketingAgreed)
      set(authSmsAgreedState, newValue.isTextMessageReciveAgreed)
      set(authEmailAgreedState, newValue.isEmailReceiveAgreed)
      set(authMarketingPushState, newValue.isMarketingInfoPushNotifSet)
    }
  },
})

export const addChildrenSelector = selector({
  key: 'addChildrenSelector',
  get: ({get}) => {
    const children = {...ADD_CHILDREN}
    children.nickname = get(authChildNicknameState)
    children.birthdate = get(authChildBirthState)
    children.height = get(authChildHeightState)
    children.weight = get(authChildWeightState)
    children.gender = get(authChildGenderState)
    children.allergyIngredients = get(authChildAllergyState)
    children.concerns = get(authChildKeywordState)
    return children
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(authChildNicknameState)
      reset(authChildBirthState)
      reset(authChildHeightState)
      reset(authChildWeightState)
      reset(authChildGenderState)
      reset(authChildAllergyState)
      reset(authChildKeywordState)
    } else {
      set(authChildNicknameState, newValue.nickname)
      set(authChildBirthState, newValue.birthdate)
      set(authChildHeightState, newValue.height)
      set(authChildWeightState, newValue.weight)
      set(authChildGenderState, newValue.gender)
      set(authChildAllergyState, newValue.allergyIngredients)
      set(authChildKeywordState, newValue.concerns)
    }
  },
})

export const authIsPhoneIdentityVerificationUsageAgreedSelector = selector({
  key: 'authIsPhoneIdentityVerificationUsageAgreedSelector',
  get: ({get}) => {
    return get(authIsPhoneIdentityVerificationUsageAgreedState)
  },
  set: ({set}, newValue) => {
    set(authIsPhoneIdentityVerificationUsageAgreedState, newValue)
  },
})

export const authUuidSelector = selector({
  key: 'authUuidSelector',
  get: ({get}) => {
    return get(authUuidState)
  },
  set: ({set}, newValue) => {
    set(authUuidState, newValue)
  },
})

export const authCiSelector = selector({
  key: 'authCiSelector',
  get: ({get}) => {
    return get(authCiState)
  },
  set: ({set}, newValue) => {
    set(authCiState, newValue)
  },
})

export const authGenderSelector = selector({
  key: 'authGenderSelector',
  get: ({get}) => {
    return get(authGenderState)
  },
  set: ({set}, newValue) => {
    set(authGenderState, newValue)
  },
})

export const authIdSelector = selector({
  key: 'authIdSelector',
  get: ({get}) => {
    return get(authIdState)
  },
  set: ({set}, newValue) => {
    set(authIdState, newValue)
  },
})

export const authPasswordSelector = selector({
  key: 'authPasswordSelector',
  get: ({get}) => {
    return get(authPasswordState)
  },
  set: ({set}, newValue) => {
    set(authPasswordState, newValue)
  },
})

export const authConfirmPasswordSelector = selector({
  key: 'authConfirmPasswordSelector',
  get: ({get}) => {
    return get(authConfirmPasswordState)
  },
  set: ({set}, newValue) => {
    set(authConfirmPasswordState, newValue)
  },
})

export const authEmailSelector = selector({
  key: 'authEmailSelector',
  get: ({get}) => {
    return get(authEmailState)
  },
  set: ({set}, newValue) => {
    set(authEmailState, newValue)
  },
})

export const authNicknameSelector = selector({
  key: 'authNicknameSelector',
  get: ({get}) => {
    return get(authNicknameState)
  },
  set: ({set}, newValue) => {
    set(authNicknameState, newValue)
  },
})

export const authIdentificationQASelector = selector({
  key: 'authIdentificationQASelector',
  get: ({get}) => {
    return get(authIdentificationQAState)
  },
  set: ({set}, newValue) => {
    set(authIdentificationQAState, newValue)
  },
})

export const authChildNicknameSelector = selector({
  key: 'authChildNicknameSelector',
  get: ({get}) => {
    return get(authChildNicknameState)
  },
  set: ({set}, newValue) => {
    set(authChildNicknameState, newValue)
  },
})

export const authChildBirthSelector = selector({
  key: 'authChildBirthSelector',
  get: ({get}) => {
    return get(authChildBirthState)
  },
  set: ({set}, newValue) => {
    set(authChildBirthState, newValue)
  },
})

export const authChildHeightSelector = selector({
  key: 'authChildHeightSelector',
  get: ({get}) => {
    return get(authChildHeightState)
  },
  set: ({set}, newValue) => {
    set(authChildHeightState, newValue)
  },
})

export const authChildWeightSelector = selector({
  key: 'authChildWeightSelector',
  get: ({get}) => {
    return get(authChildWeightState)
  },
  set: ({set}, newValue) => {
    set(authChildWeightState, newValue)
  },
})

export const authChildGenderSelector = selector({
  key: 'authChildGenderSelector',
  get: ({get}) => {
    return get(authChildGenderState)
  },
  set: ({set}, newValue) => {
    set(authChildGenderState, newValue)
  },
})

export const authAllergySelector = selector({
  key: 'authAllergySelector',
  get: ({get}) => {
    return get(authAllergyState)
  },
  set: ({set}, newValue) => {
    set(authAllergyState, newValue)
  },
})

export const authChildAllergySelector = selector({
  key: 'authChildAllergySelector',
  get: ({get}) => {
    return get(authChildAllergyState)
  },
  set: ({set}, newValue) => {
    set(authChildAllergyState, newValue)
  },
})

export const authKeywordSelector = selector({
  key: 'authKeywordSelector',
  get: ({get}) => {
    return get(authKeywordState)
  },
  set: ({set}, newValue) => {
    set(authKeywordState, newValue)
  },
})

export const authChildKeywordSelector = selector({
  key: 'authChildKeywordSelector',
  get: ({get}) => {
    return get(authChildKeywordState)
  },
  set: ({set}, newValue) => {
    set(authChildKeywordState, newValue)
  },
})

export const authPersonalMarketingAgreedSelector = selector({
  key: 'authPersonalMarketingAgreedSelector',
  get: ({get}) => {
    return get(authPersonalMarketingAgreedState)
  },
  set: ({set}, newValue) => {
    set(authPersonalMarketingAgreedState, newValue)
  },
})

export const authSmsAgreedSelector = selector({
  key: 'authSmsAgreedSelector',
  get: ({get}) => {
    return get(authSmsAgreedState)
  },
  set: ({set}, newValue) => {
    set(authSmsAgreedState, newValue)
  },
})

export const authEmailAgreedSelector = selector({
  key: 'authEmailAgreedSelector',
  get: ({get}) => {
    return get(authEmailAgreedState)
  },
  set: ({set}, newValue) => {
    set(authEmailAgreedState, newValue)
  },
})

export const authMarketingPushSelector = selector({
  key: 'authMarketingPushSelector',
  get: ({get}) => {
    return get(authMarketingPushState)
  },
  set: ({set}, newValue) => {
    set(authMarketingPushState, newValue)
  },
})

export const signUpTypeSelector = selector({
  key: 'signUpTypeSelector',
  get: ({get}) => {
    return get(signUpType)
  },
  set: ({set}, newValue) => {
    set(signUpType, newValue)
  },
})

export const authFindIdQuestionSelector = selector({
  key: 'authFindIdQuestionSelector',
  get: ({get}) => {
    return get(authFindIdQuestionState)
  },
  set: ({set}, newValue) => {
    set(authFindIdQuestionState, newValue)
  },
})
export const authResetPasswordQuestion1Selector = selector({
  key: 'authResetPasswordQuestion1Selector',
  get: ({get}) => {
    return get(authResetPasswordQuestion1State)
  },
  set: ({set}, newValue) => {
    set(authResetPasswordQuestion1State, newValue)
  },
})
export const authResetPasswordQuestion2Selector = selector({
  key: 'authResetPasswordQuestion2Selector',
  get: ({get}) => {
    return get(authResetPasswordQuestion2State)
  },
  set: ({set}, newValue) => {
    set(authResetPasswordQuestion2State, newValue)
  },
})
export const authFindIdAnswerSelector = selector({
  key: 'authFindIdAnswerSelector',
  get: ({get}) => {
    return get(authFindIdAnswerState)
  },
  set: ({set}, newValue) => {
    set(authFindIdAnswerState, newValue)
  },
})
export const authResetPasswordAnswer1Selector = selector({
  key: 'authResetPasswordAnswer1Selector',
  get: ({get}) => {
    return get(authResetPasswordAnswer1State)
  },
  set: ({set}, newValue) => {
    set(authResetPasswordAnswer1State, newValue)
  },
})
export const authResetPasswordAnswer2Selector = selector({
  key: 'authResetPasswordAnswer2Selector',
  get: ({get}) => {
    return get(authResetPasswordAnswer2State)
  },
  set: ({set}, newValue) => {
    set(authResetPasswordAnswer2State, newValue)
  },
})