import {DefaultValue, selector} from "recoil";
import {AUTH_INFORM} from "../../../constant/sign-up/SignUpInform";
import {
  authAccountKeyState,
  authIdEndNumberState,
  authIdFrontNumberState,
  authMemberKeyState,
  authNameState,
  authPhoneNumberState,
  authTelecomState
} from "../../atom/sign-up/auth";

export const authSelector = selector({
  key: 'authSelector',
  get: ({get}) => {
    const inform = {...AUTH_INFORM}
    inform.name = get(authNameState)
    inform.telecom = get(authTelecomState)
    inform.phone = get(authPhoneNumberState)
    inform.idFront = get(authIdFrontNumberState)
    inform.idEnd = get(authIdEndNumberState)
    return inform
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(authNameState)
      reset(authTelecomState)
      reset(authPhoneNumberState)
      reset(authIdFrontNumberState)
      reset(authIdEndNumberState)
    } else {
      set(authNameState, newValue.name)
      set(authTelecomState, newValue.telecom)
      set(authPhoneNumberState, newValue.phone)
      set(authIdFrontNumberState, newValue.idFront)
      set(authIdEndNumberState, newValue.idEnd)
    }
  },
})

export const authAccountKeySelector = selector({
  key: 'authAccessKeySelector',
  get: ({get}) => {
    return get(authAccountKeyState)
  },
  set: ({set}, newValue) => {
    set(authAccountKeyState, newValue)
  },
})

export const authMemberKeySelector = selector({
  key: 'authMemberKeySelector',
  get: ({get}) => {
    return get(authMemberKeyState)
  },
  set: ({set}, newValue) => {
    set(authMemberKeyState, newValue)
  },
})

export const authNameSelector = selector({
  key: 'authNameSelector',
  get: ({get}) => {
    return get(authNameState)
  },
  set: ({set}, newValue) => {
    set(authNameState, newValue)
  },
})

export const authTelecomSelector = selector({
  key: 'authTelecomSelector',
  get: ({get}) => {
    return get(authTelecomState)
  },
  set: ({set}, newValue) => {
    set(authTelecomState, newValue)
  },
})

export const authPhoneSelector = selector({
  key: 'authPhoneSelector',
  get: ({get}) => {
    return get(authPhoneNumberState)
  },
  set: ({set}, newValue) => {
    set(authPhoneNumberState, newValue)
  },
})

export const authIdFrontSelector = selector({
  key: 'authIdFrontSelector',
  get: ({get}) => {
    return get(authIdFrontNumberState)
  },
  set: ({set}, newValue) => {
    set(authIdFrontNumberState, newValue)
  },
})

export const authIdEndSelector = selector({
  key: 'authIdEndSelector',
  get: ({get}) => {
    return get(authIdEndNumberState)
  },
  set: ({set}, newValue) => {
    set(authIdEndNumberState, newValue)
  },
})
