import {selector} from "recoil";
import {dynamicIslandExpand} from "../../atom/home/dynamicIsland";

export const dynamicIslandExpandSelector = selector({
  key: 'dynamicIslandExpandSelector',
  get: ({get}) => {
    return get(dynamicIslandExpand)
  },
  set: ({set}, newValue) => {
    set(dynamicIslandExpand, newValue)
  },
});