import {selector} from "recoil";
import {layerPopupAtom, layerPopupClicked, layerPopupInfiniteBool, layerPopupSlideIndex, showLayerPopup} from "../../atom/home/layerPopup";

export const layerPopupSelector = selector({
  key: 'layerPopupSelector',
  get: ({get}) => {
    return get(layerPopupAtom)
  },
  set: ({set}, newValue) => {
    set(layerPopupAtom, newValue)
  },
});

export const showLayerPopupSelector = selector({
  key: 'showLayerPopupSelector',
  get: ({get}) => {
    return get(showLayerPopup)
  },
  set: ({set}, newValue) => {
    set(showLayerPopup, newValue)
  },
});

export const layerPopupClickedSelector = selector({
  key: 'layerPopupClickedSelector',
  get: ({get}) => {
    return get(layerPopupClicked)
  },
  set: ({set}, newValue) => {
    set(layerPopupClicked, newValue)
  },
});

export const layerPopupSlideIndexSelector = selector({
  key: 'layerPopupSlideIndexSelector',
  get: ({get}) => {
    return get(layerPopupSlideIndex)
  },
  set: ({set}, newValue) => {
    set(layerPopupSlideIndex, newValue)
  },
});

export const layerPopupInfiniteBoolSelector = selector({
  key: 'layerPopupInfiniteBoolSelector',
  get: ({get}) => {
    return get(layerPopupInfiniteBool)
  },
  set: ({set}, newValue) => {
    set(layerPopupInfiniteBool, newValue)
  },
});