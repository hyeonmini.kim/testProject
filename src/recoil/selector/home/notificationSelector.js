import {selector} from "recoil";
import {notificationHasNewItem, notificationOpen} from "../../atom/home/notification";

export const notificationOpenSelector = selector({
  key: 'notificationOpenSelector',
  get: ({get}) => {
    return get(notificationOpen)
  },
  set: ({set}, newValue) => {
    set(notificationOpen, newValue)
  },
});

export const notificationHasNewItemSelector = selector({
  key: 'notificationHasNewItemSelector',
  get: ({get}) => {
    return get(notificationHasNewItem)
  },
  set: ({set}, newValue) => {
    set(notificationHasNewItem, newValue)
  },
});