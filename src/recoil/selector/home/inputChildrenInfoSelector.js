import {DefaultValue, selector} from "recoil";
import {
  inputChildrenAllergy,
  inputChildrenAllergyList,
  inputChildrenBirth,
  inputChildrenGender,
  inputChildrenHeight,
  inputChildrenKeyword,
  inputChildrenKeywordList,
  inputChildrenWeight
} from "../../atom/home/inputChildrenInfo";
import {INPUT_CHILD_INFO} from "../../../constant/sign-up/SignUpInform";

export const inputChildInfoSelector = selector({
  key: 'inputChildInfoSelector',
  get: ({get}) => {
    const inform = {...INPUT_CHILD_INFO}
    inform.birth = get(inputChildrenBirth)
    inform.height = get(inputChildrenHeight)
    inform.weight = get(inputChildrenWeight)
    inform.gender = get(inputChildrenGender)
    inform.allergy = get(inputChildrenAllergy)
    inform.allergyList = get(inputChildrenAllergyList)
    inform.keyword = get(inputChildrenKeyword)
    inform.keywordList = get(inputChildrenKeywordList)
    return inform
  },
  set: ({set, reset}, newValue) => {
    if (newValue instanceof DefaultValue) {
      reset(inputChildrenBirth)
      reset(inputChildrenHeight)
      reset(inputChildrenWeight)
      reset(inputChildrenGender)
      reset(inputChildrenAllergy)
      reset(inputChildrenAllergyList)
      reset(inputChildrenKeyword)
      reset(inputChildrenKeywordList)
    } else {
      set(inputChildrenBirth, newValue.birth)
      set(inputChildrenHeight, newValue.height)
      set(inputChildrenWeight, newValue.weight)
      set(inputChildrenGender, newValue.gender)
      set(inputChildrenAllergy, newValue.allergy)
      set(inputChildrenAllergyList, newValue.allergyList)
      set(inputChildrenKeyword, newValue.keyword)
      set(inputChildrenKeywordList, newValue.keywordList)
    }
  },
})

export const inputChildrenBirthSelector = selector({
  key: 'inputChildrenBirthSelector',
  get: ({get}) => {
    return get(inputChildrenBirth)
  },
  set: ({set}, newValue) => {
    set(inputChildrenBirth, newValue)
  },
})

export const inputChildrenHeightSelector = selector({
  key: 'inputChildrenHeightSelector',
  get: ({get}) => {
    return get(inputChildrenHeight)
  },
  set: ({set}, newValue) => {
    set(inputChildrenHeight, newValue)
  },
})

export const inputChildrenWeightSelector = selector({
  key: 'inputChildrenWeightSelector',
  get: ({get}) => {
    return get(inputChildrenWeight)
  },
  set: ({set}, newValue) => {
    set(inputChildrenWeight, newValue)
  },
})

export const inputChildrenGenderSelector = selector({
  key: 'inputChildrenGenderSelector',
  get: ({get}) => {
    return get(inputChildrenGender)
  },
  set: ({set}, newValue) => {
    set(inputChildrenGender, newValue)
  },
})

export const inputChildrenAllergySelector = selector({
  key: 'inputChildrenAllergySelector',
  get: ({get}) => {
    return get(inputChildrenAllergy)
  },
  set: ({set}, newValue) => {
    set(inputChildrenAllergy, newValue)
  },
})

export const inputChildrenAllergyListSelector = selector({
  key: 'inputChildrenAllergyListSelector',
  get: ({get}) => {
    return get(inputChildrenAllergyList)
  },
  set: ({set}, newValue) => {
    set(inputChildrenAllergyList, newValue)
  },
})

export const inputChildrenKeywordSelector = selector({
  key: 'inputChildrenKeywordSelector',
  get: ({get}) => {
    return get(inputChildrenKeyword)
  },
  set: ({set}, newValue) => {
    set(inputChildrenKeyword, newValue)
  },
})

export const inputChildrenKeywordListSelector = selector({
  key: 'inputChildrenKeywordListSelector',
  get: ({get}) => {
    return get(inputChildrenKeywordList)
  },
  set: ({set}, newValue) => {
    set(inputChildrenKeywordList, newValue)
  },
})
