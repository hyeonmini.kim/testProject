import {selector} from "recoil";
import {homeShowInfoBanner, isOpenTooltip} from "../../atom/home/home";

export const homeShowInfoBannerSelector = selector({
  key: 'homeShowInfoBannerSelector',
  get: ({get}) => {
    return get(homeShowInfoBanner)
  },
  set: ({set}, newValue) => {
    set(homeShowInfoBanner, newValue)
  },
});

export const isOpenTooltipSelector = selector({
  key: 'isOpenTooltipSelector',
  get: ({get}) => {
    return get(isOpenTooltip)
  },
  set: ({set}, newValue) => {
    set(isOpenTooltip, newValue)
  },
});