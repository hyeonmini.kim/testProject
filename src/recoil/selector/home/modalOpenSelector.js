import {selector} from "recoil";
import {homeModalOpen} from "../../atom/home/modalOpen";

export const homeModalSelector = selector({
  key: 'homeModalSelector',
  get: ({get}) => {
    return get(homeModalOpen)
  },
  set: ({set}, newValue) => {
    set(homeModalOpen, newValue)
  },
});