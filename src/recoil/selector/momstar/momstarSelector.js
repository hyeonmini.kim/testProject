import {selector} from "recoil";
import {momStarActiveTab, momStarIsScroll} from "../../atom/momstar/momstar";

export const momStarActiveTabSelector = selector({
  key: 'momStarActiveTabSelector',
  get: ({get}) => {
    return get(momStarActiveTab)
  },
  set: ({set}, newValue) => {
    set(momStarActiveTab, newValue)
  },
});

export const momStarIsScrollSelector = selector({
  key: 'momStarIsScrollSelector',
  get: ({get}) => {
    return get(momStarIsScroll)
  },
  set: ({set}, newValue) => {
    set(momStarIsScroll, newValue)
  },
});