import {selector} from "recoil";
import {guestInfo} from "../../atom/guest/guestInfo";

export const guestInfoSelector = selector({
  key: 'guestInfoSelector',
  get: ({get}) => {
    return get(guestInfo)
  },
  set: ({set, get}, newValue) => {
    set(guestInfo, {
      ...get(guestInfo),
      ...newValue
    })
  },
});