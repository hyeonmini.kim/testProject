import {selector} from "recoil";
import {guestModalOpen} from "../../atom/guest/guestModalOpen";

export const guestModalSelector = selector({
  key: 'guestModalSelector',
  get: ({get}) => {
    return get(guestModalOpen)
  },
  set: ({set, get}, newValue) => {
    set(guestModalOpen, {
      ...get(guestModalOpen),
      ...newValue
    })
  },
});