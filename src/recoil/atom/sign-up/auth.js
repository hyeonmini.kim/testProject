import {atom} from "recoil";
import {ALLERGY_LIST, HEALTH_KEYWORD, SIGN_UP_EMAIL} from "../../../constant/sign-up/SignUp";

const authBottomButtonPositionState = atom({
  key: 'authBottomButtonPosition',
  default: 'fixed',
});
export {authBottomButtonPositionState};
const authBottomButtonMarginState = atom({
  key: 'authBottomButtonMargin',
  default: '20px',
});
export {authBottomButtonMarginState};

// bottom button disabled state
const authBottomButtonState = atom({
  key: 'authBottomButton',
  default: true,
});
export {authBottomButtonState};

// bottom button text value
const authBottomButtonTextState = atom({
  key: 'authBottomButtonText',
  default: '확인',
});
export {authBottomButtonTextState};

// bottom button click flag
const authBottomButtonClickState = atom({
  key: 'authBottomButtonClick',
  default: false,
});
export {authBottomButtonClickState};

// user name value
const authNameState = atom({
  key: 'authName',
  default: '',
})
export {authNameState};

// user telecom value
const authTelecomState = atom({
  key: 'authTelecom',
  default: '',
});
export {authTelecomState};

// user phone number value
const authPhoneNumberState = atom({
  key: 'authPhoneNumber',
  default: '',
});
export {authPhoneNumberState};

// user id front number value
const authIdFrontNumberState = atom({
  key: 'authIdFrontNumber',
  default: '',
});
export {authIdFrontNumberState};

// user id end number value
const authIdEndNumberState = atom({
  key: 'authIdEndNumber',
  default: '',
});
export {authIdEndNumberState};

// input ref index
const authInputRefIndexState = atom({
  key: 'authInputRef',
  default: -1,
});
export {authInputRefIndexState};

// open snackbar flag
const authOpenSnackbarState = atom({
  key: 'authOpenSnackbar',
  default: false,
});
export {authOpenSnackbarState};

// snackbar margin bottom value
const authSnackbarMbState = atom({
  key: 'authSnackbarMb',
  default: '84px',
});
export {authSnackbarMbState};

// snackbar message value
const authSnackbarMessageState = atom({
  key: 'authSnackbarMessage',
  default: '',
});
export {authSnackbarMessageState};

// snackbar background color value
const authSnackbarBgColorState = atom({
  key: 'authSnackbarBgColor',
  default: '#FFFFFF',
});
export {authSnackbarBgColorState};

/// --------------------------------------------------
/// 로그인 화면 state
/// --------------------------------------------------

// login id
const authLoginIdState = atom({
  key: 'loginIdState',
  default: '',
})
const authAccountKeyState = atom({
  key: 'accountKeyState',
  default: '',
})
const authLoginErrorMessageState = atom({
  key: 'loginErrorMessageState',
  default: '',
})
// member key
const authMemberKeyState = atom({
  key: 'memberKeyState',
  default: '',
})

export {
  authLoginIdState,
  authAccountKeyState,
  authLoginErrorMessageState,
  authMemberKeyState
}

/// --------------------------------------------------
/// 회원 정보 입력 화면 state
/// --------------------------------------------------

// 마케팅 등을 위한 개인정보 수집, 이용 및 제공 동의 - 문자 or 이메일
const authPersonalMarketingAgreedState = atom({
  key: 'authPersonalMarketingAgreed',
  default: true,
})
// 마케팅 등을 위한 개인정보 수집, 이용 및 제공 동의 - 문자
const authSmsAgreedState = atom({
  key: 'authSmsAgreed',
  default: true,
})
// 마케팅 등을 위한 개인정보 수집, 이용 및 제공 동의 - 이메일
const authEmailAgreedState = atom({
  key: 'authEmailAgreed',
  default: true,
})
// 마케팅 알림 수신 동의
const authMarketingPushState = atom({
  key: 'authMarketingPush',
  default: true,
})
// SNS 연동 계정이 있는데 ID 회원가입 하는 경우
const authSignUpIdExistedSnsState = atom({
  key: 'authSignUpIdExistedSnsState',
  default: false,
})

export {
  authPersonalMarketingAgreedState,
  authSmsAgreedState,
  authEmailAgreedState,
  authMarketingPushState,
  authSignUpIdExistedSnsState,
}


/// --------------------------------------------------
/// 회원 정보 입력 화면 state
/// --------------------------------------------------

// isPhoneIdentityVerificationUsageAgreed value
const authIsPhoneIdentityVerificationUsageAgreedState = atom({
  key: 'isPhoneIdentityVerificationUsageAgreed',
  default: false,
})
// uuid value
const authUuidState = atom({
  key: 'authUuid',
  default: '',
})
// ci value
const authCiState = atom({
  key: 'authCi',
  default: '',
})
// birthday value
const authBirthdayState = atom({
  key: 'authBirthday',
  default: '',
})
// gender value
const authGenderState = atom({
  key: 'authGender',
  default: '',
})
// id value
const authIdState = atom({
  key: 'authId',
  default: '',
});
// id helper text value
const authIdHelperTextState = atom({
  key: 'authIdHelperText',
  default: '',
})
// id error
const authIdErrorState = atom({
  key: 'authIdError',
  default: true,
})
// email value
const authEmailState = atom({
  key: 'authEmail',
  default: '',
});
const authEmailFindIdState = atom({
  key: 'authEmailFindId',
  default: '',
});
// email helper text value
const authEmailHelperTextState = atom({
  key: 'authEmailHelperText',
  default: SIGN_UP_EMAIL.HELPER_TEXT,
})
const authEmailFindIdHelperTextState = atom({
  key: 'authEmailFindIdHelperText',
  default: '',
})
const authFindIdAnswerHelperTextState = atom({
  key: 'authFindIdAnswerHelperText',
  default: '',
})
const authResetPasswordAnswer1HelperTextState = atom({
  key: 'authResetPasswordAnswer1HelperText',
  default: '',
})
const authResetPasswordAnswer2HelperTextState = atom({
  key: 'authResetPasswordAnswer2HelperText',
  default: '',
})
// email error
const authEmailErrorState = atom({
  key: 'authEmailError',
  default: true,
})
const authEmailFindIdErrorState = atom({
  key: 'authEmailFindIdError',
  default: true,
})
const authFindIdAnswerErrorState = atom({
  key: 'authFindIdAnswerError',
  default: true,
})
const authResetPasswordAnswer1ErrorState = atom({
  key: 'authResetPasswordAnswer1Error',
  default: true,
})
const authResetPasswordAnswer2ErrorState = atom({
  key: 'authResetPasswordAnswer2Error',
  default: true,
})
// password value
const authPasswordState = atom({
  key: 'authPassword',
  default: '',
});
// password helper text value
const authPasswordHelperTextState = atom({
  key: 'authPasswordHelperText',
  default: '',
})
// password error
const authPasswordErrorState = atom({
  key: 'authPasswordError',
  default: true,
})
// password confirm value
const authConfirmPasswordState = atom({
  key: 'authConfirmPassword',
  default: '',
});
// nickname value
const authNicknameState = atom({
  key: 'authNickname',
  default: '',
});
// nickname helper text value
const authNicknameHelperTextState = atom({
  key: 'authNicknameHelperText',
  default: '',
})
// nickname error
const authNicknameErrorState = atom({
  key: 'authNicknameError',
  default: true,
})
// identification question and answer
const authIdentificationQAState = atom({
  key: 'authIdentificationQA',
  default: [],
})
// child's nickname value
const authChildNicknameState = atom({
  key: 'authChildNickname',
  default: '',
});
// child's birth value
const authChildBirthState = atom({
  key: 'authChildBirth',
  default: '',
});
// child's height value
const authChildHeightState = atom({
  key: 'authChildHeight',
  default: '',
});
// child's weight value
const authChildWeightState = atom({
  key: 'authChildWeight',
  default: '',
});
// child's gender value
const authChildGenderState = atom({
  key: 'authChildGender',
  default: '',
});
// allergy value
const authAllergyState = atom({
  key: 'authAllergy',
  default: ALLERGY_LIST,
});
// child's allergy value
const authChildAllergyState = atom({
  key: 'authChildAllergy',
  default: undefined,
});
// keyword value
const authKeywordState = atom({
  key: 'authKeyword',
  default: HEALTH_KEYWORD,
});
// child's keyword value
const authChildKeywordState = atom({
  key: 'authChildKeyword',
  default: [],
});
const authFindIdQuestionState = atom({
  key: 'authFindIdQuestion',
  default: '',
});
const authResetPasswordQuestion1State = atom({
  key: 'authResetPasswordQuestion1',
  default: '',
});
const authResetPasswordQuestion2State = atom({
  key: 'authResetPasswordQuestion2',
  default: '',
});
const authFindIdAnswerState = atom({
  key: 'authFindIdAnswer',
  default: '',
});
const authResetPasswordAnswer1State = atom({
  key: 'authResetPasswordAnswer1',
  default: '',
});
const authResetPasswordAnswer2State = atom({
  key: 'authResetPasswordAnswer2',
  default: '',
});

export {
  authIsPhoneIdentityVerificationUsageAgreedState,
  authUuidState,
  authCiState,
  authBirthdayState,
  authGenderState,
  authIdState,
  authIdHelperTextState,
  authIdErrorState,
  authEmailState,
  authEmailFindIdState,
  authEmailHelperTextState,
  authEmailFindIdHelperTextState,
  authFindIdAnswerHelperTextState,
  authResetPasswordAnswer1HelperTextState,
  authResetPasswordAnswer2HelperTextState,
  authEmailErrorState,
  authEmailFindIdErrorState,
  authFindIdAnswerErrorState,
  authResetPasswordAnswer1ErrorState,
  authResetPasswordAnswer2ErrorState,
  authPasswordState,
  authPasswordHelperTextState,
  authPasswordErrorState,
  authConfirmPasswordState,
  authNicknameState,
  authNicknameHelperTextState,
  authNicknameErrorState,
  authIdentificationQAState,
  authChildNicknameState,
  authChildBirthState,
  authChildHeightState,
  authChildWeightState,
  authChildGenderState,
  authAllergyState,
  authChildAllergyState,
  authKeywordState,
  authChildKeywordState,
  authFindIdQuestionState,
  authResetPasswordQuestion1State,
  authResetPasswordQuestion2State,
  authFindIdAnswerState,
  authResetPasswordAnswer1State,
  authResetPasswordAnswer2State,
};

export const signUpType = atom({
  key: 'signUpType',
  default: '',
})

/// --------------------------------------------------
/// 본인 확인 질문 답 입력 화면
/// --------------------------------------------------

export const questionNumState = atom({
  key: 'questionNumState',
  default: '',
})

export const firstQuestionCodeState = atom({
  key: 'firstQuestionCodeState',
  default: '',
})

export const secondQuestionCodeState = atom({
  key: 'secondQuestionCodeState',
  default: '',
})

export const firstQuestionTextState = atom({
  key: 'firstQuestionTextState',
  default: '',
})

export const secondQuestionTextState = atom({
  key: 'secondQuestionTextState',
  default: '',
})

export const firstAnswerState = atom({
  key: 'firstAnswerState',
  default: '',
})

export const secondAnswerState = atom({
  key: 'secondAnswerState',
  default: '',
})
