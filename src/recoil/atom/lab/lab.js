import {atom} from "recoil";
import {
  BABYFOOD_ACTIVE_STEP,
  BABYFOOD_LIST,
  BABYFOOD_REASON_LIST, LAB_ORDER_TYPE,
  LAB_RECOMMEND_LIST,
  NUTRITION_ACTIVE_STEP,
  NUTRITION_SELECT_LIST,
  NUTRITION_SUR03_LIST,
  NUTRITION_SUR03_REASON_LIST,
  NUTRITION_SUR04_LIST,
  NUTRITION_SUR04_REASON_LIST,
  NUTRITION_SUR05_LIST,
  NUTRITION_SUR05_REASON_LIST,
  SURVEY_TYPE,
} from "../../../constant/lab/LabCommon";

export const labSelectedSurveyType = atom({
  key: 'labSelectedSurveyType',
  default: SURVEY_TYPE.SUR03
})

export const labBabyFoodActiveStep = atom({
  key: 'labBabyFoodActiveStep',
  default: BABYFOOD_ACTIVE_STEP.BRAND
})

export const labNutritionActiveStep = atom({
  key: 'labNutritionActiveStep',
  default: NUTRITION_ACTIVE_STEP.SELECT
})

export const labNutritionActiveStepList = atom({
  key: 'labNutritionActiveStepList',
  default: NUTRITION_ACTIVE_STEP
})

export const labIsBottomButtonDisabled = atom({
  key: 'labIsBottomButtonDisabled',
  default: true
})

export const labNutritionSelectList = atom({
  key: 'labNutritionSelectList',
  default: NUTRITION_SELECT_LIST
})

export const labNutritionSelectedLength = atom({
  key: 'labNutritionSelectedLength',
  default: 0
})

export const labIsSUR03Selected = atom({
  key: 'labIsSUR03Selected',
  default: false
})

export const labIsSUR04Selected = atom({
  key: 'labIsSUR04Selected',
  default: false
})

export const labIsSUR05Selected = atom({
  key: 'labIsSUR05Selected',
  default: false
})

export const labIsSUR06Selected = atom({
  key: 'labIsSUR06Selected',
  default: false
})

export const labBabyFoodList = atom({
  key: 'labBabyFoodList',
  default: BABYFOOD_LIST
})

export const labIsBabyFoodEtcSelected = atom({
  key: 'labIsBabyFoodEtcSelected',
  default: false
})

export const labIsNutritionSUR03EtcSelected = atom({
  key: 'labIsNutritionSUR03EtcSelected',
  default: false
})

export const labIsNutritionSUR04EtcSelected = atom({
  key: 'labIsNutritionSUR04EtcSelected',
  default: false
})

export const labIsNutritionSUR05EtcSelected = atom({
  key: 'labIsNutritionSUR05EtcSelected',
  default: false
})

export const labNutritionSUR03List = atom({
  key: 'labNutritionSUR03List',
  default: NUTRITION_SUR03_LIST
})

export const labNutritionSUR04List = atom({
  key: 'labNutritionSUR04List',
  default: NUTRITION_SUR04_LIST
})

export const labNutritionSUR05List = atom({
  key: 'labNutritionSUR05List',
  default: NUTRITION_SUR05_LIST
})

export const labBabyFoodReasonList = atom({
  key: 'labBabyFoodReasonList',
  default: BABYFOOD_REASON_LIST
})

export const labNutritionSUR03ReasonList = atom({
  key: 'labNutritionSUR03ReasonList',
  default: NUTRITION_SUR03_REASON_LIST
})

export const labNutritionSUR04ReasonList = atom({
  key: 'labNutritionSUR04ReasonList',
  default: NUTRITION_SUR04_REASON_LIST
})

export const labNutritionSUR05ReasonList = atom({
  key: 'labNutritionSUR05ReasonList',
  default: NUTRITION_SUR05_REASON_LIST
})

export const labBabyFoodSelectBrandCd = atom({
  key: 'labBabyFoodSelectBrandCd',
  default: ''
})

export const labNutritionSUR03SelectBrandCd = atom({
  key: 'labNutritionSUR03SelectBrandCd',
  default: ''
})

export const labNutritionSUR04SelectBrandCd = atom({
  key: 'labNutritionSUR04SelectBrandCd',
  default: ''
})

export const labNutritionSUR05SelectBrandCd = atom({
  key: 'labNutritionSUR05SelectBrandCd',
  default: ''
})

export const labBabyFoodSelectBrandName = atom({
  key: 'labBabyFoodSelectBrandName',
  default: ''
})

export const labNutritionSUR03SelectBrandName = atom({
  key: 'labNutritionSUR03SelectBrandName',
  default: ''
})

export const labNutritionSUR04SelectBrandName = atom({
  key: 'labNutritionSUR04SelectBrandName',
  default: ''
})

export const labNutritionSUR05SelectBrandName = atom({
  key: 'labNutritionSUR05SelectBrandName',
  default: ''
})

export const labBabyFoodSelectReasonList = atom({
  key: 'labBabyFoodSelectReasonList',
  default: []
})

export const labNutritionSUR03SelectReasonList = atom({
  key: 'labNutritionSUR03SelectReasonList',
  default: []
})

export const labNutritionSUR04SelectReasonList = atom({
  key: 'labNutritionSUR04SelectReasonList',
  default: []
})

export const labNutritionSUR05SelectReasonList = atom({
  key: 'labNutritionSUR05SelectReasonList',
  default: []
})

export const labCommentShowCommentView = atom({
  key: 'labCommentShowCommentView',
  default: false
})

export const labCommentShowCommentModify = atom({
  key: 'labCommentShowCommentModify',
  default: false
})

export const labRecommendList = atom({
  key: 'labRecommendList',
  default: LAB_RECOMMEND_LIST
})

export const labIsItemSelected = atom({
  key: 'labIsItemSelected',
  default: false
})

export const labScore = atom({
  key: 'labScore',
  default: ''
})

export const labCommentItem = atom({
  key: 'labCommentItem',
  default: null
})

export const labCommentFetch = atom({
  key: 'labCommentFetch',
  default: undefined
})

export const labCommentCheck = atom({
  key: 'labCommentCheck',
  default: false
})

export const labCommentOrder = atom({
  key: 'labCommentOrder',
  default: LAB_ORDER_TYPE.NEWEST
})

export const labCommentPostKey = atom({
  key: 'labCommentPostKey',
  default: null
})

export const labCommentCommentKey = atom({
  key: 'labCommentCommentKey',
  default: null
})