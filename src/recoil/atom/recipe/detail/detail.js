import {atom} from "recoil";

export const recipeDetailPopupDialogOpenStatus = atom({
  key: 'recipeDetailPopupDialogOpenStatus',
  default: false
})

export const recipeDetailPopupDialogType = atom({
  key: 'recipeDetailPopupDialogType',
  default: ''
})

export const recipeDetailSlideIndex = atom({
  key: 'recipeDetailSlideIndex',
  default: 0
})

export const recipeDetailUpdateCount = atom({
  key: 'recipeDetailUpdateCount',
  default: 0
})