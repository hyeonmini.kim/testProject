import {atom} from "recoil";
import {RECIPE_CREATE_ACTIVE_STEP, RECIPE_CREATE_STEP} from "../../../constant/recipe/Create";
import {RECIPE} from "../../../constant/recipe/Recipe";

export const createActiveStep = atom({
  key: 'createActiveStep',
  default: RECIPE_CREATE_ACTIVE_STEP.BASIC
})

export const createRecipeKey = atom({
  key: 'createRecipeKey',
  default: ''
})

export const createRecipeName = atom({
  key: 'createRecipeName',
  default: ''
})

export const createRecipeDesc = atom({
  key: 'createRecipeDesc',
  default: ''
})

export const createRecipeCategory = atom({
  key: 'createRecipeCategory',
  default: ''
})

export const createRecipeImages = atom({
  key: 'createRecipeImages',
  default: []
})

export const createRecipeDeleteImages = atom({
  key: 'createRecipeDeleteImages',
  default: []
})

export const createRecipeCookTime = atom({
  key: 'createRecipeCookTime',
  default: ''
})

export const createRecipeDifficulty = atom({
  key: 'createRecipeDifficulty',
  default: ''
})

export const createRecipeMonthlyAge = atom({
  key: 'createRecipeMonthlyAge',
  default: ''
})

export const createRecipeHealthNote = atom({
  key: 'createRecipeHealthNote',
  default: ''
})

export const createRecipeQuantity = atom({
  key: 'createRecipeQuantity',
  default: 1
})
export const createRecipeBasicMaterials = atom({
  key: 'createRecipeBasicMaterials',
  default: []
})
export const createRecipeSeasoningMaterials = atom({
  key: 'createRecipeSeasoningMaterials',
  default: []
})

export const createRecipeMaterialType = atom({
  key: 'createRecipeMaterialType',
  default: ''
})

export const createRecipeSteps = atom({
  key: 'createRecipeSteps',
  default: [RECIPE_CREATE_STEP.DEFAULT, RECIPE_CREATE_STEP.DEFAULT]
})

export const createTemporaryRecipe = atom({
  key: 'createTemporaryRecipe',
  default: RECIPE
})

export const createShowMaterialEdit = atom({
  key: 'createShowMaterialEdit',
  default: false
})

export const createShowMaterialAdd = atom({
  key: 'createShowMaterialAdd',
  default: false
})

export const createShowMaterialInput = atom({
  key: 'createShowMaterialInput',
  default: false
})

export const createShowMeasurementUnit = atom({
  key: 'createShowMeasurementUnit',
  default: false
})

export const createShowPreview = atom({
  key: 'createShowPreview',
  default: false
})

export const createShowReturn = atom({
  key: 'createShowReturn',
  default: false
})

export const createReturnMessage = atom({
  key: 'createReturnMessage',
  default: ''
})

export const createTag = atom({
  key: 'createTag',
  default: ''
})

export const createEmpty = atom({
  key: 'createEmpty',
  default: RECIPE
})

export const createOrderImages = atom({
  key: 'createOrderImages',
  default: []
})

export const createOrderDeleteImages = atom({
  key: 'createOrderDeleteImages',
  default: []
})