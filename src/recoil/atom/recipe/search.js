import {atom} from "recoil";
import {
  RECIPE_RESULT_STATUS,
  RECIPE_SEARCH,
  RECIPE_SEARCH_AGE_GROUP_TYPE,
  RECIPE_SEARCH_HOME_HEALTH
} from "../../../constant/recipe/Search";

export const searchRecipeSearch = atom({
  key: 'searchRecipeSearch',
  default: ''
})

export const searchRecipeResult = atom({
  key: 'searchRecipeResult',
  default: RECIPE_RESULT_STATUS.INIT
})

export const searchIsTitle = atom({
  key: 'searchIsTitle',
  default: false
})

export const searchCall = atom({
  key: 'searchCall',
  default: RECIPE_SEARCH
})

export const searchAgeGroup = atom({
  key: 'searchAgeGroup',
  default: RECIPE_SEARCH_AGE_GROUP_TYPE.ALL
})

export const searchShowDetail = atom({
  key: 'searchShowDetail',
  default: false
})

export const searchIsHome = atom({
  key: 'searchIsHome',
  default: true
})

export const searchIsAvoid = atom({
  key: 'searchIsAvoid',
  default: false
})

export const searchIsSearch = atom({
  key: 'searchIsSearch',
  default: false
})

export const searchCategory = atom({
  key: 'searchCategory',
  default: ''
})

export const searchActiveTab = atom({
  key: 'searchActiveTab',
  default: RECIPE_SEARCH_HOME_HEALTH.NAME
})

export const searchIsCall = atom({
  key: 'searchIsCall',
  default: null
})

export const searchShowFilter = atom({
  key: 'searchShowFilter',
  default: true
})

export const searchFetchItems = atom({
  key: 'searchFetchItems',
  default: []
})