import {atom} from "recoil";

const countState = atom({
  key: 'countInfo',
  default: 0
})


export {countState}

