import {atom} from "recoil";

export const guestModalOpen = atom({
  key: 'guestModalOpen',
  default: {
    isOpen: false,
    type: ''
  }
})
