import {atom} from "recoil";

export const guestInfo = atom({
  key: 'guestInfoAtom',
  default: {
    height: '',
    weight: '',
    allergies: [],
    healthKeywords: []

  }
})

