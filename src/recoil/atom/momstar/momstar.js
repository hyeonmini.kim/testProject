import {atom} from "recoil";
import {MOMSTAR_COMMON} from "../../../constant/momstar/Momstar";

export const momStarActiveTab = atom({
  key: 'momStarActiveTab',
  default: MOMSTAR_COMMON.MOMSTAR.TAB
})

export const momStarIsScroll = atom({
  key: 'momStarIsScroll',
  default: false
})