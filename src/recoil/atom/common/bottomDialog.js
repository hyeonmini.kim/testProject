import {atom} from "recoil";

const commonBottomDialogState = atom({
  key: 'commonBottomDialog',
  default: false,
})

const commonBottomDialogTitleState = atom({
  key: 'commonBottomDialogTitle',
  default: true,
})

const commonBottomDialogPageState = atom({
  key: 'commonBottomDialogPage',
  default: 0,
})

export {
  commonBottomDialogState,
  commonBottomDialogTitleState,
  commonBottomDialogPageState,
}