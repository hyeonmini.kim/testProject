import {atom} from "recoil";

const commonHeaderBackState = atom({
  key: 'commonHeaderBack',
  default: true,
});

export {commonHeaderBackState};

const commonHeaderCloseState = atom({
  key: 'commonHeaderClose',
  default: false,
});

export {commonHeaderCloseState};
