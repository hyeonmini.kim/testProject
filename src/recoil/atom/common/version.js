import {atom} from "recoil";
import {ENV_TYPE} from "../../../constant/common/Env";

export const versionCurrent = atom({
  key: 'versionCurrent',
  default: '',
})

export const versionNew = atom({
  key: 'versionNew',
  default: '',
})

export const isMock = atom({
  key: 'isMock',
  default: false,
})

export const env = atom({
  key: 'env',
  default: ENV_TYPE.PRODUCT,
})
