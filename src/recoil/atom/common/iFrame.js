import {atom} from "recoil";

export const iFrameUrl = atom({
  key: 'iFrameUrl',
  default: ''
})

export const iFrameOpen = atom({
  key: 'iFrameOpen',
  default: false
})