import {atom} from "recoil";
import {GNB_TYPE} from "../../../constant/common/GNB";

export const gnbActiveTab = atom({
  key: 'gnbActiveTab',
  default: GNB_TYPE.HOME
})