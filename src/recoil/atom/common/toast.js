import {atom} from "recoil";
import {TOAST_TYPE} from "../../../constant/common/Common";

export const toastMessageAtom = atom({
  key: 'toastMessageAtom',
  default: {
    type: TOAST_TYPE.BOTTOM_HEADER,
    message: ''
  }
})
