import {atom} from "recoil";
import {STACK_PRE_FETCHING} from "../../../constant/common/StackNavigation";

export const stackNavigation = atom({
  key: 'stackNavigation',
  default: []
})

export const stackPreFetching = atom({
  key: 'stackPreFetching',
  default: STACK_PRE_FETCHING
})