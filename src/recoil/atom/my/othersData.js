import {atom} from "recoil";

export const othersProfilePictureUrl = atom({
  key: 'othersProfilePictureUrl',
  default: ''
})

export const othersMarketingInfoPushNotifSettingModifiedDatetime = atom({
  key: 'othersMarketingInfoPushNotifSettingModifiedDatetime',
  default: ''
})

export const othersBirthDay = atom({
  key: 'othersBirthDay',
  default: ''
})

export const othersProfileSelectedBabyKey = atom({
  key: 'othersProfileSelectedBabyKey',
  default: 0
})

export const othersBriefBio = atom({
  key: 'othersBriefBio',
  default: ''
})

export const othersBabies = atom({
  key: 'othersBabies',
  default: []
})

export const othersType = atom({
  key: 'othersType',
  default: 'NORMAL_MEMBER'
})

export const othersEmailReceiveAgreementModifiedDatetime = atom({
  key: 'othersEmailReceiveAgreementModifiedDatetime',
  default: ''
})

export const othersIsTermsCollectingPersonalDataMarketingAgreed = atom({
  key: 'othersIsTermsCollectingPersonalDataMarketingAgreed',
  default: false
})

export const othersAccountKey = atom({
  key: 'othersAccountKey',
  default: 0
})

export const othersIsEmailReceiveAgreed = atom({
  key: 'myIsEmailReceiveAgreed',
  default: ''
})

export const othersPhoneNumber = atom({
  key: 'othersPhoneNumber',
  default: ''
})

export const othersSocialMediaUrl = atom({
  key: 'othersSocialMediaUrl',
  default: ''
})

export const othersIsPostCensorshipResultPushNotifSet = atom({
  key: 'othersIsPostCensorshipResultPushNotifSet',
  default: ''
})

export const othersGrade = atom({
  key: 'othersGrade',
  default: 'LV1'
})

export const othersNickname = atom({
  key: 'othersNickname',
  default: ''
})

export const othersTextMessageReciveAgreementModifiedDatetime = atom({
  key: 'othersTextMessageReciveAgreementModifiedDatetime',
  default: ''
})

export const othersIsMarketingInfoPushNotifSet = atom({
  key: 'othersIsMarketingInfoPushNotifSet',
  default: false
})

export const othersIsTextMessageReciveAgreed = atom({
  key: 'othersIsTextMessageReciveAgreed',
  default: ''
})

export const othersKey = atom({
  key: 'othersKey',
  default: undefined
})

export const othersEmail = atom({
  key: 'othersEmail',
  default: ''
})
