import {atom} from "recoil";

export const myIsExpandedAccordion = atom({
  key: 'myIsExpandedAccordion',
  default: false
})

export const myActiveTab = atom({
  key: 'myActiveTab',
  default: 'write'
})

export const mySelectedFilter = atom({
  key: 'mySelectedFilter',
  default: '전체'
})

export const myPageWrite = atom({
  key: 'myPageWrite',
  default: -1
})

export const myPageScrap = atom({
  key: 'myPageScrap',
  default: -1
})

export const myMoreSearchWrite = atom({
  key: 'myMoreSearchWrite',
  default: false
})

export const myMoreSearchScrap = atom({
  key: 'myMoreSearchScrap',
  default: -1
})

export const myWriteList = atom({
  key: 'myWriteList',
  default: []
})

export const myScrapList = atom({
  key: 'myScrapList',
  default: []
})

export const myWriteCount = atom({
  key: 'myWriteCount',
  default: 0
})

export const myScrapCount = atom({
  key: 'myScrapCount',
  default: 0
})

export const myIsSelectedFilter = atom({
  key: 'myIsSelectedFilter',
  default: false
})
// nickname value
const myNicknameState = atom({
  key: 'myNickname',
  default: '',
});
// nickname helper text value
const myNicknameHelperTextState = atom({
  key: 'myNicknameHelperText',
  default: '',
})
// nickname error
const myNicknameErrorState = atom({
  key: 'myNicknameError',
  default: true,
})
const myNicknameDuplicatedState = atom({
  key: 'myNicknameDuplicated',
  default: false,
})
// email value
const myEmailState = atom({
  key: 'myEmail',
  default: '',
});
// email helper text value
const myEmailHelperTextState = atom({
  key: 'myEmailHelperText',
  default: '',
})
// email error
const myEmailErrorState = atom({
  key: 'myEmailError',
  default: true,
})
const myEmailDuplicatedState = atom({
  key: 'myEmailDuplicated',
  default: false,
})

export {
  myEmailState,
  myEmailHelperTextState,
  myEmailErrorState,
  myEmailDuplicatedState,
  myNicknameState,
  myNicknameHelperTextState,
  myNicknameErrorState,
  myNicknameDuplicatedState,
};
