import {atom} from "recoil";

const myInformationFooterBottomButtonState = atom({
  key: 'myInformationFooterBottomButtonState',
  default: false,
});
export {myInformationFooterBottomButtonState};

const myInformationFooterBottomButtonTextState = atom({
  key: 'myInformationFooterBottomButtonTextState',
  default: '확인',
});
export {myInformationFooterBottomButtonTextState};

