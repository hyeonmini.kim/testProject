import {atom} from "recoil";

export const othersActiveTab = atom({
  key: 'othersActiveTab',
  default: 'write'
})

export const othersPageWrite = atom({
  key: 'othersPageWrite',
  default: -1
})

export const othersPageScrap = atom({
  key: 'othersPageScrap',
  default: -1
})

export const othersMoreSearchWrite = atom({
  key: 'othersMoreSearchWrite',
  default: false
})

export const othersMoreSearchScrap = atom({
  key: 'othersMoreSearchScrap',
  default: -1
})

export const othersWriteList = atom({
  key: 'othersWriteList',
  default: []
})

export const othersScrapList = atom({
  key: 'othersScrapList',
  default: []
})

export const othersWriteCount = atom({
  key: 'othersWriteCount',
  default: 0
})

export const othersScrapCount = atom({
  key: 'othersScrapCount',
  default: 0
})
