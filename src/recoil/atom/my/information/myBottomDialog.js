import {atom} from "recoil";

const myInformationBottomDialogState = atom({
  key: 'myInformationBottomDialogState',
  default: false,
})

export {
  myInformationBottomDialogState,
}

const myChildrenBottomDialogState = atom({
  key: 'myChildrenBottomDialogState',
  default: false,
})

export {
  myChildrenBottomDialogState,
}

const myFilterBottomDialogState = atom({
  key: 'myFilterBottomDialogState',
  default: false,
})

export {
  myFilterBottomDialogState,
}

const mySettingOneOnOneInquiryBottomDialogState = atom({
  key: 'settingOneOnOneInquiryBottomDialogState',
  default: false,
})

export {
  mySettingOneOnOneInquiryBottomDialogState,
}
