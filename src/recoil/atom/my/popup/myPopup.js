import {atom} from "recoil";

export const myOpenMyInformationPopup = atom({
  key: 'myOpenMyInformationPopup',
  default: false
})

export const myOpenMomStar = atom({
  key: 'myOpenMomStar',
  default: false
})

export const myOpenChildren = atom({
  key: 'myOpenChildren',
  default: false
})

export const myOpenAddChildren = atom({
  key: 'myOpenAddChildren',
  default: false
})

export const myOpenAddChildrenPage = atom({
  key: 'myOpenAddChildrenPage',
  default: 0
})

export const myOpenAddChildrenBtnPosition = atom({
  key: 'myOpenAddChildrenBtnPosition',
  default: 'initial'
})

export const myOpenAddChildrenBtnMargin = atom({
  key: 'myOpenAddChildrenBtnMargin',
  default: ''
})

export const myOpenAddChildrenBtnText = atom({
  key: 'myOpenAddChildrenBtnText',
  default: '다음'
})

export const myOpenResultAdd = atom({
  key: 'myOpenResultAdd',
  default: false
})

export const myOpenSns = atom({
  key: 'myOpenSns',
  default: false
})

export const myOpenChangePw = atom({
  key: 'myOpenChangePw',
  default: false
})

export const myOpenChangePhone = atom({
  key: 'myOpenChangePhone',
  default: false
})

export const myOpenWithdrawMembership = atom({
  key: 'myOpenWithdrawMembership',
  default: false
})

export const myOpenSnsDetail = atom({
  key: 'myOpenSnsDetail',
  default: false
})

export const myCallbackAddSns = atom({
  key: 'myCallbackAddSns',
  default: false
})