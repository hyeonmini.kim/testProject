import {atom} from "recoil";

export const settingOneOnOneInquiryKey = atom({
  key: 'settingOneOnOneInquiryKey',
  default: 0
})

export const settingOneOnOneInquiryTitle = atom({
  key: 'settingOneOnOneInquiryTitle',
  default: ''
})

export const settingOneOnOneInquiryBody = atom({
  key: 'settingOneOnOneInquiryBody',
  default: ''
})

export const settingOneOnOneInquiryAttachmentFile = atom({
  key: 'settingOneOnOneInquiryAttachmentFile',
  default: null
})

export const settingOneOnOneInquiryCategory = atom({
  key: 'settingOneOnOneInquiryCategory',
  default: ''
})

export const settingOneOnOneInquiryIsTrue = atom({
  key: 'settingOneOnOneInquiryIsTrue',
  default: true
})
