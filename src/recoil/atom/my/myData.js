import {atom} from "recoil";

export const myProfilePictureUrl = atom({
  key: 'myProfilePictureUrl',
  default: ''
})

export const myMarketingInfoPushNotifSettingModifiedDatetime = atom({
  key: 'myMarketingInfoPushNotifSettingModifiedDatetime',
  default: ''
})

export const myBirthDay = atom({
  key: 'myBirthDay',
  default: ''
})

export const myProfileSelectedBabyKey = atom({
  key: 'myProfileSelectedBabyKey',
  default: 0
})

export const myBriefBio = atom({
  key: 'myBriefBio',
  default: ''
})

export const myBabies = atom({
  key: 'myBabies',
  default: []
})

export const myType = atom({
  key: 'myType',
  default: 'NORMAL_MEMBER'
})

export const myEmailReceiveAgreementModifiedDatetime = atom({
  key: 'myEmailReceiveAgreementModifiedDatetime',
  default: ''
})

export const myIsTermsCollectingPersonalDataMarketingAgreed = atom({
  key: 'myIsTermsCollectingPersonalDataMarketingAgreed',
  default: false
})

export const myAccountKey = atom({
  key: 'myAccountKey',
  default: 0
})

export const myIsEmailReceiveAgreed = atom({
  key: 'myIsEmailReceiveAgreed',
  default: ''
})

export const myPhoneNumber = atom({
  key: 'myPhoneNumber',
  default: ''
})

export const mySocialMediaUrl = atom({
  key: 'mySocialMediaUrl',
  default: ''
})

export const myIsPostCensorshipResultPushNotifSet = atom({
  key: 'myIsPostCensorshipResultPushNotifSet',
  default: ''
})

export const myGrade = atom({
  key: 'myGrade',
  default: 'LV1'
})

export const myNickname = atom({
  key: 'myNickname',
  default: ''
})

export const myTextMessageReciveAgreementModifiedDatetime = atom({
  key: 'myTextMessageReciveAgreementModifiedDatetime',
  default: ''
})

export const myIsMarketingInfoPushNotifSet = atom({
  key: 'myIsMarketingInfoPushNotifSet',
  default: false
})

export const myIsTextMessageReciveAgreed = atom({
  key: 'myIsTextMessageReciveAgreed',
  default: ''
})

export const myKey = atom({
  key: 'myKey',
  default: 0
})

export const myEmail = atom({
  key: 'myEmail',
  default: ''
})

export const myIsProfilePictureUrlDeleted = atom({
  key: 'myIsProfilePictureUrlDeleted',
  default: false
})

export const myBabyKey = atom({
  key: 'myBabyKey',
  default: 0
})

export const myBabyParentKey = atom({
  key: 'myBabyParentKey',
  default: 0
})

export const myBabyNickname = atom({
  key: 'myBabyNickname',
  default: ''
})

export const myBabyBirthdate = atom({
  key: 'myBabyBirthdate',
  default: ''
})

export const myBabyHeight = atom({
  key: 'myBabyHeight',
  default: ''
})

export const myBabyWeight = atom({
  key: 'myBabyWeight',
  default: ''
})

export const myBabyGender = atom({
  key: 'myBabyGender',
  default: ''
})

export const myBabyProfilePictureUrl = atom({
  key: 'myBabyProfilePictureUrl',
  default: ''
})

export const myBabyProfilePictureThumbnailOrder = atom({
  key: 'myBabyProfilePictureThumbnailOrder',
  default: ''
})

export const myBabyAllergyIngredients = atom({
  key: 'myBabyAllergyIngredients',
  default: []
})

export const myBabyConcerns = atom({
  key: 'myBabyConcerns',
  default: []
})

export const myBabyIsProfilePictureUrlDeleted = atom({
  key: 'myBabyIsProfilePictureUrlDeleted',
  default: false
})

export const tempMyProfilePictureUrl = atom({
  key: 'tempMyProfilePictureUrl',
  default: ''
})

export const isChangePhoneNumber = atom({
  key: 'isChangePhoneNumber',
  default: false
})

export const tempMyMarketingInfoPushNotifSettingModifiedDatetime = atom({
  key: 'tempMyMarketingInfoPushNotifSettingModifiedDatetime',
  default: ''
})

export const tempMyBirthDay = atom({
  key: 'tempMyBirthDay',
  default: ''
})

export const tempMyProfileSelectedBabyKey = atom({
  key: 'tempMyProfileSelectedBabyKey',
  default: 0
})

export const tempMyBriefBio = atom({
  key: 'tempMyBriefBio',
  default: ''
})

export const tempMyBabies = atom({
  key: 'tempMyBabies',
  default: []
})

export const tempMyType = atom({
  key: 'tempMyType',
  default: 'NORMAL_MEMBER'
})

export const tempMyEmailReceiveAgreementModifiedDatetime = atom({
  key: 'tempMyEmailReceiveAgreementModifiedDatetime',
  default: ''
})

export const tempMyIsTermsCollectingPersonalDataMarketingAgreed = atom({
  key: 'tempMyIsTermsCollectingPersonalDataMarketingAgreed',
  default: false
})

export const tempMyAccountKey = atom({
  key: 'tempMyAccountKey',
  default: 0
})

export const tempMyIsEmailReceiveAgreed = atom({
  key: 'tempMyIsEmailReceiveAgreed',
  default: ''
})

export const tempMyPhoneNumber = atom({
  key: 'tempMyPhoneNumber',
  default: ''
})

export const tempMySocialMediaUrl = atom({
  key: 'tempMySocialMediaUrl',
  default: ''
})

export const tempMyIsPostCensorshipResultPushNotifSet = atom({
  key: 'tempMyIsPostCensorshipResultPushNotifSet',
  default: ''
})

export const tempMyGrade = atom({
  key: 'tempMyGrade',
  default: 'LV1'
})

export const tempMyNickname = atom({
  key: 'tempMyNickname',
  default: ''
})

export const tempMyTextMessageReciveAgreementModifiedDatetime = atom({
  key: 'tempMyTextMessageReciveAgreementModifiedDatetime',
  default: ''
})

export const tempMyIsMarketingInfoPushNotifSet = atom({
  key: 'tempMyIsMarketingInfoPushNotifSet',
  default: false
})

export const tempMyIsTextMessageReciveAgreed = atom({
  key: 'tempMyIsTextMessageReciveAgreed',
  default: ''
})

export const tempMyKey = atom({
  key: 'tempMyKey',
  default: 0
})

export const tempMyEmail = atom({
  key: 'tempMyEmail',
  default: ''
})

export const tempMyBabyKey = atom({
  key: 'tempMyBabyKey',
  default: 0
})

export const tempMyBabyParentKey = atom({
  key: 'tempMyBabyParentKey',
  default: 0
})

export const tempMyBabyNickname = atom({
  key: 'tempMyBabyNickname',
  default: ''
})

export const tempMyBabyBirthdate = atom({
  key: 'tempMyBabyBirthdate',
  default: ''
})

export const tempMyBabyHeight = atom({
  key: 'tempMyBabyHeight',
  default: ''
})

export const tempMyBabyWeight = atom({
  key: 'tempMyBabyWeight',
  default: ''
})

export const tempMyBabyGender = atom({
  key: 'tempMyBabyGender',
  default: ''
})

export const tempMyBabyProfilePictureUrl = atom({
  key: 'tempMyBabyProfilePictureUrl',
  default: ''
})

export const tempMyBabyProfilePictureThumbnailOrder = atom({
  key: 'tempMyBabyProfilePictureThumbnailOrder',
  default: ''
})

export const tempMyBabyAllergyIngredients = atom({
  key: 'tempMyBabyAllergyIngredients',
  default: []
})

export const tempMyBabyConcerns = atom({
  key: 'tempMyBabyConcerns',
  default: []
})

export const myCall = atom({
  key: 'myCall',
  default: null
})