import {atom} from "recoil";

export const settingShowPush = atom({
  key: 'settingShowPush',
  default: false
})

export const settingIsAutoLogin = atom({
  key: 'settingIsAutoLogin',
  default: true
})

export const settingIsScreenLock = atom({
  key: 'settingIsScreenLock',
  default: true
})

export const settingShowNotice = atom({
  key: 'settingShowNotice',
  default: false
})

export const settingShowTerms = atom({
  key: 'settingShowTerms',
  default: false
})

export const settingShowLicense = atom({
  key: 'settingShowLicense',
  default: false
})

export const settingShowFAQ = atom({
  key: 'settingShowFAQ',
  default: false
})

export const settingShowVersion = atom({
  key: 'settingShowVersion',
  default: false
})

export const settingShowOneOnOneInquiries = atom({
  key: 'settingShowOneOnOneInquiries',
  default: false
})

export const settingIsEventInfo = atom({
  key: 'settingIsEventInfo',
  default: false
})

export const settingIsRecipeResult = atom({
  key: 'settingIsRecipeResult',
  default: false
})

export const settingShowSetting = atom({
  key: 'settingShowSetting',
  default: false
})

export const settingShowNoticeDetail = atom({
  key: 'settingShowNoticeDetail',
  default: false
})

export const settingShowServiceTerms = atom({
  key: 'settingShowServiceTerms',
  default: false
})

export const settingShowPersonalTerms = atom({
  key: 'settingShowPersonalTerms',
  default: false
})

export const settingShowAgreeTerms = atom({
  key: 'settingShowAgreeTerms',
  default: false
})

export const settingShowFAQDetail = atom({
  key: 'settingShowFAQDetail',
  default: false
})

export const settingIsCall = atom({
  key: 'settingIsCall',
  default: false
})

export const settingAgreeTermsDetail = atom({
  key: 'settingAgreeTermsDetail',
  default: {}
})

export const settingShowOneOnOneInquiriesDetail = atom({
  key: 'settingShowOneOnOneInquiriesDetail',
  default: false
})

export const settingShowOneOnOneInquiriesPopup = atom({
  key: 'settingShowOneOnOneInquiriesPopup',
  default: false
})

export const settingShowRegistOneOnOneInquiry = atom({
  key: 'settingShowRegistOneOnOneInquiry',
  default: false
})

export const settingIsChangedOneOnOneInquiries = atom({
  key: 'settingIsChangedOneOnOneInquiries',
  default: false
})

export const licenseUrl = atom({
  key: 'licenseUrl',
  default: ''
})
