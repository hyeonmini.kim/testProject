import {atom} from "recoil";
import {ALLERGY_LIST, HEALTH_KEYWORD} from "../../../constant/sign-up/SignUp";

// allergy value
export const childrenAllergyIngredientsList = atom({
  key: 'childrenAllergyIngredientsList',
  default: ALLERGY_LIST,
});
// child's allergy value
export const childrenAllergyIngredients = atom({
  key: 'childrenAllergyIngredients',
  default: [],
});
// keyword value
export const childrenConcernsList = atom({
  key: 'childrenConcernsList',
  default: HEALTH_KEYWORD,
});
// child's keyword value
export const childrenConcerns = atom({
  key: 'childrenConcerns',
  default: [],
});