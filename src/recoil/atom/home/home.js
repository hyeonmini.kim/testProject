import {atom} from "recoil";

export const homeShowInfoBanner = atom({
  key: 'homeShowInfoBanner',
  default: false
})

export const isOpenTooltip = atom({
  key: 'isOpenTooltip',
  default: false
})