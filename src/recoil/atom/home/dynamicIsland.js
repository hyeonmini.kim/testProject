import {atom} from "recoil";

export const dynamicIslandExpand = atom({
  key: 'dynamicIslandExpand',
  default: true
})