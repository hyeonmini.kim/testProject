import {atom} from "recoil";

export const inputChildrenBirth = atom({
  key: 'inputChildrenBirth',
  default: ''
})

export const inputChildrenHeight = atom({
  key: 'inputChildrenHeight',
  default: ''
})

export const inputChildrenWeight = atom({
  key: 'inputChildrenWeight',
  default: ''
})

export const inputChildrenGender = atom({
  key: 'inputChildrenGender',
  default: ''
})

export const inputChildrenAllergy = atom({
  key: 'inputChildrenAllergy',
  default: undefined
})

export const inputChildrenAllergyList = atom({
  key: 'inputChildrenAllergyList',
  default: []
})

export const inputChildrenKeyword = atom({
  key: 'inputChildrenKeyword',
  default: ''
})

export const inputChildrenKeywordList = atom({
  key: 'inputChildrenKeywordList',
  default: []
})
