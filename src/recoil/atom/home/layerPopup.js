import {atom} from "recoil";
import {LAYER_POPUP} from "../../../constant/home/LayerPopup";

export const layerPopupAtom = atom({
  key: 'layerPopupAtom',
  default: LAYER_POPUP
})

export const showLayerPopup = atom({
  key: 'showLayerPopup',
  default: false
})

export const layerPopupClicked = atom({
  key: 'layerPopupClicked',
  default: false
})

export const layerPopupSlideIndex = atom({
  key: 'layerPopupSlideIndex',
  default: 0
})

export const layerPopupInfiniteBool = atom({
  key: 'layerPopupInfiniteBool',
  default: false
})