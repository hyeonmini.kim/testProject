import {atom} from "recoil";

export const notificationOpen = atom({
  key: 'notificationOpen',
  default: false
})

export const notificationHasNewItem = atom({
  key: 'notificationHasNewItem',
  default: false
})