import {atom} from "recoil";

export const homeModalOpen = atom({
  key: 'homeModalOpen',
  default: false
})
