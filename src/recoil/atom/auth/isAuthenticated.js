import {atom} from "recoil";
import {ISAUTH_INIT} from "../../../constant/common/Common";

export const isAuthenticated = atom({
  key: 'isAuthenticated',
  default: ISAUTH_INIT
})
