import {atom} from "recoil";

const commonAgreeDetailState = atom({
  key: 'commonAgreeDetail',
  default: false,
});

export {commonAgreeDetailState};

const commonDetailNumberState = atom({
  key: 'commonDetailNumber',
  default: 0,
});

export {commonDetailNumberState};

export const commonAgreeOpen = atom({
  key: 'commonAgreeOpen',
  default: false,
});

export const commonIsLoaded = atom({
  key: 'commonIsLoaded',
  default: false,
});
