import {atom} from "recoil";
import {SNS_ACCOUNT_STATUS} from "../../../constant/common/User";

export const memberInformation = atom({
  key: 'memberInformation',
  default: {}
})

export const forMyPageEntry = atom({
  key: 'forMyPageEntry',
  default: {}
})

export const userKey = atom({
  key: 'userKey',
  default: 0
})

export const userName = atom({
  key: 'userName',
  default: ''
})

export const userPhone = atom({
  key: 'userPhone',
  default: ''
})

export const userBirth = atom({
  key: 'userBirth',
  default: ''
})

export const userGender = atom({
  key: 'userGender',
  default: ''
})

export const userEmail = atom({
  key: 'userEmail',
  default: ''
})

export const userPassword = atom({
  key: 'userPassword',
  default: ''
})

export const userNickname = atom({
  key: 'userNickname',
  default: ''
})

export const userAvatar = atom({
  key: 'userAvatar',
  default: ''
})

export const userIsMomStar = atom({
  key: 'userIsMomStar',
  default: false
})

export const userChildren = atom({
  key: 'userChildren',
  default: []
})

export const userLevel = atom({
  key: 'userLevel',
  default: 0
})

export const userSnsLink = atom({
  key: 'userSnsLink',
  default: ''
})

export const userIntroduction = atom({
  key: 'userIntroduction',
  default: ''
})

export const userSnsAccount = atom({
  key: 'userSnsAccount',
  default: {SNS_ACCOUNT_STATUS}
})

export const tempUserKey = atom({
  key: 'tempUserKey',
  default: 0
})

export const tempUserName = atom({
  key: 'tempUserName',
  default: ''
})

export const tempUserPhone = atom({
  key: 'tempUserPhone',
  default: ''
})

export const tempUserBirth = atom({
  key: 'tempUserBirth',
  default: ''
})

export const tempUserGender = atom({
  key: 'tempUserGender',
  default: ''
})

export const tempUserEmail = atom({
  key: 'tempUserEmail',
  default: ''
})

export const tempUserPassword = atom({
  key: 'tempUserPassword',
  default: ''
})

export const tempUserNickname = atom({
  key: 'tempUserNickname',
  default: ''
})

export const tempUserAvatar = atom({
  key: 'tempUserAvatar',
  default: ''
})

export const tempUserIsMomStar = atom({
  key: 'tempUserIsMomStar',
  default: false
})

export const tempUserChildren = atom({
  key: 'tempUserChildren',
  default: []
})

export const tempUserLevel = atom({
  key: 'tempUserLevel',
  default: 0
})

export const tempUserSnsLink = atom({
  key: 'tempUserSnsLink',
  default: ''
})

export const tempUserIntroduction = atom({
  key: 'tempUserIntroduction',
  default: ''
})

export const tempUserSnsAccount = atom({
  key: 'tempUserSnsAccount',
  default: {SNS_ACCOUNT_STATUS}
})
