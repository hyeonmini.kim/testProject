import {atom} from "recoil";

export const accessToken = atom({
  key: 'accessToken',
  default: ''
})

export const refreshToken = atom({
  key: 'refreshToken',
  default: ''
})

export const socialAccountId = atom({
  key: 'socialAccountId',
  default: ''
})

export const transactionSeqNumber = atom({
  key: 'transactionSeqNumber',
  default: ''
})