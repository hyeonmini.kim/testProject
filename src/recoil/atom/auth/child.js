import {atom} from "recoil";

export const childId = atom({
  key: 'childId',
  default: ''
})

export const childNickname = atom({
  key: 'childNickname',
  default: ''
})

export const childGender = atom({
  key: 'childGender',
  default: ''
})

export const childBirth = atom({
  key: 'childBirth',
  default: ''
})

export const childWeight = atom({
  key: 'childWeight',
  default: ''
})

export const childHeight = atom({
  key: 'childHeight',
  default: ''
})

export const childAllergy = atom({
  key: 'childAllergy',
  default: ''
})

export const childKeyword = atom({
  key: 'childKeyword',
  default: ''
})

export const childAvatar = atom({
  key: 'childAvatar',
  default: ''
})
