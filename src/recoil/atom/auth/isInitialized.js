import {atom} from "recoil";

export const isInitialized = atom({
  key: 'isInitialized',
  default: false
})

export const isInitAxios = atom({
  key: 'isInitAxios',
  default: false
})

export const isInitEnv = atom({
  key: 'isInitEnv',
  default: false
})