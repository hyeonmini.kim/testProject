import {getEnv, isAuthApi, isMockApi} from "../utils/envUtils";
import {useMutation} from "react-query";
import {ENV, IS_MOCK} from "../config";
import {MOCK_ADD_CHILDREN} from "../mock/auth";
import {authProvider, queryOption} from "../layouts/common/AxiosProvider";
import {ERRORS, NEW_CUSTOM_ERROR} from "../constant/common/Error";
import {isValidateParam} from "../utils/validateUtils";
import * as yup from "yup";
import {PATH_DONOTS_API} from "../routes/paths";

// 아이 추가
const postAddChildrenSchema = yup.object().shape({
  nickname: yup.string(),
  birthdate: yup.string(),
  height: yup.string(),
  weight: yup.string(),
  gender: yup.string(),
  allergyIngredientsList: yup.array(),
  allergyIngredients: yup.array(),
  concernsList: yup.array(),
  concerns: yup.array(),
})
export const postAddChildren = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postAddChildrenFetch(param, isMock, env, isAuth, isSSG))
}
export const postAddChildrenFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, postAddChildrenSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_ADD_CHILDREN
  }
  return await authProvider(isSSG).post(PATH_DONOTS_API.MY.ADD_CHILDREN(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}