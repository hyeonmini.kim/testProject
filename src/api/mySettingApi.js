import {getEnv, isAuthApi, isMockApi} from "../utils/envUtils";
import {PATH_DONOTS_API} from "../routes/paths";
import {
  MOCK_FAQ,
  MOCK_FAQ2,
  MOCK_FAQ_DETAIL,
  MOCK_GET_AGREE_MKT,
  MOCK_GET_PUSH_CONTENTS,
  MOCK_GET_PUSH_MKT,
  MOCK_NOTICES,
  MOCK_NOTICES2,
  MOCK_ONE_ON_ONE_INQUIRIES,
  MOCK_ONE_ON_ONE_INQUIRIES2,
  MOCK_ONE_ON_ONE_INQUIRY_DETAIL,
  MOCK_PUT_AGREE_MKT,
  MOCK_PUT_PUSH_CONTENTS,
  MOCK_PUT_PUSH_MKT,
  MOCK_TERMS_OF_USE,
  MOCK_TERMS_OF_USE_KEY,
  MOCK_TERMS_OF_USE_TITLE
} from "../mock/setting";
import {useMutation, useQuery} from "react-query";
import {ENV, IS_MOCK} from "../config";
import {authProvider, noAuthProvider, queryOption} from "../layouts/common/AxiosProvider";
import {ERRORS, NEW_CUSTOM_ERROR} from "../constant/common/Error";
import * as yup from "yup";
import {isValidateParam, paginationSchema} from "../utils/validateUtils";
import {FAQ_CATEGORY_ITEMS} from "../constant/my/mock/FAQ";

// 공지사항 리스트
const getNoticeListSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema // 페이지네이션
  }),
})
export const getNoticeListMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getNoticeListFetch(param, isMock, env, isSSG))
}
export const getNoticeListFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getNoticeListSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_NOTICES
    } else {
      return MOCK_NOTICES2
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.SETTING.NOTICE(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.content
}

// 공지사항 상세
const getNoticeDetailSchema = yup.object().shape({
  noticePostKey: yup.number().required(), // 공지사항 키
})
export const getNoticeDetail = (param, isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useQuery(["getNoticeDetail", param], () => getNoticeDetailsFetch(param, isMock, env, isSSG), {...queryOption})
}
export const getNoticeDetailsFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getNoticeDetailSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_NOTICES
    } else {
      return MOCK_NOTICES2
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.SETTING.NOTICE_DETAIL(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

// 이용안내/FAQ
const getFaqListSchema = yup.object().shape({
  category: yup.mixed().oneOf(FAQ_CATEGORY_ITEMS.map((item) => item?.category)).required(),
  params: yup.object().shape({
    ...paginationSchema, // 페이지네이션
  }),
})
export const getFaqListMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getFaqListFetch(param, isMock, env, isSSG))
}
export const getFaqListFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getFaqListSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_FAQ
    } else {
      return MOCK_FAQ2
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.SETTING.FAQ(env, param), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.content
}

// 이용안내/FAQ 상세
const getFaqDetailSchema = yup.object().shape({
  key: yup.number().required(), // FAQ 키
})
export const getFaqDetail = (param, isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useQuery(["getFaqDetail", param], () => getFaqDetailFetch(param, isMock, env, isSSG), {...queryOption})
}
export const getFaqDetailFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getFaqDetailSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_FAQ_DETAIL
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.SETTING.FAQ_DETAIL(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

// 이용약관 모두 조회
export const getTermsOfUse = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getTermsOfUse"], () => getTermsOfUseFetch(isMock, env, isAuth, isSSG), {...queryOption})
}
export const getTermsOfUseFetch = async (isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return MOCK_TERMS_OF_USE
  }
  return await authProvider(isSSG).get(PATH_DONOTS_API.SETTING.TERMS_OF_USE(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 이용약관 개별 조회 : key
const getTermsOfUseByKeySchema = yup.object().shape({
  key: yup.number().required(),
})
export const getTermsOfUseByKeyMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getTermsOfUseByKeyFetch(param, isMock, env, isSSG))
}
export const getTermsOfUseByKeyFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getTermsOfUseByKeySchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_TERMS_OF_USE_KEY
  }
  return await noAuthProvider(isSSG).get(PATH_DONOTS_API.SETTING.TERMS_OF_USE_KEY(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 이용약관 개별 조회 : title
const getTermsOfUseByTitleSchema = yup.object().shape({
  title: yup.string().required(),
})
export const getTermsOfUseByTitle = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getTermsOfUseByTitle", param], () => getTermsOfUseByTitleFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}
export const getTermsOfUseByTitleMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getTermsOfUseByTitleFetch(param, isMock, env, isSSG))
}
export const getTermsOfUseByTitleFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (isMock) {
    return MOCK_TERMS_OF_USE_TITLE
  }
  if (!await isValidateParam(param, getTermsOfUseByTitleSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  return await noAuthProvider(isSSG).get(PATH_DONOTS_API.SETTING.TERMS_OF_USE_TITLE(env, param)).then((data) => {
    return data
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 마케팅/개인정보 동의 조회
export const getAgreeMarketingMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation(() => getAgreeMarketingFetch(isMock, env, isAuth, isSSG))
}
export const getAgreeMarketingFetch = async (isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return MOCK_GET_AGREE_MKT
  }
  return await authProvider(isSSG).get(PATH_DONOTS_API.SETTING.AGREE_MKT(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 마케팅/개인정보 동의 수정
const putAgreeMarketingSchema = yup.object().shape({
  isTermsCollectingPersonalDataMarketingAgreed: yup.boolean(),
  isTextMessageReciveAgreed: yup.boolean(),
  isEmailReceiveAgreed: yup.boolean(),
})
export const putAgreeMarketingMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => putAgreeMarketingFetch(param, isMock, env, isAuth, isSSG))
}
export const putAgreeMarketingFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, putAgreeMarketingSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_PUT_AGREE_MKT
  }
  return await authProvider(isSSG).put(PATH_DONOTS_API.SETTING.AGREE_MKT(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 마케팅 PUSH 동의 조회
export const getPushMarketingMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getPushMarketingFetch(param, isMock, env, isAuth, isSSG))
}
export const getPushMarketingFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return MOCK_GET_PUSH_MKT
  }
  return await authProvider(isSSG).get(PATH_DONOTS_API.SETTING.PUSH_MKT(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 마케팅 PUSH 동의 수정
const putPushMarketingSchema = yup.object().shape({
  isTermsCollectingPersonalDataMarketingAgreed: yup.boolean(),
})
export const putPushMarketingMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => putPushMarketingFetch(param, isMock, env, isAuth, isSSG))
}
export const putPushMarketingFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, putPushMarketingSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_PUT_PUSH_MKT
  }
  return await authProvider(isSSG).put(PATH_DONOTS_API.SETTING.PUSH_MKT(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 콘텐츠 PUSH 동의 조회
export const getPushContentsMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getPushContentsFetch(param, isMock, env, isAuth, isSSG))
}
export const getPushContentsFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return MOCK_GET_PUSH_CONTENTS
  }
  return await authProvider(isSSG).get(PATH_DONOTS_API.SETTING.PUSH_CONTENTS(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 콘텐츠 PUSH 동의 수정
const putPushContentsSchema = yup.object().shape({
  isPostCensorshipResultPushNotifSet: yup.boolean(),
})
export const putPushContentsMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => putPushContentsFetch(param, isMock, env, isAuth, isSSG))
}
export const putPushContentsFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, putPushContentsSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_PUT_PUSH_CONTENTS
  }
  return await authProvider(isSSG).put(PATH_DONOTS_API.SETTING.PUSH_CONTENTS(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 1대1 문의
// isValidateParam 추가해야함
const getOneOnOneInquiriesSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema,              // 페이지네이션
  }),
})
export const getOneOnOneInquiriesMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getOneOnOneInquiriesMutateFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}
export const getOneOnOneInquiriesMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, getOneOnOneInquiriesSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_ONE_ON_ONE_INQUIRIES?.oneOnOneInquiryPostDetailsResponseList
    } else {
      return MOCK_ONE_ON_ONE_INQUIRIES2?.oneOnOneInquiryPostDetailsResponseList
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.SETTING.ONE_ON_ONE_INQUIRIES(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.content
}

const getOneOnOneInquiryDetailSchema = yup.object().shape({
  key: yup.number().required(), // 문의키
})
export const getOneOnOneInquiryDetail = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getOneOnOneInquiryDetail", param], () => getOneOnOneInquiryDetailFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}
export const getOneOnOneInquiryDetailFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, getOneOnOneInquiryDetailSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_ONE_ON_ONE_INQUIRY_DETAIL
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.SETTING.ONE_ON_ONE_INQUIRY_DETAIL(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

const delOneOnOneInquiryMutateSchema = yup.object().shape({
  key: yup.number().required(), // 문의키
})
export const delOneOnOneInquiryMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => delOneOnOneInquiryMutateFetch(param, isMock, env, isAuth, isSSG))
}
export const delOneOnOneInquiryMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, delOneOnOneInquiryMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).delete(PATH_DONOTS_API.SETTING.DELETE_ONE_ON_ONE_INQUIRY(env, param), {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

export const postOneOnOneInquiryMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postOneOnOneInquiryMutateFetch(param, isMock, env, isAuth, isSSG))
}
export const postOneOnOneInquiryMutateFetch = async (formData, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).post(PATH_DONOTS_API.SETTING.POST_ONE_ON_ONE_INQUIRY(env), formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}