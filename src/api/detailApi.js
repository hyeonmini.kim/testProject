import {PATH_DONOTS_API} from "../routes/paths";
import {useMutation, useQuery} from "react-query";
import {getEnv, isAuthApi, isMockApi} from "../utils/envUtils";
import {ENV, IS_MOCK} from "../config";
import {authProvider, getProvider, mutationOption, queryOption} from "../layouts/common/AxiosProvider";
import {ERRORS, NEW_CUSTOM_ERROR} from "../constant/common/Error";
import * as yup from "yup";
import {isValidateParam} from "../utils/validateUtils";

// 레시피 상세 조회
const getRecipeDetailSchema = yup.object().shape({
  recipe_key: yup.string().required(), // 레시피 키
})
export const getRecipeDetail = (param, env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getRecipeDetail", param?.recipe_key], () => getRecipeDetailFetch(param, env, isAuth, isSSG), {
    ...queryOption,
    keepPreviousData: true
  })
}

export const getRecipeDetailWithEnabled = (param, enabled = true, env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getRecipeDetail", param?.recipe_key], () => getRecipeDetailFetch(param, env, isAuth, isSSG), {
    ...queryOption,
    keepPreviousData: true,
    enabled: !!enabled
  })
}

export const getRecipeDetailMutate = (env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getRecipeDetailFetch(param, env, isAuth, isSSG), {...mutationOption})
}
export const getRecipeDetailFetch = async (param, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, getRecipeDetailSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  const data = await getProvider(isSSG, isAuth).get(PATH_DONOTS_API.DETAIL.RECIPE_DETAIL(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.recipe_detail_info
}

const postRegistRecipeScrapSchema = yup.object().shape({
  recipe_key: yup.string().required(), // 레시피 키
  recipe_scrap_yn: yup.string().required()  // 레시피 스크랩 on/off 여부
})
export const postRegistRecipeScrapMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postRegistRecipeScrapMutateFetch(param, isMock, env, isAuth, isSSG))
}
export const postRegistRecipeScrapMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, postRegistRecipeScrapSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return
  }
  const data = await authProvider(isSSG).post(PATH_DONOTS_API.DETAIL.REGIST_RECIPE_SCRAP(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

const postRegistRecipeReviewSchema = yup.object().shape({
  recipe_key: yup.string().required(), // 레시피 키
  recipe_select_review: yup.string().required(),  // 레시피 선택 리뷰
  recipe_review_yn: yup.string().required()  // 레시피 리뷰 on/off 여부
})
export const postRegistRecipeReviewMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postRegistRecipeReviewMutateFetch(param, isMock, env, isAuth, isSSG))
}
export const postRegistRecipeReviewMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, postRegistRecipeReviewSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return
  }
  const data = await authProvider(isSSG).post(PATH_DONOTS_API.DETAIL.REGIST_RECIPE_REVIEW(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.recipe_review_cnt_info
}