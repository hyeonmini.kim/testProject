import {PATH_DONOTS_API} from "../routes/paths";
import {MOCK_GET_MY_INFO, MOCK_GET_MY_INFO2, MOCK_GET_RECIPE_MY_SELF, MOCK_GET_RECIPE_MY_SELF2, MOCK_SNS_LIST} from "../mock/user";
import {getEnv, isAuthApi, isMockApi} from "../utils/envUtils";
import {useMutation, useQuery} from "react-query";
import {ENV, IS_MOCK} from "../config";
import {authProvider, mutationOption, noAuthProvider, queryOption} from "../layouts/common/AxiosProvider";
import {ERRORS, NEW_CUSTOM_ERROR} from "../constant/common/Error";
import * as yup from "yup";
import {isValidateParam, paginationSchema} from "../utils/validateUtils";
import {MY_TABS, RECIPE_CHECK_STATUS} from "../constant/my/MyConstants";
import {isMock} from "../recoil/atom/common/version";

// 유저 정보 조회
export const getMyInfo = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getMyInfo"], () => getMyInfoFetch(isMock, env, isAuth, isSSG), {...queryOption})
}
export const getMyInfoFetch = async (isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_GET_MY_INFO
    } else {
      return MOCK_GET_MY_INFO2
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.MY.MY_INFO(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

const getOthersInfoSchema = yup.object().shape({
  key: yup.number().required(), // 유저키
})
export const getOthersInfo = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getOthersInfo", param], () => getOthersInfoFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}
export const getOthersInfoFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, getOthersInfoSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_GET_MY_INFO
    } else {
      return MOCK_GET_MY_INFO2
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.MY.OTHERS_INFO(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

const getSearchMyRecipesSchema = yup.object().shape({
  params: yup.object().shape({
    recipe_check_status: yup.mixed().oneOf(RECIPE_CHECK_STATUS.map((item) => item)),  // 레시피 상태 : 전체, 임시저장, 검수중, 반려, 게시중
  }),
})
export const getSearchMyRecipes = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getSearchMyRecipes", param], () => getSearchMyRecipesFetch(param, isMock, env, isAuth, isSSG), {
    ...queryOption,
    keepPreviousData: true
  })
}
export const getSearchMyRecipesFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_GET_RECIPE_MY_SELF
    } else {
      return MOCK_GET_RECIPE_MY_SELF2
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.MY.SEARCH_USER_RECIPE(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.user_recipe_obj
}

const getSearchOthersRecipesSchema = yup.object().shape({
  params: yup.object().shape({
    user_key: yup.number() // user key 값
  }),
})
export const getSearchOthersRecipes = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getSearchOthersRecipes", param], () => getSearchOthersRecipesFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}
export const getSearchOthersRecipesFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, getSearchOthersRecipesSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_GET_RECIPE_MY_SELF
    } else {
      return MOCK_GET_RECIPE_MY_SELF2
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.MY.SEARCH_USER_RECIPE(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.user_recipe_obj
}

const getSearchMyWriteRecipesMutateSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema, // 페이지네이션
    page: yup.number().moreThan(-2).required(), // 페이지네이션 -1 허용
    recipe_check_status: yup.mixed().oneOf(RECIPE_CHECK_STATUS.map((item) => item)),  // 레시피 상태 : 전체, 임시저장, 검수중, 반려, 게시중
    paging_type: yup.mixed().oneOf(MY_TABS.map((item) => item?.value))  // 페이징 타입 : write, scrap
  }),
})
// 유저 레시피 목록 조회
export const getSearchMyWriteRecipesMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getSearchMyWriteRecipesMutateFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const getSearchMyWriteRecipesMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, getSearchMyWriteRecipesMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_GET_RECIPE_MY_SELF
    } else {
      return MOCK_GET_RECIPE_MY_SELF2
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.MY.SEARCH_USER_RECIPE(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.user_recipe_obj?.recipe_write_list
}

const getSearchMyScrapRecipesMutateSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema, // 페이지네이션
    page: yup.number().moreThan(-2).required(), // 페이지네이션 -1 허용
    recipe_check_status: yup.mixed().oneOf(RECIPE_CHECK_STATUS.map((item) => item)),  // 레시피 상태 : 전체, 임시저장, 검수중, 반려, 게시중
    paging_type: yup.mixed().oneOf(MY_TABS.map((item) => item?.value))  // 페이징 타입 : write, scrap
  }),
})

export const getSearchMyScrapRecipesMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getSearchMyScrapRecipesMutateFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const getSearchMyScrapRecipesMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, getSearchMyScrapRecipesMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_GET_RECIPE_MY_SELF
    } else {
      return MOCK_GET_RECIPE_MY_SELF2
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.MY.SEARCH_USER_RECIPE(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.user_recipe_obj?.recipe_scrap_list
}

const getSearchOthersWriteRecipesMutateSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema, // 페이지네이션
    page: yup.number().moreThan(-2).required(), // 페이지네이션 -1 허용
    user_key: yup.number(), // user key
    paging_type: yup.mixed().oneOf(MY_TABS.map((item) => item?.value))  // 페이징 타입 : write, scrap
  }),
})
export const getSearchOthersWriteRecipesMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getSearchOthersWriteRecipesMutateFetch(param, isMock, env, isAuth, isSSG))
}
export const getSearchOthersWriteRecipesMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, getSearchOthersWriteRecipesMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_GET_RECIPE_MY_SELF
    } else {
      return MOCK_GET_RECIPE_MY_SELF2
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.MY.SEARCH_USER_RECIPE(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.user_recipe_obj?.recipe_write_list
}

const getSearchOthersScrapRecipesMutateSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema, // 페이지네이션
    page: yup.number().moreThan(-2).required(), // 페이지네이션 -1 허용
    user_key: yup.number(), // user key
    paging_type: yup.mixed().oneOf(MY_TABS.map((item) => item?.value))  // 페이징 타입 : write, scrap
  }),
})
export const getSearchOthersScrapRecipesMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getSearchOthersScrapRecipesMutateFetch(param, isMock, env, isAuth, isSSG))
}
export const getSearchOthersScrapRecipesMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, getSearchOthersScrapRecipesMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_GET_RECIPE_MY_SELF
    } else {
      return MOCK_GET_RECIPE_MY_SELF2
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.MY.SEARCH_USER_RECIPE(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.user_recipe_obj?.recipe_scrap_list
}

const patchMyDetailsMutateSchema = yup.object().shape({
  nickname: yup.string(), // 닉네임
  email: yup.string(),  // 이메일
  phoneNumber: yup.string(), // 전화번호
  briefBio: yup.string(), // 한줄소개
  socialMediaUrl: yup.string(), // SNS 링크
})
// 나의 정보 상세 수정
export const patchMyDetailsMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => patchMyDetailsMutateFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const patchMyDetailsMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, patchMyDetailsMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  const data = await authProvider(isSSG).patch(PATH_DONOTS_API.MY.PATCH_MY_DETAIL_INFO(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return
}

export const putMyProfilePictureMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => putMyProfilePictureMutateFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const putMyProfilePictureMutateFetch = async (formData, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).put(PATH_DONOTS_API.MY.MODIFY_MY_PROFILE_PICTURE(env), formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

export const delMyProfilePictureMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation(() => delMyProfilePictureMutateFetch(isMock, env, isAuth, isSSG), {...mutationOption})
}
export const delMyProfilePictureMutateFetch = async (isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).delete(PATH_DONOTS_API.MY.MODIFY_MY_PROFILE_PICTURE(env), {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

const patchMyChildrenDetailsMutateSchema = yup.object().shape({
  key: yup.number().required(), // 유저키
  nickname: yup.string(), // 닉네임
  birthdate: yup.string(),  // 생년월일
  height: yup.string(),  // 키
  weight: yup.string(),  // 몸무게
  gender: yup.string(),  // 성별
})
export const patchMyChildrenDetailsMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => patchMyChildrenDetailsMutateFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const patchMyChildrenDetailsMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, patchMyChildrenDetailsMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  const data = await authProvider(isSSG).patch(PATH_DONOTS_API.MY.PATCH_CHILDREN_DETAIL_INFO(env, param), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return
}

const putMyChildrenProfilePictureMutateSchema = yup.object().shape({
  key: yup.number().required(), // 유저키
})
export const putMyChildrenProfilePictureMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => putMyChildrenProfilePictureMutateFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const putMyChildrenProfilePictureMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, putMyChildrenProfilePictureMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).put(PATH_DONOTS_API.MY.MODIFY_CHILDREN_PROFILE_PICTURE(env, param), param?.formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

const delMyChildrenProfilePictureMutateSchema = yup.object().shape({
  key: yup.number().required(), // 유저키
})
export const delMyChildrenProfilePictureMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => delMyChildrenProfilePictureMutateFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const delMyChildrenProfilePictureMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, delMyChildrenProfilePictureMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).delete(PATH_DONOTS_API.MY.MODIFY_CHILDREN_PROFILE_PICTURE(env, param), {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

const putMyChildrenAllergyIngredientsMutateSchema = yup.object().shape({
  key: yup.number().required(), // 유저키
  bodyArray: yup.array()  // allergy list
})
export const putMyChildrenAllergyIngredientsMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => putMyChildrenAllergyIngredientsMutateFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const putMyChildrenAllergyIngredientsMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, putMyChildrenAllergyIngredientsMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  const data = await authProvider(isSSG).put(PATH_DONOTS_API.MY.PATCH_CHILDREN_ALLERGY_INGREDIENTS_INFO(env, param), param?.bodyArray).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return
}

const putMyChildrenConcernsMutateSchema = yup.object().shape({
  key: yup.number().required(), // 유저키
  bodyArray: yup.array()  // 관심사 list
})
export const putMyChildrenConcernsMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => putMyChildrenConcernsMutateFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const putMyChildrenConcernsMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, putMyChildrenConcernsMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  const data = await authProvider(isSSG).put(PATH_DONOTS_API.MY.PATCH_CHILDREN_CONCERNS_INFO(env, param), param?.bodyArray).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return
}

const postMyChildrenRearrangeMutateSchema = yup.object().shape({
  profileSelectedBabyKey: yup.number(), // 대표아이키
  babies: yup.array()  // 아이 list
})
export const postMyChildrenRearrangeMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postMyChildrenRearrangeMutateFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const postMyChildrenRearrangeMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, postMyChildrenRearrangeMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  const data = await authProvider(isSSG).post(PATH_DONOTS_API.MY.POST_CHILDREN_REARRANGE(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return
}

const postMyChildrenRemoveMutateSchema = yup.object().shape({
  key: yup.number().required(), // 유저키
})
export const postMyChildrenRemoveMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postMyChildrenRemoveMutateFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const postMyChildrenRemoveMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, postMyChildrenRemoveMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  const data = await authProvider(isSSG).post(PATH_DONOTS_API.MY.POST_CHILDREN_REMOVE(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return
}

// 나의 SNS 계정 연동 리스트 조회
export const getMySnsList = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = true) => {
  return useQuery(["getMySnsList"], () => getMySnsListFetch(isMock, env, isAuth, isSSG), {...queryOption})
}
export const getMySnsListFetch = async (isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return MOCK_SNS_LIST
  }
  const data = await authProvider(false).get(PATH_DONOTS_API.MY.SNS_LIST(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.mySocialAccountResponseList
}

const postAddSnsAccountInMyMutateSchema = yup.object().shape({
  ciValue: yup.object(),  // ci 값
  token: yup.string() // token 값
})
// SNS 계정 추가하기 (토큰 직접 입력)
export const postAddSnsAccountInMyMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postAddSnsAccountInMyFetch(param, isMock, env, isSSG), {...mutationOption})
}
export const postAddSnsAccountInMyFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postAddSnsAccountInMyMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  return await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.ADD_SNS_ACCOUNT(env), param?.ciValue, {
    headers: {
      Authorization: `Bearer ${param?.token}`
    }
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

const deleteRemoveSnsAccountMutateSchema = yup.object().shape({
  socialAccountConnectionStatusKey: yup.string() // token 값
})
// SNS 연동계정 해제
export const deleteRemoveSnsAccountMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => deleteRemoveSnsAccountFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const deleteRemoveSnsAccountFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, deleteRemoveSnsAccountMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  return await authProvider(isSSG).delete(PATH_DONOTS_API.MY.DELETE_SNS_ACCOUNT(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

const postChangePasswordMutateSchema = yup.object().shape({
  id: yup.string(), // id
  password: yup.string(), // password
  newPassword: yup.string() // newPassword
})
// 비밀번호 변경
export const postChangePasswordMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postChangePasswordFetch(param, isMock, env, isSSG))
}
export const postChangePasswordFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  return await noAuthProvider(isSSG).post(PATH_DONOTS_API.MY.CHANGE_PASSWORD(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [
      ERRORS.PASSWORD_NOT_MATCH,
      ERRORS.PASSWORD_INCLUDE_PERSONAL_INFORMATION
    ])
  })
}

const deleteWithdrawAccountMutateSchema = yup.object().shape({
  accountKey: yup.string(), // accountKey
  body: yup.object()  // leaveType, etcReason
})
// 회원 탈퇴
export const deleteWithdrawAccountMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => deleteWithdrawAccountFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const deleteWithdrawAccountFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, deleteWithdrawAccountMutateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).delete(PATH_DONOTS_API.MY.DELETE_ACCOUNT(env, param), {data: param?.body}).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 등급 정보 조회
export const getGradeInformation = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getGradeInformation"], () => getGradeInformationFetch(isMock, env, isAuth, isSSG), {...queryOption})
}
export const getGradeInformationFetch = async (isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.MY.CHECK_GRADE(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.parent_grade_info
}
