import {getEnv, isAuthApi, isMockApi} from "../utils/envUtils";
import {PATH_DONOTS_API} from "../routes/paths";
import {useMutation, useQuery} from "react-query";
import {ENV, IS_MOCK} from "../config";
import {
  MOCK_ADD_SNS,
  MOCK_BY_EMAIL_AND_IDENTIFY_VERIFICATION_QAS,
  MOCK_BY_EMAIL_IDENTIFY_VERIFICATION_QUESTIONS,
  MOCK_BY_ID_IDENTIFY_VERIFICATION_QUESTIONS,
  MOCK_CHECK_CI_ID,
  MOCK_CHECK_ID_CI,
  MOCK_CHECK_SIGN_UP_NO,
  MOCK_EMAIL,
  MOCK_EXIST_ID,
  MOCK_GET_MEMBER,
  MOCK_ID,
  MOCK_IS_ACCOUNT,
  MOCK_NICKNAME,
  MOCK_REQUEST_CERT,
  MOCK_RESET_PASSWORD,
  MOCK_SIGN_IN_ID,
  MOCK_SIGN_UP_ID,
  MOCK_SIGN_UP_SNS,
  MOCK_TOKEN,
  MOCK_USER_DETAIL,
  MOCK_UUID
} from "../mock/auth";
import {ERRORS, NEW_CUSTOM_ERROR} from "../constant/common/Error";
import {authProvider, noAuthProvider, queryOption} from "../layouts/common/AxiosProvider";
import * as yup from "yup";
import {isValidateParam} from "../utils/validateUtils";
import base64 from "base-64";

// uuid 발급
export const postGetUuidMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation(() => postGetUuidFetch(isMock, env, isSSG))
}
export const postGetUuidFetch = async (isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (isMock) {
    return MOCK_UUID
  }
  const data = await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.GET_UUID(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.uuid
}

// uuid로 토큰 발급
const postGetTokensSchema = yup.object().shape({
  uuid: yup.string()
})
export const postGetTokensMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postGetTokensFetch(param, isMock, env, isSSG))
}
export const postGetTokensFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postGetTokensSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_TOKEN
  }
  const data = await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.GET_TOKENS(env), {
    uuid: param?.uuid
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

// ID/PW 로그인
const postSignInByIdSchema = yup.object().shape({
  id: yup.string(),
  password: yup.string(),
})
export const postSignInByIdMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postSignInByIdFetch(param, isMock, env, isSSG))
}
export const postSignInByIdFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postSignInByIdSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_SIGN_IN_ID
  }
  return await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.SIGN_IN_ID(env), param).then((data) => {
    return data?.databody
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [
      ERRORS.PASSWORD_INCORRECT_ONCE,
      ERRORS.PASSWORD_INCORRECT_TWICE,
      ERRORS.PASSWORD_INCORRECT_THREE_TIMES,
      ERRORS.PASSWORD_INCORRECT_FOUR_TIMES,
      ERRORS.ACCOUNT_BLOCKED,
      ERRORS.ACCOUNT_NOT_FOUND,
    ])
  })
}

// 유저 프로파일 가져오기
export const getMember = (param, isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useQuery(["getMember"], () => getMemberFetch(param, isMock, env, isSSG), {...queryOption})
}
export const getMemberMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getMemberFetch(param, isMock, env, isSSG))
}
export const getMemberFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (isMock) {
    return MOCK_GET_MEMBER
  }

  // 토큰 저장 전에 해당 API를 태워야하기 때문에 직접 토큰을 넣어주어 동작시킴.
  let data = ''
  if (param?.token === '') {
    data = await authProvider(isSSG).get(PATH_DONOTS_API.AUTH.GET_MEMBER(env)).catch((error) => {
      throw NEW_CUSTOM_ERROR(error, [])
    })
  } else {
    data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.AUTH.GET_MEMBER(env), {
      headers: {
        Authorization: `Bearer ${param?.token}`
      }
    }).catch((error) => {
      throw NEW_CUSTOM_ERROR(error, [
        ERRORS.INVALID_ACCESS_TOKEN,
      ])
    })
  }
  return data?.databody
}

// 유저 정보 조회 (add token)
export const getMyInformAddTokenMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getMyInformAddTokenFetch(param, isMock, env, isAuth, isSSG))
}
export const getMyInformAddTokenFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!param) return
  if (isMock) {
    return MOCK_USER_DETAIL
  }

  let data = ''
  if (param?.token === '') {
    data = await authProvider(isSSG).get(PATH_DONOTS_API.MY.MY_INFO(env)).catch((error) => {
      throw NEW_CUSTOM_ERROR(error, [])
    })
  } else {
    data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.MY.MY_INFO(env), {
      headers: {
        Authorization: `Bearer ${param?.token}`
      }
    }).catch((error) => {
      throw NEW_CUSTOM_ERROR(error, [])
    })
  }

  return data?.databody
}

// 유효한 토큰 다시 발급하기 (app)
export const getRefreshTokenMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getRefreshTokenFetch(param, isMock, env, isSSG))
}
export const getRefreshTokenFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (isMock) {
    return MOCK_TOKEN
  }
  const data = await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.REFRESH_TOKEN(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

// 유효한 토큰 다시 발급하기 (web)
export const getRefreshTokenMutateByCookie = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getRefreshTokenMutateByCookieFetch(param, isMock, env, isSSG))
}
export const getRefreshTokenMutateByCookieFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (isMock) {
    return MOCK_TOKEN
  }
  const data = await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.REFRESH_TOKEN_COOKIE(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

// 인증번호 요청
const postAuthCodeSchema = yup.object().shape({
  name: yup.string(),
  phoneNumber: yup.string(),
  birthday: yup.string(),
  gender: yup.string(),
  koreaForeignerType: yup.string(),
  newsAgencyType: yup.string(),
})
export const postAuthCodeMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postAuthCodeFetch(param, isMock, env, isSSG))
}
export const postAuthCodeFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postAuthCodeSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_REQUEST_CERT
  }
  return await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.GET_AUTH_CODE(env), param)
    .then((data) => {
      return data?.databody
    })
    .catch((error) => {
      console.log("## smsSend error", error)
      throw NEW_CUSTOM_ERROR(error, [
        ERRORS.INVALID_NAME_OR_RESIDENT_REGISTRATION_NUMBER,
        ERRORS.SERVICE_UNAVAILABLE,
        ERRORS.MISCELLANEOUS,
      ])
    })
}

// 인증번호 확인하고 가입 정보 체크
const getCheckAuthSchema = yup.object().shape({
  joiningAccountType: yup.string(),
  param: yup.object().shape({
    phoneNumber: yup.string(),
    transactionSeqNumber: yup.string(),
    authCodeOf6Digits: yup.string(),
    socialAccountId: yup.string().nullable(),
  }),
})
export const getCheckAuthMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((params) => getCheckAuthFetch(params, isMock, env, isSSG))
}
export const getCheckAuthFetch = async (params, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(params, getCheckAuthSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_CHECK_SIGN_UP_NO
  }

  const userCi = await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.CHECK_AUTH_CODE(env, params), params.param)
    .then((data) => {
      return data?.databody?.ci
    })
    .catch((error) => {
      throw NEW_CUSTOM_ERROR(error, [
        ERRORS.SERVICE_UNAVAILABLE,
        ERRORS.INCORRECT_AUTH_CODE_ATTEMPTS_EXCEEDED,
        ERRORS.AUTH_CODE_NOT_MATCHED,
        ERRORS.AUTH_CODE_RETRANSMISSION_REQUEST_TIMEOUT,
        ERRORS.MISCELLANEOUS,
      ])
    })

  if (!userCi) return false

  return await noAuthProvider(isSSG).get(PATH_DONOTS_API.AUTH.FIND_ID(env), {
    params: {
      ci: base64.encode(userCi)
    }
  })
    .then((data) => {
      return {isExist: true, ci: userCi, data: data?.databody}
    })
    .catch((error) => {
      const code = error?.response?.data?.code
      if (code === ERRORS.ACCOUNT_NOT_FOUND) {
        return {isExist: false, ci: userCi}
      }
      throw NEW_CUSTOM_ERROR(error, [])
    })
}

// 인증번호 확인하고 ID와 가입 정보 체크
const postCheckIdAndAuthSchema = yup.object().shape({
  joiningAccountType: yup.string(),
  param: yup.object().shape({
    phoneNumber: yup.string(),
    transactionSeqNumber: yup.string(),
    authCodeOf6Digits: yup.string(),
  }),
  id: yup.string(),
})
export const postCheckIdAndAuthMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((params) => postCheckIdAndAuthFetch(params, isMock, env, isSSG))
}
export const postCheckIdAndAuthFetch = async (params, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(params, postCheckIdAndAuthSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_CHECK_CI_ID
  }

  const userCi = await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.CHECK_AUTH_CODE(env, params), params.param)
    .then((data) => {
      return data?.databody?.ci
    })
    .catch((error) => {
      throw NEW_CUSTOM_ERROR(error, [
        ERRORS.SERVICE_UNAVAILABLE,
        ERRORS.INCORRECT_AUTH_CODE_ATTEMPTS_EXCEEDED,
        ERRORS.AUTH_CODE_NOT_MATCHED,
        ERRORS.AUTH_CODE_RETRANSMISSION_REQUEST_TIMEOUT,
        ERRORS.MISCELLANEOUS,
      ])
    })

  if (!userCi) return false

  return await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.CHECK_ID_CI(env), {
    ci: userCi,
    id: params.id
  })
    .then((data) => {
      return {ci: userCi, isMatched: data?.databody?.ciAndIdVerification}
    })
    .catch((error) => {
      throw NEW_CUSTOM_ERROR(error, [])
    })
}

// CI로 가입 여부 체크
const getIsExistAccountSchema = yup.object().shape({
  ci: yup.string()
})
export const getIsExistAccountMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getIsExistAccountFetch(param, isMock, env, isSSG))
}
export const getIsExistAccountFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getIsExistAccountSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_IS_ACCOUNT
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.AUTH.IS_EXIST_ACCOUNT(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.ciVerfication
}

// ID와 CI로 입력정보 일치 체크
const postCheckIdAndCiSchema = yup.object().shape({
  ci: yup.string(),
  id: yup.string()
})
export const postCheckIdAndCiMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postCheckIdAndCiFetch(param, isMock, env, isSSG))
}
export const postCheckIdAndCiFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postCheckIdAndCiSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_CHECK_ID_CI
  }
  const data = await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.CHECK_ID_CI(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.ciAndIdVerification
}

// 아이디 존재 여부 체크
const getCheckExistIdSchema = yup.object().shape({
  id: yup.string()
})
export const getCheckExistIdMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getCheckExistIdFetch(param, isMock, env, isSSG))
}
export const getCheckExistIdFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getCheckExistIdSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_EXIST_ID
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.AUTH.CHECK_EXIST_ID(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.idVerification
}

// 아이디 중복 체크
const getIsDuplicateIdSchema = yup.object().shape({
  id: yup.string()
})
export const getIsDuplicateIdMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getIsDuplicateIdFetch(param, isMock, env, isSSG))
}
export const getIsDuplicateIdFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getIsDuplicateIdSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_ID
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.AUTH.IS_DUPLICATE_ID(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.idDuplication
}

// 비밀번호 재설정
const postResetPasswordSchema = yup.object().shape({
  ci: yup.string(),
  id: yup.string(),
  newPassword: yup.string()
})
export const postResetPasswordMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postResetPasswordFetch(param, isMock, env, isSSG))
}
export const postResetPasswordFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postResetPasswordSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_RESET_PASSWORD
  }
  const data = await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.RESET_PASSWORD(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [ERRORS.PASSWORD_INCLUDE_PERSONAL_INFORMATION])
  })
  return data
}

// 닉네임 중복 체크
const postIsDuplicateNicknameSchema = yup.object().shape({
  nickname: yup.string()
})
export const postIsDuplicateNicknameMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postIsDuplicateNicknameFetch(param, isMock, env, isSSG))
}
export const postIsDuplicateNicknameFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postIsDuplicateNicknameSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_NICKNAME
  }
  const data = await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.IS_DUPLICATE_NICKNAME(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.isNicknameDuplicate
}

// 이메일 중복 체크
const postIsDuplicateEmailSchema = yup.object().shape({
  email: yup.string()
})
export const postIsDuplicateEmailMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postIsDuplicateEmailFetch(param, isMock, env, isSSG))
}
export const postIsDuplicateEmailFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postIsDuplicateEmailSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_EMAIL
  }
  const data = await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.IS_DUPLICATE_EMAIL(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.isEmailDuplicate
}

// ID 생성 회원가입
const postSignUpIdSchema = yup.object().shape({
  uuid: yup.string(),
  ci: yup.string(),
  name: yup.string(),
  phoneNumber: yup.string(),
  birthDay: yup.string(),
  gender: yup.string(),
  id: yup.string(),
  password: yup.string(),
  email: yup.string(),
  nickname: yup.string(),
  babies: yup.array(),
  isTermsCollectingPersonalDataMarketingAgreed: yup.boolean(),
  isTextMessageReciveAgreed: yup.boolean(),
  isEmailReceiveAgreed: yup.boolean(),
  isMarketingInfoPushNotifSet: yup.boolean()
})
export const postSignUpIdMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postSignUpIdFetch(param, isMock, env, isSSG))
}
export const postSignUpIdFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postSignUpIdSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_SIGN_UP_ID
  }
  const data = await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.SIGN_UP_ID(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

// SNS 계정 연동 회원가입
const postSignUpSnsSchema = yup.object().shape({
  ci: yup.string(),
  name: yup.string(),
  phoneNumber: yup.string(),
  birthDay: yup.string(),
  gender: yup.string(),
  email: yup.string(),
  nickname: yup.string(),
  babies: yup.array(),
  isTermsCollectingPersonalDataMarketingAgreed: yup.boolean(),
  isTextMessageReciveAgreed: yup.boolean(),
  isEmailReceiveAgreed: yup.boolean(),
  isMarketingInfoPushNotifSet: yup.boolean()
})
export const postSignUpSnsMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postSignUpSnsFetch(param, isMock, env, isAuth, isSSG))
}
export const postSignUpSnsFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, postSignUpSnsSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_SIGN_UP_SNS
  }
  const data = await authProvider(isSSG).post(PATH_DONOTS_API.AUTH.SIGN_UP_SNS(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

// SNS만 가입되어 있는데 ID로 회원가입하기
const postSignUpAddIdSchema = yup.object().shape({
  ci: yup.string(),
  id: yup.string(),
  password: yup.string()
})
export const postSignUpAddIdMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postSignUpAddIdFetch(param, isMock, env, isSSG))
}
export const postSignUpAddIdFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postSignUpAddIdSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_SIGN_UP_ID
  }
  const data = await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.SIGN_UP_ADD_ID(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [ERRORS.PASSWORD_INCLUDE_PERSONAL_INFORMATION])
  })
  return data?.databody
}

// SNS 계정 추가하기
const postAddSnsAccountSchema = yup.object().shape({
  ci: yup.string()
})
export const postAddSnsAccountMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postAddSnsAccountFetch(param, isMock, env, isAuth, isSSG))
}
export const postAddSnsAccountFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, postAddSnsAccountSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_ADD_SNS
  }
  return await authProvider(isSSG).post(PATH_DONOTS_API.AUTH.ADD_SNS_ACCOUNT(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

const postByEmailIdentityVerificationQuestionsSchema = yup.object().shape({
  email: yup.string(),
})
export const postMyEmailIdentityVerificationQuestionsMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postMyEmailIdentityVerificationQuestionsFetch(param, isMock, env, isSSG))
}
export const postMyEmailIdentityVerificationQuestionsFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postByEmailIdentityVerificationQuestionsSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_BY_EMAIL_IDENTIFY_VERIFICATION_QUESTIONS
  }
  return await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.BY_EMAIL_IDENTIFY_VERIFICATION_QUESTIONS(env), param).then((data) => {
    return data?.databody
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [
      ERRORS.DATA_NOT_FOUND,
    ])
  })
}

const postByIdIdentityVerificationQuestionsSchema = yup.object().shape({
  email: yup.string(),
})
export const postByIdIdentityVerificationQuestionsMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postByIdIdentityVerificationQuestionsFetch(param, isMock, env, isSSG))
}
export const postByIdIdentityVerificationQuestionsFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postByIdIdentityVerificationQuestionsSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_BY_ID_IDENTIFY_VERIFICATION_QUESTIONS
  }
  return await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.BY_ID_IDENTIFY_VERIFICATION_QUESTIONS(env), param).then((data) => {
    return data?.databody
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [
      ERRORS.DATA_NOT_FOUND,
    ])
  })
}

const postByEmailAndIdentityVerificationQasSchema = yup.object().shape({
  email: yup.string(),
  identityVerificationQas: yup.array()
})
export const postByEmailAndIdentityVerificationQasMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postByEmailAndIdentityVerificationQasFetch(param, isMock, env, isSSG))
}
export const postByEmailAndIdentityVerificationQasFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postByEmailAndIdentityVerificationQasSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return MOCK_BY_EMAIL_AND_IDENTIFY_VERIFICATION_QAS
  }
  return await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.BY_EMAIL_AND_IDENTIFY_VERIFICATION_QAS(env), param).then((data) => {
    return data?.databody
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [
      ERRORS.QA_NOT_MATCHED,
      ERRORS.REQUEST_BODY_IS_EMPTY
    ])
  })
}

const postVerifyIdentifyWithIdAndQasSchema = yup.object().shape({
  id: yup.string(),
  identityVerificationQas: yup.array()
})
export const postVerifyIdentifyWithIdAndQasMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postVerifyIdentifyWithIdAndQasFetch(param, isMock, env, isSSG))
}
export const postVerifyIdentifyWithIdAndQasFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postVerifyIdentifyWithIdAndQasSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return ''
  }
  return await noAuthProvider(isSSG).post(PATH_DONOTS_API.AUTH.VERIFY_IDENTIFY_WITH_ID_AND_QAS(env), param).then((data) => {
    return data?.databody
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [
      ERRORS.QA_NOT_MATCHED,
      ERRORS.REQUEST_BODY_IS_EMPTY
    ])
  })
}