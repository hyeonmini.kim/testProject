import {PATH_DONOTS_API} from "../routes/paths";
import {getEnv, isMockApi} from "../utils/envUtils";
import {useMutation, useQuery} from "react-query";
import {ENV, IS_MOCK} from "../config";
import {EXPERT_LIST, SPRINKLE_LIST} from "../mock/momstar";
import {noAuthProvider, queryOption} from "../layouts/common/AxiosProvider";
import {ERRORS, NEW_CUSTOM_ERROR} from "../constant/common/Error";
import * as yup from "yup";
import {isValidateParam, paginationSchema, slicePaginationByParam} from "../utils/validateUtils";

// 랭킹 리스트 - 스프링클
const getSprinkleListSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema // 페이지네이션
  }),
})
export const getSprinkleList = (param, isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useQuery(["getSprinkleList", param], () => getSprinkleListFetch(param, isMock, env, isSSG), {...queryOption})
}
export const getSprinkleListMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getSprinkleListFetch(param, isMock, env, isSSG))
}
export const getSprinkleListFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getSprinkleListSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return [...SPRINKLE_LIST]
    } else {
      return [...SPRINKLE_LIST]
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.RANKING.SPRINKLE(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return slicePaginationByParam(param, data?.databody?.content)
}

// 랭킹 리스트 - 전문가
const getExpertListSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema // 페이지네이션
  }),
})
export const getExpertList = (param, isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useQuery(["getExpertList", param], () => getExpertListFetch(param, isMock, env, isSSG), {...queryOption})
}
export const getExpertListMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getExpertListFetch(param, isMock, env, isSSG))
}
export const getExpertListFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getExpertListSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return [...EXPERT_LIST]
    } else {
      return [...EXPERT_LIST]
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.RANKING.EXPERT(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return slicePaginationByParam(param, data?.databody.content)
}