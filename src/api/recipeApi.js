import {getEnv, isAuthApi, isMockApi} from "../utils/envUtils";
import {useMutation, useQuery} from "react-query";
import {ENV, IS_MOCK} from "../config";
import {PATH_DONOTS_API} from "../routes/paths";
import {MOCK_MATERIAL, MOCK_MATERIAL_BASE_PEPPER, MOCK_MATERIAL_BASE_SOY, MOCK_TEMP_RECIPE_KEY} from "../mock/recipe";
import {MATERIAL_BASE_TYPE, MATERIAL_TYPE} from "../constant/recipe/Material";
import {authProvider, mutationOption, noAuthProvider, queryOption} from "../layouts/common/AxiosProvider";
import {ERRORS, NEW_CUSTOM_ERROR} from "../constant/common/Error";
import * as yup from "yup";
import {isValidateParam, paginationSchema} from "../utils/validateUtils";

// 모든 레시피 (SSG 생성용)
export const getAllRecipes = async (env = ENV, isSSG = true) => {
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.DETAIL.RECIPE_ALL(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.recipe_all_list
}

// 임시 레시피 체크
export const getTempRecipeKeyMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getTempRecipeKeyFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const getTempRecipeKeyFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return MOCK_TEMP_RECIPE_KEY
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.RECIPE.TEMP_CHECK(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [
      ERRORS.DATA_NOT_FOUND
    ])
  })
  return data?.databody?.recipe_key
}

// 임시 레시피 삭제
export const deleteTempRecipeKeyMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => deleteTempRecipeKeyFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const deleteTempRecipeKeyFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).delete(PATH_DONOTS_API.RECIPE.TEMP_DELETE(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [
      ERRORS.BABY_NOT_FOUND,
      ERRORS.ACCOUNT_NOT_FOUND,
    ])
  })
}

// 레시피 간장 베이스 재료 조회
export const getSoyBaseIngredient = (param, isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useQuery(["getSoyBaseIngredient"], () => getSoyBaseIngredientFetch(param, isMock, env, isSSG), {...queryOption})
}
export const getSoyBaseIngredientFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (isMock) {
    if (isSSG) {
      return MOCK_MATERIAL_BASE_PEPPER
    } else {
      return MOCK_MATERIAL_BASE_SOY
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.SEARCH.BASE_INGREDIENT(env, MATERIAL_BASE_TYPE.SOY)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  const ingredient = data?.databody?.recipe_base_ingredient_json
  return ingredient?.map((item) => {
    return {
      ...item,
      recipe_common_ingredient_key: item?.recipe_ingredient_key,
      recipe_ingredient_key: '',
      recipe_ingredient_category: MATERIAL_TYPE.SEASONING,
    }
  })
}

// 레시피 고추장 베이스 재료 조회
export const getPepperBaseIngredient = (param, isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useQuery(["getPepperBaseIngredient"], () => getPepperBaseIngredientFetch(param, isMock, env, isSSG), {...queryOption})
}
export const getPepperBaseIngredientFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (isMock) {
    if (isSSG) {
      return MOCK_MATERIAL_BASE_SOY
    } else {
      return MOCK_MATERIAL_BASE_PEPPER
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.SEARCH.BASE_INGREDIENT(env, MATERIAL_BASE_TYPE.PEPPER)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  const ingredient = data?.databody?.recipe_base_ingredient_json
  return ingredient?.map((item) => {
    return {
      ...item,
      recipe_common_ingredient_key: item?.recipe_ingredient_key,
      recipe_ingredient_key: '',
      recipe_ingredient_category: MATERIAL_TYPE.SEASONING,
    }
  })
}

// 재료 검색
const getIngredientSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema,              // 페이지네이션
    text: yup.string().required(),    // 검색어
  }),
})
export const getIngredientMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getIngredientFetch(param, isMock, env, isAuth, isSSG))
}
export const getIngredientFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, getIngredientSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_MATERIAL
    } else {
      return MOCK_MATERIAL
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.SEARCH.INGREDIENT(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  const ingredient = data?.databody?.recipe_ingredient_json
  return ingredient?.map((item) => {
    return {
      ...item,
      recipe_common_ingredient_key: item?.recipe_ingredient_key,
      recipe_ingredient_key: '',
      recipe_ingredient_category: param?.type
    }
  })
}

// 레시피 임시등록
const postTempRecipeSchema = yup.object().shape({
  recipe_key: yup.string(),             // 레시피 키
  recipe_user_key: yup.string(),        // 레시피 유저
  recipe_name: yup.string(),            // 레시피 명
  recipe_desc: yup.string(),            // 레시피 설명
  recipe_category: yup.string(),        // 레시피 카테고리
  recipe_lead_time: yup.string(),       // 레시피 소요시간
  recipe_level: yup.string(),           // 레시피 난이도
  recipe_babyfood_step: yup.string(),   // 레시피 월령
  recipe_health_note: yup.string(),     // 레시피 건강노트
  recipe_servings: yup.number(),        // 레시피 계량
  recipe_ingredient_list: yup.array(),  // 레시피 재료 리스트
  recipe_order_list: yup.array(),       // 레시피 순서 리스트
  recipe_tag_desc: yup.string(),        // 레시피 태그
})
export const postTempRecipeMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postTempRecipeFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const postTempRecipeFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, postTempRecipeSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return true
  }
  const data = await authProvider(isSSG).post(PATH_DONOTS_API.RECIPE.TEMP_CREATE(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [
      ERRORS.DATA_NOT_FOUND
    ])
  })

  return data?.databody?.recipe_key
}

// 레시피 업로드
const patchRecipeUploadSchema = yup.object().shape({
  recipe_key: yup.string().required(), // 레시피 키
})
export const patchRecipeUploadMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => patchRecipeUploadFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const patchRecipeUploadFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, patchRecipeUploadSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).patch(PATH_DONOTS_API.RECIPE.UPLOAD(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [
      ERRORS.INVALID_PARAMETER
    ])
  })
}

// 이미지 업로드
export const postImageUploadMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postImageUploadFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const postImageUploadFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).post(PATH_DONOTS_API.RECIPE.IMAGE_UPLOAD(env), param, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 레시피 상태 업데이트
const putRecipeStatusSchema = yup.object().shape({
  recipe_key: yup.string().required(), // 레시피 키
})
export const putRecipeStatusMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => putRecipeStatusFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const putRecipeStatusFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, putRecipeStatusSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).put(PATH_DONOTS_API.RECIPE.UPDATE_STATUS(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}