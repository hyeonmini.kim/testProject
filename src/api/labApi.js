import {getEnv, isAuthApi, isMockApi} from "../utils/envUtils";
import {useMutation, useQuery} from "react-query";
import {authProvider, mutationOption, queryOption} from "../layouts/common/AxiosProvider";
import {ENV, IS_MOCK} from "../config";
import {ERRORS, NEW_CUSTOM_ERROR} from "../constant/common/Error";
import {PATH_DONOTS_API} from "../routes/paths";
import {MOCK_SURVEY_LIST, MOCK_SURVEY_YN} from "../mock/lab";
import {isValidateParam, paginationSchema} from "../utils/validateUtils";
import * as yup from "yup";

export const getRegistSurveyYn = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getRegistSurveyYn"], () => getRegistSurveyYnFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}
export const getRegistSurveyYnFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return MOCK_SURVEY_YN
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.LAB.SURVEY_REGIST_YN(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.survey_yn
}

export const getSurveyList = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getSurveyList", param?.survey_type], () => getSurveyListFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}

export const getSurveyListFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return MOCK_SURVEY_LIST
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.LAB.SURVEY_LIST(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.brand_rank_list
}

const postSurveyRegistSchema = yup.object().shape({
  survey_list: yup.array(),             // 설문조사리스트
})
export const postSurveyRegistMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postSurveyRegistMutateFetch(param, isMock, env, isAuth, isSSG), {...mutationOption})
}
export const postSurveyRegistMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, postSurveyRegistSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return true
  }
  const data = await authProvider(isSSG).post(PATH_DONOTS_API.LAB.SURVEY_REGIST(env), param?.survey_list).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })

  return data?.databody
}

// 게시글 상세
const getPostDetailSchema = yup.object().shape({
  post_key: yup.string(),
})
export const getPostDetailMutate = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getPostDetailMutateFetch(param, isMock, env, isAuth, isSSG))
}
export const getPostDetailMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, getPostDetailSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }

  const data = await authProvider(isSSG).get(PATH_DONOTS_API.LAB.POST_DETAIL(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

// 개시글 리스트
const getPostListSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema // 페이지네이션
  }),
})
export const getFirstPostListMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getFirstPostListFetch(param, isMock, env, isAuth, isSSG))
}

export const getFirstPostListFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, getPostListSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }

  const data = await authProvider(isSSG).get(PATH_DONOTS_API.LAB.POST_LIST(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

export const getPostListMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(),  isSSG = false) => {
  return useMutation((param) => getPostListFetch(param, isMock, env, isAuth, isSSG))
}
export const getPostListFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, getPostListSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }

  const data = await authProvider(isSSG).get(PATH_DONOTS_API.LAB.POST_LIST(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.post_list
}

// 게시글 등록
const postPostRegistSchema = yup.object().shape({
  params: yup.object().shape({}),
})
export const postPostRegistMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postPostRegistFetch(param, isMock, env, isAuth, isSSG))
}
export const postPostRegistFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, postPostRegistSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }

  return await authProvider(isSSG).post(PATH_DONOTS_API.LAB.POST_REGIST(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 게시글 수정 삭제
const putPostUpdateSchema = yup.object().shape({
  params: yup.object().shape({}),
})
export const putPostUpdateMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => putPostUpdateFetch(param, isMock, env, isAuth, isSSG))
}
export const putPostUpdateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, putPostUpdateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }

  return await authProvider(isSSG).put(PATH_DONOTS_API.LAB.POST_UPDATE(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 댓글 리스트
const getCommentListSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema // 페이지네이션
  }),
})
export const getFirstCommentListMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getFirstCommentListFetch(param, isMock, env, isAuth, isSSG))
}

export const getFirstCommentListFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, getPostListSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }

  const data = await authProvider(isSSG).get(PATH_DONOTS_API.LAB.COMMENT_LIST(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}
export const getCommentListMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getCommentListFetch(param, isMock, env, isAuth, isSSG))
}
export const getCommentListFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, getCommentListSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }

  const data = await authProvider(isSSG).get(PATH_DONOTS_API.LAB.COMMENT_LIST(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.comment_list
}

// 댓글 등록
const postCommentRegistSchema = yup.object().shape({
  params: yup.object().shape({}),
})
export const postCommentRegistMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postCommentRegistFetch(param, isMock, env, isAuth, isSSG))
}
export const postCommentRegistFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, postCommentRegistSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }

  return await authProvider(isSSG).post(PATH_DONOTS_API.LAB.COMMENT_REGIST(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 댓글 수정 삭제
const putCommentUpdateSchema = yup.object().shape({
  params: yup.object().shape({}),
})
export const putCommentUpdateMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => putCommentUpdateFetch(param, isMock, env, isAuth, isSSG))
}
export const putCommentUpdateFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, putCommentUpdateSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }

  return await authProvider(isSSG).put(PATH_DONOTS_API.LAB.COMMENT_UPDATE(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 좋아요
const postPostLikeSchema = yup.object().shape({
  params: yup.object().shape({}),
})
export const postPostLikeMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postPostLikeFetch(param, isMock, env, isAuth, isSSG))
}
export const postPostLikeFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, postPostLikeSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }

  return await authProvider(isSSG).post(PATH_DONOTS_API.LAB.POST_LIKE(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 신고하기
const postDeclareSchema = yup.object().shape({
  params: yup.object().shape({}),
})
export const postDeclareMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => postDeclareFetch(param, isMock, env, isAuth, isSSG))
}
export const postDeclareFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, postDeclareSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }

  return await authProvider(isSSG).post(PATH_DONOTS_API.LAB.POST_DECLARE(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

export const getPollRegistYn = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getPollRegistYn"], () => getPollRegistYnFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}
export const getPollRegistYnFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return MOCK_SURVEY_YN
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.LAB.POLL_REGIST_YN(env), {
    params: param
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.poll_yn
}

const postPollRegistSchema = yup.object().shape({
  param: yup.object().shape({}),
})
export const postPollRegistMutate = (isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => postPollRegistMutateFetch(param, isMock, env, isSSG))
}
export const postPollRegistMutateFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, postPollRegistSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }

  return await authProvider(isSSG).post(PATH_DONOTS_API.LAB.POLL_REGIST(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}
