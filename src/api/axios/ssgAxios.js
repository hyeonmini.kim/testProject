import axios from "axios"

const ssgAxios = axios.create({
  timeout: 100000
})

ssgAxios.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    return Promise.reject(error);
  }
)

ssgAxios.interceptors.response.use(
  async (response) => {
    return response?.data;
  },
  error => {
    return Promise.reject(error);
  }
);

export default ssgAxios