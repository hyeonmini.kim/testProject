import {MOCK_GET_RECIPE, MOCK_GET_RECIPE2} from "../mock/recipe";
import {PATH_DONOTS_API} from "../routes/paths";
import {getEnv, isAuthApi, isMockApi} from "../utils/envUtils";
import {useMutation, useQuery} from "react-query";
import {ENV, IS_MOCK} from "../config";
import {BABY_INFO, BABY_INFO2, MAIN_INFO, NOTIFICATION, NOTIFICATION2} from "../mock/home";
import {authProvider, noAuthProvider, queryOption} from "../layouts/common/AxiosProvider";
import {ERRORS, NEW_CUSTOM_ERROR} from "../constant/common/Error";
import * as yup from "yup";
import {isValidateParam} from "../utils/validateUtils";
import {MOCK_POPUP_LIST} from "../mock/layerPopup";

// 맞춤기반 메인 레시피
export const getRecipeCustomBasedList = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getRecipeCustomBasedList"], () => getRecipeCustomBasedListFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}
export const getRecipeCustomBasedListFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    if (isSSG) {
      return [...MOCK_GET_RECIPE]
    } else {
      return [...MOCK_GET_RECIPE2]
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.HOME.CUSTOM(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.recipe_custom_based_list
}

// 사용자 신체기반 메인 레시피
export const getRecipeUserBodyBasedList = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getRecipeUserBodyBasedList"], () => getRecipeUserBodyBasedListFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}

export const getRecipeUserBodyBasedListFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    if (isSSG) {
      return [...MOCK_GET_RECIPE]
    } else {
      return [...MOCK_GET_RECIPE2]
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.HOME.BODY(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.recipe_user_based_list
}

// 재철 재료 기반 메인 레시피
export const getSeasonIngredientBasedRecipeList = (param, isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useQuery(["getSeasonIngredientBasedRecipeList"], () => getSeasonIngredientBasedRecipeListFetch(param, isMock, env, isSSG), {...queryOption})
}
export const getSeasonIngredientBasedRecipeListFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (isMock) {
    if (isSSG) {
      return [...MOCK_GET_RECIPE]
    } else {
      return [...MOCK_GET_RECIPE2]
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.HOME.SEASONING(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.recipe_season_ingredient_based_list
}

// 메인 정보
export const getMainInfo = (param, isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useQuery(["getMainInfo"], () => getMainInfoFetch(param, isMock, env, isSSG), {...queryOption})
}
export const getMainInfoFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (isMock) {
    if (isSSG) {
      return MAIN_INFO
    } else {
      return MAIN_INFO
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.HOME.MAIN_INFO(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

// 알림 리스트
export const getNotiList = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getNotiList"], () => getNotiListFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}
export const getNotiListFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    if (isSSG) {
      return NOTIFICATION
    } else {
      return NOTIFICATION2
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.HOME.NOTI_LIST(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody
}

// 알림 읽기 업데이트
const updateNotiReadSchema = yup.object().shape({
  noti_key: yup.string().required(),
})
export const updateNotiRead = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => updateNotiReadFetch(param, isMock, env, isAuth, isSSG))
}
export const updateNotiReadFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, updateNotiReadSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).post(PATH_DONOTS_API.HOME.NOTI_READ(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 알림 전체 읽기 업데이트
export const updateNotiReadAll = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => updateNotiReadAllFetch(param, isMock, env, isAuth, isSSG))
}
export const updateNotiReadAllFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    return true
  }
  return await authProvider(isSSG).post(PATH_DONOTS_API.HOME.NOTI_READ_ALL(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
}

// 아이정보
export const getUserBabyInfo = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getUserBabyInfo"], () => getUserBabyInfoFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}
export const getUserBabyInfoFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    if (isSSG) {
      return BABY_INFO
    } else {
      return BABY_INFO2
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.HOME.BABY_INFO(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.baby_info
}

export const getPopupList = (param, isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useQuery(["getPopupList"], () => getPopupListFetch(param, isMock, env, isAuth, isSSG), {...queryOption})
}
export const getPopupListFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (isMock) {
    if (isSSG) {
      return MOCK_POPUP_LIST
    } else {
      return MOCK_POPUP_LIST
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.HOME.POPUP_LIST(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.popup_list
}