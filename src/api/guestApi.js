import {PATH_DONOTS_API} from "../routes/paths";
import {useMutation} from "react-query";
import {MOCK_GET_RECIPE, MOCK_GET_RECIPE2} from "../mock/recipe";
import {getEnv, isMockApi} from "../utils/envUtils";
import {ENV, IS_MOCK} from "../config";
import {noAuthProvider, queryOption} from "../layouts/common/AxiosProvider";
import {ERRORS, NEW_CUSTOM_ERROR} from "../constant/common/Error";
import * as yup from "yup";
import {isValidateParam} from "../utils/validateUtils";

// 비회원 신체기반 메인 레시피 목록
const getNoUserBodyBasedRecipeListSchema = yup.object().shape({
  birthday: yup.string().required(),
  gender: yup.string().required(),
  height: yup.string().required(),
  weight: yup.string().required(),
})
export const getNoUserBodyBasedRecipeListMutate = (param, isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getNoUserBodyBasedRecipeListFetch(param, isMock, env, isSSG))
}
export const getNoUserBodyBasedRecipeListFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getNoUserBodyBasedRecipeListSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return [...MOCK_GET_RECIPE]
    } else {
      return [...MOCK_GET_RECIPE2]
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.GUEST.BODY(env, param)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })

  return data?.databody?.recipe_no_user_body_based_list
}

// 비회원 알레르기기반 메인 레시피 목록
const getNoUserAllergyBasedRecipeListSchema = yup.object().shape({
  params: yup.object().shape({
    baby_allergy_ingredient_key: yup.string(),
  })
})
export const getNoUserAllergyBasedRecipeListMutate = (param, isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getNoUserAllergyBasedRecipeListFetch(param, isMock, env, isSSG))
}
export const getNoUserAllergyBasedRecipeListFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getNoUserAllergyBasedRecipeListSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return [...MOCK_GET_RECIPE]
    } else {
      return [...MOCK_GET_RECIPE2]
    }
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.GUEST.ALLERGY(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.recipe_no_user_allergy_based_list
}

// 비회원건강기반 메인 레시피 목록
const getNoUserHealthBasedRecipeListSchema = yup.object().shape({
  params: yup.object().shape({
    baby_concern_key: yup.string(),
  })
})
export const getNoUserHealthBasedRecipeListMutate = (param, isMock = isMockApi(), env = getEnv(), isSSG = false) => {
  return useMutation((param) => getNoUserHealthBasedRecipeListFetch(param, isMock, env, isSSG))
}
export const getNoUserHealthBasedRecipeListFetch = async (param, isMock = IS_MOCK, env = ENV, isSSG = true) => {
  if (!await isValidateParam(param, getNoUserHealthBasedRecipeListSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return [...MOCK_GET_RECIPE2]
    }
    return [...MOCK_GET_RECIPE]
  }
  const data = await noAuthProvider(isSSG).get(PATH_DONOTS_API.GUEST.HEALTH(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.recipe_no_user_health_based_list
}