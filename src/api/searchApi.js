import {PATH_DONOTS_API} from "../routes/paths";
import {useMutation} from "react-query";
import {getEnv, isAuthApi, isMockApi} from "../utils/envUtils";
import {ENV, IS_MOCK} from "../config";
import {MOCK_GET_RECIPE, MOCK_GET_RECIPE2} from "../mock/recipe";
import {authProvider, getProvider, queryOption} from "../layouts/common/AxiosProvider";
import {ERRORS, NEW_CUSTOM_ERROR} from "../constant/common/Error";
import * as yup from "yup";
import {isValidateParam, paginationSchema} from "../utils/validateUtils";

// 레시피 검색
const getSearchRecipeSchema = yup.object().shape({
  params: yup.object().shape({
    recipe_search_text: yup.string(),
    category_name: yup.string(),
    category_main_name: yup.string(),
    except_ingredient_yn: yup.mixed().oneOf(['Y', 'N']),
    ...paginationSchema
  }),
})

export const getSearchRecipeMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getSearchRecipeFetch(param, isMock, env, isAuth, isSSG))
}
export const getSearchRecipeFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!await isValidateParam(param, getSearchRecipeSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return [...MOCK_GET_RECIPE]
    } else {
      return [...MOCK_GET_RECIPE2]
    }
  }

  const data = await getProvider(isSSG, isAuth).get(PATH_DONOTS_API.SEARCH.RECIPE(env), {
    params: param?.params
  }).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.recipe_list
}

// 사용자 신체기반 메인 레시피 (더보기)
const getRecipeUserBodyBasedListMoreSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema
  }),
})
export const getRecipeUserBodyBasedListMoreMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getRecipeUserBodyBasedListMoreFetch(param, isMock, env, isAuth, isSSG))
}
export const getRecipeUserBodyBasedListMoreFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, getRecipeUserBodyBasedListMoreSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return [...MOCK_GET_RECIPE]
    } else {
      return [...MOCK_GET_RECIPE2]
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.SEARCH.BODY(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.recipe_user_based_list
}

// 성장 도움 레시피 (더보기)
const getRecipeCustomBasedListMoreSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema
  }),
})
export const getRecipeCustomBasedListMoreMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getRecipeCustomBasedListMoreFetch(param, isMock, env, isAuth, isSSG))
}
export const getRecipeCustomBasedListMoreFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, getRecipeCustomBasedListMoreSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return [...MOCK_GET_RECIPE]
    } else {
      return [...MOCK_GET_RECIPE2]
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.SEARCH.CUSTOM(env), param).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.recipe_custom_based_list
}

// 재철 재료 기반 메인 레시피 (더보기)
const getSeasonIngredientBasedRecipeListMoreSchema = yup.object().shape({
  params: yup.object().shape({
    ...paginationSchema
  }),
})
export const getSeasonIngredientBasedRecipeListMoreMutate = (isMock = isMockApi(), env = getEnv(), isAuth = isAuthApi(), isSSG = false) => {
  return useMutation((param) => getSeasonIngredientBasedRecipeListMoreFetch(param, isMock, env, isAuth, isSSG))
}
export const getSeasonIngredientBasedRecipeListMoreFetch = async (param, isMock = IS_MOCK, env = ENV, isAuth = false, isSSG = true) => {
  if (!isAuth) {
    throw await ERRORS.New(ERRORS.UNAUTHORIZED)
  }
  if (!await isValidateParam(param, getSeasonIngredientBasedRecipeListMoreSchema)) {
    throw await ERRORS.New(ERRORS.INVALID_PARAMETER)
  }
  if (isMock) {
    if (isSSG) {
      return [...MOCK_GET_RECIPE]
    } else {
      return [...MOCK_GET_RECIPE2]
    }
  }
  const data = await authProvider(isSSG).get(PATH_DONOTS_API.SEARCH.SEASONING(env)).catch((error) => {
    throw NEW_CUSTOM_ERROR(error, [])
  })
  return data?.databody?.recipe_season_ingredient_based_list
}