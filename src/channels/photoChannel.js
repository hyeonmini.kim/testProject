import {logger} from "../utils/loggingUtils";

export function callOpenGalleryChannel(length, callback) {
  try {
    window.flutter_inappwebview.callHandler('openGallery', 4)
      .then(async function (result) {
        if (result && result.imgCount > 0) {
          callback(result.imgList)
        }
      });
  } catch (e) {
    logger.error(e.message);
  }
}

export function callOpenNativeGalleryChannel(currentCnt, maxCnt, callback) {
  try {
    window.flutter_inappwebview.callHandler('openNativeGallery', currentCnt, maxCnt)
      .then(async function (result) {
        if (result && result.imageCnt > 0) {
          callback(result.images)
        }
      });
  } catch (e) {
    logger.error(e.message);
  }
}