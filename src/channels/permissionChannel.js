import {logger} from "../utils/loggingUtils";

class Permission {
  static camera = true
  static file = true
  static contact = true
  static message = true
}

export const PERMISSION_JSON = JSON.stringify([
  Permission.camera,
  Permission.file,
  Permission.contact,
  Permission.message,
])

export function CallNativePermission(permissionJson) {
  try {
    window.flutter_inappwebview.callHandler('openNativePermission', permissionJson);
  } catch (e) {
    logger.error(e.message)
  }
}

export function TerminateApp() {
  try {
    window.flutter_inappwebview.callHandler('openNativeAppTerminate')
  } catch (e) {
    logger.error(e.message)
  }
}

export function callOpenNativeSetTokensChannel(accessToken, refreshToken) {
  try {
    window.flutter_inappwebview.callHandler('openNativeSetTokens', accessToken, refreshToken)
  } catch (e) {
    logger.error(e.message)
  }
}

export function callOpenNativeGetTokensChannel(callback) {
  try {
    window.flutter_inappwebview.callHandler('openNativeGetTokens')
      .then(async function (result) {
        if (result) {
          callback(result)
        }
      })
  } catch (e) {
    logger.dump("openNativeGetTokens error", e.message)
    logger.error(e.message)
  }
}

// 앱 처음 실행하면 권한 허가 다이얼로그 띄우기
export function callOpenNativeFirstPermission(callback) {
  try {
    window.flutter_inappwebview.callHandler('openNativeFirstPermission')
      .then(async function (result) {
        if (result) {
          callback()
        } else {
          await CallNativePermission(PERMISSION_JSON)
        }
      })
  } catch (e) {
    logger.error(e.message)
  }
}