
import {logger} from "../utils/loggingUtils";
import {ERRORS} from "../constant/common/Error";

// 웹 로딩 완료 보고
export function callOpenNativeIsWebLoadChannel() {
  try {
    window.flutter_inappwebview.callHandler('openNativeIsWebLoad', true)
  } catch (e) {
    logger.dump('openNativeIsWebLoad', e.message)
  }
}

// 최근 로그인한 SNS 명 저장하기
export function callOpenNativeSetSnsProviderChannel(provider) {
  try {
    window.flutter_inappwebview.callHandler('openNativeSetSnsAccount', provider)
  } catch (e) {
    logger.dump('openNativeSetSnsAccount', e.message)
  }
}

// 최근 로그인한 SNS 명 불러오기
export function callOpenNativeGetSnsProviderChannel(callback) {
  try {
    window.flutter_inappwebview.callHandler('openNativeGetSnsAccount')
      .then(async function (result) {
        if (result) {
          callback(result)
        }
      })
  } catch (e) {
    logger.dump('openNativeGetSnsAccount', e.message)
  }
}

// 네이티브 기능 설정 저장하기
export const setNativeOpt = (option) => {
  if (!option) return
  try {
    window.flutter_inappwebview.callHandler('openNativeOptSet', option.type, option.value)
      .then(async function (result) {
      })
  } catch (e) {
    logger.dump('openNativeOptSet', option)
  }
}

// 네이티브 기능 설정 불러오기
export const getNativeOpt = (type, callback) => {
  try {
    window.flutter_inappwebview.callHandler('openNativeOptGet', type)
      .then(function (result) {
        return callback(result)
      })
  } catch (e) {
    throw e
    logger.dump('openNativeOptGet', {
      type : type,
      message: e.message
    })
  }
}