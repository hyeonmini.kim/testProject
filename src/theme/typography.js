// ----------------------------------------------------------------------

export function remToPx(value) {
  return Math.round(parseFloat(value) * 16);
}

export function pxToRem(value) {
  return `${value / 16}rem`;
}

export function responsiveFontSizes({sm, md, lg}) {
  return {
    '@media (min-width:600px)': {
      fontSize: pxToRem(sm),
    },
    '@media (min-width:900px)': {
      fontSize: pxToRem(md),
    },
    '@media (min-width:1200px)': {
      fontSize: pxToRem(lg),
    },
  };
}

// ----------------------------------------------------------------------

const FONT_PRIMARY = 'Noto Sans KR';

/* variant prop 명 설명 */
/* h1_24_b_1l */
/* h1 : type - header1, header2, body1, body2... */
/* 24 : font size - 24px, 18px, 16px... */
/* b : font weight - bold(700), medium(500), regular(400)... */
/* 1l : line height - 1l: one line, none: multi line */

const typography = {
  fontFamily: FONT_PRIMARY,
  fontWeightRegular: 400,
  fontWeightMedium: 500,
  fontWeightBold: 700,
  h1: {
    fontWeight: 800,
    lineHeight: 80 / 64,
    fontSize: pxToRem(40),
    ...responsiveFontSizes({sm: 52, md: 58, lg: 64}),
  },
  h2: {
    fontWeight: 800,
    lineHeight: 64 / 48,
    fontSize: pxToRem(32),
    ...responsiveFontSizes({sm: 40, md: 44, lg: 48}),
  },
  h3: {
    fontWeight: 700,
    lineHeight: 1.5,
    fontSize: pxToRem(24),
    ...responsiveFontSizes({sm: 26, md: 30, lg: 32}),
  },
  h4: {
    fontWeight: 700,
    lineHeight: 1.5,
    fontSize: pxToRem(20),
    ...responsiveFontSizes({sm: 20, md: 24, lg: 24}),
  },
  h5: {
    fontWeight: 700,
    lineHeight: 1.5,
    fontSize: pxToRem(18),
    ...responsiveFontSizes({sm: 19, md: 20, lg: 20}),
  },
  h6: {
    fontWeight: 700,
    lineHeight: 28 / 18,
    fontSize: pxToRem(17),
    ...responsiveFontSizes({sm: 18, md: 18, lg: 18}),
  },
  subtitle1: {
    fontWeight: 600,
    lineHeight: 1.5,
    fontSize: pxToRem(16),
  },
  subtitle2: {
    fontWeight: 600,
    lineHeight: 22 / 14,
    fontSize: pxToRem(14),
  },
  body1: {
    lineHeight: 1.5,
    fontSize: pxToRem(16),
    letterSpacing: '-0.02em',
  },
  body2: {
    lineHeight: 22 / 14,
    fontSize: pxToRem(14),
  },
  caption: {
    lineHeight: 1.5,
    fontSize: pxToRem(12),
  },
  overline: {
    fontWeight: 700,
    lineHeight: 1.5,
    fontSize: pxToRem(12),
    textTransform: 'uppercase',
  },
  button: {
    fontWeight: 700,
    lineHeight: 24 / 14,
    fontSize: pxToRem(14),
    textTransform: 'capitalize',
  },
  h_28_b: {
    fontSize: '28px',
    fontWeight: 700,
    lineHeight: '36px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  h_28_b_1l: {
    fontSize: '28px',
    fontWeight: 700,
    lineHeight: '28px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  h1_24_b: {
    fontSize: '24px',
    fontWeight: 700,
    lineHeight: '32px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  h1_24_b_1l: {
    fontSize: '24px',
    fontWeight: 700,
    lineHeight: '24px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  h2_20_b: {
    fontSize: '20px',
    fontWeight: 700,
    lineHeight: '26px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  h2_20_m: {
    fontSize: '20px',
    fontWeight: 500,
    lineHeight: '26px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  h2_20_r: {
    fontSize: '20px',
    fontWeight: 400,
    lineHeight: '26px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  h2_20_b_1l: {
    fontSize: '20px',
    fontWeight: 700,
    lineHeight: '20px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  h2_20_m_1l: {
    fontSize: '20px',
    fontWeight: 500,
    lineHeight: '20px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  h2_20_r_1l: {
    fontSize: '20px',
    fontWeight: 400,
    lineHeight: '20px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b_18_b: {
    fontSize: '18px',
    fontWeight: 700,
    lineHeight: '24px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b_18_m: {
    fontSize: '18px',
    fontWeight: 500,
    lineHeight: '24px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b_18_r: {
    fontSize: '18px',
    fontWeight: 400,
    lineHeight: '24px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b_18_b_1l: {
    fontSize: '18px',
    fontWeight: 700,
    lineHeight: '18px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b_18_m_1l: {
    fontSize: '18px',
    fontWeight: 500,
    lineHeight: '18px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b_18_r_1l: {
    fontSize: '18px',
    fontWeight: 400,
    lineHeight: '18px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b1_16_b: {
    fontSize: '16px',
    fontWeight: 700,
    lineHeight: '24px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b1_16_m: {
    fontSize: '16px',
    fontWeight: 500,
    lineHeight: '24px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b1_16_r: {
    fontSize: '16px',
    fontWeight: 400,
    lineHeight: '24px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b1_16_b_1l: {
    fontSize: '16px',
    fontWeight: 700,
    lineHeight: '16px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b1_16_m_1l: {
    fontSize: '16px',
    fontWeight: 500,
    lineHeight: '16px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b1_16_r_1l: {
    fontSize: '16px',
    fontWeight: 400,
    lineHeight: '16px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b2_14_b: {
    fontSize: '14px',
    fontWeight: 700,
    lineHeight: '20px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b2_14_m: {
    fontSize: '14px',
    fontWeight: 500,
    lineHeight: '20px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b2_14_r: {
    fontSize: '14px',
    fontWeight: 400,
    lineHeight: '20px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b2_14_b_1l: {
    fontSize: '14px',
    fontWeight: 700,
    lineHeight: '14px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b2_14_m_1l: {
    fontSize: '14px',
    fontWeight: 500,
    lineHeight: '14px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  b2_14_r_1l: {
    fontSize: '14px',
    fontWeight: 400,
    lineHeight: '14px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  bs_12_b: {
    fontSize: '12px',
    fontWeight: 700,
    lineHeight: '18px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  bs_12_m: {
    fontSize: '12px',
    fontWeight: 500,
    lineHeight: '18px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  bs_12_r: {
    fontSize: '12px',
    fontWeight: 400,
    lineHeight: '18px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  bs_12_b_1l: {
    fontSize: '12px',
    fontWeight: 700,
    lineHeight: '12px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  bs_12_m_1l: {
    fontSize: '12px',
    fontWeight: 500,
    lineHeight: '12px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  bs_12_r_1l: {
    fontSize: '12px',
    fontWeight: 400,
    lineHeight: '12px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  bxs_10_b: {
    fontSize: '10px',
    fontWeight: 700,
    lineHeight: '16px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  bxs_10_r: {
    fontSize: '10px',
    fontWeight: 400,
    lineHeight: '16px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  bxs_10_b_1l: {
    fontSize: '10px',
    fontWeight: 700,
    lineHeight: '10px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  bxs_10_r_1l: {
    fontSize: '10px',
    fontWeight: 400,
    lineHeight: '10px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
  btn: {
    fontSize: '16px',
    fontWeight: 700,
    lineHeight: '16px',
    letterSpacing: '-0.02em',
    whiteSpace: 'pre-wrap',
    wordBreak: 'keep-all',
  },
};

export default typography;
