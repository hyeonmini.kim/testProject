export default function Box(theme) {
  return {
    MuiBox: {
      styleOverrides: {
        maxWidthXs: {
          [theme.breakpoints.up('xs')]: {
            maxWidth: '440px'
          },
        },
        maxWidthSm: {
          [theme.breakpoints.up('sm')]: {
            maxWidth: '820px'
          },
        }
      }
    }
  };
}
