export default function Stack(theme) {
  return {
    MuiStack: {
      styleOverrides: {
        maxWidthXs: {
          [theme.breakpoints.up('xs')]: {
            maxWidth: '440px'
          },
        },
        maxWidthSm: {
          [theme.breakpoints.up('sm')]: {
            maxWidth: '820px'
          },
        }
      }
    }
  };
}
