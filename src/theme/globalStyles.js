// @mui
import {GlobalStyles as MUIGlobalStyles} from '@mui/material';

// ----------------------------------------------------------------------

export default function GlobalStyles() {
  const removeBoxBackgroundColor = {
    webkitTextFillColor: '#000',
    webkitBoxShadow: '0 0 0px 1000px #fff inset',
    boxShadow: '0 0 0px 1000px #fff inset',
    transition: 'background-color 5000s ease-in-out 0s',
  }

  const inputGlobalStyles = (
    <MUIGlobalStyles
      styles={{
        '*': {
          boxSizing: 'border-box',
        },
        html: {
          margin: 0,
          padding: 0,
          width: '100%',
          height: '100%',
          WebkitOverflowScrolling: 'touch',
        },
        body: {
          margin: 0,
          padding: 0,
          width: '100%',
          height: '100%',
          mozUserSelect: 'none',
          webkitUserSelect: 'none',
          msUserSelect: 'none',
          userSelect: 'none',
          WebkitTapHighlightColor: 'transparent',
        },
        '#__next': {
          width: '100%',
          height: '100%',
        },
        input: {
          '&[type=number]': {
            MozAppearance: 'textfield',
            '&::-webkit-outer-spin-button': {
              margin: 0,
              WebkitAppearance: 'none',
            },
            '&::-webkit-inner-spin-button': {
              margin: 0,
              WebkitAppearance: 'none',
            },
          },
          '&:-webkit-autofill': removeBoxBackgroundColor,
          '&:-webkit-autofill:hover': removeBoxBackgroundColor,
          '&:-webkit-autofill:focus': removeBoxBackgroundColor,
          '&:-webkit-autofill:active': removeBoxBackgroundColor,
          '&:autofill': removeBoxBackgroundColor,
          '&:autofill:hover': removeBoxBackgroundColor,
          '&:autofill:focus': removeBoxBackgroundColor,
          '&:autofill:active': removeBoxBackgroundColor,
        },
        img: {
          display: 'block',
          maxWidth: '100%',
        },
        ul: {
          margin: 0,
          padding: 0,
        },
      }}
    />
  );

  return inputGlobalStyles;
}
