import {alpha} from '@mui/material/styles';

// ----------------------------------------------------------------------

// SETUP COLORS

const GREY = {
  0: '#FFFFFF', // Bg3
  50: '#FAFAFA', // Bg2
  100: '#F9FAFB',
  150: '#F5F5F5', // Bg1
  200: '#F4F6F8',
  250: '#E6E6E6', // Disabled bg/border
  300: '#DFE3E8',
  350: '#CCCCCC', // Disabled txt/input
  400: '#C4CDD5',
  450: '#888888', // Lp2
  500: '#919EAB',
  550: '#888888', // Lp1
  570: '#666666', // Secondary
  600: '#637381',
  700: '#454F5B',
  750: '#222222', // Primary
  800: '#212B36',
  900: '#161C24',
  950: '#000000', // Title
};

const PRIMARY = {
  lighter: '#FFC656',
  light: '#FFB13D',
  middle: '#F9A536',
  main: '#ECA548',
  dark: '#ECA548', // hover backgroundColor
  darker: '#E8972C',
  contrastText: '#fff',
};

const SECONDARY = {
  lighter: '#684113',
  light: '#5A370D',
  middle: '#4F2F09',
  main: '#3F2404',
  dark: '#422503',
  darker: '#361F04',
  contrastText: '#fff',
};

const INFO = {
  lighter: '#CAFDF5',
  light: '#61F3F3',
  main: '#00B8D9',
  dark: '#006C9C',
  darker: '#003768',
  contrastText: '#fff',
};

const SUPPORT = {
  lighter: '#B7ECD6',
  light: '#A2E7CA',
  middle: '#90E2C0',
  main: '#86D6B4',
  dark: '#7ECDAC',
  darker: '#76C0A1',
  contrastText: '#fff',
}

const SUCCESS = {
  lighter: '#D8FBDE',
  light: '#86E8AB',
  main: '#36B37E',
  dark: '#1B806A',
  darker: '#0A5554',
  contrastText: '#fff',
};

const WARNING = {
  lighter: '#FFF5CC',
  light: '#FFD666',
  main: '#FFAB00',
  dark: '#B76E00',
  darker: '#7A4100',
  contrastText: GREY[800],
};

const ERROR = {
  lighter: '#FFE9D5',
  light: '#FFAC82',
  main: '#FF4842',
  dark: '#B71D18',
  darker: '#7A0916',
  contrastText: '#fff',
};

const COMMON = {
  common: {black: '#000', white: '#fff'},
  primary: PRIMARY,
  secondary: SECONDARY,
  info: INFO,
  success: SUCCESS,
  warning: WARNING,
  error: ERROR,
  grey: GREY,
  divider: GREY[250],
  action: {
    hover: alpha(GREY[500], 0.08),
    selected: alpha(GREY[500], 0.16),
    disabled: GREY[350],
    disabledBackground: GREY[250],
    focus: alpha(GREY[500], 0.24),
    hoverOpacity: 0.08,
    disabledOpacity: 0.48,
  },
};

export default function palette(themeMode) {
  const light = {
    ...COMMON,
    mode: 'light',
    text: {
      primary: GREY[750],
      secondary: GREY[570],
      disabled: GREY[350],
    },
    background: {paper: '#fff', default: '#fff', neutral: GREY[200]},
    action: {
      ...COMMON.action,
      active: GREY[570],
    },
  };

  const dark = {
    ...COMMON,
    mode: 'dark',
    text: {
      primary: '#fff',
      secondary: GREY[500],
      disabled: GREY[600],
    },
    background: {
      paper: GREY[800],
      default: GREY[900],
      neutral: alpha(GREY[500], 0.16),
    },
    action: {
      ...COMMON.action,
      active: GREY[500],
    },
  };

  return themeMode === 'light' ? light : dark;
}
