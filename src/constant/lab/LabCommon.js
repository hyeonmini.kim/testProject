import {getNumberTenThousand} from "../../utils/formatNumber";
import {nvlString} from "../../utils/formatString";
import {BABYFOOD_ICON, LAB_RECOMMEND_ICON, NUTRITION_ICON} from "../icons/lab/LabIcons";

export const LAB_TYPE = {
  ALERGY: 'NONE',
  NUTRITION: 'SUR01',
  BABY_FOOD: 'SUR02',
  GUEST: 'GUEST',
}

export const TTF_TYPE = {
  REGIST: '등록하기',
  MODIFY: '수정하기',
}

export const POST_TYPE = {
  LIST: '리스트화면',
  VIEW: '상세화면',
  COMMENT: '댓글화면',
}

export const CALLBACK_TYPE = {
  DELETE: 'DELETE',
  CHANGE: 'CHANGE',
  ERROR: 'ERROR',
}

export const CONTENTS_TYPE = {
  POST: '0',
  COMMENT: '1',
}

export const ACTION_TYPE = {
  REGIST: '0',
  MODIFY: '0',
  DELETE: '1',
  CANCEL: '1',
}

export const LAB_DECLARE_TYPE = {
  REP01: {
    title: '도배성 게시글인 것 같아요',
    value: 'REP01'
  },
  REP02: {
    title: '광고, 홍보성 게시글인 것 같아요',
    value: 'REP02'
  },
  REP03: {
    title: '무관한 내용이에요',
    value: 'REP03'
  },
  REP04: {
    title: '기타 (에티켓 위반 등)',
    value: 'REP04'
  },
}

export const LAB_ORDER_TYPE = {
  NEWEST: {
    title: '최신순',
    value: 'recent',
  },
  OLDEST: {
    title: '등록순',
    value: 'create',
  },
  LIKES: {
    title: '좋아요 많은 순',
    value: 'great',
  },
  COMMENTS: {
    title: '댓글 많은 순',
    value: 'comment',
  },
}

export const LAB_TEXT = {
  SCREEN_BUTTON: (type) => {
    switch (type) {
      case LAB_TYPE.BABY_FOOD:
        return '아이 이유식 입력하고 랭킹보기'
      case LAB_TYPE.NUTRITION:
        return '아이 영양제 입력하고 랭킹보기'
    }
    return
  },
  COMMENT: {
    TITLE: (type) => {
      switch (type) {
        case LAB_TYPE.BABY_FOOD:
          return '시판이유식에 관해\n자유롭게 얘기해보세요'
        case LAB_TYPE.NUTRITION:
          return '영양제에 관해\n자유롭게 얘기해보세요'
      }
      return
    },
    DECLARE: {
      TITLE: '신고하기',
      MESSAGE: '신고 처리가 되었습니다',
      LIST: [
        LAB_DECLARE_TYPE.REP01,
        LAB_DECLARE_TYPE.REP02,
        LAB_DECLARE_TYPE.REP03,
        LAB_DECLARE_TYPE.REP04,
      ]
    },
    MODIFY: '수정하기',
    ORDER: {
      TITLE: '정렬',
      LIST: [
        LAB_ORDER_TYPE.NEWEST,
        LAB_ORDER_TYPE.OLDEST,
        LAB_ORDER_TYPE.LIKES,
        LAB_ORDER_TYPE.COMMENTS,
      ]
    },
    NOT_FOUND_LIST: '등록된 글이 없습니다.',
    NOT_FOUND_COMMENT: '등록된 댓글이 없습니다.',
    PLACEHOLDER: {
      POST: '자유롭게 의견을 나눠보세요',
      POST_EXPAND: '다른 사람에게 불쾌감을 주는 악플이나 광고성 글은 임의 삭제될 수 있습니다.',
      COMMENT: '댓글을 입력하세요',
      COMMENT_EXPAND: '다른 사람에게 불쾌감을 주는 욕설, 혐오, 비하의 표현이나 타인의 권리를 침해하는 내용은 고지없이 삭제될 수 있습니다.',
    },
    COUNT: '개의 글',
    LENGTH: (len) => `${len} / 200`,
    MY_WRITING: '내가 쓴 글만 보기',
    DIALOG: {
      MIN: '최소 2자 이상 등록해야 합니다.',
      DELETE: '정말 삭제하시겠습니까?'
    },
    BUTTON: {
      REGIST: '등록하기',
      MORE: '더보기',
      DECLARATION: '신고',
      MODIFY: '수정',
      DELETE: '삭제',
      CANCEL: '취소',
      CONFIRM: '확인',
      ADD: '댓글달기',
      COUNT: (count) => `댓글 ${getNumberTenThousand(count)}`,
    }
  }

}

export const SURVEY_TYPE = {
  SUR01: {
    code: "SUR01",
    text: "영양제",
    value: false,
  },
  SUR02: {
    code: "SUR02",
    text: "시판이유식",
    value: false,
  },
  SUR03: {
    code: "SUR03",
    text: "유산균",
    value: false,
  },
  SUR04: {
    code: "SUR04",
    text: "비타민D",
    value: false,
  },
  SUR05: {
    code: "SUR05",
    text: "철분",
    value: false,
  },
  SUR06: {
    code: "SUR06",
    text: "없음",
    value: false,
  },
}

export const LAB_NUTRITION_CATEGORY_ITEM = [
  SURVEY_TYPE.SUR03,
  SURVEY_TYPE.SUR04,
  SURVEY_TYPE.SUR05,
]

export const SURVEY_SELECT_REASON = {
  SEL01: {
    code: "SEL01",
    text: "효과좋음",
    clicked: false,
  },
  SEL02: {
    code: "SEL02",
    text: "아이선호",
    clicked: false,
  },
  SEL03: {
    code: "SEL03",
    text: "가격적당",
    clicked: false,
  },
  SEL04: {
    code: "SEL04",
    text: "지인추천(or 병원, 약국)",
    clicked: false,
  },
  SEL05: {
    code: "SEL05",
    text: "쉬운섭취",
    clicked: false,
  },
  SEL06: {
    code: "SEL06",
    text: "기타",
    clicked: false,
  },
  SEL07: {
    code: "SEL07",
    text: "영양균형",
    clicked: false,
  },
  SEL08: {
    code: "SEL08",
    text: "식감, 농도",
    clicked: false,
  },
  SEL09: {
    code: "SEL09",
    text: "포장용기",
    clicked: false,
  },
  SEL10: {
    code: "SEL10",
    text: "양이많음",
    clicked: false,
  },
  SEL11: {
    code: "SEL11",
    text: "아이취향",
    clicked: false,
  },
  SEL12: {
    code: "SEL12",
    text: "종류다양",
    clicked: false,
  },
  SEL13: {
    code: "SEL13",
    text: "가격적당",
    clicked: false,
  },
}

export const SURVEY_SELECT_REASON_LIST = [
  SURVEY_SELECT_REASON.SEL01,
  SURVEY_SELECT_REASON.SEL02,
  SURVEY_SELECT_REASON.SEL03,
  SURVEY_SELECT_REASON.SEL04,
  SURVEY_SELECT_REASON.SEL05,
  SURVEY_SELECT_REASON.SEL06,
  SURVEY_SELECT_REASON.SEL07,
  SURVEY_SELECT_REASON.SEL08,
  SURVEY_SELECT_REASON.SEL09,
  SURVEY_SELECT_REASON.SEL10,
  SURVEY_SELECT_REASON.SEL11,
  SURVEY_SELECT_REASON.SEL12,
  SURVEY_SELECT_REASON.SEL13,
]

export const BABYFOOD_REASON_LIST = [
  SURVEY_SELECT_REASON.SEL07,
  SURVEY_SELECT_REASON.SEL08,
  SURVEY_SELECT_REASON.SEL09,
  SURVEY_SELECT_REASON.SEL10,
  SURVEY_SELECT_REASON.SEL11,
  SURVEY_SELECT_REASON.SEL12,
  SURVEY_SELECT_REASON.SEL13,
]

export const NUTRITION_SUR03_REASON_LIST = [
  SURVEY_SELECT_REASON.SEL01,
  SURVEY_SELECT_REASON.SEL02,
  SURVEY_SELECT_REASON.SEL03,
  SURVEY_SELECT_REASON.SEL04,
  SURVEY_SELECT_REASON.SEL05,
  SURVEY_SELECT_REASON.SEL06,
]

export const NUTRITION_SUR04_REASON_LIST = [
  SURVEY_SELECT_REASON.SEL01,
  SURVEY_SELECT_REASON.SEL02,
  SURVEY_SELECT_REASON.SEL03,
  SURVEY_SELECT_REASON.SEL04,
  SURVEY_SELECT_REASON.SEL05,
  SURVEY_SELECT_REASON.SEL06,
]

export const NUTRITION_SUR05_REASON_LIST = [
  SURVEY_SELECT_REASON.SEL01,
  SURVEY_SELECT_REASON.SEL02,
  SURVEY_SELECT_REASON.SEL03,
  SURVEY_SELECT_REASON.SEL04,
  SURVEY_SELECT_REASON.SEL05,
  SURVEY_SELECT_REASON.SEL06,
]

export const SURVEY_SELECT_REASON_TEXT = (code) => {
  let text = ''
  SURVEY_SELECT_REASON_LIST.forEach((item) => {
    if (item.code === code) {
      text = nvlString(item?.text)
      return
    }
  })
  return text
}

export const BABYFOOD_ACTIVE_STEP = {
  BRAND: 1,
  REASON: 2,
}

export const NUTRITION_ACTIVE_STEP = {
  SELECT: 20,
  SUR03_BRAND: 40,
  SUR03_ETC_REASON: 48,
  SUR03_REASON: 60,
  SUR04_BRAND: 80,
  SUR04_ETC_REASON: 88,
  SUR04_REASON: 100,
  SUR05_BRAND: 120,
  SUR05_ETC_REASON: 126,
  SUR05_REASON: 140,
}

export const SURVEY_COMMON = {
  TEXT: {
    BABYFOOD_BRAND_TITLE: "아이가 먹는 이유식 브랜드는?",
    REASON_TITLE: "이 브랜드를 선택한 이유는?\n(중복선택가능)",
    NUTRITION_SELECT_TITLE: "어떤 영양제를 \n먹이고 계세요? (중복선택가능)",
    NUTRITION_SUR03_BRAND_TITLE: "아이가 먹는 유산균 브랜드는?",
    NUTRITION_SUR04_BRAND_TITLE: "아이가 먹는 비타민D 브랜드는?",
    NUTRITION_SUR05_BRAND_TITLE: "아이가 먹는 철분 브랜드는?",
    NUTRITION_ETC_REASON_TITLE: "어떤 브랜드의 제품을 먹이고 있나요?",
    NUTRITION_SUR03_ETC_REASON_PLACEHOLDER: "ex. 포베라의 프로바이오틱스",
    NUTRITION_SUR04_ETC_REASON_PLACEHOLDER: "ex. 락피도 비타민D 드롭스",
    NUTRITION_SUR05_ETC_REASON_PLACEHOLDER: "ex. 일동 키즈튼튼 철분비타민C",
    BOTTOM_DIALOG_TITLE: "이 컨텐츠를 추천하시겠어요?",
  },
  BUTTON: {
    NEXT: '다음',
    SHOW_RESULT: '결과 보러가기',
    CONFIRM: '확인',
    DONT_SHOW_AGAIN: '하루동안 다시보지않음',
  },
}

export const BABYFOOD = {
  BOB01: {
    code: "BOB01",
    text: "엘빈즈",
    image: BABYFOOD_ICON.BOB01,
    value: false,
  },
  BOB02: {
    code: "BOB02",
    text: "베베쿡",
    image: BABYFOOD_ICON.BOB02,
    value: false,
  },
  BOB03: {
    code: "BOB03",
    text: "푸드케어",
    image: BABYFOOD_ICON.BOB03,
    value: false,
  },
  BOB04: {
    code: "BOB04",
    text: "루솔",
    image: BABYFOOD_ICON.BOB04,
    value: false,
  },
  BOB05: {
    code: "BOB05",
    text: "산골이유식",
    image: BABYFOOD_ICON.BOB05,
    value: false,
  },
  BOB06: {
    code: "BOB06",
    text: "거버",
    image: BABYFOOD_ICON.BOB06,
    value: false,
  },
  BOB07: {
    code: "BOB07",
    text: "짱죽",
    image: BABYFOOD_ICON.BOB07,
    value: false,
  },
  BOB08: {
    code: "BOB08",
    text: "기타",
    image: BABYFOOD_ICON.BOB08,
    value: false,
  },
}

export const NUTRITION = {
  LAC01: {
    code: "LAC01",
    text: "비오비타",
    example: "(ex.베베)",
    image: NUTRITION_ICON.LAC01,
    value: false,
  },
  LAC02: {
    code: "LAC02",
    text: "PH365",
    example: "(ex.프리미엄 베이비)",
    image: NUTRITION_ICON.LAC02,
    value: false,
  },
  LAC03: {
    code: "LAC03",
    text: "포베라 아이맘",
    example: "(ex.프로바이오틱스)",
    image: NUTRITION_ICON.LAC03,
    value: false,
  },
  LAC04: {
    code: "LAC04",
    text: "바이오메라",
    example: "(ex.베이비)",
    image: NUTRITION_ICON.LAC04,
    value: false,
  },
  LAC05: {
    code: "LAC05",
    text: "닥터아돌",
    example: "(ex.베이비)",
    image: NUTRITION_ICON.LAC05,
    value: false,
  },
  LAC06: {
    code: "LAC06",
    text: "바이오가이아",
    example: "(ex.프로텍티스)",
    image: NUTRITION_ICON.LAC06,
    value: false,
  },
  LAC07: {
    code: "LAC07",
    text: "드시모네",
    example: "(ex.베이비)",
    image: NUTRITION_ICON.LAC07,
    value: false,
  },
  LAC08: {
    code: "LAC08",
    text: "락토핏",
    example: "(ex.생유산균 키즈)",
    image: NUTRITION_ICON.LAC08,
    value: false,
  },
  LAC09: {
    code: "LAC09",
    text: "베베쿡",
    example: "(ex.프로바이오 베베)",
    image: NUTRITION_ICON.LAC09,
    value: false,
  },
  LAC10: {
    code: "LAC10",
    text: "아이이베",
    example: "(ex.드롭스D)",
    image: NUTRITION_ICON.LAC10,
    value: false,
  },
  LAC11: {
    code: "LAC11",
    text: "락티브 베베&키즈",
    example: "(ex.비타유산균)",
    image: NUTRITION_ICON.LAC11,
    value: false,
  },
  LAC12: {
    code: "LAC12",
    text: "기타",
    example: "",
    image: NUTRITION_ICON.LAC12,
    value: false,
  },
  VIT01: {
    code: "VIT01",
    text: "닥터썬데이",
    example: "(ex.베이비)",
    image: NUTRITION_ICON.VIT01,
    value: false,
  },
  VIT02: {
    code: "VIT02",
    text: "닥터써니디",
    example: "(ex.드롭스)",
    image: NUTRITION_ICON.VIT02,
    value: false,
  },
  VIT03: {
    code: "VIT03",
    text: "듀오락",
    example: "(ex.베이비 드롭스)",
    image: NUTRITION_ICON.VIT03,
    value: false,
  },
  VIT04: {
    code: "VIT04",
    text: "닥터아돌",
    example: "(ex.키즈아연비타민D)",
    image: NUTRITION_ICON.VIT04,
    value: false,
  },
  VIT05: {
    code: "VIT05",
    text: "트루락",
    example: "(ex.베베)",
    image: NUTRITION_ICON.VIT05,
    value: false,
  },
  VIT06: {
    code: "VIT06",
    text: "락티브 베베&키즈",
    example: "(ex.비타유산균)",
    image: NUTRITION_ICON.VIT06,
    value: false,
  },
  VIT07: {
    code: "VIT07",
    text: "아이이베",
    example: "(ex.드롭스D)",
    image: NUTRITION_ICON.VIT07,
    value: false,
  },
  VIT08: {
    code: "VIT08",
    text: "함소아",
    example: "(ex.비타민D가득)",
    image: NUTRITION_ICON.VIT08,
    value: false,
  },
  VIT09: {
    code: "VIT09",
    text: "징크디펜스",
    example: "(ex.비타민D&아연)",
    image: NUTRITION_ICON.VIT09,
    value: false,
  },
  VIT10: {
    code: "VIT10",
    text: "락피도",
    example: "(ex.드롭스 퓨어)",
    image: NUTRITION_ICON.VIT10,
    value: false,
  },
  VIT11: {
    code: "VIT11",
    text: "메디오",
    example: "(ex.듀오 플러스)",
    image: NUTRITION_ICON.VIT11,
    value: false,
  },
  VIT12: {
    code: "VIT12",
    text: "기타",
    example: "",
    image: NUTRITION_ICON.VIT12,
    value: false,
  },
  IRO01: {
    code: "IRO01",
    text: "닥터라인",
    example: "(ex.헤모키즈)",
    image: NUTRITION_ICON.IRO01,
    value: false,
  },
  IRO02: {
    code: "IRO02",
    text: "뉴트키즈",
    example: "(ex.철분)",
    image: NUTRITION_ICON.IRO02,
    value: false,
  },
  IRO03: {
    code: "IRO03",
    text: "모비타",
    example: "(ex.메디철분츄어블)",
    image: NUTRITION_ICON.IRO03,
    value: false,
  },
  IRO04: {
    code: "IRO04",
    text: "메디오",
    example: "(ex.듀오 플러스)",
    image: NUTRITION_ICON.IRO04,
    value: false,
  },
  IRO05: {
    code: "IRO05",
    text: "지엠팜 야미푸",
    example: "(ex.철분씨)",
    image: NUTRITION_ICON.IRO05,
    value: false,
  },
  IRO06: {
    code: "IRO06",
    text: "애플킨더",
    example: "(ex.튼튼철분)",
    image: NUTRITION_ICON.IRO06,
    value: false,
  },
  IRO07: {
    code: "IRO07",
    text: "아이뉴트리션",
    example: "(ex.베이비키즈철분)",
    image: NUTRITION_ICON.IRO07,
    value: false,
  },
  IRO08: {
    code: "IRO08",
    text: "GNC",
    example: "(ex.키즈철분플러스)",
    image: NUTRITION_ICON.IRO08,
    value: false,
  },
  IRO09: {
    code: "IRO09",
    text: "PH365",
    example: "(ex.프리미엄 철분)",
    image: NUTRITION_ICON.IRO09,
    value: false,
  },
  IRO10: {
    code: "IRO10",
    text: "연세 키즈텐",
    example: "(ex.철분제 플러스)",
    image: NUTRITION_ICON.IRO10,
    value: false,
  },
  IRO11: {
    code: "IRO11",
    text: "데일리원 베베키즈",
    example: "(ex.어린이 철분)",
    image: NUTRITION_ICON.IRO11,
    value: false,
  },
  IRO12: {
    code: "IRO12",
    text: "기타",
    example: "",
    image: NUTRITION_ICON.IRO12,
    value: false,
  },
}

export const BABYFOOD_LIST = [
  BABYFOOD.BOB01,
  BABYFOOD.BOB02,
  BABYFOOD.BOB03,
  BABYFOOD.BOB04,
  BABYFOOD.BOB05,
  BABYFOOD.BOB06,
  BABYFOOD.BOB07,
  BABYFOOD.BOB08,
]

export const NUTRITION_SUR03_LIST = [
  NUTRITION.LAC01,
  NUTRITION.LAC02,
  NUTRITION.LAC03,
  NUTRITION.LAC04,
  NUTRITION.LAC05,
  NUTRITION.LAC06,
  NUTRITION.LAC07,
  NUTRITION.LAC08,
  NUTRITION.LAC09,
  NUTRITION.LAC10,
  NUTRITION.LAC11,
  NUTRITION.LAC12,
]

export const NUTRITION_SUR04_LIST = [
  NUTRITION.VIT01,
  NUTRITION.VIT02,
  NUTRITION.VIT03,
  NUTRITION.VIT04,
  NUTRITION.VIT05,
  NUTRITION.VIT06,
  NUTRITION.VIT07,
  NUTRITION.VIT08,
  NUTRITION.VIT09,
  NUTRITION.VIT10,
  NUTRITION.VIT11,
  NUTRITION.VIT12,
]

export const NUTRITION_SUR05_LIST = [
  NUTRITION.IRO01,
  NUTRITION.IRO02,
  NUTRITION.IRO03,
  NUTRITION.IRO04,
  NUTRITION.IRO05,
  NUTRITION.IRO06,
  NUTRITION.IRO07,
  NUTRITION.IRO08,
  NUTRITION.IRO09,
  NUTRITION.IRO10,
  NUTRITION.IRO11,
  NUTRITION.IRO12,
]

export const NUTRITION_SELECT_LIST = [
  SURVEY_TYPE.SUR03,
  SURVEY_TYPE.SUR04,
  SURVEY_TYPE.SUR05,
  SURVEY_TYPE.SUR06,
]

export const LAB_RECOMMEND = {
  ZERO: {
    code: "ZERO",
    text: "0",
    image: LAB_RECOMMEND_ICON.ZERO,
    value: false,
  },
  ONE: {
    code: "ONE",
    text: "1",
    image: LAB_RECOMMEND_ICON.ONE,
    value: false,
  },
  TWO: {
    code: "TWO",
    text: "2",
    image: LAB_RECOMMEND_ICON.TWO,
    value: false,
  },
  THREE: {
    code: "THREE",
    text: "3",
    image: LAB_RECOMMEND_ICON.THREE,
    value: false,
  },
  FOUR: {
    code: "FOUR",
    text: "4",
    image: LAB_RECOMMEND_ICON.FOUR,
    value: false,
  },
  FIVE: {
    code: "FIVE",
    text: "5",
    image: LAB_RECOMMEND_ICON.FIVE,
    value: false,
  },
}

export const LAB_RECOMMEND_LIST = [
  LAB_RECOMMEND.ZERO,
  LAB_RECOMMEND.ONE,
  LAB_RECOMMEND.TWO,
  LAB_RECOMMEND.THREE,
  LAB_RECOMMEND.FOUR,
  LAB_RECOMMEND.FIVE,
]