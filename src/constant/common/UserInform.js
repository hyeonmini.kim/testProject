/* 유저 정보 */
export const USER_INFORM = {
  name: '',
  phone: '',
  birth: '',
  gender: '',
  id: '',
  email: '',
  password: '',
  nickname: '',
  avatar: '',
  children: [],
}

/* 아이 정보 */
export const CHILDREN_INFORM = {
  nickname: '',
  gender: '',
  birth: '',
  weight: '',
  height: '',
  allergy: '',
  keyword: '',
  avatar: '',
}
