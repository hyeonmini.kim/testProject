import {nvlString} from "../../utils/formatString";

export const CUSTOM_ERROR = 'CUSTOM_ERROR'

export const IS_CUSTOM_ERROR = (error) => {
  if (error?.type === CUSTOM_ERROR) {
    return true
  }
  return false
}

export const GET_ERROR_CODE = (error) => {
  // logger.dump("Get Error Code", {
  //   axios: error?.code,
  //   status: error?.response?.status,
  //   code: error?.response?.data?.code
  // })
  if (error?.type === CUSTOM_ERROR) {
    return error?.code
  }
  if (error?.code === 'ECONNABORTED') {
    return ERRORS.ECONNABORTED
  }
  if (error?.response?.status === ERRORS.BAD_REQUEST) {
    if (error?.response?.data?.code) {
      return error?.response?.data?.code
    }
    return ERRORS.BAD_REQUEST
  }
  if (error?.response?.status === ERRORS.UNAUTHORIZED) {
    if (error?.response?.data?.code === ERRORS.INVALID_ACCESS_TOKEN) {
      return ERRORS.INVALID_ACCESS_TOKEN
    }
    if (error?.response?.data?.code === ERRORS.INVALID_REFRESH_TOKEN) {
      return ERRORS.INVALID_REFRESH_TOKEN
    }
    return ERRORS.UNAUTHORIZED
  }
  if (error?.response?.status === ERRORS.INTERNAL_SERVER_ERROR) {
    return ERRORS.INTERNAL_SERVER_ERROR
  }
  return nvlString(error?.response?.data?.code)
}

export const NEW_CUSTOM_ERROR = (error, list) => {
  const code = GET_ERROR_CODE(error)
  if (!code) return error
  if (!list?.length) return error

  let hasError = false
  list.forEach((error) => {
    if (code === error) {
      hasError = true
      return
    }
  })

  if (hasError) {
    return ERRORS.New(error)
  }

  return error
}

export const GET_ERROR_MESSAGE = (error) => {
  const code = error?.code ? error.code : error
  switch (code) {
    case ERRORS.GENERAL:
    default:
      return '정의되지 않은 오류입니다'
    case ERRORS.DATA_NOT_FOUND:
      return '조회된 데이터가 없습니다'
    case ERRORS.REQUEST_BODY_IS_EMPTY:
      return '요청된 RequestBody의 내용이 없습니다'
    case ERRORS.INVALID_PARAMETER:
      return '파라메터 인자값이 정상적이지 않습니다'
    case ERRORS.PARENT_NOT_FOUND:
      return '존재하지 않는 회원입니다'
    case ERRORS.REQUESTED_PARENT_HAS_NO_BABY:
      return '생성요청한 회원정보가 아이정보를 가지고 있지 않습니다'
    case ERRORS.BABY_NOT_FOUND:
      return '존재하지 않는 아이입니다.'
    case ERRORS.REQUESTED_PARENT_GRADE_NOT_EXIST:
      return '요청한 회원등급은 존재하지 않습니다'
    case ERRORS.INVALID_ACCESS_TOKEN:
      return '잘못된 토큰입니다'
    case ERRORS.INVALID_REFRESH_TOKEN:
      return '만료된 토큰입니다. 토큰을 갱신하세요'
    case ERRORS.ACCOUNT_NOT_FOUND:
      return '가입된 계정이 없습니다'
    case ERRORS.PASSWORD_NOT_MATCH:
      return '비밀번호가 틀립니다'
    case ERRORS.NO_ACCOUNT_LINKED:
      return '연결된 계정이 없습니다'
    case ERRORS.PASSWORD_SAME:
      return '기존 패스워드와 동일합니다'
    case ERRORS.PASSWORD_INCLUDE_PERSONAL_INFORMATION:
      return '비밀번호에 개인정보가 포함되어 있습니다'
    case ERRORS.ALREADY_REGISTERED_ID:
      return '이미 가입된 아이디입니다'
    case ERRORS.ALREADY_REGISTERED_CI:
      return '이미 가입된 CI입니다'
    case ERRORS.ENCRYPTION_KEY_FOUND:
      return '암호화 키가 없습니다'
    case ERRORS.QA_NOT_MATCHED:
      return '답변이 일치하지 않습니다'
    case ERRORS.EXTERNAL_SERVER_KCB:
      return '휴대폰 본인 인증 서버가 문제가 있습니다'
    case ERRORS.EXTERNAL_SERVER_KAKAO:
      return 'NAVER 인증서버에 문제가 있습니다'
    case ERRORS.EXTERNAL_SERVER_NAVER:
      return 'KAKAO 인증서버에 문제가 있습니다'
    case ERRORS.EXTERNAL_SERVER_GOOGLE:
      return 'GOOGLE 인증서버에 문제가 있습니다'
    case ERRORS.EXTERNAL_SERVER_APPLE:
      return 'APPLE 인증서버에 문제가 있습니다'
    case ERRORS.EXTERNAL_SERVER_ENCRYPT:
      return '암호키 저장 서버에 문제가 있습니다'
    case ERRORS.INVALID_NAME_OR_RESIDENT_REGISTRATION_NUMBER:
      return '이름, 주민번호를 잘못 입력하셨습니다'
    case ERRORS.SERVICE_UNAVAILABLE:
      return '서비스 이용에 제한이 있습니다\n잠시 후 다시 시도해주시기 바랍니다'
    case ERRORS.INCORRECT_AUTH_CODE_ATTEMPTS_EXCEEDED:
      return '잘못된 인증번호 입력 횟수가 초과되었습니다\n다시 시도해주시기 바랍니다'
    case ERRORS.AUTH_CODE_NOT_MATCHED:
      return '인증번호가 일치하지 않습니다'
    case ERRORS.AUTH_CODE_RETRANSMISSION_REQUEST_TIMEOUT:
      return '인증번호 재전송 요청시간이 초과되었습니다\n서비스 종료 후 다시 시도해주시기 바랍니다'
    case ERRORS.MISCELLANEOUS:
      return '서비스 오류가 발생했습니다\n잠시 후 다시 시도해주시기 바랍니다'
    case ERRORS.JSON_PROCESSING_ERROR:
      return 'JSON 처리 오류입니다'
    case ERRORS.INVALID_IMAGE_URL:
      return '유효하지 않은 이미지 URL입니다'
    case ERRORS.PASSWORD_INCORRECT_ONCE:
      return '비밀번호가 틀립니다. 5회 실패시 계정이 차단됩니다(1/5)'
    case ERRORS.PASSWORD_INCORRECT_TWICE:
      return '비밀번호가 틀립니다. 5회 실패시 계정이 차단됩니다(2/5)'
    case ERRORS.PASSWORD_INCORRECT_THREE_TIMES:
      return '비밀번호가 틀립니다. 5회 실패시 계정이 차단됩니다(3/5)'
    case ERRORS.PASSWORD_INCORRECT_FOUR_TIMES:
      return '비밀번호가 틀립니다. 5회 실패시 계정이 차단됩니다(4/5)'
    case ERRORS.ACCOUNT_BLOCKED:
      return '계정이 차단되었습니다.\n 비밀번호 재설정 후, 로그인해 주세요'
  }
}

export const ERRORS = {
  New: (error) => {
    if (typeof error === 'number') {
      switch (error) {
        case ERRORS.INVALID_ACCESS_TOKEN:
          return {
            type: CUSTOM_ERROR,
            status: 401,
            code: error,
          }
        case ERRORS.INVALID_REFRESH_TOKEN:
          return {
            type: CUSTOM_ERROR,
            status: 401,
            code: error,
          }
        case ERRORS.INTERNAL_SERVER_ERROR:
          return {
            type: CUSTOM_ERROR,
            status: 500,
            code: error,
          }
        case ERRORS.DATA_NOT_FOUND:
          return {
            type: CUSTOM_ERROR,
            status: 404,
            code: error,
          }
        case ERRORS.INVALID_PARAMETER:
          return {
            type: CUSTOM_ERROR,
            status: 400,
            code: error,
          }
        case ERRORS.UNAUTHORIZED:
          return {
            type: CUSTOM_ERROR,
            status: 401,
            code: error,
          }
      }
    }
    return {
      type: CUSTOM_ERROR,
      status: error?.response?.status,
      code: error?.response?.data?.code,
    }
  },
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  NOT_FOUND: 404,
  INTERNAL_SERVER_ERROR: 500,
  GENERAL: 1000,
  DATA_NOT_FOUND: 1001,
  REQUEST_BODY_IS_EMPTY: 1002,
  INVALID_PARAMETER: 1003,
  PARENT_NOT_FOUND: 2001,
  REQUESTED_PARENT_HAS_NO_BABY: 2002,
  BABY_NOT_FOUND: 2003,
  REQUESTED_PARENT_GRADE_NOT_EXIST: 2004,
  INVALID_ACCESS_TOKEN: 4001,
  INVALID_REFRESH_TOKEN: 4002,
  ACCOUNT_NOT_FOUND: 4003,
  PASSWORD_NOT_MATCH: 4004,
  NO_ACCOUNT_LINKED: 4005,
  PASSWORD_SAME: 4006,
  PASSWORD_INCLUDE_PERSONAL_INFORMATION: 4007,
  ALREADY_REGISTERED_ID: 4008,
  ALREADY_REGISTERED_CI: 4009,
  ENCRYPTION_KEY_FOUND: 4010,
  QA_NOT_MATCHED: 4014,
  PASSWORD_INCORRECT_ONCE: 4015,
  PASSWORD_INCORRECT_TWICE: 4016,
  PASSWORD_INCORRECT_THREE_TIMES: 4017,
  PASSWORD_INCORRECT_FOUR_TIMES: 4018,
  ACCOUNT_BLOCKED: 4019,
  EXTERNAL_SERVER_KCB: 5001,
  EXTERNAL_SERVER_KAKAO: 5002,
  EXTERNAL_SERVER_NAVER: 5003,
  EXTERNAL_SERVER_GOOGLE: 5004,
  EXTERNAL_SERVER_APPLE: 5005,
  EXTERNAL_SERVER_ENCRYPT: 5006,
  INVALID_NAME_OR_RESIDENT_REGISTRATION_NUMBER: 5017,
  SERVICE_UNAVAILABLE: 5030,
  INCORRECT_AUTH_CODE_ATTEMPTS_EXCEEDED: 5130,
  AUTH_CODE_NOT_MATCHED: 5131,
  AUTH_CODE_RETRANSMISSION_REQUEST_TIMEOUT: 5136,
  MISCELLANEOUS: 5999,
  JSON_PROCESSING_ERROR: 9001,
  INVALID_IMAGE_URL: 9002,
  INVALID_ENVIRONMENT: 9998,
  EXTERNAL_SERVER_SNS: 9999,
  ECONNABORTED: 'ECONNABORTED'
}

export const GET_FRONT_ERROR_MESSAGE = (code) => {
  switch (code) {
    case ERRORS.INVALID_REFRESH_TOKEN:
      return '인증 정보가 만료되었습니다. 다시 로그인해 주세요.'
    case ERRORS.EXTERNAL_SERVER_SNS:
      return 'SNS 인증서버에 문제가 있습니다.'
    case ERRORS.INVALID_PARAMETER:
      return '파라미터 오류 입니다. 다시 시도해 주세요.'
    case ERRORS.INVALID_ENVIRONMENT:
      return '지원되지 않는 환경입니다.'
    case ERRORS.ECONNABORTED:
      return '일시적인 서버 오류입니다. 잠시 후 다시 시도해 주세요.'
  }
}
