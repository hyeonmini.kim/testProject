export const ENV_TYPE = {
  LOCAL: 'L',
  DEVELOP: 'D',
  STAGING: 'S',
  PRODUCT: 'P',
  MOCK: 'M',
  API: 'A',
}