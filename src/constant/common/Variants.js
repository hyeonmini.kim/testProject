export const FADE_DOWN = {
  open: {
    opacity: 1,
    y: 0,
    transition: {type: "spring", stiffness: 1700, damping: 200}
  },
  hidden: {
    opacity: 0,
    y: 30
  },
}