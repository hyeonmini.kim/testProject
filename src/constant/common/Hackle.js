export const HACKLE_TRACK = {

  LANDING: 'landing',                                     // 앱 오픈 이후 첫 화면 진입
  LOGIN: 'login',                                         // ID/PW 로그인 화면 진입

  // -------------------- 회원가입 --------------------
  SIGNUP_PERMISSION: 'signup_permission',                 // 앱 접근권한 허용 페이지 열릴 시
  SIGNUP_TERMS: 'signup_terms',                           // 약관동의 화면이 오픈 되었을 때
  SIGNUP_CERTIFICATION: 'signup_certification',           // 본인인증 화면이 오픈되었을 때
  SIGNUP_CERTIFICATION_CODE: 'signup_certification_code', // 본인 인증 완료 되었을 때
  SIGNUP_ID: 'signup_id',                                 // 아이디 설정 화면이 오픈되었을 때
  SIGNUP_PW: 'signup_pw',                                 // 비밀번호 설정 화면이 오픈 되었을 때
  SIGNUP_EMAIL: 'signup_email',                           // 이메일 설정 화면이 오픈 되었을 때
  SIGNUP_NAME: 'signup_name',                             // 부모 닉네임 화면이 오픈 되었을 때
  SIGNUP_KIDS_NAME: 'signup_kids_name',                   // 아이 닉네임 화면이 오픈 되었을 때
  SIGNUP_KIDS_BIRTH: 'signup_kids_birth',                 // 생년월일 입력 화면이 오픈 되엇을 때
  SIGNUP_KIDS_HEIGHT: 'signup_kids_height',               // 키 정보 입력 화면이 오픈 되었을 때
  SIGNUP_KIDS_WEIGHT: 'signup_kids_weight',               // 몸무게 정보 입력 화면이 오픈 되었을 때
  SIGNUP_KIDS_GENDER: 'signup_kids_gender',               // 성별 입력 화면이 오픈 되었을 때
  SIGNUP_KIDS_ALERGY: 'signup_kids_alergy',               // 알레르기 정보 입력 화면이 오픈 되었을 때
  SIGNUP_KIDS_KEYWORD: 'signup_kids_keyword',             // 관심 키워드 화면이 오픈 되엇을 때
  SIGNUP_COMPLETE: 'signup_complete',                     // 가입완료 화면이 오픈 되었을 때
  SIGNUP_COMPLETE_PHOTO: 'signup_complete_photo',         // 가입완료 > 프로필 사진 선택 클릭 시
  SIGNUP_COMPLETE_LATER: 'signup_complete_later',         // 가입완료 > 다음에 하기 버튼 클릭 시
  SIGNUP_COMPLETE_PROFILE_COMPLETE: 'signup_complete_profile_complete', // 가입완료 > 프로필 사진 설정 완료 후
  // -----------------------------------------------

  // -------------------- 둘러보기 --------------------
  INDEX_PUBLIC: 'index_public',                           // 둘러볼래요 메인 화면 진입
  INDEX_LOGOUT_BN01: 'index_logout_bn01',                 // 첫 번째 배너 클릭 시
  INDEX_LOGOUT_BN02: 'index_logout_bn02',                 // 두 번째 배너 클릭 시
  INDEX_LOGOUT_BN03: 'index_logout_bn03',                 // 세 번째 배너 클릭 시
  INDEX_LOGOUT_KEYWORD: 'index_logout_keyword',           // 건강키워드 클릭 시
  INDEX_LOGOUT_BN_HOT: 'index_logout_bn_hot',             // 핫 도낫츠 클릭 시
  INDEX_LOGOUT_SEASON: 'index_logout_season',             // 제철재료 레시피 클릭 시
  INDEX_LOGOUT_BN_BOTTOM: 'index_logout_bn_bottom',       // 레시피 배너 클릭 시
  CLICK_PUBLIC_GNB_RANKING: 'click_public_gnb_ranking',   // 둘러볼래요 > GNB 랭킹 클릭
  CLICK_PUBLIC_GNB_SEARCH: 'click_public_gnb_search',     // 둘러볼래요 > GNB 검색 클릭
  CLICK_PUBLIC_GNB_WRITE: 'click_public_gnb_write',       // 둘러볼래요 > GNB 레시피 작성 클릭
  CLICK_PUBLIC_GNB_MY: 'click_public_gnb_my',             // 둘러볼래요 > GNB 마이 클릭
  // -----------------------------------------------

  // ---------------------- 홈 -----------------------
  INDEX: 'index',                                         // 로그인 후 메인 화면 진입 시
  INDEX_DYNAMIC: 'index_dynamic',                         // 아일랜드 버튼 클릭 시
  ALARM: 'alarm',                                         // 개인알림 페이지 오픈 시
  INDEX_GROW: 'index_grow',                               // 성장도움레시피 클릭 시
  CONTENTS: 'contents',                                   // 전문가 콘텐츠 배너 클릭 시
  INDEX_PERSONAL: 'index_personal',                       // 우리아이 추천 레시피 클릭 시
  INDEX_PERSONAL_MORE: 'index_personal_more',             // 우리아이레시피 더보기 버튼 클릭 시
  INDEX_KEYWORD: 'index_keyword',                         // 건강 키워드 클릭 시
  INDEX_HOT_DONOTS: 'index_hot_donots',                   // 핫도나츠 클릭 시
  INDEX_SEASON: 'index_season',                           // 제철 레시피 클릭 시
  INDEX_SEASON_MORE: 'index_season_more',                 // 제철 레시피 배너 더보기 클릭시
  INDEX_BN_BOTTOM: 'index_bn_bottom',                     // 제철 레시피 배너 클릭 시
  CLICK_GNB_HOME: 'click_gnb_home',                       // GNB 홈 클릭
  CLICK_GNB_RANKING: 'click_gnb_ranking',                 // GNB 랭킹 클릭
  CLICK_GNB_WRITE: 'click_gnb_write',                     // GNB 레시피 작성 클릭
  CLICK_GNB_SEARCH: 'click_gnb_search',                   // GNB 검색 클릭
  CLICK_GNB_MY: 'click_gnb_my',                           // GNB 마이 클릭
  // ------------------------------------------------

  // ---------------------- 레시피 작성 -----------------------
  WRITE_STEP1: 'write_step1',                             // 레시피 작성 페이지가 오픈되었을 때
  WRITE_STEP2: 'write_step2',                             // 레시피 기본정보 입력 완료 버튼 클릭 시
  WRITE_STEP3: 'write_step3',                             // 래시피 부가정보 입력 완료 버튼 클릭 시
  WRITE_STEP4: 'write_step4',                             // 레시피 재료입력 버튼 클릭 시
  WRITE_STEP3_INGREDIENT1: 'write_step3_ingredient1',     // 기본재료 입력하기 버튼 클릭 시
  WRITE_STEP3_INGREDIENT1_SEARCH: 'write_step3_ingredient1_search',     // 기본재료 검색 아이콘 클릭 시
  WRITE_STEP3_INGREDIENT1_FREQUENTLY_USED: 'write_step3_ingredient1_frequently_used',   // 기본재료 입력하기 > 자주 사용하는 재료 선택 시
  WRITE_STEP3_INGREDIENT1_EDIT: 'write_step3_ingredient1_edit',    // 기본재료 편집하기 선택 시
  WRITE_STEP3_UNIT_MODIFY: 'write_step3_unit_modify',     // 계량단위보기
  WRITE_STEP3_INGREDIENT2: 'write_step3_ingredient2',     // 양념재료 입력하기 버튼 클릭 시
  WRITE_STEP3_INGREDIENT2_SEARCH: 'write_step3_ingredient2_search',     // 양념재료 검색 아이콘 클릭 시
  WRITE_STEP3_INGREDIENT2_FREQUENTLY_USED: 'write_step3_ingredient2_frequently_used',   // 양념재료 입력하기 > 자주 사용하는 양념재료 선택 시
  RECIPE_WRITE_STEP3_RESET: 'recipe_write_step3_reset',           // 양념재료 초기화 버튼 클릭 시
  RECIPE_WRITE_STEP3_SOYSAUCE: 'recipe_write_step3_soysauce',     // 양념재료 간장 베이스 버튼 클릭 시
  RECIPE_WRITE_STEP3_REDPEPPER: 'recipe_write_step3_redpepper',   // 양념 재료 고추장 버튼 클릭 시
  WRITE_STEP3_INGREDIENT2_EDIT: 'write_step3_ingredient2_edit',   // 양념재료 편집하기 선택 시
  WRITE_UNIT: 'write_unit',                                       // 재료별 단위 수정  화면 오픈 시
  WRITE_STEP4_STEP: 'write_step4_step',                           // 레시피 재료입력 버튼 클릭 시
  WRITE_PREVIEW: 'write_preview',                                 // 레시피 미리보기 화면 버튼 클릭 시
  RECIPE_PREVIEW_MODIFY: 'recipe_preview_modify',                 // 래시피 수정하기 버튼 클릭 시
  RECIPE_UPLOAD: 'recipe_upload',                                 // 래시피 업로드하기 버튼 클릭 시
  RECIPE: 'recipe',                                               // 레시피 상세보기 페이지가 오픈되었을 때
  RECIPE_DETAIL_DONOTEAT: 'recipe_detail_donoteat',               // 섭취자제 팝업 오픈 시
  RECIPE_DETAIL_ALERGY: 'recipe_detail_alergy',                   // 알레르기 재료 팝업 오픈 시
  RECIPE_DETAIL_NUTRITION: 'recipe_detail_nutrition',             // 영양소 특성정보 보기 팝업 오픈 시
  RECIPE_DETAIL_HEALTHTAG: 'recipe_detail_healthtag',             // 건강테그 더보기
  RECIPE_DETAIL_HEALTHNOTE: 'recipe_detail_healthnote',           // 건강노트 보기
  RECIPE_DETAIL_TAB_NUTRITION: 'recipe_detail_tab_nutrition',       // 영양정보 보기 버튼 클릭 시
  RECIPE_DETAIL_TAB_RECIPE: 'recipe_detail_tab_recipe',           // 만드는 법 버튼 클릭 시
  RECIPE_DETAIL_TAB_REVIEW: 'recipe_detail_tab_review',           // 리뷰 클릭 시
  RECIPE_DETAIL_SCRAP: 'recipe_detail_scrap',                     // 레시피 담기를 완료 했을 때
  RECIPE_DETAIL_REVIEW: 'recipe_detail_review',                   // 감정 이모티콘 버튼 클릭 시
  RECIPE_DETAIL_SHARE: 'recipe_detail_share',                     // 공유하기 버튼 클릭 시
  // -------------------------------------------------------

  // ---------------------- 검색 -----------------------
  SEARCH: 'search',                                               // 레시피 검색화면 오픈 시
  SEARCH_POPULAR: 'search_popular',                               // 인기 검색어 선택 시
  SEARCH_KEYWORD: 'search_keyword',                               // 건강키워드별 버튼 클릭 시
  SEARCH_INGREDIENT: 'search_ingredient',                         // 재료별 버튼 클릭 시
  SEARCH_CATEGORY: 'search_category',                             // 카테고리별 버튼 클릭 시
  SEARCH_NORESULT: 'search_noresult',                             // 검색결과가 없을 시 오픈되는 페이지
  SEARCH_LIST: 'search_list',                                     // 레시피 검색 결과 페이지 오픈 시
  LIST_FILTER_ALL: 'list_filter_all',                             // 필터버튼 전체
  LIST_FILTER_EARLY: 'list_filter_early',                         // 필터버튼 초기
  LIST_FILTER_MID: 'list_filter_mid',                             // 필터버튼 중기
  LIST_FILTER_LATE: 'list_filter_late',                           // 필터버튼 완료
  LIST_FILTER_CHILD_LATE: 'list_filter_child_late',               // 필터버튼 후기
  LIST_FILTER_CHILD_EARLY: 'list_filter_child_early',             // 필터버튼 전기
  LIST_TOGGLE_DONOT_ON: 'list_toggle_donot_on',                   // 피해야할 재료 토글 버튼 선택 시
  // --------------------------------------------------

  // ---------------------- 마이 -----------------------
  MYPAGE_MY_RECIPE: 'mypage_my_recipe',                           // 마이 페이지가 오픈되었을 때
  MY_INFO: 'my_info',                                             // 마이 페이지 > 나의 정보 오픈되었을 때
  MY_INFO_UNREGISTER: 'my_info_unregister',                       // 회원탈퇴 버튼 클릭
  MY_INFO_LOGOUT: 'my_info_logout',                               // 로그아웃 버튼 클릭
  MY_INFO_PHOTO: 'my_info_photo',                                 // 마이 페이지 > 나의 정보 > 프로필 사진 선택
  MY_CHILD_INFO: 'my_child_info',                                 // 마이 페이지 > 아이 정보 오픈되었을 때
  MY_SNS: 'my_sns',                                               // 마이페이지 내 SNS 링크 버튼 클릭 시
  MY_LEVEL_DOUGH: 'my_level_dough',                               // 마이페이지 내 레벨업 (도우)
  MY_LEVEL_CHOCO: 'my_level_choco',                               // 마이페이지 내 레벨업 (초코)
  MY_LEVEL_KING: 'my_level_king',                                 // 마이페이지 내 레벨업 (크런치킹)
  MY_TAB_RECIPE: 'my_tab_recipe',                                 // 마이페이지 내 레시피 버튼 클릭 시
  MY_TAB_SCRAP: 'my_tab_scrap',                                   // 마이페이지 내 스크랩 버튼 클릭 시
  MYPAGE_ADD_CHILD: 'mypage_add_child',                           // 마이페이지 내 아이등록 추가 페이지 오픈 시
  MYPAGE_MEMBERSHIP: 'mypage_membership',                       // 마이페이지 내 멤버십 링크 클릭 시
  MYPAGE_YOU: 'mypage_you',                                       // 다른사람(도우,초코) 마이페이지가 열렸을 시
  MYPAGE_EXPERT: 'mypage_expert',                                 // 전문가 마이페이지가 열렸을 시
  EXPERT_SNS: 'expert_sns',                                       // 전문가 마이페이지 내 SNS 링크 버튼 클릭 시
  EXPERT_TAB_RECIPE: 'expert_tab_recipe',                         // 전문가 마이페이지 내 레시피 버튼 클릭 시
  EXPERT_TAB_SCRAP: 'expert_tab_scrap',                           // 전문가 마이페이지 내 스크랩 버튼 클릭 시
  MYPAGE_CRUNCH: 'mypage_crunch',                                 // 크런치킹 마이페이지가 열렸을 시
  CRUNCH_SNS: 'crunch_sns',                                       // 크런치킹 마이페이지 내 SNS 링크 버튼 클릭 시
  CRUNCH_TAB_RECIPE: 'crunch_tab_recipe',                         // 크런치킹 마이페이지 내 레시피 버튼 클릭 시
  CRUNCH_TAB_SCRAP: 'crunch_tab_scrap',                           // 크런치킹 마이페이지 내 스크랩 버튼 클릭 시
  // --------------------------------------------------

  // ---------------------- 랭킹 -----------------------
  RANKING: 'ranking',                                             // 랭킹 페이지 진입 시
  RANKING_SPRINGCLE: 'ranking_springcle',                         // 스프링클 페이지가 오픈 되었을 때
  RANKING_EXPERT: 'ranking_expert',                               // 전문가 페이지가 오픈 되었을 때
  // --------------------------------------------------


  // -------------------- 실험실 --------------------
  INDEX_LOGOUT_NUTRIENTS: 'index_logout_nutrients',               // 둘러볼래요 > 영양제 배너 클릭
  INDEX_LOGOUT_BABYFOOD: 'index_logout_babyfood',                 // 둘러볼래요 > 이유식 배너 클릭
  INDEX_NUTRIENTS: 'index_nutrients',                             // 홈 > 영양제 배너 클릭
  NUTRIENTS_BTN: 'nutrients_btn',                                 // 아이 영양제 정보 입력 버튼 클릭
  NUTRIENTS_SELECT: 'nutrients_select',                           // 영양제 선택 화면 진입 (영양제 정보 입력)
  NUTRIENTS_SELECT_LOCTO_STEP1: 'nutrients_select_locto_step1',   // '다음' 버튼 클릭 (브랜드 선택 완료)
  NUTRIENTS_SELECT_LOCTO_STEP2: 'nutrients_select_locto_step2',   // '다음' 버튼 클릭 (기호 선택  완료)
  NUTRIENTS_SELECT_VITAMIN_STEP1: 'nutrients_select_vitamin_step1',   // '다음' 버튼 클릭 (브랜드 선택 완료)
  NUTRIENTS_SELECT_VITAMIN_STEP2: 'nutrients_select_vitamin_step2',   // '다음' 버튼 클릭 (기호 선택  완료)
  NUTRIENTS_SELECT_IRON_STEP1: 'nutrients_select_iron_step1',     // '다음' 버튼 클릭 (브랜드 선택 완료)
  NUTRIENTS_SELECT_IRON_STEP2: 'nutrients_select_iron_step2',     // '다음' 버튼 클릭 (기호 선택  완료)
  NUTRIENTS_SELECT_IRON_STEP3: 'nutrients_select_iron_step3',     // '다음' 버튼 클릭 (직접 입력 완료)
  NUTRIENTS_MAIN: 'nutrients_main',                               // 영양제 메인 화면 진입
  NUTRIENTS_MAIN_FILTER_LOCTO: 'nutrients_main_filter_locto',     // 유산균 필터 클릭
  NUTRIENTS_MAIN_FILTER_VITAMIN: 'nutrients_main_filter_vitamin', // 비타민 필터 클릭
  NUTRIENTS_MAIN_FILTER_IRON: 'nutrients_main_filter_iron',       // 철분 필터 클릭
  INDEX_BABYFOOD: 'index_babyfood',                               // 이유식 배너 클릭
  BABYFOOD_BTN: 'babyfood_btn',                                   // 이유식 정보 입력 버튼 클릭
  BABYFOOD_SELECT_STEP1: 'babyfood_select_step1',                 // '다음' 버튼 클릭 (브랜드 선택 완료)
  BABYFOOD_SELECT_STEP2: 'babyfood_select_step2',                 // '다음' 버튼 클릭 (기호 선택  완료)
  BABYFOOD_MAIN: 'babyfood_main',                                 // 이유식 메인 화면 진입
  ETC_POPUP: 'etc_popup',                                         // 기능평가 화면 진입
  ETC_BTN: 'etc_btn',                                             // '다음' 버튼 클릭 (평가완료)


  // ----------------------------------------------

  INFLOW: 'inflow', // 유입

}