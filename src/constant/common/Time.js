export const TIME = {
  MS_THIRTY_MINUTE: 30 * 60 * 1000,
  MS_FIVE_MINUTE: 5 * 60 * 1000,
  MS_THIRTY_SECOND: 30 * 1000,
  MS_ONE_DAY: 24 * 60 * 60 * 1000,
}