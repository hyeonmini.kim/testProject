export const ALLERGY_TYPE = {
  NONE: '없음',
  EGG: '난류',
  MILK: '우유',
  BUCKWHEAT: '메밀',
  PEANUT: '땅콩',
  SOYBEAN: '대두',
  WHEAT: '밀',
  PINE_NUT: '잣',
  WALNUT: '호두',
  CRAB: '게',
  SHRIMP: '새우',
  SQUID: '오징어',
  MACKEREL: '고등어',
  SHELLFISH: '조개류',
  PEACH: '복숭아',
  TOMATO: '토마토',
  CHICKEN: '닭고기',
  PORK: '돼지고기',
  BEEF: '쇠고기',
}

export const KEYWORD_TYPE = {
  COLD: '감기/수족구',
  CONSTIPATION: '변비',
  LACTOSE: '유당불내증',
  IMBALANCED: '편식',
  SKIN: '피부 건강',
  MUSCLE: '근육 강화',
  BONE: '뼈 건강',
  ANEMIA: '빈혈',
  MOUTH: '입/혀 염증',
  GUMS: '잇몸 출혈',
  EYEBALL: '안구건조증',
}