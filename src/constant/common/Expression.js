export const EXPRESSION = {
  EXCLUDE_SPECIAL: {
    ALL: (str) => {
      return str.replace(/[`~!@#$%^&*()_|+\-=?;,.:'"<>\{\}\[\]\\\/ ]/gim, '')
    },
    ALLOW_COMMA: (str) => {
      let temp = str.replace(/[`~!@#$%^&*()_|+\-=?;:'"<>\{\}\[\]\\\/ ]/gim, '')
      temp = temp.charAt(0) == ',' ? '' : temp
      temp = temp.replace('..', '.')
      return temp.replace(',,', ',')
    },
  },
  NUMBER_POINT: {
    POINT_ONE: (value, cipher) => {
      if (value.charAt(0) === '.') {
        return ''
      }
      if (value.length > 1 && value.charAt(0) === '0' && value.charAt(1) !== '.') {
        value = value.replace('0', '');
      }
      if (value.indexOf('.') >= 0) {
        value = (Math.floor(Number(value) * 10) / 10).toString()
        value = value.substring(0, cipher + 1)
      } else {
        value = value.substring(0, cipher)
      }
      if (value.charAt(value.length - 1) === '.') {
        value = value.replace('.', '');
      }
      return value
    }
  }
}