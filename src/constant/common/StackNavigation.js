import {MOMSTAR_COMMON} from "../momstar/Momstar";
import {RECIPE_RESULT_STATUS, RECIPE_SEARCH_AGE_GROUP_TYPE, RECIPE_SEARCH_HOME_HEALTH} from "../recipe/Search";

export const STACK = {
  NONE: {
    TYPE: 'NONE',
    DATA: {}
  },
  HOME: {
    TYPE: 'HOME',
    DATA: {}
  },
  GUEST: {
    TYPE: 'GUEST',
    DATA: {}
  },
  MOMSTAR: {
    TYPE: 'MOMSTAR',
    DATA: {
      activeTab: MOMSTAR_COMMON.MOMSTAR.TAB,
      isScroll: false,
      items: [],
      scrollY: 0,
      pageOffset: 0,
      hasNextOffset: false,
    }
  },
  MY_SELF: {
    TYPE: 'MY_SELF',
    DATA: {
      activeTab: 'recipe',
      isExpanded: false,
      myWriteList: [],
      myScrapList: [],
      selectedFilter: '전체',
      isSelectedFilter: false,
      scrollY: 0,
    }
  },
  MY_OTHERS: {
    TYPE: 'MY_OTHERS',
    DATA: {
      activeTab: 'recipe',
      othersWriteList: [],
      othersScrapList: [],
      scrollY: 0,
    }
  },
  MY_EXPERT: {
    TYPE: 'MY_EXPERT',
    DATA: {
      activeTab: 'recipe',
      othersWriteList: [],
      othersScrapList: [],
      scrollY: 0,
    }
  },
  MY_SETTING: {
    TYPE: 'MY_SETTING',
    DATA: {
      activeTab: 'recipe',
      items: [],
      scrollY: 0,
    }
  },
  RECIPE_CREATE: {
    TYPE: 'RECIPE_CREATE',
    DATA: {}
  },
  RECIPE_SEARCH: {
    TYPE: 'RECIPE_SEARCH',
    DATA: {
      search: '',
      result: RECIPE_RESULT_STATUS.INIT,
      isCall: null,
      isTitle: false,
      isSearch: false,
      category: '',
      isAvoid: false,
      ageGroup: RECIPE_SEARCH_AGE_GROUP_TYPE.ALL,
      activeTab: RECIPE_SEARCH_HOME_HEALTH.NAME,
      showFilter: true,
      items: [],
      scrollY: 0,
    }
  },
  RECIPE_DETAIL: {
    TYPE: 'RECIPE_DETAIL',
    DATA: {}
  },
  LAB: {
    TYPE: 'LAB',
    DATA: {}
  },
}

export const STACK_PRE_FETCHING = {
  from: STACK.NONE.TYPE,
  path: '',
  data: STACK.NONE.DATA
}