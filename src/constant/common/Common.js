export const COMMON_TEXT = {
  CONFIRM: '확인',
}

export const COMMON_DIALOG_TYPE = {
  NO_BUTTON: 'NO_BUTTON',
  ONE_BUTTON: 'ONE_BUTTON',
  TWO_BUTTON: 'TWO_BUTTON',
}

export const ISAUTH_INIT = undefined
export const INIT = undefined

export const COMMON_DIALOG = {
  type: COMMON_DIALOG_TYPE.TWO_BUTTON,
  message: '',
  isBoldMsg: false,
  subMessage: '',
  children: (children) => children,
  handleButton1: {text: '', onClick: undefined},
  handleButton2: {text: '', onClick: undefined},
}

export const TOAST_ENV = {
  MOBILE: 'M',
  DESKTOP: 'D'
}

export const TOAST_TYPE = {
  BOTTOM_HEADER: 'BOTTOM_HEADER',
  BOTTOM_DIALOG_HEADER: 'BOTTOM_DIALOG_HEADER',
  BOTTOM_DIALOG_HEADER_ERROR: 'BOTTOM_DIALOG_HEADER_ERROR',
  BOTTOM_SYSTEM_ERROR: 'BOTTOM_SYSTEM_ERROR',
  DESKTOP_BOTTOM_HEADER: 'DESKTOP_BOTTOM_HEADER',
}

export const COMMON_STR = {
  OK: 'ok',
  SNS: 'sns',
  ID: 'id',
  UUID: 'uuid',
  CI: 'ci',
  ACCOUNT_KEY: 'accountKey',
  GUEST: 'guest',
  SNS_TYPE: 'snsType',
  CALLBACK_TYPE: 'callbackType',
}

export const PROVIDER_TYPE = {
  NAVER: 'naver',
  KAKAO: 'kakao',
  GOOGLE: 'google',
}

export const IMAGE_COMPRESSION_OPTIONS = {
  maxSizeMB: 0.2, // 이미지 최대 용량
  maxWidthOrHeight: 1920, // 최대 넓이(혹은 높이)
  useWebWorker: true,
};

export const clickBorderNone = {
  '&:focus': {
    outline: 'none'
  },
  '&:focus:focus-visible': {
    outline: 'revert',
  }
}