//--- 유저정보 ---//
export const USER = {
  user_key: 0,
  name: '',
  phone: '',
  birth: '',
  gender: '',
  email: '',
  password: '',
  nickname: '',
  avatar: '',
  isMomStar: false,
  children: [],
  level: 0,
  snsLink: '',
  introduction: '',
  snsAccount: {},
}

export const CHILD = {
  id: 0,
  nickname: '',
  gender: '',
  birth: '',
  weight: '',
  height: '',
  allergy: [],
  keyword: [],
  avatar: '',
}

export const SNS_ACCOUNT_STATUS = {
  isNaver: false,
  isKakao: false,
  isApple: false,
  isGoogle: false,
}

export const USER_GRADE = {
  LV1: {
    GRADE: 'LV1',
    TEXT: '도우',
  },
  LV2: {
    GRADE: 'LV2',
    TEXT: '초코',
  },
  LV3: {
    GRADE: 'LV3',
    TEXT: '크런치킹',
  },
  LV4: {
    GRADE: 'LV4',
    TEXT: '전문가',
  },
}

export const IDENTIFICATION_QA_INFORM = {
  questionNum: '',
  firstQuestionCode: '',
  secondQuestionCode: '',
  firstQuestionText: '',
  secondQuestionText: '',
  firstAnswer: '',
  secondAnswer: '',
}
