export const PAGINATION = {
  PAGE: 0,
  PAGE_SIZE: 15,
  INIT_DATA: undefined,
  NO_DATA: []
}

export const DEFAULT_PAGE_PARAMS = {
  params: {
    page: 0,
    size: PAGINATION.PAGE_SIZE
  }
}

export const SEARCH_PAGINATION = {
  PAGE: 0,
  PAGE_SIZE: 10,
}

export const RANKING_PAGINATION = {
  PAGE: 0,
  PAGE_SIZE: 15,
}

export const RANKING_PAGE_PARAMS = {
  params: {
    page: 0,
    size: RANKING_PAGINATION.PAGE_SIZE,
  }
}