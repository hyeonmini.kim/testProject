export const URL_TYPE = {
  D_IFRAME: "d_iframe",
  D_RECIPE: "d_recipe",
  D_HOME: "d_home",
  D_RANKING: "d_ranking",
  D_CREATE: "d_create",
  D_SEARCH: "d_search",
  D_MY: "d_my",
  D_NOTICE: "d_notice",
  D_FAQ: "d_faq",
  D_EVENT: "d_event",
  D_OTHERS: "d_others",
  D_EXPERT: "d_expert",
}