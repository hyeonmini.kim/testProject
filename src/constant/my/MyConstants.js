import {USER_GRADE} from "../common/User";

export const MY_COMMON = {
  TEXT: {
    TITLE: (isNoShow) => isNoShow ? '' : 'MY 프로필',
    HOMEPAGE: '홈페이지',
    SNS_ADDRESS: 'SNS주소',
    TITLE_OTHER: '프로필',
    INFORMATION: '나의 정보',
    CHILDREN_INFORM: '아이 정보',
    NAME_NICKNAME: '이름/닉네임',
    NICK_NICKNAME: '별명/닉네임',
    HEIGHT: '키',
    WEIGHT: '몸무게',
    GENDER: '성별',
    EMAIL: '이메일',
    BIRTH: '생년월일',
    PHONE: '휴대전화',
    CERTIFICATION: '본인인증',
    SNS_LINK: 'SNS 링크(URL)',
    LEVEL_MESSEAGE: '크런치킹 등급이 되면 이용할 수 있습니다.',
    INTRODUCTION: '한줄소개',
    INTRODUCTION_MESSAGE: '한줄소개 입력은 25자까지 가능합니다.',
    INTRODUCTION_DEFAULT_MESSAGE: '한줄소개를 통해 관심사나 취향을 표현해보세요',
    DONOTS_MEMBERSHIP_PROPOSAL: '목표를 채우고 새로운 등급에 도전하세요',
    DONOTS_MEMBERSHIP_WRITE_COUNT: '글작성 수',
    DONOTS_MEMBERSHIP_SCRAP_COUNT: '스크랩 수',
    DONOTS_MEMBERSHIP_REVIEW_COUNT: '리뷰 수',
    MEMBERSHIP_INFO: '등급별혜택보기',
    PASSWORD: '비밀번호 변경하기',
    SNS_ACCOUNT: 'SNS 연동계정관리',
    WITHDRAW: '회원탈퇴',
    LOGOUT: '로그아웃',
    NOTICE_ACCOUNT_DELETE: '*연동계정을 삭제한 후 다시 연결하려면\n회원가입 절차를 진행해야합니다.',
    ACCOUNT_CONNECTED_NOW: '현재 연결된 계정',
    ACCOUNT_NOT_CONNECTED: (label) => {
      return `연결된 ${label}계정이` + '\n' + `없습니다`
    },
    NOTICE_WITHDRAW: '회원 탈퇴를 이용해주세요',
    NOTICE_WITHDRAW_PATH: '계정 설정 > 회원탈퇴',
    WITHDRAW_TITLE: '정말 탈퇴 하시겠습니까?\n떠나는 이유를 알려주세요',
    WITHDRAW_PLACEHOLDER: '기타이유를 작성해주세요. 최대 한글 30자입니다.',
    WITHDRAW_POPUP_MESSAGE: '탈퇴하시면 모든 회원정보가 삭제됩니다.\n그래도 정말 탈퇴하시겠습니까?',
    DELETE_CHILDREN: '아이 삭제시 정보가 모두 사라집니다\n그래도 삭제하시겠어요?',
    CANT_DELETE_CHILDREN: '등록된 아이가 한명일 경우\n삭제가 불가능해요',
    SNS_LINK_NOT_EXIST: '연동된 링크가 없어요',
    SNS_LINK_NOT_EXIST_ADD_LINK: '연동된 링크가 없어요\nSNS주소를 연동해주세요',
    SNS_LINK_NOT_EXIST_DETAIL: 'SNS주소를 연결하고 다양한 모습을 보여주세요',
    MEMBERSHIP: 'Membership',
    DONOTS_MEMBERSHIP_TITLE: '도낫츠\n멤버쉽',
    DONOTS_MEMBERSHIP_DESC_1: `도낫츠 멤버십이 준비한`,
    DONOTS_MEMBERSHIP_DESC_2: `과\n서비스의 주인공이 되어보세요!`,
    DONOTS_MEMBERSHIP_DESC_SPECIAL: '특별한 혜택',
    DONOTS_MEMBERSHIP_ACCORDION_TITLE: '더 많은 정보를 확인해보세요',
    DONOTS_MEMBERSHIP_ACCORDION_DESC: [
      '멤버십 등급 선정은 회원가입일로부터 전월까지의 활동 내역으로 매월 1일 결정합니다.',
      '레시피 작성은 업로드 완료일 기준으로 산정됩니다.',
      '도낫츠 멤버십 혜택은 당사 사정에 따라 사전 고지 없이 변경될 수 있으며, 이 경우 공지사항을 통해 지체없이 공지 합니다.',
      '스프링클도낫 회원의 홈 배너 노출은 등급 달성 시 최초 1회만 노출되며 매월 등급 달성한 회원수에 비례하여 랜덤하게 노출됩니다.',
    ],
    DONOTS_MEMBERSHIP_BENEFIT: '멤버쉽 혜택',
    DONOTS_MEMBERSHIP_CONDITION: '멤버쉽 조건',
    DONOTS_MEMBERSHIP_GRADE: [
      '',
      '글레이즈드',
      '초코',
      '스프링클',
    ],
    AGREE_MKT: (year, month, day) => `고객님은 ${year}년 ${month}월 ${day}일\n마케팅 등을 위한 개인정보 수집/이용에\n동의하셨습니다.`,
    DISAGREE_MKT: (year, month, day) => `고객님은 ${year}년 ${month}월 ${day}일\n마케팅 등을 위한 개인정보 수집/이용\n동의를 철회하셨습니다.`,
    AGREE_RECEIVE: (year, month, day) => `고객님은 ${year}년 ${month}월 ${day}일\n전송매체별 광고성 정보 수신에\n동의하셨습니다.`,
    DISAGREE_RECEIVE: (year, month, day) => `고객님은 ${year}년 ${month}월 ${day}일\n전송매체별 광고성 정보 수신에\n미동의하셨습니다.`,
  },
  MY_TAB_WRITE: 'write',
  MY_TAB_SCRAP: 'scrap',
  RECIPE_FILTER: '필터',
  RECIPE_NO_ITEM: '작성한 레시피가 없습니다.',
  RECIPE_NO_ITEM_FILTER: '해당하는 레시피가 없습니다.',
  RECIPE_NO_ITEM_SUB: '나만의 레시피를 작성해주세요',
  SCRAP_NO_ITEM: '스크랩한 레시피가 없습니다.',
  SCRAP_NO_ITEM_SUB: '나만의 레시피를 저장해보세요',
}

export const MY_TABS = [
  {
    value: MY_COMMON.MY_TAB_WRITE,
    label: '레시피',
  },
  {
    value: MY_COMMON.MY_TAB_SCRAP,
    label: '스크랩',
  },
];

export const RECIPE_STATUS_TEXT = {
  ALL: '전체',
  POSTING: '게시중',
  TEMPORARY: '임시저장',
  EXAMINATION: '검수중',
  REJECT: '반려',
}

export const RECIPE_CHECK_STATUS = [
  RECIPE_STATUS_TEXT.ALL,
  RECIPE_STATUS_TEXT.TEMPORARY,
  RECIPE_STATUS_TEXT.REJECT,
  RECIPE_STATUS_TEXT.EXAMINATION,
  RECIPE_STATUS_TEXT.POSTING
]

export const SNS_ACCOUNT_LIST = [
  {
    value: 'isNaver',
    label: '네이버',
  },
  {
    value: 'isKakao',
    label: '카카오',
  },
  {
    value: 'isApple',
    label: '애플',
  },
  {
    value: 'isGoogle',
    label: '구글',
  },
];

export const MY_BTN_TEXT = {
  DELETE_CONNECTED_ACCOUNT: '연동계정 삭제하기',
  CONNECT_ACCOUNT: '연결하기',
  CANCEL: '취소',
  WITHDRAW: '탈퇴하기',
  DELETE: '삭제',
  CONFIRM: '확인',
  DO_DELETE: '삭제하기',
};

export const MY_WITHDRAW_REASON_LIST = [
  '원하는 콘텐츠가 별로 없어서', '사용하기가 불편해서', '시스템 에러 및 서비스 속도 때문에', '개인정보 노출 우려', '기타'
];

export const MY_CALL = {
  CHILDREN_INFORM: 'CHILDREN_INFORM'
}

export const MY_GRADE_TYPE = {
  NORMAL: 'NORMAL_MEMBER',
  EXPERT: 'EXPERT',
}

export const MY_GRADE = {
  LV1: {
    GRADE_IMAGE: '/assets/component/membership/img/donots_dough.svg',
    IMAGE_WIDTH: '32px',
    IMAGE_HEIGHT: '18.21px',
    GRADE_TEXT: USER_GRADE.LV1.TEXT
  },
  LV2: {
    GRADE_IMAGE: '/assets/component/membership/img/donots_choco.svg',
    IMAGE_WIDTH: '28px',
    IMAGE_HEIGHT: '28px',
    GRADE_TEXT: USER_GRADE.LV2.TEXT
  },
  LV3: {
    GRADE_IMAGE: '/assets/component/membership/img/donots_crunchy_king.svg',
    IMAGE_WIDTH: '28px',
    IMAGE_HEIGHT: '28px',
    GRADE_TEXT: USER_GRADE.LV3.TEXT
  },
  LV4: {
    GRADE_IMAGE: '/assets/component/membership/img/donots_expert.svg',
    IMAGE_WIDTH: '28px',
    IMAGE_HEIGHT: '28px',
    GRADE_TEXT: USER_GRADE.LV4.TEXT
  }
}

export const MY_MEMBERSHIP = {
  DOUGH: {
    TEXT_1: '초코',
    TEXT_2: ' 등급으로 레벨업 하려면?',
    SUB_1: '프로필사진을 등록하세요',
    BTN_TEXT: () => '등록하기',
  },
  CHOCO: {
    TEXT_1: '크런치킹',
    TEXT_2: ' 등급으로 레벨업 하려면?',
    SUB_1: '레시피 ',
    SUB_2: (numerator, denominator) => `${numerator}/${denominator}`,
    SUB_3: '건 작성완료',
    BTN_TEXT: () => '작성하기',
  },
  CRUNCHY_KING: {
    TEXT_1: '',
    TEXT_2: '도낫츠 최고 등급이에요!',
    SUB_1: '당신의 SNS를 알려보세요',
    BTN_TEXT: (isExistSNS) => isExistSNS ? '작성완료': '입력하기',
  },
  EXPERT: {
    TEXT_1: '',
    TEXT_2: '도낫츠의 든든한 전문가에요!',
    SUB_1: '당신의 SNS를 알려보세요',
    BTN_TEXT: (isExistSNS) => isExistSNS ? '작성완료': '입력하기',
  }
}
