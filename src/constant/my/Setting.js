import {format} from "util";
import {nvlString} from "../../utils/formatString";
import {FAQ_CATEGORY_ITEMS} from "./mock/FAQ";

export const ONE_ON_ONE_INQUIRY = {
  key: 0,
  title: '',
  body: '',
  attachmentFile: '',
  category: 'GENERAL',
}

export const ONE_ON_ONE_INQUIRY_CATEGORY = [
  '일반', '가입/로그인', '레시피', '아이 프로필', '랭킹', '기타'
]

export const MY_SETTING = {
  TITLE: '설정',
  APP_SETTING: '앱설정',
  AUTO_LOGIN: '자동로그인',
  SCREEN_LOCK: '레시피 시청 중 화면 ON',
  APP_INFO: '앱/서비스 정보',
  NOT_FOUND_LIST: '등록된 글이 없어요.',
  PUSH: {
    HEADER_TITLE: 'PUSH설정',
    MENU_TITLE: 'PUSH 설정',
    SUB_TITLE: 'PUSH',
    EVENT_INFO: '마케팅/이벤트 정보',
    RECIPE_RESULT: '작성 콘텐츠 등록/검수 결과',
  },
  NOTICE: {
    HEADER_TITLE: '공지사항',
    MENU_TITLE: '공지사항',
  },
  TERMS: {
    HEADER_TITLE: '이용약관',
    MENU_TITLE: '이용약관',
    SERVICE: {
      HEADER_TITLE: '서비스 이용약관 [필수]',
      MENU_TITLE: '서비스 이용약관 [필수]',
      VALUE: '서비스 이용약관',
    },
    PERSONAL: {
      HEADER_TITLE: '개인정보처리방침 [필수]',
      MENU_TITLE: '개인정보처리방침 [필수]',
      VALUE: '개인정보처리방침',
    },
    AGREE: {
      HEADER_TITLE: '마케팅 등을 위한 개인정보 수집 및 이용\n동의 [선택]',
      MENU_TITLE: '마케팅 등을 위한 개인정보 수집 및 이용 동의 [선택]',
      VALUE: '마케팅 등을 위한 개인정보 수집 및 이용 동의',
      SUB_TITLE_1: '개인정보 수집 및 이용 동의 (동의철회)',
      SUB_TITLE_2: '전송매체별 광고성 정보의 수신 동의',
      AGREE: '동의',
      DISAGREE: '미동의',
      SMS: '문자',
      EMAIL: '이메일',
      REFER: '  •  개인정보 수집/이용 동의시 최소 1개 이상 선택 필수',
    }
  },
  LICENSE: {
    HEADER_TITLE: '오픈소스 라이센스',
    MENU_TITLE: '오픈소스 라이센스',
    VALUE: '오픈소스 라이센스',
  },
  FAQ: {
    HEADER_TITLE: '이용안내/FAQ',
    MENU_TITLE: '이용안내/FAQ',
  },
  VERSION: {
    HEADER_TITLE: '버전정보',
    MENU_TITLE: '버전정보',
    UPDATE_TITLE: '업데이트',
    UPDATE_MESSAGE: '최신버전으로 업데이트 해주세요',
    CURRENT_MESSAGE: '최신버전이에요',
    CURRENT_WEB_VERSION: (version) => {
      return `현재버전 v${version}`
    },
    CURRENT_VERSION: (version, webVersion) => {
      return `현재버전 v${version} (${webVersion})`
    },
  },
  ONE_ON_ONE_INQUIRIES: {
    HEADER_TITLE: '1:1 문의',
    MENU_TITLE: '1:1 문의',
    CATEGORY_PLACEHOLDER: '문의 유형을 선택해주세요.',
    TITLE_PLACEHOLDER: '제목을 작성해주세요',
    QUESTION_PLACEHOLDER: '내용을 입력해주세요',
  },
  POPUP_COMMENT: {
    REMOVE_INQUIRY_ITEM: '작성된 글이 모두 사라집니다.\n그래도 삭제하시겠어요?',
    BACK_REGIST_INQUIRY: '작성된 글이 모두 사라집니다.\n그래도 나가시겠어요?',
    REMOVE_PICTURE_REGIST_INQUIRY: '사진을\n삭제하시겠어요?'
  }
}

export const MY_SETTING_OPTION = {
  EVENT_INFO: 0,
  RECIPE_RESULT: 1,
  AUTO_LOGIN: 2,
  SCREEN_LOCK: 3,
}

/* 이용 약관 */
export const TERMS_OF_USE = [
  {
    TITLE: '서비스 이용약관',
    VALUE: '서비스 이용약관',
  },
  {
    TITLE: '개인정보 수집 및 이용 동의',
    VALUE: '개인정보 수집 및 이용 동의',
  },
  {
    TITLE: '민감정보 수집 및 이용 동의',
    VALUE: '민감정보 수집 및 이용 동의',
  },
  {
    TITLE: '마케팅 등을 위한 개인정보 수집 및 이용 동의 [선택]',
    VALUE: '마케팅 등을 위한 개인정보 수집 및 이용 동의',
  },
  {
    TITLE: '개인정보 수집 및 이용 동의',
    LINK_URL: 'https://safe.ok-name.co.kr/eterms/agreement001.jsp',
  },
  {
    TITLE: '고유식별정보 처리 동의',
    LINK_URL: 'https://safe.ok-name.co.kr/eterms/agreement003.jsp',
  },
  {
    TITLE: '통신사 이용약관 동의',
    LINK_URL: 'https://safe.ok-name.co.kr/eterms/agreement004.jsp',
  },
  {
    TITLE: '서비스 이용약관 동의',
    LINK_URL: 'https://safe.ok-name.co.kr/eterms/agreement002.jsp',
  },
  {
    TITLE: '개인정보 제 3자 제공 동의 (알뜰폰)',
    LINK_URL: 'https://safe.ok-name.co.kr/eterms/agreement005.jsp',
  },
  {
    TITLE: '마케팅 알림 수신 동의 [선택]',
    VALUE: '마케팅 알림 수신 동의',
  },
]

export const parseProvider = (provider) => {
  switch (provider) {
    case 'NAVER':
      return '네이버'
    case 'KAKAO':
      return '카카오'
    case 'GOOGLE':
      return '구글'
  }
}

export const QNA_CATEGORY = {
  GENERAL: {
    category: 'GENERAL',
    text: '일반'
  },
  SIGN_UP_AND_SIGN_IN: {
    category: 'SIGN_UP_AND_SIGN_IN',
    text: '가입/로그인',
  },
  RECIPE_POST: {
    category: 'RECIPE_POST',
    text: '레시피',
  },
  BABY_PROFILE: {
    category: 'BABY_PROFILE',
    text: '아이 프로필',
  },
  MEMBER_RANKING_BOARD: {
    category: 'MEMBER_RANKING_BOARD',
    text: '랭킹',
  },
  MISCELLANY: {
    category: 'MISCELLANY',
    text: '기타',
  },
}

export const QNA_CATEGORY_ITEMS = [
  QNA_CATEGORY.GENERAL,
  QNA_CATEGORY.SIGN_UP_AND_SIGN_IN,
  QNA_CATEGORY.RECIPE_POST,
  QNA_CATEGORY.BABY_PROFILE,
  QNA_CATEGORY.MEMBER_RANKING_BOARD,
  QNA_CATEGORY.MISCELLANY,
]

export const QNA_CATEGORY_TEXT = (category) => {
  let text = ''
  FAQ_CATEGORY_ITEMS.forEach((item) => {
    if (item.category === category) {
      text = format('[%s]', nvlString(item?.text))
      return
    }
  })
  return text
}

export const QNA_CATEGORY_CODE = (category) => {
  let text = ''
  FAQ_CATEGORY_ITEMS.forEach((item) => {
    if (item.text === category) {
      text = nvlString(item?.category)
      return
    }
  })
  return text
}