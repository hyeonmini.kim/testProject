export const ADD_CHILDREN_STEP = {
  NICKNAME: 0,
  BIRTH: 1,
  HEIGHT: 2,
  WEIGHT: 3,
  GENDER: 4,
  ALLERGY: 5,
  KEYWORD: 6,
}

export const ADD_CHILDREN = {
  nickname: '',
  birthdate: '',
  height: '',
  weight: '',
  gender: '',
  allergyIngredients: [],
  concerns: [],
}
