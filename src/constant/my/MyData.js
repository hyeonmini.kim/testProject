export const MyData = {
  profilePictureUrl: "",
  marketingInfoPushNotifSettingModifiedDatetime: "",
  birthDay: "",
  profileSelectedBabyKey: 0,
  briefBio: "",
  babies: [],
  type: "NORMAL_MEMBER",
  emailReceiveAgreementModifiedDatetime: "",
  isTermsCollectingPersonalDataMarketingAgreed: false,
  accountKey: 0,
  isEmailReceiveAgreed: "",
  phoneNumber: "",
  socialMediaUrl: "",
  isPostCensorshipResultPushNotifSet: "",
  grade: "LV1",
  nickname: "",
  textMessageReciveAgreementModifiedDatetime: "",
  isMarketingInfoPushNotifSet: false,
  isTextMessageReciveAgreed: "",
  key: 0,
  email: ""
}

export const MyBaby = {
  key: 0,
  parentKey: 0,
  nickname: "",
  birthdate: "",
  height: "",
  weight: "",
  gender: "",
  profilePictureUrl: "",
  profilePictureThumbnailOrder: "",
  allergyIngredients: [],
  concerns: [],
}
