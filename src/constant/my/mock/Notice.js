export const NOTICE_ITEMS = [
  {
    title: '한국 축구가 12년 만에 월드컵 16강에 진출했다. 사상 두 번째 원정 16강이다.',
    date: '2021.12.01',
    image: 'https://dimg.donga.com/wps/NEWS/IMAGE/2022/12/03/116825963.2.jpg',
    isNew: true,
    contents: '한국 축구가 12년 만에 월드컵 16강에 진출했다. 사상 두 번째 원정 16강이다. \n\n' +
      '파울루 벤투 감독이 이끄는 한국은 3일 0시(한국시간) 카타르 알라이얀의 에듀케이션 시티 스타디움에서 열린 포르투갈과 2022 카타르월드컵 H조 조별리그 최종 3차전에서 후반 추가시간에 터진 황희찬(울버햄튼)의 역전 결승골에 힘입어 2-1 역전승을 거뒀다.\n \n\n' +
      '이로써 1승1무1패(승점 4)가 된 한국은 포르투갈(2승1패 승점 6)에 이어 조 2위로 16강에 진출했다.\n\n' +
      '2010 남아공월드컵 이후 12년 만에 역대 두 번째 월드컵 원정 16강 진출이다.\n\n' +
      '또 2002 한일월드컵(4강), 2010 남아공월드컵(16강)에 이어 통산 세 번째 16강 진출이다.',
  },
  {
    title: '월드컵 48개국 확대안, 4년 앞당겨 강행…FIFA의 속셈은? FIFA, 월드컵 본선 48개국 확대 강행 움직임',
    date: '2021.12.23',
    image: 'https://dimg.donga.com/wps/NEWS/IMAGE/2022/11/29/116745326.2.jpg',
    isNew: true,
    contents: 'FIFA, 월드컵 본선 48개국 확대 강행 움직임, 6월 총회서 확정되면 2022년 카타르대회에 도입 \n\n' +
      '국제축구연맹 FIFA가 월드컵 본선 출전국을 48개 나라로 확대하는 안을 2022년 카타르 대회부터 밀어붙이기로 했다. 2022년 48개국 확대안은 지난 15일 미국 마이애미에서 열린 FIFA 평의회(FIFA Council)를 통과했다. FIFA 평의회는 인판티노 회장과 수석부회장 1명, 부회장 7명, 26명의 위원을 합해 35명으로 구성되는 FIFA의 주요 의결기관이다. \n\n' +
      '본선 48개국 출전을 4년 더 앞당기는 안은 오는 6월 프랑스 파리에서 열리는 211개 회원국의 FIFA 총회(FIFA Congress)에서 의결을 거쳐 통과되면 확정된다. \n\n' +
      '2016년 2월 말 당선돼 임기를 시작한 인판티노 회장의 애초 구상은 본선 40개국 참가 방안이었다. 그러다 48개국 참가 방안으로 여론의 무게 중심이 점차 옮겨 가면서, FIFA는 2017년 1월 10일, 2026년 월드컵부터 참가국을 48개국으로 늘리는 개정안을 FIFA 위원회(FIFA Committee)에서 만장일치로 통과시켰다고 발표했다. 개편안은 3개국을 한 조로 묶어 16개 조로 나누는 방안이다.\n\n' +
      '인판티노 회장과 FIFA는 여기서 그치지 않고 한발 더 나아갔다. 48개국 본선 출전을 2022년 카타르 대회로 4년 앞당겨 적용하자는 주장을 펼쳤고, 역사상 최초로 중동 아시아에서 열리는 2022년 카타르 월드컵 대회부터 48개국으로 확대하는 방안을 강행하기로 하면서 FIFA 평의회를 통과한 것이다.',
  },
  {
    title: '월드컵: 2022 카타르 월드컵 본선 조 확정. 포르투갈, 우루과이 포진, 가나는 FIFA 랭킹 60위로, 카타르 월드컵 본선행을 확정한 29개국 중 랭킹이 가장 낮다.',
    date: '2021.11.24',
    image: 'https://ichef.bbci.co.uk/news/800/cpsprodpb/F38C/production/_123984326_origin_2022.jpg.webp',
    isNew: false,
    contents: '한국 축구 국가대표팀(FIFA 랭킹 29위)이 2일 카타르 도하에서 열린 2022 카타르 월드컵 본선 조 추첨에서 포르투갈, 우루과이, 가나와 함께 H조에 편성됐다. 소위 \'죽음의 조\'는 피했지만 만만치 않다는 평가가 나온다. \n\n' +
      '한국은 현지시간으로 오는 11월 24일 우루과이, 같은 달 28일 가나, 12월 2일 포르투갈과 차례로 맞붙는다. \n\n' +
      '한국은 포르투갈과는 2002 한일 월드컵 이후 20년 만에, 우루과이와는 2010년 남아프리카공화국 대회 이후 12년 만에 월드컵 본선에서 만난다. 가나와는 월드컵 본선에서는 첫 만남이다. \n\n' +
      'FIFA 랭킹 8위의 포르투갈은 이번 카타르 대회가 역대 8번째 출전이자 5회 연속 본선 진출이다. 역대 최고 성적은 1966년 잉글랜드 대회 3위.\n' +
      '\n' +
      '크리스티아누 호날두(맨체스터 유나이티드)와 브루누 페르난드스(맨유), 디오구 조타(리버풀) 등 세계적인 선수들이 포진해 있다.\n' +
      '\n' +
      '랭킹 13위 우루과이는 월드컵 2회 우승을 차지한 전통의 강호다. 본선 출전은 이번이 통산 14번째다. 역대 A매치 전적에서도 한국이 1승 1무 6패로 크게 밀린다.\n' +
      '\n' +
      '루이스 수아레스(아틀레티코 마드리드), 에딘손 카바니(맨시티) 그리고 손흥민의 토트넘 동료인 로드리고 벤탄쿠르 등이 대표팀 소속이다.\n' +
      '\n' +
      '가나는 FIFA 랭킹 60위로, 카타르 월드컵 본선행을 확정한 29개국 중 랭킹이 가장 낮다.\n\n'
  },
  {
    title: '[오피셜] 일본, 카타르 월드컵 최종 명단 발표…유럽파만 20명',
    date: '2021.10.11',
    image: 'https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/blt689c24f46b035622/6360b9af2b316f0b659ee31f/GettyImages-1243475025.jpg?quality=80&format=pjpg&auto=webp&width=620',
    isNew: false,
    contents: '모리야스 하지메(54·일본) 감독이 이끄는 일본 축구 국가대표팀이 2022 국제축구연맹(FIFA) 카타르 월드컵 본선에 나설 최종 명단 26인을 발표했다. 유럽 무대에서 뛰는 선수들이 다수 포함됐다. \n\n' +
      '일본축구협회(JFA)는 1일 오후 2시(한국시간) 공식 채널을 통해 카타르 월드컵 최종 명단을 발표했다. 미나미노 타쿠미(27·모나코)와 요시다 마야(34·샬케04), 토미야스 다케히로(23·아스널), 카마다 다이치(26·프랑크푸르트), 쿠보 다케후사(21·레알 소시에다드) 등 이름만 들어도 알만한 유럽파가 대거 포함됐다. \n\n' +
      '이에 반해 J리그에 속한 선수는 여섯 명에 불과했다. 나가토모 유토(36·FC도쿄)와 사카이 히로키(32·우라와 레즈), 곤다 슈이치(33·시미즈 에스펄스) 등 유럽에서 활약하다가 복귀한 베테랑들과 소마 유키(25·가시마 앤틀러스), 야마네 미키(28), 타니구치 쇼고(31·이상 가와사키 프론탈레)가 승선했다. \n\n' +
      '일본은 카타르 월드컵 본선에서 \'세계 강호\' 독일과 스페인, \'북중미 복병\' 코스타리카와 함께 E조에 속했다. 16강 진출을 목표로 하는 만큼 이번 주 J리그 일정이 끝나는 대로 곧바로 카타르로 향해 현지 적응에 돌입한 후 오는 17일 캐나다와 평가전을 치를 예정이다.\n\n' +
      'GK : 곤다 슈이치(시미즈 에스펄스), 다니엘 슈미트(신트트라위던), 가와시마 에이지(스트라스부르)\n' +
      '\n' +
      'DF : 요시다 마야(샬케04), 토미야스 타케히로(아스널), 나가토모 유토(FC도쿄), 나카야마 유타(허더즈필드), 다니구치 쇼고, 야마네 미키(이상 가와사키 프론탈레), 이토 히로키(슈투트가르트), 사카이 히로키(우라와 레즈), 이타쿠라 고(묀헨글라드바흐)\n' +
      '\n' +
      'MF : 카마다 다이치(프랑크푸르트), 도안 리츠(프라이부르크), 이토 준야(스타드 드 랭스), 미나미노 타쿠미(AS모나코), 쿠보 타케후사(레알 소시에다드), 엔도 와타루(슈투트가르트), 모리타 히데마사(스포르팅), 다나카 아오(뒤셀도르프), 미토마 카오루(브라이튼 앤 호브 알비온), 시바사키 가쿠(CD레가네스), 소마 유키(나고야 그램퍼스)',
  },
  {
    title: '한국 월드컵 진출 확률 98.6%... 일본 78%, 중국 0.1%',
    date: '2021.09.09',
    image: 'https://www.kgnews.co.kr/data/photos/yhnews/202111/PYH2021111127870001300_2064b4.jpg',
    isNew: false,
    contents: '대한민국 축구대표팀의 10회 연속 월드컵 진출이 98.6%에 달한다는 전망이 나왔다. \n\n' +
      '15일(한국시간) 축구 통계 사이트 위글로벌풋볼이 공개한 2022 카타르 월드컵 본선 진출 확률에 따르면 한국은 98.6%로 아시아에서 2번째로 높았다. \n\n' +
      '이는 지난 12일까지 진행된 최종예선 5차전 결과들까지 반영한 뒤, 자체적으로 월드컵 진출 여부를 시뮬레이션한 결과다. \n\n' +
      '파울루 벤투(52·포르투갈) 감독이 이끄는 한국은 3승2무(승점 11점)을 기록, 이란(승점 13)에 이어 조 2위를 기록하고 있다. \n\n' +
      '3위 레바논(승점 5)과의 격차는 6점으로 벌어져 월드컵 본선 직행 경쟁에 유리한 고지를 선점했다. \n\n' +
      '지난달 팀당 4경기씩 치른 직후 한국의 월드컵 본선 확률은 97.68%였는데, 지난 11일 아랍에미리트(UAE)전 1-0 승리 이후 약 1%p 상승했다. \n\n' +
      'A조에선 선두 이란의 월드컵 본선 확률이 100%로 전망됐다. \n\n' +
      '이란과 한국에 이어 이라크 12.53%, 레바논 2.18%, UAE 1.47%, 시리아 0.41% 순이었다. 조 3위 레바논보다 4위 이라크의 확률이 더 높게 나왔다. \n\n' +
      'B조는 선두 사우디라바이아(승점 13·4승1무)의 본선 확률이 80.47%로 가장 높았다. 이어 일본이 78.18%, 호주는 68.75% 순이었다. 중국은 0.11%에 불과했다. \n\n' +
      '박항서 감독이 이끄는 베트남은 5전 전패로 본선 진출 확률이 0%로 전망됐다. \n\n'
  },
]