import {USER_GRADE} from "../common/User";

export const COMPLETE_SIGN_UP = {
  TITLE: '가입을 축하합니다!\n도우 등급이 되었습니다',
  CARD_TEXT: '잠깐! 프로필 사진만 등록하면\n초코 등급으로 레벨업되고\n전문가 레시피를 볼수 있어요',
  LEFT_BUTTON_TEXT: '다음에 하기',
  RIGHT_BUTTON_TEXT: '프로필 저장',
}

export const LEVEL_UP_CHOCO = {
  TITLE: '축하합니다\n초코 등급으로 레벨업!',
  CARD_TEXT_1: '초코 등급이 되면',
  CARD_TEXT_2_1: '의사, 약사, 발달전문가 등\n',
  CARD_TEXT_2_2: '전문가 레시피',
  CARD_TEXT_2_3: '를 볼 수 있어요',
  CARD_TEXT_3_1: '레시피 ',
  CARD_TEXT_3_2: '5건 작성',
  CARD_TEXT_3_3: '하면\n크런치킹 등급으로 레벨업 돼요',
  BUTTON_TEXT: '확인',
}

export const LEVEL_UP_CRUNCH_KING = {
  TITLE: '축하합니다\n크런치킹 등급으로 레벨업!',
  CARD_TEXT: '크런치킹 등급만의 혜택!\nSNS 주소를 등록하고\n내 SNS를 홍보해보세요',
  INPUT_TITLE: 'SNS 주소',
  INPUT_LABEL: 'URL을 입력해주세요',
  LEFT_BUTTON_TEXT: '다음에 하기',
  RIGHT_BUTTON_TEXT: 'SNS계정 저장',
}

export const GRADE_STEPS = [
  {
    step: USER_GRADE.LV1.GRADE,
    name: USER_GRADE.LV1.TEXT,
    img: '/assets/component/membership/img/donots_dough.svg'
  },
  {
    step: USER_GRADE.LV2.GRADE,
    name: USER_GRADE.LV2.TEXT,
    img: '/assets/component/membership/img/donots_choco.svg'
  },
  {
    step: USER_GRADE.LV3.GRADE,
    name: USER_GRADE.LV3.TEXT,
    img: '/assets/component/membership/img/donots_crunchy_king.svg'
  }
]