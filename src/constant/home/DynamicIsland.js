export const HEALTH_GRAPH = {
  DRAW_SPEED: 10,
  DEFAULT_BMI_VALUE: 100,
  MAX_BAR_POSITION: 100,
  MIDDLE_BAR_POSITION: 50,
}

export const DYNAMIC_ISLAND = {
  TOOLTIP_CONTENT: '백분위수란 같은 성별과 같은 나이의 영유아 100명 중에서 작은 쪽에서부터의 순서를 말합니다.'
}