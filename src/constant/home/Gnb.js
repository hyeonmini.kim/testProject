export const HOME_GNB = {
  HOME: '홈',
  MOMSTAR: '랭킹',
  SEARCH: '검색',
  MY: '마이',
}

export const GUEST_LOGIN_POPUP_TEXT = {
  TITLE: '로그인 후\n콘텐츠를 이용해보세요!',
  BUTTON_TEXT: {
    CLOSE: '취소',
    GO_LOGIN: '로그인'
  }
}