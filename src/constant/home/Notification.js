export const HOME_NOTIFICATION = {
  TITLE: '알림',
  NEW: '새글',
  TODAY: '오늘',
  YESTERDAY: '어제',
  THIS_WEEK: '이번 주',
  THIS_MONTH: '이번 달',
  BEFORE_MONTH: '지난 달',
  BEFORE2_MONTH: '전전달',
  LAST_MONTH: (year, month) => {
    return `${year}.${month}`
  },
  NOTICE_TITLE: '공지사항',
  NO_SEARCH_NOTICE_TITLE: '등록된 알림이 없어요.',
}

export const HOME_NOTIFICATION_TYPE = {
  RECIPE: '게시중',
  NOTICE: '공지사항',
  EXAMINATION: '검수중',
  REJECT: '반려',
  SPRINKLE: '승급',
  LV2: 'LV2',
  LV3: 'LV3',
  SUR01: 'SUR01',
  SUR02: 'SUR02',
}
