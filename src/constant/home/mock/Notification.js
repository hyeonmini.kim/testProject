import {HOME_NOTIFICATION_TYPE} from "../Notification";

export const HOME_NOTIFICATION_NEW = [
  {
    image: 'https://recipe1.ezmember.co.kr/cache/recipe/2018/02/13/e0649f3f0b446f39c9a7bdff70d953691.jpg',
    title: '\'순두부찌개. 바지락, 고기 없이도 기가 막힌 순두부찌개 만드는 법\' 레시피가 정상적으로 게시되었어요.',
    date: '방금 전',
    type: HOME_NOTIFICATION_TYPE.RECIPE,
    link: {
      name: '요리왕-비용',
      title: '향긋한-더덕무침-만들기',
      id: '11',
    },
  },
  {
    avatars: [
      'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_21.jpg',
      'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_22.jpg',
    ],
    image: 'https://recipe1.ezmember.co.kr/cache/recipe/2018/05/30/74fe4a8812825a1fca208924739b61d01.jpg',
    title: '인천끼룩이, 송파비둘기 외 사람들이 나의 레시피를 스크랩하였어요.',
    date: '방금 전',
    type: HOME_NOTIFICATION_TYPE.SCRAP,
    link: {
      name: '인천-끼룩이',
      title: '8개월아기-이유식-메뉴-달콤하고-부드러운-대파스프-만들기',
      id: '27',
    },
  },
]

export const HOME_NOTIFICATION_TODAY = [
  {
    image: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/8b67a8ea21f00802129263bf75119dcd1.jpg',
    title: '\'8개월아기 이유식 메뉴, 달콤하고 부드러운 대파스프 만들기 :)\' 레시피가 검수 요청되었어요.',
    date: '1시간 전',
    type: HOME_NOTIFICATION_TYPE.EXAMINATION,
    link: {
      recipe: {}
    },
  },
  {
    title: '한국 축구가 12년 만에 월드컵 16강에 진출했다. 사상 두 번째 원정 16강이다.',
    date: '4시간 전',
    type: HOME_NOTIFICATION_TYPE.NOTICE,
    link: {
      id: 0,
    }
  },
]

export const HOME_NOTIFICATION_YESTERDAY = [
  {
    image: 'https://recipe1.ezmember.co.kr/cache/recipe/2018/02/13/e0649f3f0b446f39c9a7bdff70d953691.jpg',
    title: '\'순두부찌개. 바지락, 고기 없이도 기가 막힌 순두부찌개 만드는 법\' 레시피가 반려되었어요.',
    date: '1일 전',
    type: HOME_NOTIFICATION_TYPE.REJECT,
    link: {
      recipe: {}
    },
  },
]

export const HOME_NOTIFICATION_THIS_WEEK = [
  {
    title: '월드컵 48개국 확대안, 4년 앞당겨 강행…FIFA의 속셈은? FIFA, 월드컵 본선 48개국 확대 강행 움직임',
    date: '2일 전',
    type: HOME_NOTIFICATION_TYPE.NOTICE,
    link: {
      id: 1,
    }
  },
  {
    image: 'https://recipe1.ezmember.co.kr/cache/recipe/2015/07/08/faef2350760b2aaabffc18225bff65cf1.jpg',
    title: '\'향긋한 더덕무침 만들기\' 레시피가 정상적으로 게시되었어요.',
    date: '4일 전',
    type: HOME_NOTIFICATION_TYPE.RECIPE,
    link: {
      name: '부천-불주먹',
      title: '5분-완성-폭탄계란찜-만들기',
      id: '9',
    },
  },
  {
    title: '월드컵: 2022 카타르 월드컵 본선 조 확정. 포르투갈, 우루과이 포진, 가나는 FIFA 랭킹 60위로',
    date: '5일 전',
    type: HOME_NOTIFICATION_TYPE.NOTICE,
    link: {
      id: 2,
    }
  },
]

export const HOME_NOTIFICATION_THIS_MONTH = [
  {
    title: '축하드려요! 부천불주먹님이 맘스타로 선정되었어요.',
    date: '2022.12.01',
    type: HOME_NOTIFICATION_TYPE.MOMSTAR,
  },
]

export const HOME_NOTIFICATION_LAST_MONTH = [
  {
    title: '[오피셜] 일본, 카타르 월드컵 최종 명단 발표…유럽파만 20명',
    date: '2022.11.22',
    type: HOME_NOTIFICATION_TYPE.NOTICE,
    link: {
      id: 3,
    }
  },
  {
    title: '한국 월드컵 진출 확률 98.6%... 일본 78%, 중국 0.1%',
    date: '2022.11.15',
    type: HOME_NOTIFICATION_TYPE.NOTICE,
    link: {
      id: 4,
    }
  },
]