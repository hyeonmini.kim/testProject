import {getRandom} from "../../utils/formatNumber";
import {RECIPE_SEARCH_TYPE} from "../recipe/Search";

export const HOME_PERSONAL_COMMON = {
  WELCOME: () => {
    switch (getRandom(3)) {
      case 0:
        return '기분 좋은 하루 되세요.'
      case 1:
        return '오늘도 즐거운 일만 가득할거예요.'
      default:
        return '행복한 일만 가득하길 바래요.'
    }
  },
  VIEW_CNT: (cnt) => {
    return `조회수 ${cnt}`
  },
  SCRAP_CNT: (cnt) => {
    return `스크랩 ${cnt}`
  },
  BABY: {
    EVALUATION: (text) => {
      if (!text) return
      return `${text}`
    },
    STATUS: (text) => {
      if (!text) return
      return `${text}`
    },
    INFO: (nickName, day, month) => {
      if (!nickName || !day || month === undefined) return
      return `${nickName}\n${day}일 (${month}개월)`
    },
    HEIGHT: '키',
    WEIGHT: '몸무게',
    BMI: 'BMI',
    ENERGY: '필요에너지',
    UPDATE: (nickName) => {
      return `${nickName} 발달 정보 업데이트 하기`
    },
  },
  CUSTOM: {
    TITLE: '우리 아이 맞춤 레시피',
    TOOLTIP_CONTENT: '우리 아이 맞춤 레시피는 ①알레르기 재료 제거식, ②알레르기 재료 대체식, ③선택한 건강 키워드 순으로 구성되어 있어요!'
  },
  USER: {
    TITLE: '성장 도움 레시피',
  },
  WEEKLY: {
    TITLE: '금주의 HOT 건강 키워드',
    KEYWORDS: [
      {
        type: RECIPE_SEARCH_TYPE.KEYWORD,
        title: '편식하는아이',
        keyword: '편식',
      },
      {
        type: RECIPE_SEARCH_TYPE.CATEGORY,
        title: '간식',
        keyword: '간식',
      },
      {
        type: RECIPE_SEARCH_TYPE.KEYWORD,
        title: '유당불내증',
        keyword: '유당불내증',
      },
      {
        type: RECIPE_SEARCH_TYPE.CATEGORY,
        title: '아이반찬',
        keyword: '반찬',
      },
      {
        type: RECIPE_SEARCH_TYPE.KEYWORD,
        title: '피부건강',
        keyword: '피부 건강',
      },
      {
        type: RECIPE_SEARCH_TYPE.CATEGORY,
        title: '한그릇뚝딱',
        keyword: '한그릇',
      },
      {
        type: RECIPE_SEARCH_TYPE.MATERIAL,
        title: '철분풍부소고기',
        keyword: '소고기',
      },
      {
        type: RECIPE_SEARCH_TYPE.CATEGORY,
        title: '특별한날',
        keyword: '특식',
      },
      {
        type: RECIPE_SEARCH_TYPE.KEYWORD,
        title: '감기/수족구',
        keyword: '감기/수족구',
      },
      {
        type: RECIPE_SEARCH_TYPE.KEYWORD,
        title: '변비',
        keyword: '변비',
      },
      {
        type: RECIPE_SEARCH_TYPE.KEYWORD,
        title: '뼈가튼튼',
        keyword: '뼈 건강',
      },
      {
        type: RECIPE_SEARCH_TYPE.KEYWORD,
        title: '빈혈에좋은',
        keyword: '빈혈',
      },
    ],
  },
  ALLERGY_TAG: (tags) => {
    let allergyTag = '알레르기 유발 재료 없음'
    if (tags?.length) {
      allergyTag = tags.join(', ')
      if (allergyTag.length > 15) {
        allergyTag = allergyTag.substring(0, 15) + '...  함유'
      } else {
        allergyTag = allergyTag + ' 함유'
      }
    }
    return allergyTag
  },
  SEASON: {
    TITLE: (material) => `제철재료 ${material ? '\'' + material + '\'' : ''} 레시피`,
  },
  MORE: '더보기',
  RECIPE_MORE: '레시피 더보기',
  HOT_DONOTS: '이달의 HOT 도낫츠',
  TOAST: {},
  PROPOSE_HOT_DONOTS: '레시피 작성하고 HOT 도낫츠가 되어보세요',
  DETAIL: '자세히보기',
  POSITION:{
    TOP:'TOP',
    DOWN:'DOWN'
  }
}