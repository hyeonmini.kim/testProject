export const HOME_GUEST_COMMON = {
  TOOLTIP: '・도낫이(여아, 34개월) 기준 샘플 콘텐츠 입니다.\n・산출된 BMI 결과를 토대로, 우리 아이의 건강한 성장을 위한 레시피를 추천해 드려요',
  SERVICE_MESSAGE: {
    LOGIN: '로그인',
    TITLE: ' 후 도낫츠의\n맞춤 콘텐츠를 이용해보세요'
  },
  BANNER: {
    TITLE: '도낫츠 맛보기'
  },
  USER: {
    TITLE: '성장 도움 레시피',
    TOOLTIP: '・도낫이(여아, 34개월) 기준 샘플 콘텐츠 입니다.\n・산출된 BMI 결과를 토대로, 우리 아이의 건강한 성장을 위한 레시피를 추천해 드려요',
  },
  CUSTOM: {
    TITLE: '우리 아이 맞춤 레시피',
    TOOLTIP: '・도낫이(여아, 34개월) 기준 샘플 콘텐츠 입니다.\n・① 알레르기 재료 제거식, ② 알레르기 재료 대체식, ③ 관심있는 건강 키워드를 토대로 구성되어 있어요!',
  },
  KEYWORD: {
    TITLE: '우리 아이 건강 레시피',
  },
  TEXT: {},
  BUTTON: {},
  TOAST: {},
}

export const GUEST_STATEMENT = {
  title: "반가워요! 도낫츠입니다.",
  subTitle: "로그인 후 콘텐츠를 이용해보세요!"
}
export const GUEST_BABY_INFO = {
  nickName: "도낫이",
  profile_weight: {
    percentage: 86,
    statement: "상위 14%",
    value: "15.5kg"
  },
  profile_bmi: {
    statement: "과체중"
  },
  profile: {
    month: 34,
    statement_evaluation: "우리아이는 100명 중\n몇 백분위수 일까요?",
    day: 1062
  },
  profile_height: {
    percentage: 63,
    statement: "상위 37%",
    value: "95.0cm"
  }
}

export const GUEST_USER_BODY = [
  {
    recipe_iframe: "static/recipe/1000515/index.html",
    recipe_name: "게살와플밥",
    recipe_desc: "게살을 넣고 만든 재밌는 모양의 와플밥",
    recipe_lead_time: "30분 이내",
    recipe_level: "쉬워요",
    image_file_path: "/static/recipe/1000515/img/main_img1.jpg",
    recipe_writer_info: {
      recipe_writer_nickname: "준맘제인",
      recipe_writer_grade: "LV3",
      recipe_writer_thumbnail_path: "/static/recipe/1000515/img/profile.jpeg",
      recipe_writer_type: "NORMAL_MEMBER"
    },
    recipe_allergy_tag: [
      "난류",
    ]
  },
  {
    recipe_iframe: "/static/recipe/1001154/index.html",
    recipe_name: "아이스크림밥",
    recipe_desc: "호기심을 자극하는 아이스크림 모양 볶음밥",
    recipe_lead_time: "1시간 이내",
    recipe_level: "할만해요",
    image_file_path: "/static/recipe/1001154/img/main_img1.png",
    recipe_writer_info: {
      recipe_writer_nickname: "아윤이밥",
      recipe_writer_grade: "LV3",
      recipe_writer_thumbnail_path: "/static/recipe/1001154/img/profile.png",
      recipe_writer_type: "NORMAL_MEMBER"
    },
    recipe_allergy_tag: [
      "토마토",
      "우유",
      "쇠고기",
    ]
  },
  {
    recipe_iframe: "static/recipe/1000546/index.html",
    recipe_name: "고구마치즈스콘",
    recipe_desc: "달콤한 고구마와 영양가득 치즈 넣은 쌀스콘",
    recipe_lead_time: "1시간 이내",
    recipe_level: "할만해요",
    image_file_path: "/static/recipe/1000546/img/main_img1.jpg",
    recipe_writer_info: {
      recipe_writer_nickname: "라베맘",
      recipe_writer_grade: "LV3",
      recipe_writer_thumbnail_path: "/static/recipe/1000546/img/profile.png",
      recipe_writer_type: "NORMAL_MEMBER"
    },
    recipe_allergy_tag: [
      "우유",
      "난류"
    ]
  },
  {
    recipe_iframe: "static/recipe/1000573/index.html",
    recipe_name: "고구마키슈",
    recipe_desc: "엄마아빠와 함께 즐기는 오감 만족 영양 간식",
    recipe_lead_time: "1시간 이상",
    recipe_level: "좀 어려워요",
    image_file_path: "/static/recipe/1000573/img/main_img1.png",
    recipe_writer_info: {
      recipe_writer_nickname: "라이프기획자",
      recipe_writer_grade: "LV3",
      recipe_writer_thumbnail_path: "",
      recipe_writer_type: "NORMAL_MEMBER"
    },
    recipe_allergy_tag: [
      "우유",
      "난류",
      "땅콩",
      "잣",
      "호두",
      "닭고기"
    ]
  },
  {
    recipe_iframe: "static/recipe/1000984/index.html",
    recipe_name: "돼지갈비찜",
    recipe_desc: "맛과 영양이 뛰어난 고단백 반찬",
    recipe_lead_time: "1시간 이내",
    recipe_level: "할만해요",
    image_file_path: "/static/recipe/1000984/img/main_img1.jpg",
    recipe_writer_info: {
      recipe_writer_nickname: "오범조가정의학과의사",
      recipe_writer_grade: "LV1",
      recipe_writer_thumbnail_path: "",
      recipe_writer_type: "EXPERT"
    },
    recipe_allergy_tag: [
      "밀",
      "대두",
      "돼지고기",
    ]
  }
]

export const GUEST_CUSTOM_BASED = [
  {
    recipe_iframe: "static/recipe/1001173/index.html",
    recipe_name: "무염소고기육전",
    recipe_desc: "소고기를 거부하던 아기가 처음으로 잘 먹었던 반찬",
    recipe_lead_time: "30분 이내",
    recipe_level: "쉬워요",
    image_file_path: "/static/recipe/1001173/img/main_img1.jpg",
    recipe_view_cnt: 269,
    recipe_scrap_cnt: 0,
    recipe_health_tag: "비타민B12 풍부",
    recipe_writer_info: {
      recipe_writer_nickname: "쑥쑥맘",
      recipe_writer_grade: "LV3",
      recipe_writer_thumbnail_path: "/static/recipe/1001173/img/profile.png",
      recipe_writer_type: "NORMAL_MEMBER"
    }
  },
  {
    recipe_iframe: "static/recipe/1000520/index.html",
    recipe_name: "단호박우유죽",
    recipe_desc: "우유 넣고 부드럽게 단호박 우유죽 만들기",
    recipe_lead_time: "30분 이내",
    recipe_level: "쉬워요",
    image_file_path: "/static/recipe/1000520/img/main_img1.jpg",
    recipe_view_cnt: 247,
    recipe_scrap_cnt: 1,
    recipe_health_tag: "비타민A 풍부",
    recipe_writer_info: {
      recipe_writer_nickname: "냠냠바른",
      recipe_writer_grade: "LV3",
      recipe_writer_thumbnail_path: "",
      recipe_writer_type: "NORMAL_MEMBER"
    }
  },
  {
    recipe_iframe: "static/recipe/1000458/index.html",
    recipe_name: "소고기브로콜리크림파스타",
    recipe_desc: "우유와 아기치즈로 간단히 만드는 크림파스타",
    recipe_lead_time: "1시간 이내",
    recipe_level: "할만해요",
    image_file_path: "/static/recipe/1000458/img/main_img1.jpg",
    recipe_view_cnt: 238,
    recipe_scrap_cnt: 0,
    recipe_health_tag: "단백질 풍부",
    recipe_writer_info: {
      recipe_writer_nickname: "조근조근유아식",
      recipe_writer_grade: "LV3",
      recipe_writer_thumbnail_path: "",
      recipe_writer_type: "NORMAL_MEMBER"
    }
  }
]