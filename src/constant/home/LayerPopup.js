export const LAYER_POPUP = {
  layerPopupList: [],
  handleDialogPaper: {text: '', onClick: undefined},
  handleDialogPaper2: {text: '', onClick: undefined},
}

export const LAYER_POPUP_BUTTON_TEXT = {
  NO_MORE_SHOW_TODAY: '오늘 하루 보지 않기',
  CLOSE: '닫기',
}