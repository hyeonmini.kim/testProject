import {BADGE_ICON_TYPE, BadgeIcons} from "./Badge/BadgeIcons";
import * as React from "react";
import {ALT_STRING} from "../common/AltString";

export function UserBadge({type, grade, isAlt = true, sx}) {
  if (type === "EXPERT") {
    return <BadgeIcons type={BADGE_ICON_TYPE.BADGE_LV4_24PX} alt={isAlt ? ALT_STRING.COMMON.BADGE_LV4 : ''} sx={{...sx}}/>
  } else if (grade === "LV1") {
    return <BadgeIcons type={BADGE_ICON_TYPE.BADGE_LV1_24PX} alt={isAlt ? ALT_STRING.COMMON.BADGE_LV1 : ''} sx={{...sx}}/>
  } else if (grade === "LV2") {
    return <BadgeIcons type={BADGE_ICON_TYPE.BADGE_LV2_24PX} alt={isAlt ? ALT_STRING.COMMON.BADGE_LV2 : ''} sx={{...sx}}/>
  } else if (grade === "LV3") {
    return <BadgeIcons type={BADGE_ICON_TYPE.BADGE_LV3_24PX} alt={isAlt ? ALT_STRING.COMMON.BADGE_LV3 : ''} sx={{...sx}}/>
  } else {
    return <></>
  }
}