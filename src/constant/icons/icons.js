import {SvgIcon} from "@mui/material";
import {ALT_STRING} from "../common/AltString";

export const ICON_COLOR = {
  BLACK: 'BLACK',
  GREY: 'GREY',
  RED: 'RED',
  WHITE: 'WHITE',
  PRIMARY: 'PRIMARY',
}

export const ICON_SIZE = {
  PX16: '16px',
  PX18: '18px',
  PX20: '20px',
  PX24: '24px',
  PX30: '30px',
  PX96: '96px',
}

export function SvgMyIcon({color = ICON_COLOR.GREY, size = ICON_SIZE.PX24, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_24PX()
  }
  const PRIMARY_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', fill: 'none', ...sx}}>
        <path
          d="M15.6752 6.42439C15.6752 8.45404 14.0298 10.0994 12.0002 10.0994C9.97055 10.0994 8.3252 8.45404 8.3252 6.42439C8.3252 4.39474 9.97055 2.74939 12.0002 2.74939C14.0298 2.74939 15.6752 4.39474 15.6752 6.42439Z"
          stroke="#ECA548" strokeWidth="2"/>
        <path
          d="M2.2002 20.1405C2.2002 16.2863 5.4202 13.1619 12.0002 13.1619C18.5802 13.1619 21.8002 16.2863 21.8002 20.1405C21.8002 20.7537 21.3528 21.2508 20.801 21.2508H3.19941C2.64756 21.2508 2.2002 20.7537 2.2002 20.1405Z"
          stroke="#ECA548" strokeWidth="2"/>
      </SvgIcon>
    )
  }
  const GREY_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', fill: 'none', ...sx}}>
        <path
          d="M15.6752 6.42439C15.6752 8.45404 14.0298 10.0994 12.0002 10.0994C9.97055 10.0994 8.3252 8.45404 8.3252 6.42439C8.3252 4.39474 9.97055 2.74939 12.0002 2.74939C14.0298 2.74939 15.6752 4.39474 15.6752 6.42439Z"
          stroke="#CCCCCC" strokeWidth="2"/>
        <path
          d="M2.2002 20.1405C2.2002 16.2863 5.4202 13.1619 12.0002 13.1619C18.5802 13.1619 21.8002 16.2863 21.8002 20.1405C21.8002 20.7537 21.3528 21.2508 20.801 21.2508H3.19941C2.64756 21.2508 2.2002 20.7537 2.2002 20.1405Z"
          stroke="#CCCCCC" strokeWidth="2"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return GREY_24PX()
      default:
        return DEFAULT()
    }
  }
  const PRIMARY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return PRIMARY_24PX()
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.PRIMARY:
      return PRIMARY_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgStarIcon({color = ICON_COLOR.GREY, size = ICON_SIZE.PX24, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_24PX()
  }
  const PRIMARY_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', fill: 'none', ...sx}}>
        <path
          d="M11.4951 2.71393C11.7017 2.29539 12.2985 2.29539 12.5051 2.71393L15.1791 8.13206C15.2611 8.29826 15.4196 8.41346 15.6031 8.44012L21.5823 9.30895C22.0442 9.37607 22.2286 9.94369 21.8944 10.2695L17.5678 14.4869C17.4351 14.6163 17.3745 14.8027 17.4058 14.9854L18.4272 20.9404C18.5061 21.4005 18.0233 21.7513 17.6101 21.5341L12.2621 18.7225C12.0981 18.6362 11.9021 18.6362 11.738 18.7225L6.39002 21.5341C5.97689 21.7513 5.49404 21.4005 5.57294 20.9404L6.59432 14.9854C6.62565 14.8027 6.56509 14.6163 6.43236 14.4869L2.10573 10.2695C1.7715 9.94369 1.95594 9.37607 2.41783 9.30895L8.39708 8.44012C8.5805 8.41346 8.73906 8.29826 8.82109 8.13206L11.4951 2.71393Z"
          stroke="#ECA548" strokeWidth="2" strokeLinejoin="round"/>
      </SvgIcon>
    )
  }
  const GREY_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', fill: 'none', ...sx}}>
        <path
          d="M11.4951 2.71393C11.7017 2.29539 12.2985 2.29539 12.5051 2.71393L15.1791 8.13206C15.2611 8.29826 15.4196 8.41346 15.6031 8.44012L21.5823 9.30895C22.0442 9.37607 22.2286 9.94369 21.8944 10.2695L17.5678 14.4869C17.4351 14.6163 17.3745 14.8027 17.4058 14.9854L18.4272 20.9404C18.5061 21.4005 18.0233 21.7513 17.6101 21.5341L12.2621 18.7225C12.0981 18.6362 11.9021 18.6362 11.738 18.7225L6.39002 21.5341C5.97689 21.7513 5.49404 21.4005 5.57294 20.9404L6.59432 14.9854C6.62565 14.8027 6.56509 14.6163 6.43236 14.4869L2.10573 10.2695C1.7715 9.94369 1.95594 9.37607 2.41783 9.30895L8.39708 8.44012C8.5805 8.41346 8.73906 8.29826 8.82109 8.13206L11.4951 2.71393Z"
          stroke="#CCCCCC" strokeWidth="2" strokeLinejoin="round"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return GREY_24PX()
      default:
        return DEFAULT()
    }
  }
  const PRIMARY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return PRIMARY_24PX()
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.PRIMARY:
      return PRIMARY_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgHomeIcon({color = ICON_COLOR.GREY, size = ICON_SIZE.PX24, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_24PX()
  }
  const PRIMARY_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', fill: 'none', ...sx}}>
        <path
          d="M9 21V14.1528C9 13.5226 9.53726 13.0116 10.2 13.0116H13.8C14.4627 13.0116 15 13.5226 15 14.1528V21M11.3046 3.21117L3.50457 8.48603C3.18802 8.7001 3 9.04665 3 9.41605V19.2882C3 20.2336 3.80589 21 4.8 21H19.2C20.1941 21 21 20.2336 21 19.2882V9.41605C21 9.04665 20.812 8.7001 20.4954 8.48603L12.6954 3.21117C12.2791 2.92961 11.7209 2.92961 11.3046 3.21117Z"
          stroke="#ECA548" strokeWidth="1.7" strokeLinecap="round"/>
      </SvgIcon>
    )
  }
  const GREY_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', fill: 'none', ...sx}}>
        <path
          d="M9 21V14.1528C9 13.5226 9.53726 13.0116 10.2 13.0116H13.8C14.4627 13.0116 15 13.5226 15 14.1528V21M11.3046 3.21117L3.50457 8.48603C3.18802 8.7001 3 9.04665 3 9.41605V19.2882C3 20.2336 3.80589 21 4.8 21H19.2C20.1941 21 21 20.2336 21 19.2882V9.41605C21 9.04665 20.812 8.7001 20.4954 8.48603L12.6954 3.21117C12.2791 2.92961 11.7209 2.92961 11.3046 3.21117Z"
          stroke="#CCCCCC" strokeWidth="1.7" strokeLinecap="round"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return GREY_24PX()
      default:
        return DEFAULT()
    }
  }
  const PRIMARY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return PRIMARY_24PX()
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.PRIMARY:
      return PRIMARY_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgCookIcon({color = ICON_COLOR.GREY, size = ICON_SIZE.PX16, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_16PX()
  }
  const GREY_16PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={ALT_STRING.MY.ICON_RECIPE_CNT} onClick={onClick} viewBox="0 0 16 16" sx={{width: '16px', height: '16px', fill: 'none', ...sx}}>
        <path
          d="M2.70365 11.6254H13.2962C13.5611 11.6254 13.7777 11.4211 13.7777 11.1714V9.80952C13.7777 6.9586 11.4425 4.61613 8.48143 4.3846V3.90794H8.96291C9.22772 3.90794 9.44439 3.70365 9.44439 3.45397C9.44439 3.20429 9.22772 3 8.96291 3H7.03698C6.77217 3 6.5555 3.20429 6.5555 3.45397C6.5555 3.70365 6.77217 3.90794 7.03698 3.90794H7.51846V4.3846C4.55735 4.61613 2.22217 6.9586 2.22217 9.80952V11.1714C2.22217 11.4211 2.43883 11.6254 2.70365 11.6254ZM3.18513 9.80952C3.18513 7.30816 5.34698 5.26984 7.99995 5.26984C10.6529 5.26984 12.8148 7.30816 12.8148 9.80952V10.7175H3.18513V9.80952Z"
          fill="#999999"/>
        <path
          d="M1.5 12.3062V12.7601C1.5 13.0098 1.71667 13.2141 1.98148 13.2141H14.0185C14.2833 13.2141 14.5 13.0098 14.5 12.7601V12.3062H1.5Z"
          fill="#999999"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX16:
        return GREY_16PX()
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgFavoriteIcon({color = ICON_COLOR.GREY, size = ICON_SIZE.PX16, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_16PX()
  }
  const GREY_16PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={ALT_STRING.MY.ICON_FAVORITE} onClick={onClick} viewBox="0 0 16 16" sx={{width: '16px', height: '16px', fill: 'none', ...sx}}>
        <path
          d="M3.75 4.08331C3.75 3.53103 4.19772 3.08331 4.75 3.08331H11.25C11.8023 3.08331 12.25 3.53103 12.25 4.08331V13.3491C12.25 13.773 11.7556 14.0046 11.4299 13.7332L8 10.875L4.57009 13.7332C4.24443 14.0046 3.75 13.773 3.75 13.3491V4.08331Z"
          stroke="#999999" strokeLinejoin="round"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX16:
        return GREY_16PX()
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgEyeOpenIcon({color = ICON_COLOR.GREY, size = ICON_SIZE.PX16, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_16PX()
  }
  const GREY_16PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={ALT_STRING.MY.ICON_VIEW} onClick={onClick} viewBox="0 0 16 16" sx={{width: '16px', height: '16px', fill: 'none', ...sx}}>
        <path
          d="M7.66667 12.999C12.7521 13.0776 14.3333 8.57808 14.3333 8.57808C14.3333 8.57808 12.8162 4 7.66667 4C2.51709 4 1 8.57808 1 8.57808C1 8.57808 2.5812 12.9204 7.66667 12.999Z"
          stroke="#999999"/>
        <circle cx="7.79434" cy="8.4269" r="2.35" stroke="#999999"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX16:
        return GREY_16PX()
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgInformationIcon({alt, tabIndex, color = ICON_COLOR.GREY, size = ICON_SIZE.PX16, onClick, onKeyDown, sx}) {
  const DEFAULT = () => {
    return GREY_16PX()
  }
  const GREY_16PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} tabIndex={tabIndex} onClick={onClick} onKeyDown={onKeyDown} viewBox="0 0 16 16" sx={{width: '16px', height: '16px', fill: 'none', ...sx}}>
        <path
          d="M7.46 12H8.66V7.2H7.46V12ZM8 5.72C8.18667 5.72 8.34333 5.66 8.47 5.54C8.59667 5.42 8.66 5.26667 8.66 5.08C8.66 4.89333 8.59667 4.73333 8.47 4.6C8.34333 4.46667 8.18667 4.4 8 4.4C7.81333 4.4 7.65667 4.46667 7.53 4.6C7.40333 4.73333 7.34 4.89333 7.34 5.08C7.34 5.26667 7.40333 5.42 7.53 5.54C7.65667 5.66 7.81333 5.72 8 5.72ZM8 16C6.90667 16 5.87333 15.79 4.9 15.37C3.92667 14.95 3.07667 14.3767 2.35 13.65C1.62333 12.9233 1.05 12.0733 0.63 11.1C0.21 10.1267 0 9.08667 0 7.98C0 6.88667 0.21 5.85333 0.63 4.88C1.05 3.90667 1.62333 3.06 2.35 2.34C3.07667 1.62 3.92667 1.05 4.9 0.63C5.87333 0.21 6.91333 0 8.02 0C9.11333 0 10.1467 0.21 11.12 0.63C12.0933 1.05 12.94 1.62 13.66 2.34C14.38 3.06 14.95 3.90667 15.37 4.88C15.79 5.85333 16 6.89333 16 8C16 9.09333 15.79 10.1267 15.37 11.1C14.95 12.0733 14.38 12.9233 13.66 13.65C12.94 14.3767 12.0933 14.95 11.12 15.37C10.1467 15.79 9.10667 16 8 16ZM8.02 14.8C9.9 14.8 11.5 14.1367 12.82 12.81C14.14 11.4833 14.8 9.87333 14.8 7.98C14.8 6.1 14.14 4.5 12.82 3.18C11.5 1.86 9.89333 1.2 8 1.2C6.12 1.2 4.51667 1.86 3.19 3.18C1.86333 4.5 1.2 6.10667 1.2 8C1.2 9.88 1.86333 11.4833 3.19 12.81C4.51667 14.1367 6.12667 14.8 8.02 14.8Z"
          fill="#CCCCCC"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX16:
        return GREY_16PX()
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgAlertIcon({color = ICON_COLOR.GREY, size = ICON_SIZE.PX16, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_16PX()
  }
  const GREY_16PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 16 16" sx={{width: '16px', height: '16px', fill: 'none', ...sx}}>
        <path
          d="M8.00007 8.81229V4.6123M8.00001 11.1456V10.979M14.4001 8.0001C14.4001 11.5347 11.5347 14.4001 8.0001 14.4001C4.46547 14.4001 1.6001 11.5347 1.6001 8.0001C1.6001 4.46547 4.46547 1.6001 8.0001 1.6001C11.5347 1.6001 14.4001 4.46547 14.4001 8.0001Z"
          stroke="#999999" strokeLinecap="round" strokeLinejoin="round"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX16:
        return GREY_16PX()
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgSearchHomeIcon({color = ICON_COLOR.GREY, size = ICON_SIZE.PX24, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_24PX()
  }
  const PRIMARY_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', fill: 'none', ...sx}}>
        <circle cx="10.9046" cy="11.1666" r="8.04762" stroke="#ECA548" strokeWidth="2"/>
        <path d="M16.3809 17.119L21.1428 21.8809" stroke="#ECA548" strokeWidth="2" strokeLinecap="round"/>
      </SvgIcon>
    )
  }

  const GREY_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', fill: 'none', ...sx}}>
        <circle cx="10.9048" cy="11.1666" r="8.04762" stroke="#CCCCCC" strokeWidth="2"/>
        <path d="M16.381 17.119L21.1429 21.8809" stroke="#CCCCCC" strokeWidth="2" strokeLinecap="round"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return GREY_24PX()
      default:
        return DEFAULT()
    }
  }
  const PRIMARY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return PRIMARY_24PX()
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.PRIMARY:
      return PRIMARY_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgSearchIcon({alt, tabIndex, color = ICON_COLOR.BLACK, size = ICON_SIZE.PX24, onClick, onKeyDown, sx}) {
  const DEFAULT = () => {
    return GREY_24PX()
  }
  const PRIMARY_24PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} tabIndex={tabIndex} onClick={onClick} onKeyDown={onKeyDown} viewBox="0 0 24 24"
               sx={{width: '24px', height: '24px', fill: 'none', ...sx}}>
        <circle cx="10.9046" cy="11.1666" r="8.04762" stroke="#ECA548" strokeWidth="2"/>
        <path d="M16.3809 17.119L21.1428 21.8809" stroke="#ECA548" strokeWidth="2" strokeLinecap="round"/>
      </SvgIcon>
    )
  }

  const GREY_24PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} tabIndex={tabIndex} onClick={onClick} onKeyDown={onKeyDown} viewBox="0 0 24 24"
               sx={{width: '24px', height: '24px', fill: 'none', ...sx}}>
        <circle cx="10.9048" cy="11.1666" r="8.04762" stroke="#CCCCCC" strokeWidth="2"/>
        <path d="M16.381 17.119L21.1429 21.8809" stroke="#CCCCCC" strokeWidth="2" strokeLinecap="round"/>
      </SvgIcon>
    )
  }

  const BLACK_24PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} tabIndex={tabIndex} onClick={onClick} onKeyDown={onKeyDown} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', ...sx}}>
        <path fillRule="evenodd" clipRule="evenodd"
              d="M18.4844 16.7616L22.6386 20.9036C22.8699 21.133 23 21.4453 23 21.7711C23 22.0969 22.8699 22.4092 22.6386 22.6386C22.4092 22.8699 22.0969 23 21.7711 23C21.4453 23 21.133 22.8699 20.9036 22.6386L16.7616 18.4844C15.054 19.8247 12.9454 20.552 10.7746 20.5493C5.37625 20.5493 1 16.173 1 10.7746C1 5.37625 5.37625 1 10.7746 1C16.173 1 20.5493 5.37625 20.5493 10.7746C20.552 12.9454 19.8247 15.054 18.4844 16.7616ZM10.7748 3.44365C6.72604 3.44365 3.44385 6.72584 3.44385 10.7746C3.44385 14.8234 6.72604 18.1056 10.7748 18.1056C14.8236 18.1056 18.1058 14.8234 18.1058 10.7746C18.1058 6.72584 14.8236 3.44365 10.7748 3.44365Z"
              fill="#222222"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return GREY_24PX()
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return BLACK_24PX()
      default:
        return DEFAULT()
    }
  }
  const PRIMARY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return PRIMARY_24PX()
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    case ICON_COLOR.PRIMARY:
      return PRIMARY_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgHandleIcon({alt, color = ICON_COLOR.GREY, size = ICON_SIZE.PX24, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_24PX()
  }
  const GREY_24PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', ...sx}}>
        <line x1="3.5" y1="6.5" x2="20.5" y2="6.5" stroke="#E6E6E6" strokeWidth="3" strokeLinecap="round"/>
        <line x1="3.5" y1="12.5" x2="20.5" y2="12.5" stroke="#E6E6E6" strokeWidth="3" strokeLinecap="round"/>
        <line x1="3.5" y1="18.5" x2="20.5" y2="18.5" stroke="#E6E6E6" strokeWidth="3" strokeLinecap="round"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return GREY_24PX()
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgResetIcon({color = ICON_COLOR.GREY, size = ICON_SIZE.PX24, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_24PX()
  }
  const GREY_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', ...sx}}>
        <path
          d="M12.0009 7.32339V5C12.0009 5 10.1725 6.91137 9.00098 8.05638L12.0009 10.864V8.78938C14.4834 8.78938 16.5009 10.7611 16.5009 13.1873C16.5009 15.1478 15.1811 16.8051 13.3685 17.3723C13.0187 17.4818 12.7509 17.7881 12.7509 18.1545C12.7509 18.604 13.1462 18.9558 13.5807 18.8407C16.1299 18.1651 18.0009 15.8921 18.0009 13.1873C18.0009 9.94751 15.3159 7.32339 12.0009 7.32339Z"
          fill="#888888"/>
        <path
          d="M7.49998 13.1872C7.49998 12.2465 7.80399 11.3723 8.32378 10.6543C8.56836 10.3164 8.57447 9.83937 8.27619 9.54785C7.9867 9.26493 7.51973 9.26308 7.26775 9.57987C6.47586 10.5755 6 11.8271 6 13.1872C6 15.8919 7.871 18.1649 10.4202 18.8405C10.8547 18.9557 11.2499 18.6039 11.2499 18.1544C11.2499 17.7879 10.9821 17.4816 10.6324 17.3722C8.8198 16.805 7.49998 15.1477 7.49998 13.1872Z"
          fill="#888888"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return GREY_24PX()
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgSmallPlusIcon({color = ICON_COLOR.BLACK, size = ICON_SIZE.PX24, onClick, sx}) {
  const DEFAULT = () => {
    return BLACK_24PX()
  }
  const BLACK_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', ...sx}}>
        <path d="M12 7.19995L12 16.8M16.8 12L7.19995 12" stroke="black" strokeWidth="2" strokeLinecap="round"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return BLACK_24PX()
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgEditIcon({color = ICON_COLOR.GREY, size = ICON_SIZE.PX24, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_24PX()
  }
  const GREY_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', ...sx}}>
        <g clipPath="url(#clip0_1473_16794)">
          <path
            d="M14.5967 17.4632L16.8762 15.1838C17.1376 14.9223 16.9579 14.4811 16.5902 14.4811L15.1278 14.4811L15.1278 7.94495C15.1278 7.49559 14.7601 7.12793 14.3107 7.12793C13.8614 7.12793 13.4937 7.49559 13.4937 7.94495L13.4937 14.4811L12.0313 14.4811C11.6636 14.4811 11.4839 14.9223 11.7453 15.1756L14.0248 17.4551C14.18 17.6185 14.4415 17.6185 14.5967 17.4632Z"
            fill="#888888"/>
          <path
            d="M8.4033 5.11536L6.12381 7.39485C5.86236 7.6563 6.04211 8.09749 6.40976 8.09749L7.87223 8.09749L7.87223 14.6337C7.87223 15.083 8.23989 15.4507 8.68925 15.4507C9.13862 15.4507 9.50627 15.083 9.50627 14.6337L9.50628 8.09749L10.9687 8.09749C11.3364 8.09749 11.5161 7.6563 11.2547 7.40302L8.97521 5.12354C8.81998 4.96013 8.55853 4.96013 8.4033 5.11536Z"
            fill="#888888"/>
        </g>
        <defs>
          <clipPath id="clip0_1473_16794">
            <rect width="24" height="24" fill="white"/>
          </clipPath>
        </defs>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return GREY_24PX()
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgPlusButtonIcon({alt, tabIndex, color = ICON_COLOR.GREY, size = ICON_SIZE.PX24, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_24PX()
  }
  const GREY_24PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} tabIndex={tabIndex} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', ...sx}}>
        <rect width="24" height="24" rx="4" fill="#F5F5F5"/>
        <path d="M12 8L12 16M16 12L8 12" stroke="#222222" strokeWidth="1.5" strokeLinecap="round"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return GREY_24PX()
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgMinusButtonIcon({alt, tabIndex, color = ICON_COLOR.GREY, size = ICON_SIZE.PX24, onClick, sx}) {
  const DEFAULT = () => {
    return GREY_24PX()
  }
  const GREY_24PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} tabIndex={tabIndex} onClick={onClick} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', ...sx}}>
        <rect width="24" height="24" rx="4" fill="#F5F5F5"/>
        <path d="M16 12L8 12" stroke="#222222" strokeWidth="1.5" strokeLinecap="round"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return GREY_24PX()
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgCloseIcon({alt, tabIndex, color = ICON_COLOR.BLACK, size = ICON_SIZE.PX24, onClick, onKeyDown, sx}) {
  const DEFAULT = () => {
    return BLACK_24PX()
  }
  const BLACK_24PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} tabIndex={tabIndex} onClick={onClick} onKeyDown={onKeyDown} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', ...sx}}>
        <path
          d="M19.6439 6.06821C20.1167 5.59542 20.1167 4.82887 19.6439 4.35607C19.1711 3.88328 18.4046 3.88328 17.9318 4.35607L12 10.2879L6.06821 4.35607C5.59542 3.88328 4.82887 3.88328 4.35607 4.35607C3.88328 4.82887 3.88328 5.59542 4.35607 6.06821L10.2879 12L4.35607 17.9318C3.88328 18.4046 3.88328 19.1711 4.35607 19.6439C4.82887 20.1167 5.59542 20.1167 6.06821 19.6439L12 13.7121L17.9318 19.6439C18.4046 20.1167 19.1711 20.1167 19.6439 19.6439C20.1167 19.1711 20.1167 18.4046 19.6439 17.9318L13.7121 12L19.6439 6.06821Z"
          fill="#222222"/>
      </SvgIcon>
    )
  }
  const WHITE_24PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} tabIndex={tabIndex} onClick={onClick} onKeyDown={onKeyDown} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', ...sx}}>
        <path
          d="M19.6439 6.06821C20.1167 5.59542 20.1167 4.82887 19.6439 4.35607V4.35607C19.1711 3.88328 18.4046 3.88328 17.9318 4.35607L12 10.2879L6.06821 4.35607C5.59542 3.88328 4.82887 3.88328 4.35607 4.35607V4.35607C3.88328 4.82887 3.88328 5.59542 4.35607 6.06821L10.2879 12L4.35607 17.9318C3.88328 18.4046 3.88328 19.1711 4.35607 19.6439V19.6439C4.82887 20.1167 5.59542 20.1167 6.06821 19.6439L12 13.7121L17.9318 19.6439C18.4046 20.1167 19.1711 20.1167 19.6439 19.6439V19.6439C20.1167 19.1711 20.1167 18.4046 19.6439 17.9318L13.7121 12L19.6439 6.06821Z"
          fill="white"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return BLACK_24PX()
      default:
        return DEFAULT()
    }
  }
  const WHITE_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return WHITE_24PX()
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    case ICON_COLOR.WHITE:
      return WHITE_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgBackIcon({alt, tabIndex, color = ICON_COLOR.BLACK, size = ICON_SIZE.PX24, onClick, onKeyDown, sx}) {
  const DEFAULT = () => {
    return BLACK_24PX()
  }
  const BLACK_24PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} tabIndex={tabIndex} onClick={onClick} onKeyDown={onKeyDown} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', ...sx}}>
        <path
          d="M23.5 12C23.5 11.3096 22.9404 10.75 22.25 10.75H5.2875L11.3875 4.65001C11.8771 4.16036 11.8757 3.36605 11.3844 2.87814C10.8954 2.39265 10.106 2.39405 9.61874 2.88126L1.24766 11.2523C0.83474 11.6653 0.83474 12.3347 1.24766 12.7477L9.61836 21.1184C10.1053 21.6053 10.8947 21.6053 11.3816 21.1184C11.8683 20.6317 11.8686 19.8429 11.3824 19.3559L5.2875 13.25H22.25C22.9404 13.25 23.5 12.6904 23.5 12Z"
          fill="#222222"/>
      </SvgIcon>
    )
  }
  const WHITE_24PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} tabIndex={tabIndex} onClick={onClick} onKeyDown={onKeyDown} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', ...sx}}>
        <path
          d="M23.5 12C23.5 11.3096 22.9404 10.75 22.25 10.75H5.2875L11.3875 4.65001C11.8771 4.16036 11.8757 3.36605 11.3844 2.87814C10.8954 2.39265 10.106 2.39405 9.61874 2.88126L1.24766 11.2523C0.83474 11.6653 0.83474 12.3347 1.24766 12.7477L9.61836 21.1184C10.1053 21.6053 10.8947 21.6053 11.3816 21.1184C11.8683 20.6317 11.8686 19.8429 11.3824 19.3559L5.2875 13.25H22.25C22.9404 13.25 23.5 12.6904 23.5 12Z"
          fill="white"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return BLACK_24PX()
      default:
        return DEFAULT()
    }
  }
  const WHITE_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX24:
        return WHITE_24PX()
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    case ICON_COLOR.WHITE:
      return WHITE_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgDeleteIcon({alt, tabIndex, color = ICON_COLOR.BLACK, size = ICON_SIZE.PX20, onClick, onKeyDown, sx}) {
  const DEFAULT = () => {
    return BLACK_20PX()
  }
  const BLACK_20PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} tabIndex={tabIndex} onClick={onClick} onKeyDown={onKeyDown} viewBox="0 0 20 20" sx={{width: '20px', height: '20px', ...sx}}>
        <path
          d="M20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10Z"
          fill="black" fillOpacity="0.7"/>
        <path d="M13 7L10 10M10 10L7 13M10 10L13 13M10 10L7 7" stroke="white" strokeWidth="1.5" strokeLinecap="round"/>
      </SvgIcon>
    )
  }
  const GREY_20PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} tabIndex={tabIndex} onClick={onClick} onKeyDown={onKeyDown} viewBox="0 0 20 20" sx={{width: '20px', height: '20px', ...sx}}>
        <path
          d="M20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10Z"
          fill="#CCCCCC"/>
        <path d="M13 7L10 10M10 10L7 13M10 10L13 13M10 10L7 7" stroke="white" strokeWidth="1.5" strokeLinecap="round"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX20:
        return GREY_20PX()
      default:
        return DEFAULT()
    }
  }
  const BLACK_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX20:
        return BLACK_20PX()
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.BLACK:
      return BLACK_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgBoyIcon({color = ICON_COLOR.GREY, sx}) {
  const DEFAULT = () => {
    return GREY_96PX()
  }

  const GREY_96PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 96 96" sx={{width: '96px', height: '96px', fill: 'none', ...sx}}>
        <path d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z"
              fill="#E6E6E6"/>
        <path
          d="M48.0096 51.1584C56.4556 51.1584 63.3024 42.9921 63.3024 32.9184C63.3024 22.8447 56.4556 14.6784 48.0096 14.6784C39.5636 14.6784 32.7168 22.8447 32.7168 32.9184C32.7168 42.9921 39.5636 51.1584 48.0096 51.1584Z"
          fill="white"/>
        <path
          d="M32.2271 30.48C32.4383 31.5648 45.7919 32.6784 53.2607 23.5776C53.7503 23.1648 53.8751 31.1616 64.0799 31.4208C64.7231 30.7776 67.4591 18.8832 55.3343 16.6464C55.3343 16.6464 47.0495 10.0032 37.7855 17.1168C37.7855 17.1168 30.0671 22.3008 32.2271 30.4704V30.48Z"
          fill="#999999"/>
        <path d="M8.84155 96C8.84155 73.92 26.736 56.0256 48.816 56.0256C70.896 56.0256 88.7904 73.92 88.7904 96" fill="white"/>
      </SvgIcon>
    )
  }

  const PRIMARY_96PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 96 96" sx={{width: '96px', height: '96px', fill: 'none', ...sx}}>
        <path d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z"
              fill="#FFECBB"/>
        <path
          d="M48.0096 51.1583C56.4556 51.1583 63.3024 42.992 63.3024 32.9183C63.3024 22.8447 56.4556 14.6783 48.0096 14.6783C39.5636 14.6783 32.7168 22.8447 32.7168 32.9183C32.7168 42.992 39.5636 51.1583 48.0096 51.1583Z"
          fill="white"/>
        <path
          d="M32.2271 30.48C32.4383 31.5648 45.7919 32.6784 53.2607 23.5776C53.7503 23.1648 53.8751 31.1616 64.0799 31.4208C64.7231 30.7776 67.4591 18.8832 55.3343 16.6464C55.3343 16.6464 47.0495 10.0032 37.7855 17.1168C37.7855 17.1168 30.0671 22.3008 32.2271 30.4704V30.48Z"
          fill="#FFCB65"/>
        <path d="M8.84155 96C8.84155 73.92 26.736 56.0256 48.816 56.0256C70.896 56.0256 88.7904 73.92 88.7904 96" fill="white"/>
      </SvgIcon>
    )
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_96PX()
    case ICON_COLOR.PRIMARY:
      return PRIMARY_96PX()
    default:
      return DEFAULT()
  }
}

export function SvgGirlIcon({color = ICON_COLOR.GREY, sx}) {
  const DEFAULT = () => {
    return GREY_96PX()
  }

  const GREY_96PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 96 96" sx={{width: '96px', height: '96px', fill: 'none', ...sx}}>
        <path d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z"
              fill="#E6E6E6"/>
        <path
          d="M36.5471 46.7232C36.5471 46.7232 31.5455 47.8944 25.7183 46.416C25.7183 46.416 24.5855 23.8944 37.5071 16.8288C37.5071 16.8288 46.8287 10.5984 57.7535 16.8288C57.7535 16.8288 73.0559 26.64 70.5791 46.224C70.5791 46.224 66.9215 49.0176 58.5215 46.224L36.5567 46.7232H36.5471Z"
          fill="#999999"/>
        <path d="M9.04321 96C9.04321 73.92 26.9376 56.0256 49.0176 56.0256C71.0976 56.0256 88.992 73.92 88.992 96" fill="white"/>
        <path
          d="M48.2017 51.8112C56.3985 51.8112 63.0433 43.6835 63.0433 33.6576C63.0433 23.6316 56.3985 15.504 48.2017 15.504C40.0049 15.504 33.3601 23.6316 33.3601 33.6576C33.3601 43.6835 40.0049 51.8112 48.2017 51.8112Z"
          fill="white"/>
        <path
          d="M33.0625 32.6112C34.4257 33.2544 43.3921 34.176 47.9617 26.0736C47.9617 26.0736 52.5409 34.4544 63.2737 33.9936C63.7153 33.936 64.9633 16.3872 48.4321 15.3408C48.3361 15.3408 32.8225 14.9088 33.0721 32.6112H33.0625Z"
          fill="#999999"/>
      </SvgIcon>
    )
  }

  const PRIMARY_96PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 96 96" sx={{width: '96px', height: '96px', fill: 'none', ...sx}}>
        <path d="M48 96C74.5097 96 96 74.5097 96 48C96 21.4903 74.5097 0 48 0C21.4903 0 0 21.4903 0 48C0 74.5097 21.4903 96 48 96Z"
              fill="#FFECBB"/>
        <path
          d="M36.5471 46.7233C36.5471 46.7233 31.5455 47.8945 25.7183 46.4161C25.7183 46.4161 24.5855 23.8945 37.5071 16.8289C37.5071 16.8289 46.8287 10.5985 57.7535 16.8289C57.7535 16.8289 73.0559 26.6401 70.5791 46.2241C70.5791 46.2241 66.9215 49.0177 58.5215 46.2241L36.5567 46.7233H36.5471Z"
          fill="#FFCB65"/>
        <path d="M9.04321 96C9.04321 73.92 26.9376 56.0256 49.0176 56.0256C71.0976 56.0256 88.992 73.92 88.992 96" fill="white"/>
        <path
          d="M48.2017 51.8111C56.3985 51.8111 63.0433 43.6835 63.0433 33.6575C63.0433 23.6316 56.3985 15.5039 48.2017 15.5039C40.0049 15.5039 33.3601 23.6316 33.3601 33.6575C33.3601 43.6835 40.0049 51.8111 48.2017 51.8111Z"
          fill="white"/>
        <path
          d="M33.0625 32.6112C34.4257 33.2544 43.3921 34.176 47.9617 26.0736C47.9617 26.0736 52.5409 34.4544 63.2737 33.9936C63.7153 33.936 64.9633 16.3872 48.4321 15.3408C48.3361 15.3408 32.8225 14.9088 33.0721 32.6112H33.0625Z"
          fill="#FFCB65"/>
      </SvgIcon>
    )
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_96PX()
    case ICON_COLOR.PRIMARY:
      return PRIMARY_96PX()
    default:
      return DEFAULT()
  }
}

export function SvgCheckIcon({alt, color = ICON_COLOR.PRIMARY, size = ICON_SIZE.PX20, sx}) {
  const DEFAULT = () => {
    return PRIMARY_20PX()
  }

  const GREY_20PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} viewBox="0 0 20 20" sx={{width: '20px', height: '20px', fill: 'none', ...sx}}>
        <path
          d="M8.21674 16.1053C7.98577 16.1046 7.76547 16.0029 7.60841 15.8246L3.55841 11.2895C3.24314 10.9359 3.25993 10.3802 3.59591 10.0483C3.93188 9.71646 4.45981 9.73414 4.77507 10.0878L8.20841 13.9387L15.2167 5.8685C15.4105 5.61447 15.7221 5.49443 16.0259 5.55675C16.3297 5.61907 16.5756 5.85347 16.6646 6.1655C16.7536 6.47754 16.671 6.81579 16.4501 7.04393L8.83341 15.8159C8.67781 15.9974 8.45731 16.1023 8.22507 16.1053H8.21674Z"
          fill="#CCCCCC"/>
      </SvgIcon>
    )
  }

  const PRIMARY_20PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} viewBox="0 0 20 20" sx={{widthL: '20px', height: '20px', fill: 'none', ...sx}}>
        <path
          d="M8.21674 15.7894C7.98577 15.7886 7.76547 15.687 7.60841 15.5087L3.55841 10.9736C3.24314 10.62 3.25993 10.0643 3.59591 9.7324C3.93188 9.40055 4.45981 9.41822 4.77507 9.77188L8.20841 13.6228L15.2167 5.55258C15.4105 5.29856 15.7221 5.17851 16.0259 5.24083C16.3297 5.30315 16.5756 5.53756 16.6646 5.84959C16.7536 6.16162 16.671 6.49987 16.4501 6.72802L8.83341 15.4999C8.67781 15.6815 8.45731 15.7864 8.22507 15.7894H8.21674Z"
          fill="#ECA548"/>
      </SvgIcon>
    )
  }

  const GREY_18PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} viewBox="0 0 18 18" sx={{width: '18px', height: '18px', fill: 'none', ...sx}}>
        <path
          d="M7.39507 14.3157C7.18719 14.315 6.98892 14.2235 6.84757 14.0631L3.20257 9.98149C2.91883 9.6632 2.93394 9.16306 3.23632 8.86439C3.53869 8.56572 4.01383 8.58162 4.29757 8.89991L7.38757 12.3657L13.6951 5.10254C13.8695 4.87392 14.1499 4.76589 14.4233 4.82197C14.6967 4.87806 14.9181 5.08902 14.9982 5.36985C15.0783 5.65068 15.0039 5.9551 14.8051 6.16044L7.95007 14.0552C7.81003 14.2185 7.61158 14.313 7.40257 14.3157H7.39507Z"
          fill="#CCCCCC"/>
      </SvgIcon>
    )
  }

  const PRIMARY_18PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} viewBox="0 0 18 18" sx={{width: '18px', height: '18px', fill: 'none', ...sx}}>
        <path
          d="M7.39482 14.3157C7.18695 14.315 6.98868 14.2235 6.84732 14.0631L3.20232 9.98149C2.91859 9.6632 2.9337 9.16306 3.23607 8.86439C3.53845 8.56572 4.01359 8.58162 4.29732 8.89991L7.38732 12.3657L13.6948 5.10254C13.8692 4.87392 14.1497 4.76589 14.4231 4.82197C14.6965 4.87806 14.9178 5.08902 14.9979 5.36985C15.078 5.65068 15.0037 5.9551 14.8048 6.16044L7.94982 14.0552C7.80979 14.2185 7.61134 14.313 7.40232 14.3157H7.39482Z"
          fill="#ECA548"/>
      </SvgIcon>
    )
  }

  const GREY_16PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} viewBox="0 0 16 16" sx={{width: '16px', height: '16px', fill: 'none', ...sx}}>
        <path d="M12.8002 4.40002L5.64068 11.6L3.2002 9.14574" stroke="#E6E6E6" strokeWidth="2" strokeLinecap="round"
              strokeLinejoin="round"/>
      </SvgIcon>
    )
  }

  const PRIMARY_16PX = () => {
    return (
      <SvgIcon role={'img'} titleAccess={alt} viewBox="0 0 16 16" sx={{width: '16px', height: '16px', fill: 'none', ...sx}}>
        <path d="M12.8002 4.3999L5.64068 11.5999L3.2002 9.14562" stroke="#ECA548" strokeWidth="2" strokeLinecap="round"
              strokeLinejoin="round"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX18:
        return GREY_18PX()
      case ICON_SIZE.PX16:
        return GREY_16PX()
      default:
        return GREY_20PX()
    }
  }

  const PRIMARY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX18:
        return PRIMARY_18PX()
      case ICON_SIZE.PX16:
        return PRIMARY_16PX()
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.PRIMARY:
      return PRIMARY_ICON(size)
    default:
      return DEFAULT()
  }
}

export function SvgCheckRoundIcon({color = ICON_COLOR.PRIMARY, size = ICON_SIZE.PX30, sx}) {
  const DEFAULT = () => {
    return PRIMARY_30PX()
  }

  const GREY_30PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 30 30" sx={{width: '30px', height: '30px', fill: 'none', ...sx}}>
        <rect x="4.16675" y="4.16675" width="21.6667" height="21.6667" rx="10.8333" fill="#CCCCCC"/>
        <path
          d="M13.8111 18.3331C13.6571 18.3326 13.5102 18.2683 13.4055 18.1554L10.7055 15.2831C10.4953 15.0592 10.5065 14.7072 10.7305 14.497C10.9545 14.2869 11.3065 14.298 11.5166 14.522L13.8055 16.9609L18.4777 11.8498C18.6069 11.6889 18.8147 11.6129 19.0172 11.6524C19.2197 11.6918 19.3837 11.8403 19.443 12.0379C19.5023 12.2355 19.4473 12.4498 19.3 12.5942L14.2222 18.1498C14.1185 18.2648 13.9715 18.3312 13.8166 18.3331H13.8111Z"
          fill="white"/>
      </SvgIcon>
    )
  }

  const PRIMARY_30PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 30 30" sx={{width: '30px', height: '30px', fill: 'none', ...sx}}>
        <rect x="4.1665" y="4.16675" width="21.6667" height="21.6667" rx="10.8333" fill="#ECA548"/>
        <path
          d="M13.8113 18.3331C13.6573 18.3326 13.5105 18.2683 13.4058 18.1554L10.7058 15.2831C10.4956 15.0592 10.5068 14.7072 10.7308 14.497C10.9547 14.2869 11.3067 14.298 11.5169 14.522L13.8058 16.9609L18.478 11.8498C18.6072 11.6889 18.8149 11.6129 19.0174 11.6524C19.22 11.6918 19.3839 11.8403 19.4433 12.0379C19.5026 12.2355 19.4475 12.4498 19.3002 12.5942L14.2224 18.1498C14.1187 18.2648 13.9717 18.3312 13.8169 18.3331H13.8113Z"
          fill="white"/>
      </SvgIcon>
    )
  }

  const GREY_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', fill: 'none', ...sx}}>
        <path
          d="M2.3999 11.9999C2.3999 6.69797 6.69797 2.3999 11.9999 2.3999C17.3018 2.3999 21.5999 6.69797 21.5999 11.9999C21.5999 17.3018 17.3018 21.5999 11.9999 21.5999C6.69797 21.5999 2.3999 17.3018 2.3999 11.9999Z"
          fill="#E6E6E6"/>
        <path d="M15.3515 9.84839L10.8 14.3999L9.24854 12.8484" stroke="white" strokeWidth="2" strokeLinecap="round"
              strokeLinejoin="round"/>
      </SvgIcon>
    )
  }

  const PRIMARY_24PX = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 24 24" sx={{width: '24px', height: '24px', fill: 'none', ...sx}}>
        <path
          d="M2.3999 11.9999C2.3999 6.69797 6.69797 2.3999 11.9999 2.3999C17.3018 2.3999 21.5999 6.69797 21.5999 11.9999C21.5999 17.3018 17.3018 21.5999 11.9999 21.5999C6.69797 21.5999 2.3999 17.3018 2.3999 11.9999Z"
          fill="#ECA548"/>
        <path d="M15.3515 9.84839L10.8 14.3999L9.24854 12.8484" stroke="white" strokeWidth="2" strokeLinecap="round"
              strokeLinejoin="round"/>
      </SvgIcon>
    )
  }

  const GREY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX30:
        return GREY_30PX()
      case ICON_SIZE.PX24:
        return GREY_24PX()
      default:
        return GREY_30PX()
    }
  }

  const PRIMARY_ICON = (size) => {
    switch (size) {
      case ICON_SIZE.PX30:
        return PRIMARY_30PX()
      case ICON_SIZE.PX24:
        return PRIMARY_24PX()
      default:
        return DEFAULT()
    }
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_ICON(size)
    case ICON_COLOR.PRIMARY:
      return PRIMARY_ICON(size)
    default:
      return DEFAULT()
  }
}

// 시계 아이콘
export function SvgWatchRoundIcon({color = ICON_COLOR.GREY, sx}) {
  const DEFAULT = (sx) => {
    return GREY_14PX(sx)
  }

  const GREY_14PX = (sx) => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 14 14" sx={{width: '14px', height: '14px', fill: 'none', ...sx}}>
        <path fillRule="evenodd" clipRule="evenodd"
              d="M1.16663 7.00008C1.16663 3.77842 3.7783 1.16675 6.99996 1.16675C8.54706 1.16675 10.0308 1.78133 11.1247 2.87529C12.2187 3.96925 12.8333 5.45299 12.8333 7.00008C12.8333 10.2217 10.2216 12.8334 6.99996 12.8334C3.7783 12.8334 1.16663 10.2217 1.16663 7.00008ZM6.99996 7.58341H9.33329C9.65546 7.58341 9.91663 7.32225 9.91663 7.00008C9.91663 6.67792 9.65546 6.41675 9.33329 6.41675H7.58329V4.66675C7.58329 4.34458 7.32213 4.08341 6.99996 4.08341C6.67779 4.08341 6.41663 4.34458 6.41663 4.66675V7.00008C6.41663 7.32225 6.67779 7.58341 6.99996 7.58341Z"
              fill="#888888"/>
      </SvgIcon>
    )
  }

  const RED_14PX = (sx) => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 14 14" sx={{width: '14px', height: '14px', fill: 'none', ...sx}}>
        <path fillRule="evenodd" clipRule="evenodd"
              d="M1.16663 7.00008C1.16663 3.77842 3.7783 1.16675 6.99996 1.16675C8.54706 1.16675 10.0308 1.78133 11.1247 2.87529C12.2187 3.96925 12.8333 5.45299 12.8333 7.00008C12.8333 10.2217 10.2216 12.8334 6.99996 12.8334C3.7783 12.8334 1.16663 10.2217 1.16663 7.00008ZM6.99996 7.58341H9.33329C9.65546 7.58341 9.91663 7.32225 9.91663 7.00008C9.91663 6.67792 9.65546 6.41675 9.33329 6.41675H7.58329V4.66675C7.58329 4.34458 7.32213 4.08341 6.99996 4.08341C6.67779 4.08341 6.41663 4.34458 6.41663 4.66675V7.00008C6.41663 7.32225 6.67779 7.58341 6.99996 7.58341Z"
              fill="#FF4842"/>
      </SvgIcon>
    )
  }

  switch (color) {
    case ICON_COLOR.GREY:
      return GREY_14PX(sx)
    case ICON_COLOR.RED:
      return RED_14PX(sx)
    default:
      return DEFAULT(sx)
  }
}
