import {SvgIcon} from "@mui/material";

export const ALLERGY_NONE = (onClick, isSelected) => {
  const SELECT_NONE = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 38 38" sx={{width: '38px', height: '38px', fill: 'none'}}>
        <circle cx="19" cy="19" r="17.5" stroke="#ECA548" strokeWidth="3"/>
        <path d="M7.6001 6.65002L31.3501 31.35" stroke="#ECA548" strokeWidth="3" strokeLinecap="round"/>
      </SvgIcon>
    )
  }

  const UNSELECT_NONE = () => {
    return (
      <SvgIcon role={'img'} aria-hidden={true} onClick={onClick} viewBox="0 0 38 38" sx={{width: '38px', height: '38px', fill: 'none'}}>
        <circle cx="19" cy="19" r="17.5" stroke="#CCCCCC" strokeWidth="3"/>
        <path d="M7.6001 6.65002L31.3501 31.35" stroke="#CCCCCC" strokeWidth="3" strokeLinecap="round"/>
      </SvgIcon>
    )
  }

  return isSelected ? SELECT_NONE() : UNSELECT_NONE()
}
