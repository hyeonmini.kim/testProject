import Image from "../../../../../components/image";
import {PATH_DONOTS_STATIC_PAGE} from "../../../../../routes/paths";

export const NUTRITION_SUR03_LAC07 = (onClick, sx) => {
  return (
    <Image
      alt={''}
      src={PATH_DONOTS_STATIC_PAGE.ICONS('/images/icons/lab/nutrition/sur03/LAC07.png')}
      // src={'/assets/images/lab/nutrition/sur03/LAC07.png'}
      onClick={onClick}
      sx={{width: '80px', height: '80px', ...sx}}
    />
  )
}