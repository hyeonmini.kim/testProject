import Image from "../../../components/image";
import {PATH_DONOTS_STATIC_PAGE} from "../../../routes/paths";

export const LAB_RECOMMEND_0 = (onClick, sx) => {
  return (
    <Image
      alt={''}
      src={PATH_DONOTS_STATIC_PAGE.ICONS('/images/icons/lab/lab_recommend_0.png')}
      // src={'/assets/images/lab/lab_recommend_0.png'}
      onClick={onClick}
      sx={{width: '40px', height: '40px', ...sx}}
    />
  )
}