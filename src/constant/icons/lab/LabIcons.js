import {BABYFOOD_TEXT, LAB_RECOMMEND_TEXT, NUTRITION_TEXT} from "../../common/labKeyword";
import {BABYFOOD_BOB01} from "./babyfood/bob01";
import {BABYFOOD_BOB02} from "./babyfood/bob02";
import {BABYFOOD_BOB03} from "./babyfood/bob03";
import {BABYFOOD_BOB04} from "./babyfood/bob04";
import {BABYFOOD_BOB05} from "./babyfood/bob05";
import {BABYFOOD_BOB06} from "./babyfood/bob06";
import {BABYFOOD_BOB07} from "./babyfood/bob07";
import {BABYFOOD_BOB08} from "./babyfood/bob08";
import {NUTRITION_SUR03_LAC01} from "./nutrition/sur03/lac01";
import {NUTRITION_SUR03_LAC02} from "./nutrition/sur03/lac02";
import {NUTRITION_SUR03_LAC03} from "./nutrition/sur03/lac03";
import {NUTRITION_SUR03_LAC04} from "./nutrition/sur03/lac04";
import {NUTRITION_SUR03_LAC05} from "./nutrition/sur03/lac05";
import {NUTRITION_SUR03_LAC06} from "./nutrition/sur03/lac06";
import {NUTRITION_SUR03_LAC07} from "./nutrition/sur03/lac07";
import {NUTRITION_SUR03_LAC08} from "./nutrition/sur03/lac08";
import {NUTRITION_SUR03_LAC09} from "./nutrition/sur03/lac09";
import {NUTRITION_SUR03_LAC10} from "./nutrition/sur03/lac10";
import {NUTRITION_SUR03_LAC11} from "./nutrition/sur03/lac11";
import {NUTRITION_SUR03_LAC12} from "./nutrition/sur03/lac12";
import {NUTRITION_SUR04_VIT01} from "./nutrition/sur04/vit01";
import {NUTRITION_SUR04_VIT02} from "./nutrition/sur04/vit02";
import {NUTRITION_SUR04_VIT03} from "./nutrition/sur04/vit03";
import {NUTRITION_SUR04_VIT04} from "./nutrition/sur04/vit04";
import {NUTRITION_SUR04_VIT05} from "./nutrition/sur04/vit05";
import {NUTRITION_SUR04_VIT06} from "./nutrition/sur04/vit06";
import {NUTRITION_SUR04_VIT07} from "./nutrition/sur04/vit07";
import {NUTRITION_SUR04_VIT08} from "./nutrition/sur04/vit08";
import {NUTRITION_SUR04_VIT09} from "./nutrition/sur04/vit09";
import {NUTRITION_SUR04_VIT10} from "./nutrition/sur04/vit10";
import {NUTRITION_SUR04_VIT11} from "./nutrition/sur04/vit11";
import {NUTRITION_SUR04_VIT12} from "./nutrition/sur04/vit12";
import {NUTRITION_SUR05_IRO01} from "./nutrition/sur05/iro01";
import {NUTRITION_SUR05_IRO02} from "./nutrition/sur05/iro02";
import {NUTRITION_SUR05_IRO03} from "./nutrition/sur05/iro03";
import {NUTRITION_SUR05_IRO04} from "./nutrition/sur05/iro04";
import {NUTRITION_SUR05_IRO05} from "./nutrition/sur05/iro05";
import {NUTRITION_SUR05_IRO06} from "./nutrition/sur05/iro06";
import {NUTRITION_SUR05_IRO07} from "./nutrition/sur05/iro07";
import {NUTRITION_SUR05_IRO08} from "./nutrition/sur05/iro08";
import {NUTRITION_SUR05_IRO09} from "./nutrition/sur05/iro09";
import {NUTRITION_SUR05_IRO10} from "./nutrition/sur05/iro10";
import {NUTRITION_SUR05_IRO11} from "./nutrition/sur05/iro11";
import {NUTRITION_SUR05_IRO12} from "./nutrition/sur05/iro12";
import {BABYFOOD_BOB09} from "./babyfood/bob09";
import {NUTRITION_SUR03_LAC13} from "./nutrition/sur03/lac13";
import {NUTRITION_SUR04_VIT13} from "./nutrition/sur04/vit13";
import {NUTRITION_SUR05_IRO13} from "./nutrition/sur05/iro13";
import {LAB_RECOMMEND_0} from "./lab_recommend_0";
import {LAB_RECOMMEND_1} from "./lab_recommend_1";
import {LAB_RECOMMEND_2} from "./lab_recommend_2";
import {LAB_RECOMMEND_3} from "./lab_recommend_3";
import {LAB_RECOMMEND_4} from "./lab_recommend_4";
import {LAB_RECOMMEND_5} from "./lab_recommend_5";

export const BABYFOOD_ICON = {
  BOB01: {TEXT: BABYFOOD_TEXT.BOB01},
  BOB02: {TEXT: BABYFOOD_TEXT.BOB02},
  BOB03: {TEXT: BABYFOOD_TEXT.BOB03},
  BOB04: {TEXT: BABYFOOD_TEXT.BOB04},
  BOB05: {TEXT: BABYFOOD_TEXT.BOB05},
  BOB06: {TEXT: BABYFOOD_TEXT.BOB06},
  BOB07: {TEXT: BABYFOOD_TEXT.BOB07},
  BOB08: {TEXT: BABYFOOD_TEXT.BOB08},
  BOB09: {TEXT: BABYFOOD_TEXT.BOB09},
}

export const NUTRITION_ICON = {
  LAC01: {TEXT: NUTRITION_TEXT.LAC01},
  LAC02: {TEXT: NUTRITION_TEXT.LAC02},
  LAC03: {TEXT: NUTRITION_TEXT.LAC03},
  LAC04: {TEXT: NUTRITION_TEXT.LAC04},
  LAC05: {TEXT: NUTRITION_TEXT.LAC05},
  LAC06: {TEXT: NUTRITION_TEXT.LAC06},
  LAC07: {TEXT: NUTRITION_TEXT.LAC07},
  LAC08: {TEXT: NUTRITION_TEXT.LAC08},
  LAC09: {TEXT: NUTRITION_TEXT.LAC09},
  LAC10: {TEXT: NUTRITION_TEXT.LAC10},
  LAC11: {TEXT: NUTRITION_TEXT.LAC11},
  LAC12: {TEXT: NUTRITION_TEXT.LAC12},
  LAC13: {TEXT: NUTRITION_TEXT.LAC13},
  VIT01: {TEXT: NUTRITION_TEXT.VIT01},
  VIT02: {TEXT: NUTRITION_TEXT.VIT02},
  VIT03: {TEXT: NUTRITION_TEXT.VIT03},
  VIT04: {TEXT: NUTRITION_TEXT.VIT04},
  VIT05: {TEXT: NUTRITION_TEXT.VIT05},
  VIT06: {TEXT: NUTRITION_TEXT.VIT06},
  VIT07: {TEXT: NUTRITION_TEXT.VIT07},
  VIT08: {TEXT: NUTRITION_TEXT.VIT08},
  VIT09: {TEXT: NUTRITION_TEXT.VIT09},
  VIT10: {TEXT: NUTRITION_TEXT.VIT10},
  VIT11: {TEXT: NUTRITION_TEXT.VIT11},
  VIT12: {TEXT: NUTRITION_TEXT.VIT12},
  VIT13: {TEXT: NUTRITION_TEXT.VIT13},
  IRO01: {TEXT: NUTRITION_TEXT.IRO01},
  IRO02: {TEXT: NUTRITION_TEXT.IRO02},
  IRO03: {TEXT: NUTRITION_TEXT.IRO03},
  IRO04: {TEXT: NUTRITION_TEXT.IRO04},
  IRO05: {TEXT: NUTRITION_TEXT.IRO05},
  IRO06: {TEXT: NUTRITION_TEXT.IRO06},
  IRO07: {TEXT: NUTRITION_TEXT.IRO07},
  IRO08: {TEXT: NUTRITION_TEXT.IRO08},
  IRO09: {TEXT: NUTRITION_TEXT.IRO09},
  IRO10: {TEXT: NUTRITION_TEXT.IRO10},
  IRO11: {TEXT: NUTRITION_TEXT.IRO11},
  IRO12: {TEXT: NUTRITION_TEXT.IRO12},
  IRO13: {TEXT: NUTRITION_TEXT.IRO13},
}

export const LAB_RECOMMEND_ICON = {
  ZERO: {TEXT: LAB_RECOMMEND_TEXT.ZERO},
  ONE: {TEXT: LAB_RECOMMEND_TEXT.ONE},
  TWO: {TEXT: LAB_RECOMMEND_TEXT.TWO},
  THREE: {TEXT: LAB_RECOMMEND_TEXT.THREE},
  FOUR: {TEXT: LAB_RECOMMEND_TEXT.FOUR},
  FIVE: {TEXT: LAB_RECOMMEND_TEXT.FIVE},
}

export function SvgLabIcons({type, onClick, sx}) {
  switch (type) {
    case BABYFOOD_ICON.BOB01.TEXT:
      return BABYFOOD_BOB01(onClick, sx)
    case BABYFOOD_ICON.BOB02.TEXT:
      return BABYFOOD_BOB02(onClick, sx)
    case BABYFOOD_ICON.BOB03.TEXT:
      return BABYFOOD_BOB03(onClick, sx)
    case BABYFOOD_ICON.BOB04.TEXT:
      return BABYFOOD_BOB04(onClick, sx)
    case BABYFOOD_ICON.BOB05.TEXT:
      return BABYFOOD_BOB05(onClick, sx)
    case BABYFOOD_ICON.BOB06.TEXT:
      return BABYFOOD_BOB06(onClick, sx)
    case BABYFOOD_ICON.BOB07.TEXT:
      return BABYFOOD_BOB07(onClick, sx)
    case BABYFOOD_ICON.BOB08.TEXT:
      return BABYFOOD_BOB08(onClick, sx)
    case BABYFOOD_ICON.BOB09.TEXT:
      return BABYFOOD_BOB09(onClick, sx)
    case NUTRITION_ICON.LAC01.TEXT:
      return NUTRITION_SUR03_LAC01(onClick, sx)
    case NUTRITION_ICON.LAC02.TEXT:
      return NUTRITION_SUR03_LAC02(onClick, sx)
    case NUTRITION_ICON.LAC03.TEXT:
      return NUTRITION_SUR03_LAC03(onClick, sx)
    case NUTRITION_ICON.LAC04.TEXT:
      return NUTRITION_SUR03_LAC04(onClick, sx)
    case NUTRITION_ICON.LAC05.TEXT:
      return NUTRITION_SUR03_LAC05(onClick, sx)
    case NUTRITION_ICON.LAC06.TEXT:
      return NUTRITION_SUR03_LAC06(onClick, sx)
    case NUTRITION_ICON.LAC07.TEXT:
      return NUTRITION_SUR03_LAC07(onClick, sx)
    case NUTRITION_ICON.LAC08.TEXT:
      return NUTRITION_SUR03_LAC08(onClick, sx)
    case NUTRITION_ICON.LAC09.TEXT:
      return NUTRITION_SUR03_LAC09(onClick, sx)
    case NUTRITION_ICON.LAC10.TEXT:
      return NUTRITION_SUR03_LAC10(onClick, sx)
    case NUTRITION_ICON.LAC11.TEXT:
      return NUTRITION_SUR03_LAC11(onClick, sx)
    case NUTRITION_ICON.LAC12.TEXT:
      return NUTRITION_SUR03_LAC12(onClick, sx)
    case NUTRITION_ICON.LAC13.TEXT:
      return NUTRITION_SUR03_LAC13(onClick, sx)
    case NUTRITION_ICON.VIT01.TEXT:
      return NUTRITION_SUR04_VIT01(onClick, sx)
    case NUTRITION_ICON.VIT02.TEXT:
      return NUTRITION_SUR04_VIT02(onClick, sx)
    case NUTRITION_ICON.VIT03.TEXT:
      return NUTRITION_SUR04_VIT03(onClick, sx)
    case NUTRITION_ICON.VIT04.TEXT:
      return NUTRITION_SUR04_VIT04(onClick, sx)
    case NUTRITION_ICON.VIT05.TEXT:
      return NUTRITION_SUR04_VIT05(onClick, sx)
    case NUTRITION_ICON.VIT06.TEXT:
      return NUTRITION_SUR04_VIT06(onClick, sx)
    case NUTRITION_ICON.VIT07.TEXT:
      return NUTRITION_SUR04_VIT07(onClick, sx)
    case NUTRITION_ICON.VIT08.TEXT:
      return NUTRITION_SUR04_VIT08(onClick, sx)
    case NUTRITION_ICON.VIT09.TEXT:
      return NUTRITION_SUR04_VIT09(onClick, sx)
    case NUTRITION_ICON.VIT10.TEXT:
      return NUTRITION_SUR04_VIT10(onClick, sx)
    case NUTRITION_ICON.VIT11.TEXT:
      return NUTRITION_SUR04_VIT11(onClick, sx)
    case NUTRITION_ICON.VIT12.TEXT:
      return NUTRITION_SUR04_VIT12(onClick, sx)
    case NUTRITION_ICON.VIT13.TEXT:
      return NUTRITION_SUR04_VIT13(onClick, sx)
    case NUTRITION_ICON.IRO01.TEXT:
      return NUTRITION_SUR05_IRO01(onClick, sx)
    case NUTRITION_ICON.IRO02.TEXT:
      return NUTRITION_SUR05_IRO02(onClick, sx)
    case NUTRITION_ICON.IRO03.TEXT:
      return NUTRITION_SUR05_IRO03(onClick, sx)
    case NUTRITION_ICON.IRO04.TEXT:
      return NUTRITION_SUR05_IRO04(onClick, sx)
    case NUTRITION_ICON.IRO05.TEXT:
      return NUTRITION_SUR05_IRO05(onClick, sx)
    case NUTRITION_ICON.IRO06.TEXT:
      return NUTRITION_SUR05_IRO06(onClick, sx)
    case NUTRITION_ICON.IRO07.TEXT:
      return NUTRITION_SUR05_IRO07(onClick, sx)
    case NUTRITION_ICON.IRO08.TEXT:
      return NUTRITION_SUR05_IRO08(onClick, sx)
    case NUTRITION_ICON.IRO09.TEXT:
      return NUTRITION_SUR05_IRO09(onClick, sx)
    case NUTRITION_ICON.IRO10.TEXT:
      return NUTRITION_SUR05_IRO10(onClick, sx)
    case NUTRITION_ICON.IRO11.TEXT:
      return NUTRITION_SUR05_IRO11(onClick, sx)
    case NUTRITION_ICON.IRO12.TEXT:
      return NUTRITION_SUR05_IRO12(onClick, sx)
    case NUTRITION_ICON.IRO13.TEXT:
      return NUTRITION_SUR05_IRO13(onClick, sx)
    case LAB_RECOMMEND_ICON.ZERO.TEXT:
      return LAB_RECOMMEND_0(onClick, sx)
    case LAB_RECOMMEND_ICON.ONE.TEXT:
      return LAB_RECOMMEND_1(onClick, sx)
    case LAB_RECOMMEND_ICON.TWO.TEXT:
      return LAB_RECOMMEND_2(onClick, sx)
    case LAB_RECOMMEND_ICON.THREE.TEXT:
      return LAB_RECOMMEND_3(onClick, sx)
    case LAB_RECOMMEND_ICON.FOUR.TEXT:
      return LAB_RECOMMEND_4(onClick, sx)
    case LAB_RECOMMEND_ICON.FIVE.TEXT:
      return LAB_RECOMMEND_5(onClick, sx)
  }
}
