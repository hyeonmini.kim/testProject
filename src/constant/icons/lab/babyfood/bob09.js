import Image from "../../../../components/image";
import {PATH_DONOTS_STATIC_PAGE} from "../../../../routes/paths";

export const BABYFOOD_BOB09 = (onClick, sx) => {
  return (
    <Image
      alt={''}
      src={PATH_DONOTS_STATIC_PAGE.ICONS('/images/icons/lab/babyfood/BOB09.png')}
      // src={'/assets/images/lab/babyfood/BOB09.png'}
      onClick={onClick}
      sx={{width: '80px', height: '80px', ...sx}}
    />
  )
}