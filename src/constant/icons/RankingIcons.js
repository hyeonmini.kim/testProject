import {Box, SvgIcon, Typography} from "@mui/material";
import {getNumberWithoutText} from "../../utils/formatNumber";

export function RankingIcons({rank, sx}) {

  const ranking = getNumberWithoutText(rank)

  switch (true) {
    case ranking === 0:
      return (
        <Box sx={{...sx}}>
          <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 22 22" sx={{width: '22px', height: '22px', fill: 'none', ...sx}}>
            <circle cx="11" cy="11" r="10.5" fill="#829094" stroke="white"/>
          </SvgIcon>
          <Typography aria-hidden={true} color={'white'} sx={{position: 'absolute', left: 8, top: -3, fontSize: '16px'}}>-</Typography>
        </Box>
      )
    case ranking === 1:
      return (
        <Box sx={{...sx}}>
          <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 22 22" sx={{width: '22px', height: '22px', fill: 'none'}}>
            <circle cx="11" cy="11" r="10.5" fill="#FFB800" stroke="white"/>
          </SvgIcon>
          <Typography aria-hidden={true} color={'white'} variant='body2' sx={{position: 'absolute', left: 7, top: -1}}>1</Typography>
        </Box>
      )
    case ranking === 2:
      return (
        <Box sx={{...sx}}>
          <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 22 22" sx={{width: '22px', height: '22px', fill: 'none', ...sx}}>
            <circle cx="11" cy="11" r="10.5" fill="#898989" stroke="white"/>
          </SvgIcon>
          <Typography aria-hidden={true} color={'white'} variant='body2' sx={{position: 'absolute', left: 7, top: -1}}>2</Typography>
        </Box>
      )
    case ranking === 3:
      return (
        <Box sx={{...sx}}>
          <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 22 22" sx={{width: '22px', height: '22px', fill: 'none', ...sx}}>
            <circle cx="11" cy="11" r="10.5" fill="#C59247" stroke="white"/>
          </SvgIcon>
          <Typography aria-hidden={true} color={'white'} variant='body2' sx={{position: 'absolute', left: 7, top: -1}}>3</Typography>
        </Box>
      )
    case ranking < 10:
      return (
        <Box sx={{...sx}}>
          <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 22 22" sx={{width: '22px', height: '22px', fill: 'none', ...sx}}>
            <circle cx="11" cy="11" r="10.5" fill="#829094" stroke="white"/>
          </SvgIcon>
          <Typography aria-hidden={true} color={'white'} variant='body2' sx={{position: 'absolute', left: 7, top: -1}}>{ranking}</Typography>
        </Box>
      )
    case ranking < 100:
      return (
        <Box sx={{...sx}}>
          <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 22 22" sx={{width: '22px', height: '22px', fill: 'none', ...sx}}>
            <circle cx="11" cy="11" r="10.5" fill="#829094" stroke="white"/>
          </SvgIcon>
          <Typography aria-hidden={true} color={'white'} variant='body2' sx={{position: 'absolute', left: 4, top: -1}}>{ranking}</Typography>
        </Box>
      )
    case ranking < 1000:
      return (
        <Box sx={{...sx}}>
          <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 22 22" sx={{width: '22px', height: '22px', fill: 'none', ...sx}}>
            <circle cx="11" cy="11" r="10.5" fill="#829094" stroke="white"/>
          </SvgIcon>
          <Typography aria-hidden={true} color={'white'} sx={{position: 'absolute', left: 3, top: 3, fontSize: '10px'}}>{ranking}</Typography>
        </Box>
      )
    default:
      return (
        <Box sx={{...sx}}>
          <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 22 22" sx={{width: '22px', height: '22px', fill: 'none', ...sx}}>
            <circle cx="11" cy="11" r="10.5" fill="#829094" stroke="white"/>
          </SvgIcon>
          <Typography aria-hidden={true} color={'white'} variant='body2' sx={{position: 'absolute', left: 0, top: 3, fontSize: '3px'}}>{ranking}</Typography>
        </Box>
      )
  }
}