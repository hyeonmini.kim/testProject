const date = new Date()

/* 도낫츠 이용약관 */
export const CHECK_LIST = {
  TRUE: [true, true, true, true, true, true, true, true, true, true, true, true],
  FALSE: [false, false, false, false, false, false, false, false, false, false, false, false],
}

export const AGREE_DONOTS = {
  TITLE: '도낫츠 이용약관',
  SERVICE: '서비스 이용약관',
  PERSONAL: '개인정보 수집 및 이용 동의',
  SENSITIVE: '민감정보 수집 및 이용 동의',
  MARKETING: '마케팅 등을 위한 개인정보 수집 및 이용 동의 [선택]',
  SMS: '문자',
  EMAIL: '이메일',
}

export const AGREE_PERSONAL = {
  TITLE: '본인인증 이용약관',
  PERSONAL: '개인정보 수집 및 이용 동의',
  UNIQUE: '고유식별정보 처리 동의',
  TELECOM: '통신사 이용약관 동의',
  SERVICE: '서비스 이용약관 동의',
  RESALE_PHONE: '개인정보 제 3자 제공 동의 (알뜰폰)',
}

export const AGREE_ALARM = {
  TITLE: '앱(App) 알림 수신 동의',
  MARKETING: '마케팅 알림 수신 동의 [선택]',
  AGREE_MARKETING: `${date.getMonth() + 1}월 ${date.getDate()}일 ${date.getHours()}시에 마케팅 수신을 동의하였습니다.`,
  DISAGREE_MARKETING: `${date.getMonth() + 1}월 ${date.getDate()}일 ${date.getHours()}시에 마케팅 수신을 거부하였습니다.`,
}

export const AGREE_BOTTOM = {
  ALL: '전체 약관에 동의합니다.',
  BUTTON_ALL: '약관 전체 동의',
  BUTTON_COMMON: '약관 동의',
}
