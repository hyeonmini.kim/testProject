/* ID 회원가입 입력한 회원정보 */
export const SIGN_UP_INFORM_ID = {
  isPhoneIdentityVerificationUsageAgreed: false,
  uuid: '',
  ci: '',
  name: '',
  phoneNumber: '',
  birthDay: '',
  gender: '',
  id: '',
  password: '',
  email: '',
  nickname: '',
  identityVerificationQas: [],
  babies: [],
  isTermsCollectingPersonalDataMarketingAgreed: true,
  isTextMessageReciveAgreed: true,
  isEmailReceiveAgreed: true,
  isMarketingInfoPushNotifSet: true
}

/* SNS 회원가입 입력한 회원정보 */
export const SIGN_UP_INFORM_SNS = {
  uuid: '',
  ci: '',
  name: '',
  phoneNumber: '',
  birthDay: '',
  gender: '',
  email: '',
  nickname: '',
  babies: [],
  isTermsCollectingPersonalDataMarketingAgreed: true,
  isTextMessageReciveAgreed: true,
  isEmailReceiveAgreed: true,
  isMarketingInfoPushNotifSet: true
}

/* 입력한 인증정보 */
export const AUTH_INFORM = {
  name: '',
  telecom: '',
  phone: '',
  idFront: '',
  idEnd: '',
}

/* 둘러보기 화면 아이 정보 입력 정보 */
export const INPUT_CHILD_INFO = {
  birth: '',
  height: '',
  weight: '',
  gender: '',
  allergy: undefined,
  allergyList: [],
  keyword: [],
  keywordList: [],
}