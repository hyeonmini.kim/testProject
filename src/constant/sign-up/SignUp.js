import {ALLERGY_ICON} from "../icons/AllergyIcons";
import {ALLERGY_TYPE, KEYWORD_TYPE} from "../common/HealthKeyword";

/* 메인 */
export const MAIN = {
  TITLE: '오늘도 건강하게\n우리 아이를 위한 donots',
  LOGIN: '로그인',
  DIVIDER: '|',
  SIGN_UP: '회원가입',
  GUEST: '둘러볼래요',
  PERSONAL: '개인정보처리방침',
  SERVICE: '이용약관',
  RESENT_MESSAGE: (provider) => `최근에 ${provider}으로 로그인했어요`,
}

/* 로그인 화면 */
export const LOGIN = {
  TITLE: 'ID/PW로\n로그인하기',
  AUTO: '자동 로그인하기',
  FIND_ID: '아이디 찾기',
  JOIN: '회원가입',
  AGREE: '이용약관',
  PERSONAL: '개인정보처리방침',
  DIVIDER: '|',
  RESET_PASSWORD: '비밀번호 재설정',
  ACCOUNT_NOT_FOUND: '가입된 계정이 없습니다.',
}

/* 진입 타입 */
export const FROM_TYPE = {
  SIGN_UP_ID: 'sign_up_id',
  SIGN_UP_ID_IOS: 'sign_up_id_ios',
  SIGN_UP_SNS: 'sign_up_sns',
  FIND_ID: 'find_id',
  RESET_PASSWORD: 'reset_password',
  GUEST_CHILD_PROFILE: 'guest_child_profile',
  GUEST_CHILD_ALLERGY: 'guest_child_allergy',
  GUEST_CHILD_KEYWORD: 'guest_child_keyword',
  ADDITIONAL_CHILD_INFORM: 'additional_child_inform',
  CHANGE_PASSWORD: 'change_password',
}

/* 입력정보 타이틀 */
export const SIGN_UP_TITLE = {
  PHONE: '본인인증을 위해\n휴대폰 번호를 입력해 주세요.',
  BIRTH: '생년월일을 입력해주세요',
  ID: '아이디를\n입력해주세요',
  EMAIL: '이메일을\n입력해주세요',
  EMAIL_FIND_ID: '회원가입시 사용한\n이메일 계정을 입력해주세요',
  INPUT_ANSWER_FIND_ID: '본인 확인을 위해 가입시\n입력한 답변을 입력해주세요',
  PASSWORD: '비밀번호를\n설정해주세요',
  NICKNAME: '회원님의 닉네임을\n입력해주세요',
  IDENTIFICATION: '본인 확인을 위한\n질문 2가지를 선택해주세요',
  CHILD_NICKNAME: '아이의 이름이나 닉네임을\n입력해주세요',
  CHILD_BIRTH: '아이의 생년월일\n6자리를 입력해주세요',
  CHILD_HEIGHT: '아이의 키를\n입력해주세요',
  CHILD_WEIGHT: '아이의 몸무게를\n입력해주세요',
  CHILD_GENDER: '아이의 성별을\n체크해주세요',
  CHILD_ALLERGY: '아이에게 알레르기를 유발하는\n재료를 모두 선택해주세요',
  CHILD_KEYWORD: '아이와 관련해 관심있는\n키워드를 3개까지 선택해주세요',
  RESET_PASSWORD: '회원가입시 사용한\n아이디를 입력해주세요',
}

/* 입력정보 서브 타이틀 */
export const SIGN_UP_SUBTITLE = {
  CHILD_ALLERGY: '알레르기를 유발한 음식을 알려주면\n정확한 음식 추천을 해줄 수 있어요',
  CHILD_KEYWORD: '관심사가 비슷한 부모들의\n건강 레시피를 추천받을 수 있어요',
}

/* 본인인증 섹션 */
export const SIGN_UP_PHONE = {
  LABEL_NAME: '이름을 입력해주세요.',
  LABEL_TELECOM: '통신사',
  LABEL_PHONE: '휴대폰 번호',
  LABEL_ID_FRONT_NUM: '주민번호',
  HELP_AUTH: '휴대폰 \'본인인증\'에 대해 궁금하신가요?',
}

/* 생년월일 섹션 */
export const SIGN_UP_BIRTH = {
  LABEL: '생년월일 여덟자리를 입력해주세요',
  HELPER_TEXT: '생년월일 여덟자리를 입력해주세요.',
  HELPER_TEXT_INVALID: '생년월일을 다시 한번 확인해 주세요.',
  TEMP_NAME: '도낫츠',
  TEMP_PHONE: '01000000000',
  TEMP_GENDER: '1',
  TEMP_CI: 'TEMP_CI',
}

/* 아이디 섹션 */
export const SIGN_UP_ID = {
  HELPER_TEXT: '영문소문자/숫자 6~12자리, 공란 및 특수문자 불가합니다.',
  HELPER_TEXT_DUPLICATION: '사용중인 아이디입니다. 다시 한번 확인해 주세요',
  HELPER_TEXT_NOTHING: '존재하지 않는 아이디입니다. 다시 한번 확인해 주세요',
  LABEL: '아이디를 입력해주세요',
}

/* 비밀번호 섹션 */
export const SIGN_UP_PASSWORD = {
  HELPER_TEXT: '영문, 숫자, 특수문자 포함 8~15자로 입력해주세요.',
  LABEL_INPUT: '비밀번호를 입력해주세요',
  LABEL_CONFIRM: '비밀번호를 확인해주세요',
  WRONG: '비밀번호가 일치하지 않습니다.',
  INVALID: '비밀번호에 개인정보가 포함되어 있습니다.',
}

/* 이메일 섹션 */
export const SIGN_UP_EMAIL = {
  HELPER_TEXT: '이메일은 ID/PW 분실시 본인 확인을 위한 용도로 사용됩니다.',
  HELPER_TEXT_WRONG: '이메일을 정확하게 입력헤주세요',
  HELPER_TEXT_DUPLICATION: '사용중인 이메일입니다. 다시 한번 확인해 주세요',
  LABEL: 'donots@donots.co.kr',
  LABEL_FIND_ID: 'donots@donots.co.kr',
  LABEL_INPUT_ANSWER: '답변을 입력해주세요',
  COLLECT_PURPOSE_NOTICE: '이메일은 하기와 같은 목적으로 수집합니다.',
  COLLECT_PURPOSE_1: '서비스 이용 관련 공지사항 안내',
  COLLECT_PURPOSE_2: '회원정보 및 계정 관련 고객안내',
  COLLECT_PURPOSE_3: '정책 변경 등 주요사항 안내 등',
}

/* 닉네임 섹션 */
export const SIGN_UP_NICKNAME = {
  HELPER_TEXT: '영문, 한글, 숫자 포함 10자 이내, 공란 및 특수문자 불가합니다.',
  HELPER_TEXT_DUPLICATION: '사용중인 닉네임입니다. 다시 한번 확인해 주세요',
  LABEL: '회원님의 닉네임을 입력해주세요',
}

/* 본인 확인 질문 섹션 */
export const SIGN_UP_IDENTIFICATION = {
  HELPER_TEXT: '한글/영문/숫자만 가능하며, 최대 10자리까지 입력 가능합니다.',
  HELPER_TEXT_INVALID: '한글/영문/숫자만 가능합니다.',
  LABEL_QUESTION: '질문을 선택해주세요',
  LABEL_ANSWER: '질문의 답을 입력해주세요',
  BOTTOM_DIALOG_TITLE: '질문선택',
  FIRST_QUESTION: '첫번째질문',
  SECOND_QUESTION: '두번째질문',
  QUESTION: [
    {code: 'FAVORITE_FOOD', text: '내가 가장 좋아하는 음식은?'},
    {code: 'FAVORITE_SONG_TITLE', text: '내가 가장 좋아하는 노래 제목은?'},
    {code: 'FAVORITE_CHARACTER', text: '내가 가장 좋아하는 캐릭터는?'},
    {code: 'FAVORITE_COLOR', text: '내가 가장 좋아하는 색깔은?'},
    {code: 'CLOSEST_FRIEND_NAME_IN_ELEMENTARY_SCHOOL', text: '초등학교 때 가장 친했던 친구 이름은?'},
    {code: 'DESIRED_DATE_TO_REMEMBER', text: '기억하고 싶은 날짜가 있다면?'},
    {code: 'PET_NAME', text: '나의 반려동물 이름은?'},
    {code: 'CHILDHOOD_NICKNAME', text: '나의 어릴적 별명은?'},
    {code: 'MOST_DESIRED_COUNTRY_TO_TRAVEL', text: '가장 여행하고 싶은 나라는?'},
    {code: 'MOST_MEMORABLE_TEACHER', text: '가장 기억에 남는 선생님 성함은?'},
  ],
  POPUP_MESSAGE: '선택한 질문과 답변을\n다시 한번 확인해주세요',
  POPUP_OK: '확인',
  POPUP_CANCEL: '취소',
}

/* 아이 닉네임 섹션 */
export const SIGN_UP_CHILD_NICKNAME = {
  HELPER_TEXT: '영문, 한글, 숫자 포함 10자 이내, 공란 및 특수문자 불가합니다.',
  LABEL: '아이의 이름이나 닉네임을 입력해주세요',
}

/* 아이 생년월일 섹션 */
export const SIGN_UP_CHILD_BIRTH = {
  LABEL: '생년월일 여섯자리를 입력해주세요',
  HELPER_TEXT: '생년월일 여섯자리를 입력해주세요.',
  HELPER_TEXT_EXCESS: '아이가 태어나면 등록해주세요.',
}

/* 아이 키 BMI(체질량지수, body mass index) 값 */
export const CHILD_HEIGHT = {
  MAX: 174.4,
  MIN: 44.8,
}

/* 아이 키 섹션 */
export const SIGN_UP_CHILD_HEIGHT = {
  LABEL: '소수점 첫째 자리까지 입력해주세요',
  HELPER_TXT: '소수점 첫째 자리까지 입력하시면 정확한 정보를 받을 수 있어요',
  HELPER_TXT_MAX_HEIGHT: `최대 키(${CHILD_HEIGHT.MAX}CM)를 초과하여 입력하였습니다.`,
  HELPER_TXT_MIN_HEIGHT: `입력가능한 최소 키(${CHILD_HEIGHT.MIN}CM) 미만으로 입력하였습니다.`,
}

/* 아이 몸무게 BMI(체질량지수, body mass index) 값 */
export const CHILD_WEIGHT = {
  MAX: 77.7,
  MIN: 2.3,
}

/* 아이 몸무게 섹션 */
export const SIGN_UP_CHILD_WEIGHT = {
  LABEL: '소수점 첫째 자리까지 입력해주세요',
  HELPER_TXT: '소수점 첫째 자리까지 입력하시면 정확한 정보를 받을 수 있어요',
  HELPER_TXT_MAX_WEIGHT: `최대 몸무게(${CHILD_WEIGHT.MAX}kg)를 초과하여 입력하였습니다.`,
  HELPER_TXT_MIN_WEIGHT: `입력가능한 최소 몸무게(${CHILD_WEIGHT.MIN}kg) 미만으로 입력하였습니다.`,
}

/* 아이 성별 섹션 */
export const SIGN_UP_CHILD_GENDER = {
  BOY: '남아',
  GIRL: '여아',
}

/* 아이 알러지 섹션 */
export const ALLERGY_LIST = [
  {alg_name: ALLERGY_TYPE.NONE, alg_key: '', value: true, image: ALLERGY_ICON.ALLERGY_NONE},
  {alg_name: ALLERGY_TYPE.MILK, alg_key: 'ALG000001', value: false, image: ALLERGY_ICON.ALLERGY_MILK},
  {alg_name: ALLERGY_TYPE.EGG, alg_key: 'ALG000002', value: false, image: ALLERGY_ICON.ALLERGY_EGG},
  {alg_name: ALLERGY_TYPE.WHEAT, alg_key: 'ALG000003', value: false, image: ALLERGY_ICON.ALLERGY_WHEAT},
  {alg_name: ALLERGY_TYPE.BUCKWHEAT, alg_key: 'ALG000004', value: false, image: ALLERGY_ICON.ALLERGY_BUCKWHEAT},
  {alg_name: ALLERGY_TYPE.SOYBEAN, alg_key: 'ALG000005', value: false, image: ALLERGY_ICON.ALLERGY_SOYBEAN},
  {alg_name: ALLERGY_TYPE.CRAB, alg_key: 'ALG000006', value: false, image: ALLERGY_ICON.ALLERGY_CRAB},
  {alg_name: ALLERGY_TYPE.SHRIMP, alg_key: 'ALG000007', value: false, image: ALLERGY_ICON.ALLERGY_SHRIMP},
  {alg_name: ALLERGY_TYPE.SQUID, alg_key: 'ALG000008', value: false, image: ALLERGY_ICON.ALLERGY_SQUID},
  {alg_name: ALLERGY_TYPE.MACKEREL, alg_key: 'ALG000009', value: false, image: ALLERGY_ICON.ALLERGY_MACKEREL},
  {alg_name: ALLERGY_TYPE.SHELLFISH, alg_key: 'ALG000010', value: false, image: ALLERGY_ICON.ALLERGY_SHELLFISH},
  {alg_name: ALLERGY_TYPE.PEANUT, alg_key: 'ALG000011', value: false, image: ALLERGY_ICON.ALLERGY_PEANUT},
  {alg_name: ALLERGY_TYPE.PINE_NUT, alg_key: 'ALG000012', value: false, image: ALLERGY_ICON.ALLERGY_PINE_NUT},
  {alg_name: ALLERGY_TYPE.WALNUT, alg_key: 'ALG000013', value: false, image: ALLERGY_ICON.ALLERGY_WALNUT},
  {alg_name: ALLERGY_TYPE.PORK, alg_key: 'ALG000014', value: false, image: ALLERGY_ICON.ALLERGY_PORK},
  {alg_name: ALLERGY_TYPE.PEACH, alg_key: 'ALG000015', value: false, image: ALLERGY_ICON.ALLERGY_PEACH},
  {alg_name: ALLERGY_TYPE.TOMATO, alg_key: 'ALG000016', value: false, image: ALLERGY_ICON.ALLERGY_TOMATO},
  {alg_name: ALLERGY_TYPE.CHICKEN, alg_key: 'ALG000017', value: false, image: ALLERGY_ICON.ALLERGY_CHICKEN},
  {alg_name: ALLERGY_TYPE.BEEF, alg_key: 'ALG000018', value: false, image: ALLERGY_ICON.ALLERGY_BEEF},
]

/* 아이 관심 키워드 섹션 */
export const HEALTH_KEYWORD = [
  {health_name: KEYWORD_TYPE.COLD, health_key: 'HINT000008', clicked: false},
  {health_name: KEYWORD_TYPE.CONSTIPATION, health_key: 'HINT000010', clicked: false},
  {health_name: KEYWORD_TYPE.LACTOSE, health_key: 'HINT000011', clicked: false},
  {health_name: KEYWORD_TYPE.IMBALANCED, health_key: 'HINT000009', clicked: false},
  {health_name: KEYWORD_TYPE.SKIN, health_key: 'HINT000004', clicked: false},
  {health_name: KEYWORD_TYPE.MUSCLE, health_key: 'HINT000005', clicked: false},
  {health_name: KEYWORD_TYPE.BONE, health_key: 'HINT000006', clicked: false},
  {health_name: KEYWORD_TYPE.ANEMIA, health_key: 'HINT000007', clicked: false},
  {health_name: KEYWORD_TYPE.MOUTH, health_key: 'HINT000002', clicked: false},
  {health_name: KEYWORD_TYPE.GUMS, health_key: 'HINT000003', clicked: false},
  {health_name: KEYWORD_TYPE.EYEBALL, health_key: 'HINT000001', clicked: false},
]

/* 비밀번호 재설정 섹션 */
export const RESET_PASSWORD = {
  HELPER_TEXT: '영문소문자/숫자 6~12자리, 공란 및 특수문자 불가합니다.',
  LABEL: '아이디를 입력해주세요',
}

/* 인증번호 입력 섹션 */
export const AUTH_CODE = {
  TITLE: '인증번호 입력',
  RETRY: '인증번호 다시 요청하기',
  TOAST_INCORRECT: '인증번호가 일치하지 않습니다. 다시 확인해 주세요.',
  TOAST_FIVE_TIMES: '인증번호 입력 횟수를 초과했습니(5회).\n인증번호를 다시 요청해 주세요.',
  TOAST_TIMEOUT: '입력 시간을 초과했습니다.\n인증번호를 다시 요청해 주세요.',
  TOAST_RETRY: '인증번호를 다시 요청했어요.',
}

/* 결과 화면 진입 타입 */
export const CONFIRM_TYPE = {
  CORRECT: 'correct',
  INCORRECT: 'incorrect',
  REJECT: 'reject',
  SUCCESS: 'success',
  ADD_CHILDREN: 'addChildren',
}

/* 결과 확인 화면 */
export const CONFIRM_SIGN_UP = {
  IMAGE_CHECK: '/assets/images/common/common_check.svg',
  IMAGE_NO: '/assets/images/common/common_no.svg',
  CORRECT: {title: '이미 가입된 회원입니다.', detail: 'anns1231\n가입일자 20.12.11'},
  INCORRECT: {title: '아쉽지만 일치하는 회원정보가 없어요', detail: '지금 도낫츠에 가입하시면\n내 아이에게 딱 맞는 레시피를 만날 수 있어요!'},
  REJECT: '14세 미만의 회원님은\n다음에 만나요',
  SUCCESS: {title: '축하합니다!\n가입이 완료되었어요', detail: '도낫츠에서 다양한 혜택을 누려보세요!'},
  ADD_CHILDREN: '아이정보 추가가\n완료되었어요!',
}

/* 비밀번호 변경하기 화면 */
export const CHANGE_PASSWORD = {
  TITLE: '비밀번호 변경',
  CURRENT: '현재 비밀번호',
  NEW: '새 비밀번호',
  NEW_CONFIRM: '새 비밀번호 다시 입력',
  HELPER_TEXT: '영문, 숫자, 특수문자 포함 8~15자로 입력해주세요.',
  INVALID: '생년월일 또는 휴대폰번호가 포함되었습니다.',
  WRONG: '비밀번호가 일치하지 않습니다.',
}

/* 회원가입 에러 메시지 */
export const SIGN_UP_DIALOG = {
  DUPLICATE_ID: '사용중인 아이디입니다\n다시 한번 확인해 주세요',
  DUPLICATE_EMAIL: '사용중인 이메일입니다\n다시 한번 확인해 주세요',
  DUPLICATE_NICKNAME: '사용중인 닉네임입니다\n다시 한번 확인해 주세요',
  INCORRECT_INFORM: '아이디 정보가 일치하지 않습니다\n다시 한번 확인해 주세요',
  CONFIRM: '확인',
}

/* 성별 구분 */
export const GENDER = {
  male: 'MALE',
  female: 'FEMALE',
}

/* SNS 계정 연동 */
export const CONNECT_SNS = {
  FAIL: 'SNS 계정 연결에 실패했습니다. 다시 시도해 주세요',
}

/* 토큰 리턴값 */
export const NO_TOKEN_MSG = '저장된 값이 없습니다.'

/* 본인인증 타입 */
export const CERT_TYPE = {
  OWN_ACCOUNT: 'OWN_ACCOUNT',
  SOCIAL_ACCOUNT: 'SOCIAL_ACCOUNT',
}

export const FIND_ID_QUESTIONS_CODE_LIST = {
  FAVORITE_FOOD: 'FAVORITE_FOOD',
  FAVORITE_SONG_TITLE: 'FAVORITE_SONG_TITLE',
  FAVORITE_CHARACTER: 'FAVORITE_CHARACTER',
  FAVORITE_COLOR: 'FAVORITE_COLOR',
  CLOSEST_FRIEND_NAME_IN_ELEMENTARY_SCHOOL: 'CLOSEST_FRIEND_NAME_IN_ELEMENTARY_SCHOOL',
  DESIRED_DATE_TO_REMEMBER: 'DESIRED_DATE_TO_REMEMBER',
  PET_NAME: 'PET_NAME',
  CHILDHOOD_NICKNAME: 'CHILDHOOD_NICKNAME',
  MOST_DESIRED_COUNTRY_TO_TRAVEL: 'MOST_DESIRED_COUNTRY_TO_TRAVEL',
  MOST_MEMORABLE_TEACHER: 'MOST_MEMORABLE_TEACHER',
}

export const FIND_ID_QUESTIONS = [
  {code: FIND_ID_QUESTIONS_CODE_LIST.FAVORITE_FOOD, question: '내가 가장 좋아하는 음식은?'},
  {code: FIND_ID_QUESTIONS_CODE_LIST.FAVORITE_SONG_TITLE, question: '내가 가장 좋아하는 노래 제목은?'},
  {code: FIND_ID_QUESTIONS_CODE_LIST.FAVORITE_CHARACTER, question: '내가 가장 좋아하는 캐릭터는?'},
  {code: FIND_ID_QUESTIONS_CODE_LIST.FAVORITE_COLOR, question: '내가 가장 좋아하는 색깔은?'},
  {code: FIND_ID_QUESTIONS_CODE_LIST.CLOSEST_FRIEND_NAME_IN_ELEMENTARY_SCHOOL, question: '초등학교 때 가장 친했던 친구 이름은?'},
  {code: FIND_ID_QUESTIONS_CODE_LIST.DESIRED_DATE_TO_REMEMBER, question: '기억하고 싶은 날짜가 있다면?'},
  {code: FIND_ID_QUESTIONS_CODE_LIST.PET_NAME, question: '나의 반려동물 이름은?'},
  {code: FIND_ID_QUESTIONS_CODE_LIST.CHILDHOOD_NICKNAME, question: '나의 어릴적 별명은?'},
  {code: FIND_ID_QUESTIONS_CODE_LIST.MOST_DESIRED_COUNTRY_TO_TRAVEL, question: '가장 여행하고 싶은 나라는?'},
  {code: FIND_ID_QUESTIONS_CODE_LIST.MOST_MEMORABLE_TEACHER, question: '가장 기억에 남는 선생님 성함은?'},
]

export const GET_FIND_ID_QUESTIONS_TEXT = (code) => {
  let text = ''
  FIND_ID_QUESTIONS.forEach((item) => {
    if (item?.code === code) {
      text = item?.question
      return
    }
  })
  return text
}

export const GET_FIND_ID_QUESTIONS_CODE = (question) => {
  let code = ''
  FIND_ID_QUESTIONS.forEach((item) => {
    if (item?.question === question) {
      code = item?.code
      return
    }
  })
  return code
}