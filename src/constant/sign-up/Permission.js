/* 도낫츠 접근권한 */
export const PERMISSION_DONOTS = {
  TITLE: '원활한 앱 사용을 위해\n접근 권한을 허용해 주세요.',
  SUBTITLE: '서비스 이용 시 필요한 접근 권한을 알려 드립니다.',
  PERMISSION: '선택적 접근권한',
  CAMERA: '카메라',
  CAMERA_DESC: '프로필, 레시피 작성 시 파일 접근',
  STORAGE: '저장공간',
  STORAGE_DESC: '프로필, 레시피 작성 시 파일 접근',
  CONTACT: '연락처',
  CONTACT_DESC: '레시피 공유',
  DESC: '선택적 접근 권한을 허용하지 않아도 서비스를 이용하실 수 있습니다. 다만, 일부 서비스 이용은 제한될 수 있어요.',
  CONFIRM: '확인'
}