export const MOMSTAR_COMMON = {
  DEFAULT: {name: '', intro: '', avatar: '', cnt: {hit: 0, scrap: 0, recipe: 0}},
  MOMSTAR: {
    TAB: '크런치킹',
    TITLE: '오늘의 스프링클도낫츠'
  },
  EXPERT: {
    TAB: '전문가',
    TITLE: '오늘의 전문가'
  },
}

export const MOMSTAR_TABS = [
  MOMSTAR_COMMON.MOMSTAR.TAB,
  MOMSTAR_COMMON.EXPERT.TAB,
]