export const MATERIAL_TYPE = {
  BASIC: '기본재료',
  SEASONING: '양념재료',
}

export const MATERIAL_USED = {
  BASIC: ['소고기', '돼지고기', '닭고기', '두부', '달걀', '새우', '오징어', '감자', '당근', '버섯', '토마토'],
  SEASONING: ['소금', '설탕', '깨', '간장', '고추장', '된장', '케찹', '식용유', '참기름', '들기름']
}

export const MATERIAL_BASE_TYPE = {
  SOY: '간장 베이스',
  PEPPER: '고추장 베이스',
}

export const MATERIAL_BASE = {
  SOY: [
    {
      material: '아기간장',
      quantity: 1,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '설탕',
      quantity: 1,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {material: '마늘', quantity: 1, unit: '쪽', unitValue: 1, units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '쪽', value: 1}]},
  ],
  PEPPER: [
    {
      material: '고추장',
      quantity: 1,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {material: '양파', quantity: 1, unit: '개', unitValue: 1, units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '개', value: 1}]},
    {material: '마늘', quantity: 1, unit: '쪽', unitValue: 1, units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '쪽', value: 1}]},
  ],
}

export const MATERIAL_SEARCHED = [
  {
    material: '(국내산 1 등급)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(국내산 2 등급)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(국내산 3 등급)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(중국산 A 등급)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(중국산 B 등급)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(중국산 C 등급)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(미국산)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(말레이시아산)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(호주산)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(일본산)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(대만산)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(홍콩산)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(카타르산)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
  {
    material: '(이라크산)',
    quantity: 100,
    unit: 'g',
    unitValue: 100,
    units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '근', value: 1}]
  },
]