//--- 공통 ---//

export const RECIPE_CREATE_ACTIVE_STEP = {
  BASIC: 0,
  ADDITIONAL: 1,
  MATERIAL: 2,
  STEP: 3,
}

export const RECIPE_CREATE_COMMON = {
  STEPS: ['기본정보', '부가정보', '재료', '요리순서'],
  TEXT: {
    TITLE: '레시피 작성',
    OPTION: '(선택)',
    PREVIEW: '작성내용 미리보기',
    EXAMINATION: '검수중',
    ALERT: '작성 내용 중 수정 해야 할 사항이 있어요. ',
    RETURN: '수정 요청사항',
    INPUT_CONFIRM: (step) => {
      return `미입력된 단계가 있어요.\n ( ${step} )`
    },
    CLOSE_CONFIRM: '정말 나가시겠습니까?',
    CLOSE_SAVE_CONFIRM: '저장 후 나가시겠습니까?',
    CONTINUE_CONFIRM: '작성중인 레시피가 있어요\n이어서 작성하시겠어요?',
  },
  BUTTON: {
    CONFIRM: '확인',
    NEXT: '다음',
    NEW: '새로 작성하기',
    CONTINUE: '이어서 작성하기',
    CANCEL: '취소',
    CLOSE: '나가기',
    SAVE: '저장하기',
    PREVIEW: '작성내용 미리보기',
    MODIFY: '수정하기',
    UPLOAD: '업로드하기',
  },
  TOAST: {
    TEMPORARY: '임시 저장 되었습니다.',
    COMPLETE: '레시피가 업로드 되었습니다.',
    IMAGE_MAX: '사진은 5장까지 등록 가능합니다.'
  }
}

//--- 기본정보 ---//
export const RECIPE_CREATE_BASIC = {
  TEXT: {
    REPRESENTATIVE: '대표사진',
    NAME_PLACEHOLDER: '요리명을 입력해주세요 (ex. 소고기 야채죽)',
    DESC_PLACEHOLDER: '설명을 입력해주세요 (ex. 정성 가득 한그릇 특식)',
    CATEGORY_GROUP: '카테고리'
  },
  CATEGORY_GROUP: [
    {title: '밥/죽', value: '밥/죽'},
    {title: '면요리', value: '면요리'},
    {title: '국/찌개', value: '국/찌개'},
    {title: '반찬', value: '반찬'},
    {title: '한그릇', value: '한그릇'},
    {title: '특식', value: '특식'},
    {title: '간식', value: '간식'},
    {title: '베이킹', value: '베이킹'},
    {title: '음료', value: '음료'},
  ]
}

export const RECIPE_IMAGE_TYPE = {
  RECIPE: 'recipe',
  ORDER: 'order',
}

//--- 부가정보 ---//
export const RECIPE_CREATE_ADDITIONAL = {
  TEXT: {
    COOK_TIME: '소요시간',
    DIFFICULTY: '난이도',
    MONTHLY_AGE: '월령',
    HEALTH_NOTE: (nickname) => `${nickname ? nickname : '꼬미엄마'}\'s 건강노트`,
    HEALTH_NOTE_PLACEHOLDER: '공유할 건강 노하우가 있으면 입력해주세요\n\n' +
      '(ex. 기침을 많이 하던 꼬미에게 먹였더니 도움이 됐어요. 비타민C가 풍부한 재료를 활용하면 면역력을 높여 감기를 예방할 수 있어요!)',
  },
  COOK_TIME_GROUP: [
    {title: '30분 이내', value: '30분 이내'},
    {title: '1시간 이내', value: '1시간 이내'},
    {title: '1시간 이상', value: '1시간 이상'},
  ],
  DIFFICULTY_GROUP: [
    {title: '쉬워요', value: '쉬워요'},
    {title: '할만해요', value: '할만해요'},
    {title: '좀 어려워요', value: '좀 어려워요'},
  ],
  MONTHLY_AGE_GROUP: [
    {title: '초기 이유식', subTitle: '4~6개월', value: '초기이유식'},
    {title: '중기 이유식', subTitle: '7~9개월', value: '중기이유식'},
    {title: '후기 이유식', subTitle: '10~12개월', value: '후기이유식'},
    {title: '전기 유아식', subTitle: '13~36개월', value: '전기유아식'},
    {title: '후기 유아식', subTitle: '37~60개월', value: '후기유아식'}
  ],
}

export const REPLACE_RECIPE_AGE_GROUP = (age) => {
  let text = ''
  RECIPE_CREATE_ADDITIONAL.MONTHLY_AGE_GROUP.forEach((item) => {
    if (item.value === age) {
      text = item.title
      return
    }
  })
  return text
}

//--- 재료 ---//
export const RECIPE_CREATE_MATERIAL = {
  TITLE: {
    EDIT: '편집하기',
    INPUT: '재료 입력하기',
    MEASUREMENT_UNIT: '계량단위보기',
  },
  TEXT: {
    REPRESENTATIVE: '대표',
    MEASUREMENT: '계량기준',
    UNIT: '회분',
    BASIC: '기본재료',
    SEASONING: '양념재료',
    FREQUENTLY_USED: '자주 사용하는 재료',
    SEARCH: '재료를 검색해보세요',
    HINT_TITLE: '주요 양념 계량 단위',
    HINT_DESC: '1스푼(큰술) : 15ml ・1꼬집(약간) : 2g ・조금 : 4~6g',
  },
  BUTTON: {
    MATERIAL_ADD: '재료 입력하기',
    MEASUREMENT_UNIT: '계량단위보기',
    CLEAR: '초기화',
    SOY_BASE: '간장 베이스',
    PEPPER_BASE: '고추장 베이스',
  },
}

//--- 요리순서 ---//
export const RECIPE_CREATE_STEP = {
  DEFAULT: {
    recipe_key: '',
    recipe_order_key: '',
    recipe_order: 0,
    recipe_order_desc: '',
    image_file_key: '',
    image_file_path: '',
  },
  STEP: (step) => {
    return `STEP ${step}`
  },
  TEXT: {
    STEP_PLACEHOLDER:
      '요리 내용을 입력해주세요\n' +
      'ex. 닭고기를 10분간 밑간을 해줍니다.',
    STEP_MAX_TOAST: 'STEP은 20개까지 등록가능합니다.',
    TAG: '태그',
    OPTION: '(선택)',
    TAG_PLACEHOLDER: 'ex. 변비에좋은식이섬유풍부,칼슘풍부',
  },
  BUTTON: {
    STEP_ADD: 'STEP 추가하기',
  }
}

//--- 계량단위보기 ---//
export const RECIPE_CREATE_MEASUREMENT_UNITS = [
  {
    title:
      '1스푼\n' +
      '(1큰술)',
    desc:
      '15ml\n' +
      '(=3작은술)'
  },
  {
    title:
      '1티스푼\n' +
      '(1작은술)',
    desc:
      '5ml'
  },
  {
    title:
      '1컵',
    desc:
      '200ml'
  },
  {
    title:
      '1종이컵',
    desc:
      '180ml'
  },
  {
    title:
      '고기 1근',
    desc:
      '600g'
  },
  {
    title:
      '채소 1근',
    desc:
      '400g'
  },
  {
    title:
      '채소 1단',
    desc:
      '500g'
  },
  {
    title:
      '1줌',
    desc:
      '한손 가득 쥐어진 정도'
  },
  {
    title:
      '생선 1토막',
    desc:
      '생선 약 1/3분량'
  },
  {
    title:
      '1토막',
    desc:
      '2~3cm 두께 분량의 분량'
  },
  {
    title:
      '두부 1모',
    desc:
      '300g'
  },
  {
    title:
      '1꼬집 (약간)',
    desc:
      '2g'
  },
  {
    title:
      '조금',
    desc:
      '4~6g'
  },
]