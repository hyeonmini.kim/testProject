export function getCookDifficulty(difficulty) {
  let difficultyString = "완료기 이유식";
  if (difficulty >= 80)
    difficultyString = "초기 이유식";
  else if (difficulty >= 60)
    difficultyString = "중기 이유식";
  else if (difficulty >= 40)
    difficultyString = "후기 이유식";
  else if (difficulty >= 20)
    difficultyString = "완료기 이유식";
  else
    difficultyString = "유아식";

  return difficultyString;
}

export const health_note_text_short = "단백질도 풍부하고 야채도 함께 먹일 수 있어 좋아요. 저처럼 아이 성장이 느려 걱정이라면 이 레시피 한번 활용해보세요!";
export const health_note_text_long = "단백질도 풍부하고 야채도 함께 먹일 수 있어 좋아요. 저처럼 아이 성장이 느려 걱정이라면 이 레시피 한번 활용해보세요.어떤 점에서는 좋다고 생각이 되요.어떤 점에서는 좋다고 생각이 되요";

export const temp_time_text = "분이내";

export const temp_popup_text = {
  age: "12개월~",
  desc: "클로스트리디움 보툴리늄균 오염으로 발생하는 ‘영아 보툴리누스증’은 1세 미만의 영아에게 신경마비 증상 등을 일으킬 수 있으며, 경우에 따라서는 치명적인 위해 사고를 초래할 수 있다.",
}
