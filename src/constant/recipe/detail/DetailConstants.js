export const TABS = [
  {
    value: 'nutrients',
    label: '영양 정보',
  },
  {
    value: 'howToMake',
    label: '만드는법',
  },
  {
    value: 'review',
    label: '리뷰',
  },
];

export const TABS_NUTRIENTS = [
  {
    value: 'macronutrient',
    label: '다량영양소',
  },
  {
    value: 'minerals',
    label: '무기질',
  },
  {
    value: 'waterVitamins',
    label: '수용성비타민',
  },
  {
    value: 'fatVitamins',
    label: '지용성비타민',
  },
];

export const SNACKBAR_MESSAGE = {
  SCRAP_OFF: `스크랩을 취소하였습니다`,
  SCRAP_ON: `마이레시피에 스크랩 되었습니다`,
  SHARE: `공유하였습니다`,
  REVIEW: `리뷰가 등록되었습니다`,
  RESET_PASSWORD: '비밀번호가 재설정되었습니다',
};

export const HEALTH_NOTE_TEXT = "'s 건강 노트";

export const BUTTON_TEXT = {
  SCRAP: "스크랩하기",
  FOLD: "접기",
}

export const CAUTION_MATERIAL = {
  TITLE: "주의해야 할 재료가 있어요",
  AVOID_INGREDIENTS: "섭취 자제 재료",
  ALLERGENS: "알레르기 유발 재료",
  NUTRIENTS: "영양소 특성",
  ALLERGY: " 알레르기",
}

export const VIEW_NUTRIENTS_SOURCE = "영양정보 출처";
export const VIEW_NUTRIENTS_PROPERTIES = "영양소 특성 보기";

export const COUNT_TITLE = {
  VIEWS: "조회수",
  SCRAP: "스크랩",
  RECIPE: "레시피",
}

export const NUTRIENTS_POPUP_MACRONUTRIENT_TAB_LIST = [
  {
    ingredient_name: "* 에너지(kcal)",
    ingredient_desc: "우리 몸의 모든 세포는 우리가 섭취한 식품으로부터 대사된 에너지를 필요로 합니다. 식품으로 섭취한 영양소 중 탄수화물, 단백질, 지방만 에너지를 낼 수 있고, 나머지 비타민이나 무기질은 이들 영양소가 에너지를 낼 수 있도록 대사되는데 도움을 주는 영양소입니다. 에너지 섭취가 소비보다 많을 경우 비만이, 적을 경우 저체중이 우려되니 주의가 필요해요.",
  },
  {
    ingredient_name: "탄수화물",
    ingredient_desc: "탄수화물은 1g당 4kcal를 제공하는 에너지원으로 체내에서 사용됩니다. 특히 뇌세포와 신경세포, 적혈구는 포도당을 에너지원으로 사용하므로, 탄수화물 섭취는 생명 유지에 필수적입니다. 또한 인슐린 분비와 식후 혈당 수준에 영향을 주는 주요 영양소이기도 해요. 한국인 영양소 섭취 기준에 따르면, 1-5세 성장기 유아의 기준, 하루 100g의 탄수화물 섭취를 권장하고 있으니 참고하세요.",
  },
  {
    ingredient_name: "단백질",
    ingredient_desc: "단백질은 1g당 4kcal를 제공하는 에너지원으로, 체내 합성이 가능하여 식품을 통해 섭취하지 않아도 되는 비필수아미노산과 체내 합성이 불가능하거나 합성을 하여도 그 양이 너무 적어 꼭 식품을 통해 섭취해야 하는 필수 아미노산으로 분류되므로, 필수 아미노산 섭취에 신경써야 합니다. 단백질은 체내에서 다양한 역할을 하는데, 새로운 조직 합성과 보수, 효소와 호르몬 합성, 신경전달물질 합성, 항체 형성, 수분 조절, 산/염기 평형 조절, 에너지 공급 등이 있어요.",
  },
  {
    ingredient_name: "식이섬유",
    ingredient_desc: "식이섬유는 탄수화물의 한 종류로 우리 몸에서 소화하지 못하는 특징을 가지고 있어요. 소화가 되지 않다보니 배변량을 증가시키고, 대변이 대장에서 통과되는 시간을 단축함으로써 배변을 촉진하는 역할을 합니다. 위나 장의 음식물 통과 시간을 느리게 하여 포만감을 유지시키고, 신체 내에서는 포도당이나 콜레스테롤 등의 영양소 흡수가 천천히 되거나 억제되도록 도와줍니다.",
  },
  {
    ingredient_name: "지방",
    ingredient_desc: "중성지방은 1g당 9kcal의 열량을 제공하는 에너지원으로 농축에너지원이라 합니다. 적은 양의 식사로도 많은 에너지를 제공하므로, 효율적 에너지원이나 과잉 섭취 시 비만이 될 수 있으니 주의해야 합니다. 그 외에도 지용성 비타민이 체내로 흡수되도록 도와주고, 장기를 충격으로부터 보호해주며, 체온 조절의 역할을 합니다. 인지질과 콜레스테롤은 세포막의 구성성분으로 작용하고, 특히 콜레스테롤은 담즙산, 비타민D 전구체, 스테로이드 호르몬 등을 합성하는데에도 사용됩니다.",
  },
  {
    ingredient_name: "수분",
    ingredient_desc: "성인을 기준으로 우리 몸의 약 60%는 수분으로 구성되어 있습니다. 이처럼 수분은 체조직의 가장 많은 부분을 차지하는 구성성분으로 체내 영양소와 노폐물의 이동통로이고, 각 조직에 필요한 영양소를 녹여주는 역할을 합니다. 그 외에도 체온 조절 및 음식의 소화를 돕고, 내장 기관을 외부 충격으로부터 보호하며, 체내 전해질 균형을 맞춰줍니다.",
  },
  {
    ingredient_name: "당류",
    ingredient_desc: "당은 탄수화물의 기본 단위인 단당류, 단당류 2개가 결합된 이당류, 단당류 3-10개가 결합된 올리고당류, 10개 이상이 결합된 다당류로 분류됩니다. 이 가운데 단당류나 이당류 즉, 당이 적게 결합된 이들을 섭취하게 되면 혈당을 빠르게 상승시키는 작용이 있어요. 세계보건기구(WHO)의 당 섭취 기준에 따르면 총 에너지 섭취량의 10% 이내 섭취를 권고하고 있어요.",
  },
];

export const NUTRIENTS_POPUP_MINERALS_TAB_LIST = [
  {
    ingredient_name: "칼슘",
    ingredient_desc: "칼슘은 대부분 몸에서 뼈와 치아를 만드는 주성분으로 작용합니다. 치아에 축적된 칼슘은 대체되지 않아 칼슘 섭취와 더불어 관리가 매우 중요합니다. 칼슘은 Ca2+ 형태로 체액을 돌아다니며 신체조절작용을 하고, 영양소 이동에 관여하며, 신경전달물질 분비를 촉진시키는 역할을 합니다. 근육 수축에도 칼슘이 사용되어 골격근 뿐 아니라 심근 수축에도 칼슘이 부족하면 근육 강직이 나타날 수 있으니 주의해야 합니다.",
  },
  {
    ingredient_name: "철",
    ingredient_desc: "철의 가장 중요한 기능은 헤모글로빈의 구성 성분으로 각 조직에 산소를 공급하는데 있습니다. 철은 근육 미오글로빈의 구성 성분으로도 필수적인데, 미오글로빈은 근육 속 산소 저장고라 생각하면 이해하기 쉬워요. 따라서 철이 부족하게 되면 우리 온 몸에 산소 공급이 원활하게 되지 않아 빈혈 증세가 나타날 수 있어요.",
  },
  {
    ingredient_name: "아연",
    ingredient_desc: "아연은 우리 몸의 각종 효소 구성성분이고, 면역 기능에 필수적인 역할을 합니다. 미각에도 영향을 미쳐 입맛이 없는 경우에 아연을 섭취하게 되면 증진된다는 보고가 있어요. 아연이 부족하면 성장 지연, 미각 저하 등이 일어나지만, 우리나라에서 아연 부족 현상은 거의 나타나지 않아요.",
  },
  {
    ingredient_name: "나트륨",
    ingredient_desc: "나트륨은 우리나라에서 필요량 이상 섭취되고 있는 대표적인 영양소로, 좋지 않은 인식을 갖고 있지만, 우리 몸 안에서 산과 알칼리의 평형 유지, 삼투압 조절, 근육 수축 등의 작용에 매우 중요한 역할을 해요. 나트륨 과잉증으로는 부종과 고혈압이 있으니 적정량 섭취가 필요합니다.",
  },
];

export const NUTRIENTS_POPUP_WATERVITAMINS_TAB_LIST = [
  {
    ingredient_name: "비타민C",
    ingredient_desc: "비타민 C는 대표적인 항산화 비타민이에요. 활성 산소를 제거하면서 세포를 보호해주는데 이는 생리적으로 매우 중요한 기능입니다. 콜라겐 합성, 중성지방 축적을 억제하는 카르니틴 합성 등의 역할도 있으며, 철이나 칼슘이 우리 몸에 잘 흡수되도록 도와주기도 해요. 비타민 C가 부족하면 출혈이 잘 생기고, 면역기능이 감소되며 상처 회복이 느려지고 빈혈 등이 나타날 수 있습니다.",
  },
  {
    ingredient_name: "티아민(B1)",
    ingredient_desc: "티아민은 리보플라빈, 니아신과 함께 에너지 대사에 필수적인 수용성 비타민 B 복합체 중 하나입니다. 탄수화물, 단백질, 지방이 체내에서 에너지를 만들어내는 과정에서 중요한 작용을 하고, 이외에도 유전 물질인 DNA와 RNA를 합성하는데 필요한 조효소 제공에도 필수적입니다.",
  },
  {
    ingredient_name: "리보플라빈(B2)",
    ingredient_desc: "리보플라빈은 티아민, 니아신과 함께 에너지 대사에 필수적인 수용성 비타민 B 복합체 중 하나로, 니아신 합성에 도움을 주기도 합니다. 리보플라빈이 부족한 식사를 하게 되면 입과 혀에서 염증의 형태가 나타날 수 있는데, 입 가장자리가 갈라지거나 염증이 생기는 구순구각염이나 혀가 붉고 쓰린 증상의 설염, 구내염, 피부염 등이 이에 해당돼요.",
  },
  {
    ingredient_name: "니아신(B3)",
    ingredient_desc: "니아신은 티아민, 리보플라빈과 함께 에너지 대사에 필수적인 수용성 비타민 B 복합체 중 하나입니다. 특히 포도당, 지방, 알코올 대사에 중요하며, 니아신이 부족한 식사를 하게 될 경우, 처음에는 식욕 부진, 체중 감소 등이 나타나고 점차 피로나 우울감 등이 나타날 수 있어요.",
  },
  {
    ingredient_name: "엽산(B9)",
    ingredient_desc: "엽산은 유전 물질인 DNA와 RNA 합성, 메타오닌, 콜린 합성에 관여하는 매우 중요한 수용성 비타민 B 복합체 중 하나입니다. 이외에도 적혈구 세포 성숙에 이용되므로 엽산이 부족할 경우에는 적혈구 성숙이 이루어지지 않아, 비정상적이고 크기만 큰 미성숙한 적혈구가 생성되어 거대적아구성 빈혈이 나타날 수도 있습니다. 또한 엽산은 태아 신경관 장애 위험을 감소시키므로 임신 초기 엽산 보충은 매우 중요해요.",
  },
  {
    ingredient_name: "비타민B12",
    ingredient_desc: "비타민 B12는 엽산이 활성형으로 되어 체내 역할을 할 수 있도록 도와주며, 신경계 정상화에 필요합니다. 비타민 B12가 부족하면 악성 빈혈이 나타날 수 있는데, 일반적으로 섭취한 비타민 B12가 부족하기 보다는 체내 흡수가 되지 않아 부족한 경우가 많습니다. 동물성 식품에 주로 함유되어 있으므로, 채식주의자에게서는 식이 비타민 B12 자체가 부족하여 결핍증이 나타날 수 있으니 주의가 필요해요.",
  },
];

export const NUTRIENTS_POPUP_FATVITAMINS_TAB_LIST = [
  {
    ingredient_name: "비타민A",
    ingredient_desc: "비타민 A는 시각 기능을 정상적으로 유지하는데 중요한 역할을 합니다. 밝은 빛을 느끼는 세포와 어두운 빛을 느끼는 세포에 의해 눈은 빛에 대한 감각을 느끼는데, 비타민 A는 어두운 빛을 느끼는 세포에 함유된 색소 단백질인 로돕신의 구성성분이에요. 따라서 비타민 A가 부족하면 어두운 빛에 대한 감각이 떨어져 야맹증이 됩니다. 이외에도 비타민 A는 면역력 증진과 관련하여 질병에 대한 감염을 막아주고, 상피조직 유지, 항산화 및 항암 효과를 가지고 있어요.",
  },
  {
    ingredient_name: "비타민D",
    ingredient_desc: "비타민 D는 우리 몸에 흡수된 칼슘이 뼈나 치아 등에 단단하게 만들어질 수 있도록 도와주고, 혈장 칼슘의 농도를 유지 시켜요. 비타민 D가 부족하면 뼈가 단단해지지 못하게 되는데, 아이들의 경우에는 구루병이 생길 수 있고, 성인의 경우에는 골다공증, 골연화증이 나타날 수 있어요. 반면, 너무 많이 섭취하게 되면 식욕부진, 오심, 피로, 갈증, 구토, 설사 뿐 아니라, 고칼슘혈증, 고칼슘뇨증 등이 나타나 신장에 무리를 줄 수 있으므로, 각 연령에 맞는 적정량을 섭취하는 것이 매우 중요합니다.",
  },
  {
    ingredient_name: "비타민E",
    ingredient_desc: "우리 몸의 세포는 세포막으로 둘러쌓여 있는데, 이 세포막에는 불포화지방산, 즉 산화되기 쉬운 형태의 지방산이 포함되어 있어요. 비타민 E는 이런 세포막의 산화를 막아주는 항산화 비타민으로 알려져 있으며, 이외에도 면역 기능, 항암 효과와 관련이 있습니다. 비타민 E가 부족하면 세포막 손상으로 적혈구가 파괴되어 빈혈이 나타날 수 있고 노화 증상이 더 빠르게 나타난다고 알려져 있어요.",
  },
];

export const NUTRIENTS_TEXT = {
  TITLE: (temp) => {
    return `이 레시피의 1회 섭취 시 열량은
총 ${temp}kcal 이에요 `
  },
  NUTRIENTS_AVOID_TITLE: (temp) => {
    return `섭취에 주의해야 할 재료가 
${temp}개 들어 있어요`
  },
  NUTRIENTS_ALLERGIES_TITLE: (temp) => {
    return `알레르기를 유발 할 수 있는 재료가 
${temp}개 들어 있어요`
  },
  TOOLTIP: (temp) => {
    return `・${temp} 하루 권장 섭취 영양소 기준`
  },
  TOOLTIP_FIXED: "・각 재료별 기준으로 산출된 지표로, 실제 조리된 음식과는 상이할 수 있습니다",
  SUB_TITLE: {
    GOOD: "대표 영양소",
    BAD: "당류・나트륨",
  },
  MY_BABY: "우리아이",
  LIMIT_TAKE: "상한 섭취량",
};

export const NUTRIENTS_NAME_ARRAY_GOOD = [
  ['탄수화물', '단백질', '식이섬유', '지방', '칼슘', '철',],
  ['수분', '비타민A', '비타민D', '비타민E', '비타민C', '티아민',],
  ['리보플라빈', '니아신', '엽산', '비타민B12', '아연',],
];

export const NUTRIENTS_NAME_ARRAY_BAD = [
  '당류', '나트륨',
];

export const INGREDIENTS_TEXT = {
  TITLE_FIRST: (temp) => {
    return `이 레시피는 ${temp}회분 기준으로`
  },
  TITLE_FIXED: "작성되었어요",
  SUB_TITLE: {
    BASICS: "기본재료",
    SEASONINGS: "양념재료",
  },
  STEP: "STEP ",
};

export const REVIEW_TEXT = {
  TITLE: "이 레시피 어땠나요?",
  BUTTON_TEXT: "리뷰 등록하기",
}

export const REVIEW_ARRAY = [
  {
    name: '아이취향',
    image: 'REVIEW_LIKE',
    example: 'like',
    checked: 'N',
  },
  {
    name: '맛이훌륭',
    image: 'REVIEW_GREAT',
    example: 'great',
    checked: 'N',
  },
  {
    name: '조리쉬움',
    image: 'REVIEW_EASY',
    example: 'easy',
    checked: 'N',
  },
  {
    name: '영양풍부',
    image: 'REVIEW_RICH',
    example: 'rich',
    checked: 'N',
  },
  // ['아이취향', '/assets/icons/detail/review_my_baby.svg', 'like'],
  // ['맛이훌륭', '/assets/icons/detail/review_taste.svg', 'taste'],
  // ['조리쉬움', '/assets/icons/detail/review_easy_cook.svg', 'difficulty'],
  // ['영양풍부', '/assets/icons/detail/review_nutrients.svg', 'nutrition'],
];

export const LOGIN_POPUP_TEXT = {
  TITLE: '로그인 후\n콘텐츠를 이용해보세요!',
  BUTTON_TEXT: {
    CLOSE: '취소',
    GO_LOGIN: '로그인'
  }
}

export const GUEST_POPUP_TEXT = {
  TITLE: '로그인하면\n우리아이 맞춤 정보를 볼 수 있어요',
  BUTTON_TEXT: {
    CLOSE: '닫기',
    GO_LOGIN: '로그인하기'
  }
}

export const GRADE_POPUP_TEXT = {
  TITLE: '초코 등급이 되면\n전문가 레시피를 볼 수 있어요.',
  SUB_TITLE: '프로필 사진만 등록하고\n5초만에 초코 등급이 되어보세요',
  BUTTON_TEXT: {
    CLOSE: '다음에 하기',
    SHOW_BENEFIT: '프로필 등록하기'
  }
}

export const WITHDRAW_MEMBER_POPUP_TEXT = {
  TITLE: '탈퇴한 회원입니다.',
}