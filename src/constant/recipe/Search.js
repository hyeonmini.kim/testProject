import {ICON_TYPE} from "../icons/ImageIcons";
import {KEYWORD_TYPE} from "../common/HealthKeyword";

export const RECIPE_SEARCH_HOME_HEALTH = {
  NAME: '건강키워드별',
  ITEM: [
    {title: KEYWORD_TYPE.COLD, type: KEYWORD_TYPE.COLD},
    {title: KEYWORD_TYPE.CONSTIPATION, type: KEYWORD_TYPE.CONSTIPATION},
    {title: KEYWORD_TYPE.LACTOSE, type: KEYWORD_TYPE.LACTOSE},
    {title: KEYWORD_TYPE.IMBALANCED, type: KEYWORD_TYPE.IMBALANCED},
    {title: KEYWORD_TYPE.SKIN, type: KEYWORD_TYPE.SKIN},
    {title: KEYWORD_TYPE.MUSCLE, type: KEYWORD_TYPE.MUSCLE},
    {title: KEYWORD_TYPE.BONE, type: KEYWORD_TYPE.BONE},
    {title: KEYWORD_TYPE.ANEMIA, type: KEYWORD_TYPE.ANEMIA},
    {title: KEYWORD_TYPE.MOUTH, type: KEYWORD_TYPE.MOUTH},
    {title: KEYWORD_TYPE.GUMS, type: KEYWORD_TYPE.GUMS},
    {title: KEYWORD_TYPE.EYEBALL, type: KEYWORD_TYPE.EYEBALL},
  ],
}

export const RECIPE_SEARCH_HOME_MATERIAL = {
  NAME: '재료별',
  ITEM: [
    {title: '소고기', type: ICON_TYPE.MATERIAL_BEEF},
    {title: '돼지고기', type: ICON_TYPE.MATERIAL_PORK},
    {title: '닭고기', type: ICON_TYPE.MATERIAL_CHICKEN},
    {title: '육류', type: ICON_TYPE.MATERIAL_MEAT},
    {title: '수산물', type: ICON_TYPE.MATERIAL_MARINE},
    {title: '채소류', type: ICON_TYPE.MATERIAL_VEGETABLE},
    {title: '버섯', type: ICON_TYPE.MATERIAL_MUSHROOM},
    {title: '과일류', type: ICON_TYPE.MATERIAL_FRUIT},
    {title: '쌀', type: ICON_TYPE.MATERIAL_RICE},
    {title: '밀가루', type: ICON_TYPE.MATERIAL_FLOUR},
    {title: '곡류', type: ICON_TYPE.MATERIAL_GRAIN},
    {title: '콩/견과', type: ICON_TYPE.MATERIAL_BEAN},
    {title: '난류', type: ICON_TYPE.MATERIAL_EGG},
    {title: '유제품', type: ICON_TYPE.MATERIAL_MILK},
  ],
}

export const RECIPE_SEARCH_HOME_CATEGORY = {
  NAME: '카테고리별',
  ITEM: [
    {title: '밥/죽', type: ICON_TYPE.FOOD_RICE},
    {title: '면요리', type: ICON_TYPE.FOOD_NOODLE},
    {title: '국/찌개', type: ICON_TYPE.FOOD_STEW},
    {title: '반찬', type: ICON_TYPE.FOOD_SIDE},
    {title: '한그릇', type: ICON_TYPE.FOOD_BOWL},
    {title: '특식', type: ICON_TYPE.FOOD_SPECIAL},
    {title: '간식', type: ICON_TYPE.FOOD_SNACK},
    {title: '베이킹', type: ICON_TYPE.FOOD_BAKING},
    {title: '음료', type: ICON_TYPE.FOOD_DRINK},
  ],
}

export const RECIPE_SEARCH_POPULAR = {
  NAME: '인기 검색어',
  ITEM: ['볶음밥', '떡볶이', '멸치볶음', '소고기', '돼지고기', '달걀', '감자', '버섯', '토마토', '두부']
}

export const RECIPE_SEARCH_AGE_GROUP_TYPE = {
  ALL: '전체',
  EARLY: '초기이유식',
  MIDDLE: '중기이유식',
  LAST: '후기이유식',
  EARLY_CHILD: '전기유아식',
  LAST_CHILD: '후기유아식',
}

export const RECIPE_SEARCH_AGE_GROUP_TYPE_MONTH = {
  ALL: '',
  EARLY: '4~6개월',
  MIDDLE: '7~9개월',
  LAST: '10~12개월',
  EARLY_CHILD: '13~36개월',
  LAST_CHILD: '37~60개월',
}

export const RECIPE_SEARCH_TYPE = {
  INPUT: 'INPUT',
  KEYWORD: 'KEYWORD',
  MATERIAL: 'MATERIAL',
  SEASONING: 'SEASONING',
  CATEGORY: 'CATEGORY',
  COMEBACK: 'COMEBACK',
  USER: 'USER',
  CUSTOM: 'CUSTOM',
}

export const RECIPE_SEARCH_COMMON = {
  TABS: [
    RECIPE_SEARCH_HOME_HEALTH.NAME,
    RECIPE_SEARCH_HOME_MATERIAL.NAME,
    RECIPE_SEARCH_HOME_CATEGORY.NAME
  ],
  AGE_GROUP: [
    {value: RECIPE_SEARCH_AGE_GROUP_TYPE.ALL, subTitle: RECIPE_SEARCH_AGE_GROUP_TYPE_MONTH.ALL},
    {value: RECIPE_SEARCH_AGE_GROUP_TYPE.EARLY, subTitle: RECIPE_SEARCH_AGE_GROUP_TYPE_MONTH.EARLY},
    {value: RECIPE_SEARCH_AGE_GROUP_TYPE.MIDDLE, subTitle: RECIPE_SEARCH_AGE_GROUP_TYPE_MONTH.MIDDLE},
    {value: RECIPE_SEARCH_AGE_GROUP_TYPE.LAST, subTitle: RECIPE_SEARCH_AGE_GROUP_TYPE_MONTH.LAST},
    {value: RECIPE_SEARCH_AGE_GROUP_TYPE.EARLY_CHILD, subTitle: RECIPE_SEARCH_AGE_GROUP_TYPE_MONTH.EARLY_CHILD},
    {value: RECIPE_SEARCH_AGE_GROUP_TYPE.LAST_CHILD, subTitle: RECIPE_SEARCH_AGE_GROUP_TYPE_MONTH.LAST_CHILD},
  ],
  SEARCH_TYPE_CUSTOM: '우리 아이 맞춤 레시피',
  SEARCH_TYPE_USER: '성장 도움 레시피',
  TEXT: {
    TITLE: '검색',
    SEARCH_PLACEHOLDER: '아이 맞춤 레시피를 찾아보세요',
    AVOID: '피해야할 재료가 들어간 레시피 제외',
    TOOLTIP: '우리 아이 알레르기 재료와 \n' +
      '우리아이 월령에 먹어서는 안 될 재료가 \n' +
      '들어있는 레시피는 제외하고 보여드릴게요',
    NO_SEARCH_RESULT_TITLE: '해당되는 검색 결과가 없어요.',
    NO_SEARCH_RESULT_SUB_TITLE: '검색어를 다시 한번 확인해 주세요.',
  },
  NONE: ''
}

export const RECIPE_RESULT_STATUS = {
  INIT: undefined,
  ERROR: 'ERROR',
  NO_DATA: [],
  NONE: ''
}

export const RECIPE_SEARCH = {
  type: undefined,
  search: '',
  result: '',
  isCall: false,
  isTitle: false,
  isSearch: false,
  category: '',
  isAvoid: false,
  ageGroup: RECIPE_SEARCH_AGE_GROUP_TYPE.ALL,
  activeTab: RECIPE_SEARCH_HOME_HEALTH.NAME,
}
