export const RECIPE = {
  recipe_key: '',
  recipe_user_key: '',
  recipe_name: '',
  recipe_desc: '',
  recipe_category: '',
  recipe_lead_time: '',
  recipe_level: '',
  recipe_babyfood_step: '',
  recipe_health_note: '',
  recipe_servings: 1,
  recipe_order_list: [],
  recipe_reject_msg: '',
  recipe_tag_desc: '',
}

export const RECIPE_STATUS = {
  TEMPORARY: 'TEMPORARY',
  REJECT: 'REJECT',
  EXAMINATION: 'EXAMINATION',
  APPROVAL: 'APPROVAL',
}

export const RECIPE_MOCK_TEMPORARY = {
  status: RECIPE_STATUS.TEMPORARY,
  name: '인천끼룩호빵',
  desc: '인천사람들이 좋아하는 끼룩스 요리',
  category: '밥/죽',
  images: [
    "https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg",
    "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg",
  ],
  cookTime: '30분이내',
  difficulty: '할만해요',
  monthlyAge: '중기 이유식',
  healthNote: '건강을 위해서라면 비타민이 풍부한 인천끼룩호빵!',
  quantity: 1,
  basicMaterials: [
    {
      material: '비둘기',
      quantity: 1,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '호빵',
      quantity: 2,
      unit: 'kg',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '새우깡',
      quantity: 3,
      unit: 'g',
      unitValue: 100,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
  ],
  seasoningMaterials: [
    {
      material: '인천간장',
      quantity: 1,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '고추장',
      quantity: 2,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
  ],
  steps: [
    {
      text: '인천비둘기를 잡는다.',
      image: "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/8b67a8ea21f00802129263bf75119dcd1.jpg"
    },
    {
      text: '인천비둘기를 손질한다.',
      image: "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/7a648fb8055752dfff7857f931f6aa8e1.jpg"
    },
    {text: '인천비둘기를 요리한다.', image: null},
  ],
  tag: '부천불주먹레시피,끼룩추천',
}

export const RECIPE_MOCK_EXAMINATION = {
  status: RECIPE_STATUS.EXAMINATION,
  name: '인천끼룩호빵',
  desc: '인천사람들이 좋아하는 끼룩스 요리',
  category: '밥/죽',
  images: [
    "https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg",
    "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg",
  ],
  cookTime: '30분이내',
  difficulty: '할만해요',
  monthlyAge: '중기 이유식',
  healthNote: '건강을 위해서라면 비타민이 풍부한 인천끼룩호빵!',
  quantity: 1,
  basicMaterials: [
    {
      material: '비둘기',
      quantity: 1,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '호빵',
      quantity: 2,
      unit: 'kg',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '새우깡',
      quantity: 3,
      unit: 'g',
      unitValue: 100,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
  ],
  seasoningMaterials: [
    {
      material: '인천간장',
      quantity: 1,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '고추장',
      quantity: 2,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
  ],
  steps: [
    {
      text: '인천비둘기를 잡는다.',
      image: "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/8b67a8ea21f00802129263bf75119dcd1.jpg"
    },
    {
      text: '인천비둘기를 손질한다.',
      image: "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/7a648fb8055752dfff7857f931f6aa8e1.jpg"
    },
    {text: '인천비둘기를 요리한다.', image: null},
  ],
  tag: '인천끼룩이화이팅',
}

export const RECIPE_MOCK_REJECT = {
  status: RECIPE_STATUS.REJECT,
  returnMessage: '레시피를 작성해 주셔서 감사합니다.\n\n' +
    '아래 내용이 도낫츠의 레시피 작성 규정에 적합하지 않아 수정을 요청드립니다.\n\n' +
    '1. 부적절한 단어사용\n' +
    'STEP1에 \'**\' 단어 대신 다른 단어를 사용해주세요.\n\n' +
    '2. 부적절한 단위사용\n' +
    'STEP3에 \'**\' 재료 단위를 올바르게 작성해주세요.',
  name: '인천끼룩호빵',
  desc: '인천사람들이 좋아하는 끼룩스 요리',
  category: '밥/죽',
  images: [
    "https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg",
    "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg",
  ],
  cookTime: '30분이내',
  difficulty: '할만해요',
  monthlyAge: '중기 이유식',
  healthNote: '건강을 위해서라면 비타민이 풍부한 인천끼룩호빵!',
  quantity: 1,
  basicMaterials: [
    {
      material: '비둘기',
      quantity: 1,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '호빵',
      quantity: 2,
      unit: 'kg',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '새우깡',
      quantity: 3,
      unit: 'g',
      unitValue: 100,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
  ],
  seasoningMaterials: [
    {
      material: '인천간장',
      quantity: 1,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '고추장',
      quantity: 2,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
  ],
  steps: [
    {
      text: '인천비둘기를 잡는다.',
      image: "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/8b67a8ea21f00802129263bf75119dcd1.jpg"
    },
    {
      text: '인천비둘기를 손질한다.',
      image: "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/7a648fb8055752dfff7857f931f6aa8e1.jpg"
    },
    {text: '인천비둘기를 요리한다.', image: null},
  ],
  tag: '송파비둘기추천',
}

export const RECIPE_MOCK_APPROVAL = {
  status: RECIPE_STATUS.APPROVAL,
  name: '인천끼룩호빵',
  desc: '인천사람들이 좋아하는 끼룩스 요리',
  category: '밥/죽',
  images: [
    "https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg",
    "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg",
  ],
  cookTime: '30분이내',
  difficulty: '할만해요',
  monthlyAge: '중기 이유식',
  healthNote: '건강을 위해서라면 비타민이 풍부한 인천끼룩호빵!',
  quantity: 1,
  basicMaterials: [
    {
      material: '비둘기',
      quantity: 1,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '호빵',
      quantity: 2,
      unit: 'kg',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '새우깡',
      quantity: 3,
      unit: 'g',
      unitValue: 100,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
  ],
  seasoningMaterials: [
    {
      material: '인천간장',
      quantity: 1,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
    {
      material: '고추장',
      quantity: 2,
      unit: '스푼',
      unitValue: 1,
      units: [{unit: 'g', value: 100}, {unit: 'kg', value: 1}, {unit: '스푼', value: 1}]
    },
  ],
  steps: [
    {
      text: '인천비둘기를 잡는다.',
      image: "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/8b67a8ea21f00802129263bf75119dcd1.jpg"
    },
    {
      text: '인천비둘기를 손질한다.',
      image: "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/7a648fb8055752dfff7857f931f6aa8e1.jpg"
    },
    {text: '인천비둘기를 요리한다.', image: null},
  ],
  tag: '송파비둘기추천',
}

/*
  // Images
  [file, file, file, file]

  // Materials
  [
    {material:'아기간장', quantity:1, unit: '스푼', unitValue: 1, units: [{unit:'g', value:100},{unit:'kg', value:1},{unit:'스푼', value:1}]},
    {material:'아기간장', quantity:1, unit: '스푼', unitValue: 1, units: [{unit:'g', value:100},{unit:'kg', value:1},{unit:'스푼', value:1}]},
    {material:'아기간장', quantity:1, unit: '스푼', unitValue: 1, units: [{unit:'g', value:100},{unit:'kg', value:1},{unit:'스푼', value:1}]},
  ]

  // Steps
 [
    {text: '', image: null},
    {text: '', image: null},
    {text: '', image: null},
  ]
*/