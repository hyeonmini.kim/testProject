import {PATH_DONOTS_STATIC_PAGE} from "../../routes/paths";

export const MAIN_DESKTOP = {
  TITLE1: '오늘도 건강하게',
  TITLE2: '우리 아이를 위한',
  INFO1: '앱 다운로드하고',
  INFO2: '더 간편하게 이용하세요!'
}

export const MAIN_TITLE = {
  HEADER: (title) => {
    if (!title) {
      return `도낫츠 | 우리 아이를 위한 donots`
    } else {
      return `도낫츠 | ${title}`
    }
  },
}

export const MAIN_META = {
  TITLE: '우리 아이를 위한 donots',
  DESC: '도낫츠(donots) - 아이 영양관리, 건강꿀팁 공유',
  IMAGE: 'https://d1f1anino86jvg.cloudfront.net/store/meta.jpg'
}