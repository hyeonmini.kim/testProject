import {AppBar, Box, Container, IconButton, Toolbar, Typography} from "@mui/material";
import {styled} from "@mui/material/styles";
import {HEADER} from "../../../config";
import {bgBlur} from "../../../utils/cssStyles";
import BodyLayout from "./BodyLayout";
import {SvgBackIcon, SvgCloseIcon} from "../../../constant/icons/icons";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";

const ToolbarStyle = styled(Toolbar)(({theme}) => ({
  height: HEADER.H_MOBILE,
  zIndex: theme.zIndex.appBar + 1,
  ...bgBlur({
    color: theme.palette.background.default,
  }),
  transition: theme.transitions.create(['height'], {
    duration: theme.transitions.duration.shorter,
  }),
}));

export default function HeaderDialogLayout({title, onBack, showClose, onClose, children}) {
  return (
    <>
      <AppBar sx={{boxShadow: 0, background: 'white'}}>
        <Container disableGutters maxWidth={'xs'} sx={{px: '20px'}}>
          <ToolbarStyle disableGutters sx={{'&.MuiToolbar-root': {minHeight: HEADER.H_MOBILE}}}>
            <Box sx={{display: 'flex', width: '100%', justifyContent: 'space-between', alignItems: 'center'}}>
              {onBack && (
                <IconButton onClick={onBack} tabIndex={-1} disableFocusRipple sx={{
                  m: '-8px',
                  position: 'absolute',
                  left: 0,
                  '&:hover': {
                    backgroundColor: '#FFFFFF',
                  },
                }}>
                  <SvgBackIcon alt={ALT_STRING.COMMON.BTN_BACK} tabIndex={0} onKeyDown={(e) => handleEnterPress(e, () => onBack)} sx={{...clickBorderNone}}/>
                </IconButton>
              )}
              <Typography align="center" sx={{
                fontSize: '18px', fontWeight: '500', lineHeight: '24px', color: '#000000', flexGrow: 1
              }}>
                {title}
              </Typography>
              {showClose && (
                <IconButton onClick={onClose} tabIndex={-1} disableFocusRipple sx={{
                  m: '-8px',
                  position: 'absolute',
                  right: 0,
                  '&:hover': {
                    backgroundColor: '#FFFFFF',
                  },
                }}>
                  <SvgCloseIcon alt={ALT_STRING.COMMON.BTN_CLOSE} tabIndex={0} onKeyDown={(e) => handleEnterPress(e, () => onClose)}
                                sx={{...clickBorderNone}}/>
                </IconButton>
              )}
            </Box>
          </ToolbarStyle>
        </Container>
      </AppBar>
      <BodyLayout>
        {children}
      </BodyLayout>
    </>
  );
}