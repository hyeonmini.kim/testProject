import {AppBar, Box, Container, IconButton, InputAdornment, Toolbar,} from "@mui/material";
import {styled} from "@mui/material/styles";
import {HEADER} from "../../../config";
import {bgBlur} from "../../../utils/cssStyles";
import PropTypes from "prop-types";
import BodyLayout from "./BodyLayout";
import {useEffect, useRef, useState} from "react";
import {RECIPE_CREATE_MATERIAL} from "../../../constant/recipe/Create";
import {ICON_COLOR, SvgBackIcon, SvgDeleteIcon, SvgSearchIcon} from "../../../constant/icons/icons";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";
import TextFieldWrapper from "../../../components/common/TextFieldWrapper";

const ToolbarStyle = styled(Toolbar)(({theme}) => ({
  height: HEADER.H_MOBILE,
  zIndex: theme.zIndex.appBar + 1,
  ...bgBlur({
    color: theme.palette.background.default,
  }),
  transition: theme.transitions.create(['height'], {
    duration: theme.transitions.duration.shorter,
  }),
}));


HeaderSearchLayout.propTypes = {
  search: PropTypes.string,
  setSearch: PropTypes.func,
  setShowSearch: PropTypes.func,
  handleBackButton: PropTypes.func,
  handleSearchButton: PropTypes.func,
  children: PropTypes.any,
}

export default function HeaderSearchLayout({
                                             search,
                                             setSearch,
                                             setShowSearch,
                                             handleBackButton,
                                             handleSearchButton,
                                             children,
                                             onClick
                                           }) {
  const [temp, setTemp] = useState('');
  const [focus, setFocus] = useState(false);
  const searchRef = useRef(null);

  useEffect(() => {
    setTemp(search)
  }, [search])

  const handleSearchChange = (event) => {
    let value = event.target.value?.substring(0, 20);
    setTemp(value);
  }

  const handleSearchFocus = () => {
    setFocus(true)
  }

  const handleSearchBlur = () => {
    setFocus(false)
  }

  const handleSearchClear = () => {
    setSearch('')
    setShowSearch(false)
    searchRef.current.focus()
  }

  const handleClick = () => {
    handleSearchButton(temp)
    if(onClick){
      onClick()
    }
  }

  return (
    <>
      <AppBar sx={{boxShadow: 0, background: 'white'}}>
        <Container disableGutters maxWidth={'xs'} sx={{px: '20px'}}>
          <ToolbarStyle disableGutters sx={{'&.MuiToolbar-root': {minHeight: HEADER.H_MOBILE}}}>
            <Box sx={{width: '100%', display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
              <SvgBackIcon
                alt={ALT_STRING.COMMON.BTN_BACK}
                tabIndex={0}
                onClick={handleBackButton}
                onKeyDown={(e) => handleEnterPress(e, handleBackButton)}
                sx={{...clickBorderNone}}
              />
              <TextFieldWrapper
                autoComplete='off'
                inputRef={searchRef}
                placeholder={RECIPE_CREATE_MATERIAL.TEXT.SEARCH}
                variant="outlined"
                value={temp}
                onChange={handleSearchChange}
                onFocus={handleSearchFocus}
                onBlur={handleSearchBlur}
                onKeyDown={(e) => {
                  if (e.key === 'Enter') {
                    handleSearchButton(temp)
                  }
                }}
                sx={{ml: '15px', flexGrow: 1}}
                inputProps={{title: ALT_STRING.SEARCH.INPUT_SEARCH}}
                InputProps={{
                  startAdornment: (focus || temp.length > 0) ? (<></>) : (
                    <InputAdornment position="start">
                      <SvgSearchIcon alt={ALT_STRING.SEARCH.ICON_SEARCH} color={ICON_COLOR.GREY}/>
                    </InputAdornment>
                  ),
                  endAdornment: temp.length > 0 ? (
                    <InputAdornment position="end">
                      <SvgDeleteIcon alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} tabIndex={0} color={ICON_COLOR.GREY} onClick={handleSearchClear}
                                     onKeyDown={(e) => handleEnterPress(e, handleSearchClear)} sx={{...clickBorderNone}}/>
                    </InputAdornment>
                  ) : (<></>),
                  sx: {
                    height: '46px',
                    '& input': {fontSize: '16px', fontWeight: '500', color: '#222222'},
                    '&.MuiInputBase-root': {pl: '10px'},
                    '& input::placeholder': {fontSize: '16px', fontWeight: '500', color: '#cccccc'},
                  }
                }}
              />
              {(focus || temp.length > 0) && (
                <IconButton tabIndex={-1} disableFocusRipple onClick={handleClick} sx={{
                  m: '-8px -8px -8px 8px',
                  '&:hover': {
                    backgroundColor: '#FFFFFF',
                  },
                }}>
                  <SvgSearchIcon alt={ALT_STRING.SEARCH.ICON_SEARCH} tabIndex={temp ? 0 : -1} sx={{...clickBorderNone}}/>
                </IconButton>
              )}
            </Box>
          </ToolbarStyle>
        </Container>
      </AppBar>
      <BodyLayout>
        {children}
      </BodyLayout>
    </>
  );
}