import PropTypes from 'prop-types';
import AuthGuard from "../../../auth/AuthGuard";
import {useSetRecoilState} from "recoil";
import {gnbActiveTabSelector} from "../../../recoil/selector/common/gnbSelector";
import {useEffect} from "react";
import {GNB_TYPE} from "../../../constant/common/GNB";

RecipeCreateLayout.propTypes = {
  children: PropTypes.node,
};

export default function RecipeCreateLayout({children}) {
  const setActiveTab = useSetRecoilState(gnbActiveTabSelector);
  useEffect(() => {
    setActiveTab(GNB_TYPE.CREATE)
  }, []);
  return (
    <AuthGuard>
      {children}
    </AuthGuard>
  );
}
