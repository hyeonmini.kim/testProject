import PropTypes from 'prop-types';

DetailLayout.propTypes = {
  children: PropTypes.node,
};

export default function DetailLayout({children}) {
  return (
    <>
      {children}
    </>
  )
}
