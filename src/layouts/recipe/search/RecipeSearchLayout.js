import PropTypes from 'prop-types';
import {useSetRecoilState} from "recoil";
import {gnbActiveTabSelector} from "../../../recoil/selector/common/gnbSelector";
import {useEffect} from "react";
import {GNB_TYPE} from "../../../constant/common/GNB";

RecipeSearchLayout.propTypes = {
  children: PropTypes.node,
};

export default function RecipeSearchLayout({children}) {
  const setActiveTab = useSetRecoilState(gnbActiveTabSelector);
  useEffect(() => {
    setActiveTab(GNB_TYPE.SEARCH)
  }, []);
  return (
    <>
      {children}
    </>
  );
}
