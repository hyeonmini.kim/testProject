import {AppBar, Box, Container, IconButton, InputAdornment, Toolbar, Typography,} from "@mui/material";
import PropTypes from "prop-types";
import BodyLayout from "./BodyLayout";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {RECIPE_SEARCH_AGE_GROUP_TYPE, RECIPE_SEARCH_COMMON} from "../../../constant/recipe/Search";
import {HEADER} from "../../../config";
import {useRecoilState, useResetRecoilState} from "recoil";
import {
  searchAgeGroupSelector,
  searchFetchItemsSelector,
  searchIsAvoidSelector,
  searchIsSearchSelector,
  searchIsTitleSelector,
  searchRecipeResultSelector,
  searchRecipeSearchSelector
} from "../../../recoil/selector/recipe/searchSelector";
import {ICON_COLOR, SvgBackIcon, SvgDeleteIcon, SvgSearchIcon} from "../../../constant/icons/icons";
import {sxFixedCenterMainLayout} from "../../main/MainLayout";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../../constant/common/Common";
import {ALT_STRING} from "../../../constant/common/AltString";
import TextFieldWrapper from "../../../components/common/TextFieldWrapper";

HeaderSearchLayout.propTypes = {
  children: PropTypes.any,
}

export default function HeaderSearchLayout({onBack, children}) {
  return (
    <>
      <AppBar sx={{boxShadow: 0, mx: 0, px: 0, background: 'white', ...sxFixedCenterMainLayout}}>
        <Container disableGutters maxWidth={'xs'} sx={{px: '20px'}}>
          <Toolbar disableGutters sx={{height: HEADER.H_MOBILE}}>
            <Box sx={{display: 'flex', width: '100%', justifyContent: 'space-between'}}>
              <ToolbarLayout onBack={onBack}/>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <BodyLayout>
        {children}
      </BodyLayout>
    </>
  );
}

function ToolbarLayout({onBack}) {
  const [isSearch, setIsSearch] = useRecoilState(searchIsSearchSelector);
  const [isTitle, setIsTitle] = useRecoilState(searchIsTitleSelector);
  const [focus, setFocus] = useState(false);

  if (isTitle) {
    return (
      <Box sx={{width: '100%', display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
        <BackButtonLayout onBack={onBack}/>
        <SearchTitleLayout/>
      </Box>
    )
  }

  if (isSearch) {
    return (
      <Box sx={{width: '100%', display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
        <BackButtonLayout onBack={onBack}/>
        <SearchInputLayout focus={focus} setFocus={setFocus}/>
        <SearchButtonLayout focus={focus}/>
      </Box>
    )
  }

  return (
    <Typography sx={{color: '#000000', fontSize: '20px', fontWeight: '700', lineHeight: '26px'}}>
      {RECIPE_SEARCH_COMMON.TEXT.TITLE}
    </Typography>
  )
}

function BackButtonLayout({onBack}) {
  return (
    <SvgBackIcon
      alt={ALT_STRING.COMMON.BTN_BACK}
      tabIndex={0}
      onClick={onBack}
      onKeyDown={(e) => handleEnterPress(e, onBack)}
      sx={{...clickBorderNone}}
    />
  )
}

function SearchButtonLayout({focus}) {
  const [search, setSearch] = useRecoilState(searchRecipeSearchSelector);
  const [result, setResult] = useRecoilState(searchRecipeResultSelector);
  const resetAgeGroup = useResetRecoilState(searchAgeGroupSelector)
  const resetIsAvoid = useResetRecoilState(searchIsAvoidSelector)
  const resetFetchItems = useResetRecoilState(searchFetchItemsSelector);

  const handleSearchClick = () => {
    if (!search) return
    setResult(search)
    resetAgeGroup()
    resetIsAvoid()
    resetFetchItems()
  }

  return (
    <>
      {(focus || search.length > 0) && (
        <IconButton
          tabIndex={-1}
          disableFocusRipple
          onClick={handleSearchClick}
          sx={{
            m: '-8px -8px -8px 8px',
            '&:hover': {
              backgroundColor: '#FFFFFF',
            },
          }}
          onKeyDown={(e) => handleEnterPress(e, handleSearchClick)}
        >
          <SvgSearchIcon alt={ALT_STRING.SEARCH.ICON_SEARCH} tabIndex={search ? 0 : -1} sx={{...clickBorderNone}}/>
        </IconButton>
      )}
    </>
  )
}

function SearchTitleLayout() {
  const [search, setSearch] = useRecoilState(searchRecipeSearchSelector);
  const [ageGroup, setAgeGroup] = useRecoilState(searchAgeGroupSelector);

  return (
    <Box sx={{mr: '24px', display: 'flex', flexDirection: 'row', flexGrow: 1, justifyContent: 'center'}}>
      <Typography
        sx={{fontSize: '18px', fontWeight: '500', lineHeight: '18px', letterSpacing: '-0.02em', color: '#000000'}}>
        {search}
      </Typography>
      {ageGroup !== RECIPE_SEARCH_AGE_GROUP_TYPE.ALL &&
        <Typography
          sx={{fontSize: '18px', fontWeight: '400', lineHeight: '18px', letterSpacing: '-0.02em', color: '#000000'}}>
          ({ageGroup})
        </Typography>
      }
    </Box>
  )
}

function SearchInputLayout({focus, setFocus}) {
  const searchRef = useRef(null);
  const [search, setSearch] = useRecoilState(searchRecipeSearchSelector);
  const [isSearch, setIsSearch] = useRecoilState(searchIsSearchSelector);
  const [result, setResult] = useRecoilState(searchRecipeResultSelector);
  const resetFetchItems = useResetRecoilState(searchFetchItemsSelector);
  const resetAgeGroup = useResetRecoilState(searchAgeGroupSelector)
  const resetIsAvoid = useResetRecoilState(searchIsAvoidSelector)

  useEffect(() => {
    searchRef.current.focus()
  }, [])

  const handleSearchClear = () => {
    setSearch('')
    setResult('')
    searchRef.current.focus()
    resetAgeGroup()
    resetIsAvoid()
    resetFetchItems()
  };
  const handleSearchChange = (event) => {
    let value = event.target.value?.substring(0, 20);
    setSearch(value);
    setIsSearch(true)
  };

  const handleSearchClick = () => {
    if (!search) return
    setResult(search)
    resetAgeGroup()
    resetIsAvoid()
    resetFetchItems()
  }

  return (
    <TextFieldWrapper
      tabIndex={0}
      autoComplete='off'
      inputRef={searchRef}
      placeholder={RECIPE_SEARCH_COMMON.TEXT.SEARCH_PLACEHOLDER}
      label={''}
      variant="outlined"
      value={search}
      onChange={handleSearchChange}
      onKeyDown={(e) => {
        if (e.key === 'Enter') {
          handleSearchClick()
        }
      }}
      onFocus={() => setFocus(true)}
      onBlur={() => setFocus(false)}
      sx={{ml: '15px', flexGrow: 1}}
      inputProps={{title: ALT_STRING.SEARCH.INPUT_SEARCH}}
      InputProps={{
        startAdornment: (focus || search.length > 0) ? (<></>) : (
          <InputAdornment position="start">
            <SvgSearchIcon alt={ALT_STRING.SEARCH.ICON_SEARCH} color={ICON_COLOR.GREY}/>
          </InputAdornment>
        ),
        endAdornment: search.length > 0 ? (
          <InputAdornment position="end">
            <SvgDeleteIcon
              alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT}
              tabIndex={0}
              color={ICON_COLOR.GREY}
              onClick={handleSearchClear}
              onKeyDown={(e) => {
                e.stopPropagation()
                handleEnterPress(e, handleSearchClear)
              }}
              sx={{...clickBorderNone}}/>
          </InputAdornment>
        ) : (<></>),
        sx: {
          height: '46px',
          '& input': {fontSize: '16px', fontWeight: '500', color: '#222222'},
          '&.MuiInputBase-root': {pl: '10px'},
          '& input::placeholder': {fontSize: '16px', fontWeight: '500', color: '#cccccc'},
        }
      }}
    />
  )
}