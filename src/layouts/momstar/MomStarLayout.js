import PropTypes from 'prop-types';
import GlobalNavigation from "../common/GlobalNavigation";
import {useSetRecoilState} from "recoil";
import {gnbActiveTabSelector} from "../../recoil/selector/common/gnbSelector";
import {useEffect} from "react";
import {GNB_TYPE} from "../../constant/common/GNB";

MomStarLayout.propTypes = {
  children: PropTypes.node,
};

export default function MomStarLayout({children}) {
  const setActiveTab = useSetRecoilState(gnbActiveTabSelector);
  useEffect(() => {
    setActiveTab(GNB_TYPE.RANKING)
  }, []);

  return (
    <GlobalNavigation>
      {children}
    </GlobalNavigation>
  );
}
