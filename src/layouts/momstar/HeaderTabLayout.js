import {AppBar, Box, Button, Container, Stack, Toolbar} from "@mui/material";
import PropTypes from "prop-types";
import BodyLayout from "./BodyLayout";
import {HEADER} from "../../config";
import {useRecoilState, useRecoilValue} from "recoil";
import {momStarActiveTabSelector, momStarIsScrollSelector,} from "../../recoil/selector/momstar/momstarSelector";
import {MOMSTAR_TABS} from "../../constant/momstar/Momstar";
import * as React from "react";
import {sxFixedCenterMainLayout} from "../main/MainLayout";
import {clickBorderNone} from "../../constant/common/Common";

HeaderTabLayout.propTypes = {
  children: PropTypes.any,
}

export default function HeaderTabLayout({children, handleTapClick}) {
  const [activeTap, setActiveTap] = useRecoilState(momStarActiveTabSelector)
  const isScroll = useRecoilValue(momStarIsScrollSelector)

  return (
    <>
      <AppBar sx={{boxShadow: 0, background: 'white', ...sxFixedCenterMainLayout}}>
        <Container maxWidth={'xs'} sx={{px: '20px'}}>
          <Toolbar disableGutters sx={{height: HEADER.H_MOBILE}}>
            <Stack flexDirection={'row'} columnGap={'15px'}>
              {MOMSTAR_TABS.map((tab, index) => (
                <Button
                  tabIndex={1}
                  disableFocusRipple
                  key={index}
                  variant="text"
                  onClick={() => handleTapClick(tab)}
                  sx={{
                    p: 0,
                    fontSize: '20px',
                    fontWeight: activeTap === tab ? 700 : 500,
                    lineHeight: '26px',
                    color: activeTap === tab ? '#222222' : '#CCCCCC',
                    '&:hover': {
                      boxShadow: 'none',
                      background: 'white',
                    },
                    '&.MuiButton-root': {
                      minWidth: 0
                    },
                    ...clickBorderNone
                  }}
                >
                  {tab}
                </Button>
              ))}
            </Stack>
          </Toolbar>
        </Container>
        {isScroll && <Box sx={{
          width: '100%',
          height: '3px',
          background: 'linear-gradient(180deg, #EEEEEE 0%, rgba(238, 238, 238, 0) 100%)'
        }}/>}
      </AppBar>
      <BodyLayout>
        {children}
      </BodyLayout>
    </>
  );
}