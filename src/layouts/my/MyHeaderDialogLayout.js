import {AppBar, Box, Container, IconButton, Toolbar, Typography} from "@mui/material";
import {styled} from "@mui/material/styles";
import {HEADER} from "../../config";
import {bgBlur} from "../../utils/cssStyles";
import MyBodyLayout from "./MyBodyLayout";
import {SvgBackIcon} from "../../constant/icons/icons";
import * as React from "react";
import {clickBorderNone} from "../../constant/common/Common";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../constant/common/AltString";

const ToolbarStyle = styled(Toolbar)(({theme}) => ({
  height: HEADER.H_MOBILE,
  zIndex: theme.zIndex.appBar + 1,
  ...bgBlur({
    color: theme.palette.background.default,
  }),
  transition: theme.transitions.create(['height'], {
    duration: theme.transitions.duration.shorter,
  }),
}));

export default function MyHeaderDialogLayout({
                                               children,
                                               title,
                                               isHeadTitle = false,
                                               onBack,
                                               showDelete = false,
                                               onRemove,
                                             }) {

  return (
    <>
      <AppBar sx={{boxShadow: 0, background: 'white'}}>
        <Container disableGutters maxWidth={'xs'} sx={{px: '20px'}}>
          <ToolbarStyle disableGutters sx={{'&.MuiToolbar-root': {minHeight: HEADER.H_MOBILE}}}>
            <Box sx={{display: 'flex', width: '100%', justifyContent: 'space-between', alignItems: 'center'}}>
              {
                onBack
                  ? <BackSection onBack={onBack} title={title}/>
                  : <HeadTitleSection isHeadTitle={isHeadTitle} title={title}/>
              }
              {showDelete && <DeleteSection onRemove={onRemove}/>}
            </Box>
          </ToolbarStyle>
        </Container>
      </AppBar>
      <MyBodyLayout>
        {children}
      </MyBodyLayout>
    </>
  );
}

function BackSection({onBack, title}) {
  return (
    <>
      <IconButton
        tabIndex={-1}
        disableFocusRipple
        onClick={onBack}
        sx={{
          m: '-8px',
          p: 0,
          color: '#222222',
          pl: '8px',
          mr: '12px',
          position: 'absolute',
          '&:hover': {
            boxShadow: 'none',
            backgroundColor: 'white',
          }
        }}
      >
        <SvgBackIcon alt={ALT_STRING.COMMON.BTN_BACK} tabIndex={1} onKeyDown={(e) => handleEnterPress(e, () => onBack)} sx={{...clickBorderNone}}/>
      </IconButton>
      <Typography align="center" sx={{color: '#000000', fontSize: '18px', fontWeight: '500', lineHeight: '24px', flexGrow: 1}}>
        {title}
      </Typography>
    </>
  )
}

function HeadTitleSection({isHeadTitle, title}) {
  return (
    <>
      {
        !isHeadTitle
          ? <SmallHeadTitleSection title={title}/>
          : <BigHeadTitleSection title={title}/>
      }
    </>
  )
}

function BigHeadTitleSection({title}) {
  return (
    <Typography sx={{color: '#000000', fontSize: '20px', fontWeight: '700', lineHeight: '26px', flexGrow: 1}}>
      {title}
    </Typography>
  )
}

function SmallHeadTitleSection({title}) {
  return (
    <Typography sx={{color: '#000000', fontSize: '18px', fontWeight: '500', lineHeight: '24px', flexGrow: 1}}>
      {title}
    </Typography>
  )
}

function DeleteSection({onRemove}) {
  return (
    <Typography
      tabIndex={1}
      onClick={onRemove}
      onKeyDown={(e) => handleEnterPress(e, onRemove)}
      sx={{color: '#ECA548', fontSize: '16px', fontWeight: '500', lineHeight: '24px', position: 'absolute', right: 0, ...clickBorderNone}}
    >
      삭제하기
    </Typography>
  )
}