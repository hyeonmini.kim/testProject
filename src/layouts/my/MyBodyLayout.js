import PropTypes from 'prop-types';
import {Box} from '@mui/material';
import {BEFU, HEADER} from '../../config';
// ----------------------------------------------------------------------

MyBodyLayout.propTypes = {
  sx: PropTypes.object,
  children: PropTypes.node,
};

export default function MyBodyLayout({children, sx, ...other}) {
  return (
    <Box
      sx={{
        flexGrow: 1,
        pt: `${HEADER.H_MOBILE}px`,
        pb: `${BEFU.B_MARGIN}px`,
        ...sx,
      }}
    >
      {children}
    </Box>
  );
}
