import GlobalNavigation from '../common/GlobalNavigation';
import AuthGuard from "../../auth/AuthGuard";
import {useSetRecoilState} from "recoil";
import {gnbActiveTabSelector} from "../../recoil/selector/common/gnbSelector";
import {GNB_TYPE} from "../../constant/common/GNB";
import {useEffect} from "react";

export default function MyLayout({children}) {
  const setActiveTab = useSetRecoilState(gnbActiveTabSelector);
  useEffect(() => {
    setActiveTab(GNB_TYPE.MY)
  }, [])

  return (
    <AuthGuard>
      <GlobalNavigation>
        {children}
      </GlobalNavigation>
    </AuthGuard>
  );
}
