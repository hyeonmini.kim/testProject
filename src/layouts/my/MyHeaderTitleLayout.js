import {AppBar, Box, Container, IconButton, Toolbar, Typography} from "@mui/material";
import {styled} from "@mui/material/styles";
import {HEADER} from "../../config";
import {bgBlur} from "../../utils/cssStyles";
import MyBodyLayout from "./MyBodyLayout";
import {SvgBackIcon, SvgCloseIcon} from "../../constant/icons/icons";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {useRecoilState} from "recoil";
import {
  myOpenChildrenSelector,
  myOpenMomStarSelector,
  myOpenMyInformationPopupSelector
} from "../../recoil/selector/my/popup/myPopupSelector";
import {sxFixedCenterMainLayout} from "../main/MainLayout";
import {clickBorderNone} from "../../constant/common/Common";
import {ALT_STRING} from "../../constant/common/AltString";

const ToolbarStyle = styled(Toolbar)(({theme}) => ({
  height: HEADER.H_MOBILE,
  zIndex: theme.zIndex.appBar + 1,
  ...bgBlur({
    color: theme.palette.background.default,
  }),
  transition: theme.transitions.create(['height'], {
    duration: theme.transitions.duration.shorter,
  }),
}));

export default function MyHeaderTitleLayout({
                                              title,
                                              onBack,
                                              showSetting,
                                              onSetting,
                                              children,
                                              isHeadTitle = false,
                                              showClose,
                                              onClose,
                                              showDelete = false,
                                              onRemove
                                            }) {

  const [open, setOpen] = useRecoilState(myOpenMyInformationPopupSelector);
  const [openMomStar, setOpenMomStar] = useRecoilState(myOpenMomStarSelector);
  const [openChildren, setOpenChildren] = useRecoilState(myOpenChildrenSelector)

  return (
    <>
      <AppBar sx={{boxShadow: 0, background: 'white', ...sxFixedCenterMainLayout}}>
        <Container disableGutters maxWidth={'xs'} sx={{px: '20px'}}>
          <ToolbarStyle disableGutters sx={{'&.MuiToolbar-root': {minHeight: HEADER.H_MOBILE}}}>
            <Box sx={{display: 'flex', width: '100%', justifyContent: 'space-between', alignItems: 'center'}}>
              {
                onBack
                  ? <BackSection onBack={onBack} title={title}/>
                  : <HeadTitleSection isHeadTitle={isHeadTitle} title={title}/>
              }
              {(showSetting && !open && !openMomStar && !openChildren) && <SettingSection onSetting={onSetting}/>}
              {showClose && <CloseSection onClose={onClose}/>}
              {showDelete && <DeleteSection onRemove={onRemove}/>}
            </Box>
          </ToolbarStyle>
        </Container>
      </AppBar>
      <MyBodyLayout>
        {children}
      </MyBodyLayout>
    </>
  );
}

function BackSection({onBack, title}) {
  return (
    <>
      <IconButton
        tabIndex={-1}
        disableFocusRipple
        onClick={onBack}
        sx={{
          m: '-8px',
          p: 0,
          color: '#222222',
          pl: '8px',
          mr: '12px',
          position: 'absolute',
          '&:hover': {
            boxShadow: 'none',
            backgroundColor: 'white',
          }
        }}
      >
        <SvgBackIcon alt={ALT_STRING.COMMON.BTN_BACK} tabIndex={1} sx={{...clickBorderNone}}/>
      </IconButton>
      <Typography align="center" sx={{color: '#000000', fontSize: '18px', fontWeight: '500', lineHeight: '24px', flexGrow: 1}}>
        {title}
      </Typography>
    </>
  )
}

function HeadTitleSection({isHeadTitle, title}) {
  return (
    <>
      {
        !isHeadTitle
          ? <SmallHeadTitleSection title={title}/>
          : <BigHeadTitleSection title={title}/>
      }
    </>
  )
}

function BigHeadTitleSection({title}) {
  return (
    <Typography sx={{color: '#000000', fontSize: '20px', fontWeight: '700', lineHeight: '26px', flexGrow: 1}}>
      {title}
    </Typography>
  )
}

function SmallHeadTitleSection({title}) {
  return (
    <Typography sx={{color: '#000000', fontSize: '18px', fontWeight: '500', lineHeight: '24px', flexGrow: 1}}>
      {title}
    </Typography>
  )
}

function SettingSection({onSetting}) {
  return (
    <IconButton
      tabIndex={-1}
      disableFocusRipple
      onClick={onSetting}
      sx={{m: '-8px', backgroundColor: "#FFFFFF", '&:hover': {boxShadow: 'none', backgroundColor: '#FFFFFF'}}}
    >
      <SvgCommonIcons alt={ALT_STRING.MY.ICON_SETTING} tabIndex={1} type={ICON_TYPE.SETTING} sx={{...clickBorderNone}}/>
    </IconButton>
  )
}

function CloseSection({onClose}) {
  return (
    <IconButton
      onClick={onClose}
      sx={{m: '-8px', backgroundColor: "#FFFFFF", '&:hover': {boxShadow: 'none', backgroundColor: '#FFFFFF'}}}
    >
      <SvgCloseIcon alt={ALT_STRING.COMMON.BTN_CLOSE} sx={{position: 'absolute', right: 0}}/>
    </IconButton>
  )
}

function DeleteSection({onRemove}) {
  return (
    <Typography
      onClick={onRemove}
      sx={{color: '#ECA548', fontSize: '16px', fontWeight: '500', lineHeight: '24px', position: 'absolute', right: 0}}
    >
      삭제하기
    </Typography>
  )
}