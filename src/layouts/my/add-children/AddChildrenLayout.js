import AuthGuard from "../../../auth/AuthGuard";
import PropTypes from "prop-types";

AddChildrenLayout.propTypes = {
  children: PropTypes.node,
}

export default function AddChildrenLayout({children}) {
  return (
    <AuthGuard>
      {children}
    </AuthGuard>
  )
}
