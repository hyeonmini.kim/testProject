import {AppBar, Box, Container, IconButton, Toolbar} from "@mui/material";
import {styled} from "@mui/material/styles";
import {HEADER} from "../../../config";
import {bgBlur} from "../../../utils/cssStyles";
import PropTypes from "prop-types";
import BodyLayout from "./BodyLayout";
import {SvgBackIcon, SvgCloseIcon} from "../../../constant/icons/icons";
import {sxFixedCenterMainLayout} from "../../main/MainLayout";
import {clickBorderNone} from "../../../constant/common/Common";
import {ALT_STRING} from "../../../constant/common/AltString";

const ToolbarStyle = styled(Toolbar)(({theme}) => ({
  height: HEADER.H_MOBILE,
  zIndex: theme.zIndex.appBar + 1,
  ...bgBlur({
    color: theme.palette.background.default,
  }),
  transition: theme.transitions.create(['height'], {
    duration: theme.transitions.duration.shorter,
  }),
}));

AddChildrenHeaderLayout.propTypes = {
  onBack: PropTypes.func,
  children: PropTypes.any,
}

export default function AddChildrenHeaderLayout({onBack, children}) {
  return (
    <>
      <AppBar sx={{boxShadow: 0, background: 'white', ...sxFixedCenterMainLayout}}>
        <Container disableGutters maxWidth={'xs'} sx={{px: '20px'}}>
          <ToolbarStyle disableGutters sx={{'&.MuiToolbar-root': {minHeight: HEADER.H_MOBILE}}}>
            <Box sx={{display: 'flex', width: '100%', justifyContent: 'space-between', alignItems: 'center'}}>
              <IconButton onClick={onBack} tabIndex={-1} disableFocusRipple sx={{
                m: '-8px',
                position: 'absolute',
                left: 0,
                '&:hover': {
                  backgroundColor: '#FFFFFF',
                },
              }}>
                <SvgBackIcon alt={ALT_STRING.COMMON.BTN_BACK} tabIndex={0} sx={{...clickBorderNone}}/>
              </IconButton>
            </Box>
          </ToolbarStyle>
        </Container>
      </AppBar>
      <BodyLayout>
        {children}
      </BodyLayout>
    </>
  );
}
