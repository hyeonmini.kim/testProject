import PropTypes from 'prop-types';
import GlobalNavigation from '../common/GlobalNavigation';
import {useEffect} from "react";
import {useResetRecoilState, useSetRecoilState} from "recoil";
import {gnbActiveTabSelector} from "../../recoil/selector/common/gnbSelector";
import {GNB_TYPE} from "../../constant/common/GNB";
import {useBefuAuthContext} from "../../auth/useAuthContext";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import {useRouter} from "next/router";
import {resetTokens} from "../../auth/utils";
import {TOAST_TYPE} from "../../constant/common/Common";
import {ERRORS, GET_FRONT_ERROR_MESSAGE} from "../../constant/common/Error";
import {accessTokenSelector, refreshTokenSelector} from "../../recoil/selector/auth/accessTokenSelector";
import {toastMessageSelector} from "../../recoil/selector/common/toastSelector";

GuestLayout.propTypes = {
  children: PropTypes.node,
};

export default function GuestLayout({children}) {
  const setActiveTab = useSetRecoilState(gnbActiveTabSelector)
  const resetAccessToken = useResetRecoilState(accessTokenSelector)
  const resetRefreshToken = useResetRecoilState(refreshTokenSelector)
  const {logout} = useBefuAuthContext()

  const router = useRouter()

  useEffect(() => {
    setActiveTab(GNB_TYPE.HOME)
  }, [])

  const checkRouterQuery = () => {
    resetTokens()
    resetAccessToken()
    resetRefreshToken()
    logout()
  }

  const {replace} = useRouter();
  const {isAuthenticated} = useBefuAuthContext();
  if (router?.query?.unauthorized) {
    checkRouterQuery()
    return (
      <GlobalNavigation>
        {children}
      </GlobalNavigation>
    )
  } else {
    if (isAuthenticated) {
      replace(PATH_DONOTS_PAGE.HOME)
    } else {
      return (
        <GlobalNavigation>
          {children}
        </GlobalNavigation>
      )
    }
  }
}
