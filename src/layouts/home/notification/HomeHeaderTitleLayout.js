import {AppBar, Box, Container, IconButton, Toolbar, Typography} from "@mui/material";
import {styled} from "@mui/material/styles";
import PropTypes from "prop-types";
import {bgBlur} from "../../../utils/cssStyles";
import Iconify from "../../../components/iconify";
import {HEADER} from "../../../config";
import HomeBodyLayout from "./HomeBodyLayout";
import {sxFixedCenterMainLayout} from "../../main/MainLayout";

const ToolbarStyle = styled(Toolbar)(({theme}) => ({
  height: HEADER.H_MOBILE,
  zIndex: theme.zIndex.appBar + 1,
  ...bgBlur({
    color: theme.palette.background.default,
  }),
  transition: theme.transitions.create(['height'], {
    duration: theme.transitions.duration.shorter,
  }),
}));


HomeHeaderTitleLayout.propTypes = {
  title: PropTypes.string,
  onBack: PropTypes.func,
  children: PropTypes.any,
}

export default function HomeHeaderTitleLayout({title, onBack, children}) {
  return (
    <>
      <AppBar sx={{boxShadow: 0, background: 'white', ...sxFixedCenterMainLayout}}>
        <Container maxWidth={'xs'} sx={{px: '20px'}}>
          <ToolbarStyle disableGutters sx={{'&.MuiToolbar-root': {minHeight: HEADER.H_MOBILE}}}>
            <Box sx={{display: 'flex', width: '100%', justifyContent: 'space-between'}}>
              {onBack ? (
                <>
                  <IconButton sx={{
                    p: 0,
                    color: '#222222',
                    mr: '12px',
                    position: 'absolute',
                    '&:hover': {
                      boxShadow: 'none',
                      backgroundColor: 'white',
                    }
                  }} onClick={onBack}>
                    <Iconify icon="eva:arrow-back-fill" width={24}/>
                  </IconButton>
                  <Typography align="center" sx={{color: '#000000', fontSize: '18px', fontWeight: '500', lineHeight: '24px', flexGrow: 1}}>
                    {title}
                  </Typography>
                </>
              ) : (
                <Typography sx={{color: '#000000', fontSize: '18px', fontWeight: '500', lineHeight: '24px', flexGrow: 1}}>
                  {title}
                </Typography>
              )}
            </Box>
          </ToolbarStyle>
        </Container>
      </AppBar>
      <HomeBodyLayout>
        {children}
      </HomeBodyLayout>
    </>
  );
}