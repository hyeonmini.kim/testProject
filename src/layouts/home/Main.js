import PropTypes from 'prop-types';
import {Box} from '@mui/material';
import {useSettingsContext} from '../../components/settings';

const SPACING = 8;

Main.propTypes = {
  sx: PropTypes.object,
  children: PropTypes.node,
};

export default function Main({children, sx, ...other}) {
  return (
    <>
      {children}
    </>
  );
}
