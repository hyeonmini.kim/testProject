import PropTypes from 'prop-types';
import AuthGuard from "../../auth/AuthGuard";

LabLayout.propTypes = {
  children: PropTypes.node,
};

export default function LabLayout({children}) {
  return (
    <AuthGuard>
      {children}
    </AuthGuard>
  );
}
