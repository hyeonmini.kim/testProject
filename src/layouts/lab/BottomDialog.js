import {MainDialogLayout} from "../main/MainLayout";
import {DialogContent, Fade, Slide} from "@mui/material";
import {Z_INDEX} from "../../constant/common/ZIndex";
import * as React from "react";

const transition = React.forwardRef(
  function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  }
);

export default function BottomDialog({open, onClose, children}) {
  return (
    <MainDialogLayout
      open={open}
      onClose={onClose}
      TransitionComponent={transition}
      disableScrollLock={true}
      fullWidth
      PaperProps={{
        style: {
          margin: 0,
          borderRadius: '16px 16px 0 0',
          position: 'fixed',
          bottom: 0,
          backgroundColor: '#FFFFFF',
        },
      }}
      sx={{
        zIndex: Z_INDEX.BOTTOM_DIALOG, '& .MuiDialog-paper': {mx: 0}
      }}
    >
      <DialogContent sx={{margin: '0 20px 6px 20px', padding: 0, textAlign: '-webkit-center'}}>
        {children}
      </DialogContent>
    </MainDialogLayout>
  )
}