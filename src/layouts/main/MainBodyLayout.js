import {Box} from "@mui/material";
import {BEFU, HEADER} from "../../config";

export default function MainBodyLayout({children, sx, ...other}) {
  return (
    <Box
      sx={{
        flexGrow: 1,
        pt: `${HEADER.H_MOBILE}px`,
        pb: `${BEFU.B_MARGIN}px`,
        ...sx,
      }}
    >
      {children}
    </Box>
  );
}
