import {Box, Dialog, Typography} from "@mui/material";
import {hideScrollbarY} from "../../utils/cssStyles";
import useResponsive from "../../hooks/useResponsive";
import Head from "next/head";
import {MAIN_DESKTOP, MAIN_TITLE} from "../../constant/main/Main";
import {useRouter} from "next/router";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import Image from "../../components/image";
import {PATH_DONOTS_PAGE, PATH_DONOTS_STORE_URL} from "../../routes/paths";
import {clickBorderNone} from "../../constant/common/Common";
import {useSetRecoilState} from "recoil";
import {toastMessageSelector} from "../../recoil/selector/common/toastSelector";
import * as React from "react";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../constant/common/AltString";

export default function MainLayout({children, title, mainRef}) {
  const isDesktop = useResponsive('up', 'md');
  return (
    <>
      <Head>
        <title>
          {MAIN_TITLE.HEADER(title)}
        </title>
      </Head>
      <Box
        component='main'
        sx={{
          width: '100%',
          height: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          background: '#FAFAFA',
          ...hideScrollbarY,
        }}
      >
        <Box component='aside'>
          {isDesktop && <DesktopSection/>}
        </Box>
        <Box
          ref={mainRef}
          component='section'
          sx={{
            position: 'relative',
            width: '100%',
            maxWidth: '440px',
            height: '100%',
            background: '#fff',
            boxShadow: '-1px 0 0 #E6E6E6, 1px 0 0 #E6E6E6',
            overflow: 'auto',
            scrollbarWidth: 'none',
            '::-webkit-scrollbar': {display: 'none'},
          }}>
          {children}
        </Box>
      </Box>
    </>
  );
}

export function MainDialogLayout({children, bgcolor = '#FFF', sx, mainRef, ...other}) {
  return (
    <Dialog {...other} sx={{...sx, ...sxFixedCenterMainLayout}}>
      <Box
        ref={mainRef}
        sx={{
          position: 'relative',
          width: '100%',
          maxWidth: '440px',
          height: '100%',
          background: bgcolor,
          overflow: 'auto',
          scrollbarWidth: 'none',
          '::-webkit-scrollbar': {display: 'none'},
        }}
      >
        {children}
      </Box>
    </Dialog>
  )
}

export const sxFixedCenterMainLayout = {
  width: '100%',
  maxWidth: '440px',
  left: '50%',
  right: 'auto',
  transform: 'translateX(-50%)',
  '@media (min-width: 820px)': {
    transform: 'translateX(-6.8%)'
  },
}

function DesktopSection() {
  const router = useRouter()
  const {asPath} = useRouter()
  const setToastMessage = useSetRecoilState(toastMessageSelector)

  const handlePlayStore = () => {
    router.push(PATH_DONOTS_STORE_URL.ANDROID)
  }

  const handleAppStore = () => {
    router.push(PATH_DONOTS_STORE_URL.IOS)
  }

  return (
    <Box
      sx={{
        width: '360px',
        minWidth: '360px',
        height: 'auto',
        pl: '30px',
        mr: '20px',
        ...hideScrollbarY,
      }}
    >
      <Box sx={{mb: '136px'}}>
        <Box sx={{width: '60px', mb: '16px'}}>
          <SvgCommonIcons type={ICON_TYPE.LOGO_CHOCO_46PX} alt={ALT_STRING.MAIN.DONOTS_LOGO} sx={{width: '60px', height: '60px'}}/>
        </Box>
        <Typography
          sx={{
            color: '#ECA548',
            fontSize: '24px',
            fontWeight: 700,
            lineHeight: '34.75px',
            letterSpacing: '-1px'
          }}
        >
          {MAIN_DESKTOP.TITLE1}
        </Typography>
        <Box sx={{display: 'flex'}}>
          <Typography
            sx={{
              color: '#000000',
              fontSize: '24px',
              fontWeight: 700,
              lineHeight: '34.75px',
              letterSpacing: '-1px'
            }}
          >
            {MAIN_DESKTOP.TITLE2}
          </Typography>
          <SvgCommonIcons type={ICON_TYPE.LOGO_TEXT} sx={{width: '109px', ml: '10px', mt: '5px'}}/>
        </Box>
      </Box>
      <Box className="left_bottom">
        <Typography
          sx={{
            color: '#000000',
            fontSize: '14px',
            lineHeight: '16.94px',
            letterSpacing: '-1px'
          }}
        >
          {MAIN_DESKTOP.INFO1}
        </Typography>
        <Typography
          sx={{
            color: '#000000',
            fontSize: '14px',
            lineHeight: '16.94px',
            letterSpacing: '-1px'
          }}
        >
          {MAIN_DESKTOP.INFO2}
        </Typography>
        <Box
          sx={{
            width: '100%',
            marginTop: '11px',
            marginBottom: '20px',
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center'
          }}
        >
          <SvgCommonIcons
            tabIndex={asPath?.includes(PATH_DONOTS_PAGE.GUEST) ? 0 : -1}
            type={ICON_TYPE.STORE_ANDROID}
            alt={ALT_STRING.MAIN.AOS_DOWNLOAD}
            sx={{width: '137px', height: '41px', mr: '8px', ...clickBorderNone}}
            onClick={handlePlayStore}
            onKeyDown={(e) => handleEnterPress(e, handlePlayStore)}
          />
          <SvgCommonIcons
            tabIndex={asPath?.includes(PATH_DONOTS_PAGE.GUEST) ? 0 : -1}
            type={ICON_TYPE.STORE_IOS}
            alt={ALT_STRING.MAIN.IOS_DOWNLOAD}
            sx={{width: '137px', height: '41px', ...clickBorderNone}}
            onClick={handleAppStore}
            onKeyDown={(e) => handleEnterPress(e, handleAppStore)}
          />
        </Box>
        <Image alt={ALT_STRING.MAIN.APP_QR_CODE} src="/assets/images/main/qr.png" isLazy={false} style={{width: '80px'}}/>
      </Box>
    </Box>
  )
}
