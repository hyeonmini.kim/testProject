import {
  bottomDialogHeaderErrorToastOption,
  bottomDialogHeaderToastOption,
  bottomHeaderToastOption,
  bottomSystemErrorOption, desktopBottomHeaderToastOption
} from "../../utils/toast";
import {useRecoilValue, useResetRecoilState} from "recoil";
import {useEffect, useState} from "react";
import {toastMessageSelector} from "../../recoil/selector/common/toastSelector";
import {TOAST_ENV, TOAST_TYPE} from "../../constant/common/Common";
import Snackbar from '@mui/material/Snackbar';
import {Z_INDEX} from "../../constant/common/ZIndex";
import {SnackbarContent} from "@mui/material";
import {sxFixedCenterMainLayout} from "../main/MainLayout";

export default function ToastProvider({}) {
  const toastMessage = useRecoilValue(toastMessageSelector)
  const resetToastMessage = useResetRecoilState(toastMessageSelector)

  const [message, setMessage] = useState('')
  const [show, setShow] = useState(false)
  const [option, setOption] = useState({})


  useEffect(() => {
    if (toastMessage.message) {
      let type = TOAST_ENV.MOBILE
      setMessage(toastMessage.message)
      switch (toastMessage.type) {
        case TOAST_TYPE.BOTTOM_HEADER:
          setOption(bottomHeaderToastOption)
          break;
        case TOAST_TYPE.BOTTOM_DIALOG_HEADER:
          setOption(bottomDialogHeaderToastOption)
          break;
        case TOAST_TYPE.BOTTOM_DIALOG_HEADER_ERROR:
          setOption(bottomDialogHeaderErrorToastOption)
          break;
        case TOAST_TYPE.BOTTOM_SYSTEM_ERROR:
          setOption(bottomSystemErrorOption)
          break;
        case TOAST_TYPE.DESKTOP_BOTTOM_HEADER:
          type = 'D'
          setOption(desktopBottomHeaderToastOption)
          break;
        default:
          break;
      }
      setShow(type)

      resetToastMessage()
    }
  }, [toastMessage])

  const handleClose = (event, reason) => {
    if (reason === 'timeout') {
      setShow(null)
    }
  }

  return (
    <Snackbar
      key={toastMessage.type}
      open={!!show}
      onClose={handleClose}
      autoHideDuration={option.duration}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center'
      }}
      message={message}
      sx={show === TOAST_ENV.DESKTOP ? [{
        zIndex: Z_INDEX.TOAST,
        ...option.sx,
        px: '20px',
      }] : [{
        zIndex: Z_INDEX.TOAST,
        ...option.sx,
        ...sxFixedCenterMainLayout,
        px: '20px',
      }]}
    >
      <SnackbarContent
        style={{backgroundColor: option.backgroundColor}}
        message={message}
        sx={{width: '100%'}}
      />
    </Snackbar>
  );
}