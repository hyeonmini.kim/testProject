import useChannel from "../../hooks/useChannel";
import {useEffect} from "react";

export default function NativeProvider({children}) {
  const {openNativeInit} = useChannel()

  useEffect(() => {
    openNativeInit()
  }, []);

  return children
}