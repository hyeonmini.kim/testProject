import {useEffect, useRef} from "react";
import {getFormDataLog, logger} from "../../utils/loggingUtils";
import axios from "axios";
import {useRecoilState, useResetRecoilState} from "recoil";
import {accessTokenSelector, refreshTokenSelector} from "../../recoil/selector/auth/accessTokenSelector";
import ssgAxios from "../../api/axios/ssgAxios";
import {getRefreshTokenMutate, getRefreshTokenMutateByCookie} from "../../api/authApi";
import {resetTokens, setTokens} from "../../auth/utils";
import {isNative} from "../../utils/envUtils";
import {useRouter} from "next/router";
import {ERRORS, GET_ERROR_CODE} from "../../constant/common/Error";
import {initAxiosSelector} from "../../recoil/selector/auth/initializeSelector";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import {getHackleClientId} from "../../utils/hackleUtils";

const authAxios = axios.create({
  timeout: 15 * 1000,
})

const noAuthAxios = axios.create({
  timeout: 15 * 1000,
})

export const authProvider = (isSSG) => {
  return isSSG ? ssgAxios : authAxios
}

export const noAuthProvider = (isSSG) => {
  return isSSG ? ssgAxios : noAuthAxios
}

export const getProvider = (isSSG, isAuth = false) => {
  if (isSSG) {
    return ssgAxios
  } else if (isAuth) {
    return authAxios
  }
  return noAuthAxios
}

export const queryOption = {
  retry: 1,
  retryDelay: 3000,
}

export const mutationOption = {
  retry: (failureCount, error) => {
    console.log('mutationOption error count', failureCount)
    if (failureCount > 0) return false
    return error.response?.status === 401
  },
  retryDelay: 3000,
}

export default function AxiosProvider({children}) {
  const accessTokenRef = useRef(null);
  const refreshTokenRef = useRef(null);
  const clientIdRef = useRef(null);
  const isLock = useRef(false);

  const [accessToken, setAccessToken] = useRecoilState(accessTokenSelector);
  const [refreshToken, setRefreshToken] = useRecoilState(refreshTokenSelector);
  const resetAccessToken = useResetRecoilState(accessTokenSelector);
  const resetRefreshToken = useResetRecoilState(refreshTokenSelector);

  const [isInitAxios, setIsInitAxios] = useRecoilState(initAxiosSelector);

  const router = useRouter();
  const {mutateAsync: mutateGetAccessToken} = getRefreshTokenMutate()
  const {mutateAsync: mutateGetAccessTokenByCookie} = getRefreshTokenMutateByCookie()

  const printToken = (token) => {
    if (!token || token?.length < 4) {
      return "no token"
    } else if (token?.length > 3) {
      return token.slice(-3)
    }
  }

  useEffect(() => {
    clientIdRef.current = getHackleClientId()
  }, []);

  useEffect(() => {
    logger.dump("#### 0-1.Change Pre Access Token:", printToken(accessTokenRef?.current))
    logger.dump("#### 0-1.Change Access Token:", printToken(accessToken))

    accessTokenRef.current = accessToken
  }, [accessToken]);

  useEffect(() => {
    logger.dump("#### 0-2.Change PreRefresh Token:", printToken(refreshTokenRef?.current))
    logger.dump("#### 0-2.Change Refresh Token:", printToken(refreshToken))
    refreshTokenRef.current = refreshToken
  }, [refreshToken]);

  useEffect(() => {
    const requestInterceptor = (isAuth = false, config) => {
      if (isAuth && accessTokenRef) {
        logger.dump("## Token", printToken(accessTokenRef?.current))
        config.headers.Authorization = 'Bearer ' + accessTokenRef?.current
      }
      config.headers.client_id = clientIdRef?.current
      const data = config?.data
      logger.info("## Request", JSON.parse(JSON.stringify({
        url: config?.url,
        header: config?.headers,
        params: config?.params,
        data: data instanceof FormData ? getFormDataLog(data) : data
      })))
      logger.dump("Request", {
        url: config?.url,
        params: config?.params,
      })
      return config;
    }

    const requestErrorInterceptor = (error) => {
      logger.error('Request error ', error);
      return Promise.reject(error);
    }

    const responseInterceptor = async (response) => {
      logger.info("Response", JSON.parse(JSON.stringify({
        status: response?.status,
        url: response?.request?.responseURL,
        header: response?.headers,
        data: response?.data?.databody,
      })))
      logger.dump("Response", {
        url: response?.request?.responseURL,
        status: response?.status,
      })
      return response?.data;
    }

    const retryAuthRequest = async (error, token) => {
      logger.dump("#### 8.Retry Api")

      isLock.current = false
      error.config.headers.Authorization = 'Bearer ' + token
      return await authAxios({
        ...error?.config,
        headers: error?.config.headers.toJSON()
      })
    }

    const rejectError = async (error) => {
      isLock.current = false
      return Promise.reject(error);
    }

    const authorizedError = (error) => {
      resetTokens()
      resetAccessToken()
      resetRefreshToken()
      isLock.current = false

      router.replace({
        pathname: PATH_DONOTS_PAGE.GUEST,
        query: {
          unauthorized: true,
        }
      })

      return Promise.reject(error)
    }

    const authorizedWebError = (error) => {
      resetAccessToken()
      resetRefreshToken()
      isLock.current = false

      router.replace({
        pathname: PATH_DONOTS_PAGE.GUEST,
        query: {
          unauthorized: true,
        }
      })

      return Promise.reject(error)
    }

    const responseErrorInterceptor = async (error) => {
      logger.dump("Get Error Code", {
        axios: error?.code,
        status: error?.response?.status,
        code: error?.response?.data?.code
      })

      if (GET_ERROR_CODE(error) === ERRORS.INVALID_ACCESS_TOKEN) {
        logger.dump("#### 1.Invalid Access Token")
        if (isLock.current) {
          logger.dump("#### 999. Return by Lock")
          return Promise.reject(error);
        }
        logger.dump("#### 2.Refresh Start with Lock")

        isLock.current = true
        if (isNative()) {
          try {

            logger.dump("#### 3.Refresh Api Call with Refresh Token:", printToken(refreshTokenRef?.current))
            const result = await mutateGetAccessToken({
              refreshToken: refreshTokenRef?.current
            })
            if (result?.token && result?.refreshToken) {
              logger.dump("#### 4-1.Refresh Api Success")
              logger.dump("#### 5-1.Native Access Token:", printToken(result?.token))
              logger.dump("#### 5-2.Native Refresh Token:", printToken(result?.refreshToken))

              setAccessToken(result?.token)
              setRefreshToken(result?.refreshToken)
              setTokens(result?.token, result?.refreshToken)
              return retryAuthRequest(error, result?.token)
            }
            logger.dump("#### 4-2.Refresh Api Result No Token, Logout")
            await authorizedError(error)
          } catch (error) {
            logger.dump("#### 5.Refresh Api Error, Logout", {
              axios: error?.code,
              status: error?.response?.status,
              code: error?.response?.data?.code
            })
            await authorizedError(error)
          }
        } else {
          try {
            const result = await mutateGetAccessTokenByCookie(null)
            if (result?.token) {
              setAccessToken(result?.token)
              return retryAuthRequest(error, result?.token)
            }
            return authorizedWebError(error)
          } catch (error) {
            return authorizedWebError(error)
          }
        }
      } else {
        logger.error('## Response error ', JSON.parse(JSON.stringify({
          axios: error?.code,
          status: error?.response?.status,
          url: error?.config?.url,
          header: error?.response?.headers ? error?.response?.headers : error?.response?.header,
          data: error?.response?.data,
        })))
        logger.dump("Response", {
          axios: error?.code,
          status: error?.response?.status,
          url: error?.config?.url,
        })
        return Promise.reject(error);
      }
    }

    if (authAxios.interceptors.request.handlers.length < 1) {
      authAxios.interceptors.request.use((config) => requestInterceptor(true, config), requestErrorInterceptor)
      authAxios.interceptors.response.use(responseInterceptor, responseErrorInterceptor)
      authAxios.defaults.withCredentials = true
      noAuthAxios.interceptors.request.use((config) => requestInterceptor(false, config), requestErrorInterceptor)
      noAuthAxios.interceptors.response.use(responseInterceptor, responseErrorInterceptor)
      noAuthAxios.defaults.withCredentials = true
      setIsInitAxios(true)
    }
  }, [])

  return children
}


