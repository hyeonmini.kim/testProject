import IFrameDialog from "../../components/common/IFrameDialog";
import * as React from "react";
import {useEffect, useState} from "react";
import {useRecoilState} from "recoil";
import {iFrameOpenSelector, iFrameUrlSelector} from "../../recoil/selector/common/iFrameSelector";

export default function IFrameProvider() {
  const [iframeUrl, setIframeUrl] = useRecoilState(iFrameUrlSelector)
  const [iFrameOpen, setIFrameOpen] = useRecoilState(iFrameOpenSelector)
  const [newIframeUrl, setNewIframeUrl] = useState(null)

  useEffect(() => {
    if (iframeUrl) {
      setNewIframeUrl(iframeUrl)
      setIFrameOpen(true)
      setIframeUrl('')
    }
  }, [iframeUrl])

  return (
    <IFrameDialog
      open={iFrameOpen}
      url={newIframeUrl}
      onBack={() => setIFrameOpen(false)}
    />
  )
}