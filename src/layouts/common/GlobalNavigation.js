import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import {PATH_DONOTS_PAGE} from '../../routes/paths';
import {gnbActiveTabSelector} from "../../recoil/selector/common/gnbSelector";
import {GNB_TYPE} from "../../constant/common/GNB";
import {Box, Container, Stack, SvgIcon, Typography} from "@mui/material";
import {forMyPageEntrySelector} from "../../recoil/selector/auth/userSelector";
import {Z_INDEX} from "../../constant/common/ZIndex";
import {ICON_COLOR, SvgHomeIcon, SvgMyIcon, SvgSearchIcon, SvgStarIcon} from "../../constant/icons/icons";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {GUEST_LOGIN_POPUP_TEXT, HOME_GNB} from "../../constant/home/Gnb";
import {sxFixedCenterMainLayout} from "../main/MainLayout";

import {useRouter} from 'next/router';
import {useRecoilState, useRecoilValue, useSetRecoilState} from "recoil";
import usePreRecipeCreate from "../../hooks/usePreRecipeCreate";
import {authenticateSelector} from "../../recoil/selector/auth/authenticateSelector";
import useStackNavigation from "../../hooks/useStackNavigation";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {ALT_STRING} from "../../constant/common/AltString";
import {clickBorderNone, COMMON_DIALOG_TYPE} from "../../constant/common/Common";
import {dialogSelector} from "../../recoil/selector/common/dialogSelector";

export default function GlobalNavigation({children}) {
  const router = useRouter()
  const {handleRecipe} = usePreRecipeCreate()
  const [activeTab, setActiveTab] = useRecoilState(gnbActiveTabSelector);
  const user = useRecoilValue(forMyPageEntrySelector);
  const [isAuthenticated, setIsAuthenticated] = useRecoilState(authenticateSelector);
  const setDialogMessage = useSetRecoilState(dialogSelector)

  const {clearStack} = useStackNavigation()

  const handleActiveTab = (event, value) => {
    if (activeTab === value) return
    clearStack()
    switch (value) {
      case GNB_TYPE.HOME:
        setActiveTab(value)
        if (isAuthenticated) {
          router.push(PATH_DONOTS_PAGE.HOME);
          setHackleTrack(HACKLE_TRACK.CLICK_GNB_HOME)
        } else {
          router.push(PATH_DONOTS_PAGE.GUEST)
        }
        break
      case GNB_TYPE.RANKING:
        setActiveTab(value)
        router.push(PATH_DONOTS_PAGE.RANKING);
        if (isAuthenticated) {
          setHackleTrack(HACKLE_TRACK.CLICK_GNB_RANKING)
        } else {
          setHackleTrack(HACKLE_TRACK.CLICK_PUBLIC_GNB_RANKING)
        }
        break
      case GNB_TYPE.CREATE:
        handleRecipe()
        if (isAuthenticated) {
          setHackleTrack(HACKLE_TRACK.CLICK_GNB_WRITE)
        } else {
          setHackleTrack(HACKLE_TRACK.CLICK_PUBLIC_GNB_WRITE)
        }
        break
      case GNB_TYPE.SEARCH:
        setActiveTab(value)
        router.push(PATH_DONOTS_PAGE.RECIPE.SEARCH);
        if (isAuthenticated) {
          setHackleTrack(HACKLE_TRACK.CLICK_GNB_SEARCH)
        } else {
          setHackleTrack(HACKLE_TRACK.CLICK_PUBLIC_GNB_SEARCH)
        }
        break
      case GNB_TYPE.MY:
        if (isAuthenticated) {
          setActiveTab(value)
          router.push(PATH_DONOTS_PAGE.MY.SELF);
          setHackleTrack(HACKLE_TRACK.CLICK_GNB_MY)
        } else {
          setHackleTrack(HACKLE_TRACK.CLICK_PUBLIC_GNB_MY)
          setDialogMessage({
            type: COMMON_DIALOG_TYPE?.TWO_BUTTON,
            message: GUEST_LOGIN_POPUP_TEXT?.TITLE,
            leftButtonProps: {text: GUEST_LOGIN_POPUP_TEXT?.BUTTON_TEXT?.CLOSE},
            rightButtonProps: {
              text: GUEST_LOGIN_POPUP_TEXT?.BUTTON_TEXT?.GO_LOGIN, onClick: () => {
                router?.push(PATH_DONOTS_PAGE.AUTH.LOGIN)
              }
            },
          })
        }
        break
    }
  }

  return (
    <>
      {children}
      <Box position='fixed' sx={{zIndex: Z_INDEX.GNB, bottom: 0, ...sxFixedCenterMainLayout}} component='nav'>
        <Container maxWidth={'xs'}>
          <GNBBackgroud/>
          <BottomNavigation
            sx={{backgroundColor: 'transparent', height: '65px'}}
            value={activeTab}
            onChange={(event, value) => handleActiveTab(event, value)}
          >
            <BottomNavigationAction
              tabIndex={95}
              focusRipple={false}
              icon={
                <Box sx={{flexDirection: 'column', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                  {
                    activeTab === GNB_TYPE.HOME
                      ? <SvgHomeIcon color={ICON_COLOR.PRIMARY}/>
                      : <SvgHomeIcon/>
                  }
                  <Typography sx={{
                    mt: '3px',
                    fontSize: '12px',
                    fontWeight: activeTab === GNB_TYPE.HOME ? 700 : 500,
                    lineHeight: '12px',
                    color: activeTab === GNB_TYPE.HOME ? '#ECA548' : '#CCCCCC'
                  }}>
                    {HOME_GNB.HOME}
                  </Typography>
                </Box>
              }
              sx={{
                px: '10px',
                minWidth: '50px',
                backgroundColor: 'transparent',
                borderRadius: 50,
                ...clickBorderNone
              }}
            />
            <BottomNavigationAction
              tabIndex={96}
              focusRipple={false}
              icon={
                <Box sx={{flexDirection: 'column', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                  {
                    activeTab === GNB_TYPE.RANKING
                      ? <SvgStarIcon color={ICON_COLOR.PRIMARY}/>
                      : <SvgStarIcon/>
                  }
                  <Typography sx={{
                    mt: '3px',
                    fontSize: '12px',
                    fontWeight: activeTab === GNB_TYPE.RANKING ? 700 : 500,
                    lineHeight: '12px',
                    color: activeTab === GNB_TYPE.RANKING ? '#ECA548' : '#CCCCCC'
                  }}>
                    {HOME_GNB.MOMSTAR}
                  </Typography>
                </Box>
              }
              sx={{
                pl: '20px', pr: '0px',
                minWidth: '50px',
                backgroundColor: 'transparent',
                borderRadius: 50,
                ...clickBorderNone
              }}
            />

            <BottomNavigationAction
              tabIndex={97}
              focusRipple={false}
              icon={<SvgCommonIcons alt={ALT_STRING.GNB.CREATE_RECIPE} type={ICON_TYPE.RECIPE_CREATE}/>}
              sx={{
                px: 0,
                top: -35,
                minWidth: '96px',
                maxWidth: '96px',
                borderRadius: 100,
                ...clickBorderNone
              }}
            />
            <BottomNavigationAction
              tabIndex={98}
              focusRipple={false}
              icon={
                <Box sx={{flexDirection: 'column', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                  {
                    activeTab === GNB_TYPE.SEARCH
                      ? <SvgSearchIcon alt={''} color={ICON_COLOR.PRIMARY}/>
                      : <SvgSearchIcon alt={''} color={ICON_COLOR.GREY}/>
                  }
                  <Typography sx={{
                    mt: '3px',
                    fontSize: '12px',
                    fontWeight: activeTab === GNB_TYPE.SEARCH ? 700 : 500,
                    lineHeight: '12px',
                    color: activeTab === GNB_TYPE.SEARCH ? '#ECA548' : '#CCCCCC'
                  }}>
                    {HOME_GNB.SEARCH}
                  </Typography>
                </Box>
              }
              sx={{
                pl: '0px', pr: '20px',
                minWidth: '50px',
                backgroundColor: 'transparent',
                borderRadius: 50,
                ...clickBorderNone
              }}
            />
            <BottomNavigationAction
              tabIndex={99}
              focusRipple={false}
              icon={
                <Box sx={{flexDirection: 'column', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                  {
                    activeTab === GNB_TYPE.MY
                      ? <SvgMyIcon color={ICON_COLOR.PRIMARY}/>
                      : <SvgMyIcon/>
                  }
                  <Typography sx={{
                    mt: '3px',
                    fontSize: '12px',
                    fontWeight: activeTab === GNB_TYPE.MY ? 700 : 500,
                    lineHeight: '12px',
                    color: activeTab === GNB_TYPE.MY ? '#ECA548' : '#CCCCCC'
                  }}>
                    {HOME_GNB.MY}
                  </Typography>
                </Box>
              }
              sx={{
                px: '10px',
                minWidth: '50px',
                backgroundColor: 'transparent',
                borderRadius: 50,
                ...clickBorderNone
              }}
            />
          </BottomNavigation>
        </Container>
      </Box>
    </>
  );
}

function GNBBackgroud() {
  return (
    <Stack
      direction={'row'}
      sx={{
        position: 'absolute',
        mx: 'auto',
        bottom: 0,
        left: 0,
        right: 0,
        height: 67,
        width: '100%',
        maxWidth: '440px',
        backgroundColor: 'transparent'
      }}
    >
      <Box
        sx={{
          flexGrow: 1,
          backgroundColor: 'white',
          borderTopLeftRadius: 30,
          borderTopRightRadius: 25,
          boxShadow: '-4px -4px 40px rgba(149, 168, 196, 0.5)',
        }}
      />
      <Box sx={{
        pt: '-10px',
        ml: '-5px',
        mr: '-6px',
        width: 70,
        position: 'relative',
      }}>

        <Box
          sx={{
            position: 'absolute',
            bottom: -39,
            left: -57,
            objectFit: 'cover',
          }}
        >
          <SvgIcon role={'img'} aria-hidden={true} viewBox="0 0 183 150" sx={{width: '183px', height: '150px'}}>
            <g>
              <path
                d="M126.026 56.7795C118.12 67.0927 111.141 81.68 91 81.68C70.8949 81.68 64.5071 67.1452 56.8715 56.8353C53.1526 51.8139 47.8124 50 41.5638 50H40V120H91H143V50H141.396C135.148 50 129.828 51.8203 126.026 56.7795Z"
                fill="white"/>
            </g>
          </SvgIcon>
        </Box>
      </Box>
      <Box
        sx={{
          flexGrow: 1,
          boxShadow: '4px -4px 40px rgba(149, 168, 196, 0.5)',
          backgroundColor: 'white',
          borderTopLeftRadius: 25,
          borderTopRightRadius: 30,
        }}
      />
    </Stack>
  )
}