import {AppBar, Box, IconButton, Toolbar, Typography} from "@mui/material";
import {SvgBackIcon, SvgCloseIcon} from "../../constant/icons/icons";
import {styled} from "@mui/material/styles";
import {HEADER} from "../../config";
import {bgBlur} from "../../utils/cssStyles";
import IFrameBodyLayout from "./IFrameBodyLayout";
import {clickBorderNone} from "../../constant/common/Common";
import {ALT_STRING} from "../../constant/common/AltString";

const ToolbarStyle = styled(Toolbar)(({theme}) => ({
  height: HEADER.H_MOBILE,
  zIndex: theme.zIndex.appBar + 1,
  ...bgBlur({
    color: theme.palette.background.default,
  }),
  transition: theme.transitions.create(['height'], {
    duration: theme.transitions.duration.shorter,
  }),
}));

export default function IFrameLayout({backTabIndex, noToolbar, title, isLeft, onBack, onClose, children}) {
  return (
    <>
      {
        noToolbar ? (
          <IFrameToolbarLayout backTabIndex={backTabIndex} noToolbar title={title} isLeft={isLeft} onBack={onBack} onClose={onClose}/>
        ) : (
          <AppBar sx={{boxShadow: 0, background: noToolbar ? 'transparent' : 'white'}}>
            <ToolbarStyle
              disableGutters
              sx={{
                background: noToolbar ? 'transparent' : 'white',
                '&.MuiToolbar-root': {minHeight: HEADER.H_MOBILE}
              }}
            >
              <IFrameToolbarLayout backTabIndex={backTabIndex} title={title} isLeft={isLeft} onBack={onBack} onClose={onClose}/>
            </ToolbarStyle>
          </AppBar>
        )}
      <IFrameBodyLayout noToolbar={noToolbar}>
        {children}
      </IFrameBodyLayout>
    </>
  );
}

function IFrameToolbarLayout({backTabIndex, noToolbar, title, isLeft, onBack, onClose}) {
  return (
    <Box sx={{display: 'flex', width: '100%', justifyContent: 'space-between', alignItems: 'center'}}>
      {onBack && (
        <IconButton
          tabIndex={-1}
          disableFocusRipple
          onClick={onBack}
          sx={{
            position: 'fixed',
            top: '12px',
            left: '12px',
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: 'transparent',
            },
          }}
        >
          <SvgBackIcon alt={ALT_STRING.COMMON.BTN_BACK} tabIndex={backTabIndex} sx={{...clickBorderNone}}/>
        </IconButton>
      )}
      {!noToolbar && (
        <Box sx={{flexGrow: 1}}>
          <Typography
            align={isLeft ? 'left' : 'center'}
            sx={{
              px: '50px',
              fontSize: '20px',
              fontWeight: '500',
              lineHeight: '24px',
              color: '#000000',
              wordBreak: 'break-all',
            }}
          >
            {title}
          </Typography>
        </Box>
      )}
      {onClose && (
        <IconButton
          tabIndex={-1}
          disableFocusRipple
          onClick={onClose}
          sx={{
            position: 'fixed',
            top: '12px',
            right: '12px',
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: 'transparent',
            },
          }}>
          <SvgCloseIcon alt={ALT_STRING.COMMON.BTN_CLOSE} tabIndex={backTabIndex} sx={{...clickBorderNone}}/>
        </IconButton>
      )}
    </Box>
  )
}