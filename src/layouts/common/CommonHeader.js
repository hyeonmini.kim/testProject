import {HEADER} from "../../config";
import {Container, IconButton, Stack, Typography} from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import CloseIcon from '@mui/icons-material/Close';
import {commonHeaderBackState, commonHeaderCloseState} from "../../recoil/atom/common/commonHeader";
import {useRecoilValue} from "recoil";
import PropTypes from "prop-types";
import {sxFixedCenterMainLayout} from "../main/MainLayout";
import {clickBorderNone} from "../../constant/common/Common";
import {ALT_STRING} from "../../constant/common/AltString";

CommonHeader.propTypes = {
  handleClick: PropTypes.func.isRequired,
  title: PropTypes.string,
}

export default function CommonHeader({backTabIndex, handleClick, title}) {
  const isBack = useRecoilValue(commonHeaderBackState);
  const isClose = useRecoilValue(commonHeaderCloseState);

  return (
    <Container
      maxWidth={'xs'}
      disableGutters
      sx={{
        px: '20px',
        position: 'fixed',
        top: 0,
        alignItems: 'center',
        justifyContent: 'space-between',
        display: 'flex',
        zIndex: 999,
        ...sxFixedCenterMainLayout
      }}
    >
      <Stack direction="row" height={`${HEADER.H_MOBILE}px`}>
        <IconButton
          tabIndex={-1}
          disableFocusRipple
          onClick={handleClick}
          sx={{
            p: 0,
            mr: '16px',
            color: '#222222',
            visibility: isBack ? 'visible' : 'hidden',
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: '#FFFFFF',
            }
          }}
        >
          <ArrowBackIcon titleAccess={ALT_STRING.COMMON.BTN_BACK} role={'img'} tabIndex={backTabIndex} sx={{...clickBorderNone}}/>
        </IconButton>

        <Typography
          sx={{
            visibility: isBack ? 'visible' : 'hidden',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '20px',
            lineHeight: '26px',
            display: 'flex',
            alignItems: 'center',
            letterSpacing: '-0.03em',
            color: '#000000',
            paddingTop: '5px',
          }}
        >
          {title}
        </Typography>
      </Stack>

      <IconButton
        onClick={handleClick}
        sx={{
          p: 0,
          color: '#222222',
          height: `${HEADER.H_MOBILE}px`,
          visibility: isClose ? 'visible' : 'hidden',
          '&:hover': {
            boxShadow: 'none',
            backgroundColor: '#FFFFFF',
          }
        }}
      >
        <CloseIcon/>
      </IconButton>
    </Container>
  );
}