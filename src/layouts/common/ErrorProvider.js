import {useQueryClient} from "react-query";
import {useEffect} from "react";
import {useRecoilState} from "recoil";
import {toastMessageSelector} from "../../recoil/selector/common/toastSelector";
import {ERRORS, GET_ERROR_CODE, GET_FRONT_ERROR_MESSAGE, IS_CUSTOM_ERROR} from "../../constant/common/Error";
import {TOAST_TYPE} from "../../constant/common/Common";
import {ENV} from "../../config";
import {ENV_TYPE} from "../../constant/common/Env";

export default function ErrorProvider() {
  const [toastMessage, setToastMessage] = useRecoilState(toastMessageSelector)
  const queryClient = useQueryClient();
  const queryCache = queryClient.getQueryCache();
  const mutationCache = queryClient.getMutationCache();

  const handleShowError = async (error) => {
    if (IS_CUSTOM_ERROR(error)) {
      switch (GET_ERROR_CODE(error)) {
        case ERRORS.INVALID_PARAMETER:
          setToastMessage({
            type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
            message: GET_FRONT_ERROR_MESSAGE(ERRORS.INVALID_PARAMETER)
          })
          break
        default:
          break
      }
    } else if (error?.code) {
      switch (GET_ERROR_CODE(error)) {
        case ERRORS.ECONNABORTED:
          setToastMessage({
            type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
            message: GET_FRONT_ERROR_MESSAGE(ERRORS.ECONNABORTED)
          })
          break
        case ERRORS.BAD_REQUEST:
        default:
          if (ENV !== ENV_TYPE.PRODUCT) {
            setToastMessage({
              type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
              message: JSON.stringify({
                url: error?.config?.url,
                code: error?.code,
                message: error?.message
              })
            })
          }
          break
      }
    }
  }

  useEffect(() => {
    const unsubscribe1 = queryCache.subscribe(event => handleShowError(event?.query?.state?.error));
    const unsubscribe2 = mutationCache.subscribe(event => handleShowError(event?.state?.error));
    return unsubscribe1, unsubscribe2
  }, []);
}