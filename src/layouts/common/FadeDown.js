import {m} from "framer-motion";
import {FADE_DOWN} from "../../constant/common/Variants";

export default function FadeDown({children, container, item}) {

  if (container) {
    return (
      <m.div initial="hidden" animate="open">
        <m.div
          variants={{
            open: {
              clipPath: "inset(0% 0% 0% 0% round 10px)",
              transition: {
                type: "spring",
                bounce: 0,
                duration: 0.7,
                delayChildren: 0.3,
                staggerChildren: 0.05
              }
            },
          }}
          style={{width: '100%', display: 'flex', flexWrap: 'wrap', flexDirection: 'row'}}
        >
          {children}
        </m.div>
      </m.div>
    )
  } else if (item) {
    return (
      <m.div variants={FADE_DOWN}>
        {children}
      </m.div>
    )
  }
}