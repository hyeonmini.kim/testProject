import PropTypes from 'prop-types';
import {Box} from '@mui/material';
import {HEADER} from "../../config";

IFrameBodyLayout.propTypes = {
  sx: PropTypes.object,
  children: PropTypes.node,
};

export default function IFrameBodyLayout({noToolbar, children, sx, ...other}) {
  return (
    <Box
      component="main"
      sx={{
        flexGrow: 1,
        height: '100%',
        pt: noToolbar ? 0 : `${HEADER.H_MOBILE}px`,
        ...sx,
      }}
    >
      {children}
    </Box>
  );
}
