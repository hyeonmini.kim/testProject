import {Box, Typography} from "@mui/material";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {LOGIN} from "../../constant/sign-up/SignUp";
import * as React from "react";
import {useEffect} from "react";
import {useRecoilState} from "recoil";
import IFrameDialog from "../../components/common/IFrameDialog";
import {settingShowPersonalTermsSelector, settingShowServiceTermsSelector} from "../../recoil/selector/my/settingSelector";
import {MY_SETTING} from "../../constant/my/Setting";
import {getTermsOfUseByTitle} from "../../api/mySettingApi";

export default function Footer() {
  const [showServiceTerms, setShowServiceTerms] = useRecoilState(settingShowServiceTermsSelector)
  const [showPersonalTerms, setShowPersonalTerms] = useRecoilState(settingShowPersonalTermsSelector)

  const {data: personalContents} = getTermsOfUseByTitle({
    title: encodeURIComponent(MY_SETTING.TERMS.PERSONAL.VALUE)
  })
  const {data: serviceContents} = getTermsOfUseByTitle({
    title: encodeURIComponent(MY_SETTING.TERMS.SERVICE.VALUE)
  })

  const handleClickAgree = () => {
    setShowServiceTerms(true)
  }

  const handleClickPersonal = () => {
    setShowPersonalTerms(true)
  }

  useEffect(() => {
    return () => {
      setShowServiceTerms(false)
      setShowPersonalTerms(false)
    }
  }, [])

  return (
    <>
      <Box sx={{display: 'flex', justifyContent: 'center', mt: '50px', mb: '50px'}}>
        <Typography
          tabIndex={5}
          variant={'b2_14_r'}
          onClick={handleClickAgree}
          onKeyDown={(e) => handleEnterPress(e, handleClickAgree)}
          sx={{color: '#999999', cursor: 'pointer'}}
        >
          {LOGIN.AGREE}
        </Typography>
        <Typography variant={'b2_14_m'} sx={{color: '#999999', mx: '8px'}}>
          {LOGIN.DIVIDER}
        </Typography>
        <Typography
          tabIndex={5}
          variant={'b2_14_b'}
          onClick={handleClickPersonal}
          onKeyDown={(e) => handleEnterPress(e, handleClickPersonal)}
          sx={{color: '#999999', cursor: 'pointer'}}
        >
          {LOGIN.PERSONAL}
        </Typography>
      </Box>

      {showServiceTerms &&
        <IFrameDialog open={showServiceTerms} url={serviceContents?.bodyHtmlFileUrl}
                      onBack={() => setShowServiceTerms(false)}/>}
      {showPersonalTerms &&
        <IFrameDialog open={showPersonalTerms} url={personalContents?.bodyHtmlFileUrl}
                      onBack={() => setShowPersonalTerms(false)}/>}

    </>
  )
}

