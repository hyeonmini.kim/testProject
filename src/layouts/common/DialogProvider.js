import {useRecoilState, useResetRecoilState} from "recoil";
import {useEffect, useState} from "react";
import {COMMON_DIALOG_TYPE} from "../../constant/common/Common";
import {dialogSelector, dialogShowDialogSelector} from "../../recoil/selector/common/dialogSelector";
import DialogPopup from "../../sections/common/DialogPopup";

export default function DialogProvider() {
  const [dialog, setDialog] = useRecoilState(dialogSelector)
  const resetDialog = useResetRecoilState(dialogSelector)
  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)
  const [newDialog, setNewDialog] = useState(null)

  useEffect(() => {
    if (dialog?.message) {
      setNewDialog({...dialog})
      switch (dialog.type) {
        case COMMON_DIALOG_TYPE.ONE_BUTTON:
          setShowDialog(true)
          break;
        case COMMON_DIALOG_TYPE.TWO_BUTTON:
          setShowDialog(true)
          break;
        default:
          break;
      }
      resetDialog()
    }
  }, [dialog])

  return (
    <DialogPopup dialog={newDialog}/>
  );
}