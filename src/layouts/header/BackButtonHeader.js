import {AppBar, IconButton, Toolbar, Typography} from "@mui/material";
import {styled} from "@mui/material/styles";
import {HEADER} from "../../config";
import {bgBlur} from "../../utils/cssStyles";
import CloseIcon from '@mui/icons-material/Close';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import PropTypes from "prop-types";
import BackButtonBody from "./BackButtonBody";
import {sxFixedCenterMainLayout} from "../main/MainLayout";

const ToolbarStyle = styled(Toolbar)(({theme}) => ({
  height: HEADER.H_MOBILE,
  zIndex: theme.zIndex.appBar + 1,
  ...bgBlur({
    color: theme.palette.background.default,
  }),
  transition: theme.transitions.create(['height'], {
    duration: theme.transitions.duration.shorter,
  }),
}));


BackButtonHeader.propTypes = {
  title: PropTypes.string,
  handleClick: PropTypes.func.isRequired,
  isCloseBtn: PropTypes.bool,
  children: PropTypes.any,
}

export default function BackButtonHeader({title, handleClick, isCloseBtn = false, children}) {
  return (
    <>
      <AppBar sx={{boxShadow: 0, background: 'white', alignItems: isCloseBtn ? 'end' : 'start', ...sxFixedCenterMainLayout}}>
        <ToolbarStyle disableGutters sx={{background: 'white', px: '20px', '&.MuiToolbar-root': {minHeight: HEADER.H_MOBILE}}}>
          <IconButton sx={{p: 0}} onClick={handleClick}>
            {isCloseBtn ? <CloseIcon/> : <ArrowBackIcon/>}
          </IconButton>
          {
            !isCloseBtn &&
            <Typography variant="h4" sx={{ml: 1, color: 'text.primary'}}>
              {title}
            </Typography>
          }
        </ToolbarStyle>
      </AppBar>
      <BackButtonBody>
        {children}
      </BackButtonBody>
    </>
  );
}