import PropTypes from 'prop-types';
// @mui
import {Box} from '@mui/material';
// hooks
// config
import {BEFU, HEADER} from '../../config';
// components
import {useSettingsContext} from '../../components/settings';

// ----------------------------------------------------------------------

BackButtonBody.propTypes = {
  sx: PropTypes.object,
  children: PropTypes.node,
};

export default function BackButtonBody({children, sx, ...other}) {
  const {themeLayout} = useSettingsContext();

  return (
    <Box
      sx={{
        flexGrow: 1,
        pt: `${HEADER.H_MOBILE}px`,
        pb: `${BEFU.B_MARGIN}px`,
        ...sx,
      }}
    >
      {children}
    </Box>
  );
}
