export const MOCK_POPUP_LIST = [
  {
    popup_key: "1000021",
    popup_subject: "변경된 팝업 타입......테슽......",
    popup_type: "HTML_EVENT_DETAILS",
    popup_register: "09930262",
    popup_url: "https://d3o94hoavggqsh.cloudfront.net/images/popup/20230628015931-Luka_Dončić_2021.jpg",
    popup_link: "d_iframe=https://d1jqpeehmfugex.cloudfront.net/professional_column/baby_food_early.html",
    popup_order: "1",
    popup_start_date: "2023-06-23 17:00:00",
    popup_end_date: "2024-06-25 15:00:00",
    created_datetime: "2024-06-28 10:59:34"
  },
  {
    popup_key: "1000023",
    popup_subject: "팝업등록1",
    popup_type: "FAQ",
    popup_register: "09930262",
    popup_url: "https://d3o94hoavggqsh.cloudfront.net/images/popup/20230628022311-고양이2.jpeg",
    popup_link: "d_faq=534",
    popup_order: "2",
    popup_start_date: "2023-06-01 00:00:00",
    popup_end_date: "2024-06-02 00:00:00",
    created_datetime: "2023-06-28 11:22:55"
  },
  {
    popup_key: "1000024",
    popup_subject: "new things!",
    popup_type: "FAQ",
    popup_register: "09930262",
    popup_url: "https://d3o94hoavggqsh.cloudfront.net/images/popup/20230630010627-p179591213516477_399.jpg",
    popup_link: "d_notice=62",
    popup_order: "3",
    popup_start_date: "2023-06-23 17:00:00",
    popup_end_date: "2024-07-01 09:00:00",
    created_datetime: "2023-06-30 10:06:29"
  }
]