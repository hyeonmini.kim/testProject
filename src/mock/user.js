export const MOCK_GET_USER = [
  {
    key: 1,
    name: '당산비룡',
  },
  {
    key: 2,
    name: '부천불주먹',
  },
]

export const MOCK_GET_USER2 = [
  {
    key: 1,
    name: '당산비룡',
  },
  {
    key: 2,
    name: '부천불주먹',
  },
]

export const MOCK_GET_MY_INFO = {
  profilePictureUrl: "",
  birthDay: "1984-04-30",
  phoneNumber: "010-4785-8262",
  profileSelectedBabyKey: 2,
  postCensorshipResultNotifSet: false,
  marketingInfoNotifSet: false,
  termsCollectingPersonalDataMarketingAgreed: true,
  briefBio: "2변경된 간단소개 테스트 문구",
  babies: [
    {
      key: 2,
      parentKey: 1,
      nickname: "baby6611122222",
      birthdate: "2022-10-01",
      height: "66",
      weight: "6",
      gender: "MALE",
      profilePictureUrl: "",
      profilePictureThumbnailOrder: 1,
      allergyIngredients: [
        {
          key: "ALG000004",
          name: "메밀"
        },
        {
          key: "ALG000003",
          name: "밀"
        }
      ],
      concerns: [
        {
          key: "HINT000007",
          name: "빈혈"
        },
        {
          key: "HINT000006",
          name: "뼈 건강"
        }
      ]
    },
    {
      key: 33,
      parentKey: 1,
      nickname: "baby10",
      birthdate: "2022-10-01",
      height: "50",
      weight: "5",
      gender: "FEMALE",
      profilePictureUrl: "",
      profilePictureThumbnailOrder: 2,
      allergyIngredients: [
        {
          key: "ALG000010",
          name: "조개류"
        },
        {
          key: "ALG000011",
          name: "땅콩"
        }
      ],
      concerns: [
        {
          key: "HINT000010",
          name: "골연화증"
        },
        {
          key: "HINT000011",
          name: "빈혈"
        }
      ]
    },
    {
      key: 32,
      parentKey: 1,
      nickname: "baby10",
      birthdate: "2022-10-01",
      height: "50",
      weight: "5",
      gender: "FEMALE",
      profilePictureUrl: "",
      profilePictureThumbnailOrder: 0,
      allergyIngredients: [
        {
          key: "ALG000010",
          name: "조개류"
        },
        {
          key: "ALG000011",
          name: "땅콩"
        }
      ],
      concerns: [
        {
          key: "HINT000010",
          name: "골연화증"
        },
        {
          key: "HINT000011",
          name: "빈혈"
        }
      ]
    }
  ],
  type: "NORMAL_MEMBER",
  accountKey: 1745,
  socialMediaUrl: "2modified@instagram.com",
  grade: "LV1",
  nickname: "아콩이아빠김경렬입니다하하하하",
  key: 1,
  email: "2modified@email.com"
}

export const MOCK_GET_MY_INFO2 = {
  profilePictureUrl: "",
  birthDay: "1984-04-30",
  phoneNumber: "010-4785-8262",
  profileSelectedBabyKey: 2,
  postCensorshipResultNotifSet: false,
  marketingInfoNotifSet: false,
  termsCollectingPersonalDataMarketingAgreed: true,
  briefBio: "2변경된 간단소개 테스트 문구",
  babies: [
    {
      key: 2,
      parentKey: 1,
      nickname: "baby6611122222",
      birthdate: "2022-10-01",
      height: "66",
      weight: "6",
      gender: "MALE",
      profilePictureUrl: "",
      profilePictureThumbnailOrder: 1,
      allergyIngredients: [
        {
          key: "ALG000004",
          name: "메밀"
        },
        {
          key: "ALG000003",
          name: "밀"
        }
      ],
      concerns: [
        {
          key: "HINT000007",
          name: "빈혈"
        },
        {
          key: "HINT000006",
          name: "뼈 건강"
        }
      ]
    },
    {
      key: 33,
      parentKey: 1,
      nickname: "baby10",
      birthdate: "2022-10-01",
      height: "50",
      weight: "5",
      gender: "FEMALE",
      profilePictureUrl: "",
      profilePictureThumbnailOrder: 2,
      allergyIngredients: [
        {
          key: "ALG000010",
          name: "조개류"
        },
        {
          key: "ALG000011",
          name: "땅콩"
        }
      ],
      concerns: [
        {
          key: "HINT000010",
          name: "골연화증"
        },
        {
          key: "HINT000011",
          name: "빈혈"
        }
      ]
    },
    {
      key: 32,
      parentKey: 1,
      nickname: "baby10",
      birthdate: "2022-10-01",
      height: "50",
      weight: "5",
      gender: "FEMALE",
      profilePictureUrl: "",
      profilePictureThumbnailOrder: 0,
      allergyIngredients: [
        {
          key: "ALG000010",
          name: "조개류"
        },
        {
          key: "ALG000011",
          name: "땅콩"
        }
      ],
      concerns: [
        {
          key: "HINT000010",
          name: "골연화증"
        },
        {
          key: "HINT000011",
          name: "빈혈"
        }
      ]
    }
  ],
  type: "NORMAL_MEMBER",
  accountKey: 1745,
  socialMediaUrl: "2modified@instagram.com",
  grade: "LV1",
  nickname: "인천의자랑끼룩이화이팅하하하하",
  key: 1,
  email: "2modified@email.com"
}

export const MOCK_GET_RECIPE_MY_SELF = {
  recipe_write_list: [
    {
      recipe_key: 1000012,
      recipe_name: "김치찌개",
      recipe_desc: "돼지고기를 넣은 김치찌개",
      recipe_view_cnt: 0,
      recipe_scrap_cnt: 0,
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg',
      recipe_tag_desc: "식이섬유풍부,칼슘풍부",
      recipe_check_status: "임시저장"
    },
    {
      recipe_key: 1000017,
      recipe_name: "김치찌개",
      recipe_desc: "돼지고기를 넣은 김치찌개",
      recipe_view_cnt: 0,
      recipe_scrap_cnt: 0,
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg',
      recipe_check_status: "임시저장"
    },
    {
      recipe_key: 1000014,
      recipe_name: "김치찌개",
      recipe_desc: "돼지고기를 넣은 김치찌개",
      recipe_view_cnt: 0,
      recipe_scrap_cnt: 0,
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg',
      recipe_check_status: "반려"
    },
    {
      recipe_key: 1000013,
      recipe_name: "김치찌개",
      recipe_desc: "돼지고기를 넣은 김치찌개",
      recipe_view_cnt: 0,
      recipe_scrap_cnt: 0,
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000003,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피 연어",
      recipe_view_cnt: 0,
      recipe_scrap_cnt: 0,
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "게시중"
    },
    {
      recipe_key: 1000015,
      recipe_name: "김치찌개",
      recipe_desc: "돼지고기를 넣은 김치찌개",
      recipe_view_cnt: 0,
      recipe_scrap_cnt: 0,
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_tag_desc: "식이섬유풍부,칼슘풍부",
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000011,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000010,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000009,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000008,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000007,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000005,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000004,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    }
  ],
  recipe_scrap_list: [],
  recipe_posting_count: "15",
  scrap_booked_count: "50",
  recipe_writer_cnt: 185,
  recipe_scrap_cnt: 23,
  write_recipe_count: 13,
  scrap_recipe_count: 0,
  review_recipe_count: 6
}

export const MOCK_GET_RECIPE_MY_SELF2 = {
  recipe_write_list: [
    {
      recipe_key: 1000017,
      recipe_name: "김치찌개",
      recipe_desc: "돼지고기를 넣은 김치찌개",
      recipe_view_cnt: 0,
      recipe_scrap_cnt: 0,
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg',
      recipe_check_status: "임시저장"
    },
    {
      recipe_key: 1000012,
      recipe_name: "김치찌개",
      recipe_desc: "돼지고기를 넣은 김치찌개",
      recipe_view_cnt: 0,
      recipe_scrap_cnt: 0,
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg',
      recipe_tag_desc: "식이섬유풍부,칼슘풍부",
      recipe_check_status: "반려"
    },
    {
      recipe_key: 1000014,
      recipe_name: "김치찌개",
      recipe_desc: "돼지고기를 넣은 김치찌개",
      recipe_view_cnt: 0,
      recipe_scrap_cnt: 0,
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000013,
      recipe_name: "김치찌개",
      recipe_desc: "돼지고기를 넣은 김치찌개",
      recipe_view_cnt: 0,
      recipe_scrap_cnt: 0,
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg',
      recipe_check_status: "게시중"
    },
    {
      recipe_key: 1000003,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피 연어",
      recipe_view_cnt: 0,
      recipe_scrap_cnt: 0,
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000015,
      recipe_name: "김치찌개",
      recipe_desc: "돼지고기를 넣은 김치찌개",
      recipe_view_cnt: 0,
      recipe_scrap_cnt: 0,
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_tag_desc: "식이섬유풍부,칼슘풍부",
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000011,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000010,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000009,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000008,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000007,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000005,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    },
    {
      recipe_key: 1000004,
      recipe_name: "연어빠바요뜨",
      recipe_desc: "아이들이 좋아하는 레시피",
      image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
      recipe_check_status: "검수중"
    }
  ],
  recipe_scrap_list: [],
  recipe_posting_count: "15",
  scrap_booked_count: "50",
  recipe_writer_cnt: 185,
  recipe_scrap_cnt: 23,
  write_recipe_count: 13,
  scrap_recipe_count: 0,
  review_recipe_count: 6
}

export const MOCK_SNS_LIST = [
  {
    socialAccountConnectionStatusKey: 33,
    isConnectionStatus: false,
    providerType: "APPLE",
    email: null
  },
  {
    socialAccountConnectionStatusKey: 34,
    isConnectionStatus: false,
    providerType: "KAKAO",
    email: null
  },
  {
    socialAccountConnectionStatusKey: 35,
    isConnectionStatus: false,
    providerType: "GOOGLE",
    email: null
  },
  {
    socialAccountConnectionStatusKey: 36,
    isConnectionStatus: true,
    providerType: "NAVER",
    email: "kyobo1121@gmai.com"
  }
]
