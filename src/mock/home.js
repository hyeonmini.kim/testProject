export const MAIN_INFO = {
  health_list: [
    {
      "health_key": "HINT000001",
      "health_name": "안구건조증",
      "health_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "health_key": "HINT000002",
      "health_name": "입/혀 염증",
      "health_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "health_key": "HINT000003",
      "health_name": "잇몸 출혈",
      "health_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "health_key": "HINT000004",
      "health_name": "피부 건강",
      "health_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "health_key": "HINT000005",
      "health_name": "근육 강화",
      "health_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "health_key": "HINT000006",
      "health_name": "뼈 건강",
      "health_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "health_key": "HINT000007",
      "health_name": "빈혈",
      "health_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "health_key": "HINT000008",
      "health_name": "감기/수족구",
      "health_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "health_key": "HINT000009",
      "health_name": "편식",
      "health_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "health_key": "HINT000010",
      "health_name": "변비",
      "health_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "health_key": "HINT000011",
      "health_name": "유당불내증",
      "health_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    }
  ],
  ingredient_search_category_list: [
    {
      "ig_search_catg_key": 1,
      "ig_search_catg_name": "소고기",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 2,
      "ig_search_catg_name": "돼지고기",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 3,
      "ig_search_catg_name": "닭고기",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 4,
      "ig_search_catg_name": "육류",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 5,
      "ig_search_catg_name": "수산물",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 6,
      "ig_search_catg_name": "채소류",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 7,
      "ig_search_catg_name": "버섯류",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 8,
      "ig_search_catg_name": "과일류",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 9,
      "ig_search_catg_name": "쌀",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 10,
      "ig_search_catg_name": "밀가루",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 11,
      "ig_search_catg_name": "곡류",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 12,
      "ig_search_catg_name": "콩/견과류",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 13,
      "ig_search_catg_name": "난류",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 14,
      "ig_search_catg_name": "유제품류",
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "ig_search_catg_key": 15,
      "ig_search_img": "https://s3.ap-northeast-2.amazonaws.com/mybucket/xxx.jpg"
    }
  ],
  alg_list: [
    {
      "alg_key": "ALG000001",
      "alg_name": "우유",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000002",
      "alg_name": "난류",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000003",
      "alg_name": "밀",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000004",
      "alg_name": "메밀",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000005",
      "alg_name": "대두",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000006",
      "alg_name": "게",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000007",
      "alg_name": "새우",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000008",
      "alg_name": "오징어",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000009",
      "alg_name": "고등어",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000010",
      "alg_name": "조개류",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000011",
      "alg_name": "땅콩",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000012",
      "alg_name": "잣",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000013",
      "alg_name": "호두",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000014",
      "alg_name": "돼지고기",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000015",
      "alg_name": "복숭아",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000016",
      "alg_name": "토마토",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000017",
      "alg_name": "닭고기",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    },
    {
      "alg_key": "ALG000018",
      "alg_name": "쇠고기",
      "alg_img": "https://s3.apnortheast2.amazonaws.com/mybucket/xxx.jpg"
    }
  ],
  banner_info_list: [
    {
      "banner_type": 1,
      "banner_subject": "월령별 섭취 자제 재료",
      "banner_url": "https://d3am0bqv86scod.cloudfront.net/banner_test.jpg",
      "banner_link": "/static/info/index1.html"
    },
    {
      "banner_type": 2,
      "banner_subject": "알레르기 유발 재료",
      "banner_url": "https://d3am0bqv86scod.cloudfront.net/banner_test.jpg",
      "banner_link": "/static/info/index2.html"
    },
  ],
  banner_spring_list: [
    {
      "banner_type": 1,
      "banner_url": "https://d3am0bqv86scod.cloudfront.net/banner_test.jpg",
      "banner_link": "https://naver.com"
    },
    {
      "banner_type": 2,
      "banner_url": "https://d3am0bqv86scod.cloudfront.net/banner_test.jpg",
      "banner_link": "https://naver.com"
    },
    {
      "banner_type": 3,
      "banner_url": "https://d3am0bqv86scod.cloudfront.net/banner_test.jpg",
      "banner_link": "https://naver.com"
    }
  ],
  banner_season_object: {
    "banner_type": 1,
    "banner_url": "https://d3am0bqv86scod.cloudfront.net/banner_test.jpg",
    "banner_link": "https://d3am0bqv86scod.cloudfront.net/banner1/index.html"
  },
  season_ingredient: '고구마'
}


export const NOTIFICATION = {
  "new_noti_list": [
    {
      "noti_key": "1000017",
      "noti_subject": "끼룩 호방을 검수 하고있어요",
      "noti_type": "검수",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000002",
      "noti_recipe_name": "끼룩 호빵",
      "noti_read_yn": "N",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000007",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000002",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "N",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000001",
      "noti_subject": "백종원 김치찌개가 검수 반려되었어요",
      "noti_type": "반려",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000002",
      "noti_recipe_name": "백종원 김치찌개",
      "noti_read_yn": "N",
      "noti_target_user_key": "20230111",
      "created_datetime": "20230111"
    },
  ],
  "today_noti_list": [],
  "yesterday_noti_list": [],
  "before7_noti_list": [
    {
      "noti_key": "1000005",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000002",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000001",
      "noti_subject": "된장찌개레시피가 검수 반려되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000003",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000006",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000004",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000002",
      "noti_subject": "된장찌개레시피가 검수 반려되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000005",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000003",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000006",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000004",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000007",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000027",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000008",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000028",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000009",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000010",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000010",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    }
  ],
  "cur_month_noti_list": [],
  "before_noti_list": [],
  "before2_noti_list": [],
  "before_month": "2022.12",
  "before2_month": "2022.11",
  "nick_name": "parent1"
}
export const NOTIFICATION2 = {
  "new_noti_list": [
    {
      "noti_key": "1000007",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000012",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "N",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000001",
      "noti_subject": "백종원 김치찌개가 검수 반려되었어요",
      "noti_type": "반려",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000022",
      "noti_recipe_name": "백종원 김치찌개",
      "noti_read_yn": "N",
      "noti_target_user_key": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000001",
      "noti_subject": "끼룩 호방을 검수 하고있어요",
      "noti_type": "검수중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000022",
      "noti_recipe_name": "끼룩 호빵",
      "noti_read_yn": "N",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000017",
      "noti_subject": "공지사항 입니다",
      "noti_type": "공지사항",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1",
      "noti_recipe_name": "끼룩 호빵",
      "noti_read_yn": "N",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000017",
      "noti_subject": "이달의 스프링클 당첨",
      "noti_type": "승급",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000042",
      "noti_recipe_name": "끼룩 호빵",
      "noti_read_yn": "N",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
  ],
  "today_noti_list": [],
  "yesterday_noti_list": [],
  "before7_noti_list": [
    {
      "noti_key": "1000005",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000052",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000001",
      "noti_subject": "된장찌개레시피가 검수 반려되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000062",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000006",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000072",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000002",
      "noti_subject": "된장찌개레시피가 검수 반려되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000082",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000003",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000092",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000004",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000102",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000027",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000202",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000028",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000302",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    },
    {
      "noti_key": "1000010",
      "noti_subject": "된장찌개 레시피가 정상적으로 게시되었어요",
      "noti_type": "게시중",
      "noti_img_key": "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
      "noti_recipe_key": "1000402",
      "noti_recipe_name": "된장찌개",
      "noti_read_yn": "Y",
      "noti_target_user_key": "",
      "created_datetime": "20230111",
      "create_date": "2023.01.11"
    }
  ],
  "cur_month_noti_list": [],
  "before_noti_list": [],
  "before2_noti_list": [],
  "before_month": "2022.12",
  "before2_month": "2022.11",
  "nick_name": "parent1"
}

export const BABY_INFO = {
  profile_weight: {
    "percentage": 100,
    "statement": "상위1%",
    "value": "18.0kg"
  },
  profile_dashboard: {
    "energy": 568,
    "energy_statement": "568kcal"
  },
  profile_bmi: {
    "percentage": 75,
    "statement": "과체중"
  },
  baby_key: "1",
  profile: {
    "month": 6,
    "rank": 0,
    "statement_evaluation": "아이가 아직 2살 미만이에요.",
    "statement_status": "우리 아이가 잘 자라고 있어요!",
    "day": 210
  },
  profile_height: {
    "percentage": 100,
    "statement": "상위1%",
    "value": "124.0cm"
  }
}

export const BABY_INFO2 = {
  profile_weight: {
    "percentage": 80,
    "statement": "상위7%",
    "value": "22.0kg"
  },
  profile_dashboard: {
    "energy": 466,
    "energy_statement": "399kcal"
  },
  profile_bmi: {
    "percentage": 25,
    "statement": "저체중"
  },
  baby_key: "1",
  profile: {
    "month": 8,
    "rank": 1,
    "statement_evaluation": "아이가 아직 3살 미만이에요.",
    "statement_status": "우리 아이가 잘 자라고 있어요!",
    "day": 210
  },
  profile_height: {
    "percentage": 100,
    "statement": "상위1%",
    "value": "124.0cm"
  }
}