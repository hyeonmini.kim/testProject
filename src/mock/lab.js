export const MOCK_SURVEY_YN = 'N'

export const MOCK_SURVEY_LIST = [
  {
    "survey_order": 1,
    "brand_name": "엘빈즈",
    "survey_num": 1234,
    "survey_rate": 35.1,
    "select_reason_list": [
      {
        "select_reason_order": 1,
        "select_reason": "SEL07",
        "select_reason_rate": 95.1,
      },
      {
        "select_reason_order": 2,
        "select_reason": "SEL08",
        "select_reason_rate": 85.0,
      },
      {
        "select_reason_order": 3,
        "select_reason": "SEL09",
        "select_reason_rate": 74.9,
      },
      {
        "select_reason_order": 4,
        "select_reason": "SEL10",
        "select_reason_rate": 64.8,
      },
      {
        "select_reason_order": 5,
        "select_reason": "SEL11",
        "select_reason_rate": 54.7,
      },
      {
        "select_reason_order": 6,
        "select_reason": "SEL12",
        "select_reason_rate": 44.6,
      },
      {
        "select_reason_order": 7,
        "select_reason": "SEL13",
        "select_reason_rate": 34.5,
      },
    ],
  },
  {
    "survey_order": 2,
    "brand_name": "PH365",
    "survey_num": 834,
    "survey_rate": 35.0,
    "select_reason_list": [
      {
        "select_reason_order": 1,
        "select_reason": "SEL13",
        "select_reason_rate": 35.1,
      },
      {
        "select_reason_order": 2,
        "select_reason": "SEL12",
        "select_reason_rate": 35.0,
      },
      {
        "select_reason_order": 3,
        "select_reason": "SEL11",
        "select_reason_rate": 34.9,
      },
      {
        "select_reason_order": 4,
        "select_reason": "SEL10",
        "select_reason_rate": 34.8,
      },
      {
        "select_reason_order": 5,
        "select_reason": "SEL09",
        "select_reason_rate": 34.7,
      },
      {
        "select_reason_order": 6,
        "select_reason": "SEL08",
        "select_reason_rate": 34.6,
      },
      {
        "select_reason_order": 7,
        "select_reason": "SEL07",
        "select_reason_rate": 34.5,
      },
    ],
  },
  {
    "survey_order": 3,
    "brand_name": "포베라아이암",
    "survey_num": 734,
    "survey_rate": 34.1,
    "select_reason_list": [
      {
        "select_reason_order": 1,
        "select_reason": "SEL07",
        "select_reason_rate": 35.1,
      },
      {
        "select_reason_order": 2,
        "select_reason": "SEL08",
        "select_reason_rate": 35.0,
      },
      {
        "select_reason_order": 3,
        "select_reason": "SEL09",
        "select_reason_rate": 34.9,
      },
      {
        "select_reason_order": 4,
        "select_reason": "SEL10",
        "select_reason_rate": 34.8,
      },
      {
        "select_reason_order": 5,
        "select_reason": "SEL11",
        "select_reason_rate": 34.7,
      },
      {
        "select_reason_order": 6,
        "select_reason": "SEL12",
        "select_reason_rate": 34.6,
      },
      {
        "select_reason_order": 7,
        "select_reason": "SEL13",
        "select_reason_rate": 34.5,
      },
    ],
  },
  {
    "survey_order": 4,
    "brand_name": "바이오메라",
    "survey_num": 634,
    "survey_rate": 34.0,
    "select_reason_list": [
      {
        "select_reason_order": 1,
        "select_reason": "SEL13",
        "select_reason_rate": 35.1,
      },
      {
        "select_reason_order": 2,
        "select_reason": "SEL12",
        "select_reason_rate": 35.0,
      },
      {
        "select_reason_order": 3,
        "select_reason": "SEL11",
        "select_reason_rate": 34.9,
      },
      {
        "select_reason_order": 4,
        "select_reason": "SEL10",
        "select_reason_rate": 34.8,
      },
      {
        "select_reason_order": 5,
        "select_reason": "SEL09",
        "select_reason_rate": 34.7,
      },
      {
        "select_reason_order": 6,
        "select_reason": "SEL08",
        "select_reason_rate": 34.6,
      },
      {
        "select_reason_order": 7,
        "select_reason": "SEL07",
        "select_reason_rate": 34.5,
      },
    ],
  },
  {
    "survey_order": 5,
    "brand_name": "닥터아돌",
    "survey_num": 534,
    "survey_rate": 33.1,
    "select_reason_list": [
      {
        "select_reason_order": 1,
        "select_reason": "SEL07",
        "select_reason_rate": 35.1,
      },
      {
        "select_reason_order": 2,
        "select_reason": "SEL08",
        "select_reason_rate": 35.0,
      },
      {
        "select_reason_order": 3,
        "select_reason": "SEL09",
        "select_reason_rate": 34.9,
      },
      {
        "select_reason_order": 4,
        "select_reason": "SEL10",
        "select_reason_rate": 34.8,
      },
      {
        "select_reason_order": 5,
        "select_reason": "SEL11",
        "select_reason_rate": 34.7,
      },
      {
        "select_reason_order": 6,
        "select_reason": "SEL12",
        "select_reason_rate": 34.6,
      },
      {
        "select_reason_order": 7,
        "select_reason": "SEL13",
        "select_reason_rate": 34.5,
      },
    ],
  },
]