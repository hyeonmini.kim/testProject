/*
recipe_user_based_list: [
  {
    recipe_key: '1000018',
    recipe_name: '떡꼬치 쉽게 만드는 법 아이들 간식으로 인기 짱!',
    recipe_desc: '만능 양념장으로 후딱 만들 수 있는 떡꼬치 알려드릴께요.\\n제가 올린 레시피에 만능양념장 만드는 레시피가 있습니다.',
    recipe_lead_time: '30분이내',
    recipe_level: '어려움',
    recipe_review_total_count: 0,
    image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
    recipe_tag_desc: '식이섬유풍부,칼슘풍부',
    recipe_user_key: 'rhtjdwls84',
    recipe_user_name: '부천불주먹',
    recipe_user_thumbnail: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_24.jpg',
    recipe_view_cnt: 77,
    recipe_scrap_cnt: 9,
    recipe_allergy_tag: ['쌀', '계란'],
    recipe_expose_yn: 'Y',
  }
]
 */

export const MOCK_GET_RECIPE = [
  {
    recipe_key: '1000012',
    recipe_name: '8개월아기 이유식 메뉴, 달콤하고 부드러운 대파스프 만들기 :)',
    recipe_desc: '역시 추운날은 궁물요리가 최고지요 ㅎㅎㅎ 암요암요',
    recipe_lead_time: '1시간 이상',
    recipe_level: '좀 어려워요',
    recipe_review_total_count: 17218,
    image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg',
    recipe_user_key: 'rhtjdwls81',
    recipe_user_name: '인천끼룩이',
    recipe_user_thumbnail: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_20.jpg',
    recipe_view_cnt: 1024,
    recipe_scrap_cnt: 871,
    recipe_allergy_tag: ['게껍질', '우유', '쌀', '계란'],
    recipe_expose_yn: 'Y',
  },
  {
    recipe_key: '1000018',
    recipe_name: '떡꼬치 쉽게 만드는 법 아이들 간식으로 인기 짱!',
    recipe_desc: '만능 양념장으로 후딱 만들 수 있는 떡꼬치 알려드릴께요.제가 올린 레시피에 만능양념장 만드는 레시피가 있습니다.',
    recipe_lead_time: '30분이내',
    recipe_level: '어려움',
    recipe_review_total_count: 10,
    image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
    recipe_user_key: 'rhtjdwls80',
    recipe_user_name: '부천불주먹',
    recipe_user_thumbnail: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_19.jpg',
    recipe_view_cnt: 814,
    recipe_scrap_cnt: 468,
    recipe_allergy_tag: ['쌀', '계란'],
    recipe_expose_yn: 'Y',
  },
  {
    recipe_key: '1000004',
    recipe_name: '닭한마리 칼국수와 죽까지 양념장까지 알려드려요',
    recipe_desc: '닭요리를 직접 해먹는것은 참으로 쉬운일은 아니지만 이렇게 만들어 놓으니 너무 맛있다는 아이들때문에 자주 해먹는 레시피입니다.',
    recipe_lead_time: '1시간 이내',
    recipe_level: '할만해요',
    recipe_review_total_count: 1440,
    image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/04/c7055d6a53d17c96bbe28d096132b3911_f.jpg',
    recipe_user_key: 'rhtjdwls82',
    recipe_user_name: '당산비룡',
    recipe_user_thumbnail: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_21.jpg',
    recipe_view_cnt: 144,
    recipe_scrap_cnt: 66,
    recipe_allergy_tag: ['고구마', '감자', '쌀', '계란'],
    recipe_expose_yn: 'Y',
  },
  {
    recipe_key: '1000017',
    recipe_name: '5분 완성 폭탄계란찜 만들기',
    recipe_desc: '몽글몽글 비주얼 끝판왕 폭탄계란찜을 만들었어요~!! 5분이면 만드는 초간단 레시피랍니당♡♡♡ 아이들이 신가해하면서 너무 잘먹어요~!! 만드는법도 간단하고 맛도 너무 좋은 폭탄계란찜 다함께 만들어보세용~!!♡♡♡ 꺄악~~꺆~~',
    recipe_lead_time: '1시간 이내',
    recipe_level: '쉬워요',
    recipe_review_total_count: 1844,
    image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2015/06/08/db3fa52c5b6f3e4946164aa5049d24b5_m.jpg',
    recipe_user_key: 'rhtjdwls83',
    recipe_user_name: '송파비둘기',
    recipe_user_thumbnail: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_22.jpg',
    recipe_view_cnt: 77,
    recipe_scrap_cnt: 9,
    recipe_allergy_tag: ['닭', '쌀', '계란'],
  },
  {
    recipe_key: '1000016',
    recipe_name: '꼬치전 예쁘고 빠르게 부치기',
    recipe_desc: '센스있는 전부치기 꿀 팁 셋! 끼우고, 달걀물 입히고, 부치고!! 꼬치계의 최강자 꼬치전 예쁘고 빠르게 부치는 법!',
    recipe_lead_time: '30분 이내',
    recipe_level: '쉬워요',
    recipe_review_total_count: 17,
    image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2018/02/13/e0649f3f0b446f39c9a7bdff70d953691.jpg',
    recipe_user_key: 'rhtjdwls84',
    recipe_user_name: '신당동떡꼬치',
    recipe_user_thumbnail: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_23.jpg',
    recipe_view_cnt: 4874,
    recipe_scrap_cnt: 986,
    recipe_allergy_tag: ['더덕', '코코넛밀크', '쌀', '계란'],
  },
]

export const MOCK_GET_RECIPE2 = [
  {
    recipe_key: '1000017',
    recipe_name: '5분 완성 폭탄계란찜 만들기',
    recipe_desc: '몽글몽글 비주얼 끝판왕 폭탄계란찜을 만들었어요~!! 5분이면 만드는 초간단 레시피랍니당♡♡♡ 아이들이 신가해하면서 너무 잘먹어요~!! 만드는법도 간단하고 맛도 너무 좋은 폭탄계란찜 다함께 만들어보세용~!!♡♡♡ 꺄악~~꺆~~',
    recipe_lead_time: '1시간 이내',
    recipe_level: '쉬워요',
    recipe_review_total_count: 1844,
    image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2015/06/08/db3fa52c5b6f3e4946164aa5049d24b5_m.jpg',
    recipe_user_key: 'rhtjdwls83',
    recipe_user_name: '송파비둘기',
    recipe_user_thumbnail: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_22.jpg',
    recipe_view_cnt: 77,
    recipe_scrap_cnt: 9,
    recipe_allergy_tag: ['닭', '쌀', '계란'],
  },
  {
    recipe_key: '1000016',
    recipe_name: '꼬치전 예쁘고 빠르게 부치기',
    recipe_desc: '센스있는 전부치기 꿀 팁 셋! 끼우고, 달걀물 입히고, 부치고!! 꼬치계의 최강자 꼬치전 예쁘고 빠르게 부치는 법!',
    recipe_lead_time: '30분 이내',
    recipe_level: '쉬워요',
    recipe_review_total_count: 17,
    image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2018/02/13/e0649f3f0b446f39c9a7bdff70d953691.jpg',
    recipe_user_key: 'rhtjdwls84',
    recipe_user_name: '신당동떡꼬치',
    recipe_user_thumbnail: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_23.jpg',
    recipe_view_cnt: 4874,
    recipe_scrap_cnt: 986,
    recipe_allergy_tag: ['더덕', '코코넛밀크', '쌀', '계란'],
  },
  {
    recipe_key: '1000018',
    recipe_name: '떡꼬치 쉽게 만드는 법 아이들 간식으로 인기 짱!',
    recipe_desc: '만능 양념장으로 후딱 만들 수 있는 떡꼬치 알려드릴께요.제가 올린 레시피에 만능양념장 만드는 레시피가 있습니다.',
    recipe_lead_time: '30분이내',
    recipe_level: '어려움',
    recipe_review_total_count: 10,
    image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2021/11/24/3147cb0a8f53ed7266c792f7d07ffbd21.jpg',
    recipe_user_key: 'rhtjdwls80',
    recipe_user_name: '부천불주먹',
    recipe_user_thumbnail: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_19.jpg',
    recipe_view_cnt: 814,
    recipe_scrap_cnt: 468,
    recipe_allergy_tag: ['쌀', '계란'],
    recipe_expose_yn: 'Y',
  },
  {
    recipe_key: '1000012',
    recipe_name: '8개월아기 이유식 메뉴, 달콤하고 부드러운 대파스프 만들기 :)',
    recipe_desc: '역시 추운날은 궁물요리가 최고지요 ㅎㅎㅎ 암요암요',
    recipe_lead_time: '1시간 이상',
    recipe_level: '좀 어려워요',
    recipe_review_total_count: 17218,
    image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg',
    recipe_user_key: 'rhtjdwls81',
    recipe_user_name: '인천끼룩이',
    recipe_user_thumbnail: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_20.jpg',
    recipe_view_cnt: 1024,
    recipe_scrap_cnt: 871,
    recipe_allergy_tag: ['게껍질', '우유', '쌀', '계란'],
    recipe_expose_yn: 'Y',
  },
  {
    recipe_key: '1000004',
    recipe_name: '닭한마리 칼국수와 죽까지 양념장까지 알려드려요',
    recipe_desc: '닭요리를 직접 해먹는것은 참으로 쉬운일은 아니지만 이렇게 만들어 놓으니 너무 맛있다는 아이들때문에 자주 해먹는 레시피입니다.',
    recipe_lead_time: '1시간 이내',
    recipe_level: '할만해요',
    recipe_review_total_count: 1440,
    image_file_path: 'https://recipe1.ezmember.co.kr/cache/recipe/2022/09/04/c7055d6a53d17c96bbe28d096132b3911_f.jpg',
    recipe_user_key: 'rhtjdwls82',
    recipe_user_name: '당산비룡',
    recipe_user_thumbnail: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_21.jpg',
    recipe_view_cnt: 144,
    recipe_scrap_cnt: 66,
    recipe_allergy_tag: ['고구마', '감자', '쌀', '계란'],
    recipe_expose_yn: 'Y',
  },
]

export const MOCK_TEMP_RECIPE_KEY = '1000022'

export const MOCK_RECIPE_DETAIL = {
  recipe_user_key: "rhtjdwls84",
  recipe_key: "1000002",
  recipe_name: "된장찌개",
  recipe_desc: "차돌박이를 넣은 된장찌개",
  recipe_category: "국/찌개",
  recipe_health_develop: "[피부 건강, 근육 강화, 빈혈, 감기/수족구, 편식]",
  recipe_lead_time: "1시간 이내",
  recipe_level: "좀 어려워요",
  recipe_babyfood_step: "중기 이유식",
  recipe_health_note: "구수한 된장",
  recipe_scrap_cnt: 0,
  recipe_view_cnt: 41,
  recipe_scrap_yn: "",
  recipe_review_yn: "",
  recipe_main_img_key: "1000016",
  recipe_img_key_1: "",
  recipe_img_key_2: "",
  recipe_img_key_3: "",
  recipe_img_key_4: "",
  recipe_main_img_path: "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
  recipe_img_path_1: "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-recipe-2-JIN01728.jpg",
  recipe_img_path_2: "",
  recipe_img_path_3: "",
  recipe_img_path_4: "",
  recipe_servings: 2,
  recipe_cal: 780,
  recipe_reject_msg: "",
  recipe_temp_step: "",
  recipe_check_status: "전시",
  recipe_write_status: "",
  recipe_ingredient_list: [
    {
      recipe_key: "",
      recipe_ingredient_key: "1000005",
      recipe_common_ingredient_key: "N000001",
      recipe_ingredient_name: "김치",
      recipe_ingredient_category: "기본재료",
      recipe_ingredient_main_category: "야채류",
      recipe_ingredient_countunit: "kg, g",
      recipe_ingredient_main_countunit: "kg",
      recipe_ingredient_amount: 1,
      recipe_ingredient_main_yn: "Y",
      recipe_ingredient_allergy_category: "",
      recipe_ingredient_babystep: "0",
      created_datetime: "",
      last_modified_datetime: ""
    },
    {
      recipe_key: "",
      recipe_ingredient_key: "1000006",
      recipe_common_ingredient_key: "N000001",
      recipe_ingredient_name: "돼지고기",
      recipe_ingredient_category: "기본재료",
      recipe_ingredient_main_category: "돼지고기류",
      recipe_ingredient_countunit: "kg, g",
      recipe_ingredient_main_countunit: "kg",
      recipe_ingredient_amount: 1,
      recipe_ingredient_main_yn: "N",
      recipe_ingredient_allergy_category: "",
      recipe_ingredient_babystep: "0",
      created_datetime: "",
      last_modified_datetime: ""
    },
    {
      recipe_key: "",
      recipe_ingredient_key: "1000005",
      recipe_common_ingredient_key: "N000001",
      recipe_ingredient_name: "간장",
      recipe_ingredient_category: "양념재료",
      recipe_ingredient_main_category: "야채류",
      recipe_ingredient_countunit: "kg, g",
      recipe_ingredient_main_countunit: "kg",
      recipe_ingredient_amount: 1,
      recipe_ingredient_main_yn: "N",
      recipe_ingredient_allergy_category: "",
      recipe_ingredient_babystep: "0",
      created_datetime: "",
      last_modified_datetime: ""
    },
    {
      recipe_key: "",
      recipe_ingredient_key: "1000006",
      recipe_common_ingredient_key: "N000001",
      recipe_ingredient_name: "소금",
      recipe_ingredient_category: "양념재료",
      recipe_ingredient_main_category: "돼지고기류",
      recipe_ingredient_countunit: "kg, g",
      recipe_ingredient_main_countunit: "kg",
      recipe_ingredient_amount: 1,
      recipe_ingredient_main_yn: "N",
      recipe_ingredient_allergy_category: "",
      recipe_ingredient_babystep: "0",
      created_datetime: "",
      last_modified_datetime: ""
    },
  ],
  recipe_ingredient: "",
  recipe_order_list: [
    {
      recipe_key: "",
      recipe_order_key: "1000005",
      recipe_order: "1",
      recipe_order_desc: "닭봉을 우유에 넣어 비린내 제거",
      image_file_key: "1000011",
      image_file_path: "https://d3am0bqv86scod.cloudfront.net/recipe/1000001/1000001-main-JIN01704.jpg"
    },
    {
      recipe_key: "",
      recipe_order_key: "1000006",
      recipe_order: "2",
      recipe_order_desc: "닭봉 끓이다",
      image_file_key: "1000012",
      image_file_path: "https://d3am0bqv86scod.cloudfront.net/recipe/1000001/1000001-main-JIN01704.jpg"
    }
  ],
  recipe_order: "",
  recipe_search_text: "",
  category_main_name: "",
  category_name: "",
  except_ingredient_yn: "",
  recipe_tag_no: 0,
  recipe_tag_desc: "",
  recipe_tag_type: "",
  recipe_select_review: "",
  created_datetime: "",
  last_modified_datetime: "",
  recipe_ingredient_babystep: "",
  recipe_review_cnt_info: {
    recipe_review_cnt_like: 0,
    recipe_review_cnt_great: 0,
    recipe_review_cnt_easy: 0,
    recipe_review_cnt_rich: 0,
    recipe_review_cnt_like_user: 0,
    recipe_review_cnt_great_user: 0,
    recipe_review_cnt_easy_user: 0,
    recipe_review_cnt_rich_user: 0
  },
  recipe_writer_info: {
    recipe_user_key: "",
    recipe_writer_nickname: "parent1",
    recipe_writer_grade: "LV1",
    recipe_writer_thumbnail_path: ""
  },
  recipe_restriction_list: [],
  recipe_allergy_list: [
    {
      recipe_ingredient_name: "토마토",
      ingredient_img_path: "https://donots-ml.s3.ap-northeast-2.amazonaws.com/alergy_source/source_b16.svg",
      ingredient_desc: "토마토는 씨를 같이 먹는 채소로 씨와 붉은색소인 라이코펜이 주요 항원이라 합니다. 12개월 이후 천천히 섭취를 시도해보세요. 토마토는 생으로 섭취하는 경우도 있지만, 일반적으로 음식에 케첩이나 토마토 소스를 넣어 사용하기도 합니다. 따라서 토마토를 재료로 한 소스류 뿐 아니라, 토마토를 손질했던 칼, 도마, 믹서기 등의 교차오염을 주의해주세요.",
      ingredient_baby_allergy_yn: "false"
    },
    {
      recipe_ingredient_name: "닭고기",
      ingredient_img_path: "https://donots-ml.s3.ap-northeast-2.amazonaws.com/alergy_source/source_b17.svg",
      ingredient_desc: "닭고기 알레르기가 있으면 닭을 주재료로 한 요리는 섭취를 피하는 것이 좋습니다. 알레르기를 일으키는 단백질 성분이 열에 비교적 안정적이므로 닭고기를 잘 익혀도 알레르기 반응이 유지됩니다. 닭은 육수의 재료로 흔히 사용되고, 치킨스톡, 닭고기맛분말, 치킨파우더 등 다양하게 식품에 사용되니 섭취 전 식품의 원재료명을 잘 확인해주세요. 알레르기로 인해 닭고기 섭취가 어려운 경우에는 흰살생선이나 해조류, 소고기로 양질의 단백질과 철분을 보충해 주세요.",
      ingredient_baby_allergy_yn: "false"
    }
  ],
  recipe_nutrient_list: [
    {
      recipe_nutrient_name: "탄수화물",
      recipe_nutrient_quantity: "15.0"
    },
    {
      recipe_nutrient_name: "단백질",
      recipe_nutrient_quantity: "100.0"
    },
    {
      recipe_nutrient_name: "식이섬유",
      recipe_nutrient_quantity: "8.0"
    },
    {
      recipe_nutrient_name: "지방",
      recipe_nutrient_quantity: "87.0"
    },
    {
      recipe_nutrient_name: "칼슘",
      recipe_nutrient_quantity: "8.0"
    },
    {
      recipe_nutrient_name: "철",
      recipe_nutrient_quantity: "36.0"
    },
    {
      recipe_nutrient_name: "수분",
      recipe_nutrient_quantity: "8.0"
    },
    {
      recipe_nutrient_name: "비타민A",
      recipe_nutrient_quantity: "32.0"
    },
    {
      recipe_nutrient_name: "비타민D",
      recipe_nutrient_quantity: "0.0"
    },
    {
      recipe_nutrient_name: "비타민E",
      recipe_nutrient_quantity: "5.0"
    },
    {
      recipe_nutrient_name: "비타민C",
      recipe_nutrient_quantity: "17.0"
    },
    {
      recipe_nutrient_name: "티아민",
      recipe_nutrient_quantity: "47.0"
    },
    {
      recipe_nutrient_name: "리보플라빈",
      recipe_nutrient_quantity: "38.0"
    },
    {
      recipe_nutrient_name: "니아신",
      recipe_nutrient_quantity: "100.0"
    },
    {
      recipe_nutrient_name: "엽산",
      recipe_nutrient_quantity: "14.0"
    },
    {
      recipe_nutrient_name: "비타민B12",
      recipe_nutrient_quantity: "0.0"
    },
    {
      recipe_nutrient_name: "아연",
      recipe_nutrient_quantity: "6.0"
    },
    {
      recipe_nutrient_name: "당류",
      recipe_nutrient_quantity: "4.0"
    },
    {
      recipe_nutrient_name: "나트륨",
      recipe_nutrient_quantity: "50.0"
    }
  ],
  recipe_tag_list: [
    {
      recipe_tag_no: "1",
      recipe_tag_desc: "된장"
    },
    {
      recipe_tag_no: "2",
      recipe_tag_desc: " 굿"
    },
    {
      recipe_tag_no: "3",
      recipe_tag_desc: " 레시피"
    }
  ],
  json_allergy_array: [],
  row_num: 0,
  record_size: 0,
  image_file_list: [
    {
      image_file_key: "1000011",
      image_file_path: "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-main-JIN01728.jpg",
    },
    {
      image_file_key: "1000012",
      image_file_path: "https://d3am0bqv86scod.cloudfront.net/recipe/1000002/1000002-recipe-2-JIN01728.jpg",
    }
  ],
  recipe_health_tag: "단백질 풍부, 단백질 풍부, 단백질 풍부, 단백질 풍부",
  restrict_yn: ""
}

export const MOCK_MATERIAL_BASE_SOY = [
  {
    "recipe_ingredient_main_countunit": "스푼",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "P001650",
    "recipe_ingredient_amount": 1.0,
    "row_num": 0,
    "recipe_ingredient_name": "참기름",
    "recipe_ingredient_category": "양념재료",
  },
  {
    "recipe_ingredient_main_countunit": "스푼",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "P001871",
    "recipe_ingredient_amount": 1.0,
    "row_num": 1,
    "recipe_ingredient_name": "올리고당",
    "recipe_ingredient_category": "양념재료",
  },
  {
    "recipe_ingredient_main_countunit": "스푼",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "R003011",
    "recipe_ingredient_amount": 1.0,
    "row_num": 2,
    "recipe_ingredient_name": "간장 개량",
    "recipe_ingredient_category": "양념재료",
  }
]

export const MOCK_MATERIAL_BASE_PEPPER = [
  {
    "recipe_ingredient_main_countunit": "스푼",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "P001871",
    "recipe_ingredient_amount": 1.0,
    "row_num": 0,
    "recipe_ingredient_name": "올리고당",
    "recipe_ingredient_category": "양념재료",
  },
  {
    "recipe_ingredient_main_countunit": "g",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "채소류",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "R000874",
    "recipe_ingredient_amount": 100.0,
    "row_num": 1,
    "recipe_ingredient_name": "마늘 깐마늘",
    "recipe_ingredient_category": "양념재료",
  },
  {
    "recipe_ingredient_main_countunit": "스푼",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "R003011",
    "recipe_ingredient_amount": 1.0,
    "row_num": 2,
    "recipe_ingredient_name": "간장 개량",
    "recipe_ingredient_category": "양념재료",
  },
  {
    "recipe_ingredient_main_countunit": "스푼",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "R003020",
    "recipe_ingredient_amount": 1.0,
    "row_num": 3,
    "recipe_ingredient_name": "고추장 개량",
    "recipe_ingredient_category": "양념재료",
  }
]

export const MOCK_MATERIAL = [
  {
    "recipe_ingredient_main_countunit": "g",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "수산물",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "N000083",
    "recipe_ingredient_amount": 100.0,
    "row_num": 0,
    "recipe_ingredient_name": "군소"
  },
  {
    "recipe_ingredient_main_countunit": "g",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "수산물",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "N000170",
    "recipe_ingredient_amount": 100.0,
    "row_num": 1,
    "recipe_ingredient_name": "다시마 소건품"
  },
  {
    "recipe_ingredient_main_countunit": "g",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "수산물",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "N000209",
    "recipe_ingredient_amount": 100.0,
    "row_num": 2,
    "recipe_ingredient_name": "돌가자미 소건품"
  },
  {
    "recipe_ingredient_main_countunit": "g",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "수산물",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "N000231",
    "recipe_ingredient_amount": 100.0,
    "row_num": 3,
    "recipe_ingredient_name": "둥근돌김 소건품"
  },
  {
    "recipe_ingredient_main_countunit": "g",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "수산물",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "N000239",
    "recipe_ingredient_amount": 100.0,
    "row_num": 4,
    "recipe_ingredient_name": "뜸부기 소건품"
  },
  {
    "recipe_ingredient_main_countunit": "g",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "수산물",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "N000254",
    "recipe_ingredient_amount": 100.0,
    "row_num": 5,
    "recipe_ingredient_name": "매생이 소건품"
  },
  {
    "recipe_ingredient_main_countunit": "g",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "수산물",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "N000266",
    "recipe_ingredient_amount": 100.0,
    "row_num": 6,
    "recipe_ingredient_name": "멸치 소건품"
  },
  {
    "recipe_ingredient_main_countunit": "g",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "수산물",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "N000283",
    "recipe_ingredient_amount": 100.0,
    "row_num": 7,
    "recipe_ingredient_name": "명태 소건품"
  },
  {
    "recipe_ingredient_main_countunit": "g",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "수산물",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "N000315",
    "recipe_ingredient_amount": 100.0,
    "row_num": 8,
    "recipe_ingredient_name": "미역 소건품"
  },
  {
    "recipe_ingredient_main_countunit": "g",
    "recipe_ingredient_countunit": "g, kg, 마리",
    "recipe_ingredient_main_category": "수산물",
    "recipe_ingredient_key": "",
    "recipe_common_ingredient_key": "N000341",
    "recipe_ingredient_amount": 100.0,
    "row_num": 9,
    "recipe_ingredient_name": "바지락 소건품"
  },
]