import {FIND_ID_QUESTIONS_CODE_LIST} from "../constant/sign-up/SignUp";

export const MOCK_SIGN_IN_ID = {
  token: "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJreW9ibzExMSIsInJvbGUiOiJST0xFX1VTRVIiLCJleHAiOjE2NzU5OTMyNzJ9.0xI9wRJz7xlzrhcJCYW_TRiAhXIVlXXsZtG3zGS_CZA",
  refreshToken: "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5MjZEOTZDOTAwMzBERDU4NDI5RDI3NTFBQzFCREJCQyIsImV4cCI6MTY3NjI1MDY3Mn0.PkBpQ8okARqkGxumdJ6FwWkAQF-FdpfdNG-4G6oZe_0",
}

export const MOCK_GET_MEMBER = {
  name: "W3DkFQo8pp/aue35GZvAlg==",
  ci: "Qgu0ubK8JR",
  id: "kyobo111",
  accountKey: 46,
  roleType: "USER",
  token: "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJreW9ibzExMSIsInJvbGUiOiJST0xFX1VTRVIiLCJleHAiOjE2NzU5OTMyNzJ9.0xI9wRJz7xlzrhcJCYW_TRiAhXIVlXXsZtG3zGS_CZA",
  refreshToken: "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5MjZEOTZDOTAwMzBERDU4NDI5RDI3NTFBQzFCREJCQyIsImV4cCI6MTY3NjI1MDY3Mn0.PkBpQ8okARqkGxumdJ6FwWkAQF-FdpfdNG-4G6oZe_0",
  createdAt: "2023-02-10 01:07:21",
  lastSignInAt: "2023-02-10 01:07:21",
  memberKey: 133
}

export const MOCK_USER_DETAIL = {
  profilePictureUrl: "",
  marketingInfoPushNotifSettingModifiedDatetime: "",
  birthDay: "1989-01-01",
  profileSelectedBabyKey: 105,
  briefBio: "",
  babies: [
    {
      key: 105,
      parentKey: 104,
      nickname: "김영영1",
      birthdate: "2020-01-01",
      height: "123.1",
      weight: "23.3",
      gender: "MALE",
      profilePictureUrl: "",
      profilePictureThumbnailOrder: 0,
      allergyIngredients: [
        {
          key: "ALG000004",
          name: "메밀"
        }
      ],
      concerns: [
        {
          key: "HINT000010",
          name: "변비"
        }
      ]
    }
  ],
  type: "NORMAL_MEMBER",
  emailReceiveAgreementModifiedDatetime: "",
  isTermsCollectingPersonalDataMarketingAgreed: true,
  accountKey: 35,
  isEmailReceiveAgreed: true,
  phoneNumber: "01099939893",
  socialMediaUrl: "",
  isPostCensorshipResultPushNotifSet: true,
  grade: "LV1",
  nickname: "김영영",
  textMessageReciveAgreementModifiedDatetime: "",
  isMarketingInfoPushNotifSet: true,
  isTextMessageReciveAgreed: true,
  key: 104,
  email: "kimzerozero1@gmail.com"
}

export const MOCK_AUTH_CODE = '000000'

export const MOCK_IS_ACCOUNT = false

export const MOCK_FIND_ID = {
  id: 'kyobo112',
  createdAt: '2023-01-30 08:44:56.386018',
  snsInfoDtoList: []
}

export const MOCK_CHECK_ID_CI = true

export const MOCK_EXIST_ID = true

export const MOCK_ID = false

export const MOCK_RESET_PASSWORD = ''

export const MOCK_NICKNAME = false

export const MOCK_EMAIL = false

export const MOCK_REQUEST_CERT = {
  transactionSeqNumber: ''
}

export const MOCK_CHECK_SIGN_UP_NO = {
  isExist: false,
  ci: "888888",
}

export const MOCK_CHECK_SIGN_UP = {
  isExist: true,
  ci: "777777",
  data: {
    id: "kyobo111",
    createdAt: "2023-02-06 07:41:00.139787",
    snsInfoDtoList: [
      {
        provider: "NAVER",
        email: "kyobo1313@nate.com"
      }
    ]
  }
}

export const MOCK_CHECK_CI_ID = {
  ci: "777777",
  isMatched: true
}

export const MOCK_CHECK_CERT = '5556'

export const MOCK_ADD_CHILDREN = ''

export const MOCK_SIGN_UP_ID = {
  name: "VaRDev+yQhEWqWoZB+B/rA==",
  ci: "fjow4139120",
  id: "kyobo781",
  accountKey: 44,
  roleType: "USER",
  token: "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJreW9ibzc4MSIsInJvbGUiOiJST0xFX1VTRVIiLCJleHAiOjE2NzUzMjA5MDh9.dk15mb7zgBF5TFnGsyb89mr3P91upUGB3ybJ8fWu2ZQ",
  createdAt: "2023-02-02 06:55:08.119307",
  lastSignInAt: "2023-02-02 06:55:08.119307",
  memberKey: 163
}

export const MOCK_SIGN_UP_SNS = {
  name: "yYShO18DNXttczaFm7F55g==",
  ci: "333444335",
  accountKey: 51,
  roleType: "USER",
  createdAt: "2023-02-02 08:30:55.688948",
  lastSignInAt: "2023-02-02 08:30:55.688948",
  socialEmail: "nayadk123@nate.com",
  memberKey: 177
}

export const MOCK_UUID = '89356487-54fa-435a-b344-5dccbbc3ee89'

export const MOCK_TOKEN = {
  token: "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJreW9ibzExMSIsInJvbGUiOiJST0xFX1VTRVIiLCJleHAiOjE2NzU5OTMyNzJ9.0xI9wRJz7xlzrhcJCYW_TRiAhXIVlXXsZtG3zGS_CZA",
  refreshToken: "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5MjZEOTZDOTAwMzBERDU4NDI5RDI3NTFBQzFCREJCQyIsImV4cCI6MTY3NjI1MDY3Mn0.PkBpQ8okARqkGxumdJ6FwWkAQF-FdpfdNG-4G6oZe_0",
  socialAccountId: "2636260150",
  isAlreadyConnectedSocialAccount: false
}

export const MOCK_ADD_SNS = ''

export const MOCK_BY_EMAIL_IDENTIFY_VERIFICATION_QUESTIONS = {
  isPhoneIdentityVerificationUsageAgreed: false,
  identityVerificationQuestions: [FIND_ID_QUESTIONS_CODE_LIST.FAVORITE_FOOD]
}

export const MOCK_BY_ID_IDENTIFY_VERIFICATION_QUESTIONS = {
  isPhoneIdentityVerificationUsageAgreed: false,
  identityVerificationQuestions: [FIND_ID_QUESTIONS_CODE_LIST.FAVORITE_FOOD, FIND_ID_QUESTIONS_CODE_LIST.FAVORITE_SONG_TITLE]
}

export const MOCK_BY_EMAIL_AND_IDENTIFY_VERIFICATION_QAS = {
  id: "foo"
}