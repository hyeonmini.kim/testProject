/*
recipe_detail_info: {
  recipe_key: "1000018",
  recipe_name: "김치찌개",
  recipe_desc: "돼지고기를 넣은 김치찌개",
  recipe_category: "국/찌개",
  recipe_health_develop: "[모발]",
  recipe_lead_time: "30분이내",
  recipe_level: "할만해요",
  recipe_babyfood_step: "초기 이유식",
  recipe_health_note: "아이들이 안 좋아하는 음식",
  recipe_scrap_cnt: 11,
  recipe_view_cnt: 109,
  recipe_main_img_path: "https://d3am0bqv86scod.cloudfront.net/recipe/1000004/main/1000004-main-JIN01728.jpg",
  recipe_img_path_1: "d3am0bqv86scod.cloudfront.net/recipe/1000018/sub/1000018-sub-1-20221215133327-JIN08420.jpg",
  recipe_img_path_2: "d3am0bqv86scod.cloudfront.net/recipe/1000018/sub/1000018-sub-1-20221215133327-JIN08420.jpg",
  recipe_img_path_3: "d3am0bqv86scod.cloudfront.net/recipe/1000018/sub/1000018-sub-1-20221215133327-JIN08420.jpg",
  recipe_img_path_4: "d3am0bqv86scod.cloudfront.net/recipe/1000018/sub/1000018-sub-1-20221215133327-JIN08420.jpg",
  recipe_servings: 1,
  recipe_cal: 500,
  recipe_temp_step: "요리순서",
  recipe_check_status: "임시저장중",
  recipe_ingredient_list: [
    {
      recipe_ingredient_key: "1000026",
      recipe_ingredient_name: "김치",
      recipe_ingredient_category: "0",
      recipe_ingredient_main_category: "버섯",
      recipe_ingredient_countunit: "kg, g",
      recipe_ingredient_main_countunit: "kg",
      recipe_ingredient_amount: "1",
      recipe_ingredient_main_yn: "N",
      recipe_ingredient_allergy_category: "N"
    },
    {
      recipe_ingredient_key: "1000027",
      recipe_ingredient_name: "돼지고기",
      recipe_ingredient_category: "0",
      recipe_ingredient_main_category: "돼지고기류",
      recipe_ingredient_countunit: "kg, g",
      recipe_ingredient_main_countunit: "kg",
      recipe_ingredient_amount: "1",
      recipe_ingredient_main_yn: "N",
      recipe_ingredient_allergy_category: "우유",
      recipe_ingredient_babystep: "12"
    }
  ],
  recipe_order_list: [
    {
      recipe_order_key: "1000021",
      recipe_order: "1",
      recipe_order_desc: "닭봉을 우유에 넣어 비린내 제거"
    },
    {
      recipe_order_key: "1000022",
      recipe_order: "2",
      recipe_order_desc: "닭봉 끓이다"
    }
  ],
  recipe_tag_no: 0,
  recipe_select_review: "아이취향,조리쉬움",
  recipe_review_cnt_info: {
    recipe_review_cnt_1: 1,
    recipe_review_cnt_2: 0,
    recipe_review_cnt_3: 1,
    recipe_review_cnt_4: 0
  },
  recipe_writer_nickname: '부천불주먹',
  recipe_writer_info: {},
  recipe_restriction_list: [
    {
      ingredient_name: "꿀",
      ingredient_img_path: "https://s3.ap-northeast-2.amazonaws.com/mybucket/puppy.jpg",
      ingredient_age: "12개월~",
      ingredient_desc: "클로스트리움 보틀리늄균 오염으로 발생하는..."
    }
  ],
  recipe_allergy_list: [
    {
      ingredient_name: "우유",
      ingredient_img_path: "https://s3.ap-northeast-2.amazonaws.com/mybucket/milk.jpg",
      ingredient_desc: "우유 알레르기의 대표적인 증상으로는 복통이나 구토를 동반한...",
      ingredient_baby_allergy_yn: "0"
    }
  ],
  recipe_nutrient_list: [
    {
      recipe_nutrient_name: "탄수화물"
    }
  ],
  recipe_tag_list: [
    {
      recipe_tag_no: "1",
      recipe_tag_desc: "김치"
    },
    {
      recipe_tag_no: "1",
      recipe_tag_desc: "식이섬유풍부"
    },
    {
      recipe_tag_no: "2",
      recipe_tag_desc: "칼슘풍부"
    }
  ],
  json_allergy_array: [],
  last_num: 0,
  record_size: 0
}
*/

export const MOCK_GET_RECIPE_ALL = [
  {
    recipe_key: 1000002,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000008,
    recipe_name: "시금치 된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000003,
    recipe_name: "명태조림",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000006,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000004,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000005,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000007,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000009,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000010,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000014,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000015,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000027,
    recipe_name: "시금치 볶음",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000016,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000024,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000025,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000011,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000012,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000026,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000013,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000018,
    recipe_name: "문어숙회",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000017,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000019,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000020,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000021,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000022,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000023,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  }
];

export const MOCK_GET_RECIPE_ALL2 = [
  {
    recipe_key: 1000002,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000008,
    recipe_name: "시금치 된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000003,
    recipe_name: "명태조림",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000006,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000004,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000005,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000007,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000009,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000010,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000014,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000015,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000027,
    recipe_name: "시금치 볶음",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000016,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000024,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000025,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000011,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000012,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000026,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000013,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000018,
    recipe_name: "문어숙회",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000017,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000019,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000020,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000021,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000022,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  },
  {
    recipe_key: 1000023,
    recipe_name: "된장찌개",
    recipe_user_key: "1",
    recipe_user_name: "parent1"
  }
];

export const MOCK_GET_RECIPE_DETAIL =
  {
    recipe_user_key: "",
    recipe_key: "1000018",
    recipe_name: "김치찌개",
    recipe_desc: "돼지고기를 넣은 김치찌개",
    recipe_category: "국/찌개",
    recipe_health_develop: "[모발]",
    recipe_lead_time: "30분이내",
    recipe_level: "할만해요",
    recipe_babyfood_step: "초기이유식",
    recipe_health_note: "아이들이 안 좋아하는 음식아이들이 안 좋아하는 음식아이들이 안 좋아하는 음식아이들이 안 좋아하는 음식아이들이 안 좋아하는 음식아이들이 안 좋아하는 음식아이들이 안 좋아하는 음식",
    recipe_scrap_cnt: 11,
    recipe_view_cnt: 109,
    recipe_scrap_yn: "Y",
    image_file_list: [
      {
        "image_file_key": "1000016",
        "image_file_path": "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg"
      },
      {
        "image_file_key": "1000018",
        "image_file_path": "https://recipe1.ezmember.co.kr/cache/recipe/2018/02/13/e0649f3f0b446f39c9a7bdff70d953691.jpg"
      }
    ],
    recipe_servings: 1,
    recipe_cal: 500,
    recipe_temp_step: "요리순서",
    recipe_check_status: "임시저장중",
    recipe_ingredient_list: [
      {
        recipe_ingredient_key: "1000026",
        recipe_ingredient_name: "김치",
        recipe_ingredient_category: "기본재료",
        recipe_ingredient_main_category: "버섯",
        recipe_ingredient_countunit: "kg, g",
        recipe_ingredient_main_countunit: "kg",
        recipe_ingredient_amount: "1",
        recipe_ingredient_main_yn: "N",
        recipe_ingredient_allergy_category: "N"
      },
      {
        recipe_ingredient_key: "1000027",
        recipe_ingredient_name: "돼지고기",
        recipe_ingredient_category: "기본재료",
        recipe_ingredient_main_category: "돼지고기류",
        recipe_ingredient_countunit: "kg, g",
        recipe_ingredient_main_countunit: "kg",
        recipe_ingredient_amount: "1",
        recipe_ingredient_main_yn: "N",
        recipe_ingredient_allergy_category: "우유",
        recipe_ingredient_babystep: "12"
      }
    ],
    recipe_order_list: [
      {
        recipe_order_key: "1000021",
        recipe_order: "1",
        recipe_order_desc: "닭봉을 우유에 넣어 비린내 제거",
        image_file_path: "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg"
      },
      {
        recipe_order_key: "1000022",
        recipe_order: "2",
        recipe_order_desc: "닭봉 끓이다",
        image_file_path: "https://recipe1.ezmember.co.kr/cache/recipe/2018/02/13/e0649f3f0b446f39c9a7bdff70d953691.jpg"
      }
    ],
    recipe_tag_no: 0,
    recipe_review_cnt_info: {
      recipe_review_cnt_like: 5,
      recipe_review_cnt_great: 0,
      recipe_review_cnt_easy: 5,
      recipe_review_cnt_rich: 0,
      recipe_review_cnt_like_user: 1,
      recipe_review_cnt_great_user: 0,
      recipe_review_cnt_easy_user: 1,
      recipe_review_cnt_rich_user: 0
    },
    recipe_writer_nickname: '당산비룡',
    recipe_writer_thumnail_path: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_19.jpg',
    recipe_writer_info: {
      recipe_user_key: 1,
      recipe_writer_nickname: '당산비룡',
      recipe_writer_thumnail_path: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_19.jpg',
      recipe_writer_grade: "LV1",
      recipe_writer_baby_nickname: "화이트",
      recipe_writer_type: "NORMAL_MEMBER"
    },
    recipe_restriction_list: [
      {
        ingredient_name: "꿀",
        ingredient_img_path: "https://cdn-icons-png.flaticon.com/512/5721/5721737.png",
        ingredient_age: "12개월~",
        ingredient_desc: "클로스트리움 보틀리늄균 오염으로 발생하는..."
      }
    ],
    recipe_allergy_list: [
      {
        ingredient_name: "우유",
        ingredient_img_path: "https://en.pimg.jp/018/861/665/1/18861665.jpg",
        ingredient_desc: "우유 알레르기의 대표적인 증상으로는 복통이나 구토를 동반한...",
        ingredient_baby_allergy_yn: "Y"
      }
    ],
    recipe_nutrient_list: [
      {
        recipe_nutrient_name: "탄수화물",
        recipe_nutrient_quantity: "1",
      },
      {
        recipe_nutrient_name: "단백질",
        recipe_nutrient_quantity: "2",
      },
      {
        recipe_nutrient_name: "식이섬유",
        recipe_nutrient_quantity: "3",
      },
      {
        recipe_nutrient_name: "지방",
        recipe_nutrient_quantity: "4",
      },
      {
        recipe_nutrient_name: "칼슘",
        recipe_nutrient_quantity: "5",
      },
      {
        recipe_nutrient_name: "철",
        recipe_nutrient_quantity: "6",
      },
      {
        recipe_nutrient_name: "수분",
        recipe_nutrient_quantity: "7",
      },
      {
        recipe_nutrient_name: "비타민A",
        recipe_nutrient_quantity: "8",
      },
      {
        recipe_nutrient_name: "비타민D",
        recipe_nutrient_quantity: "9",
      },
      {
        recipe_nutrient_name: "비타민E",
        recipe_nutrient_quantity: "10",
      },
      {
        recipe_nutrient_name: "비타민C",
        recipe_nutrient_quantity: "11",
      },
      {
        recipe_nutrient_name: "티아민",
        recipe_nutrient_quantity: "12",
      },
      {
        recipe_nutrient_name: "리보플라빈",
        recipe_nutrient_quantity: "13",
      },
      {
        recipe_nutrient_name: "니아신",
        recipe_nutrient_quantity: "14",
      },
      {
        recipe_nutrient_name: "엽산",
        recipe_nutrient_quantity: "15",
      },
      {
        recipe_nutrient_name: "비타민B12",
        recipe_nutrient_quantity: "16",
      },
      {
        recipe_nutrient_name: "아연",
        recipe_nutrient_quantity: "17",
      },
      {
        recipe_nutrient_name: "당류",
        recipe_nutrient_quantity: "100",
      },
      {
        recipe_nutrient_name: "나트륨",
        recipe_nutrient_quantity: "101",
      },
    ],
    recipe_tag_list: [
      {
        recipe_tag_no: "1",
        recipe_tag_desc: "김치"
      },
      {
        recipe_tag_no: "1",
        recipe_tag_desc: "식이섬유풍부"
      },
      {
        recipe_tag_no: "2",
        recipe_tag_desc: "칼슘풍부"
      }
    ],
    json_allergy_array: [],
    last_num: 0,
    record_size: 0,
    recipe_health_tag: "식이섬유풍부,칼슘풍부,식이섬유풍부,칼슘풍부,식이섬유풍부,칼슘풍부",
  }

export const MOCK_GET_RECIPE_DETAIL2 =
  {
    recipe_key: "1000011",
    recipe_name: "김치찌개",
    recipe_desc: "돼지고기를 넣은 김치찌개",
    recipe_category: "국/찌개",
    recipe_health_develop: "[모발]",
    recipe_lead_time: "30분이내",
    recipe_level: "할만해요",
    recipe_babyfood_step: "초기이유식",
    recipe_health_note: "아이들이 안 좋아하는 음식아이들이 안 좋아하는 음식아이들이 안 좋아하는 음식아이들이 안 좋아하는 음식아이들이 안 좋아하는 음식아이들이 안 좋아하는 음식아이들이 안 좋아하는 음식",
    recipe_scrap_cnt: 11,
    recipe_view_cnt: 109,
    recipe_scrap_yn: "Y",
    image_file_list: [
      {
        "image_file_key": "1000016",
        "image_file_path": "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg"
      },
      {
        "image_file_key": "1000018",
        "image_file_path": "https://recipe1.ezmember.co.kr/cache/recipe/2018/02/13/e0649f3f0b446f39c9a7bdff70d953691.jpg"
      }
    ],
    recipe_servings: 1,
    recipe_cal: 500,
    recipe_temp_step: "요리순서",
    recipe_check_status: "임시저장중",
    recipe_ingredient_list: [
      {
        recipe_ingredient_key: "1000026",
        recipe_ingredient_name: "김치",
        recipe_ingredient_category: "기본재료",
        recipe_ingredient_main_category: "버섯",
        recipe_ingredient_countunit: "kg, g",
        recipe_ingredient_main_countunit: "kg",
        recipe_ingredient_amount: "1",
        recipe_ingredient_main_yn: "N",
        recipe_ingredient_allergy_category: "N"
      },
      {
        recipe_ingredient_key: "1000027",
        recipe_ingredient_name: "돼지고기",
        recipe_ingredient_category: "기본재료",
        recipe_ingredient_main_category: "돼지고기류",
        recipe_ingredient_countunit: "kg, g",
        recipe_ingredient_main_countunit: "kg",
        recipe_ingredient_amount: "1",
        recipe_ingredient_main_yn: "N",
        recipe_ingredient_allergy_category: "우유",
        recipe_ingredient_babystep: "12"
      }
    ],
    recipe_order_list: [
      {
        recipe_order_key: "1000021",
        recipe_order: "1",
        recipe_order_desc: "닭봉을 우유에 넣어 비린내 제거",
        image_file_path: "https://recipe1.ezmember.co.kr/cache/recipe/2022/09/21/bb2cd8c9af0ab1f268c4567275f182111.jpg"
      },
      {
        recipe_order_key: "1000022",
        recipe_order: "2",
        recipe_order_desc: "닭봉 끓이다",
        image_file_path: "https://recipe1.ezmember.co.kr/cache/recipe/2018/02/13/e0649f3f0b446f39c9a7bdff70d953691.jpg"
      }
    ],
    recipe_tag_no: 0,
    recipe_review_cnt_info: {
      recipe_review_cnt_like: 5,
      recipe_review_cnt_great: 0,
      recipe_review_cnt_easy: 5,
      recipe_review_cnt_rich: 0,
      recipe_review_cnt_like_user: 1,
      recipe_review_cnt_great_user: 0,
      recipe_review_cnt_easy_user: 1,
      recipe_review_cnt_rich_user: 0
    },
    recipe_writer_nickname: '당산비룡',
    recipe_writer_thumnail_path: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_19.jpg',
    recipe_writer_info: {
      recipe_user_key: 1,
      recipe_writer_nickname: '당산비룡',
      recipe_writer_thumnail_path: 'https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_19.jpg',
      recipe_writer_grade: "LV1",
      recipe_writer_baby_nickname: "화이트",
      recipe_writer_type: "NORMAL_MEMBER"
    },
    recipe_restriction_list: [
      {
        ingredient_name: "꿀",
        ingredient_img_path: "https://cdn-icons-png.flaticon.com/512/5721/5721737.png",
        ingredient_age: "12개월~",
        ingredient_desc: "클로스트리움 보틀리늄균 오염으로 발생하는..."
      }
    ],
    recipe_allergy_list: [
      {
        ingredient_name: "우유",
        ingredient_img_path: "https://en.pimg.jp/018/861/665/1/18861665.jpg",
        ingredient_desc: "우유 알레르기의 대표적인 증상으로는 복통이나 구토를 동반한...",
        ingredient_baby_allergy_yn: "Y"
      }
    ],
    recipe_nutrient_list: [
      {
        recipe_nutrient_name: "탄수화물",
        recipe_nutrient_quantity: "1",
      },
      {
        recipe_nutrient_name: "단백질",
        recipe_nutrient_quantity: "2",
      },
      {
        recipe_nutrient_name: "식이섬유",
        recipe_nutrient_quantity: "3",
      },
      {
        recipe_nutrient_name: "지방",
        recipe_nutrient_quantity: "4",
      },
      {
        recipe_nutrient_name: "칼슘",
        recipe_nutrient_quantity: "5",
      },
      {
        recipe_nutrient_name: "철",
        recipe_nutrient_quantity: "6",
      },
      {
        recipe_nutrient_name: "수분",
        recipe_nutrient_quantity: "7",
      },
      {
        recipe_nutrient_name: "비타민A",
        recipe_nutrient_quantity: "8",
      },
      {
        recipe_nutrient_name: "비타민D",
        recipe_nutrient_quantity: "9",
      },
      {
        recipe_nutrient_name: "비타민E",
        recipe_nutrient_quantity: "10",
      },
      {
        recipe_nutrient_name: "비타민C",
        recipe_nutrient_quantity: "11",
      },
      {
        recipe_nutrient_name: "티아민",
        recipe_nutrient_quantity: "12",
      },
      {
        recipe_nutrient_name: "리보플라빈",
        recipe_nutrient_quantity: "13",
      },
      {
        recipe_nutrient_name: "니아신",
        recipe_nutrient_quantity: "14",
      },
      {
        recipe_nutrient_name: "엽산",
        recipe_nutrient_quantity: "15",
      },
      {
        recipe_nutrient_name: "비타민B12",
        recipe_nutrient_quantity: "16",
      },
      {
        recipe_nutrient_name: "아연",
        recipe_nutrient_quantity: "17",
      },
      {
        recipe_nutrient_name: "당류",
        recipe_nutrient_quantity: "100",
      },
      {
        recipe_nutrient_name: "나트륨",
        recipe_nutrient_quantity: "101",
      },
    ],
    recipe_tag_list: [
      {
        recipe_tag_no: "1",
        recipe_tag_desc: "김치"
      },
      {
        recipe_tag_no: "1",
        recipe_tag_desc: "식이섬유풍부"
      },
      {
        recipe_tag_no: "2",
        recipe_tag_desc: "칼슘풍부"
      }
    ],
    json_allergy_array: [],
    last_num: 0,
    record_size: 0,
    recipe_health_tag: "식이섬유풍부,칼슘풍부,식이섬유풍부,칼슘풍부,식이섬유풍부,칼슘풍부",
  }
