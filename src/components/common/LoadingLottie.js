import {useEffect, useState} from 'react';
// @mui
import {styled} from '@mui/material/styles';
//
import Lottie from "react-lottie-player";
import LoadingJson from "../../../public/assets/icons/lottie/Loading.json";
// ----------------------------------------------------------------------

const StyledRoot = styled('div')(({theme}) => ({
  right: 0,
  bottom: 0,
  zIndex: 9998,
  width: '100%',
  height: '100%',
  position: 'fixed',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: 'rgba(0, 0, 0, 0.1)',
}));

// ----------------------------------------------------------------------

export default function LoadingLottie() {
  const [mounted, setMounted] = useState(false);

  useEffect(() => setMounted(true), []);

  if (!mounted) {
    return null;
  }

  return (
    <StyledRoot>
      <Lottie
        animationData={LoadingJson}
        play
        speed={2}
        style={{width: '300px', height: '300px', marginBottom: '100px'}}
      />
    </StyledRoot>
  );
}
