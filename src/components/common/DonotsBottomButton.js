import {Box, Button} from "@mui/material";
import {useRecoilValue} from "recoil";
import {authBottomButtonMarginState, authBottomButtonPositionState, authBottomButtonState} from "../../recoil/atom/sign-up/auth";
import PropTypes from "prop-types";
import {clickBorderNone} from "../../constant/common/Common";

DonotsBottomButton.propTypes = {
  handleClick: PropTypes.func.isRequired,
  buttonText: PropTypes.string,
}

export default function DonotsBottomButton({tabIndex, handleClick, buttonText}) {
  // 버튼 위치 조정 state
  const buttonPosition = useRecoilValue(authBottomButtonPositionState);
  const buttonMargin = useRecoilValue(authBottomButtonMarginState);
  // 버튼 제어 state
  const isDisabledButton = useRecoilValue(authBottomButtonState);

  return (
    <Box
      sx={{
        position: buttonPosition,
        bottom: 0,
        left: 0,
        right: 0,
        margin: buttonMargin,
      }}
    >
      <Button
        tabIndex={tabIndex}
        disableFocusRipple
        fullWidth
        variant="contained"
        disabled={isDisabledButton}
        onClick={handleClick}
        sx={{
          height: '52px',
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '16px',
          lineHeight: '28px',
          textAlign: 'center',
          color: '#FFFFFF',
          '&.Mui-disabled': {
            color: '#B3B3B3',
            backgroundColor: '#E6E6E6',
          },
          '&:hover': {
            boxShadow: 'none',
            backgroundColor: 'primary.main',
          },
          ...clickBorderNone
        }}
      >
        {buttonText}
      </Button>
    </Box>
  );
}