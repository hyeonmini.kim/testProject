import ReactLoading from "react-loading";
import {Box} from "@mui/material";

export default function Loader() {
  return (
    <Box
      sx={{
        width: '100%',
        height: '80%',
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
        alignItems: 'center',
      }}
    >
      <ReactLoading type="spin" color="primary.main"/>
    </Box>
  );
};
