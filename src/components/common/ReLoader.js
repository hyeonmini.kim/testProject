import {Box} from "@mui/material";
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';

export default function ReLoader() {
  return (
    <Box
      sx={{
        width: '100%',
        height: '80%',
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
        alignItems: 'center',
      }}
    >
      <AddCircleOutlineIcon sx={{color: 'gray', fontSize: '65px'}}/>
    </Box>
  );
};
