import {useEffect, useState} from 'react';
import {styled} from '@mui/material/styles';
import {keyframes} from "@mui/system";
import {Z_INDEX} from "../../constant/common/ZIndex";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {getRandom} from "../../utils/formatNumber";
import {sxFixedCenterMainLayout} from "../../layouts/main/MainLayout";

const loadingD = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

const StyledRoot = styled('div')(({dim, bgWhite}) => ({
  right: 0,
  bottom: 0,
  zIndex: Z_INDEX.LOADING,
  width: '100%',
  height: '100%',
  position: 'fixed',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: dim ? 'rgba(0, 0, 0, 0.15)' : bgWhite ? '#FFFFFF' : 'transparent',
  ...sxFixedCenterMainLayout
}));

const StyledDialog = styled('div')(({dim}) => ({
  right: 0,
  bottom: 0,
  zIndex: Z_INDEX.LOADING,
  width: '100%',
  height: '100%',
  position: 'fixed',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: dim ? 'rgba(0, 0, 0, 0.15)' : 'transparent',
}));

export default function LoadingScreen({dialog, dim = false, bgWhite = false}) {
  const [mounted, setMounted] = useState(false);

  useEffect(() => setMounted(true), []);

  if (!mounted) {
    return
  }

  let ICON = ICON_TYPE.LOGO_SPRINKLE_46PX
  switch (getRandom(3)) {
    case 0:
      ICON = ICON_TYPE.LOGO_CHOCO_46PX
      break;
    case 1:
      ICON = ICON_TYPE.LOGO_PLAIN_46PX
      break;
  }

  if (dialog) {
    return (
      <StyledDialog dim={dim}>
        <SvgCommonIcons type={ICON} sx={{
          width: '46px',
          animation: `${loadingD} 1.2s cubic-bezier(.21,.3,.13,.71) infinite;`
        }}/>
      </StyledDialog>
    );
  } else {
    return (
      <StyledRoot dim={dim} bgWhite={bgWhite}>
        <SvgCommonIcons type={ICON} sx={{
          width: '46px',
          animation: `${loadingD} 1.2s cubic-bezier(.21,.3,.13,.71) infinite;`
        }}/>
      </StyledRoot>
    );
  }
}
