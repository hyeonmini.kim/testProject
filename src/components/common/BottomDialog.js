import {Box, Dialog, DialogContent, DialogTitle, Paper, Slide,} from "@mui/material";
import * as React from "react";
import Draggable from "react-draggable";
import {useRecoilState, useRecoilValue} from "recoil";
import {commonBottomDialogState, commonBottomDialogTitleState} from "../../recoil/atom/common/bottomDialog";
import {Z_INDEX} from "../../constant/common/ZIndex";
import {sxFixedCenterMainLayout} from "../../layouts/main/MainLayout";

export default function BottomDialog({children}) {
  const [isOpen, setIsOpen] = useRecoilState(commonBottomDialogState);
  const isTitle = useRecoilValue(commonBottomDialogTitleState);

  const transition = React.forwardRef(
    function Transition(props, ref) {
      return <Slide direction="up" ref={ref} {...props} />;
    }
  );

  const handleClick = (e) => {
    e.preventDefault();
  }

  const handleClose = (event, reason) => {
    if (reason === "backdropClick") {
      return;
    }
    setIsOpen(false);
  }

  const onStopHandler = (e, data) => {
    if (data.y > (data.node.offsetHeight / 2)) {
      // 다이얼로그 사라짐
      setIsOpen(false);
    }
  }

  const paperComponent = (props) => {
    return (
      <Draggable
        handle="#draggable-dialog-title"
        cancel={'[class*="MuiDialogContent-root"]'}
        axis="y"
        bounds={{top: 0}}
        onStop={onStopHandler}
        position={{x: 0, y: 0}}
      >
        <Paper {...props} />
      </Draggable>
    );
  }

  return (
    <Dialog
      open={isOpen}
      onClose={handleClose}
      onClick={handleClick}
      TransitionComponent={transition}
      disableScrollLock={true}
      fullWidth
      PaperComponent={paperComponent}
      aria-labelledby="draggable-dialog-title"
      PaperProps={{
        style: {
          margin: 0,
          borderRadius: '16px 16px 0 0',
          position: 'fixed',
          bottom: 0,
          backgroundColor: '#FFFFFF',
        },
      }}
      sx={{
        visibility: isOpen ? 'visible' : 'hidden',
        zIndex: Z_INDEX.SLIDE_BOTTOM_DIALOG,
        ...sxFixedCenterMainLayout
      }}
    >
      <DialogTitle
        id="draggable-dialog-title"
        sx={{
          textAlign: '-webkit-center',
          p: '10px 0 23px 0',
          display: isTitle ? 'block' : 'none'
        }}
      >
        <Box sx={{width: '50px', height: '7px', backgroundColor: '#CCCCCC', borderRadius: '5.5px'}}/>
      </DialogTitle>
      <DialogContent sx={{margin: '0 20px 20px 20px', padding: 0, textAlign: '-webkit-center'}}>
        {children}
      </DialogContent>
    </Dialog>
  );
}