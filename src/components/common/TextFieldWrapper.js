import {TextField} from "@mui/material";

export default function TextFieldWrapper({...other}) {
  return (
    <TextField
      {...other}
      inputProps={{autoCorrect: 'off', ...other?.inputProps}}
    />
  )
}
