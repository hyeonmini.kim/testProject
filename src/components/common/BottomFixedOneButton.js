import PropTypes from "prop-types";
import {Box, Button, Container, Grid} from "@mui/material";
import {Z_INDEX} from "../../constant/common/ZIndex";
import {clickBorderNone} from "../../constant/common/Common";

BottomFixedOneButton.propTypes = {
  buttonProps: PropTypes.object,
  sx: PropTypes.object,
};

export default function BottomFixedOneButton({buttonProps, sx}) {
  if (!buttonProps) return

  return (
    <Box
      sx={{
        p: '20px',
        position: 'fixed',
        zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
        bgcolor: 'white',
        bottom: 0,
        left: 0,
        right: 0,
        ...sx,
      }}
    >
      <Container maxWidth={'xs'} disableGutters>
        <Button
          tabIndex={1}
          disableFocusRipple
          fullWidth
          variant="contained"
          onClick={buttonProps?.onClick}
          disabled={buttonProps?.isDisabled}
          sx={{
            px: 0,
            height: '52px',
            fontSize: '16px',
            fontWeight: '700',
            lineHeight: '28px',
            '&:hover': {
              boxShadow: 'none',
            },
            ...clickBorderNone,
          }}
        >
          {buttonProps?.text}
        </Button>
      </Container>
    </Box>
  )
}