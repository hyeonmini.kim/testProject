import {InputAdornment, TextField} from "@mui/material";
import PropTypes from "prop-types";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {useState} from "react";
import {ALT_STRING} from "../../constant/common/AltString";

DonotsTextField.propTypes = {
  handleChange: PropTypes.func.isRequired,
  helperText: PropTypes.string,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  setValue: PropTypes.any.isRequired,
  isValidate: PropTypes.bool,
};

export default function DonotsTextField(
  {
    tabIndex,
    handleChange,
    helperText = '',
    label,
    value,
    type,
    setValue,
    isValidate = true,
    onEnterKeyDown,
  }) {

  const [focus, setFocus] = useState(false)

  const handleFocus = () => setFocus(true)
  const handleBlur = () => setFocus(false)
  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      onEnterKeyDown ? onEnterKeyDown() : null
    }
  }
  const handleCancel = () => setValue('')

  return (
    <>
      <TextField
        fullWidth
        autoComplete='off'
        value={value}
        label={value === '' ? `${label}` : ' '}
        type={type}
        onChange={handleChange}
        onFocus={handleFocus}
        onBlur={handleBlur}
        onKeyDown={handleKeyDown}
        variant="standard"
        helperText={value === '' ? helperText : isValidate ? '' : helperText}
        inputProps={{tabIndex: tabIndex, autoCorrect: 'off'}}
        InputProps={{
          endAdornment: value && focus && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
            </InputAdornment>
          ),
          style: {
            paddingBottom: '9px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#222222',
          },
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#CCCCCC',
          }
        }}
        FormHelperTextProps={{
          style: {
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            letterSpacing: '-0.03em',
            color: value === '' ? '#888888' : isValidate ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          marginTop: '4px',
          '& .MuiInput-root:before': {
            borderBottom: value === '' ? '1px solid #CCCCCC' : isValidate ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: value === '' ? '1px solid #CCCCCC' : isValidate ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: value === '' ? '1px solid #222222' : isValidate ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: value === '' ? '1px solid #222222' : isValidate ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: value === '' ? '1px solid #222222' : isValidate ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </>
  );
}