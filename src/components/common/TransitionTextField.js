import {Box, Button, Stack, Typography} from "@mui/material";
import {useState} from "react";
import TextFieldWrapper from "./TextFieldWrapper";
import {clickBorderNone, COMMON_DIALOG_TYPE} from "../../constant/common/Common";
import {LAB_TEXT, TTF_TYPE} from "../../constant/lab/LabCommon";
import {isMaxLine} from "../../utils/formatString";
import {useSetRecoilState} from "recoil";
import PropTypes from "prop-types";
import {nvlNumber} from "../../utils/formatNumber";
import {dialogSelector} from "../../recoil/selector/common/dialogSelector";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../constant/common/AltString";

TransitionTextField.propTypes = {
  placeholder: PropTypes.shape({
    fold: PropTypes.string,
    expand: PropTypes.string
  }),
};

export default function TransitionTextField({type = TTF_TYPE.REGIST, text, onRegist, onChange, placeholder, sx}) {
  const [focus, setFocus] = useState(false)
  const [temp, setTemp] = useState(text)

  const handleCancel = () => {
    setTemp('')
    setFocus(false)
  }

  const handleChange = (event) => {
    let value = event.target.value?.trimStart()?.substring(0, 200);
    if (isMaxLine(value, 5)) return

    if (onChange) {
      onChange(value)
    }
    setTemp(value)
  }

  const isExpand = () => {
    return (focus || type === TTF_TYPE.MODIFY)
  }

  return (
    <TextFieldWrapper
      autoComplete='off'
      placeholder={isExpand() ? placeholder?.expand : placeholder.fold}
      fullWidth
      onFocus={() => setFocus(true)}
      onChange={handleChange}
      multiline
      value={temp}
      minRows={isExpand() ? 5 : 1}
      maxRows={5}
      inputProps={{title: ALT_STRING.LAB.COMMENT.INPUT}}
      InputProps={{
        maxLength: 200,
        endAdornment: (
          isExpand() && <TextFieldBottom text={temp} onClick={onRegist} onCancel={handleCancel}/>
        ),
        sx: {
          px: '20px',
          pt: '20px',
          pb: isExpand() ? '68px' : '20px',
          width: '100%',
          bgcolor: 'white',
          "& textarea": {
            width: '100%',
            fontSize: '16px', fontWeight: '400', lineHeight: '24px', color: '#222222',
            scrollbarWidth: 'none',
            '::-webkit-scrollbar': {display: 'none'}
          },
          '& textarea::placeholder': {
            fontSize: '16px', fontWeight: '400', lineHeight: '24px', color: '#999999'
          }
        }
      }}
      sx={{
        ...sx,
        '& .MuiOutlinedInput-input': {transition: '0.3s'},
      }}
    />
  )
}

function TextFieldBottom({text, onClick, onCancel}) {

  return (
    <Box sx={{
      left: 0,
      right: 0,
      bottom: 0,
      px: '20px',
      pb: '20px',
      position: 'absolute',
      display: 'flex',
      justifyContent: 'space-between',
    }}>

      {/* 게시글 입력 글자수 */}
      <Typography
        sx={{
          color: '#666666',
          fontSize: '14px',
          fontWeight: 400,
          lineHeight: '36px',
        }}
      >
        {LAB_TEXT.COMMENT.LENGTH(nvlNumber(text?.length))}
      </Typography>

      {/* 게시글 입력 취소, 등록 */}
      {onClick && <TextFieldBottomButton text={text} onClick={onClick} onCancel={onCancel}/>}

    </Box>
  )
}

function TextFieldBottomButton({text, onClick, onCancel}) {
  const setDialogMessage = useSetRecoilState(dialogSelector)

  const handleClick = (event, text) => {
    if (text?.length < 2) {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.ONE_BUTTON,
        message: LAB_TEXT.COMMENT.DIALOG.MIN,
        handleButton1: {
          text: LAB_TEXT.COMMENT.BUTTON.CONFIRM,
        }
      })
    } else {
      if (onClick) {
        onClick(text)
      }
      if (onCancel) {
        onCancel()
      }
    }
  }

  return (
    <Stack direction={'row'} spacing={'16px'}>

      {/* 게시글 입력 취소 */}
      <Typography
        tabIndex={0}
        onKeyDown={(e) => handleEnterPress(e, onCancel)}
        onClick={onCancel}
        sx={{
          color: '#222222',
          fontWeight: 400,
          fontSize: '14px',
          lineHeight: '36px',
          cursor: 'pointer'
        }}
      >
        {LAB_TEXT.COMMENT.BUTTON.CANCEL}
      </Typography>

      {/* 게시글 등록하기 */}
      <Button
        tabIndex={0}
        disableFocusRipple
        variant="outlined"
        onKeyDown={(e) => handleEnterPress(e, (e) => handleClick(e, text))}
        onClick={(event) => handleClick(event, text)}
        sx={{
          px: 0,
          width: '95px',
          height: '36px',
          borderRadius: '8px',
          borderColor: '#E6E6E6',
          '&:hover': {
            borderColor: '#E6E6E6',
          },
          ...clickBorderNone,
        }}
      >
        <Typography sx={{fontSize: '14px', fontWeight: '400', lineHeight: '16px', color: '#222222'}}>
          {LAB_TEXT.COMMENT.BUTTON.REGIST}
        </Typography>
      </Button>
    </Stack>
  )
}