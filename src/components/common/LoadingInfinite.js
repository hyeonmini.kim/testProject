import {styled} from "@mui/material/styles";
import {keyframes} from '@mui/system';
import {Z_INDEX} from "../../constant/common/ZIndex";
import {useEffect, useState} from "react";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";

const loadingD = keyframes`
  0% { transform: rotate(0deg); }
  50% {transform:rotate(180deg);}
  100% { transform:rotate(360deg);}
`;

const StyledRoot = styled('div')(({theme}) => ({
  right: 0,
  bottom: 0,
  zIndex: Z_INDEX.LOADING,
  width: '100%',
  height: '100%',
  position: 'fixed',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: 'rgba(0, 0, 0, 0.10)',
}));

export default function LoadingInfinite() {
  const [mounted, setMounted] = useState(false);

  useEffect(() => setMounted(true), []);

  if (!mounted) {
  }

  return (
    <StyledRoot>
      <SvgCommonIcons type={ICON_TYPE.LOGO_56PX}
                      sx={{width: '56px', height: '56px', animation: `${loadingD} 1.0s cubic-bezier(.17,.37,.43,.67) infinite;`}}/>
    </StyledRoot>
  );
}
