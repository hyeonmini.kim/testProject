import {Box, Checkbox, FormControlLabel, FormGroup, Typography} from "@mui/material";
import {AGREE_DONOTS} from "../../../constant/sign-up/AgreeTerms";
import PropTypes from "prop-types";
import {ICON_COLOR, ICON_SIZE, SvgCheckIcon} from "../../../constant/icons/icons";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {TERMS_OF_USE} from "../../../constant/my/Setting";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../../constant/common/Common";
import {ALT_STRING} from "../../../constant/common/AltString";

AgreeMarketingCheckboxForm.propTypes = {
  mktChecked: PropTypes.any.isRequired,
  smsChecked: PropTypes.any.isRequired,
  emailChecked: PropTypes.any.isRequired,
  handleChange: PropTypes.func.isRequired,
}

export default function AgreeMarketingCheckboxForm({mktChecked, smsChecked, emailChecked, handleChange, handleClick}) {
  return (
    <FormGroup>
      <Box sx={{display: 'flex', alignItems: 'flex-start', justifyContent: 'space-between', pl: '5px', py: '10px'}}>
        <FormControlLabel
          control={
            <Checkbox
              tabIndex={0}
              id={'3'}
              onChange={handleChange}
              checked={mktChecked}
              onKeyDown={(e) => handleEnterPress(e, handleChange)}
              icon={<SvgCheckIcon alt={ALT_STRING.COMMON.ICON_CHECK} color={ICON_COLOR.GREY}/>}
              checkedIcon={<SvgCheckIcon alt={ALT_STRING.COMMON.ICON_CHECK} color={ICON_COLOR.PRIMARY}/>}
              sx={{
                width: '20px',
                height: '20px',
                '&.MuiCheckbox-root svg': {
                  width: '20px',
                  height: '20px',
                }
              }}
            />
          }
          label={
            <Typography variant={'b1_16_r'} sx={{ml: '10px', color: '#000000'}}>
              {TERMS_OF_USE[3].TITLE}
            </Typography>
          }
          sx={{alignItems: 'flex-start', ml: 0}}
        />
        <SvgCommonIcons
          alt={ALT_STRING.COMMON.BTN_MOVE}
          tabIndex={0}
          id={'3'}
          type={ICON_TYPE.ARROW_RIGHT_16PX}
          onClick={handleClick}
          onKeyDown={(e) => handleEnterPress(e, handleClick)}
          sx={{mr: '5px', ...clickBorderNone}}
        />
      </Box>
      <Box sx={{ml: '25px'}}>
        <FormControlLabel
          control={
            <Checkbox
              tabIndex={0}
              id={'4'}
              onChange={handleChange}
              checked={smsChecked}
              onKeyDown={(e) => handleEnterPress(e, handleChange)}
              icon={<SvgCheckIcon alt={ALT_STRING.COMMON.ICON_CHECK} color={ICON_COLOR.GREY} size={ICON_SIZE.PX18}/>}
              checkedIcon={<SvgCheckIcon alt={ALT_STRING.COMMON.ICON_CHECK} color={ICON_COLOR.PRIMARY} size={ICON_SIZE.PX18}/>}
              sx={{
                width: '18px',
                height: '18px',
                '&.MuiCheckbox-root svg': {
                  width: '18px',
                  height: '18px',
                }
              }}
            />
          }
          label={
            <Typography variant={'b2_14_r_1l'} sx={{color: '#222222', alignSelf: 'center', ml: '4px'}}>
              {AGREE_DONOTS.SMS}
            </Typography>
          }
          sx={{alignItems: 'flex-start', ml: 0, verticalAlign: 'top'}}
        />
        <FormControlLabel
          control={
            <Checkbox
              tabIndex={0}
              id={'5'}
              onChange={handleChange}
              checked={emailChecked}
              onKeyDown={(e) => handleEnterPress(e, handleChange)}
              icon={<SvgCheckIcon alt={ALT_STRING.COMMON.ICON_CHECK} color={ICON_COLOR.GREY} size={ICON_SIZE.PX18}/>}
              checkedIcon={<SvgCheckIcon alt={ALT_STRING.COMMON.ICON_CHECK} color={ICON_COLOR.PRIMARY} size={ICON_SIZE.PX18}/>}
              sx={{
                width: '18px',
                height: '18px',
                '&.MuiCheckbox-root svg': {
                  width: '18px',
                  height: '18px',
                }
              }}
            />
          }
          label={
            <Typography variant={'b2_14_r_1l'} sx={{color: '#222222', alignSelf: 'center', ml: '4px'}}>
              {AGREE_DONOTS.EMAIL}
            </Typography>
          }
          sx={{alignItems: 'flex-start', ml: 0, verticalAlign: 'top'}}
        />
      </Box>
    </FormGroup>
  )
}