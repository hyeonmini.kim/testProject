import {Box, Checkbox, FormControlLabel, FormGroup, Typography} from "@mui/material";
import PropTypes from "prop-types";
import {ICON_COLOR, SvgCheckIcon} from "../../../constant/icons/icons";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";

AgreeCheckboxForm.propTypes = {
  id: PropTypes.string.isRequired,
  detailId: PropTypes.string.isRequired,
  labelText: PropTypes.string.isRequired,
  checked: PropTypes.any.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
};

export default function AgreeCheckboxForm({id, detailId, labelText, checked, handleChange, handleClick}) {
  return (
    <FormGroup>
      <Box sx={{display: 'flex', alignItems: 'flex-start', justifyContent: 'space-between', pl: '5px', py: '10px'}}>
        <FormControlLabel
          control={
            <Checkbox
              tabIndex={0}
              id={id}
              onChange={handleChange}
              checked={checked}
              onKeyDown={(e) => handleEnterPress(e, handleChange)}
              icon={<SvgCheckIcon alt={ALT_STRING.COMMON.ICON_CHECK} color={ICON_COLOR.GREY}/>}
              checkedIcon={<SvgCheckIcon alt={ALT_STRING.COMMON.ICON_CHECK} color={ICON_COLOR.PRIMARY}/>}
              sx={{
                width: '20px',
                height: '20px',
                '&.MuiCheckbox-root svg': {
                  width: '20px',
                  height: '20px',
                },
              }}
            />
          }
          label={
            <Typography variant={'b1_16_r_1l'} sx={{ml: '10px', color: '#000000'}}>
              {labelText}
            </Typography>
          }
          sx={{alignItems: 'flex-start', ml: 0}}
        />
        <SvgCommonIcons
          alt={ALT_STRING.COMMON.BTN_MOVE}
          tabIndex={0}
          id={detailId}
          type={ICON_TYPE.ARROW_RIGHT_16PX}
          onClick={handleClick}
          onKeyDown={(e) => handleEnterPress(e, handleClick)}
          sx={{mr: '5px', ...clickBorderNone}}
        />
      </Box>
    </FormGroup>
  )
}