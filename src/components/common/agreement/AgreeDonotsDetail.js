import {Box, Button} from "@mui/material";
import {HEADER} from "../../../config";
import PropTypes from "prop-types";
import {MainDialogLayout} from "../../../layouts/main/MainLayout";
import * as React from "react";
import HeaderDialogLayout from "../../../layouts/auth/HeaderDialogLayout";
import {Z_INDEX} from "../../../constant/common/ZIndex";
import {PERMISSION_DONOTS} from "../../../constant/sign-up/Permission";
import IFrameBodyLayout from "../../../layouts/common/IFrameBodyLayout";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";

AgreeDonotsDetail.propTypes = {
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.any.isRequired,
  linkUrl: PropTypes.string.isRequired,
}

export default function AgreeDonotsDetail({open, setOpen, linkUrl, handleClick}) {
  const handleClickBack = () => setOpen(false)

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      PaperProps={{style: {margin: 0}}}
    >
      <HeaderDialogLayout handleClick={handleClickBack}/>

      <IFrameBodyLayout noToolbar={false}>
        <iframe title={ALT_STRING.MAIN.DONOTS_AGREE} src={linkUrl}
                style={{width: '100%', height: '100%', border: 'none', paddingBottom: '92px'}}/>
      </IFrameBodyLayout>

      <Box
        sx={{
          px: '20px',
          py: '20px',
          width: '100%',
          maxWidth: '440px',
          position: 'fixed',
          bgcolor: 'white',
          zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
          bottom: 0,
          left: 0,
        }}
      >
        <Button
          tabIndex={1}
          disableFocusRipple
          variant="contained"
          fullWidth
          onClick={handleClick}
          onKeyDown={(e) => handleEnterPress(e, handleClick)}
          sx={{
            height: '52px',
            fontStyle: 'normal',
            fontWeight: 700,
            fontSize: '16px',
            lineHeight: '28px',
            color: '#FFFFFF',
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: 'primary.main',
            },
            ...clickBorderNone
          }}
        >
          {PERMISSION_DONOTS.CONFIRM}
        </Button>
      </Box>

    </MainDialogLayout>
  );
}