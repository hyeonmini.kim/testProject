import {Checkbox, FormControlLabel, FormGroup, Typography} from "@mui/material";
import {AGREE_BOTTOM} from "../../../constant/sign-up/AgreeTerms";
import PropTypes from "prop-types";
import {ICON_COLOR, SvgCheckRoundIcon} from "../../../constant/icons/icons";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../../constant/common/Common";

AgreeAllCheckboxForm.propTypes = {
  checked: PropTypes.any.isRequired,
  handleChange: PropTypes.func.isRequired,
}

export default function AgreeAllCheckboxForm({checked, handleChange}) {
  return (
    <FormGroup>
      <FormControlLabel
        control={
          <Checkbox
            onChange={handleChange}
            checked={checked}
            icon={<SvgCheckRoundIcon color={ICON_COLOR.GREY}/>}
            checkedIcon={<SvgCheckRoundIcon color={ICON_COLOR.PRIMARY}/>}
            sx={{width: '30px', height: '30px'}}
          />
        }
        label={
          <Typography variant={'b1_16_m_1l'} sx={{ml: '5px', color: '#000000'}}>
            {AGREE_BOTTOM.ALL}
          </Typography>
        }
      />
    </FormGroup>
  )
}