import {Box, Button} from "@mui/material";
import {HEADER} from "../../../config";
import PropTypes from "prop-types";
import {MainDialogLayout} from "../../../layouts/main/MainLayout";
import HeaderDialogLayout from "../../../layouts/auth/HeaderDialogLayout";
import * as React from "react";
import {Z_INDEX} from "../../../constant/common/ZIndex";
import {PERMISSION_DONOTS} from "../../../constant/sign-up/Permission";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";

AgreeCertDetail.propTypes = {
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.any.isRequired,
  title: PropTypes.string.isRequired,
  linkUrl: PropTypes.string.isRequired,
}

export default function AgreeCertDetail({open, setOpen, title, linkUrl, handleClick}) {
  const handleClickBack = () => setOpen(false)

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      PaperProps={{style: {margin: 0}}}
    >
      <HeaderDialogLayout handleClick={handleClickBack} title={title}/>

      <Box
        sx={{
          overflowY: 'hidden',
          height: '100%',
          marginTop: `${HEADER.H_MOBILE}px`,
        }}
      >
        <Box sx={{position: 'absolute', right: 0, backgroundColor: 'white', width: '60px', height: '38px'}}></Box>
        <iframe title={ALT_STRING.MAIN.AUTH_AGREE} src={linkUrl} style={{width: '100%', height: '100%', border: 'none'}}/>
      </Box>


      <Box
        sx={{
          px: '20px',
          py: '20px',
          width: '100%',
          maxWidth: '440px',
          position: 'fixed',
          bgcolor: 'white',
          zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
          bottom: 0,
          left: 0,
        }}
      >
        <Button
          tabIndex={1}
          disableFocusRipple
          variant="contained"
          fullWidth
          onClick={handleClick}
          onKeyDown={(e) => handleEnterPress(e, handleClick)}
          sx={{
            height: '52px',
            fontStyle: 'normal',
            fontWeight: 700,
            fontSize: '16px',
            lineHeight: '28px',
            color: '#FFFFFF',
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: 'primary.main',
            },
            ...clickBorderNone
          }}
        >
          {PERMISSION_DONOTS.CONFIRM}
        </Button>
      </Box>

    </MainDialogLayout>
  );
}