import {Box, Button, Divider, Typography} from "@mui/material";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import * as React from "react";
import {useEffect, useState} from "react";
import {useRouter} from "next/router";
import {FROM_TYPE} from "../../../constant/sign-up/SignUp";
import {AGREE_ALARM, AGREE_BOTTOM, AGREE_DONOTS, AGREE_PERSONAL, CHECK_LIST} from "../../../constant/sign-up/AgreeTerms";
import AgreeCheckboxForm from "./AgreeCheckboxForm";
import AgreeMarketingCheckboxForm from "./AgreeMarketingCheckboxForm";
import AgreeAllCheckboxForm from "./AgreeAllCheckboxForm";
import {useRecoilState, useSetRecoilState} from "recoil";
import {toastMessageSelector} from "../../../recoil/selector/common/toastSelector";
import {clickBorderNone, TOAST_TYPE} from "../../../constant/common/Common";
import LoadingScreen from "../LoadingScreen";
import {commonIsLoadedSelector} from "../../../recoil/selector/auth/agreeDetailSelector";
import {getTermsOfUseByTitleMutate} from "../../../api/mySettingApi";
import {TERMS_OF_USE} from "../../../constant/my/Setting";
import AgreeDonotsDetail from "./AgreeDonotsDetail";
import AgreeCertDetail from "./AgreeCertDetail";
import {
  authEmailAgreedSelector,
  authIsPhoneIdentityVerificationUsageAgreedSelector,
  authMarketingPushSelector,
  authPersonalMarketingAgreedSelector,
  authSmsAgreedSelector
} from "../../../recoil/selector/sign-up/signUpSelector";
import {MainDialogLayout} from "../../../layouts/main/MainLayout";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {isIosNative} from "../../../utils/envUtils";
import {commonHeaderBackState, commonHeaderCloseState} from "../../../recoil/atom/common/commonHeader";
import HeaderDialogLayout from "../../../layouts/recipe/create/HeaderDialogLayout";

export default function AgreeContents({isOpen, setIsOpen, signUpType, openDetail, setOpenDetail, openDetailCert, setOpenDetailCert}) {
  const [btnDisabled, setBtnDisabled] = useState(false)
  const [buttonText, setButtonText] = useState(AGREE_BOTTOM.BUTTON_ALL)

  const [checked, setChecked] = useState(CHECK_LIST.TRUE)
  const [allChecked, setAllChecked] = useState(true)

  const [detailIndex, setDetailIndex] = useState(0)
  const [detailTitle, setDetailTitle] = useState('')
  const [detailText, setDetailText] = useState('')

  const [isLoaded, setIsLoaded] = useRecoilState(commonIsLoadedSelector)

  const setMktAgreed = useSetRecoilState(authPersonalMarketingAgreedSelector)
  const setSmsAgreed = useSetRecoilState(authSmsAgreedSelector)
  const setEmailAgreed = useSetRecoilState(authEmailAgreedSelector)
  const setMktPushAgreed = useSetRecoilState(authMarketingPushSelector)

  const setIsVerificationUsage = useSetRecoilState(authIsPhoneIdentityVerificationUsageAgreedSelector)

  const [isBack, setIsBack] = useRecoilState(commonHeaderBackState);
  const [isClose, setIsClose] = useRecoilState(commonHeaderCloseState);

  const router = useRouter()
  const setToastMessage = useSetRecoilState(toastMessageSelector)

  const {mutate: termsOfUseByTitle} = getTermsOfUseByTitleMutate()

  const handleAllChange = (e) => {
    setChecked(!e.target.checked ? CHECK_LIST.FALSE : CHECK_LIST.TRUE)
    setAllChecked(!allChecked)
  }

  const handleChange = (e) => {
    const index = parseInt(e.target.id)
    let newArray = replaceItemAtIndex(checked, index, !checked[index])
    if (index === 3) {
      newArray = replaceItemAtIndex(newArray, 4, !checked[index])
      newArray = replaceItemAtIndex(newArray, 5, !checked[index])
    } else if (index === 4) {
      newArray = replaceItemAtIndex(newArray, 3, !(checked[4] && !checked[5]))
    } else if (index === 5) {
      newArray = replaceItemAtIndex(newArray, 3, !(!checked[4] && checked[5]))
    }
    setChecked(newArray)
  }

  function replaceItemAtIndex(arr, index, newValue) {
    return [...arr.slice(0, index), newValue, ...arr.slice(index + 1)]
  }

  const handleDetailClick = (e) => {
    const index = e.target.id !== '' ? parseInt(e.target.id) : parseInt(e.target.parentNode.id)
    setDetailIndex(index)

    termsOfUseByTitle({
      title: encodeURIComponent(TERMS_OF_USE[index].VALUE)
    }, {
      onSuccess: (result) => {
        setDetailTitle(result?.title)
        setDetailText(result?.bodyHtmlFileUrl)

        setOpenDetail(true)
      }
    })
  }

  const handleDetailCertClick = (e) => {
    const index = e.target.id !== '' ? parseInt(e.target.id) : parseInt(e.target.parentNode.id)
    setDetailIndex(index)

    setDetailTitle(TERMS_OF_USE[index].TITLE)
    setDetailText(TERMS_OF_USE[index].LINK_URL)

    setOpenDetailCert(true)
  }

  const handleClickConfirm = () => {
    let index = detailIndex
    if (index > 3) {
      index = index + 2
    }

    let newArray = replaceItemAtIndex(checked, index, true)
    if (index === 3) {
      newArray = replaceItemAtIndex(newArray, 4, true)
      newArray = replaceItemAtIndex(newArray, 5, true)
    }

    setChecked(newArray)
    setOpenDetail(false)
    setOpenDetailCert(false)
  }

  useEffect(() => {
    // 전체 약관 체크 컨트롤
    const indexList = []
    for (let i = 0; i < checked.length; i++) {
      if (checked[i] === false) {
        if (isIosNative()) {
          if (i !== 3 && i !== 4 && i !== 5 && i !== 6 && i !== 7 && i !== 8 && i !== 9 && i !== 10 && i !== 11) {
            indexList.push(i)
          }
        } else {
          if (i !== 3 && i !== 4 && i !== 5 && i !== 11) {
            indexList.push(i)
          }
        }
      }
    }
    setAllChecked(indexList.length <= 0)

    // 동의 버튼 컨트롤
    setBtnDisabled(!allChecked)
    setButtonText(checked.includes(false) ? AGREE_BOTTOM.BUTTON_COMMON : AGREE_BOTTOM.BUTTON_ALL)

    // 선택 동의 여부 컨트롤
    setMktAgreed(checked[3])
    setSmsAgreed(checked[4])
    setEmailAgreed(checked[5])
    setMktPushAgreed(checked[11])
  }, [checked, allChecked])

  const onClickConfirm = () => {
    setIsLoaded(true)

    if (signUpType === 'id') {
      if (isIosNative()) {
        setIsVerificationUsage(false)
        router.push({pathname: PATH_DONOTS_PAGE.AUTH.SIGNUP, query: {from: FROM_TYPE.SIGN_UP_ID_IOS}})
      } else {
        setIsVerificationUsage(true)
        router.push({pathname: PATH_DONOTS_PAGE.AUTH.SIGNUP, query: {from: FROM_TYPE.SIGN_UP_ID}})
      }
    } else if (signUpType === 'sns') {
      router.push({pathname: PATH_DONOTS_PAGE.AUTH.SIGNUP, query: {from: FROM_TYPE.SIGN_UP_SNS}})
    }

    // 마케팅 수신 동의 결과 토스트
    setToastMessage({
      type: TOAST_TYPE.BOTTOM_HEADER,
      message: (!checked[3] && !checked[11]) ? AGREE_ALARM.DISAGREE_MARKETING : AGREE_ALARM.AGREE_MARKETING,
    })
  }

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_TERMS)
  }, [])

  useEffect(() => {
    setIsBack(false)
    setIsClose(true)

    return () => {
      setIsBack(true)
      setIsClose(false)
    }
  }, [])

  return (
    <MainDialogLayout
      fullScreen
      open={isOpen}
      PaperProps={{style: {margin: 0}}}
    >
      <HeaderDialogLayout
        showClose={true}
        onClose={() => setIsOpen(false)}
      >
        <Box sx={{p: '0 0 180px'}}>

          {/* 도낫츠 이용약관 */}
          <Box sx={{p: '20px 30px 10px 30px'}}>
            <Typography variant={'b1_16_m_1l'} sx={{color: '#000000'}}>
              {AGREE_DONOTS.TITLE}
            </Typography>
            <Divider sx={{height: '1px', color: '#E6E6E6', m: '20px 0 10px'}}/>
            <AgreeCheckboxForm id={'0'} detailId={'0'} labelText={TERMS_OF_USE[0].TITLE} checked={checked[0]}
                               handleChange={handleChange} handleClick={handleDetailClick}/>
            <AgreeCheckboxForm id={'1'} detailId={'1'} labelText={TERMS_OF_USE[1].TITLE} checked={checked[1]}
                               handleChange={handleChange} handleClick={handleDetailClick}/>
            <AgreeCheckboxForm id={'2'} detailId={'2'} labelText={TERMS_OF_USE[2].TITLE} checked={checked[2]}
                               handleChange={handleChange} handleClick={handleDetailClick}/>
            <AgreeMarketingCheckboxForm mktChecked={checked[3]} smsChecked={checked[4]} emailChecked={checked[5]}
                                        handleChange={handleChange} handleClick={handleDetailClick}/>
          </Box>

          {/* 본인인증 이용약관 */}
          {!isIosNative() &&
            <Box sx={{p: '20px 30px 10px 30px'}}>
              <Typography variant={'b1_16_m_1l'} sx={{color: '#000000'}}>
                {AGREE_PERSONAL.TITLE}
              </Typography>
              <Divider sx={{height: '1px', color: '#E6E6E6', m: '20px 0 10px'}}/>
              <AgreeCheckboxForm id={'6'} detailId={'4'} labelText={AGREE_PERSONAL.PERSONAL} checked={checked[6]}
                                 handleChange={handleChange} handleClick={handleDetailCertClick}/>
              <AgreeCheckboxForm id={'7'} detailId={'5'} labelText={AGREE_PERSONAL.UNIQUE} checked={checked[7]}
                                 handleChange={handleChange} handleClick={handleDetailCertClick}/>
              <AgreeCheckboxForm id={'8'} detailId={'6'} labelText={AGREE_PERSONAL.TELECOM} checked={checked[8]}
                                 handleChange={handleChange} handleClick={handleDetailCertClick}/>
              <AgreeCheckboxForm id={'9'} detailId={'7'} labelText={AGREE_PERSONAL.SERVICE} checked={checked[9]}
                                 handleChange={handleChange} handleClick={handleDetailCertClick}/>
              <AgreeCheckboxForm id={'10'} detailId={'8'} labelText={AGREE_PERSONAL.RESALE_PHONE} checked={checked[10]}
                                 handleChange={handleChange} handleClick={handleDetailCertClick}/>
            </Box>
          }

          {/* 앱(App) 알림 수신 동의 */}
          <Box sx={{p: '20px 30px 10px 30px'}}>
            <Typography variant={'b1_16_m_1l'} sx={{color: '#000000'}}>
              {AGREE_ALARM.TITLE}
            </Typography>
            <Divider sx={{height: '1px', color: '#E6E6E6', m: '20px 0 10px'}}/>
            <AgreeCheckboxForm id={'11'} detailId={'9'} labelText={TERMS_OF_USE[9].TITLE} checked={checked[11]}
                               handleChange={handleChange} handleClick={handleDetailClick}/>
          </Box>

          {/* 전체 약관 동의 및 버튼 */}
          <Box
            sx={{
              position: 'fixed',
              left: 0,
              right: 0,
              bottom: 0,
              backgroundColor: '#FFFFFF',
              borderTop: '1px solid #E6E6E6',
            }}
          >
            <Box sx={{m: '15px 20px 20px 20px'}}>
              <Box sx={{display: 'flex', mb: '15px', px: '14px'}}>
                <AgreeAllCheckboxForm checked={allChecked} handleChange={handleAllChange}/>
              </Box>
              <Button
                tabIndex={0}
                disableFocusRipple
                variant="contained"
                fullWidth
                disabled={btnDisabled}
                sx={{
                  height: '52px',
                  fontStyle: 'normal',
                  fontWeight: 700,
                  fontSize: '16px',
                  lineHeight: '24px',
                  color: '#FFFFFF',
                  '&.Mui-disabled': {
                    color: '#B3B3B3',
                    backgroundColor: '#CCCCCC',
                  },
                  '&:hover': {
                    boxShadow: 'none',
                    backgroundColor: 'primary.main',
                  },
                  ...clickBorderNone
                }}
                onClick={onClickConfirm}
              >
                {buttonText}
              </Button>
            </Box>
          </Box>

        </Box>

      </HeaderDialogLayout>

      {/* 이용 약관 상세 (도나츠) 전체화면 다이얼로그 */}
      {openDetail && <AgreeDonotsDetail open={openDetail} setOpen={setOpenDetail} linkUrl={detailText} handleClick={handleClickConfirm}/>}
      {/* 이용 약관 상세 (본인인증) 전체화면 다이얼로그 */}
      {openDetailCert && <AgreeCertDetail open={openDetailCert} setOpen={setOpenDetailCert} title={detailTitle} linkUrl={detailText}
                                          handleClick={handleClickConfirm}/>}

      {isLoaded && <LoadingScreen dialog/>}

    </MainDialogLayout>
  )
}
