import {Button, Typography} from "@mui/material";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../constant/common/Common";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {getNumberTenThousand} from "../../utils/formatNumber";

export default function LikeIconButton({like, isCheck = false, onClick, sx}) {
  return (
    <Button
      disableFocusRipple
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
      sx={{
        px: 0,
        minWidth: '44px',
        height: '24px',
        borderRadius: '99px',
        alignItems: 'center',
        border: isCheck ? '1px solid #ECA548' : '1px solid #E6E6E6',
        '&:hover': {
          bgcolor: '#FFFFFF',
          boxShadow: 'none',
        },
        ...sx,
        ...clickBorderNone,
      }}
    >
      <SvgCommonIcons
        type={isCheck ? ICON_TYPE.LIKE_MAIN : ICON_TYPE.LIKE_BLACK}
        sx={{
          ml: '10px'
        }}
      />

      <Typography
        sx={{
          ml: '4px',
          mr: '10px',
          fontWeight: '700',
          fontSize: '12px',
          lineHeight: '24px',
          color: isCheck ? 'primary.main' : '#222222'
        }}
      >
        {getNumberTenThousand(like)}
      </Typography>
    </Button>
  )
}