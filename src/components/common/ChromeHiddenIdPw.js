import * as React from "react";
import TextFieldWrapper from "./TextFieldWrapper";
import {ALT_STRING} from "../../constant/common/AltString";

export default function ChromeHiddenIdPw() {
  return (
    <>
      <TextFieldWrapper inputProps={{tabIndex: -1}} autoComplete='off' type="text" label={ALT_STRING.SIGN_UP.TMP_ID}
                        sx={{opacity: "0", position: "absolute", top: "-100px"}}/>
      <TextFieldWrapper inputProps={{tabIndex: -1}} autoComplete='off' type="password" label={ALT_STRING.SIGN_UP.TMP_PASSWORD}
                        sx={{opacity: "0", position: "absolute", top: "-100px"}}/>
    </>
  )
}