import * as React from "react";
import {forwardRef, useRef, useState} from "react";
import {Fade, Slide} from "@mui/material";
import IFrameLayout from "../../layouts/common/IFrameLayout";
import {Z_INDEX} from "../../constant/common/ZIndex";
import {MainDialogLayout} from "../../layouts/main/MainLayout";
import LoadingScreen from "./LoadingScreen";

const SlideUp = forwardRef((props, ref) => <Slide direction="up" ref={ref} {...props} />);

export const TRANSITION_TYPE = {
  SLIDE: SlideUp,
  FADE: Fade,
}

export default function IFrameDialog({
                                       backTabIndex = 0,
                                       open,
                                       transition = TRANSITION_TYPE.SLIDE,
                                       noToolbar,
                                       title,
                                       url = '/404',
                                       isLeft,
                                       onBack,
                                       onClose
                                     }) {
  const iframeRef = useRef(null)
  const [loading, setLoading] = useState(true)

  return (
    <MainDialogLayout
      fullScreen open={open}
      onClose={onClose}
      TransitionComponent={transition}
      sx={{
        zIndex: Z_INDEX.DIALOG,
        '& .MuiDialog-paper': {
          mx: 0,
        }
      }}
    >
      <IFrameLayout backTabIndex={backTabIndex} noToolbar={noToolbar} title={title} isLeft={isLeft} onBack={onBack} onClose={onClose}>
        <iframe
          onLoad={() => setLoading(false)}
          ref={iframeRef}
          src={url}
          style={{marginRight: 0, width: '100%', height: '100%', border: 'none', display: 'block'}}
        />
      </IFrameLayout>
      {loading && <LoadingScreen dialog bgWhite/>}
    </MainDialogLayout>
  );
}