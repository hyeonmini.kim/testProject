import {Dialog, DialogContent, DialogTitle, Divider, Slide, Typography,} from "@mui/material";
import * as React from "react";
import {useRecoilState} from "recoil";
import {commonBottomDialogState} from "../../recoil/atom/common/bottomDialog";
import {Z_INDEX} from "../../constant/common/ZIndex";
import {sxFixedCenterMainLayout} from "../../layouts/main/MainLayout";
import {useEffect} from "react";

export default function BottomDialogQA({title, children}) {
  const [isOpen, setIsOpen] = useRecoilState(commonBottomDialogState)

  const transition = React.forwardRef(
    function Transition(props, ref) {
      return <Slide direction="up" ref={ref} {...props} />
    }
  );

  const handleClick = (e) => {
    e.preventDefault();
    setIsOpen(false)
  }

  useEffect(() => {
    return () => setIsOpen(false)
  }, [])

  return (
    <Dialog
      open={isOpen}
      onClick={handleClick}
      TransitionComponent={transition}
      fullWidth
      PaperProps={{
        style: {
          margin: 0,
          borderRadius: '16px 16px 0 0',
          position: 'fixed',
          bottom: 0,
          backgroundColor: '#FFFFFF',
          height: '300px',
        },
      }}
      sx={{
        visibility: isOpen ? 'visible' : 'hidden',
        zIndex: Z_INDEX.SLIDE_BOTTOM_DIALOG,
        ...sxFixedCenterMainLayout
      }}
    >
      <DialogTitle sx={{padding: '20px 20px 0 20px'}}>
        <Typography
          tabIndex={0}
          sx={{
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            letterSpacing: '-0.02em',
            color: '#000000',
          }}
        >
          {title}
        </Typography>
        <Divider sx={{height: '1px', backgroundColor: '#E6E6E6', mt: '20px', mb: 0}}/>
      </DialogTitle>
      <DialogContent dividers sx={{margin: '0 20px 0 20px', padding: 0, textAlign: '-webkit-center'}}>
        {children}
      </DialogContent>
    </Dialog>
  )
}