import {nvlString} from "../../../utils/formatString";
import {Box, Skeleton, Stack, Typography} from "@mui/material";
import Image from "../../image";
import TextMaxLine from "../../text-max-line";
import {getNumberTenThousand, nvlNumber} from "../../../utils/formatNumber";
import * as React from "react";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {COUNT_TITLE} from "../../../constant/recipe/detail/DetailConstants";
import {ALT_STRING} from "../../../constant/common/AltString";

export default function RecipeItemExperts({item, isLazy, onClick}) {
  const {
    recipe_key,
    recipe_name,
    recipe_user_name,
    image_file_path,
    recipe_desc,
    recipe_view_cnt,
    recipe_scrap_cnt
  } = item;

  return (
    <Stack
      tabIndex={4}
      direction="row"
      spacing="8px"
      sx={{my: '12px', ...clickBorderNone}}
      onClick={() => onClick(recipe_user_name, recipe_name, recipe_key)}
      onKeyDown={(e) => handleEnterPress(e, () => onClick(recipe_user_name, recipe_name, recipe_key))}
    >
      <Box sx={{position: 'relative'}}>
        {
          image_file_path
            ? <Image alt={ALT_STRING.COMMON.RECIPE(recipe_name)} src={image_file_path} sx={{borderRadius: '8px', width: '100px', height: '100px'}} isLazy={isLazy}/>
            : <Skeleton variant="rounded" sx={{borderRadius: '8px', width: '100px', height: '100px'}}/>
        }
      </Box>
      <Box sx={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between', flexGrow: 1}}>
        <TextMaxLine open={true} line={1} sx={{fontSize: '16px', fontWeight: 500, lineHeight: '24px'}}>
          {nvlString(recipe_name)}
        </TextMaxLine>
        <TextMaxLine open={true} line={2} sx={{fontSize: '14px', fontWeight: 400, lineHeight: '20px'}}>
          {nvlString(recipe_desc)}
        </TextMaxLine>
        <Box sx={{flexGrow: 1}}/>
        <Stack direction="row" alignItems={'center'}>
          <Typography fontWeight="400" fontSize="12px"
                      sx={{lineHeight: '12px', mr: '16px', color: '#888888'}}>
            {`${COUNT_TITLE.VIEWS} ${getNumberTenThousand(nvlNumber(recipe_view_cnt))}`}
          </Typography>
          <Typography fontWeight="400" fontSize="12px"
                      sx={{lineHeight: '12px', mr: '16px', color: '#888888'}}>
            {`${COUNT_TITLE.SCRAP} ${getNumberTenThousand(nvlNumber(recipe_scrap_cnt))}`}
          </Typography>
        </Stack>
      </Box>
    </Stack>
  );
}