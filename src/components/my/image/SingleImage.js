import PropTypes from 'prop-types';
import {useDropzone} from 'react-dropzone';
import {Box, IconButton, Stack} from '@mui/material';
import {alpha, styled} from '@mui/material/styles';
import Iconify from "../../iconify";
import {useRecoilState} from "recoil";
import {isNative} from "../../../utils/envUtils";
import SingleImagePreview from "./SingleImagePreview";
import {settingOneOnOneInquiryAttachmentFileSelector} from "../../../recoil/selector/my/setting/one-on-one-inquiry/oneOnOneInquirySelector";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import * as React from "react";
import {settingShowOneOnOneInquiriesPopupSelector} from "../../../recoil/selector/my/settingSelector";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";
// ----------------------------------------------------------------------

const StyledDropZone = styled('div')(({theme}) => ({
  outline: 'none',
  cursor: 'pointer',
  overflow: 'hidden',
  alignItems: 'center',
  justifyContent: 'center',
  width: '80px',
  transition: theme.transitions.create('padding'),
  backgroundColor: 'white',
  '&:active': {
    backgroundColor: '#E6E6E6'
  },
  border: '1px solid #E6E6E6',
  borderRadius: '8px',
  ...clickBorderNone
}));

// ----------------------------------------------------------------------

SingleImage.propTypes = {
  onDrop: PropTypes.func,
  onClick: PropTypes.func,
  onDelete: PropTypes.func,
};

export default function SingleImage({onClick, onDelete, ...other}) {
  const [attachmentFile, setAttachmentFile] = useRecoilState(settingOneOnOneInquiryAttachmentFileSelector)
  const [showOneOnOneInquiriesPopup, setShowOneOnOneInquiriesPopup] = useRecoilState(settingShowOneOnOneInquiriesPopupSelector)
  const {getRootProps, getInputProps} = useDropzone({
    multiple: false,
    accept: {
      'image/*': ['.jpeg', '.gif', '.png'],
    },
    ...other,
  });

  const openPopup = () => {
    setShowOneOnOneInquiriesPopup(true)
  }

  const image = attachmentFile

  if (isNative()) {
    return (
      <Box sx={{position: 'relative'}}>
        {attachmentFile ? (<SingleImagePreview file={image}/>
        ) : (
          <StyledDropZone tabIndex={1} onClick={onClick}>
            <ImageDropZoneChildren image={attachmentFile}/>
          </StyledDropZone>)}
        {attachmentFile && <DeleteButtonChildren onDelete={onDelete}/>}
      </Box>
    )
  } else {
    return (
      <Box sx={{position: 'relative'}}>
        {attachmentFile ? (<SingleImagePreview file={image}/>
        ) : (
          <StyledDropZone {...getRootProps()} tabIndex={1}>
            <input title={ALT_STRING.COMMON.ADD_IMAGE} {...getInputProps()}  />
            <ImageDropZoneChildren image={attachmentFile}/>
          </StyledDropZone>)}
        {attachmentFile && <DeleteButtonChildren onDelete={onDelete}/>}
      </Box>
    )
  }
}

function ImageDropZoneChildren({image}) {
  return (
    <>
      {image
        ? <SingleImagePreview file={image}/>
        : <DropZoneChildren/>
      }
    </>
  )
}

function DeleteButtonChildren({onDelete}) {
  return (
    <IconButton
      size="small"
      onClick={onDelete}
      sx={{
        top: 2,
        right: 2,
        p: '0px',
        position: 'absolute',
        color: (theme) => alpha(theme.palette.common.white, 0.8),
        bgcolor: (theme) => alpha(theme.palette.grey[900], 0.72),
        '&:hover': {
          bgcolor: (theme) => alpha(theme.palette.grey[900], 0.72),
        },
      }}
    >
      <Iconify alt={ALT_STRING.SETTING.ICON_DEL_PICTURE} icon="eva:close-fill" width={18}/>
    </IconButton>
  )
}

function DropZoneChildren() {
  return (
    <Stack alignItems="center" justifyContent="center" sx={{height: '80px'}}>
      <SvgCommonIcons alt={ALT_STRING.SETTING.ADD_PICTURE} type={ICON_TYPE.PLUS_GRAY}/>
    </Stack>
  )
}