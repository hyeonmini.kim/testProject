import PropTypes from 'prop-types';
import {Card, Stack, Typography} from '@mui/material';
import {useTheme} from "@mui/material/styles";
import {fileData} from "../../file-thumbnail";
import Image from "../../image";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {ALT_STRING} from "../../../constant/common/AltString";

// ----------------------------------------------------------------------

ChildThumbnail.propTypes = {
  sx: PropTypes.object,
  imgSx: PropTypes.object,
  isRepresent: PropTypes.bool,
  imageView: PropTypes.bool,
  onDownload: PropTypes.func,
  file: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

export default function ChildThumbnail({child, index, isRepresent = false, imgSx}) {
  const {preview = ''} = fileData(child?.profilePictureUrl);
  const theme = useTheme();
  const renderContent =
    <Image
      src={preview}
      sx={{
        width: 1,
        height: 1,
        flexShrink: 0,
        // border: '1px solid #e6e6e6',
        objectFit: 'cover',
        ...imgSx,
      }}
      isLazy={false}
    />
  return (
    <Card sx={{borderRadius: '8px', border: '1px solid #e6e6e6'}}>
      {child?.profilePictureUrl ? renderContent : getDefaultChildrenAvatar(index, child?.gender)}
      {isRepresent && <Stack
        spacing={2}
        direction="row"
        alignItems="center"
        justifyContent="center"
        sx={{
          // ...bgBlur({
          //   color: '#000000',
          // }),
          background: 'rgba(0, 0, 0, 0.7)',
          width: 1,
          height: '18px',
          left: 0,
          bottom: 0,
          position: 'absolute',
        }}
      >
        <Typography sx={{color: '#FFFFFF', fontSize: '12px', fontWeight: '400', lineHeight: '18px'}}>
          대표
        </Typography>
      </Stack>
      }
    </Card>
  );
}

function getDefaultChildrenAvatar(index, childGender) {
  let avatarImage = '';

  if (index === 0) {
    if (childGender === 'MALE') {
      return <SvgCommonIcons alt={ALT_STRING.MY.PROFILE_CHILDREN} type={ICON_TYPE.PROFILE_BOY_FIRST}/>
      // avatarImage = '/assets/icons/my/boy_first.svg';
    } else {
      return <SvgCommonIcons alt={ALT_STRING.MY.PROFILE_CHILDREN} type={ICON_TYPE.PROFILE_GIRL_FIRST}/>
      // avatarImage = '/assets/icons/my/girl_first.svg';
    }
  } else if (index === 1) {
    if (childGender === 'MALE') {
      return <SvgCommonIcons alt={ALT_STRING.MY.PROFILE_CHILDREN} type={ICON_TYPE.PROFILE_BOY_SECOND}/>
      // avatarImage = '/assets/icons/my/boy_second.svg';
    } else {
      return <SvgCommonIcons alt={ALT_STRING.MY.PROFILE_CHILDREN} type={ICON_TYPE.PROFILE_GIRL_SECOND}/>
      // avatarImage = '/assets/icons/my/girl_second.svg';
    }
  } else if (index === 2) {
    if (childGender === 'MALE') {
      return <SvgCommonIcons alt={ALT_STRING.MY.PROFILE_CHILDREN} type={ICON_TYPE.PROFILE_BOY_THIRD}/>
      // avatarImage = '/assets/icons/my/boy_third.svg';
    } else {
      return <SvgCommonIcons alt={ALT_STRING.MY.PROFILE_CHILDREN} type={ICON_TYPE.PROFILE_GIRL_THIRD}/>
      // avatarImage = '/assets/icons/my/girl_third.svg';
    }
  }

  return avatarImage;
}