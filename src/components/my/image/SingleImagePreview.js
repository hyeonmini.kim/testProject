import PropTypes from 'prop-types';
import Image from "../../image";
import {useRecoilState} from "recoil";
import {settingShowOneOnOneInquiriesPopupSelector} from "../../../recoil/selector/my/settingSelector";
import * as React from "react";
import {forwardRef} from "react";
import {AppBar, Box, Container, Fade, Slide, Toolbar} from "@mui/material";
import {Z_INDEX} from "../../../constant/common/ZIndex";
import {ICON_COLOR, SvgCloseIcon} from "../../../constant/icons/icons";
import {MainDialogLayout} from "../../../layouts/main/MainLayout";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../../constant/common/Common";
import {ALT_STRING} from "../../../constant/common/AltString";
// ----------------------------------------------------------------------

SingleImagePreview.propTypes = {
  file: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

export default function SingleImagePreview({alt = '', file}) {
  const [showOneOnOneInquiriesPopup, setShowOneOnOneInquiriesPopup] = useRecoilState(settingShowOneOnOneInquiriesPopupSelector)

  const onClick = (e) => {
    e.stopPropagation()
    setShowOneOnOneInquiriesPopup(true)
  }

  const onBack = (e) => {
    e.stopPropagation()
    setShowOneOnOneInquiriesPopup(false)
  }

  if (!file) {
    return null;
  }
  const imgUrl = typeof file === 'string' ? file : file.preview;
  return (
    <Box tabIndex={1} sx={{...clickBorderNone}} onKeyDown={(e) => handleEnterPress(e, onClick)}>
      <Image
        alt={alt}
        src={imgUrl}
        onClick={onClick}
        ratio={'1/1'}
        sx={{
          height: '80px',
          borderRadius: '8px'
        }}
      />

      <OneOnOneInquiriesPopup imgUrl={imgUrl}
                              open={showOneOnOneInquiriesPopup}
                              onBack={onBack}/>
    </Box>
  );
}

OneOnOneInquiriesPopup.propTypes = {
  onBack: PropTypes.func,
};

const Transition = forwardRef((props, ref) => <Slide direction="up" ref={ref} {...props} />);

export function OneOnOneInquiriesPopup({imgUrl, open, onBack}) {
  const [showOneOnOneInquiriesPopup, setShowOneOnOneInquiriesPopup] = useRecoilState(settingShowOneOnOneInquiriesPopupSelector)

  const imageUrl = typeof imgUrl === 'string' ? imgUrl : imgUrl?.preview

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0, height: '100%', backgroundColor: 'black'}}}
    >
      <Container maxWidth={'xs'} disableGutters
                 sx={{px: '0px', height: '100%', bgcolor: 'black', position: 'relative', zIndex: 99, m: 'auto'}}>
        <Box
          sx={{
            height: '100vh',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Image alt={''} src={imageUrl} ratio="1/1" isLazy={false}/>
        </Box>
      </Container>
      <AppBar position="absolute" elevation={0} sx={{height: '58px', backgroundColor: 'black'}}>
        <Container maxWidth={'xs'} disableGutters>
          <Toolbar sx={{justifyContent: 'end'}}>
            <SvgCloseIcon
              alt={ALT_STRING.COMMON.BTN_CLOSE}
              tabIndex={1}
              color={ICON_COLOR.WHITE}
              onClick={onBack}
              onKeyDown={(e) => handleEnterPress(e, onBack)}
            />
          </Toolbar>
        </Container>
      </AppBar>
    </MainDialogLayout>
  )
}