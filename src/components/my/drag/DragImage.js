import PropTypes from 'prop-types';
import {Avatar, IconButton, Stack, Typography} from '@mui/material';
import {Draggable} from "react-beautiful-dnd";
import {fileData} from "../../file-thumbnail";
import ChildThumbnail from "../image/ChildThumbnail";
import {ICON_COLOR, SvgDeleteIcon} from "../../../constant/icons/icons";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";
// ----------------------------------------------------------------------

DragImage.propTypes = {
  sx: PropTypes.object,
  file: PropTypes.object,
  onRemove: PropTypes.func,
  index: PropTypes.number
};

export default function DragImage({child, isRemove, profileSelectedBabyKey, onRemove, sx, index, handleClick}) {
  if (!child) {
    return null;
  }
  const {key} = fileData(child?.profilePictureUrl);
  return (
    <Draggable key={key + index} draggableId={key + index} index={index}>
      {(provided) => (
        <Stack
          tabIndex={3}
          id={index}
          {...provided.draggableProps}
          ref={provided.innerRef}
          onClick={handleClick}
          onKeyDown={(e) => handleEnterPress(e, handleClick)}
          sx={{position: 'relative', ...clickBorderNone}}
        >
          <Avatar
            alt={ALT_STRING.MY.PROFILE_CHILDREN}
            sx={{
              width: '56px',
              height: '56px',
              mx: '6px',
              ...sx,
            }}
          >
            <Stack {...provided.dragHandleProps} tabIndex={-1}>
              <ChildThumbnail
                isRepresent={profileSelectedBabyKey === child?.key ? true : false}
                imageView
                child={child}
                imgSx={{width: '56px', height: '56px', mx: '6px',}}
                index={index}
              />
            </Stack>
          </Avatar>

          {isRemove && (
            <IconButton
              tabIndex={-1}
              disableFocusRipple
              size="small"
              onMouseDown={() => onRemove(index)}
              sx={{
                top: 2,
                right: 0,
                p: '1px',
                position: 'absolute',
              }}
            >
              <SvgDeleteIcon alt={ALT_STRING.MY.ICON_DEL_CHILDREN} color={ICON_COLOR.BLACK}/>
            </IconButton>
          )}
          <Typography fontWeight="400" fontSize="14px"
                      sx={{mt: '8px', alignSelf: 'center', lineHeight: '20px', color: '#222222'}}>
            {child?.nickname?.length > 3 ? child?.nickname.substring(0, 3) + "..." : child?.nickname}
          </Typography>
        </Stack>
      )}
    </Draggable>
  )
}
