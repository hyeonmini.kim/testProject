import {nvlString} from "../../utils/formatString";
import {Box, Skeleton, Stack, Typography} from "@mui/material";
import Image from "../image";
import TextMaxLine from "../text-max-line";
import * as React from "react";
import {useEffect, useState} from "react";
import {getNumberTenThousand, nvlNumber} from "../../utils/formatNumber";
import {MY_COMMON} from "../../constant/my/MyConstants";
import {clickBorderNone} from "../../constant/common/Common";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {COUNT_TITLE} from "../../constant/recipe/detail/DetailConstants";
import {ALT_STRING} from "../../constant/common/AltString";

export default function RecipeItem({item, isLazy, onClick, activeTab}) {
  const [statusColor, setStatusColor] = useState('');
  const [statusBackGroundColor, setStatusBackGroundColor] = useState('');

  const {
    recipe_key,
    recipe_name,
    recipe_user_name,
    image_file_path,
    recipe_desc,
    recipe_view_cnt,
    recipe_scrap_cnt,
    recipe_check_status,
  } = item;

  useEffect(() => {
    switch (recipe_check_status) {
      case '임시저장':
        setStatusColor('#DB7C00');
        setStatusBackGroundColor('#FFF8E8');
        break
      case '검수중':
        setStatusColor('#3D6A4A');
        setStatusBackGroundColor('#EFFAEC');
        break
      case '반려':
        setStatusColor('#FF3B34');
        setStatusBackGroundColor('#FFEAEA');
        break
      case '게시중':
        setStatusColor('#666666');
        setStatusBackGroundColor('#F5F5F5');
        break
      default:
        break
    }
  }, [item])

  return (
    <Stack
      tabIndex={10}
      direction="row"
      spacing="12px"
      sx={{my: '12px', ...clickBorderNone}}
      onClick={() => onClick(recipe_check_status, recipe_user_name, recipe_name, recipe_key)}
      onKeyDown={(e) => handleEnterPress(e, () => onClick(recipe_check_status, recipe_user_name, recipe_name, recipe_key))}
    >
      <Box sx={{position: 'relative'}}>
        {
          image_file_path
            ? <Image alt={ALT_STRING.COMMON.RECIPE(recipe_name)} src={image_file_path} sx={{borderRadius: '8px', width: '124px', height: '124px'}} isLazy={isLazy}/>
            : <Skeleton variant="rounded" sx={{borderRadius: '8px', width: '124px', height: '124px'}}/>
        }
      </Box>
      <Box sx={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between', flexGrow: 1}}>
        {recipe_check_status && activeTab === MY_COMMON.MY_TAB_WRITE &&
          <Box
            sx={{
              height: '26px',
              width: 'fit-content',
              padding: '4px 10px',
              alignItems: 'center',
              backgroundColor: statusBackGroundColor,
              borderRadius: '8px',
              mb: '6px'
            }}>
            <Typography fontWeight="400" fontSize="12px" sx={{lineHeight: '18px', color: statusColor}}>
              {nvlString(recipe_check_status)}
            </Typography>
          </Box>
        }
        <TextMaxLine open={true} line={1} sx={{fontSize: '16px', fontWeight: 500, lineHeight: '24px', mb: '2px'}}>
          {nvlString(recipe_name)}
        </TextMaxLine>
        <TextMaxLine open={true} line={2} sx={{fontSize: '14px', fontWeight: 400, lineHeight: '20px'}}>
          {nvlString(recipe_desc)}
        </TextMaxLine>
        <Box sx={{flexGrow: 1}}/>
        <Stack direction="row" alignItems={'center'}>
          <Typography fontWeight="400" fontSize="12px" sx={{lineHeight: '12px', mr: '16px', color: '#888888'}}>
            {`${COUNT_TITLE.VIEWS} ${getNumberTenThousand(nvlNumber(recipe_view_cnt))}`}
          </Typography>
          <Typography fontWeight="400" fontSize="12px" sx={{lineHeight: '12px', mr: '16px', color: '#888888'}}>
            {`${COUNT_TITLE.SCRAP} ${getNumberTenThousand(nvlNumber(recipe_scrap_cnt))}`}
          </Typography>
        </Stack>
      </Box>
    </Stack>
  );
}