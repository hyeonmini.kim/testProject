import PropTypes from 'prop-types';
//
import {Stack, Typography} from "@mui/material";
import * as React from "react";
import {useRecoilState} from "recoil";
import {forMyPageEntrySelector} from "../../../../recoil/selector/auth/userSelector";
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";

// ----------------------------------------------------------------------

AvatarChildrenPreview.propTypes = {
  file: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

export default function AvatarChildrenPreview({file}) {
  const [user, setUser] = useRecoilState(forMyPageEntrySelector);
  if (!file) {
    return null;
  }
  const imgUrl = typeof file === 'string' ? file : file.preview;
  return (
    <Stack
      direction="row"
      sx={{
        alignItems: 'center',
      }}
    >
      <SvgCommonIcons type={ICON_TYPE.CHOICE_PHOTO}/>
      <Typography
        value="촬영 또는 라이브러리에서 선택"
        sx={{
          fontStyle: 'normal',
          fontWeight: 500,
          fontSize: '16px',
          lineHeight: '24px',
          color: '#222222',
          paddingX: '10px',
          paddingY: '20px',
        }}
      >
        촬영 또는 라이브러리에서 선택
      </Typography>
    </Stack>
  );
}
