import PropTypes from 'prop-types';
import {useDropzone} from 'react-dropzone';
// @mui
import {Box, Stack, Typography} from '@mui/material';
import {styled} from '@mui/material/styles';
//
//
import {isNative} from "../../../utils/envUtils";
import {useRecoilState} from "recoil";
import {forMyPageEntrySelector} from "../../../recoil/selector/auth/userSelector";
import * as React from "react";
import AvatarChildrenPreview from "./preview/AvatarChildrenPreview";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";

// ----------------------------------------------------------------------

const StyledDropZone = styled('div')(({theme}) => ({
  // width: '88px',
  // height: '88px',
  // margin: 'auto',
  // display: 'flex',
  // cursor: 'pointer',
  // overflow: 'hidden',
  // // borderRadius: '50%',
  // alignItems: 'center',
  // position: 'relative',
  // justifyContent: 'center',
  // textDecoration: 'none',
  // WebkitTapHighlightColor : 'transparent',
  // border: `1px dashed ${alpha(theme.palette.grey[500], 0.32)}`,
}));

const StyledPlaceholder = styled('div')(({theme}) => ({
  zIndex: 7,
  display: 'flex',
  borderRadius: '50%',
  position: 'absolute',
  alignItems: 'center',
  flexDirection: 'column',
  justifyContent: 'center',
  width: `calc(100% - 16px)`,
  height: `calc(100% - 16px)`,
  color: theme.palette.text.disabled,
  backgroundColor: theme.palette.background.neutral,
  transition: theme.transitions.create('opacity', {
    easing: theme.transitions.easing.easeInOut,
    duration: theme.transitions.duration.shorter,
  }),
}));

// ----------------------------------------------------------------------

UploadChildrenAvatar.propTypes = {
  onClick: PropTypes.func,
};

export default function UploadChildrenAvatar({profilePictureUrl, onClick, ...other}) {
  const {getRootProps, getInputProps} = useDropzone({
    multiple: false,
    accept: {
      'image/*': ['.jpeg', '.gif', '.png'],
    },
    ...other,
  });

  const image = profilePictureUrl;

  if (isNative()) {
    return (
      <Box sx={{position: 'relative'}}>
        <StyledDropZone tabIndex={1} onClick={onClick}>
          <ImageDropZoneChildren image={image}/>
        </StyledDropZone>
      </Box>
    )
  } else {
    return (
      <Box sx={{position: 'relative'}}>
        <StyledDropZone {...getRootProps()} tabIndex={1}>
          <input title={ALT_STRING.COMMON.ADD_IMAGE} {...getInputProps()}  />
          <ImageDropZoneChildren image={profilePictureUrl}/>
        </StyledDropZone>
      </Box>
    )
  }
}

function ImageDropZoneChildren({image}) {
  return (
    <>
      {image
        ? <AvatarChildrenPreview file={image}/>
        : <DropZoneChildren/>
      }
    </>
  )
}

function DropZoneChildren() {
  const [user, setUser] = useRecoilState(forMyPageEntrySelector);

  return (
    <Stack
      direction="row"
      sx={{
        alignItems: 'center',
      }}
    >
      <SvgCommonIcons type={ICON_TYPE.CHOICE_PHOTO}/>
      <Typography
        value="촬영 또는 라이브러리에서 선택"
        sx={{
          fontStyle: 'normal',
          fontWeight: 500,
          fontSize: '16px',
          lineHeight: '24px',
          color: '#222222',
          paddingX: '10px',
          paddingY: '20px',
        }}
      >
        촬영 또는 라이브러리에서 선택
      </Typography>
    </Stack>
  )
}
