import {Box, Stack, Typography} from "@mui/material";
import TextMaxLine from "../../text-max-line";
import {nvlString} from "../../../utils/formatString";
import * as React from "react";
import {QNA_CATEGORY_TEXT} from "../../../constant/my/Setting";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";

export default function OneOnOneInquiriesItem({item, onClick}) {
  return (
    <Stack
      tabIndex={1}
      sx={{display: 'flex', flexDirection: 'row', height: '48px', alignItems: 'center'}}
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
    >
      <Box sx={{
        height: '24px',
        minWidth: 'fit-content',
        padding: '4px 10px 6px 10px',
        alignItems: 'center',
        backgroundColor: item?.status === "ANSWER_COMPLETED" ? '#ECA548' : '#CCCCCC',
        borderRadius: '20px',
        mr: '8px',
      }}>
        <Typography fontWeight="500" fontSize="14px"
                    sx={{lineHeight: '14px', color: 'white'}}>
          {item?.status === "ANSWER_COMPLETED" ? '답변완료' : '미답변'}
        </Typography>
      </Box>
      <TextMaxLine line={1} sx={{fontSize: '16px', fontWeight: 400, lineHeight: '24px', color: '#222222'}}>
        {QNA_CATEGORY_TEXT(item?.category)} {nvlString(item?.inquiryTitle)}
      </TextMaxLine>
    </Stack>
  )
}