import PropTypes from "prop-types";
import {Box, Button, Stack, Typography} from "@mui/material";
import ScrollContainer from "react-indiana-drag-scroll";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";

WrapGroupButton.propTypes = {
  items: PropTypes.array,
  selected: PropTypes.string,
  onClick: PropTypes.func,
};

export default function WrapGroupButton({items, selected, onClick}) {
  return (
    <ScrollContainer className="scroll-container">
      <Stack direction="row" spacing={'10px'}>
        {items?.map((item, index) => (
          <Box
            tabIndex={2}
            key={index}
            sx={{position: 'relative', ...clickBorderNone}}
            onKeyDown={(e) => handleEnterPress(e, () => onClick(item))}
          >
            <Button
              variant={'contained'}
              value={item?.category}
              onClick={() =>
                onClick(item)
              }
              sx={{
                px: 0,
                height: '42px',
                lineHeight: '26px',
                color: item?.category === selected ? '#FFFFFF' : '#666666',
                border: 1,
                borderColor: item?.category === selected ? 'primary.main' : '#E6E6E6',
                borderRadius: '99px',
                backgroundColor: item?.category === selected ? 'primary.main' : '#FFFFFF',
                '&:hover': {
                  boxShadow: 'none',
                  backgroundColor: item?.category === selected ? 'primary.main' : '#FFFFFF',
                },
              }}
            >
              <Typography noWrap sx={{mx: '20px', fontSize: '14px', fontWeight: item?.category === selected ? 800 : 500}}>
                {item?.text}
              </Typography>
            </Button>
          </Box>
        ))}
      </Stack>
    </ScrollContainer>
  )
}