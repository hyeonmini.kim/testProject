import {Box, Stack, Typography} from "@mui/material";
import TextMaxLine from "../../text-max-line";
import {getYYMMDDFromDate, nvlString} from "../../../utils/formatString";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";

export default function NoticeItem({item, onClick}) {
  return (
    <Box
      tabIndex={1}
      sx={{
        flexDirection: 'column',
        minHeight: '64px',
        maxHeight: '98px',
        display: 'flex',
        justifyContent: 'space-between',
        ...clickBorderNone
      }}
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
    >
      <TextMaxLine open={true} line={2} sx={{
        mt: '10px',
        mb: '8px',
        fontSize: '16px',
        fontWeight: 400,
        lineHeight: '24px',
        color: '#000000'
      }}>
        {nvlString(item.title)}
      </TextMaxLine>
      <Stack sx={{mb: '12px', flexDirection: 'row', alignItems: 'center'}}>
        <Typography align={'center'} sx={{fontSize: '14px', fontWeight: 400, lineHeight: '20px', color: '#888888'}}>
          {getYYMMDDFromDate(item?.createdDate)}
        </Typography>
        {item?.isNewPost && <SvgCommonIcons type={ICON_TYPE.NEW_ITEM_DOT} sx={{ml: '5px'}}/>}
      </Stack>
    </Box>
  )
}