import {Box, Stack, Typography} from "@mui/material";
import TextMaxLine from "../../text-max-line";
import {getYYMMDDFromDate, nvlString} from "../../../utils/formatString";
import {FAQ_CATEGORY_TEXT} from "../../../constant/my/mock/FAQ";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";

export default function FAQItem({item, onClick}) {
  return (
    <Box
      tabIndex={3}
      sx={{flexDirection: 'column', height: '74px', display: 'flex', justifyContent: 'space-between', ...clickBorderNone}}
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
    >
      <TextMaxLine line={1} sx={{mt: '10px', fontSize: '16px', fontWeight: 400, lineHeight: '24px', color: '#000000'}}>
        {FAQ_CATEGORY_TEXT(item?.faqCategory)} {nvlString(item?.question)}
      </TextMaxLine>
      <Stack sx={{mb: '12px', flexDirection: 'row', alignItems: 'center'}}>
        <Typography align={'center'} sx={{fontSize: '14px', fontWeight: 400, lineHeight: '20px', color: '#888888'}}>
          {getYYMMDDFromDate(item?.createdDatetime)}
        </Typography>
      </Stack>
    </Box>
  )
}