import {Box, IconButton, Switch, Typography} from "@mui/material";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";

export default function SettingItem({title, toggle, onClick}) {
  return (
    <Box
      tabIndex={1}
      alignItems={'center'}
      sx={{height: '56px', display: 'flex', justifyContent: 'space-between', ...clickBorderNone}}
      onClick={toggle !== undefined ? undefined : onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
    >
      <Box sx={{flexGrow: 1}}>
        <Typography sx={{
          fontSize: '16px',
          pr: '10px',
          fontWeight: 400,
          lineHeight: '24px',
          color: '#000000',
          whiteSpace: 'pre-wrap',
          wordBreak: 'keep-all'
        }}>
          {title}
        </Typography>
      </Box>
      {toggle !== undefined ? (
        <Switch
          checked={toggle}
          onChange={onClick}
          sx={{
            p: '5px',
            mx: '-10px',
            boxShadow: 'none',
            ".MuiSwitch-thumb": {
              width: '22px',
              height: '22px',
            },
            ".MuiButtonBase-root": {
              left: '0px',
              height: '38px',
              transform: 'translateX(-3px)',
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: 'transparent',
              }
            },
            ".MuiSwitch-switchBase.Mui-checked": {
              left: '0px',
              height: '38px',
              transform: 'translateX(15px)',
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: 'transparent',
              }
            }
          }}/>
      ) : (
        <IconButton tabIndex={-1} disableRipple onClick={onClick} sx={{
          m: '-8px',
          backgroundColor: "#FFFFFF",
          '&:hover': {
            boxShadow: 'none',
            backgroundColor: '#FFFFFF',
          },
        }}>
          <SvgCommonIcons alt={ALT_STRING.SETTING.MOVE(title)} type={ICON_TYPE.ARROW_RIGHT_24PX}/>
        </IconButton>
      )
      }
    </Box>
  )
}