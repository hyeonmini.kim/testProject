import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {Box, Container, Skeleton, Stack} from "@mui/material";
import {Swiper, SwiperSlide} from "swiper/react";
import {Pagination} from "swiper";
import {useRecoilState} from "recoil";
import {labSelectedSurveyTypeSelector} from "../../recoil/selector/lab/labSelector";
import {LAB_TYPE} from "../../constant/lab/LabCommon";
import LabRankingCardSkeletonItem from "./LabRankingCardSkeletonItem";

export default function LabRankingCardSkeleton({type}) {
  const swiperRef = useRef(null)
  const [swiperIndex, setSwiperIndex] = useState(0);
  const [selectedSurveyType, setSelectedSurveyType] = useRecoilState(labSelectedSurveyTypeSelector);

  useEffect(() => {
    setSwiperIndex(0)
    swiperRef?.current?.slideTo(0, 0)
  }, [selectedSurveyType])

  return (
    <>
      <Container disableGutters sx={{mt: '40px'}}>
        <Swiper
          onSwiper={(swiper) => {
            swiperRef.current = swiper
          }}
          onActiveIndexChange={(swiperCore) => {
            setSwiperIndex(swiperCore.activeIndex);
          }}
          slidesPerView={1.03}
          modules={[Pagination]}
          spaceBetween={20}
          style={{
            paddingTop: '40px',
            marginTop: '-40px',
            paddingLeft: '87px',
            // marginRight: '-87px',
            paddingRight: '87px',
            paddingBottom: '20px',
            marginBottom: '-20px',

          }}
        >
          {([...Array(5)]).map((item, index) =>
            <SwiperSlide key={index}>
              <LabRankingCardSkeletonItem/>
            </SwiperSlide>
          )}
        </Swiper>

        <Box sx={{height: '10px'}}/>

        {([...Array(type === LAB_TYPE.BABY_FOOD ? 7 : 6)]).map((item, index) =>
          <Stack direction="row" key={index} sx={{position: 'relative', mx: '20px', my: '10px'}}>
            <Skeleton variant="rectangular" sx={{width: '100%', height: '38px', borderRadius: '5px'}}/>
          </Stack>
        )}

      </Container>
    </>
  )
}