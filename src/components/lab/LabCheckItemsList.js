import {Box, Grid, Typography} from "@mui/material";
import {clickBorderNone} from "../../constant/common/Common";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import * as React from "react";

export default function LabCheckItemsList({list, handleClick, surveyType}) {
  return (
    <Box sx={{flexGrow: 1}}>
      <Grid
        container
        rowSpacing="8px"
        sx={{marginTop: '22px', textAlign: 'center'}}
      >
        {list?.map((obj, idx) => (
          <Grid item key={idx} xs={12} sx={{height: '68px', '& .MuiGrid-root.MuiGrid-item': {padding: 0}}}>
            <Box
              tabIndex={0}
              sx={{
                height: '100%',
                borderRadius: '8px',
                backgroundColor: obj?.value ? '#FFEED8' : '#F5F5F5',
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                ...clickBorderNone
              }}
              onClick={() => handleClick(obj?.code, surveyType)}
              onKeyDown={(e) => handleEnterPress(e, () => handleClick(obj?.code, surveyType))}
            >
              <Box
                sx={{
                  ml: '17px',
                  my: '16px',
                  pt: '4px'
                }}
              >
                {obj?.value ? <SvgCommonIcons type={ICON_TYPE.CHECK_FILLED_24PX}/> : <SvgCommonIcons type={ICON_TYPE.CHECK_24PX}/>}
              </Box>
              <Box sx={{width: '12px'}}/>
              <Typography
                sx={{
                  fontWeight: obj?.value ? 700 : 500,
                  fontSize: '18px',
                  lineHeight: '24px',
                  letterSpacing: '-0.03em',
                  color: obj?.value ? '#ECA548' : '#222222',
                  py: '18px',
                }}
              >
                {obj?.text}
              </Typography>
            </Box>
          </Grid>
        ))}
      </Grid>
    </Box>
  )
}