import {Avatar, Stack, Typography} from "@mui/material";
import {UserBadge} from "../../constant/icons/UserBadge";
import * as React from "react";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {ALT_STRING} from "../../constant/common/AltString";

export default function AvatarProfile({avatar, nickname, type, grade, date, sx}) {
  return (
    <Stack
      direction={'row'}
      sx={{
        alignItems: 'center',
        position: 'relative',
        ...sx
      }}
    >

      {/* 아바타 */}
      <WriterAvatar alt={ALT_STRING.COMMON.NICKNAME(nickname)} avatar={avatar}/>

      {/* 닉네임 & 날짜*/}
      <NickNameAndDate nickname={nickname} date={date}/>

      {/* 뱃지 */}
      <UserBadge
        type={type}
        grade={grade}
        sx={{position: 'absolute', left: '32px', bottom: '0px', width: '16px', zIndex: 11}}
      />

    </Stack>
  )
}

function WriterAvatar({alt, avatar}) {
  return (
    avatar
      ? (
        <Avatar
          alt={alt}
          src={avatar}
          sx={{
            width: 40,
            height: 40,
            alignItems: 'center',
          }}
        />
      ) : (
        <SvgCommonIcons
          alt={ALT_STRING.HOME.PROFILE}
          type={ICON_TYPE.PROFILE_NO_IMAGE_30PX}
          sx={{width: 40, height: 40}}
        />
      )
  )
}

function NickNameAndDate({nickname, date, sx}) {
  return (
    date
      ? (
        <Stack spacing={'8px'} sx={{height: '40px', display: 'flex', flexDirection: 'column'}}>
          <Typography sx={{pl: '16px', fontSize: '16px', fontWeight: 400, lineHeight: '16px', color: '#222222', ...sx}}>
            {nickname}
          </Typography>
          <Typography sx={{pl: '16px', fontSize: '14px', fontWeight: 400, lineHeight: '16px', color: '#888888', ...sx}}>
            {date}
          </Typography>
        </Stack>
      ) : (
        <Typography sx={{pl: '16px', fontSize: '16px', fontWeight: 400, lineHeight: '16px', color: '#222222', ...sx}}>
          {nickname}
        </Typography>
      )
  )
}