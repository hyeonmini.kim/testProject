import {Box, Grid, Typography} from "@mui/material";
import {clickBorderNone} from "../../constant/common/Common";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import * as React from "react";
import {SvgLabIcons} from "../../constant/icons/lab/LabIcons";
import {LAB_TYPE, SURVEY_TYPE} from "../../constant/lab/LabCommon";
import {BABYFOOD_TEXT, NUTRITION_TEXT} from "../../constant/common/labKeyword";

export default function LabBrandItems({list, handleClick, type, etcText, surveyType}) {
  return (
    <Box sx={{flexGrow: 1}}>
      <Grid
        container
        columnSpacing="10px"
        rowSpacing="12px"
        sx={{marginTop: '22px', textAlign: 'center'}}
      >
        {list?.map((obj, idx) => (
          <Grid item key={idx} xs={4} sx={{height: '154px', '& .MuiGrid-root.MuiGrid-item': {padding: 0}}}>
            <Box
              tabIndex={0}
              sx={{
                height: '100%',
                border: obj?.value ? '1px solid #ECA548' : '1px solid #E6E6E6',
                borderRadius: '8px',
                backgroundColor: '#FFFFFF',
                display: 'flex',
                flexDirection: 'column',
                ...clickBorderNone
              }}
              onClick={() => handleClick(obj?.text, surveyType)}
              onKeyDown={(e) => handleEnterPress(e, () => handleClick(obj?.text, surveyType))}
            >
              {
                type === LAB_TYPE.BABY_FOOD
                  ? <BabyFoodBrandItem obj={obj} etcText={etcText}/>
                  : <NutritionItem obj={obj} etcText={etcText} surveyType={surveyType}/>
              }
            </Box>
          </Grid>
        ))}
      </Grid>
    </Box>
  )
}

function BabyFoodBrandItem({obj, etcText}) {
  return (
    <>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: '18px',
          paddingBottom: '8px',
        }}
      >
        {
          obj?.text !== etcText || !obj?.value
            ? <SvgLabIcons type={obj?.code}/>
            : <SvgLabIcons type={BABYFOOD_TEXT.BOB09}/>
        }
      </Box>
      <Typography
        sx={{
          fontWeight: obj?.value ? 500 : 400,
          fontSize: '12px',
          lineHeight: '18px',
          letterSpacing: '-0.03em',
          color: obj?.value ? '#ECA548' : '#999999',
          paddingBottom: '18px',
        }}
      >
        {obj?.text}
      </Typography>
    </>
  )
}

function NutritionItem({obj, etcText, surveyType}) {
  return (
    <>
      {
        obj?.text !== etcText ? <TwoLine obj={obj}/> : <OneLine obj={obj} etcText={etcText} surveyType={surveyType}/>
      }
    </>
  )
}

function TwoLine({obj}) {
  return (
    <>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: '17px',
          paddingBottom: '4px',
        }}
      >
        <SvgLabIcons type={obj?.code}/>
      </Box>
      <Typography
        sx={{
          fontWeight: obj?.value ? 900 : 700,
          fontSize: '12px',
          lineHeight: '12px',
          letterSpacing: '-0.02em',
          color: obj?.value ? '#ECA548' : '#999999',
          paddingBottom: '6px',
        }}
      >
        {obj?.text}
      </Typography>
      <Typography
        sx={{
          fontWeight: obj?.value ? 500 : 400,
          fontSize: '10px',
          lineHeight: '10px',
          letterSpacing: '-0.02em',
          color: obj?.value ? '#ECA548' : '#999999',
          paddingBottom: '10px',
        }}
      >
        {obj?.example}
      </Typography>
    </>
  )
}

function OneLine({obj, etcText, surveyType}) {
  return (
    <>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: '26px',
          paddingBottom: '2px',
        }}
      >
        {
          obj?.text !== etcText || !obj?.value
            ? <SvgLabIcons type={obj?.code}/>
            : getEtcSelectedIcon(surveyType)
        }
      </Box>
      <Typography
        sx={{
          fontWeight: obj?.value ? 900 : 700,
          fontSize: '12px',
          lineHeight: '18px',
          letterSpacing: '-0.02em',
          color: obj?.value ? '#ECA548' : '#999999',
        }}
      >
        {obj?.text}
      </Typography>
    </>
  )
}

function getEtcSelectedIcon(surveyType) {

  if (surveyType === SURVEY_TYPE.SUR03.code) {
    return (
      <SvgLabIcons type={NUTRITION_TEXT.LAC13}/>
    )
  } else if (surveyType === SURVEY_TYPE.SUR04.code) {
    return (
      <SvgLabIcons type={NUTRITION_TEXT.VIT13}/>
    )
  } else if (surveyType === SURVEY_TYPE.SUR05.code) {
    return (
      <SvgLabIcons type={NUTRITION_TEXT.IRO13}/>
    )
  }
}