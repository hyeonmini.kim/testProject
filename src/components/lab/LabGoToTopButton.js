import {Container, IconButton} from "@mui/material";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {ALT_STRING} from "../../constant/common/AltString";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../constant/common/Common";
import * as React from "react";
import {Z_INDEX} from "../../constant/common/ZIndex";
import {sxFixedCenterMainLayout} from "../../layouts/main/MainLayout";

export default function LabGoToTopButton({dialog, mainRef}) {

  const styleContainer = {
    position: 'fixed',
    bottom: '0px',
    zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
    '&.MuiContainer-root': {background: 'none',},
  }

  return (
    <Container
      maxWidth={'xs'}
      disableGutters
      sx={dialog ? {...styleContainer, ...sxFixedCenterMainLayout} : styleContainer}
    >
      <IconButton
        tabIndex={-1}
        disableFocusRipple
        onClick={() => mainRef?.current?.scrollTo({top: 0, left: 0, behavior: 'smooth'})}
        sx={{
          position: 'fixed',
          bottom: '20px',
          right: '20px',
          m: '-8px',
          '&:hover': {
            boxShadow: 'none',
            backgroundColor: 'transparent'
          },
        }}
      >
        <SvgCommonIcons
          alt={ALT_STRING.RECIPE_DETAIL.BTN_MOVE_UP}
          tabIndex={0}
          onKeyDown={(e) => handleEnterPress(e, () => mainRef?.current?.scrollTo({top: 0, left: 0, behavior: 'smooth'}))}
          type={ICON_TYPE?.BTN_TOP}
          sx={{...clickBorderNone}}/>
      </IconButton>
    </Container>
  )
}