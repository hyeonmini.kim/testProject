import {Box, Button, Container} from "@mui/material";
import {clickBorderNone} from "../../constant/common/Common";
import * as React from "react";

export default function LabBottomFixedButton({btnRef, handleNextButton, isDisabled, text}) {

  return (
    <Box
      sx={{
        px: '20px',
        py: '20px',
        position: 'fixed',
        bgcolor: 'white',
        zIndex: 1,
        bottom: 0,
        left: 0,
        right: 0,
      }}
    >
      <Container maxWidth={'xs'} disableGutters>
        <Button
          tabIndex={0}
          disableFocusRipple
          fullWidth variant="contained"
          onClick={handleNextButton}
          disabled={isDisabled}
          ref={btnRef}
          sx={{
            height: '52px',
            fontSize: '16px',
            fontWeight: '700',
            lineHeight: '28px',
            backgroundColor: isDisabled ? '#E6E6E6' : 'primary.main',
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: isDisabled ? '#E6E6E6' : 'primary.main',
            },
            ...clickBorderNone,
          }}
        >
          {text}
        </Button>
      </Container>
    </Box>
  )
}