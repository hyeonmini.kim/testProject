import {Box, Button, Container, Divider, Stack, Typography} from "@mui/material";
import {SURVEY_COMMON} from "../../constant/lab/LabCommon";
import {clickBorderNone} from "../../constant/common/Common";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import * as React from "react";
import {useEffect, useState} from "react";
import {SvgLabIcons} from "../../constant/icons/lab/LabIcons";
import {replaceItemAtIndex} from "../../utils/arrayUtils";
import {useRecoilState} from "recoil";
import {labIsItemSelectedSelector, labRecommendListSelector, labScoreSelector} from "../../recoil/selector/lab/labSelector";
import {setLocalStorage} from "../../utils/storageUtils";
import {TIME} from "../../constant/common/Time";
import useStackNavigation from "../../hooks/useStackNavigation";

export default function LabRecommendContents({onClose, handleButtonClick}) {

  const [labRecommendList, setLabRecommendList] = useRecoilState(labRecommendListSelector)
  const [labIsItemSelected, setLabIsItemSelected] = useRecoilState(labIsItemSelectedSelector)
  const [score, setScore] = useRecoilState(labScoreSelector)

  const [isDisabled, setIsDisabled] = useState(true)

  const {navigation} = useStackNavigation()
  useEffect(() => {
    if (labIsItemSelected) {
      setIsDisabled(false)
    } else {
      setIsDisabled(true)
    }
  }, [labIsItemSelected])

  const handleTextClick = () => {
    setLocalStorage('isLabBottomDialogNotOpenOneDay', true, TIME.MS_ONE_DAY)
    onClose()
    navigation.back()
  }

  const handleItemClick = (item) => {
    let newList = labRecommendList;

    const selectedIndex = labRecommendList?.findIndex((listItem) => listItem?.value === true)
    const index = labRecommendList?.findIndex((listItem) => listItem?.text === item)

    if (selectedIndex >= 0 && index !== selectedIndex) {
      newList = replaceItemAtIndex(newList, selectedIndex, {
        text: newList[selectedIndex]?.text,
        value: false,
        image: newList[selectedIndex]?.image,
        code: newList[selectedIndex]?.code,
      })

      newList = replaceItemAtIndex(newList, index, {
        text: item,
        value: !newList[index]?.value,
        image: newList[index]?.image,
        code: newList[index]?.code,
      })
    } else {
      const index = labRecommendList?.findIndex((listItem) => listItem?.text === item)

      newList = replaceItemAtIndex(newList, index, {
        text: item,
        value: !newList[index]?.value,
        image: newList[index]?.image,
        code: newList[index]?.code,
      })
    }

    const idx = newList?.findIndex((listItem) => listItem?.value === true)
    if (idx >= 0) {
      setLabIsItemSelected(true)
      setScore(labRecommendList[index]?.text)
    } else {
      setLabIsItemSelected(false)
      setScore(labRecommendList[index]?.text)
    }

    setLabRecommendList(newList)
  }

  return (
    <Box sx={{mt: '5px', mb: '11px', textAlign: 'start'}}>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '20px',
          lineHeight: '20px',
          color: '#000000',
          paddingY: '25px'
        }}
      >
        {SURVEY_COMMON.TEXT.BOTTOM_DIALOG_TITLE}
      </Typography>
      <Divider sx={{height: '1px', backgroundColor: '#E6E6E6'}}/>

      <Stack direction="row" justifyContent="space-between" alignItems="center" sx={{mt: '30px', mb: '15px'}}>
        {labRecommendList.map((item, index) => (
          <Box
            tabIndex={0}
            key={index}
            onClick={() => handleItemClick(item?.text)}
            onKeyDown={(e) => handleEnterPress(e, () => handleItemClick(item?.text))}
            sx={{pt: labIsItemSelected ? '0px' : '3px', pb: labIsItemSelected ? '0px' : '3px', ...clickBorderNone}}
          >
            <SvgLabIcons type={item?.code} sx={{width: item?.value ? '50px' : '40px', height: item?.value ? '50px' : '40px'}}/>
            <Box sx={{height: item?.value ? '10px' : '14px'}}/>
            <Typography
              sx={{
                fontStyle: 'normal',
                fontWeight: item?.value ? 500 : 400,
                fontSize: item?.value ? '18px' : '12px',
                lineHeight: '12px',
                color: '#222222',
                textAlign: 'center'
              }}
            >
              {item?.text}
            </Typography>
          </Box>))}
      </Stack>

      <Box
        sx={{
          py: '10px',
          bgcolor: 'white',
          zIndex: 1,
          bottom: 0,
          left: 0,
          right: 0,
        }}
      >
        <Container maxWidth={'xs'} disableGutters>
          <Button
            tabIndex={0}
            disableFocusRipple
            fullWidth
            variant="contained"
            onClick={handleButtonClick}
            disabled={isDisabled}
            sx={{
              height: '52px',
              fontSize: '16px',
              fontWeight: '700',
              lineHeight: '28px',
              backgroundColor: 'primary.main',
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: 'primary.main',
              },
              ...clickBorderNone,
            }}
          >
            {SURVEY_COMMON.BUTTON.CONFIRM}
          </Button>
        </Container>
      </Box>

      <Typography
        tabIndex={0}
        sx={{
          fontStyle: 'normal',
          fontWeight: 400,
          fontSize: '14px',
          lineHeight: '14px',
          color: '#999999',
          textAlign: 'center',
          my: '10px'
        }}
        onKeyDown={(e) => handleEnterPress(e, handleTextClick)}
        onClick={handleTextClick}
      >
        {SURVEY_COMMON.BUTTON.DONT_SHOW_AGAIN}
      </Typography>
    </Box>
  )
}