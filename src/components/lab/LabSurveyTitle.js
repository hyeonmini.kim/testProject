import {Typography} from "@mui/material";
import * as React from "react";

export default function LabSurveyTitle({title}) {
  return (
    <Typography
      sx={{
        fontStyle: 'normal',
        fontWeight: 700,
        fontSize: '21px',
        lineHeight: '24px',
        letterSpacing: '-0.02em',
        color: '#000000',
        whiteSpace: 'pre-wrap',
        textAlign: ''
      }}
    >
      {title}
    </Typography>
  )
}