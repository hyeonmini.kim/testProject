import {Box, Card, CardContent, Skeleton} from "@mui/material";
import * as React from "react";

export default function LabRankingCardSkeletonItem() {
  return (
    <Card
      sx={{
        // width: '145px',
        p: '20px',
        borderRadius: '8px',
        boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.08)',
      }}
    >
      <Skeleton variant="rectangular" sx={{width: '18px', height: '30px', borderRadius: '5px'}}/>
      <Box>
        <Skeleton variant="rectangular" sx={{width: '107px', height: '92px', borderRadius: '5px', m: '0 auto'}}/>
      </Box>
      <CardContent sx={{mt: '16px', p: 0, "&:last-child": {pb: 0}}}>
        <Skeleton variant="rectangular" sx={{width: '44px', height: '24px', borderRadius: '5px', mb: '4px'}}/>
        <Skeleton variant="rectangular" sx={{width: '145px', height: '20px', borderRadius: '5px'}}/>
      </CardContent>
    </Card>
  )
}