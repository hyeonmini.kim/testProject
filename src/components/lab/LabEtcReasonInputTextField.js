import {Box, Grid} from "@mui/material";
import TextFieldWrapper from "../common/TextFieldWrapper";
import * as React from "react";

export default function LabEtcReasonInputTextField({placeHolder, brandName, handleBrandName}) {
  return (
    <Box sx={{mt: '20px'}}>
      <Grid
        item xs={12}
      >
        <Box
          rowGap={'5px'}
          display="grid"
        >
          <TextFieldWrapper
            autoComplete='off'
            placeholder={placeHolder}
            value={brandName}
            multiline
            minRows={9}
            onChange={handleBrandName}
            sx={{borderColor: 'red'}}
            inputProps={{tabIndex: 1, style: {padding: '10px'}}}
            InputProps={{
              sx: {
                '& textarea': {
                  fontSize: '16px', fontWeight: '500', lineHeight: '24px', color: '#222222'
                },
                '& textarea::placeholder': {
                  fontSize: '16px', fontWeight: '500', lineHeight: '24px', color: '#CCCCCC'
                }
              }
            }}
            sx={{
              "& .MuiOutlinedInput-root": {
                padding: '10px',
                '& fieldset': {
                  border: '1px solid #E6E6E6',
                },
                // border: '1px solid #F5F5F5'
              },
              '& input::placeholder': {
                color: '#CCCCCC',
                style: {
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '24px',
                  color: '#CCCCCC',
                },
              },
            }}
          />
        </Box>
      </Grid>
    </Box>
  )
}