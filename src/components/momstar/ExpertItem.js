import PropTypes from "prop-types";
import {Avatar, Box, IconButton, Stack, Typography} from "@mui/material";
import TextMaxLine from "../text-max-line";
import {MotionContainer} from "../animate";
import React from "react";
import {getNumberTenThousand} from "../../utils/formatNumber";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {BADGE_ICON_TYPE, BadgeIcons} from "../../constant/icons/Badge/BadgeIcons";
import {RankingIcons} from "../../constant/icons/RankingIcons";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../constant/common/AltString";
import {COUNT_TITLE} from "../../constant/recipe/detail/DetailConstants";

ExpertItem.propTypes = {
  item: PropTypes.object,
  onClick: PropTypes.func,
};

export default function ExpertItem({item, rank, onClick}) {
  return (
    <MotionContainer>
      <Stack
        tabIndex={2}
        direction={'row'}
        sx={{display: 'flex'}}
        onClick={() => onClick(item)}
        onKeyDown={(e) => handleEnterPress(e, () => onClick(item))}
      >
        <Box sx={{m: 0, p: 0, position: 'relative'}}>
          {
            item?.profilePictureUrl
              ? <AvatarSection alt={ALT_STRING.COMMON.NICKNAME(item?.nickname)} avatar={item?.profilePictureUrl}/>
              : <SvgCommonIcons alt={ALT_STRING.COMMON.NICKNAME(item?.nickname)} type={ICON_TYPE.PROFILE_NO_IMAGE_80PX} sx={{width: '60px'}}/>
          }
          <RankingIcons rank={rank} sx={{position: 'absolute', left: '0px', top: '0px'}}/>
          <BadgeIcons
            alt={ALT_STRING.COMMON.BADGE_LV4}
            type={BADGE_ICON_TYPE.BADGE_LV4_24PX}
            sx={{position: 'absolute', right: '-6px', bottom: '1px', width: '24px'}}
          />
        </Box>
        <Box sx={{
          mx: '12px',
          p: 0,
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
          flexGrow: 1
        }}>
          <Typography sx={{fontSize: '16px', fontWeight: 500, lineHeight: '24px', color: '#222222'}}>
            {item?.nickname}
          </Typography>
          <TextMaxLine line={1} sx={{fontSize: '14px', fontWeight: 400, lineHeight: '20px', color: '#222222'}}>
            {item?.briefBio}
          </TextMaxLine>
          <Box sx={{flexGrow: 1}}/>
          <Stack direction={'row'} alignItems={'center'} sx={{mt: '3px'}}>
            <Typography sx={{fontSize: '12px', lineHeight: '12px', mr: '16px', fontWeight: 400, color: '#888888'}}>
              {`${COUNT_TITLE.VIEWS} ${getNumberTenThousand(item?.pageviewedCount)}`}
            </Typography>
            <Typography sx={{fontSize: '12px', lineHeight: '12px', mr: '16px', fontWeight: 400, color: '#888888'}}>
              {`${COUNT_TITLE.SCRAP} ${getNumberTenThousand(item?.scrapbookedCount)}`}
            </Typography>
            <Typography sx={{fontSize: '12px', lineHeight: '12px', mr: '16px', fontWeight: 400, color: '#888888'}}>
              {`${COUNT_TITLE.RECIPE} ${getNumberTenThousand(item?.recipePostingCount)}`}
            </Typography>
          </Stack>
        </Box>
        <IconButton
          tabIndex={-1}
          sx={{
            m: '0 -8px',
            backgroundColor: "#FFFFFF",
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: '#FFFFFF',
            },
          }}
        >
          <SvgCommonIcons alt={ALT_STRING.COMMON.BTN_MOVE} type={ICON_TYPE.ARROW_RIGHT_24PX}/>
        </IconButton>
      </Stack>
    </MotionContainer>
  );
}

function AvatarSection({alt, avatar}) {
  return (
    <Avatar
      alt={alt}
      src={avatar}
      sx={{
        width: '60px',
        height: '60px',
        border: 1,
        borderColor: '#E6E6E6',
      }}
    />
  )
}