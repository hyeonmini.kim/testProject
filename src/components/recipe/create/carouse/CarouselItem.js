import PropTypes from "prop-types";
import {fileData} from "../../../file-thumbnail";
import {Box, Paper} from "@mui/material";
import Image from "../../../image";

CarouselItem.propTypes = {
  image: PropTypes.any,
};

export default function CarouselItem({image}) {
  const {preview = ''} = fileData(image.image_file_path);
  return (
    <Paper sx={{borderRadius: 0, overflow: 'hidden', position: 'relative',}}>
      <Image alt={''} src={preview} ratio="1/1"/>
      <Box
        sx={{
          bottom: 0,
          zIndex: 9,
          width: '100%',
          height: '130px',
          textAlign: 'left',
          position: 'absolute',
          color: 'common.white',
          background: 'linear-gradient(0deg, rgba(0, 0, 0, 0.8) 11.38%, rgba(217, 217, 217, 0) 91.87%)'
        }}
      >
      </Box>
      <Box
        sx={{
          top: 0,
          zIndex: 9,
          width: '100%',
          height: '58px',
          textAlign: 'left',
          position: 'absolute',
          color: 'common.white',
          background: 'linear-gradient(180deg, rgba(0, 0, 0, 0.8) 11.38%, rgba(217, 217, 217, 0) 91.87%)'
        }}
      >
      </Box>
    </Paper>
  )
}