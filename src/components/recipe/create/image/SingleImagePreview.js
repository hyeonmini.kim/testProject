import PropTypes from 'prop-types';
import Image from "../../../image";
import {ALT_STRING} from "../../../../constant/common/AltString";
// ----------------------------------------------------------------------

SingleImagePreview.propTypes = {
  file: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

export default function SingleImagePreview({index, file}) {
  if (!file) {
    return null;
  }
  const imgUrl = typeof file === 'string' ? file : file.preview;
  return (
    <Image
      alt={`스텝 ${index + 1} 이미지`}
      src={imgUrl}
      ratio={'4/3'}
      isLazy={false}
      sx={{
        height: '60px',
      }}
    />
  );
}
