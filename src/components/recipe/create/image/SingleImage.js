import PropTypes from 'prop-types';
import {useDropzone} from 'react-dropzone';
import {Box, Stack, Typography} from '@mui/material';
import {styled} from '@mui/material/styles';
import Iconify from "../../../iconify";
import {useRecoilValue} from "recoil";
import {createRecipeStepsSelector} from "../../../../recoil/selector/recipe/createSelector";
import {isNative} from "../../../../utils/envUtils";
import SingleImagePreview from "./SingleImagePreview";
import {RECIPE_CREATE_COMMON} from "../../../../constant/recipe/Create";
import {SvgDeleteIcon} from "../../../../constant/icons/icons";
import {clickBorderNone} from "../../../../constant/common/Common";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../../constant/common/AltString";
// ----------------------------------------------------------------------

const StyledDropZone = styled('div')(({theme}) => ({
  outline: 'none',
  cursor: 'pointer',
  overflow: 'hidden',
  alignItems: 'center',
  justifyContent: 'center',
  width: '80px',
  transition: theme.transitions.create('padding'),
  backgroundColor: '#F5F5F5',
  '&:active': {
    backgroundColor: '#E6E6E6'
  },
  border: '1px solid #E6E6E6',
  borderRadius: '8px',
}));

// ----------------------------------------------------------------------

SingleImage.propTypes = {
  onDrop: PropTypes.func,
  onClick: PropTypes.func,
  onDelete: PropTypes.func,
  imageIndex: PropTypes.number,
};

export default function SingleImage({imageIndex, onClick, onDelete, ...other}) {
  const steps = useRecoilValue(createRecipeStepsSelector)
  const {getRootProps, getInputProps} = useDropzone({
    multiple: false,
    accept: {
      'image/*': ['.jpeg', '.gif', '.png'],
    },
    ...other,
  });
  const image = steps[imageIndex]?.image_file_path

  if (isNative()) {
    return (
      <Box sx={{position: 'relative'}}>
        <StyledDropZone tabIndex={0} onClick={onClick} sx={{...clickBorderNone}}>
          <ImageDropZoneChildren index={imageIndex} image={image}/>
        </StyledDropZone>
        {image && <DeleteButtonChildren onDelete={onDelete}/>}
      </Box>
    )
  } else {
    return (
      <Box sx={{position: 'relative'}}>
        <StyledDropZone {...getRootProps()} tabIndex={0} sx={{...clickBorderNone}}>
          <input title={ALT_STRING.COMMON.ADD_IMAGE} {...getInputProps()}  />
          <ImageDropZoneChildren index={imageIndex} image={image}/>
        </StyledDropZone>
        {image && <DeleteButtonChildren onDelete={onDelete}/>}
      </Box>
    )
  }
}

function ImageDropZoneChildren({index, image}) {
  return (
    <>
      {image ? <SingleImagePreview index={index} file={image}/> : <DropZoneChildren/>}
    </>
  )
}

function DeleteButtonChildren({onDelete}) {
  return (
    <SvgDeleteIcon alt={ALT_STRING.RECIPE_CREATE.BTN_DEL_IMAGE} onClick={onDelete} sx={{position: 'absolute', top: 2, right: 2}}/>
  )
}

function DropZoneChildren() {
  return (
    <Stack alignItems="center" justifyContent="center" sx={{height: '60px'}}>
      <Iconify alt={ALT_STRING.RECIPE_CREATE.BTN_ADD_IMAGE} icon="ic:round-add-a-photo" width={24} sx={{color: '#888888'}}/>
      <Typography sx={{fontSize: '10px', fontWeight: '400', lineHeight: '12px', color: '#888888'}}>
        {RECIPE_CREATE_COMMON.TEXT.OPTION}
      </Typography>
    </Stack>
  )
}