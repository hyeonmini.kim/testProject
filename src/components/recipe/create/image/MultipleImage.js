import PropTypes from 'prop-types';
import {useDropzone} from 'react-dropzone';
import {Box, Stack, Typography} from '@mui/material';
import {styled} from '@mui/material/styles';
import {useRecoilValue} from "recoil";
import {createRecipeImagesSelector} from "../../../../recoil/selector/recipe/createSelector";
import {isNative} from "../../../../utils/envUtils";
import Iconify from "../../../iconify";
import {clickBorderNone} from "../../../../constant/common/Common";
import {ALT_STRING} from "../../../../constant/common/AltString";
// ----------------------------------------------------------------------

const StyledDropZone = styled('div')(({theme}) => ({
  outline: 'none',
  cursor: 'pointer',
  overflow: 'hidden',
  alignItems: 'center',
  justifyContent: 'center',
  width: '80px',
  transition: theme.transitions.create('padding'),
  backgroundColor: '#F5F5F5',
  '&:active': {
    backgroundColor: '#E6E6E6'
  },
  border: '1px solid #E6E6E6',
  borderRadius: '8px',
}));

// ----------------------------------------------------------------------

MultipleImage.propTypes = {
  onDrop: PropTypes.func,
  onClick: PropTypes.func,
};

export default function MultipleImage({onClick, ...other}) {
  const {getRootProps, getInputProps} = useDropzone({
    multiple: true,
    accept: {
      'image/*': ['.jpeg', '.gif', '.png'],
    },
    ...other,
  });
  if (isNative()) {
    return (
      <Box sx={{position: 'relative'}}>
        <StyledDropZone onClick={onClick} tabIndex={0} sx={{...clickBorderNone}}>
          <DropZoneChildren/>
        </StyledDropZone>
      </Box>
    )
  } else {
    return (
      <Box sx={{position: 'relative'}}>
        <StyledDropZone {...getRootProps()} tabIndex={0} sx={{...clickBorderNone}}>
          <input title={ALT_STRING.RECIPE_CREATE.ADD_IMAGE} {...getInputProps()}  />
          <DropZoneChildren/>
        </StyledDropZone>
      </Box>
    )
  }
}

function DropZoneChildren() {
  const image_file_path = useRecoilValue(createRecipeImagesSelector)
  return (
    <Stack alignItems="center" justifyContent="center" sx={{height: '80px'}}>
      <Iconify alt={ALT_STRING.RECIPE_CREATE.BTN_ADD_IMAGE} icon="ic:round-add-a-photo" width={24} sx={{color: '#888888'}}/>
      <Typography sx={{fontSize: '11px', fontWeight: '400', lineHeight: '12px', color: '#888888'}}>
        {image_file_path?.length ? image_file_path.length : 0}/5
      </Typography>
    </Stack>
  )
}