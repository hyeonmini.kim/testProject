import PropTypes from 'prop-types';
import {Card, Stack, Typography} from '@mui/material';
import {alpha, useTheme} from "@mui/material/styles";
import {fileData} from "../../../file-thumbnail";
import Image from "../../../image";
import {RECIPE_CREATE_BASIC} from "../../../../constant/recipe/Create";
import {clickBorderNone} from "../../../../constant/common/Common";

// ----------------------------------------------------------------------

FileThumbnail.propTypes = {
  sx: PropTypes.object,
  imgSx: PropTypes.object,
  isRepresent: PropTypes.bool,
  imageView: PropTypes.bool,
  onDownload: PropTypes.func,
  file: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

export default function FileThumbnail({index, file, isRepresent = false, imgSx}) {
  const {preview = ''} = fileData(file);
  const theme = useTheme();
  const renderContent =
    <Image
      alt={`대표사진 ${index + 1}`}
      src={preview}
      sx={{
        width: 1,
        height: 1,
        flexShrink: 0,
        objectFit: 'cover',
        ...imgSx,
      }}
    />
  return (
    <Card tabIndex={0} sx={{borderRadius: '8px', border: '1px solid #e6e6e6', ...clickBorderNone}}>
      {renderContent}
      {isRepresent && <Stack
        spacing={2}
        direction="row"
        alignItems="center"
        justifyContent="center"
        sx={{
          backgroundColor: alpha('#000000', 0.7),
          width: 1,
          height: '22px',
          left: 0,
          bottom: 0,
          position: 'absolute',
        }}
      >
        <Typography sx={{color: '#FFFFFF', fontSize: '14px', fontWeight: '400', lineHeight: '20px'}}>
          {RECIPE_CREATE_BASIC.TEXT.REPRESENTATIVE}
        </Typography>
      </Stack>
      }
    </Card>
  );
}
