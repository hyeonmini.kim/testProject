import PropTypes from 'prop-types';
import {Grid, IconButton, Stack, Typography} from '@mui/material';
import {SvgMinusButtonIcon, SvgPlusButtonIcon} from "../../../../constant/icons/icons";
import {clickBorderNone} from "../../../../constant/common/Common";
import {ALT_STRING} from "../../../../constant/common/AltString";

// ----------------------------------------------------------------------

CenterIncrementerButton.propTypes = {
  sx: PropTypes.object,
  unit: PropTypes.string,
  onIncrease: PropTypes.func,
  onDecrease: PropTypes.func,
  quantity: PropTypes.number,
};

export default function CenterIncrementerButton({quantity, onIncrease, onDecrease, unit, sx}) {
  return (
    <Grid container
          direction="row"
          alignItems="center"
          sx={{
            borderRadius: '8px',
            border: '1px solid #E6E6E6',
            ...sx,
          }}
    >
      <Grid item xs={12}>
        <Stack
          direction={'row'}
          justifyContent={'center'}
          alignItems={'center'}
        >
          {/* 마이너스 버튼 */}
          <IconButton tabIndex={-1} disableFocusRipple onClick={onDecrease} sx={{
            p: 0,
            '&:hover': {
              backgroundColor: '#FFFFFF',
            },
          }}>
            <SvgMinusButtonIcon alt={ALT_STRING.RECIPE_CREATE.BTN_DECREASE} tabIndex={0} sx={{...clickBorderNone}}/>
          </IconButton>

          {/* 타이틀 */}
          <Typography align='center' sx={{
            width: '60px',
            fontSize: '16px',
            fontWeight: '500',
            lineHeight: '24px',
            color: '#222222',
            mx: '16px'
          }}>
            {quantity}{unit}
          </Typography>

          {/* 플러스 버튼 */}
          <IconButton tabIndex={-1} disableFocusRipple onClick={onIncrease} sx={{
            p: 0,
            '&:hover': {
              backgroundColor: '#FFFFFF',
            },
          }}>
            <SvgPlusButtonIcon alt={ALT_STRING.RECIPE_CREATE.BTN_INCREASE} tabIndex={0} sx={{...clickBorderNone}}/>
          </IconButton>
        </Stack>
      </Grid>
    </Grid>
  )
}
