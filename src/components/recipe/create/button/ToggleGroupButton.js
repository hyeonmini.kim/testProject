import PropTypes from "prop-types";
import {Grid, Stack, ToggleButton, Typography} from "@mui/material";
import {nvlString} from "../../../../utils/formatString";
import {clickBorderNone} from "../../../../constant/common/Common";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";

ToggleGroupButton.propTypes = {
  title: PropTypes.string,
  items: PropTypes.array,
  selected: PropTypes.string,
  showSubTitle: PropTypes.bool,
  onChange: PropTypes.func,
};

export default function ToggleGroupButton({title, items, selected, showSubTitle = false, onChange}) {
  return (
    <>
      <Typography sx={{fontSize: '14px', fontWeight: '700', lineHeight: '22px', color: '#888888'}}>
        {title}
      </Typography>
      <Grid container spacing={1}>
        {items.map((item, index) => (
          <Grid key={index} item xs={4}>
            <ToggleButton
              tabIndex={0}
              disableFocusRipple
              key={item?.value}
              value={item?.value}
              fullWidth={true}
              onChange={() =>
                onChange(item?.value)
              }
              onClick={(e) => handleEnterPress(e, onChange(item?.value))}
              sx={{
                px: 0,
                height: '48px',
                lineHeight: '26px',
                color: item?.value === selected ? 'primary.main' : '#666666',
                border: '1.4px solid',
                borderColor: item?.value === selected ? 'primary.main' : '#E6E6E6',
                backgroundColor: '#FFFFFF',
                '&:hover': {
                  backgroundColor: '#FFFFFF',
                },
                ...clickBorderNone,
              }}
            >
              <Stack>
                <Typography
                  sx={{fontSize: showSubTitle ? '14px' : '16px', fontWeight: item?.value === selected ? '700' : '400'}}>
                  {nvlString(item?.title)}
                </Typography>
                {item?.subTitle && (
                  <Typography sx={{fontSize: '12px', mt: '-2px', fontWeight: item?.value === selected ? '700' : '400'}}>
                    {nvlString(item?.subTitle)}
                  </Typography>
                )}
              </Stack>
            </ToggleButton>
          </Grid>
        ))}
      </Grid>
    </>
  )
}