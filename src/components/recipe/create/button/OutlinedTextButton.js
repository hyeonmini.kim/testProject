import PropTypes from "prop-types";
import {Button, Typography} from "@mui/material";
import {SvgResetIcon} from "../../../../constant/icons/icons";
import {clickBorderNone} from "../../../../constant/common/Common";

OutlinedTextButton.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
  icon: PropTypes.string,
};

export default function OutlinedTextButton({text, onClick, isClear}) {

  return (
    <Button
      tabIndex={0}
      disableFocusRipple
      variant="outlined"
      onClick={onClick}
      sx={{
        px: 0,
        width: '100%',
        height: '42px',
        borderRadius: '99px',
        borderColor: '#E6E6E6',
        '&:hover': {
          borderColor: '#E6E6E6',
        },
        ...clickBorderNone,
      }}
    >
      {isClear &&
        <SvgResetIcon/>
      }
      <Typography sx={{fontSize: '14px', fontWeight: '500', lineHeight: '22px', color: '#666666'}}>
        {text}
      </Typography>
    </Button>
  )
}