import PropTypes from "prop-types";
import {Button, Typography} from "@mui/material";
import {clickBorderNone} from "../../../../constant/common/Common";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";

ContainedTextButton.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
};

export default function ContainedTextButton({text, onClick}) {
  return (
    <Button
      tabIndex={0}
      variant="contained"
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
      sx={{
        px: '20px',
        height: '42px',
        borderRadius: '99px',
        backgroundColor: '#F5F5F5',
        '&:hover': {
          boxShadow: 'none',
          backgroundColor: '#F5F5F5'
        },
        ...clickBorderNone,
      }}
    >
      <Typography sx={{fontSize: '14px', fontWeight: '500', lineHeight: '22px', color: '#666666'}}>
        {text}
      </Typography>
    </Button>
  )
}