import PropTypes from "prop-types";
import {Box, Button, Container} from "@mui/material";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {sxFixedCenterMainLayout} from "../../../../layouts/main/MainLayout";
import {clickBorderNone} from "../../../../constant/common/Common";

BottomFixedButton.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
  isDisabled: PropTypes.bool,
  sx: PropTypes.object,
};

export default function BottomFixedButton({text, onClick, isDisabled = false, sx}) {
  return (
    <Box
      sx={{
        p: '20px',
        position: 'fixed',
        zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
        bgcolor: 'white',
        bottom: 0,
        left: 0,
        right: 0,
        ...sx,
      }}
    >
      <Container maxWidth={'xs'} disableGutters>
        <Button
          tabIndex={0}
          disableFocusRipple
          fullWidth
          variant="contained"
          onClick={onClick}
          disabled={isDisabled}
          sx={{
            height: '52px',
            fontSize: '16px',
            fontWeight: '700',
            lineHeight: '28px',
            backgroundColor: isDisabled ? '#E6E6E6' : 'primary.main',
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: isDisabled ? '#E6E6E6' : 'primary.main',
            },
            ...clickBorderNone,
          }}
        >
          {text}
        </Button>
      </Container>
    </Box>
  )
}