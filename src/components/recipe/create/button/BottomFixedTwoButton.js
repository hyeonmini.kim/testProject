import PropTypes from "prop-types";
import {Box, Button, Container, Grid} from "@mui/material";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {sxFixedCenterMainLayout} from "../../../../layouts/main/MainLayout";
import {clickBorderNone} from "../../../../constant/common/Common";

BottomFixedTwoButton.propTypes = {
  leftButtonProps: PropTypes.object,
  rightButtonProps: PropTypes.object,
  sx: PropTypes.object,
}
export default function BottomFixedTwoButton({leftButtonProps, rightButtonProps, sx}) {
  if (!leftButtonProps) return
  if (!rightButtonProps) return

  return (
    <Box
      sx={{
        p: '20px',
        position: 'fixed',
        zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
        bgcolor: 'white',
        bottom: 0,
        left: 0,
        right: 0,
        ...sx,
      }}
    >
      <Container maxWidth={'xs'} disableGutters>
        <Grid container spacing={'10px'}>
          <Grid item xs={6}>
            <Button
              tabIndex={1}
              disableFocusRipple
              fullWidth
              variant="outlined"
              onClick={leftButtonProps?.onClick}
              disabled={leftButtonProps?.isDisabled}
              sx={{
                px: 0,
                height: '52px',
                fontSize: '16px',
                fontWeight: '700',
                lineHeight: '28px',
                '&:hover': {
                  boxShadow: 'none',
                },
                ...clickBorderNone,
              }}
            >
              {leftButtonProps?.text}
            </Button>
          </Grid>
          <Grid item xs={6}>
            <Button
              tabIndex={1}
              disableFocusRipple
              fullWidth
              variant="contained"
              onClick={rightButtonProps?.onClick}
              disabled={rightButtonProps?.isDisabled}
              sx={{
                px: 0,
                height: '52px',
                fontSize: '16px',
                fontWeight: '700',
                lineHeight: '28px',
                backgroundColor: rightButtonProps?.isDisabled ? '#E6E6E6' : 'primary.main',
                '&:hover': {
                  boxShadow: 'none',
                  backgroundColor: rightButtonProps?.isDisabled ? '#E6E6E6' : 'primary.main',
                },
                ...clickBorderNone,
              }}
            >
              {rightButtonProps?.text}
            </Button>
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
}