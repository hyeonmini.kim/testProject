import PropTypes from 'prop-types';
import {Box, IconButton, Stack, Typography} from '@mui/material';
import TextMaxLine from "../../../text-max-line";
import {RECIPE_CREATE_MATERIAL} from "../../../../constant/recipe/Create";
import {ICON_COLOR, SvgCloseIcon, SvgMinusButtonIcon, SvgPlusButtonIcon} from "../../../../constant/icons/icons";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import MenuPopover from "../../../menu-popover";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../../../constant/common/Common";
import {ALT_STRING} from "../../../../constant/common/AltString";

// ----------------------------------------------------------------------
TitleIncrementerButton.propTypes = {
  onIncrease: PropTypes.func,
  onDecrease: PropTypes.func,
  onTitleClick: PropTypes.func,
  onQuantityClick: PropTypes.func,
  item: PropTypes.object,
  sx: PropTypes.object,
};

export default function TitleIncrementerButton({
                                                 mainRef,
                                                 onIncrease,
                                                 onDecrease,
                                                 onTitleClick,
                                                 onQuantityClick,
                                                 item,
                                                 isMain = false,
                                                 sx
                                               }) {

  const firstRef = useRef(null)
  const [customized, setCustomized] = useState(null);

  useEffect(() => {
    if (firstRef?.current) {
      handleOpenCustomized(firstRef?.current)
    }
  }, [firstRef])

  const handleOpenCustomized = (currentTarget) => {
    setCustomized(currentTarget);
  };

  const handleCloseCustomized = () => {
    setCustomized(null);
  };

  return (
    <Box
      direction="row"
      alignItems="center"
      sx={{
        position: 'relative',
        display: 'flex',
        flexDirection: 'row',
        borderRadius: '8px',
        border: '1px solid #E6E6E6',
        ...sx,
      }}
    >
      <Box sx={{flexGrow: 1}}>
        {isMain && <Representative/>}
        <TextMaxLine
          line={1}
          persistent
          sx={{
            position: 'relative',
            height: 1,
            top: '0%',
            fontSize: '16px',
            fontWeight: '400',
            lineHeight: '24px',
            color: '#222222',
            ml: '20px'
          }}
          onClick={onTitleClick}
          onKeyDown={(e) => handleEnterPress(e, onTitleClick)}
        >
          {item?.recipe_ingredient_name}
        </TextMaxLine>
      </Box>
      <Box>
        <Stack
          direction={'row'}
          justifyContent={'flex-end'}
          alignItems={'center'}
          sx={{mx: '20px'}}
        >
          {/* 마이너스 버튼 */}
          <IconButton tabIndex={-1} disableFocusRipple onClick={onDecrease} sx={{
            p: 0,
            '&:hover': {
              backgroundColor: '#FFFFFF',
            },
          }}>
            <SvgMinusButtonIcon alt={ALT_STRING.RECIPE_CREATE.BTN_DECREASE} tabIndex={0} sx={{...clickBorderNone}}/>
          </IconButton>

          {/* 타이틀 */}
          <Typography
            tabIndex={0}
            align='center'
            sx={{
              width: '64px',
              fontSize: '14px',
              fontWeight: '500',
              lineHeight: '24px',
              color: '#222222',
              mx: '10px',
              letterSpacing: '-0.02em'
            }}
            onClick={onQuantityClick}
            onKeyDown={(e) => handleEnterPress(e, onQuantityClick)}
          >
            {item?.recipe_ingredient_amount}{item?.recipe_ingredient_main_countunit}
          </Typography>
          {isMain && <Box ref={firstRef}/>}
          {isMain &&
            <IntroduceTooltip mainRef={mainRef} customized={customized} setCustomized={setCustomized}
                              handleCloseCustomized={handleCloseCustomized}/>}

          {/* 플러스 버튼 */}
          <IconButton tabIndex={-1} disableFocusRipple onClick={onIncrease} sx={{
            p: 0,
            '&:hover': {
              backgroundColor: '#FFFFFF',
            },
          }}>
            <SvgPlusButtonIcon alt={ALT_STRING.RECIPE_CREATE.BTN_INCREASE} tabIndex={0} sx={{...clickBorderNone}}/>
          </IconButton>
        </Stack>
      </Box>
    </Box>
  )
};

function IntroduceTooltip({mainRef, customized, setCustomized, handleCloseCustomized}) {
  return (
    <MenuPopover
      mainRef={mainRef}
      open={customized}
      setOpen={setCustomized}
      onClose={handleCloseCustomized}
      arrow='bottom-center-create'
      sx={{backgroundColor: 'primary.main', p: 0, borderRadius: '4px', height: '26px', width: '143px', zIndex: 9999,}}
      arrowBottomStyle={{backgroundColor: '#ECA548'}}
    >
      <Stack
        tabIndex={0}
        direction="row"
        onKeyDown={(e) => {
          e.preventDefault();
          handleEnterPress(e, handleCloseCustomized)
        }}
        sx={{alignItems: 'center', ...clickBorderNone}}
      >
        <SvgCloseIcon alt={ALT_STRING.COMMON.BTN_CLOSE} color={ICON_COLOR?.WHITE} onClick={handleCloseCustomized}
                      sx={{m: '0 6px', width: '12px', height: '12px'}}/>
        <Typography fontWeight="500" fontSize="12px"
                    sx={{lineHeight: '25px', color: 'white', whiteSpace: 'pre-wrap'}}>
          직접 입력하려면 click
        </Typography>
      </Stack>
    </MenuPopover>
  )
}

function Representative() {
  return (
    <>
      <Box
        style={{
          position: 'absolute',
          borderTopLeftRadius: '8px',
          borderBottom: '27px solid #FFFFFF',
          borderLeft: '32px solid rgba(0, 0, 0, 0.7)',
          left: '0%',
          top: '0%',
          width: '32.5px',
          height: '27px',
        }}
      />
      <Typography
        style={{
          position: 'absolute',
          left: '3px',
          top: '3px',
          color: '#FFFFFF',
          fontWeight: 400,
          fontSize: '9px',
          transform: 'rotate(-39deg)'
        }}
      >
        {RECIPE_CREATE_MATERIAL.TEXT.REPRESENTATIVE}
      </Typography>
    </>
  )
}