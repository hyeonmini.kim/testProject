import {useRecoilState} from "recoil";
import {createOrderDeleteImagesSelector, createRecipeStepsSelector} from "../../../../recoil/selector/recipe/createSelector";
import {isNative} from "../../../../utils/envUtils";
import {bytesToImageFile} from "../../../../utils/fileUtils";
import {callOpenNativeGalleryChannel} from "../../../../channels/photoChannel";
import {Draggable} from "react-beautiful-dnd";
import {Box, Grid, InputAdornment, Stack, Typography} from "@mui/material";
import {RECIPE_CREATE_STEP} from "../../../../constant/recipe/Create";
import SingleImage from "../image/SingleImage";
import {ICON_COLOR, SvgDeleteIcon, SvgHandleIcon} from "../../../../constant/icons/icons";
import imageCompression from "browser-image-compression";
import {IMAGE_COMPRESSION_OPTIONS} from "../../../../constant/common/Common";
import {useState} from "react";
import LoadingScreen from "../../../common/LoadingScreen";
import {ALT_STRING} from "../../../../constant/common/AltString";
import TextFieldWrapper from "../../../common/TextFieldWrapper";

export default function DragStep({dragKey, index}) {
  const [steps, setSteps] = useRecoilState(createRecipeStepsSelector)
  const [isCompress, setIsCompress] = useState(false)

  const handleSteps = (event, index) => {
    let text = event.target.value?.substring(0, 200)
    const newItem = {...steps[index]}
    const newArray = [...steps]
    newItem.recipe_order_desc = text
    newArray[index] = newItem
    setSteps(newArray)
  };

  const setFileWithImage = async (image, index) => {
    if (isNative()) {
      image = bytesToImageFile(image)
    } else {
      setIsCompress(true)
      const resizingBlob = await imageCompression(image, IMAGE_COMPRESSION_OPTIONS);
      image = new File([resizingBlob], image.name, {type: image.type});
      setIsCompress(false)
    }
    const newImage = Object.assign(image, {
      preview: URL.createObjectURL(image),
    })
    const newItem = {...steps[index]}
    const newArray = [...steps]
    newItem.image_file_path = newImage
    newArray[index] = newItem
    setSteps(newArray)
  }

  const handleDrop = (images, index) => {
    setFileWithImage(images[0], index)
  }

  const handleGalleryChannel = (index) => {
    document.activeElement.blur()
    callOpenNativeGalleryChannel(0, 1, (images) => setFileWithImage(images[0], index))
  }

  const [deleteOrderImages, setDeleteOrderImages] = useRecoilState(createOrderDeleteImagesSelector)
  const handleRemoveImage = (index) => {
    let newItem = {...steps[index]}
    const newArray = [...steps]
    if (typeof newItem?.image_file_path === "string") {
      setDeleteOrderImages([...deleteOrderImages, newItem])
      newItem = {
        ...steps[index],
        image_file_key: ''
      }
    }
    newArray[index] = {
      ...newItem,
      image_file_path: ''
    }
    setSteps(newArray)
  }

  const handleRemoveStep = (index) => {
    const newArray = [...steps]
    newArray.splice(index, 1)
    setSteps(newArray)
  }

  return (
    <>
      <Draggable key={dragKey + index} draggableId={dragKey + index} index={index}>
        {(provided) => (
          <Grid
            item xs={12}
            {...provided.draggableProps}
            ref={provided.innerRef}
          >
            <Box
              {...provided.dragHandleProps}
              tabIndex={-1}
              rowGap={'5px'}
              display="grid"
            >
              {/* STEP 타이틀 버튼 */}
              <Stack tabIndex={0} direction={'row'} columnGap={'10px'} sx={{display: 'flex', alignItems: 'center'}}>
                <SvgHandleIcon alt={ALT_STRING.RECIPE_CREATE.ICON_CHANGE_ORDER}/>
                <Typography sx={{fontSize: '14px', fontWeight: '700', lineHeight: '22px', color: '#222222'}}>
                  {RECIPE_CREATE_STEP.STEP(index + 1)}
                </Typography>
                {steps?.length > 2 &&
                  <SvgDeleteIcon alt={ALT_STRING.RECIPE_CREATE.BTN_DEL} color={ICON_COLOR.GREY} onClick={() => handleRemoveStep(index)}/>
                }
              </Stack>

              {/* STEP 내용 */}
              <TextFieldWrapper
                autoComplete='off'
                placeholder={RECIPE_CREATE_STEP.TEXT.STEP_PLACEHOLDER}
                value={steps[index].recipe_order_desc}
                multiline
                minRows={3}
                onChange={(event) => handleSteps(event, index)}
                sx={{borderColor: 'red'}}
                inputProps={{title: ALT_STRING.RECIPE_CREATE.INPUT_RECIPE_DETAIL}}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end" sx={{width: '120px'}}>
                      <Box sx={{position: 'absolute', right: '16px', top: '16px',}}>
                        {isNative()
                          ? <SingleImage imageIndex={index}
                                         onClick={() => handleGalleryChannel(index)}
                                         onDelete={() => handleRemoveImage(index)}/>
                          : <SingleImage imageIndex={index}
                                         onDrop={(images) => handleDrop(images, index)}
                                         onDelete={() => handleRemoveImage(index)}/>
                        }
                      </Box>
                    </InputAdornment>
                  ),
                  sx: {
                    '& textarea': {
                      fontSize: '16px', fontWeight: '400', lineHeight: '24px', color: '#222222'
                    },
                    '& textarea::placeholder': {
                      fontSize: '16px', fontWeight: '500', lineHeight: '24px', color: '#CCCCCC'
                    }
                  }
                }}
              />
            </Box>
          </Grid>
        )}
      </Draggable>
      {isCompress && <LoadingScreen/>}
    </>
  )
}
