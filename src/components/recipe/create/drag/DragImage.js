import PropTypes from 'prop-types';
import {Box, Stack} from '@mui/material';
import {Draggable} from "react-beautiful-dnd";
import FileThumbnail from "../image/FileThumbnail";
import {fileData} from "../../../file-thumbnail";
import {SvgDeleteIcon} from "../../../../constant/icons/icons";
import {clickBorderNone} from "../../../../constant/common/Common";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../../constant/common/AltString";
// ----------------------------------------------------------------------

DragImage.propTypes = {
  sx: PropTypes.object,
  file: PropTypes.any,
  onRemove: PropTypes.func,
  index: PropTypes.number
};

export default function DragImage({file, onRemove, sx, index}) {
  if (!file) {
    return null;
  }
  const {key} = fileData(file);
  return (
    <Draggable key={key + index} draggableId={key + index} index={index}>
      {(provided) => (
        <Box
          {...provided.draggableProps}
          ref={provided.innerRef}
          sx={{
            position: 'relative',
            ...sx,
          }}
          onKeyDown={(e) => handleEnterPress(e, onRemove)}
        >
          <Stack {...provided.dragHandleProps} tabIndex={-1}>
            <FileThumbnail
              index={index}
              isRepresent={index === 0 ? true : false}
              imageView
              file={file}
              imgSx={{width: '80px', height: '80px'}}
            />
          </Stack>

          {onRemove
            && <SvgDeleteIcon
              alt={ALT_STRING.RECIPE_CREATE.BTN_DEL_IMAGE}
              onClick={() => onRemove(index)}
              sx={{position: 'absolute', top: 2, right: 2}}
            />}
        </Box>
      )}
    </Draggable>
  )
}

