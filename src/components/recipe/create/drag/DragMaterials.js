import {Box, Stack} from "@mui/material";
import {Draggable} from "react-beautiful-dnd";
import {ICON_COLOR, SvgDeleteIcon, SvgHandleIcon} from "../../../../constant/icons/icons";
import TextMaxLine from "../../../text-max-line";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../../constant/common/AltString";

export default function DragMaterials({open, dragKey, item, index, onRemove}) {
  if (!item) return

  return (
    <Draggable key={dragKey + index} draggableId={dragKey + index} index={index}>
      {(provided) => (
        <Box
          {...provided.draggableProps}
          ref={provided.innerRef}
        >
          <Stack {...provided.dragHandleProps}
                 direction={'row'}
                 spacing={'10px'}
                 sx={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}
                 onKeyDown={(e) => handleEnterPress(e, () => onRemove(index))}
          >
            <SvgHandleIcon alt={ALT_STRING.RECIPE_CREATE.ICON_CHANGE_ORDER}/>
            <Box sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              height: '56px',
              width: '100%',
              borderRadius: '8px',
              border: '1px solid #E6E6E6',
            }}>
              <TextMaxLine open={open} line={1} sx={{
                mx: '20px',
                align: 'center',
                fontSize: '16px',
                fontWeight: '500',
                lineHeight: '24px',
                color: '#222222'
              }}>
                {item?.recipe_ingredient_name} {item?.recipe_ingredient_amount}{item?.recipe_ingredient_main_countunit}
              </TextMaxLine>
            </Box>
            <SvgDeleteIcon alt={ALT_STRING.RECIPE_CREATE.BTN_DEL} color={ICON_COLOR.GREY} onClick={() => onRemove(index)}/>
          </Stack>
        </Box>
      )}
    </Draggable>
  )
}