import Carousel from 'react-slick';

export {default as CarouselDotsDetail} from './CarouselDotsDetail';

export default Carousel;
