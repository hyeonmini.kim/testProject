// @mui
import {styled} from '@mui/material/styles';
import {Box, Stack} from '@mui/material';

// ----------------------------------------------------------------------

const StyledRoot = styled(Box, {
  shouldForwardProp: (prop) => prop !== 'rounded',
})(({rounded, theme}) => ({
  zIndex: 9,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  color: theme.palette.primary.main,
  '& li': {
    width: 16,
    height: 16,
    opacity: 0.32,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
    '&.slick-active': {
      opacity: 1,
      '& span': {
        backgroundColor: '#888888',
      },
      ...(rounded && {
        '& span': {
          width: 16,
          borderRadius: 6,
        },
      }),
    },
  },
}));

const StyledDot = styled('span')(({theme}) => ({
  width: 8,
  height: 8,
  borderRadius: '50%',
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.short,
  }),
  border: '1px solid #888888',
}));

// ----------------------------------------------------------------------

export default function NutrientsCarouselDotsDetail(props) {
  const rounded = props?.rounded || false;

  const sx = props?.sx;

  return {
    appendDots: (dots) => (
      <>
        <StyledRoot component="ul" rounded={rounded} sx={sx} {...props}>
          {dots}
        </StyledRoot>
      </>
    ),
    customPaging: () => (
      <Stack
        tabIndex={8}
        component="button"
        alignItems="center"
        justifyContent="center"
        sx={{width: 1, height: 1, cursor: 'pointer', backgroundColor: 'transparent', border: 'none'}}
      >
        <StyledDot/>
      </Stack>
    ),
  };
}
