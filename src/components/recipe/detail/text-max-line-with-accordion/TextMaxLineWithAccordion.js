import PropTypes from 'prop-types';
import {forwardRef, useEffect, useRef, useState} from 'react';
import {Accordion, AccordionSummary, IconButton, Link, Typography,} from '@mui/material';
import useTypography from '../../../text-max-line/useTypography';
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import {setHackleTrack} from "../../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../../constant/common/Hackle";
import {clickBorderNone} from "../../../../constant/common/Common";

// ----------------------------------------------------------------------

const TextMaxLineWithAccordion = forwardRef(
  ({asLink, variant = 'b1_16_r', line = 2, persistent = false, children, sx, onClick, ...other}, ref) => {
    const {lineHeight} = useTypography(variant);

    const styles_close = {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      display: '-webkit-box',
      WebkitLineClamp: line,
      WebkitBoxOrient: 'vertical',
      ...(persistent && {
        height: lineHeight * line,
      }),
      ...sx,
    };

    const styles_open = {
      // overflow: 'hidden',
      // textOverflow: 'ellipsis',
      // display: '-webkit-box',
      // WebkitLineClamp: line,
      // WebkitBoxOrient: 'vertical',
      // ...(persistent && {
      //   height: lineHeight * line,
      // }),
      ...sx,
    };

    const [styles, setStyles] = useState(styles_close);
    const [overflowed, setOverflowed] = useState(false);

    const thisRef = useRef(); // ref 변수 선언

    useEffect(() => {
      const {scrollHeight, clientHeight} = thisRef.current;
      setOverflowed(scrollHeight > clientHeight);
    }, [])

    const handleChangeControlled = () => (event, isExpanded) => {
      if (isExpanded && onClick) {
        onClick()
      }

      setStyles(isExpanded ? styles_open : styles_close)
    };

    if (asLink) {
      return (
        <Link tabIndex={-1} color="inherit" ref={ref} variant={variant} sx={{...styles_close}} {...other}>
          {children}
        </Link>
      );
    }

    return (
      <>
        {overflowed ? (
          <Accordion
            disableGutters
            onChange={handleChangeControlled()}
            sx={{
              '&.MuiAccordion-root:before': {display: 'none'},
              '&.MuiAccordion-root': {backgroundColor: '#FAFAFA', boxShadow: 'none'},
            }}
          >
            <AccordionSummary
              tabIndex={-1}
              expandIcon={
                <IconButton
                  tabIndex={4}
                  sx={{
                    m: '-8px',
                    backgroundColor: "#FAFAFA",
                    '&:hover': {
                      boxShadow: 'none',
                      backgroundColor: '#FAFAFA',
                    },
                    ...clickBorderNone
                  }}
                >
                  <SvgCommonIcons type={ICON_TYPE.IC_EXPAND_MORE}/>
                </IconButton>}
              sx={{
                '&.MuiAccordionSummary-root': {flexDirection: 'column', padding: 0},
                '& .MuiAccordionSummary-content': {width: 1}
              }}
            >
              <Typography ref={thisRef} variant={variant} sx={{...styles}} {...other}>
                {children}
              </Typography>
            </AccordionSummary>
          </Accordion>
        ) : (
          <Typography ref={thisRef} variant={variant} sx={{...styles, mt: '12px'}} {...other}>
            {children}
          </Typography>
        )}
      </>
    );
  }
);

TextMaxLineWithAccordion.propTypes = {
  sx: PropTypes.object,
  asLink: PropTypes.bool,
  line: PropTypes.number,
  persistent: PropTypes.bool,
  children: PropTypes.node,
  variant: PropTypes.oneOf([
    'body1',
    'body2',
    'button',
    'caption',
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'inherit',
    'overline',
    'subtitle1',
    'subtitle2',
  ]),
};

export default TextMaxLineWithAccordion;
