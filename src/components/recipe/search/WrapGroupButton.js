import PropTypes from "prop-types";
import {Box, Button, Stack, Typography} from "@mui/material";
import React from "react";
import ScrollContainer from "react-indiana-drag-scroll";
import {clickBorderNone} from "../../../constant/common/Common";
import {nvlString} from "../../../utils/formatString";

WrapGroupButton.propTypes = {
  items: PropTypes.array,
  selected: PropTypes.string,
  onClick: PropTypes.func,
};

export default function WrapGroupButton({items, selected, onClick}) {
  return (
    <Box sx={{mx: '-20px'}}>
      <ScrollContainer className="scroll-container">
        <Stack direction="row" spacing={'10px'} sx={{display: 'flex', mb: '10px'}}>
          <Box sx={{pl: '10px'}}/>
          {items?.map((item, index) => (
            <Box key={index} sx={{position: 'relative'}}>
              <Button
                tabIndex={0}
                disableFocusRipple
                variant={'contained'}
                value={item?.value}
                fullWidth={true}
                onClick={() => onClick(item?.value)}
                sx={{
                  px: 0,
                  height: '50px',
                  lineHeight: '26px',
                  color: item?.value === selected ? '#FFFFFF' : '#666666',
                  border: 1,
                  borderColor: item?.value === selected ? 'primary.main' : '#E6E6E6',
                  borderRadius: '100px',
                  backgroundColor: item?.value === selected ? 'primary.main' : '#FFFFFF',
                  '&:hover': {
                    boxShadow: 'none',
                    backgroundColor: item?.value === selected ? 'primary.main' : '#FFFFFF',
                  },
                  ...clickBorderNone
                }}
              >
                <Stack>
                  <Typography
                    noWrap
                    sx={{mx: '24px', fontSize: '14px', letterSpacing: '-0.02em', fontWeight: item?.value === selected ? 700 : 500}}>
                    {nvlString(item?.value)}
                  </Typography>
                  {item?.subTitle && (
                    <Typography noWrap sx={{fontSize: '12px', mt: '-2px', fontWeight: item?.value === selected ? 700 : 400}}>
                      {nvlString(item?.subTitle)}
                    </Typography>
                  )}
                </Stack>
              </Button>
            </Box>
          ))}
          <Box sx={{pr: '10px'}}/>
        </Stack>
      </ScrollContainer>
    </Box>
  )
}




