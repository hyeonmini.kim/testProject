import {Box, Skeleton, Stack, Typography} from "@mui/material";
import Image from "../../image";
import TextMaxLine from "../../text-max-line";
import PropTypes from "prop-types";
import {getNumberTenThousand} from "../../../utils/formatNumber";
import * as React from "react";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {COUNT_TITLE} from "../../../constant/recipe/detail/DetailConstants";
import {ALT_STRING} from "../../../constant/common/AltString";

SearchRecipeItem.propTypes = {
  item: PropTypes.object,
};

export default function SearchRecipeItem({item, isLazy = false, onClick}) {
  const {
    recipe_name,
    recipe_desc,
    image_file_path,
    recipe_key,
    recipe_view_cnt,
    recipe_scrap_cnt,
    recipe_writer_info,
  } = item;

  return (
    <Stack
      tabIndex={0}
      direction={'row'}
      spacing={'12px'}
      sx={{display: 'flex', my: '11px'}}
      onClick={() => onClick(recipe_writer_info?.recipe_writer_nickname, recipe_name, recipe_key)}
      onKeyDown={(e) => handleEnterPress(e, () => onClick(recipe_writer_info?.recipe_writer_nickname, recipe_name, recipe_key))}
    >
      <Box sx={{m: 0, p: 0, position: 'relative'}}>
        {
          image_file_path
            ? <Image alt={ALT_STRING.COMMON.RECIPE(recipe_name)} isLazy={isLazy} src={image_file_path} sx={{borderRadius: '8px', width: '100px', height: '100px'}}/>
            : <Skeleton variant="rounded" sx={{borderRadius: '8px', width: '100px', height: '100px'}}/>
        }
      </Box>
      <Box sx={{m: 0, p: 0, display: 'flex', flexDirection: 'column', justifyContent: 'space-between', flexGrow: 1}}>
        <TextMaxLine open={true} line={1} sx={{fontSize: '16px', fontWeight: 500, lineHeight: '24px', mb: '2px'}}>
          {recipe_name}
        </TextMaxLine>
        <TextMaxLine open={true} line={2} sx={{fontSize: '14px', fontWeight: 400, lineHeight: '20px'}}>
          {recipe_desc}
        </TextMaxLine>
        <Box sx={{flexGrow: 1}}/>
        <Stack alignItems={'center'} direction={'row'}>
          <Typography fontWeight="400" fontSize="12px" sx={{lineHeight: '12px', mr: '16px', color: '#888888'}}>
            {`${COUNT_TITLE.VIEWS} ${getNumberTenThousand(recipe_view_cnt)}`}
          </Typography>
          <Typography fontWeight="400" fontSize="12px" sx={{lineHeight: '12px', mr: '16px', color: '#888888'}}>
            {`${COUNT_TITLE.SCRAP} ${getNumberTenThousand(recipe_scrap_cnt)}`}
          </Typography>
        </Stack>
      </Box>
    </Stack>
  );
}