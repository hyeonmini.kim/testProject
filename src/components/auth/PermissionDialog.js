import * as React from 'react';
import {Box, Button, DialogContent, Typography} from "@mui/material";
import {CallNativePermission} from "../../channels/permissionChannel";
import {PERMISSION_DONOTS} from "../../constant/sign-up/Permission";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {MainDialogLayout} from "../../layouts/main/MainLayout";
import {Z_INDEX} from "../../constant/common/ZIndex";
import {useEffect} from "react";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";

export default function PermissionDialog({open, setOpen, permissionJson}) {
  const permissionList = [
    {image: ICON_TYPE.CAMERA, title: PERMISSION_DONOTS.CAMERA, desc: PERMISSION_DONOTS.CAMERA_DESC},
    {image: ICON_TYPE.STORAGE, title: PERMISSION_DONOTS.STORAGE, desc: PERMISSION_DONOTS.STORAGE_DESC},
    {image: ICON_TYPE.CONTACT, title: PERMISSION_DONOTS.CONTACT, desc: PERMISSION_DONOTS.CONTACT_DESC},
  ]

  const handleClick = (e) => {
    CallNativePermission(permissionJson)
    setOpen(false)
  };

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_PERMISSION)
  }, [])

  return (
    <MainDialogLayout
      onClose={handleClick}
      fullScreen
      open={open}
      PaperProps={{style: {margin: 0, padding: '20px'}}}
      sx={{zIndex: Z_INDEX.DIALOG_ON_GNB}}
    >
      <DialogContent sx={{p: 0}}>
        <Box sx={{m: '68px 0 47px 0', p: 0}}>
          <Box sx={{mb: '13px'}}>
            <Typography variant={'h1_24_b'} sx={{color: '#000000'}}>
              {PERMISSION_DONOTS.TITLE}
            </Typography>
          </Box>
          <Box>
            <Typography variant={'b1_16_r'} sx={{color: '#888888'}}>
              {PERMISSION_DONOTS.SUBTITLE}
            </Typography>
          </Box>
        </Box>
        <Box sx={{mb: '50px'}}>
          <Box sx={{mb: '24px'}}>
            <Typography variant={'b_18_m'} sx={{color: '#222222'}}>
              {PERMISSION_DONOTS.PERMISSION}
            </Typography>
          </Box>
          {permissionList.map((obj, index) => (
            <Box key={index} sx={{display: 'flex', mb: '18px', alignItems: 'flex-start'}}>
              <SvgCommonIcons type={obj.image}/>
              <Box sx={{ml: '12px'}}>
                <Box>
                  <Typography variant={'b1_16_r_1l'} sx={{color: '#222222'}}>
                    {obj.title}
                  </Typography>
                </Box>
                <Box>
                  <Typography variant={'b2_14_r_1l'} sx={{color: '#888888'}}>
                    {obj.desc}
                  </Typography>
                </Box>
              </Box>
            </Box>
          ))}
        </Box>
        <Box>
          <Typography variant={'b2_14_r'} sx={{color: '#888888'}}>
            {PERMISSION_DONOTS.DESC}
          </Typography>
        </Box>
      </DialogContent>
      <Box
        sx={{
          px: '20px',
          py: '20px',
          width: '100%',
          maxWidth: '440px',
          position: 'fixed',
          bgcolor: 'white',
          zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
          bottom: 0,
          left: 0,
        }}
      >
        <Button
          variant="contained"
          fullWidth
          onClick={handleClick}
          sx={{
            height: '52px',
            fontStyle: 'normal',
            fontWeight: 700,
            fontSize: '16px',
            lineHeight: '28px',
            color: '#FFFFFF',
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: 'primary.main',
            },
          }}
        >
          {PERMISSION_DONOTS.CONFIRM}
        </Button>
      </Box>
    </MainDialogLayout>
  );
}