import * as React from "react";
import PropTypes from "prop-types";

Timer.propTypes = {
  time: PropTypes.number.isRequired,
  setTimeByMs: PropTypes.func.isRequired,
}

export default function Timer({time, setTimeByMs}) {
  React.useEffect(() => {
    const count = setInterval(() => {
      if ((time / 1000 % 60) > 0) {
        setTimeByMs(time - 1000);
      }
      if ((time / 1000 % 60) === 0) {
        if ((time / 1000 / 60) === 0) {
          clearInterval(count);
        } else {
          setTimeByMs(time - 1000);
        }
      }
    }, 1000);
    return () => clearInterval(count);
  }, [time]);

  return (
    <>
      {`0${Math.floor(time / 1000 / 60)}`}
      :
      {(time / 1000 % 60) < 10 ? `0${time / 1000 % 60}` : time / 1000 % 60}
    </>
  );
}