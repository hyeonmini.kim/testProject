import {Link, Skeleton} from "@mui/material";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {Pagination} from "swiper";
import {Swiper, SwiperSlide} from "swiper/react";
import React from "react";
import NextLink from "next/link";
import Lottie from "react-lottie";
import {isJsonFileType} from "../../../utils/fileUtils";
import Image from "../../image";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {useRouter} from "next/router";

export default function MomStarBanner({items, onClick}) {
  return (
    <Swiper
      slidesPerView={1.03}
      modules={[Pagination]}
      spaceBetween={15}
      style={{
        marginLeft: '-20px',
        paddingLeft: '20px',
        marginRight: '-20px',
        paddingRight: '20px',
        marginBottom: '30px',
      }}
    >
      {
        (items?.length ? items.slice(0, 5) : [...Array(5)]).map((item, index) =>
          item ? (
            <SwiperSlide key={index}>
              <MomstarBannerItem url={item?.banner_url} userKey={item?.banner_link} onClick={onClick}/>
            </SwiperSlide>
          ) : (
            <SwiperSlide key={index}>
              <Skeleton variant="rectangular" width={'100%'} height={'170px'} sx={{borderRadius: 2}}/>
            </SwiperSlide>
          )
        )
      }
    </Swiper>
  );
}

function MomstarBannerItem({url, userKey, onClick}) {
  const isLottie = isJsonFileType(url)
  const router = useRouter()
  return (
    <NextLink
      tabIndex={-1}
      href={PATH_DONOTS_PAGE.MY.OTHERS(userKey)}
      passHref
    >
      <Link tabIndex={-1} style={{textDecoration: 'none', 'WebkitTapHighlightColor': 'transparent'}}>
        {
          isLottie && url ? (
            <Lottie
              tabIndex={9}
              onClick={onClick}
              onKeyDown={(e) => handleEnterPress(e, () => router?.push(PATH_DONOTS_PAGE.MY.OTHERS(userKey)))}
              options={{
                path: url,
              }}
              play
              style={{zIndex: 2, position: 'sticky'}}
            />
          ) : (
            <Image
              tabIndex={9}
              isLazy={false}
              src={url}
              onClick={onClick}
              onKeyDown={(e) => handleEnterPress(e, () => router?.push(PATH_DONOTS_PAGE.MY.OTHERS(userKey)))}
            />
          )
        }
      </Link>
    </NextLink>
  )
}