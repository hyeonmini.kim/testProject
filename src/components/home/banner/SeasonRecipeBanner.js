import Image from "../../image";
import usePreRecipeCreate from "../../../hooks/usePreRecipeCreate";
import {isJsonFileType} from "../../../utils/fileUtils";
import Lottie from "react-lottie";
import React from "react";
import {Box, Skeleton} from "@mui/material";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";

export default function SeasonRecipeBanner({alt, url, onClick, sx}) {
  const {handleRecipe} = usePreRecipeCreate()
  if (!url) {
    return (
      <Skeleton variant="rectangular" width={'100%'} height={'170px'} sx={{borderRadius: 2, ...sx}}/>
    )
  }
  const isLottie = isJsonFileType(url)

  const handleClick = () => {
    if (onClick) {
      onClick()
    }
    handleRecipe()
  }

  return (
    <Box tabIndex={12} onClick={handleClick} onKeyDown={(e) => handleEnterPress(e, handleClick)} sx={{...sx}}>
      {
        isLottie ? (
          <Lottie
            options={{
              path: url,
            }}
            play
            style={{zIndex: 2, position: 'sticky'}}
          />
        ) : (
          <Image alt={alt} isLazy={false} src={url}/>
        )
      }
    </Box>
  )
}
