import {Swiper, SwiperSlide} from "swiper/react";
import React, {useEffect, useState} from "react";
import {Container, Skeleton} from "@mui/material";
import Lottie from "react-lottie";
import Image from "../../image";
import {isJsonFileType} from "../../../utils/fileUtils";
import IFrameDialog from "../../common/IFrameDialog";
import {Pagination} from "swiper";
import {useRecoilState} from "recoil";
import {homeShowInfoBannerSelector} from "../../../recoil/selector/home/homeSelector";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";
import {useVariation} from "@hackler/react-sdk";
import {LAB_TYPE} from "../../../constant/lab/LabCommon";
import {useRouter} from "next/router";
import {PATH_DONOTS_PAGE, PATH_DONOTS_STATIC_PAGE} from "../../../routes/paths";
import {getEnv} from "../../../utils/envUtils";

export default function InfoBanner({tabIndex, items, guest, onClick, sx}) {
  const [showInfoBanner, setShowInfoBanner] = useRecoilState(homeShowInfoBannerSelector)
  const [banners, setBanners] = useState([])

  const setBannerItem = (item, items) => {
    if (items) {
      setBanners([item, ...items])
    } else {
      setBanners([item])
    }
  }

  let variation
  if (!guest) {
    variation = useVariation(6)
  }

  const env = getEnv()
  useEffect(() => {
    if (guest) {
      setBannerItem({
        banner_link: PATH_DONOTS_PAGE.LAB.GUEST,
        banner_subject: '영양제',
        banner_type: LAB_TYPE.GUEST,
        banner_url: PATH_DONOTS_STATIC_PAGE.ICONS('/images/banner/lab1/banner_b.png', env),
      }, items)
    } else {
      if (variation === 'B') {
        setBannerItem({
          banner_link: PATH_DONOTS_PAGE.LAB.NUTRITION,
          banner_subject: '영양제',
          banner_type: LAB_TYPE.NUTRITION,
          banner_url: PATH_DONOTS_STATIC_PAGE.ICONS('/images/banner/lab1/banner_b.png', env),
        }, items)
      } else if (variation === 'C') {
        setBannerItem({
          banner_link: PATH_DONOTS_PAGE.LAB.BABY_FOOD,
          banner_subject: '시판 이유식',
          banner_type: LAB_TYPE.BABY_FOOD,
          banner_url: PATH_DONOTS_STATIC_PAGE.ICONS('/images/banner/lab1/banner_c.png', env),
        }, items)
      } else {
        setBannerItem({
          banner_link: PATH_DONOTS_PAGE.LAB.ALLERGY(env),
          banner_subject: '알레르기 유발 재료',
          banner_type: LAB_TYPE.ALERGY,
          banner_url: PATH_DONOTS_STATIC_PAGE.ICONS('/images/banner/lab1/banner_a.png', env),
        }, items)
      }
    }
  }, [items])

  return (
    <Container disableGutters sx={{...sx}}>
      {
        banners?.length === 1
          ? <InfoBannerItem tabIndex={tabIndex} item={banners[0]} onClick={onClick}/>
          : <SwiperBanner tabIndex={tabIndex} items={banners} onClick={onClick}/>
      }
      {showInfoBanner &&
        <IFrameDialog
          open={!!showInfoBanner}
          url={showInfoBanner?.banner_link}
          onBack={() => setShowInfoBanner(false)}
        />
      }
    </Container>
  )
}

function SwiperBanner({tabIndex, items, onClick}) {
  return (
    <Swiper
      slidesPerView={1.03}
      modules={[Pagination]}
      spaceBetween={15}
      style={{
        marginLeft: '-20px',
        paddingLeft: '20px',
        marginRight: '-20px',
        paddingRight: '20px',
      }}
    >
      {
        (items?.length ? items.slice(0, 5) : [...Array(5)]).map((item, index) =>
          item ? (
            <SwiperSlide key={index}>
              <InfoBannerItem tabIndex={tabIndex} index={index} item={item} onClick={onClick}/>
            </SwiperSlide>
          ) : (
            <SwiperSlide key={index}>
              <Skeleton variant="rectangular" width={'100%'} height={'170px'} sx={{borderRadius: 2}}/>
            </SwiperSlide>
          )
        )
      }
    </Swiper>
  )
}

function InfoBannerItem({tabIndex, index = 0, item, onClick}) {
  const [showInfoBanner, setShowInfoBanner] = useRecoilState(homeShowInfoBannerSelector)
  const isLottie = isJsonFileType()
  const router = useRouter();

  const handleClick = () => {
    if (item?.banner_type === LAB_TYPE.GUEST) {
      router.push(item?.banner_link)
      return
    }

    if (onClick) {
      onClick(index, item?.banner_type, item?.banner_subject)
    }
    if (item?.banner_type === LAB_TYPE.NUTRITION || item?.banner_type === LAB_TYPE.BABY_FOOD) {
      router.push(item?.banner_link)
    } else {
      setShowInfoBanner(item)
    }
  }

  function isContentsBanner(subject) {
    switch (subject) {
      case '영양제':
      case '시판 이유식':
      case '알레르기 유발 재료':
        return true
      default:
        return false
    }
  }

  return (
    <>
      {
        isLottie && item?.banner_url ? (
          <Lottie
            tabIndex={tabIndex}
            options={{
              path: item?.banner_url,
            }}
            play
            style={{zIndex: 2, position: 'sticky'}}
            onClick={handleClick}
            onKeyDown={(e) => handleEnterPress(e, handleClick)}
          />
        ) : item?.banner_url ? (
          <Image
            alt={isContentsBanner(item?.banner_subject)
              ? ALT_STRING.HOME.CONTENT(item?.banner_subject) : ALT_STRING.HOME.EXPERT(item?.banner_subject)}
            tabIndex={tabIndex}
            isLazy={false}
            src={item?.banner_url}
            sx={{zIndex: 2,}}
            onClick={handleClick}
            onKeyDown={(e) => handleEnterPress(e, handleClick)}
          />
        ) : (
          <Skeleton variant="rectangular" width={'100%'} height={'170px'} sx={{borderRadius: 2}}/>
        )
      }
    </>
  )
}