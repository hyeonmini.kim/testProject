import {Avatar, Stack, Typography} from "@mui/material";
import Image from "../../image";

export default function CloseDynamic({title, onClickBell}) {

  return (

    <Stack direction="row" alignItems="center" spacing={2} sx={{justifyContent: 'space-between'}}>

      {/* 프로필 사진 */}
      <Avatar
        alt="profile"
        src="https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_1.jpg"
        sx={{
          width: 26,
          height: 26,
        }}
      />
      {/* 문구 */}
      <Typography sx={{fontSize: '14px', fontWeight: 400, lineHeight: '20px'}}>
        {title}
      </Typography>
      <Image alt={''} src="/assets/icons/home/dynamicIsland/bell.svg" sx={{width: '18px', height: '22px'}} onClick={onClickBell}/>
    </Stack>
  )
}