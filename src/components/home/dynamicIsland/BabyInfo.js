import {Avatar, Box, Skeleton, Stack, Typography} from "@mui/material";
import * as React from "react";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {HOME_PERSONAL_COMMON} from "../../../constant/home/Personalization";
import {SvgInformationIcon} from "../../../constant/icons/icons";
import MenuPopover from "../../menu-popover";
import {DYNAMIC_ISLAND} from "../../../constant/home/DynamicIsland";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {useRecoilState} from "recoil";
import {isOpenTooltipSelector} from "../../../recoil/selector/home/homeSelector";
import {clickBorderNone} from "../../../constant/common/Common";
import {ALT_STRING} from "../../../constant/common/AltString";

export default function BabyInfoSection({nickName, info, avatar, mainRef}) {
  const babyProfile = HOME_PERSONAL_COMMON.BABY.INFO(nickName, info?.profile?.day, info?.profile?.month)

  return (
    <Stack spacing={'24px'}>

      {/* 아바타 & 프로파일 */}
      <Stack direction="row" spacing={'10px'}>
        <BabyAvatarSection avatar={avatar}/>
        <BabyProfileSection babyProfile={babyProfile}/>
      </Stack>

      {/* 백분위수 메시지 */}
      <BabyTitleSection
        babyTitle={info?.profile?.statement_evaluation}
        babyMonth={info?.profile?.month}
        mainRef={mainRef}
      />
    </Stack>
  )
}

function BabyAvatarSection({avatar}) {
  return (
    avatar
      ? <BabyAvatar avatar={avatar}/>
      : <SvgCommonIcons alt={ALT_STRING.HOME.PROFILE} type={ICON_TYPE.PROFILE_NO_IMAGE_80PX} sx={{width: '36px', height: '36px'}}/>
  )
}

function BabyAvatar({avatar}) {
  return (
    <Avatar
      alt={ALT_STRING.HOME.PROFILE}
      src={avatar}
      sx={{
        width: 36,
        height: 36,
        alignItems: 'center'
      }}
    />
  )
}

function BabyProfileSection({babyProfile}) {
  return (
    babyProfile
      ? <BabyProfile babyProfile={babyProfile}/>
      : <BabyProfileSkeleton/>

  )
}

function BabyProfile({babyProfile}) {
  return (
    <Typography
      align={'left'}
      sx={{
        fontSize: '14px',
        fontWeight: 400,
        lineHeight: '18px',
        color: '#666666',
        whiteSpace: 'pre-wrap',
      }}>
      {babyProfile}
    </Typography>
  )
}

function BabyProfileSkeleton() {
  return (
    <Stack>
      <Skeleton variant="rectangular" sx={{width: '50px', height: '14px', my: '2px', borderRadius: '5px'}}/>
      <Skeleton variant="rectangular" sx={{width: '100px', height: '14px', my: '2px', borderRadius: '5px'}}/>
    </Stack>
  )
}

function BabyTitleSection({babyTitle, babyMonth, mainRef}) {
  return (
    babyTitle
      ? <BabyTitle babyTitle={babyTitle} babyMonth={babyMonth} mainRef={mainRef}/>
      : <BabyTitleSkeleton/>
  )
}

function BabyTitle({babyTitle, babyMonth, mainRef}) {
  return (
    <Box sx={{position: 'relative', display: 'flex'}}>
      <Typography
        align={'left'}
        sx={{position: 'relative', width: 0.95, fontSize: '24px', fontWeight: 700, lineHeight: '30px', whiteSpace: 'pre-wrap'}}
      >
        {babyTitle}
      </Typography>
      {babyMonth > 23 && <BabyTitleTooltip mainRef={mainRef}/>}
    </Box>
  )
}

function BabyTitleSkeleton() {
  return (
    <Stack spacing={'6px'}>
      <Skeleton variant="rectangular" sx={{width: '250px', height: '27px', borderRadius: '5px'}}/>
      <Skeleton variant="rectangular" sx={{width: '180px', height: '27px', borderRadius: '5px'}}/>
    </Stack>
  )
}

function BabyTitleTooltip({mainRef}) {
  const [isTooltip, setIsTooltip] = useRecoilState(isOpenTooltipSelector)

  const handleIconClick = (event) => {
    event.stopPropagation();
    setIsTooltip(event.currentTarget)
  }

  return (
    <>
      <SvgInformationIcon
        alt={ALT_STRING.COMMON.ICON_TOOLTIP}
        tabIndex={3}
        onClick={(event) => handleIconClick(event)}
        onKeyDown={(e) => handleEnterPress(e, (event) => handleIconClick(event))}
        sx={{
          position: 'absolute',
          right: 0,
          top: 8,
          ...clickBorderNone
        }}
      />
      <MenuPopover
        isStopPropagation
        mainRef={mainRef}
        open={isTooltip}
        setOpen={setIsTooltip}
        arrow='home-babyinfo'
        onClose={() => setIsTooltip(null)}
        sx={{
          mt: '10px',
          ml: '10px',
          px: '20px',
          pt: '20px',
          pb: '20px',
          backgroundColor: 'white',
          border: '1px solid #E6E6E6',
          borderRadius: '8px',
          maxWidth: 310,
        }}
      >
        <Typography
          sx={{fontSize: '14px', fontWeight: 400, lineHeight: '20px', color: '#666666'}}>
          {DYNAMIC_ISLAND.TOOLTIP_CONTENT}
        </Typography>
      </MenuPopover>
    </>
  )
}