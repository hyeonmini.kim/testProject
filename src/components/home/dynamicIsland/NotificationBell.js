import {Box} from "@mui/material";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {useRecoilState} from "recoil";
import {notificationHasNewItemSelector, notificationOpenSelector} from "../../../recoil/selector/home/notificationSelector";
import Lottie from "react-lottie-player";
import AlramJson from "../../../../public/assets/icons/lottie/Alram.json";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";

export default function NotificationBell() {
  const [hasNewItem, setHasNewItem] = useRecoilState(notificationHasNewItemSelector)
  const [showNotification, setShowNotification] = useRecoilState(notificationOpenSelector)

  const handleClick = (event) => {
    event.stopPropagation();
    setShowNotification(true)
  }

  return (
    hasNewItem
      ? <BellLottie onClick={handleClick}/>
      : <BellIcon onClick={handleClick}/>
  )
}

function BellLottie({onClick}) {
  return (
    <Lottie
      alt={ALT_STRING.HOME.ALARM}
      tabIndex={2}
      animationData={AlramJson}
      play
      style={{width: '36px', height: '36px', position: 'absolute', right: '10px', top: '16px'}}
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
      sx={{zIndex: 100}}
    />
  )
}

function BellIcon({onClick}) {
  return (
    <Box
      tabIndex={2}
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
      sx={{width: '27px', position: 'absolute', right: '15px', top: '22px'}}
    >
      <SvgCommonIcons alt={ALT_STRING.HOME.ALARM} type={ICON_TYPE.BELL}/>
    </Box>
  )
}