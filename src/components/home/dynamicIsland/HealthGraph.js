import {LinearProgress, linearProgressClasses, Stack, Typography,} from "@mui/material";
import {useEffect, useRef, useState} from "react";
import {useRecoilState} from "recoil";
import {dynamicIslandExpandSelector} from "../../../recoil/selector/home/dynamicIslandSelector";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {useQueryClient} from "react-query";
import {HEALTH_GRAPH} from "../../../constant/home/DynamicIsland";

export default function HealthGraph({info}) {
  const height_percent = info?.profile_height?.percentage ? info?.profile_height?.percentage : 0
  const weight_percent = info?.profile_weight?.percentage ? info?.profile_weight?.percentage : 0
  const bmi_percent = info?.profile_bmi?.percentage ? info?.profile_bmi?.percentage : 0
  const height_statement = info?.profile_height?.statement ? info?.profile_height?.statement : ''
  const weight_statement = info?.profile_weight?.statement ? info?.profile_weight?.statement : ''
  const bmi_statement = info?.profile_bmi?.statement ? info?.profile_bmi?.statement : ''

  const [expand, setExpand] = useRecoilState(dynamicIslandExpandSelector);
  const [value, setValue] = useState([height_percent, weight_percent, bmi_percent])
  const [rank, setRank] = useState([height_statement, weight_statement, bmi_statement])

  const [position, setPosition] = useState([0, 0, 0])

  const isDrawingDone = useRef(false)
  const [isFirst, setIsFirst] = useState(true)
  const queryClient = useQueryClient()

  const timer = useRef(null)
  const drawHealthGraph = () => {
    setIsFirst(false)
    timer.current = setInterval(() => {
      setPosition((oldValue) => {
        let newValue = [0, 0, 0]
        let isDoneList = [false, false, false]
        if (oldValue[0] >= value[0]) {
          isDoneList[0] = true
          newValue[0] = value[0]
        } else {
          newValue[0] = oldValue[0] + HEALTH_GRAPH.DRAW_SPEED
        }

        if (oldValue[1] >= value[1]) {
          isDoneList[1] = true
          newValue[1] = value[1]
        } else {
          newValue[1] = oldValue[1] + HEALTH_GRAPH.DRAW_SPEED
        }

        if (oldValue[2] >= HEALTH_GRAPH.DEFAULT_BMI_VALUE) {
          isDoneList[2] = true
          newValue[2] = HEALTH_GRAPH.DEFAULT_BMI_VALUE
        } else {
          newValue[2] = oldValue[2] + HEALTH_GRAPH.DRAW_SPEED
        }

        isDrawingDone.current = !isDoneList.includes(false)
        return newValue
      })
    }, 100);
  }

  useEffect(() => {
    if (isDrawingDone.current) {
      clearInterval(timer.current)
    }
  }, [isDrawingDone.current])


  useEffect(() => {
    if (expand && isFirst) {
      drawHealthGraph()
    } else {
      setIsFirst(true)
      setPosition([0, 0, 0])
      isDrawingDone.current = false
    }
  }, [expand])

  useEffect(() => {
    return () => {
      queryClient.removeQueries('getUserBabyInfo')
    }
  }, [])

  return (
    <Stack direction="row" alignItems="center" spacing={2} justifyContent='center'>

      <LinearProgressItem value={position[0]} color={'#73DF78'} rank={rank[0]}/>

      <LinearProgressItem value={position[1]} color={'#74BCFF'} rank={rank[1]}/>

      <LinearProgressItem value={position[2]} color={'#FF7C9C'} rank={rank[2]} barPosition={(65 - value[2] * 0.75)}/>

    </Stack>
  )
}

function LinearProgressItem({value, rank, color = '#73DF78', barPosition}) {
  return (
    <Stack spacing={1} width='65px' alignItems='center' position='relative'>
      <LinearProgress
        sx={{
          width: 15,
          height: 100,
          [`& .${linearProgressClasses.bar}`]: {
            borderRadius: '99px',
            background: barPosition ? 'linear-gradient(#EF6F8E,#F9ADBF)' : color,
            transform: `translateY(${100 - value}%) !important`
          },
          bgcolor: '#EDEDED',
          borderRadius: 20,
        }}
        value={value}
        variant="determinate"
      />

      <Typography sx={{fontWeight: 500, fontSize: '14px', lineHeight: '14px', color: {color}}}>
        {rank}
      </Typography>

      <BarPosition value={value} barPosition={barPosition}/>
    </Stack>
  )
}

function BarPosition({value, barPosition}) {
  if (barPosition) {
    if (value >= HEALTH_GRAPH.MAX_BAR_POSITION) {
      return (
        <SvgCommonIcons type={ICON_TYPE.THIN_BAR} sx={{position: 'absolute', top: `${barPosition}%`}}/>
      )
    }
  } else if (value < HEALTH_GRAPH.MIDDLE_BAR_POSITION) {
    return (
      <SvgCommonIcons type={ICON_TYPE.MIDDLE_BAR} sx={{position: 'absolute', top: '42px'}}/>
    )
  }
}