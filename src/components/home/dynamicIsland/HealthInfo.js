import {Stack, Typography,} from "@mui/material";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {HOME_PERSONAL_COMMON} from "../../../constant/home/Personalization";
import * as React from "react";

export default function HealthInfo({info}) {
  return (
    <Stack direction="row" alignItems="center" spacing={3} justifyContent='center'>

      <Stack spacing='2px' width='90px' alignItems='center'>
        <SvgCommonIcons type={ICON_TYPE.HEALTH_HEIGHT}/>
        <Typography sx={{fontWeight: 400, fontSize: "12px", lineHeight: '18px'}}>
          {HOME_PERSONAL_COMMON.BABY.HEIGHT}
        </Typography>
        <Typography sx={{fontWeight: 500, fontSize: "16px", lineHeight: '24px'}}>
          {info?.profile_height?.value ? info?.profile_height?.value : ''}
        </Typography>
      </Stack>

      <Stack spacing='2px' width='90px' alignItems='center'>
        <SvgCommonIcons type={ICON_TYPE.HEALTH_WEIGHT}/>
        <Typography sx={{fontWeight: 400, fontSize: "12px", lineHeight: '18px'}}>
          {HOME_PERSONAL_COMMON.BABY.WEIGHT}
        </Typography>
        <Typography sx={{fontWeight: 500, fontSize: "16px", lineHeight: '24px'}}>
          {info?.profile_weight?.value ? info?.profile_weight?.value : ''}
        </Typography>
      </Stack>

      <Stack spacing='2px' width='90px' alignItems='center'>
        <SvgCommonIcons type={ICON_TYPE.HEALTH_ENERGY}/>
        <Typography sx={{fontWeight: 400, fontSize: "12px", lineHeight: '18px'}}>
          {HOME_PERSONAL_COMMON.BABY.ENERGY}
        </Typography>
        <Typography sx={{fontWeight: 500, fontSize: "16px", lineHeight: '24px'}}>
          {info?.profile_dashboard?.energy_statement ? info?.profile_dashboard?.energy_statement : ''}
        </Typography>
      </Stack>

    </Stack>
  )
}