import {Avatar, Box, Stack, Typography} from "@mui/material";
import Image from "../image";
import {m} from "framer-motion";
import getVariant from "../../sections/_examples/extra/animate/getVariant";
import {MotionContainer} from "../animate";

export default function CloseDynamic({title, subTitle}) {

  return (

    <Stack direction="row" alignItems="center" spacing={2} sx={{justifyContent: 'space-around'}}>

      {/* 프로필 사진 */}
      <MotionContainer>
        <Box
          variants={getVariant('fadeIn')}
          component={m.div}
        >
          <Avatar
            alt="profile"
            src="https://minimal-assets-api-dev.vercel.app/assets/images/avatars/avatar_1.jpg"
            sx={{
              width: 55,
              height: 55,
            }}
          />
        </Box>
      </MotionContainer>

      {/* 문구 */}
      <Box>
        <Stack direction="row" alignItems="center">
          <Image alt={''} src="/assets/icons/home/dynamicIsland/weather.svg" sx={{alignSelf: 'end'}}/>
          <MotionContainer>
            <Box
              component={m.span}
              variants={getVariant('fadeIn')}
              sx={{
                borderRadius: 1,
              }}>
              <Typography fontWeight="400" fontSize="16px" sx={{fontSize: '14px', fontWeight: 400, lineHeight: '20px', ml: '3px'}}>
                {subTitle}
              </Typography>
            </Box>
          </MotionContainer>

        </Stack>
        <MotionContainer>
          <Box
            component={m.span}
            variants={getVariant('fadeIn')}
            sx={{
              borderRadius: 1,
            }}>
            <Typography fontWeight="400" fontSize="16px" sx={{fontSize: '16px', fontWeight: 700, lineHeight: '24px'}}>
              {title}
            </Typography>
          </Box>
        </MotionContainer>


      </Box>

      <MotionContainer>
        <Box
          component={m.img}
          variants={getVariant('fadeIn')}
          src='/assets/icons/home/dynamicIsland/bell.svg'
          sx={{
            borderRadius: 1,
            width: '25px',
            height: '25px'
          }}>
        </Box>
      </MotionContainer>


      {/*<IconButton aria-label="alarm" sx={{ mt: 1.9 }}>*/}
      {/*  <Iconify icon={"eva:bell-outline"} width={30} height={30} />*/}
      {/*</IconButton>*/}
    </Stack>
  )
}