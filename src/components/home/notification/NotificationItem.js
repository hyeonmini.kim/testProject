import {Avatar, AvatarGroup, Box} from "@mui/material";
import Image from "../../image";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {useRecoilValue, useSetRecoilState} from "recoil";
import {createTemporaryRecipeSelector} from "../../../recoil/selector/recipe/createSelector";
import useStackNavigation from "../../../hooks/useStackNavigation";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {updateNotiRead} from "../../../api/homeApi";
import {HOME_NOTIFICATION_TYPE} from "../../../constant/home/Notification";
import {RECIPE_STATUS} from "../../../constant/recipe/Recipe";
import {STACK} from "../../../constant/common/StackNavigation";
import LoadingScreen from "../../common/LoadingScreen";
import {getRecipeDetailMutate} from "../../../api/detailApi";
import {getURIString} from "../../../utils/formatString";
import {forMyPageEntrySelector} from "../../../recoil/selector/auth/userSelector";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";

export default function NotificationItem({item, isNew = false, onClick, setShowChoco, setShowCrunchyKing}) {
  const user = useRecoilValue(forMyPageEntrySelector)
  const setTempRecipe = useSetRecoilState(createTemporaryRecipeSelector)
  const {
    noti_key,
    noti_subject,
    noti_type,
    noti_img_key,
    noti_recipe_key,
    noti_recipe_name,
    noti_comment_key,
    create_date,
  } = item;

  const {navigation, preFetch} = useStackNavigation()
  const {mutate: mutateUpdateNotiRead} = updateNotiRead()
  const {mutate: mutateRecipeDetail, isLoading} = getRecipeDetailMutate()

  const handleAction = () => {
    switch (noti_type) {
      case HOME_NOTIFICATION_TYPE.RECIPE:
        onClick(PATH_DONOTS_PAGE.RECIPE.DETAIL(user?.nickname, getURIString(noti_recipe_name), noti_recipe_key))
        break
      case HOME_NOTIFICATION_TYPE.NOTICE:
        navigation.push(STACK.HOME.TYPE, PATH_DONOTS_PAGE.MY.NOTICE(noti_recipe_key), {
          ...STACK.HOME.DATA,
          open: true,
        })
        break
      case HOME_NOTIFICATION_TYPE.EXAMINATION:
        mutateRecipeDetail({
            recipe_key: noti_recipe_key
          }, {
            onSuccess: (data) => {
              setTempRecipe({
                status: RECIPE_STATUS.EXAMINATION,
                ...data
              })
              onClick(PATH_DONOTS_PAGE.RECIPE.CREATE)
            }
          }
        )
        break
      case HOME_NOTIFICATION_TYPE.REJECT:
        mutateRecipeDetail({
            recipe_key: noti_recipe_key
          }, {
            onSuccess: (data) => {
              setTempRecipe({
                status: RECIPE_STATUS.REJECT,
                ...data
              })
              onClick(PATH_DONOTS_PAGE.RECIPE.CREATE)
            }
          }
        )
        break
      case HOME_NOTIFICATION_TYPE.SPRINKLE:
        onClick(PATH_DONOTS_PAGE.MY.SELF)
        break
      case HOME_NOTIFICATION_TYPE.LV2:
        setShowChoco(true)
        break
      case HOME_NOTIFICATION_TYPE.LV3:
        setShowCrunchyKing(true)
        break
      case HOME_NOTIFICATION_TYPE.SUR01:
      case HOME_NOTIFICATION_TYPE.SUR02:
        navigation.push(STACK.HOME.TYPE, PATH_DONOTS_PAGE.LAB.COMMENT_DETAIL(noti_recipe_key, noti_comment_key), {
          ...STACK.HOME.DATA,
          showNotification: true,
        })
        break
    }
  }

  const handleClick = () => {
    if (isNew) {
      mutateUpdateNotiRead({
        noti_key: noti_key,
      }, {
        onSuccess: () => handleAction()
      })
    } else {
      handleAction()
    }
  }

  const showNotiImage = () => {
    switch (noti_type) {
      case HOME_NOTIFICATION_TYPE.RECIPE:
      case HOME_NOTIFICATION_TYPE.EXAMINATION:
      case HOME_NOTIFICATION_TYPE.REJECT:
        return noti_img_key ? true : false
    }
    return false
  }

  return (
    <Box
      tabIndex={0}
      sx={{
        flexDirection: 'row',
        height: '50px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        columnGap: '20px'
      }}
      onClick={handleClick}
      onKeyDown={(e) => handleEnterPress(e, handleClick)}
    >
      {/* 아바타 */}
      <ItemAvatar type={noti_type}/>

      {/* 아이템 제목 */}
      <ItemSubject subject={noti_subject} date={create_date}/>

      {/* 아이템 이미지 */}
      {showNotiImage() && <ItemImage alt={noti_recipe_name} src={noti_img_key}/>}

      {/* 로딩 스크린 */}
      {isLoading && <LoadingScreen dialog/>}
    </Box>
  )
}

function ItemSubject({subject, date}) {
  return (
    <Box sx={{flexGrow: 1, fontSize: '14px', fontWeight: 500, lineHeight: '20px', color: '#222222'}}>
      {subject}
      <Box display='inline'
           sx={{
             color: '#888888',
             fontSize: '14px',
             fontWeight: 400,
             wordBreak: 'keep-all'
           }}> {date} </Box>
    </Box>
  )
}

function ItemImage({alt, src}) {
  return (
    <Box sx={{display: 'flex', alignItems: 'center'}}>
      <Image alt={alt} src={src} isLazy={false} sx={{width: '50px', height: '50px', borderRadius: '4px'}}/>
    </Box>
  )
}

function ItemAvatar({type}) {
  let src = ICON_TYPE.NO_AVATAR1
  switch (type) {
    case HOME_NOTIFICATION_TYPE.RECIPE:
      src = ICON_TYPE.NOTICE_RECIPE_POSTING
      break
    case HOME_NOTIFICATION_TYPE.NOTICE:
      src = ICON_TYPE.NOTICE_ITEM
      break
    case HOME_NOTIFICATION_TYPE.EXAMINATION:
      src = ICON_TYPE.NOTICE_RECIPE_EXAMINATION
      break
    case HOME_NOTIFICATION_TYPE.REJECT:
      src = ICON_TYPE.NOTICE_RECIPE_REJECT
      break
    case HOME_NOTIFICATION_TYPE.SPRINKLE:
    case HOME_NOTIFICATION_TYPE.LV2:
      src = ICON_TYPE.LOGO_40PX
      break
    case HOME_NOTIFICATION_TYPE.LV3:
      src = ICON_TYPE.LOGO_SPRINKLE_40PX
      break
    case HOME_NOTIFICATION_TYPE.SUR01:
    case HOME_NOTIFICATION_TYPE.SUR02:
      src = ICON_TYPE.NOTICE_POST_COMMENT
      break
  }
  return (
    <Box sx={{display: 'flex', alignItems: 'center'}}>
      <SvgCommonIcons type={src} sx={{width: '40px', height: '40px'}}/>
    </Box>
  )
}

function ItemAvatarGroup({item}) {
  return (
    <AvatarGroup
      max={2}
      sx={{
        '& .MuiAvatarGroup-avatar': {
          width: 35,
          height: 35,
          '&:first-of-type': {
            ml: '-35px',
            mt: '10px',
          },
        },
      }}
    >
      <Avatar src={item?.avatars ? item.avatars[0] : '/assets/icons/detail/no_avatar.svg'} sx={{zIndex: 0}}/>
      <Avatar src={item?.avatars ? item.avatars[1] : '/assets/icons/detail/no_avatar.svg'} sx={{zIndex: 1}}/>
    </AvatarGroup>
  )
}