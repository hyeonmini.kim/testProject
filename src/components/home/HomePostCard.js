import {Avatar, Box, Card, Stack,} from "@mui/material";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import Image from "../image";
import PostContent from "./PostContent"
import * as React from "react";
import {useEffect} from "react";
import {getURIString} from "../../utils/formatString";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {useRouter} from "next/router";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {UserBadge} from "../../constant/icons/UserBadge";
import TextMaxLine from "../text-max-line";
import {ALT_STRING} from "../../constant/common/AltString";

export default function HomePostCard({post, index, onClick}) {
  const {
    recipe_name,
    recipe_writer_info,
    recipe_key,
  } = post;

  const linkTo = PATH_DONOTS_PAGE.RECIPE.DETAIL(recipe_writer_info?.recipe_writer_nickname, getURIString(recipe_name), recipe_key);
  const router = useRouter()

  useEffect(() => {
    router?.prefetch(linkTo)
  }, [router])

  const handleClick = () => {
    if (onClick) {
      onClick(index, recipe_key)
    }
    router?.push(post?.recipe_iframe ? post?.recipe_iframe : linkTo)
  }

  return (
    <Card
      tabIndex={5}
      sx={{
        borderRadius: '8px', border: '1px solid #F5F5F5',
        boxShadow: '0px 6px 123px rgba(174, 174, 174, 0.11), 0px 2.54258px 54.0057px rgba(174, 174, 174, 0.06), 0px 1.41914px 20.0513px rgba(174, 174, 174, 0.04)',
      }}
      onClick={handleClick}
      onKeyDown={(e) => handleEnterPress(e, handleClick)}
    >

      {/* 포스트 이미지 & 프로파일 */}
      <PostImageProfile post={post} index={index}/>

      {/* 포스트 컨텐츠 */}
      <PostContent post={post}/>

    </Card>
  );
}

function PostImageProfile({post, index}) {
  const {
    image_file_path,
    recipe_writer_info,
    recipe_name,
  } = post;

  return (
    <Box sx={{position: "relative"}}>

      {/* 레시피 이미지 */}
      <Image alt={ALT_STRING.COMMON.RECIPE(recipe_name)} src={image_file_path} ratio="1/1" isLazy={index >= 2}/>

      {/* 하단 배경 */}
      <BottomGradient/>

      {/* 아바타 & 프로파일 */}
      <AvatarProfileSection
        alt={recipe_writer_info?.recipe_writer_nickname}
        avatar={recipe_writer_info?.recipe_writer_thumbnail_path}
        nickname={recipe_writer_info?.recipe_writer_nickname}
        type={recipe_writer_info?.recipe_writer_type}
        grade={recipe_writer_info?.recipe_writer_grade}
      />

    </Box>
  )
}

function BottomGradient() {
  return (
    <Box
      sx={{
        position: 'absolute',
        width: '100%',
        height: '110px',
        bottom: 0,
        background: 'linear-gradient(180deg, rgba(0, 0, 0, 0.5) 0%, rgba(217, 217, 217, 0) 100%)',
        transform: 'rotate(-180deg)',
        zIndex: 5,
      }}>
    </Box>
  )
}

function AvatarProfileSection({alt, avatar, nickname, type, grade}) {
  return (
    <Stack
      direction={'row'}
      sx={{
        position: 'absolute',
        left: 20,
        bottom: 15,
        zIndex: 9,
        width: '80%',
        alignItems: 'center',
      }}
    >
      {/* 아바타 */}
      {
        avatar
          ? <WriterAvatar alt={''} avatar={avatar}/>
          : <SvgCommonIcons alt={''} type={ICON_TYPE.PROFILE_NO_IMAGE_30PX} sx={{width: 30}}/>
      }

      {/* 닉네임 */}
      <TextMaxLine
        open={true}
        line={1}
        sx={{pl: '10px', fontSize: '16px', fontWeight: 400, lineHeight: '24px', zIndex: 99, color: '#FFFFFF'}}
      >
        {nickname}
      </TextMaxLine>

      {/* 뱃지 */}
      <UserBadge
        type={type}
        grade={grade}
        isAlt={false}
        sx={{position: 'absolute', left: '22px', bottom: '-3px', width: '16px', zIndex: 11}}
      />
    </Stack>
  )
}

function WriterAvatar({alt, avatar}) {
  return (
    <Avatar
      alt={alt}
      src={avatar}
      sx={{
        width: 30,
        height: 30,
        alignItems: 'center',
      }}
    />
  )
}