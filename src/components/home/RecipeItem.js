import {Avatar, Box, Stack, Typography} from "@mui/material";
import Image from "../image";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import TextMaxLine from "../text-max-line";
import * as React from "react";
import {useEffect} from "react";
import {getNumberTenThousand} from "../../utils/formatNumber";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {getURIString} from "../../utils/formatString";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {useRouter} from "next/router";
import {UserBadge} from "../../constant/icons/UserBadge";
import {HOME_PERSONAL_COMMON} from "../../constant/home/Personalization";
import {ALT_STRING} from "../../constant/common/AltString";

export default function RecipeItem({tabIndex, post, onClick, index}) {
  const {
    recipe_writer_info,
    recipe_name,
    recipe_key,
  } = post;

  const linkTo = PATH_DONOTS_PAGE.RECIPE.DETAIL(recipe_writer_info?.recipe_writer_nickname, getURIString(recipe_name), recipe_key);

  const router = useRouter()
  useEffect(() => {
    router?.prefetch(linkTo)
  }, [router])

  const handleClick = () => {
    if (onClick) {
      onClick(recipe_key, recipe_name, index)
    }
    router?.push(post?.recipe_iframe ? post?.recipe_iframe : linkTo)
  }

  return (
    <Stack
      tabIndex={tabIndex}
      direction='row'
      spacing='10px'
      onClick={handleClick}
      onKeyDown={(e) => handleEnterPress(e, handleClick)}
    >

      {/* 포스트 이미지 & 프로파일 */}
      <PostImageProfile post={post}/>

      {/* 포스트 컨텐츠 */}
      <PostContents post={post}/>

    </Stack>
  );
}

function PostContents({post}) {
  const {
    recipe_level,
    recipe_name,
    recipe_health_tag,
    recipe_lead_time,
    recipe_view_cnt,
    recipe_scrap_cnt,
  } = post

  return (
    <Stack spacing={'6px'} sx={{display: 'flex'}}>
      {/* 레시피 태그 */}
      {recipe_health_tag && <RecipeHealthTag recipe_health_tag={recipe_health_tag}/>}

      {/* 레시피 타이틀 */}
      <TextMaxLine open={true} line={2} sx={{fontSize: '18px', fontWeight: 500, lineHeight: '24px'}}>
        {recipe_name}
      </TextMaxLine>

      {/* 조회수 & 스크랩 */}
      <Stack direction={'row'} spacing={'12px'} sx={{pt: '2px', mb: '14px'}}>
        <Typography sx={{fontSize: '12px', fontWeight: 400, lineHeight: '16px', mr: '4px', color: '#888888'}}>
          {HOME_PERSONAL_COMMON.VIEW_CNT(getNumberTenThousand(recipe_view_cnt))}
        </Typography>
        <Typography sx={{fontSize: '12px', fontWeight: 400, lineHeight: '16px', color: '#888888'}}>
          {HOME_PERSONAL_COMMON.SCRAP_CNT(getNumberTenThousand(recipe_scrap_cnt))}
        </Typography>
      </Stack>

      {/* 칩 */}
      <Stack direction={'row'} spacing={'8px'} sx={{mt: '12px', mb: '14px'}}>
        <PostChip title={recipe_lead_time}/>
        <PostChip title={recipe_level}/>
      </Stack>
    </Stack>
  )
}

function RecipeHealthTag({recipe_health_tag}) {
  return (
    <Box sx={{display: 'flex'}}>
      <HealthChip title={recipe_health_tag}/>
      <Box/>
    </Box>
  )
}

function PostImageProfile({post}) {
  const {
    recipe_name,
    image_file_path,
    recipe_writer_info,
  } = post;

  return (
    <Box sx={{position: 'relative'}}>

      {/* 레시피 이미지 */}
      <Image
        alt={ALT_STRING.COMMON.RECIPE(recipe_name)}
        src={image_file_path} ratio="1/1"
        sx={{width: '132px', height: '132px', borderRadius: '8px'}}
      />

      {/* 하단 배경 */}
      <BottomGradient/>

      {/* 아바타 & 프로파일 */}
      <AvatarProfileSection
        alt={recipe_writer_info?.recipe_writer_nickname}
        avatar={recipe_writer_info?.recipe_writer_thumbnail_path}
        nickname={recipe_writer_info?.recipe_writer_nickname}
        type={recipe_writer_info?.recipe_writer_type}
        grade={recipe_writer_info?.recipe_writer_grade}
      />

    </Box>
  )
}

function BottomGradient() {
  return (
    <Box
      sx={{
        position: 'absolute',
        width: '100%',
        height: '66px',
        bottom: 0,
        background: 'linear-gradient(180deg, rgba(0, 0, 0, 0.5) 0%, rgba(217, 217, 217, 0) 100%)',
        transform: 'rotate(-180deg)',
        borderRadius: '8px',
        zIndex: 5,
      }}>
    </Box>
  )
}

function AvatarProfileSection({alt, avatar, nickname, type, grade}) {
  return (
    <Stack
      direction={'row'}
      sx={{
        position: 'absolute',
        left: 10,
        bottom: 10,
        zIndex: 9,
        alignItems: 'center',
        width: '90%',
      }}
    >
      {/* 아바타 */}
      <Box sx={{width: '22px'}}>
        {
          avatar
            ? <WriterAvatar alt={ALT_STRING.COMMON.NICKNAME(alt)} avatar={avatar}/>
            : <SvgCommonIcons alt={''} type={ICON_TYPE.PROFILE_NO_IMAGE_30PX} sx={{width: 22}}/>
        }
      </Box>

      {/* 닉네임 */}
      <TextMaxLine
        open={true}
        line={1}
        sx={{pl: type ? '10px' : '6px', fontSize: '14px', fontWeight: 400, lineHeight: '20px', zIndex: 99, color: '#FFFFFF'}}
      >
        {nickname}
      </TextMaxLine>

      {/* 뱃지 */}
      <UserBadge
        type={type}
        grade={grade}
        isAlt={false}
        sx={{position: 'absolute', left: '15px', bottom: '-3px', width: '14px', zIndex: 11}}
      />
    </Stack>
  )
}

function WriterAvatar({alt, avatar}) {
  return (
    <Avatar
      alt={alt}
      src={avatar}
      sx={{
        width: 22,
        height: 22,
      }}
    />
  )
}

function HealthChip({title}) {
  return (
    <Box
      sx={{
        height: '24px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '4px',
        px: '6px',
        bgcolor: '#EFFAEC',
        fontSize: '12px',
        fontWeight: 400,
        lineHeight: '12px',
        color: '#3D6A4A'
      }}
    >
      {title}
    </Box>
  )
}

function PostChip({title}) {
  return (
    <Box
      sx={{
        width: '70px',
        height: '24px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '4px',
        bgcolor: '#FAFAFA',
        fontSize: '12px',
        fontWeight: 400,
        lineHeight: '12px',
        color: '#888888'
      }}
    >
      {title}
    </Box>
  )
}