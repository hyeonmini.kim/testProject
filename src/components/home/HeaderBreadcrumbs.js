import {Box, Stack, Typography} from "@mui/material";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import * as React from "react";
import {useState} from "react";
import {HOME_PERSONAL_COMMON} from "../../constant/home/Personalization";
import {SvgInformationIcon} from "../../constant/icons/icons";
import MenuPopover from "../menu-popover";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../constant/common/Common";
import {ALT_STRING} from "../../constant/common/AltString";
import zIndex from "@mui/material/styles/zIndex";
import {Z_INDEX} from "../../constant/common/ZIndex";

export default function HeaderBreadcrumbs({mainRef, tabIndex, heading, tooltip, onClick, sx}) {
  return (
    <Box sx={{...sx, display: 'inline-block', width: 'inherit'}}>
      <Box sx={{display: "flex", alignItems: "center", justifyContent: 'space-between'}}>
        <Box sx={{
          fontSize: '20px',
          fontWeight: 700,
          lineHeight: '20px',
          position: 'relative',
        }}>
          {heading}
          {tooltip && (
            <HeaderToolTip tabIndex={tabIndex} mainRef={mainRef} tooltip={tooltip}/>
          )}
        </Box>
        {onClick && <HeaderMore tabIndex={tabIndex} onClick={onClick}/>}
      </Box>
    </Box>
  );
}

function HeaderMore({tabIndex, onClick}) {
  return (
    <Stack
      tabIndex={tabIndex}
      direction={'row'}
      spacing={'4px'}
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
      sx={{
        zIndex: 2,
      }}
    >
      <Typography sx={{fontSize: '14px', fontWeight: 500, lineHeight: '14px', color: '#888888'}}>
        {HOME_PERSONAL_COMMON.MORE}
      </Typography>
      <SvgCommonIcons type={ICON_TYPE.ARROW_RIGHT_14PX}/>
    </Stack>
  )
}

function HeaderToolTip({tabIndex, mainRef, tooltip}) {
  const [isTooltip, setIsTooltip] = useState(false)

  const handleIconClick = (event) => {
    event.stopPropagation();
    setIsTooltip(event.currentTarget)
  }

  return (
    <>
      <SvgInformationIcon
        alt={ALT_STRING.COMMON.ICON_TOOLTIP}
        tabIndex={tabIndex}
        onClick={(event) => handleIconClick(event)}
        onKeyDown={(e) => handleEnterPress(e, (event) => handleIconClick(event))}
        sx={{
          position: 'absolute',
          right: -22,
          top: 3,
          zIndex: Z_INDEX.TOOLTIP,
          ...clickBorderNone
        }}
      />
      <MenuPopover
        mainRef={mainRef}
        open={isTooltip}
        setOpen={setIsTooltip}
        arrow={tooltip?.arrow ? tooltip.arrow : 'top-right'}
        onClose={() => setIsTooltip(null)}
        sx={{
          mt: '10px',
          px: '20px',
          pt: '20px',
          pb: '20px',
          backgroundColor: 'white',
          border: '1px solid #E6E6E6',
          borderRadius: '8px',
          maxWidth: 330
        }}
      >
        <Typography
          sx={{fontSize: '14px', fontWeight: 400, lineHeight: '20px', color: '#666666'}}>
          {tooltip?.contents}
        </Typography>
      </MenuPopover>
    </>
  )
}
