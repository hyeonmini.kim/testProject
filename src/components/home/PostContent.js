import {Box, CardContent, Stack} from "@mui/material";

import TextMaxLine from "../text-max-line";
import * as React from "react";
import {HOME_PERSONAL_COMMON} from "../../constant/home/Personalization";

export default function PostContent({post}) {
  const {
    recipe_name,
    recipe_desc,
    recipe_review_total_count,
    recipe_lead_time,
    recipe_level,
    recipe_allergy_tag
  } = post;

  return (
    <CardContent sx={{px: '20px', mt: '15px', mb: '13px', pt: 0, "&:last-child": {pb: 0}}}>
      <TextMaxLine open={true} line={1} sx={{fontSize: '20px', fontWeight: 500, lineHeight: '26px'}}>
        {recipe_name}
      </TextMaxLine>
      <TextMaxLine line={1} sx={{fontSize: '16px', fontWeight: 400, lineHeight: '24px', mt: '10px'}}>
        {HOME_PERSONAL_COMMON.ALLERGY_TAG(recipe_allergy_tag)}
      </TextMaxLine>
      <Stack direction={'row'} spacing={'8px'} sx={{mt: '12px', mb: '14px'}}>
        <PostTag title={recipe_lead_time} />
        <PostTag title={recipe_level} />
      </Stack>
    </CardContent>
  );
}

function PostTag({title}) {
  return (
    <Box
      sx={{
        width: '70px',
        height: '24px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '4px',
        bgcolor: '#FAFAFA',
        fontSize: '12px',
        fontWeight: 400,
        lineHeight: '12px',
        color: '#888888'
      }}
    >
      {title}
    </Box>
  )
}