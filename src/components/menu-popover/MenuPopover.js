import {Popover} from '@mui/material';
import getPosition from './getPosition';
import {StyledArrow} from './styles';
import {useEffect} from "react";

export default function MenuPopover({
                                      open,
                                      setOpen,
                                      children,
                                      arrow = 'top-right',
                                      disabledArrow,
                                      sx,
                                      arrowBottomStyle,
                                      mainRef,
                                      isStopPropagation = false,
                                      ...other
                                    }) {
  const {style, anchorOrigin, transformOrigin} = getPosition(arrow);

  function handleClose(event) {
    if (isStopPropagation) {
      event.stopPropagation();
    }
    setTimeout(() => setOpen(null), 100)
  }

  useEffect(() => {
    if (open) {
      mainRef?.current?.addEventListener('scroll', (event) => handleClose(event), false);
      mainRef?.current?.addEventListener('click', handleClose, false);
    }
    return () => {
      mainRef?.current?.removeEventListener('scroll', handleClose, false);
      mainRef?.current?.removeEventListener('click', handleClose, false);
    }
  }, [open, mainRef?.current])

  return (
    <Popover
      disableScrollLock={true}
      open={Boolean(open)}
      anchorEl={open}
      anchorOrigin={anchorOrigin}
      transformOrigin={transformOrigin}
      PaperProps={{
        sx: {
          p: 1,
          width: 'auto',
          overflow: 'inherit',
          ...style,
          '& .MuiMenuItem-root': {
            px: 1,
            typography: 'body2',
            borderRadius: 0.75,
            '& svg': {mr: 2, width: 20, height: 20, flexShrink: 0},
          },
          ...sx,
        },
      }}
      {...other}
      sx={{
        pointerEvents: 'none',
      }}
    >
      {!disabledArrow && <StyledArrow arrow={arrow} arrowBottomStyle={arrowBottomStyle}/>}

      {children}
    </Popover>
  );
}
