export default function getPosition(arrow) {
  let props;

  switch (arrow) {
    case 'home-babyinfo':
      props = {
        style: {},
        anchorOrigin: {vertical: 'bottom', horizontal: 'right'},
        transformOrigin: {vertical: 'top', horizontal: 'right'},
      };
      break;
    case 'home-custom':
      props = {
        style: {},
        anchorOrigin: {vertical: 'bottom', horizontal: -25},
        transformOrigin: {vertical: 'top', horizontal: 'center'},
      };
      break;
    case 'guest-user':
      props = {
        style: {},
        anchorOrigin: {vertical: 'bottom', horizontal: 10},
        transformOrigin: {vertical: 'top', horizontal: 'center'},
      };
      break;
    case 'guest-custom':
      props = {
        style: {},
        anchorOrigin: {vertical: 'bottom', horizontal: -33},
        transformOrigin: {vertical: 'top', horizontal: 'center'},
      };
      break;
    case 'top-left':
      props = {
        style: {ml: -0.75},
        anchorOrigin: {vertical: 'bottom', horizontal: 'left'},
        transformOrigin: {vertical: 'top', horizontal: 'left'},
      };
      break;
    case 'top-left-detail':
      props = {
        style: {ml: -1.75},
        anchorOrigin: {vertical: 15, horizontal: 225},
        transformOrigin: {vertical: 'top', horizontal: 'right'},
      };
      break;
    case 'top-center':
      props = {
        style: {},
        anchorOrigin: {vertical: 'bottom', horizontal: 'center'},
        transformOrigin: {vertical: 'top', horizontal: 'center'},
      };
      break;
    case 'top-right':
      props = {
        style: {ml: 0.75},
        anchorOrigin: {vertical: 'bottom', horizontal: 'right'},
        transformOrigin: {vertical: 'top', horizontal: 'right'},
      };
      break;
    case 'bottom-left':
      props = {
        style: {ml: -0.75},
        anchorOrigin: {vertical: 'top', horizontal: 'left'},
        transformOrigin: {vertical: 'bottom', horizontal: 'left'},
      };
      break;
    case 'bottom-center':
      props = {
        style: {},
        anchorOrigin: {vertical: 'top', horizontal: 'center'},
        transformOrigin: {vertical: 'bottom', horizontal: 'center'},
      };
      break;
    case 'bottom-center-create':
      props = {
        style: {},
        anchorOrigin: {vertical: -20, horizontal: -42},
        transformOrigin: {vertical: 'bottom', horizontal: 'center'},
      };
      break;
    case 'bottom-center-my':
      props = {
        style: {},
        anchorOrigin: {vertical: -12, horizontal: 32},
        transformOrigin: {vertical: 'bottom', horizontal: 'center'},
      };
      break;
    case 'bottom-right':
      props = {
        style: {ml: 0.75},
        anchorOrigin: {vertical: 'top', horizontal: 'right'},
        transformOrigin: {vertical: 'bottom', horizontal: 'right'},
      };
      break;
    case 'left-top':
      props = {
        style: {mt: -0.75},
        anchorOrigin: {vertical: 'top', horizontal: 'right'},
        transformOrigin: {vertical: 'top', horizontal: 'left'},
      };
      break;
    case 'left-center':
      props = {
        anchorOrigin: {vertical: 'center', horizontal: 'right'},
        transformOrigin: {vertical: 'center', horizontal: 'left'},
      };
      break;
    case 'left-bottom':
      props = {
        style: {mt: 0.75},
        anchorOrigin: {vertical: 'bottom', horizontal: 'right'},
        transformOrigin: {vertical: 'bottom', horizontal: 'left'},
      };
      break;
    case 'right-top':
      props = {
        style: {mt: -0.75},
        anchorOrigin: {vertical: 'top', horizontal: 'left'},
        transformOrigin: {vertical: 'top', horizontal: 'right'},
      };
      break;
    case 'right-center':
      props = {
        anchorOrigin: {vertical: 'center', horizontal: 'left'},
        transformOrigin: {vertical: 'center', horizontal: 'right'},
      };
      break;
    case 'right-bottom':
      props = {
        style: {mt: 0.75},
        anchorOrigin: {vertical: 'bottom', horizontal: 'left'},
        transformOrigin: {vertical: 'bottom', horizontal: 'right'},
      };
      break;

    // top-right
    default:
      props = {
        style: {ml: 0.75},
        anchorOrigin: {vertical: 'bottom', horizontal: 'right'},
        transformOrigin: {vertical: 'top', horizontal: 'right'},
      };
  }

  return props;
}
