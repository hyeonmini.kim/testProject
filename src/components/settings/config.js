// PLEASE REMOVE `LOCAL STORAGE` WHEN YOU CHANGE SETTINGS.
// ----------------------------------------------------------------------

export const defaultSettings = {
  themeMode: 'light',
  themeDirection: 'ltr',
  //themeContrast: 'default',
  themeContrast: 'bold',
  themeLayout: 'horizontal',
  themeColorPresets: 'default',
  themeStretch: false,
};
