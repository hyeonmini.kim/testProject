import {useEffect, useState} from 'react';
// @mui
import {styled} from '@mui/material/styles';
//
import Lottie from "react-lottie-player";
import BookmarkJson from "../../../../public/assets/icons/lottie/Bookmark.json";
import {sxFixedCenterMainLayout} from "../../../layouts/main/MainLayout";
import {Z_INDEX} from "../../../constant/common/ZIndex";
// ----------------------------------------------------------------------

const StyledRoot = styled('div')(({theme}) => ({
  right: 0,
  bottom: 0,
  zIndex: Z_INDEX.LOADING,
  width: '100%',
  height: '100%',
  position: 'fixed',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: 'rgba(0, 0, 0, 0.1)',
  ...sxFixedCenterMainLayout
}));

// ----------------------------------------------------------------------

export default function BookmarkScreen() {
  const [mounted, setMounted] = useState(false);

  useEffect(() => setMounted(true), []);

  if (!mounted) {
    return
  }

  return (
    <StyledRoot>
      <Lottie
        animationData={BookmarkJson}
        play
        speed={2}
        style={{width: '150px', height: '150px'}}
      />
    </StyledRoot>
  );
}
