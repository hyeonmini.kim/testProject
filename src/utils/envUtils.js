import {ENV, IS_MOCK} from "../config";
import {useRecoilValue} from "recoil";
import {envSelector, isMockSelector} from "../recoil/selector/common/versionSelector";
import {isAuthenticated} from "../recoil/atom/auth/isAuthenticated";

export function isNative() {
  const userAgent = typeof navigator === 'undefined' ? 'SSR' : navigator.userAgent;
  return userAgent == 'donots' || userAgent == 'donotsi' ? true : false
}

export function isIosNative() {
  const userAgent = typeof navigator === 'undefined' ? 'SSR' : navigator.userAgent;
  return userAgent == 'donotsi' ? true : false
}

export function isIos() {
  const userAgent = typeof navigator === 'undefined' ? 'SSR' : navigator.userAgent;
  return /iPhone|iPad|iPod/i.test(userAgent);
}

export function isMockApi() {
  if (isNative()) {
    return useRecoilValue(isMockSelector)
  } else {
    return IS_MOCK
  }
}

export function getEnv() {
  if (isNative()) {
    return useRecoilValue(envSelector)
  } else {
    return ENV
  }
}

export function isAuthApi() {
  return useRecoilValue(isAuthenticated)
}