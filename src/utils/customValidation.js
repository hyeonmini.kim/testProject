import {SIGN_UP_BIRTH, SIGN_UP_CHILD_BIRTH} from "../constant/sign-up/SignUp";

export const checkChildBirthValidation = (value, setIsTrue, setIsBottomBtnDisabled, setHelperText) => {
  // 숫자 6자리 정규식
  const regex = /[^0-9]/g
  let inputDate = value?.replace(regex, '')
  // 월, 일 입력값 검증
  const year = parseInt(inputDate?.substring(0, 2))
  const month = parseInt(inputDate?.substring(2, 4))
  const date = parseInt(inputDate?.substring(4, 6))
  const curYear = (new Date().getFullYear()).toString().substring(2, 4)
  const curMonth = (new Date().getMonth() + 1) < 10 ? `0${(new Date().getMonth() + 1).toString()}` : (new Date().getMonth() + 1).toString()
  const curDate = new Date().getDate() < 10 ? `0${(new Date().getDate()).toString()}` : (new Date().getDate()).toString()
  const curDateNum = parseInt(curYear + curMonth + curDate)
  const lastDate = new Date(2000 + year, month, 0).getDate()
  const isMonth = month > 0 ? month <= 12 : false
  const isDate = date > 0 ? date <= lastDate : false

  switch (inputDate.length) {
    case 0:
      setIsTrue(true)
      setHelperText(SIGN_UP_CHILD_BIRTH.HELPER_TEXT)
      setIsBottomBtnDisabled(true)
      break;
    case 6:
      if (isMonth && isDate && parseInt(inputDate) <= curDateNum) {
        setIsTrue(true)
        setIsBottomBtnDisabled(false)
      } else {
        if (!isMonth || !isDate) {
          setHelperText(SIGN_UP_CHILD_BIRTH.HELPER_TEXT)
        } else {
          setHelperText(SIGN_UP_CHILD_BIRTH.HELPER_TEXT_EXCESS)
        }
        setIsTrue(false)
        setIsBottomBtnDisabled(true)
      }
      break;
    default:
      setIsTrue(false)
      setHelperText(SIGN_UP_CHILD_BIRTH.HELPER_TEXT)
      setIsBottomBtnDisabled(true)
      break;
  }
}

export const checkBirthValidation = (value, setIsTrue, setIsBottomBtnDisabled, setHelperText) => {
  // 숫자 6자리 정규식
  const regex = /[^0-9]/g
  let inputDate = value?.replace(regex, '')
  // 월, 일 입력값 검증
  const year = parseInt(inputDate?.substring(0, 4))
  const month = parseInt(inputDate?.substring(4, 6))
  const date = parseInt(inputDate?.substring(6, 8))
  const curYear = (new Date().getFullYear()).toString()
  const curMonth = (new Date().getMonth() + 1) < 10 ? `0${(new Date().getMonth() + 1).toString()}` : (new Date().getMonth() + 1).toString()
  const curDate = new Date().getDate() < 10 ? `0${(new Date().getDate()).toString()}` : (new Date().getDate()).toString()
  const curDateNum = parseInt(curYear + curMonth + curDate)
  const lastDate = new Date(year, month, 0).getDate()
  const isMonth = month > 0 ? month <= 12 : false
  const isDate = date > 0 ? date <= lastDate : false

  switch (inputDate.length) {
    case 0:
      setIsTrue(true)
      setHelperText(SIGN_UP_BIRTH.HELPER_TEXT)
      setIsBottomBtnDisabled(true)
      break;
    case 8:
      if (isMonth && isDate && parseInt(inputDate) <= curDateNum && year > 1900) {
        setIsTrue(true)
        setIsBottomBtnDisabled(false)
      } else {
        setIsTrue(false)
        setHelperText(SIGN_UP_BIRTH.HELPER_TEXT_INVALID)
        setIsBottomBtnDisabled(true)
      }
      break;
    default:
      setIsTrue(false)
      setHelperText(SIGN_UP_BIRTH.HELPER_TEXT)
      setIsBottomBtnDisabled(true)
      break;
  }
}
