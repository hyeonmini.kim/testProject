export function division(arr, n) {
  const len = arr?.length;
  const cnt = Math.floor(len / n) + (Math.floor(len % n) > 0 ? 1 : 0);
  const tmp = [];

  for (let i = 0; i < cnt; i++) {
    tmp.push(arr.splice(0, n));
  }

  return tmp;
}

export function replaceItemAtIndex(arr, index, newValue) {
  return [...arr?.slice(0, index), newValue, ...arr?.slice(index + 1)];
}