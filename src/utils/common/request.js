import axios from "axios";

const service = axios.create({
  timeout: 100000
})

service.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    console.error('### request error ', error);
    return Promise.reject(error);
  }
)

// response interceptor
service.interceptors.response.use(
  async (response) => {
    const {data} = response;

    // console.info('### API Response');
    // console.info('### response data: ', data)

    // switch (status) {
    //   case 404:
    //     if (typeof window !== 'undefined') {
    //       window.location.href = '/404'
    //     }
    //     console.error('### 404 Error ');
    //     // throw new NotFoundError()
    //     break;
    //
    //   case 403:
    //     if (typeof window !== 'undefined') {
    //       window.location.href = '/403'
    //     }
    //     console.error('### 403 Error ');
    //     // throw new ForbiddenError()
    //     break;
    //
    //   case 402:
    //     //토큰 만료시(Result Code 정의 필요)
    //     if (!excludePath.includes(config.url)) {
    //       console.info('### 토큰이 유효하지 않음');
    //       /**
    //        * 1. App에서 refresh/access token 받기
    //        *
    //        * 2. refresh token 이 있을경우
    //        *  2.1 Access Token 요청
    //        *
    //        * 3. refresh token 이 없는 경우
    //        *  3.1 로그인 페이지로 이동 + isAuth Context Data false 처리
    //        */
    //     }
    //     break;
    //
    //   case 500:
    //     if (typeof window !== 'undefined') {
    //       window.location.href = '/500'
    //     }
    //     console.error('### 500 Error ');
    //     // throw new AuthError()
    //     break;
    // }

    return data;
  },
  error => {
    //예외 에러 처리

    // errorController(error)

    // 서버 요청을 취소한 경우 또는 네트워크 에러인 경우
    // console.error('### response error', error);

    // // axios 에서 서버 요청을 취소한 경우에 실행.
    // if (axios.isCancel(error)) {
    //   return Promise.reject(error);
    // }
    return Promise.reject(error);
  }
);

export default service

