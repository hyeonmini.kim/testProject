import {PAGINATION} from "../constant/common/Pagination";

export function nvlPage(page) {
  if (!page) {
    return page = PAGINATION.PAGE
  } else {
    return page
  }
}

export function nvlPageSize(size) {
  if (!size) {
    return size = PAGINATION.PAGE_SIZE
  } else {
    return size
  }
}