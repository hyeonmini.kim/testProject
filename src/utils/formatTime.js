import {format, formatDistanceToNow, getTime} from "date-fns";

export function dateYYYYMMDD(date) {
  return format(new Date(date), 'yyyy.MM.dd');
}

export function fDate(date, newFormat) {
  const fm = newFormat || 'dd MMM yyyy';

  return date ? format(new Date(date), fm) : '';
}

export function fDateTime(date, newFormat) {
  const fm = newFormat || 'dd MMM yyyy p';

  return date ? format(new Date(date), fm) : '';
}

export function fTimestamp(date) {
  return date ? getTime(new Date(date)) : '';
}

export function fToNow(date) {
  return date
    ? formatDistanceToNow(new Date(date), {
      addSuffix: true,
    })
    : '';
}

export function checkBirthdayYYMMDD(birthDay) {
  let value = birthDay?.substring(0, 6);
  switch (value.length) {
    case 1:
      const regex1 = /(?:[0-9])/g;
      return regex1.test(value)
    case 2:
      const regex2 = /(?:[0-9]{2})/g;
      return regex2.test(value)
    case 3:
      const regex3 = /(?:[0-9]{2}(?:0|1))/g;
      return regex3.test(value)
    case 4:
      const regex4 = /(?:[0-9]{2}(?:0[1-9]|1[0-2]))/g;
      return regex4.test(value)
    case 5:
      const regex5 = /(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:[0-3]))/g;
      return regex5.test(value)
    case 6:
      const regex6 = /(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))/g;
      return regex6.test(value)
    default:
      return true
  }
}