export const logger = {
  info: (title, msg) => console.log(getLogMessage(title), getLogMessage(msg)),
  warn: (title, msg) => console.warn(getLogMessage(title), getLogMessage(msg)),
  debug: (title, msg) => console.debug(getLogMessage(title), getLogMessage(msg)),
  error: (title, msg) => console.error(getLogMessage(title), getLogMessage(msg)),
  dump: (title, msg) => console.debug('##@@ ' + getDumpLogMessage(title), getDumpLogMessage(msg)),
}

const getDumpLogMessage = (msg) => {
  if (!msg) return ''
  return typeof msg === "object"
    ? JSON.stringify(msg)
    : msg
}

const getLogMessage = (msg) => {
  if (!msg) return ''
  return typeof msg === "object"
    ? JSON.parse(JSON.stringify(msg))
    : msg
}

export const getFormDataLog = (formData) => {
  const formDataObj = {};
  formData.forEach((value, key) => (formDataObj[key] = value));
  return JSON.parse(JSON.stringify(formDataObj))
}