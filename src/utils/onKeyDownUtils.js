export const handleEnterPress = (e, onclick) => {
  if (e.key === 'Enter') {
    onclick(e)
  }
}