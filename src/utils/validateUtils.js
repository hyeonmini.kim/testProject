import {logger} from "./loggingUtils";
import * as yup from "yup";
import {REGEX_PATTERN} from "../constant/common/Regex";

export const idSchema = yup.string().matches(REGEX_PATTERN.SIGN_IN_ID)
export const passwordSchema = yup.string().matches(REGEX_PATTERN.PASSWORD)
export const emailSchema = yup.string().email()

export const paginationSchema = {
  page: yup.number().moreThan(-1).required(),
  size: yup.number().moreThan(0).required(),
}

export const isValidateParam = async (param, schema) => {
  if (!schema) return false
  return await schema.validate(param).then((result) => {
    return true
  }).catch((error) => {
    logger.error(error.message)
    return false
  })
}

export const slicePaginationByParam = (param, data) => {
  if (!data?.length) {
    return []
  }
  const paginationSize = param?.params?.size
  if (paginationSize) {
    return data?.slice(0, paginationSize)
  }
  return data
}