function byteArrayToBase64(byteArray) {
  let binary = '';
  let bytes = new Uint8Array(byteArray);
  let len = bytes.byteLength;
  for (let i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return window.btoa(binary);
}

function urlToFile(url, filename) {
  let arr = url.split(','), mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], filename, {type: mime});
}

export function bytesToImageFile(bytes) {
  return urlToFile('data:image/png;base64,' + byteArrayToBase64(bytes), 'image.png')
}

export function isImageFileType(filename) {
  if (!filename) return false
  const fileType = filename.slice(filename.lastIndexOf('.') + 1).toLowerCase();
  switch (fileType) {
    case 'jpg':
    case 'jpeg':
    case 'png':
    case 'gif':
    case 'bmp':
      return true
  }
  return false
}

export function isJsonFileType(filename) {
  if (!filename) return false
  const fileType = filename.slice(filename.lastIndexOf('.') + 1).toLowerCase();
  switch (fileType) {
    case 'json':
      return true
  }
  return false
}