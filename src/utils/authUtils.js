import {ENV_TYPE} from "../constant/common/Env";

export const changeNewsAgencyType = (telecom) => {
  switch (telecom) {
    case 'SKT':
      return '01'
    case 'KT':
      return '02'
    case 'LG U+':
      return '03'
    case 'SKT 알뜰폰':
      return '04'
    case 'KT 알뜰폰':
      return '05'
    case 'LG U+ 알뜰폰':
      return '06'
    default:
      return '00'
  }
}

export const changeGenderType = (idEnd) => {
  switch (idEnd) {
    case '1':
    case '3':
      return '1'
    case '2':
    case '4':
      return '2'
  }
}

// 생년월일 6자리 -> 8자리 변환
export const changeBirthFromYYMMDDToYYYYMMDD = (idFront, idEnd) => {
  switch (idEnd) {
    case '1':
    case '2':
      return '19' + idFront
    case '3':
    case '4':
      return '20' + idFront
  }
}

// 핸드폰번호에서 -(hyphen) 빼기
export const removeHyphenFromPhoneNumber = (phoneNumber) => {
  return phoneNumber.replaceAll('-', '')
}

// 환경에 따른 transaction sequence number 검증
export const checkTransactionSeqNumberByEnv = (transactionSeqNumber, env) => {
  if (env === ENV_TYPE.DEVELOP || env === ENV_TYPE.LOCAL) {
    return !transactionSeqNumber
  } else {
    return !!transactionSeqNumber
  }
}
