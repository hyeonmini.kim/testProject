export const parseRemoveBracket = (title) => {
  const subIndex = title.indexOf('[')
  return title.substring(0, subIndex === -1 ? title.length : subIndex - 1)
}