
const toastStyled = {
  color: '#FFFFFF',
  fontSize: '14px',
  fontWeight: '400',
  lineHeight: '20px',
  whiteSpace: 'pre-line',
  wordWrap: 'break-word',
  wordBreak: 'break-all',
}

const bottomToastStyled = {
  ...toastStyled,
  bottom: '93px',
  '@media all and (min-width: 440px) and (max-width: 820px)': {
    bottom: '93px',
  },
  '@media all and (min-width: 820px)': {
    bottom: '93px',
  },
}

const bottomDialogToastStyled = {
  ...toastStyled,
  bottom: '261px',
  '@media all and (min-width: 440px) and (max-width: 820px)': {
    bottom: '261px',
  },
  '@media all and (min-width: 820px)': {
    bottom: '261px',
  },
}

export const bottomHeaderToastOption = {
  duration: 3000,
  backgroundColor: '#222222',
  sx: {
    ...bottomToastStyled,
  }
}

export const bottomSystemErrorOption = {
  duration: 3000,
  backgroundColor: '#FF4842',
  sx: {
    ...bottomToastStyled,
  }
}

export const bottomDialogHeaderToastOption = {
  duration: 3000,
  backgroundColor: '#222222',
  sx: {
    ...bottomDialogToastStyled,
  }
}

export const bottomDialogHeaderErrorToastOption = {
  duration: 3000,
  backgroundColor: '#FF4842',
  sx: {
    ...bottomDialogToastStyled,
  }
}

export const desktopBottomHeaderToastOption = {
  duration: 3000,
  backgroundColor: '#FF4842',
  sx: {
    ...bottomToastStyled,
  }
}