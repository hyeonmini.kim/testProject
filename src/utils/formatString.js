import {format} from "date-fns";

export function getURIString(stringToReplace) {
  if (!stringToReplace) return ''
  return stringToReplace.replace(/[\{\}\[\]\/?,;:|\)*~`!^\-_+┼<>@\#$%&\'\"\\(\=]/gi, "").replace(/^\s+|\s+$/g, '').replace(/ +/gi, "-");
}

export function nvlString(string) {
  if (!string) {
    return ''
  }

  return string
}

export function removeLine(string) {
  if (!string) {
    return ''
  }
  return string.replace("\n", "");
}

export function removePercent(string) {
  if (!string) {
    return ''
  }
  return string.replace("%", "");
}

export function isMaxLine(string, count) {
  if (!string) {
    return false
  }
  return count <= string.split("\n")?.length - 1;
}

export function removeMultipleSpace(string) {
  if(!string) {
    return ''
  }
  return string.replace(/ +/g, ' ')
}

export function removeNBSP(string) {
  if (!string) {
    return ''
  }

  return string.replace(/\xA0/g, ' ')
}


export function getYYMMDDFromHyphen(string) {
  // const strArray=string?.split('-');
  const yy = string?.slice(2, 4)
  const mm = string?.slice(5, 7)
  const dd = string?.slice(8, 10)
  return `${yy}.${mm}.${dd}`
}

export function getYYMMDDFromOnlyNumber(string) {
  // const strArray=string?.split('-');
  const yy = string?.slice(2, 4)
  const mm = string?.slice(4, 6)
  const dd = string?.slice(6, 8)
  return `${yy}.${mm}.${dd}`
}

export function getYYMMDDFromDate(date) {
  if (!date) return ''
  try {
    return format(new Date(date), 'yyyy.MM.dd');
  } catch (e) {
    if (date?.length > 16) {
      let returnDate
      returnDate = date?.replaceAll("-", ".")
      returnDate = returnDate?.substring(0, 16)
      return returnDate
    }
  }
}

export function getMMDDFromDate(date) {
  if (!date) return ''
  try {
    return format(new Date(date), 'MM.dd');
  } catch (e) {
    if (date?.length > 16) {
      let returnDate
      returnDate = date?.replaceAll("-", ".")
      returnDate = returnDate?.substring(5, 10)
      return returnDate
    }
  }
}

export function getYYMMDDHHMMFromDate(date) {
  if (!date) return ''
  try {
    return format(new Date(date), 'yyyy.MM.dd HH:mm');
  } catch (e) {
    if (date?.length > 16) {
      let returnDate
      returnDate = date?.replaceAll("-", ".")
      returnDate = returnDate?.substring(0, 16)
      return returnDate
    }
  }
}

export function getHyphenPhoneNumber(string) {
  return string?.replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/, "$1-$2-$3").replace("--", "-");
}

export function isCorrectUrl(string) {
  if (string?.startsWith("http")) {
    return string
  } else {
    return "https://" + string
  }
}
