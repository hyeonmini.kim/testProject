import numeral from "numeral";

export function getNumberTenThousand(number) {
  if (!number) return '0'
  if (number < 10000) {
    return number.toLocaleString('ko-KR')
  }
  return (number / 10000).toFixed(1) + '만'
}

export function getNumberHundredThousand(number) {
  if (!number) return '0'
  if (number < 100000) {
    return number.toLocaleString('ko-KR')
  }
  return (number / 100000).toFixed(1) + '만'
}

export function getRandom(max) {
  return Math.floor(Math.random() * max)
}

export function nvlNumber(number) {
  if (!number) {
    return 0
  }
  return number
}

export const getNumberWithoutText = (number) => {
  if (typeof number === 'number') {
    return number
  } else {
    return 0
  }
}

export function fNumber(number) {
  return numeral(number).format();
}

export function fCurrency(number) {
  const format = number ? numeral(number).format('$0,0.00') : '';

  return result(format, '.00');
}

export function fPercent(number) {
  const format = number ? numeral(Number(number) / 100).format('0.0%') : '';

  return result(format, '.0');
}

export function fShortenNumber(number) {
  const format = number ? numeral(number).format('0.00a') : '';

  return result(format, '.00');
}

export function fData(number) {
  const format = number ? numeral(number).format('0.0 b') : '';

  return result(format, '.0');
}

function result(format, key = '.00') {
  const isInteger = format.includes(key);

  return isInteger ? format.replace(key, '') : format;
}

export function getFloatFixed(value, fixed) {
  return parseFloat(Math.round(value * 100) / 100).toFixed(fixed);
}