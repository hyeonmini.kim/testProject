import {ENV} from "../config";
import {ENV_TYPE} from "../constant/common/Env";
import {createInstance} from "@hackler/react-sdk";

const hackleClient = createInstance(getHackleSDKKey(), getHackleConfig())

export function getHackleClient() {
  return hackleClient
}

export function setHackleTrack(key, property) {
  if (!key) return

  hackleClient?.track({
    key: key,
    properties: property
  })
}

export function setHackleUserId(id) {
  if(!id) return

  hackleClient.setUser({
    userId: id,
  })
}

export function getHackleUser() {
  const user = hackleClient.getUser()
  return user
}

export function getHackleClientId() {
  const user = hackleClient.getUser()
  return user?.deviceId ? user?.deviceId : ''
}

function getHackleSDKKey() {
  let key = process.env.KEY_DEV_HACKLE
  switch (ENV) {
    case ENV_TYPE.PRODUCT:
      key = process.env.KEY_PRD_HACKLE
      break;
    case ENV_TYPE.STAGING:
    case ENV_TYPE.DEVELOP:
    case ENV_TYPE.LOCAL:
      key = process.env.key = process.env.KEY_DEV_HACKLE
      break;
  }
  return key
}

function getHackleConfig() {
  switch (ENV) {
    case ENV_TYPE.PRODUCT:
      return {
        debug: false
      }
    case ENV_TYPE.STAGING:
    case ENV_TYPE.DEVELOP:
    case ENV_TYPE.LOCAL:
      return {
        debug: true
      }
  }
}