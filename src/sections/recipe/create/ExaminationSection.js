import {Box, Button, Container, Divider, Fade} from "@mui/material";
import {useRecoilValue} from "recoil";
import {createRecipeSelector} from "../../../recoil/selector/recipe/createSelector";
import {RECIPE_CREATE_COMMON} from "../../../constant/recipe/Create";
import {forMyPageEntrySelector} from "../../../recoil/selector/auth/userSelector";
import CarouselSection from "./preview/CarouselSection";
import TopMainSection from "./preview/TopMainSection";
import IngredientsSection from "./preview/IngredientsSection";
import MakeStepsSection from "./preview/MakeStepsSection";
import TagSection from "./preview/TagSection";
import {createRecipeImages} from "../../../recoil/atom/recipe/create";
import {MainDialogLayout, sxFixedCenterMainLayout} from "../../../layouts/main/MainLayout";
import HeaderDialogLayout from "../../../layouts/recipe/create/HeaderDialogLayout";
import {clickBorderNone} from "../../../constant/common/Common";
import * as React from "react";
import {putRecipeStatusMutate} from "../../../api/recipeApi";
import {Z_INDEX} from "../../../constant/common/ZIndex";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";

export default function ExaminationSection({open, onModify, onClose}) {
  const recipe = useRecoilValue(createRecipeSelector)
  const imageFilePath = useRecoilValue(createRecipeImages)
  const user = useRecoilValue(forMyPageEntrySelector)

  const {mutate: mutatePutRecipeStatus} = putRecipeStatusMutate()
  const handleModify = () => {
    setHackleTrack(HACKLE_TRACK.RECIPE_PREVIEW_MODIFY)

    mutatePutRecipeStatus({
      recipe_key: recipe?.recipe_key
    }, {
      onSuccess: () => {
        onModify()
      }
    })
  };

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      onClose={onClose}
      TransitionComponent={Fade}
      sx={{'& .MuiDialog-paper': {mx: 0}}}
    >
      <HeaderDialogLayout
        title={RECIPE_CREATE_COMMON.TEXT.EXAMINATION}
        showClose={true}
        onClose={onClose}
      >
        <Container disableGutters maxWidth={'xs'} sx={{position: 'relative'}}>
          <CarouselSection images={imageFilePath} user={user}/>
          <Box sx={{
            mt: '-34px',
            pb: '112px',
            width: '100%',
            position: 'absolute',
            backgroundColor: 'white',
            borderRadius: '30px',
            zIndex: 99
          }}>
            <TopMainSection recipe={recipe} user={user} sx={{mt: '30px', mx: '20px'}}/>
            <Divider sx={{maxWidth: "xs", px: '0px', mt: '50px'}}/>
            <IngredientsSection recipe={recipe} sx={{px: '20px', pt: '100px', mt: '-50px'}}/>
            <Divider sx={{maxWidth: "xs", px: '0px', mt: '50px', mb: '20px'}}/>
            <MakeStepsSection steps={recipe?.recipe_order_list} sx={{px: '0px', my: '20px'}}/>
            {recipe?.recipe_tag_desc && <TagSection tags={recipe?.recipe_tag_desc?.split(',')}/>}
          </Box>
          <BottomFixedButton onClick={handleModify}/>
        </Container>
      </HeaderDialogLayout>
    </MainDialogLayout>
  )
}

function BottomFixedButton({onClick}) {
  return (
    <Box
      sx={{
        px: '20px',
        py: '20px',
        position: 'fixed',
        bgcolor: 'white',
        zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
        bottom: 0,
        left: 0,
        right: 0,
      }}
    >
      <Container maxWidth={'xs'} disableGutters>
        <Button
          tabIndex={0}
          disableFocusRipple
          fullWidth variant="contained"
          onClick={onClick}
          sx={{
            height: '52px',
            fontSize: '16px',
            fontWeight: '700',
            lineHeight: '28px',
            backgroundColor: 'primary.main',
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: 'primary.main',
            },
            ...clickBorderNone,
          }}
        >
          {RECIPE_CREATE_COMMON.BUTTON.MODIFY}
        </Button>
      </Container>
    </Box>
  )
}
