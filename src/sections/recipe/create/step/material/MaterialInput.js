import {Box, Container, Fade, FormControl, InputAdornment, MenuItem, Select, Typography} from "@mui/material";
import {useEffect, useState} from "react";
import {useRecoilState} from "recoil";
import {createRecipeMaterialsSelector, createRecipeMaterialTypeSelector,} from "../../../../../recoil/selector/recipe/createSelector";
import PropTypes from "prop-types";
import BottomFixedButton from "../../../../../components/recipe/create/button/BottomFixedButton";
import {RECIPE_CREATE_COMMON, RECIPE_CREATE_MATERIAL} from "../../../../../constant/recipe/Create";
import {EXPRESSION} from "../../../../../constant/common/Expression";
import {MATERIAL_TYPE} from "../../../../../constant/recipe/Material";
import {MainDialogLayout} from "../../../../../layouts/main/MainLayout";
import HeaderDialogLayout from "../../../../../layouts/recipe/create/HeaderDialogLayout";
import {setHackleTrack} from "../../../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../../../constant/common/Hackle";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";
import {handleEnterPress} from "../../../../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../../../../constant/common/Common";
import {ALT_STRING} from "../../../../../constant/common/AltString";

MaterialInput.propTypes = {
  open: PropTypes.bool,
  openIndex: PropTypes.number,
  onClose: PropTypes.func,
};

export default function MaterialInput({open, openIndex, onClose}) {
  const [item, setItem] = useState(null)
  const [quantity, setQuantity] = useState('')
  const [isDisabled, setIsDisabled] = useState(false)
  const [materials, setMaterials] = useRecoilState(createRecipeMaterialsSelector)
  const [materialType, setMaterialType] = useRecoilState(createRecipeMaterialTypeSelector)
  const [openMenu, setOpenMenu] = useState(false)

  useEffect(() => {
    if (materials?.length > openIndex) {
      setItem(materials[openIndex])
      setQuantity(materials[openIndex]?.recipe_ingredient_amount)
    }
  }, [open])

  const unitMinMax = (item) => {
    return {unit: 1, min: 0.1, max: 1000}
  }

  useEffect(() => {
    const {min, max} = unitMinMax(item)
    if (min > Number(quantity) || max < Number(quantity)) {
      setIsDisabled(true)
    } else {
      setIsDisabled(false)
    }
  }, [quantity])

  const handleMaterialChange = (event) => {
    let value = event.target.value?.toString()
    value = EXPRESSION.NUMBER_POINT.POINT_ONE(value, 4)
    setQuantity(value)
  };

  const handleUnitChange = (event) => {
    if (event.target.value) {
      setItem({...item, recipe_ingredient_amount: 0, recipe_ingredient_main_countunit: event.target.value})
      setQuantity('')
    }
  };

  const handleMaterialSave = () => {
    const newArray = [...materials]
    newArray[openIndex] = {...item, recipe_ingredient_amount: Number(quantity)}
    setMaterials(newArray)
    onClose()
  };

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.WRITE_UNIT)
  }, [])

  return (
    <>
      <MainDialogLayout
        fullScreen
        open={open}
        onClose={onClose}
        TransitionComponent={Fade}
        sx={{'& .MuiDialog-paper': {mx: 0}}}
      >
        <HeaderDialogLayout
          title={RECIPE_CREATE_MATERIAL.TITLE.INPUT}
          showClose={true}
          onClose={onClose}
        >
          {item &&
            <Container maxWidth={'xs'} sx={{mt: '60px', mb: '72px', px: '20px'}}>
              <Box
                display={'flex'}
                flexDirection={'column'}
                justifyContent={'center'}
                alignItems={'center'}
              >
                <Typography sx={{fontSize: '20px', fontWeight: 400, lineHeight: '24px', color: '#222222'}}>
                  {item?.recipe_ingredient_name}
                </Typography>
                <TextFieldWrapper
                  autoComplete='off'
                  value={quantity}
                  placeholder={"00"}
                  onChange={handleMaterialChange}
                  variant="standard"
                  type="number"
                  inputProps={{inputMode: 'decimal', tabIndex: 0, title: ALT_STRING.RECIPE_CREATE.INPUT_MATERIAL}}
                  InputProps={{
                    disableUnderline: true,
                    endAdornment:
                      <InputAdornment onKeyDown={(e) => handleEnterPress(e, () => setOpenMenu(!openMenu))} position="end" sx={{mr: '10px'}}>
                        <Select
                          tabIndex={0}
                          value={item?.recipe_ingredient_main_countunit}
                          onChange={handleUnitChange}
                          onClick={() => setOpenMenu(!openMenu)}
                          MenuProps={{open: openMenu}}
                          inputProps={{tabIndex: -1, title: ALT_STRING.RECIPE_CREATE.INPUT_MATERIAL}}
                          sx={{
                            fontSize: '34px',
                            letterSpacing: '-0.02em',
                            fontWeight: 700,
                            color: '#222222',
                            '.MuiInputBase-input': {p: 0},
                            '.MuiOutlinedInput-notchedOutline': {border: 0},
                            '&.Mui-focused .MuiOutlinedInput-notchedOutline': {border: 0}
                          }}
                        >
                          {item?.recipe_ingredient_countunit?.split(',')?.filter((unit) => unit?.length > 0).map((unit, index) => (
                            <MenuItem tabIndex={0} key={index} value={unit} sx={{...clickBorderNone}}>
                              {unit}
                            </MenuItem>
                          ))}
                        </Select>
                      </InputAdornment>,
                    sx: {
                      '& input': {
                        width: '88px',
                        fontSize: '34px',
                        lineHeight: '34px',
                        fontWeight: 700,
                        letterSpacing: '-0.02em',
                        color: '#222222',
                        textAlign: 'right'
                      },
                      '& input::placeholder': {
                        fontSize: '34px',
                        lineHeight: '34px',
                        fontWeight: 700,
                        letterSpacing: '-0.02em',
                        color: '#CCCCCC'
                      },
                    }
                  }}
                  sx={{
                    mt: '30px',
                  }}
                />
              </Box>
              {materialType === MATERIAL_TYPE.SEASONING &&
                <Box>
                  <Typography sx={{
                    mt: '105px',
                    fontSize: '14px',
                    fontWeight: 700,
                    lineHeight: '14px',
                    letterSpacing: '-0.02em',
                    color: '#888888'
                  }}>
                    {RECIPE_CREATE_MATERIAL.TEXT.HINT_TITLE}
                  </Typography>
                  <Typography sx={{
                    mt: '7px',
                    fontSize: '14px',
                    fontWeight: 400,
                    lineHeight: '20px',
                    letterSpacing: '-0.02em',
                    color: '#888888'
                  }}>
                    {RECIPE_CREATE_MATERIAL.TEXT.HINT_DESC}
                  </Typography>
                </Box>
              }
            </Container>
          }
          {open && <BottomFixedButton text={RECIPE_CREATE_COMMON.BUTTON.CONFIRM} isDisabled={isDisabled} onClick={handleMaterialSave}/>}
        </HeaderDialogLayout>
      </MainDialogLayout>

    </>
  )
}

