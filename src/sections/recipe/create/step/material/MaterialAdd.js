import {Box, Container, Divider, Fade, Grid, List, ListItem, ListItemButton, ListItemText, Typography} from "@mui/material";
import * as React from "react";
import {useEffect, useState} from "react";
import {useRecoilState, useRecoilValue} from "recoil";
import {createRecipeMaterialsSelector, createRecipeMaterialTypeSelector,} from "../../../../../recoil/selector/recipe/createSelector";
import PropTypes from "prop-types";
import HeaderSearchLayout from "../../../../../layouts/recipe/create/HeaderSearchLayout";
import {MATERIAL_TYPE, MATERIAL_USED} from "../../../../../constant/recipe/Material";
import ContainedTextButton from "../../../../../components/recipe/create/button/ContainedTextButton";
import {RECIPE_CREATE_MATERIAL} from "../../../../../constant/recipe/Create";
import {Z_INDEX} from "../../../../../constant/common/ZIndex";
import {getIngredientMutate} from "../../../../../api/recipeApi";
import {RECIPE_SEARCH_COMMON} from "../../../../../constant/recipe/Search";
import usePagination from "../../../../../hooks/usePagination";
import LoadingScreen from "../../../../../components/common/LoadingScreen";
import {MainDialogLayout} from "../../../../../layouts/main/MainLayout";
import {clickBorderNone} from "../../../../../constant/common/Common";
import {setHackleTrack} from "../../../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../../../constant/common/Hackle";

MaterialAdd.propTypes = {
  open: PropTypes.bool,
  openIndex: PropTypes.number,
  onClose: PropTypes.func,
};

export default function MaterialAdd({open, openIndex, onClose}) {
  const [search, setSearch] = useState('');
  const [showSearch, setShowSearch] = useState(false);
  const [materials, setMaterials] = useRecoilState(createRecipeMaterialsSelector)
  const [materialType, setMaterialType] = useRecoilState(createRecipeMaterialTypeSelector)

  const {
    param,
    items,
    setItems,
    setTarget,
    pageOffset,
    hasNextOffset,
    initPagination,
    isLoading,
    getPaginationItems,
  } = usePagination(getIngredientMutate)

  param.current = {
    params: {
      text: search
    }
  }

  useEffect(() => {
    if (search) {
      getPaginationItems()
    }
  }, [search])


  const handleSearch = (value) => {
    setSearch(value)
    setShowSearch(true)
  }

  const handleSelectMaterial = (e, item) => {
    let newArray
    const newItem = {
      ...item,
      recipe_ingredient_category: materialType
    }
    if (openIndex >= 0) {
      newArray = [...materials]
      newArray[openIndex] = newItem
    } else {
      newArray = [...materials, newItem]
    }
    setMaterials(newArray)
    onClose(e)
  }

  const onTrackIngredientSearch = () => {
    switch (materialType){
      case MATERIAL_TYPE.BASIC:
        setHackleTrack(HACKLE_TRACK.WRITE_STEP3_INGREDIENT1_SEARCH)
        break
      case MATERIAL_TYPE.SEASONING:
        setHackleTrack(HACKLE_TRACK.WRITE_STEP3_INGREDIENT2_SEARCH)
        break
    }
  }

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      onClose={onClose}
      TransitionComponent={Fade}
      sx={{'& .MuiDialog-paper': {mx: 0}, zIndex: Z_INDEX.DIALOG}}
    >
      <HeaderSearchLayout
        search={search}
        setSearch={setSearch}
        setShowSearch={setShowSearch}
        handleBackButton={onClose}
        handleSearchButton={handleSearch}
        onClick={onTrackIngredientSearch}
      >
        {isLoading && <LoadingScreen dialog/>}
        {
          showSearch
            ? items?.length
              ? <SearchResult items={items} hasNextOffset={hasNextOffset} setTarget={setTarget}
                              onClick={handleSelectMaterial}/>
              : isLoading
                ? <></>
                : <SearchNoResult/>
            : <SearchUsed onClick={handleSearch}/>
        }
      </HeaderSearchLayout>
    </MainDialogLayout>
  )
}

function SearchUsed({onClick}) {
  let usedMaterials = []
  const materialType = useRecoilValue(createRecipeMaterialTypeSelector)

  switch (materialType) {
    case MATERIAL_TYPE.BASIC:
    default:
      usedMaterials = MATERIAL_USED.BASIC
      break;
    case MATERIAL_TYPE.SEASONING:
      usedMaterials = MATERIAL_USED.SEASONING
      break;
  }
  const handleClick = (value) => {
    onClick(value)

    switch (materialType) {
      case MATERIAL_TYPE.BASIC:
        setHackleTrack(HACKLE_TRACK.WRITE_STEP3_INGREDIENT1_FREQUENTLY_USED,{
          ingredient: value
        })
        break;
      case MATERIAL_TYPE.SEASONING:
        setHackleTrack(HACKLE_TRACK.WRITE_STEP3_INGREDIENT2_FREQUENTLY_USED,{
          seasoning: value
        })
        break;
    }

  }
  return (
    <Container disableGutters maxWidth={'xs'} sx={{mt: '30px', mb: '112px', px: '20px'}}>
      <Typography sx={{fontSize: '14px', fontWeight: '700', lineHeight: '22px', color: '#888888'}}>
        {RECIPE_CREATE_MATERIAL.TEXT.FREQUENTLY_USED}
      </Typography>
      <Grid container sx={{mt: '-10px'}}>
        {usedMaterials.map((value, index) => (
          <Grid key={index} item sx={{mt: '20px', mr: '10px'}}>
            <ContainedTextButton text={value} onClick={()=>handleClick(value)}/>
          </Grid>
        ))}
      </Grid>
    </Container>
  )
}

function SearchResult({items, hasNextOffset, setTarget, onClick}) {
  return (
    <Container disableGutters maxWidth={'xs'} sx={{px: '0px'}}>
      <List>
        {items?.map((item, index) => (
          <Box key={index}>
            <ListItemButton tabIndex={0} onClick={(e) => onClick(e, item)} sx={{px: '20px', py: '0px', height: '64px', ...clickBorderNone}}>
              <ListItem sx={{px: 0}}>
                <ListItemText sx={{fontWeight: '400px', fontSize: '16px', color: '#666666'}}>
                  {item?.recipe_ingredient_name}
                </ListItemText>
              </ListItem>
            </ListItemButton>
            <Divider sx={{borderColor: '#F2F2F2'}}/>
          </Box>
        ))}
      </List>
      {hasNextOffset.current && <Box ref={setTarget}/>}
    </Container>
  )
}

function SearchNoResult() {
  return (
    <Box
      sx={{
        height: 'calc(100vh - 220px)',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <Typography align={'center'} sx={{fontSize: '16px', fontWeight: 500, color: '#666666', lineHeight: '24px'}}>
        {RECIPE_SEARCH_COMMON.TEXT.NO_SEARCH_RESULT_TITLE}
      </Typography>
      <Typography align={'center'} sx={{fontSize: '14px', fontWeight: 400, color: '#888888', lineHeight: '20px'}}>
        {RECIPE_SEARCH_COMMON.TEXT.NO_SEARCH_RESULT_SUB_TITLE}
      </Typography>
    </Box>
  )
}

