import {Container, Fade, Grid, Typography} from "@mui/material";
import PropTypes from "prop-types";
import {RECIPE_CREATE_MATERIAL, RECIPE_CREATE_MEASUREMENT_UNITS} from "../../../../../constant/recipe/Create";
import React, {useEffect} from "react";
import {Z_INDEX} from "../../../../../constant/common/ZIndex";
import {nvlString} from "../../../../../utils/formatString";
import {MainDialogLayout} from "../../../../../layouts/main/MainLayout";
import HeaderDialogLayout from "../../../../../layouts/recipe/create/HeaderDialogLayout";
import {setHackleTrack} from "../../../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../../../constant/common/Hackle";

MeasurementUnit.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
};

export default function MeasurementUnit({open, onClose}) {
  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.WRITE_STEP3_UNIT_MODIFY)
  }, [])

  return (
    <>
      <MainDialogLayout
        fullScreen
        open={open}
        onClose={onClose}
        TransitionComponent={Fade}
        sx={{'& .MuiDialog-paper': {mx: 0}, zIndex: Z_INDEX.DIALOG}}
      >
        <HeaderDialogLayout
          title={RECIPE_CREATE_MATERIAL.TITLE.MEASUREMENT_UNIT}
          showClose={true}
          onClose={onClose}
        >
          <Container disableGutters maxWidth={'xs'} sx={{mt: '20px', mb: '72px', px: '20px'}}>
            <Grid container sx={{border: '1px solid #E6E6E6', borderRadius: '4px'}}>
              {RECIPE_CREATE_MEASUREMENT_UNITS.map((item, index) => (
                <React.Fragment key={index}>
                  <Grid item xs={4.5} sx={{
                    py: '10px',
                    px: '12px',
                    borderRight: '1px solid #E6E6E6',
                    borderBottom: index < RECIPE_CREATE_MEASUREMENT_UNITS.length - 1 ? '1px solid #E6E6E6' : 0
                  }}>
                    <Typography sx={{
                      whiteSpace: 'pre-line',
                      fontSize: '16px',
                      fontWeight: '400',
                      lineHeight: '21px',
                      color: '#222222'
                    }}>
                      {nvlString(item?.title)}
                    </Typography>
                  </Grid>
                  <Grid item xs={7.5} sx={{
                    py: '10px',
                    px: '12px',
                    borderBottom: index < RECIPE_CREATE_MEASUREMENT_UNITS.length - 1 ? '1px solid #E6E6E6' : 0
                  }}>
                    <Typography sx={{
                      whiteSpace: 'pre-line',
                      fontSize: '16px',
                      fontWeight: '400',
                      lineHeight: '21px',
                      color: '#666666'
                    }}>
                      {nvlString(item?.desc)}
                    </Typography>
                  </Grid>
                </React.Fragment>
              ))}
            </Grid>
          </Container>
        </HeaderDialogLayout>
      </MainDialogLayout>
    </>
  )
}

