import {Box, Container, Fade, Typography} from "@mui/material";
import {useEffect, useState} from "react";
import {useRecoilState} from "recoil";
import {createRecipeMaterialsSelector, createRecipeMaterialTypeSelector,} from "../../../../../recoil/selector/recipe/createSelector";
import {DragDropContext, Droppable} from "react-beautiful-dnd";
import DragMaterials from "../../../../../components/recipe/create/drag/DragMaterials";
import PropTypes from "prop-types";
import BottomFixedButton from "../../../../../components/recipe/create/button/BottomFixedButton";
import {RECIPE_CREATE_COMMON, RECIPE_CREATE_MATERIAL} from "../../../../../constant/recipe/Create";
import {MainDialogLayout} from "../../../../../layouts/main/MainLayout";
import HeaderDialogLayout from "../../../../../layouts/recipe/create/HeaderDialogLayout";

MaterialEdit.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
};

export default function MaterialEdit({open, onClose}) {
  const [subTitle, setSubTitle] = useState('');
  const [newMaterials, setNewMaterials] = useState([])
  const [materials, setMaterials] = useRecoilState(createRecipeMaterialsSelector)
  const [materialType, setMaterialType] = useRecoilState(createRecipeMaterialTypeSelector)

  useEffect(() => {
    setSubTitle(materialType)
    if (materials) {
      setNewMaterials([...materials])
    }
  }, [open])

  const handleRemoveMaterial = (index) => {
    if (newMaterials.length > 0) {
      const newArray = [...newMaterials]
      newArray.splice(index, 1)
      setNewMaterials(newArray)
    }
  };

  const handleDragEndMaterial = (result) => {
    const {destination, source} = result;
    if (!destination) return;
    if (destination.droppableId === source.droppableId && destination.index === source.index) return;
    const newArray = [...newMaterials]
    const [removed] = newArray.splice(source.index, 1);
    newArray.splice(destination.index, 0, removed);
    setNewMaterials(newArray);
  };

  const handleSaveMaterial = () => {
    setMaterials(newMaterials)
    onClose()
  };

  return (
    <>
      <MainDialogLayout
        fullScreen
        open={open}
        onClose={onClose}
        TransitionComponent={Fade}
        sx={{'& .MuiDialog-paper': {mx: 0}}}
      >
        <HeaderDialogLayout
          title={RECIPE_CREATE_MATERIAL.TITLE.EDIT}
          showClose={true}
          onClose={onClose}
        >
          <Container disableGutters maxWidth={'xs'} sx={{mt: '30px', mb: '112px', px: '20px'}}>
            <Typography sx={{fontSize: '14px', fontWeight: '700', lineHeight: '22px', color: '#888888'}}>
              {subTitle}
            </Typography>
            {newMaterials.length > 0 &&
              <DragDropContext onDragEnd={handleDragEndMaterial}>
                <Droppable droppableId="materialId" direction="vertical" type="column">
                  {
                    (provided) => (
                      <Box
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        rowGap={'10px'}
                        display="grid"
                        sx={{mt: '10px'}}
                      >
                        {newMaterials.map((item, index) => (
                          <DragMaterials open={open} key={index} dragKey={'material'} index={index} item={item}
                                         onRemove={handleRemoveMaterial}/>
                        ))}
                        {provided.placeholder}
                      </Box>
                    )}
                </Droppable>
              </DragDropContext>
            }
          </Container>
          {open && <BottomFixedButton text={RECIPE_CREATE_COMMON.BUTTON.SAVE} onClick={handleSaveMaterial}/>}
        </HeaderDialogLayout>
      </MainDialogLayout>
    </>
  )
}