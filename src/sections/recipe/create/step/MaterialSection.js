import React, {useState} from 'react';
import {Box, Button, Grid, Stack, Typography,} from '@mui/material';
import {useRecoilState, useSetRecoilState} from "recoil";
import {
  createRecipeBasicMaterialsSelector,
  createRecipeMaterialTypeSelector,
  createRecipeQuantitySelector,
  createRecipeSeasoningMaterialsSelector,
  createShowMaterialAddSelector,
  createShowMaterialEditSelector,
  createShowMaterialInputSelector,
  createShowMeasurementUnitSelector
} from "../../../../recoil/selector/recipe/createSelector";
import CenterIncrementerButton from "../../../../components/recipe/create/button/CenterIncrementerButton";
import TitleIncrementerButton from "../../../../components/recipe/create/button/TitleIncrementerButton";
import MaterialAdd from "./material/MaterialAdd";
import MaterialEdit from "./material/MaterialEdit";
import {MATERIAL_BASE_TYPE, MATERIAL_TYPE} from "../../../../constant/recipe/Material";
import OutlinedTextButton from "../../../../components/recipe/create/button/OutlinedTextButton";
import MaterialInput from "./material/MaterialInput";
import MeasurementUnit from "./material/MeasurementUnit";
import {RECIPE_CREATE_COMMON, RECIPE_CREATE_MATERIAL} from "../../../../constant/recipe/Create";
import PropTypes from "prop-types";
import Big from 'big.js';
import {SvgEditIcon, SvgSmallPlusIcon} from "../../../../constant/icons/icons";
import {getPepperBaseIngredient, getSoyBaseIngredient} from "../../../../api/recipeApi";
import {setHackleTrack} from "../../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../../constant/common/Hackle";
import {clickBorderNone} from "../../../../constant/common/Common";

export default function MaterialSection({mainRef}) {
  const [basicMaterials, setBasicMaterials] = useRecoilState(createRecipeBasicMaterialsSelector)
  const [seasoningMaterials, setSeasoningMaterials] = useRecoilState(createRecipeSeasoningMaterialsSelector)

  const unitMinMax = (item) => {
    return {unit: 1, min: 0.1, max: 1000}
  }

  const getBaseMaterials = (type) => {
    switch (type) {
      case MATERIAL_TYPE.BASIC:
        return {baseMaterials: basicMaterials, setBaseMaterials: setBasicMaterials}
      case MATERIAL_TYPE.SEASONING:
        return {baseMaterials: seasoningMaterials, setBaseMaterials: setSeasoningMaterials}
    }
  }

  const unitPlus = (quantity) => {
    if (!quantity) return 0.1
    let unit
    if (quantity < 1) {
      unit = 0.1
    } else if (quantity < 10) {
      unit = 1
      quantity = Math.floor(quantity)
    } else if (quantity < 100) {
      unit = 10
      quantity = Math.floor(quantity / 10) * 10
    } else if (quantity < 1000) {
      unit = 100
      quantity = Math.floor(quantity / 100) * 100
    }
    return Math.min(1000, Big(quantity).plus(unit))
  }

  const unitMinus = (quantity) => {
    if (!quantity) return 0.1
    let unit
    if (quantity <= 1) {
      unit = 0.1
    } else if (quantity <= 10) {
      unit = 1
      quantity = Math.ceil(quantity)
    } else if (quantity <= 100) {
      unit = 10
      quantity = Math.ceil(quantity / 10) * 10
    } else if (quantity <= 1000) {
      unit = 100
      quantity = Math.ceil(quantity / 100) * 100
    }
    return Math.max(0.1, Big(quantity).minus(unit))
  }

  const handleMaterialQuantityIncrease = (type, item, index) => {
    const {baseMaterials, setBaseMaterials} = getBaseMaterials(type)
    if (!baseMaterials) return
    const {unit, max} = unitMinMax(item)
    if (item?.recipe_ingredient_amount < max) {
      let newItem = {...item}
      let newArray = [...baseMaterials]
      newItem.recipe_ingredient_amount = unitPlus(newItem?.recipe_ingredient_amount)
      newArray[index] = newItem
      setBaseMaterials(newArray)
    }
  }

  const handleMaterialQuantityDecrease = (type, item, index) => {
    const {baseMaterials, setBaseMaterials} = getBaseMaterials(type)
    if (!baseMaterials) return
    const {unit, min} = unitMinMax(item)
    if (item?.recipe_ingredient_amount > min) {
      let newItem = {...item}
      let newArray = [...baseMaterials]
      newItem.recipe_ingredient_amount = unitMinus(newItem?.recipe_ingredient_amount)
      newArray[index] = newItem
      setBaseMaterials(newArray)
    }
  }

  const setCreateRecipeMaterialType = useSetRecoilState(createRecipeMaterialTypeSelector)
  const [showMaterialEdit, setShowMaterialEdit] = useRecoilState(createShowMaterialEditSelector)
  const handleMaterialEdit = (type) => {
    switch (type){
      case MATERIAL_TYPE.BASIC:
        setHackleTrack(HACKLE_TRACK.WRITE_STEP3_INGREDIENT1_EDIT)
        break
      case MATERIAL_TYPE.SEASONING:
        setHackleTrack(HACKLE_TRACK.WRITE_STEP3_INGREDIENT2_EDIT)
        break
    }
    setShowMaterialEdit(true)
    setCreateRecipeMaterialType(type)
  }

  const [showMaterialAdd, setShowMaterialAdd] = useRecoilState(createShowMaterialAddSelector)
  const handleMaterialAdd = (type, index) => {
    setShowMaterialAdd(true)
    setCreateRecipeMaterialType(type)
    if (index >= 0) {
      setShowMaterialIndex(index)
    } else {
      setShowMaterialIndex(undefined)
    }
  }

  const [showMaterialInput, setShowMaterialInput] = useRecoilState(createShowMaterialInputSelector)
  const handleMaterialInput = (type, index) => {
    setShowMaterialInput(true)
    setCreateRecipeMaterialType(type)
    if (index >= 0) {
      setShowMaterialIndex(index)
    } else {
      setShowMaterialIndex(undefined)
    }
  }

  const [showMeasurementUnit, setShowMeasurementUnit] = useRecoilState(createShowMeasurementUnitSelector)
  const [showMaterialIndex, setShowMaterialIndex] = useState(0)


  return (
    <>
      <Box sx={{mt: '40px', mb: '112px'}}>

        {/* 계량기준 */}
        <QuantitySection/>

        {/* 기본재료 */}
        <BasicSection
          mainRef={mainRef}
          handleMaterialAdd={handleMaterialAdd}
          handleMaterialEdit={handleMaterialEdit}
          handleMaterialInput={handleMaterialInput}
          handleMaterialQuantityDecrease={handleMaterialQuantityDecrease}
          handleMaterialQuantityIncrease={handleMaterialQuantityIncrease}
        />

        {/* 양념재료 */}
        <SeasoningSection
          handleMaterialAdd={handleMaterialAdd}
          handleMaterialEdit={handleMaterialEdit}
          handleMaterialInput={handleMaterialInput}
          handleMaterialQuantityDecrease={handleMaterialQuantityDecrease}
          handleMaterialQuantityIncrease={handleMaterialQuantityIncrease}
        />
      </Box>

      {/* 재료편집 다이얼로그 */}
      {showMaterialEdit && <MaterialEdit open={showMaterialEdit} onClose={() => setShowMaterialEdit(false)}/>}

      {/* 재료추가 다이얼로그 */}
      {showMaterialAdd &&
        <MaterialAdd
          open={showMaterialAdd}
          openIndex={showMaterialIndex}
          onClose={(e) => {
            e.preventDefault()
            setShowMaterialAdd(false)
          }}/>}

      {/* 재료단위입력 다이얼로그 */}
      {showMaterialInput &&
        <MaterialInput open={showMaterialInput} openIndex={showMaterialIndex} onClose={() => setShowMaterialInput(false)}/>}

      {/* 계량단위보기 다이얼로그 */}
      {showMeasurementUnit &&
        <MeasurementUnit open={showMeasurementUnit} onClose={() => setShowMeasurementUnit(false)}/>}

    </>
  );
}

function QuantitySection({}) {
  const [quantity, setQuantity] = useRecoilState(createRecipeQuantitySelector)

  const handleQuantityIncrease = () => {
    if (quantity < 10) {
      setQuantity((prevQuantity) => prevQuantity + 1);
    }
  }
  const handleQuantityDecrease = () => {
    if (quantity > 1) {
      setQuantity((prevQuantity) => prevQuantity - 1);
    }
  }

  return (
    <Box
      rowGap={'10px'}
      display="grid"
    >
      <Typography sx={{fontSize: '14px', fontWeight: '700', lineHeight: '22px', color: '#888888'}}>
        {RECIPE_CREATE_MATERIAL.TEXT.MEASUREMENT}
      </Typography>
      <CenterIncrementerButton
        sx={{height: '56px'}}
        unit={RECIPE_CREATE_MATERIAL.TEXT.UNIT}
        quantity={quantity}
        onIncrease={handleQuantityIncrease}
        onDecrease={handleQuantityDecrease}
      />
    </Box>
  )
}

function BasicSection({
                        mainRef,
                        handleMaterialEdit,
                        handleMaterialAdd,
                        handleMaterialInput,
                        handleMaterialQuantityIncrease,
                        handleMaterialQuantityDecrease
                      }) {
  const [quantity, setQuantity] = useRecoilState(createRecipeQuantitySelector)
  const [basicMaterials, setBasicMaterials] = useRecoilState(createRecipeBasicMaterialsSelector)
  const [showMeasurementUnit, setShowMeasurementUnit] = useRecoilState(createShowMeasurementUnitSelector)

  const handleClick = () => {
    handleMaterialAdd(MATERIAL_TYPE.BASIC)
    setHackleTrack(HACKLE_TRACK.WRITE_STEP3_INGREDIENT1)
  }

  return (
    <>
      {/* 기본재료 타이틀 */}
      <Stack direction='row' justifyContent="space-between" alignItems='center' sx={{mt: '60px'}}>
        <Typography sx={{fontSize: '14px', fontWeight: '700', lineHeight: '22px', color: '#888888'}}>
          {RECIPE_CREATE_MATERIAL.TEXT.BASIC}
        </Typography>
        {basicMaterials?.length > 1 &&
          <EditButton onClick={() => handleMaterialEdit(MATERIAL_TYPE.BASIC)}/>
        }
      </Stack>

      {/* 기본재료 리스트 */}
      {basicMaterials?.length > 0 &&
        <Box
          rowGap={'10px'}
          display="grid"
          sx={{mt: '10px'}}
        >
          {basicMaterials?.map((item, index) => (
            <TitleIncrementerButton
              mainRef={mainRef}
              key={index}
              item={item}
              quantity={quantity}
              onTitleClick={() => handleMaterialAdd(MATERIAL_TYPE.BASIC, index)}
              onQuantityClick={() => handleMaterialInput(MATERIAL_TYPE.BASIC, index)}
              onIncrease={() => handleMaterialQuantityIncrease(MATERIAL_TYPE.BASIC, item, index)}
              onDecrease={() => handleMaterialQuantityDecrease(MATERIAL_TYPE.BASIC, item, index)}
              isMain={!index}
              sx={{height: '56px', m: 0}}
            />
          ))}
        </Box>
      }
      {/* 기본재료 입력하기 버튼 */}
      <AddButton onClick={handleClick}/>

      {/* 기본재료 계량단위보기 버튼 */}
      <Box sx={{mt: '10px', display: 'flex', justifyContent: 'flex-end'}}>
        <Button
          tabIndex={0}
          disableFocusRipple
          variant="text"
          onClick={() => setShowMeasurementUnit(true)}
          sx={{
            p: 0,
            fontSize: '14px',
            fontWeight: '400',
            lineHeight: '22px',
            color: '#666666',
            textDecorationLine: 'underline',
            backgroundColor: '#FFFFFF',
            '&:hover': {
              textDecorationLine: 'underline',
              backgroundColor: '#FFFFFF',
            },
            ...clickBorderNone,
          }}
        >
          {RECIPE_CREATE_MATERIAL.BUTTON.MEASUREMENT_UNIT}
        </Button>
      </Box>
    </>
  )
}

function SeasoningSection({
                            handleMaterialEdit,
                            handleMaterialAdd,
                            handleMaterialInput,
                            handleMaterialQuantityIncrease,
                            handleMaterialQuantityDecrease
                          }) {
  const [quantity, setQuantity] = useRecoilState(createRecipeQuantitySelector)
  const [seasoningMaterials, setSeasoningMaterials] = useRecoilState(createRecipeSeasoningMaterialsSelector)

  const {data: soyBase} = getSoyBaseIngredient()
  const {data: pepperBase} = getPepperBaseIngredient()

  const handleSeasoningMaterialClear = () => {
    setHackleTrack(HACKLE_TRACK.RECIPE_WRITE_STEP3_RESET)
    setSeasoningMaterials([])
  };

  const handleSeasoningMaterialBase = (base) => {
    let base_type
    switch (base) {
      case MATERIAL_BASE_TYPE.SOY:
        setHackleTrack(HACKLE_TRACK.RECIPE_WRITE_STEP3_SOYSAUCE)
        base_type = soyBase ? soyBase : []
        break;
      case MATERIAL_BASE_TYPE.PEPPER:
        setHackleTrack(HACKLE_TRACK.RECIPE_WRITE_STEP3_REDPEPPER)
        base_type = pepperBase ? pepperBase : []
        break;
    }
    if (base_type) {
      setSeasoningMaterials(base_type)
    }
  }
  const handleClick = () => {
    handleMaterialAdd(MATERIAL_TYPE.SEASONING)
    setHackleTrack(HACKLE_TRACK.WRITE_STEP3_INGREDIENT2)
  }

  return (
    <>
      {/* 양념재료 타이틀 */}
      <Stack direction='row' justifyContent="space-between" alignItems='center' sx={{mt: '60px', height: '24px'}}>
        <Box sx={{fontSize: '14px', fontWeight: 700, lineHeight: '22px', color: '#888888'}}>
          {RECIPE_CREATE_MATERIAL.TEXT.SEASONING}
          <Box sx={{
            ml: '5px',
            fontSize: '14px',
            fontWeight: 400,
            lineHeight: '22px',
            color: '#888888',
            display: 'inline'
          }}>{RECIPE_CREATE_COMMON.TEXT.OPTION} </Box>
        </Box>
        {seasoningMaterials?.length > 1 &&
          <EditButton onClick={() => handleMaterialEdit(MATERIAL_TYPE.SEASONING)}/>
        }
      </Stack>

      {/* 양념재료 베이스 선택 버튼 */}
      <Grid container direction="row" columnSpacing={'10px'} sx={{mt: '10px'}}>
        <Grid item xs={3.5}>
          <OutlinedTextButton text={RECIPE_CREATE_MATERIAL.BUTTON.CLEAR} isClear
                              onClick={handleSeasoningMaterialClear}/>
        </Grid>
        <Grid item xs={4}>
          <OutlinedTextButton text={RECIPE_CREATE_MATERIAL.BUTTON.SOY_BASE}
                              onClick={() => handleSeasoningMaterialBase(MATERIAL_BASE_TYPE.SOY)}/>
        </Grid>
        <Grid item xs={4.5}>
          <OutlinedTextButton text={RECIPE_CREATE_MATERIAL.BUTTON.PEPPER_BASE}
                              onClick={() => handleSeasoningMaterialBase(MATERIAL_BASE_TYPE.PEPPER)}/>
        </Grid>
      </Grid>

      {/* 양념재료 리스트 */}
      {seasoningMaterials?.length > 0 &&
        <Box rowGap={'10px'} display="grid" sx={{mt: '20px'}}>
          {seasoningMaterials?.map((item, index) => (
            <TitleIncrementerButton
              key={index}
              item={item}
              quantity={quantity}
              onTitleClick={() => handleMaterialAdd(MATERIAL_TYPE.SEASONING, index)}
              onQuantityClick={() => handleMaterialInput(MATERIAL_TYPE.SEASONING, index)}
              onIncrease={() => handleMaterialQuantityIncrease(MATERIAL_TYPE.SEASONING, item, index)}
              onDecrease={() => handleMaterialQuantityDecrease(MATERIAL_TYPE.SEASONING, item, index)}
              sx={{width: '100%', height: '56px', m: 0}}
            />
          ))}
        </Box>
      }
      {/* 양념재료 입력하기 버튼 */}
      <AddButton onClick={handleClick}/>
    </>
  )
}

function AddButton({onClick}) {
  return (
    <Box sx={{pt: '20px'}}>
      <Button
        tabIndex={0}
        fullWidth
        size="large"
        variant="contained"
        onClick={onClick}
        disableFocusRipple
        sx={{
          color: '#222222',
          fontSize: '16px',
          fontWeight: '500',
          lineHeight: '26px',
          borderRadius: '8px',
          backgroundColor: '#F5F5F5',
          '&:hover': {
            backgroundColor: '#F5F5F5',
          },
          ...clickBorderNone,
        }}
      >
        <Stack direction={'row'} sx={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
          <SvgSmallPlusIcon/>
          <Typography sx={{fontWeight: '500', fontSize: '16px', lineHeight: '23px'}}>
            {RECIPE_CREATE_MATERIAL.BUTTON.MATERIAL_ADD}
          </Typography>
        </Stack>
      </Button>
    </Box>
  )
}

EditButton.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
};

function EditButton({text = '편집하기', onClick}) {
  return (
    <Button
      tabIndex={0}
      disableFocusRipple
      size="small"
      onClick={onClick}
      sx={{
        p: 0,
        backgroundColor: '#FFFFFF',
        '&:hover': {
          backgroundColor: '#FFFFFF',
        },
        ...clickBorderNone,
      }}
    >
      <Stack direction={'row'}>
        <SvgEditIcon/>
        <Typography
          align='right'
          sx={{fontSize: '14px', fontWeight: '400', lineHeight: '22px', color: '#666666'}}
        >
          {text}
        </Typography>
      </Stack>
    </Button>
  )
}