import {Box, Button, Grid, Typography,} from '@mui/material';
import {useRecoilState} from "recoil";
import {createRecipeStepsSelector, createTagSelector} from "../../../../recoil/selector/recipe/createSelector";
import {RECIPE_CREATE_STEP} from "../../../../constant/recipe/Create";
import {DragDropContext, Droppable} from "react-beautiful-dnd";
import DragStep from "../../../../components/recipe/create/drag/DragStep";
import React, {useEffect} from "react";
import {toastMessageSelector} from "../../../../recoil/selector/common/toastSelector";
import {clickBorderNone, TOAST_TYPE} from "../../../../constant/common/Common";
import {EXPRESSION} from "../../../../constant/common/Expression";
import {SvgSmallPlusIcon} from "../../../../constant/icons/icons";
import TextFieldWrapper from "../../../../components/common/TextFieldWrapper";
import {setHackleTrack} from "../../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../../constant/common/Hackle";
import {ALT_STRING} from "../../../../constant/common/AltString";

export default function StepSection() {
  return (
    <Box sx={{mt: '40px', mb: '112px'}}>

      {/* 스텝 입력 */}
      <StepsInputSection/>

      {/* 태그 입력 */}
      <TagInputSection/>
    </Box>
  );
}

function StepsInputSection() {
  const [steps, setSteps] = useRecoilState(createRecipeStepsSelector)
  const [toastMessage, setToastMessage] = useRecoilState(toastMessageSelector)

  useEffect(() => {
    if (steps?.length < 2) {
      const newArray = [RECIPE_CREATE_STEP.DEFAULT, RECIPE_CREATE_STEP.DEFAULT]
      steps?.forEach((item, index) => newArray[index] = item)
      setSteps(newArray)
    }
  }, [steps])

  const handleDragEndStep = (result) => {
    const {destination, source} = result;
    if (!destination) return;
    if (destination.droppableId === source.droppableId && destination.index === source.index) return;
    const newArray = [...steps]
    const [removed] = newArray.splice(source.index, 1);
    newArray.splice(destination.index, 0, removed);
    setSteps(newArray)
  }

  const handlerAddStep = () => {
    if (steps.length < 20) {
      const newArray = [...steps]
      newArray.push(RECIPE_CREATE_STEP.DEFAULT)
      setSteps(newArray)
      setHackleTrack(HACKLE_TRACK.WRITE_STEP4_STEP)
    } else {
      setToastMessage({type: TOAST_TYPE.BOTTOM_HEADER, message: RECIPE_CREATE_STEP.TEXT.STEP_MAX_TOAST})
    }
  };

  return (
    <>
      {/* 스텝 리스트 */}
      <DragDropContext onDragEnd={handleDragEndStep}>
        <Droppable droppableId="imageid" direction="vertical" type="column">
          {
            (provided) => (
              <Grid
                {...provided.droppableProps}
                ref={provided.innerRef}
                container
                rowGap={'60px'}
              >
                {steps?.map((item, index) => (
                  <DragStep key={index} dragKey={'step'} index={index}/>
                ))}
                {provided.placeholder}
              </Grid>
            )}
        </Droppable>
      </DragDropContext>

      {/* 스텝 추가하기 */}
      <Button
        tabIndex={0}
        disableFocusRipple
        fullWidth
        size="large"
        variant="contained"
        onClick={() => handlerAddStep()}
        sx={{
          mt: '20px',
          color: '#222222',
          borderRadius: '8px',
          backgroundColor: '#F5F5F5',
          '&:hover': {
            backgroundColor: '#F5F5F5',
          },
          ...clickBorderNone,
        }}
      >
        <SvgSmallPlusIcon/>
        <Typography sx={{fontWeight: '500', fontSize: '16px', lineHeight: '23px'}}>
          {RECIPE_CREATE_STEP.BUTTON.STEP_ADD}
        </Typography>
      </Button>
    </>
  )
}

function TagInputSection() {
  const [tag, setTag] = useRecoilState(createTagSelector)

  const handleChange = (event) => {
    let value = EXPRESSION.EXCLUDE_SPECIAL.ALLOW_COMMA(event.target.value)
    value = value.substring(0, 30);
    setTag(value)
  };

  return (
    <Box sx={{mt: '60px'}}>
      <Box sx={{fontSize: '14px', fontWeight: 700, lineHeight: '22px', color: '#888888'}}>
        {RECIPE_CREATE_STEP.TEXT.TAG}
        <Box
          sx={{ml: '5px', fontSize: '14px', fontWeight: 400, lineHeight: '22px', color: '#888888', display: 'inline'}}>
          {RECIPE_CREATE_STEP.TEXT.OPTION}
        </Box>
      </Box>

      <TextFieldWrapper
        autoComplete='off'
        fullWidth={true}
        placeholder={RECIPE_CREATE_STEP.TEXT.TAG_PLACEHOLDER}
        variant="outlined"
        value={tag}
        onChange={handleChange}
        inputProps={{title: ALT_STRING.RECIPE_CREATE.INPUT_TAG}}
        sx={{
          mt: '10px',
          '& input': {fontSize: '16px', fontWeight: '500', color: '#222222'},
          '& input::placeholder': {fontSize: '16px', fontWeight: '500', color: '#CCCCCC'},
        }}
      />
    </Box>
  )
}