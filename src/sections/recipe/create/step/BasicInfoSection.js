import {Box, Stack} from '@mui/material';
import {useRecoilState, useSetRecoilState} from "recoil";
import {
  createRecipeCategorySelector,
  createRecipeDeleteImagesSelector,
  createRecipeDescSelector,
  createRecipeImagesSelector,
  createRecipeNameSelector,
} from "../../../../recoil/selector/recipe/createSelector";
import {RECIPE_CREATE_BASIC, RECIPE_CREATE_COMMON,} from "../../../../constant/recipe/Create";
import ToggleGroupButton from "../../../../components/recipe/create/button/ToggleGroupButton";
import MultipleImage from "../../../../components/recipe/create/image/MultipleImage";
import {DragDropContext, Droppable} from "react-beautiful-dnd";
import DragImage from "../../../../components/recipe/create/drag/DragImage";
import {toastMessageSelector} from "../../../../recoil/selector/common/toastSelector";
import {IMAGE_COMPRESSION_OPTIONS, TOAST_TYPE} from "../../../../constant/common/Common";
import {callOpenNativeGalleryChannel} from "../../../../channels/photoChannel";
import {isNative} from "../../../../utils/envUtils";
import {hideScrollbarX} from "../../../../utils/cssStyles";
import {bytesToImageFile} from "../../../../utils/fileUtils";
import imageCompression from "browser-image-compression";
import {useState} from "react";
import LoadingScreen from "../../../../components/common/LoadingScreen";
import TextFieldWrapper from "../../../../components/common/TextFieldWrapper";
import {ALT_STRING} from "../../../../constant/common/AltString";

export default function BasicInfoSection({}) {
  const [recipeName, setRecipeName] = useRecoilState(createRecipeNameSelector)
  const [recipeDesc, setRecipeDesc] = useRecoilState(createRecipeDescSelector)
  const [category, setCategory] = useRecoilState(createRecipeCategorySelector)

  const handleRecipeName = (event) => {
    let value = event.target.value?.substring(0, 30);
    setRecipeName(value);

  };
  const handleRecipeDesc = (event) => {
    let value = event.target.value?.substring(0, 50);
    setRecipeDesc(value);
  };

  return (
    <Box sx={{mt: '40px', mb: '112px'}}>

      {/* 대표사진 */}
      <ImageSection/>

      {/* 요리 명 */}
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        placeholder={RECIPE_CREATE_BASIC.TEXT.NAME_PLACEHOLDER}
        value={recipeName}
        variant="standard"
        onChange={handleRecipeName}
        inputProps={{title: ALT_STRING.RECIPE_CREATE.INPUT_RECIPE_NAME}}
        InputProps={{
          sx: {
            mt: '30px',
            height: '52px',
            '& input': {fontSize: '16px', fontWeight: '400', lineHeight: '24px', color: '#222222'},
            '& input::placeholder': {fontSize: '16px', fontWeight: '500', lineHeight: '24px', color: '#CCCCCC'},
          }
        }}
      />

      {/* 요리 설명 */}
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        placeholder={RECIPE_CREATE_BASIC.TEXT.DESC_PLACEHOLDER}
        value={recipeDesc}
        variant="standard"
        onChange={handleRecipeDesc}
        inputProps={{title: ALT_STRING.RECIPE_CREATE.INPUT_RECIPE_DETAIL}}
        InputProps={{
          sx: {
            mt: '10px',
            height: '52px',
            '& input': {fontSize: '16px', fontWeight: '400', lineHeight: '24px', color: '#222222'},
            '& input::placeholder': {fontSize: '16px', fontWeight: '500', color: '#CCCCCC'},
          }
        }}
      />

      {/* 카테고리 */}
      <Box rowGap={'10px'} columnGap={2} display="grid" sx={{mt: '60px'}}>
        <ToggleGroupButton title={RECIPE_CREATE_BASIC.TEXT.CATEGORY_GROUP}
                           items={RECIPE_CREATE_BASIC.CATEGORY_GROUP} selected={category} onChange={setCategory}/>
      </Box>

    </Box>
  );
}

function ImageSection({}) {
  const [files, setFiles] = useRecoilState(createRecipeImagesSelector)
  const setToastMessage = useSetRecoilState(toastMessageSelector);
  const [isCompress, setIsCompress] = useState(false)

  const isMaxImage = (imageCount) => {
    if (imageCount > 5) {
      setToastMessage({type: TOAST_TYPE.BOTTOM_HEADER, message: RECIPE_CREATE_COMMON.TOAST.IMAGE_MAX})
      return true
    }
    return false
  }
  const setFilesWithImages = async (images) => {
    if (isMaxImage(images.length + files.length)) return
    let newArray = []
    let newArrayOrder = []

    if (isNative()) {
      images.map((file) => {
        file = bytesToImageFile(file)
        const newFile = {
          index: null,
          image_file_key: '',
          image_file_path: Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        }
        newArrayOrder.push(files.length + newArray.length)
        newArray.push(newFile)
      });
    } else {
      setIsCompress(true)
      for (const image of images) {
        const newFile = await webCompress(image)
        newArrayOrder.push(files.length + newArray.length)
        newArray.push(newFile)
      }
      setIsCompress(false)
    }
    setFiles([...files, ...newArray])
  }

  const webCompress = async (file) => {
    const resizingBlob = await imageCompression(file, IMAGE_COMPRESSION_OPTIONS);
    file = new File([resizingBlob], file.name, {type: file.type});

    return {
      index: null,
      image_file_key: '',
      image_file_path: Object.assign(file, {
        preview: URL.createObjectURL(file),
      })
    }
  }

  const handleGalleryChannel = () => {
    if (isMaxImage(files.length + 1)) return
    callOpenNativeGalleryChannel(files.length, 5, setFilesWithImages)
  }

  const handleDrop = (acceptedFiles) => {
    setFilesWithImages(acceptedFiles)
  }

  return (
    <>
      <Stack direction={'row'} spacing={2}>
        {isNative()
          ? <MultipleImage onClick={handleGalleryChannel}/>
          : <MultipleImage onDrop={handleDrop}/>
        }
        <DragImageContext/>
      </Stack>
      {isCompress && <LoadingScreen/>}
    </>
  )
}

function DragImageContext() {
  const [files, setFiles] = useRecoilState(createRecipeImagesSelector)
  const [deleteRecipeImages, setDeleteRecipeImages] = useRecoilState(createRecipeDeleteImagesSelector)

  const handleRemoveFile = (index) => {
    if (files?.length > 0) {
      const newArray = [...files]
      const [removed] = newArray.splice(index, 1)
      if (removed?.image_file_key) {
        setDeleteRecipeImages([...deleteRecipeImages, removed])
      }
      setFiles(newArray)
    }
  };

  const handleDragEndFile = (result) => {
    const {destination, source} = result;
    if (!destination) return;
    if (destination.droppableId === source.droppableId && destination.index === source.index) return;
    const newArray = [...files]
    const [removed] = newArray.splice(source.index, 1);
    newArray.splice(destination.index, 0, removed);
    setFiles(newArray);
  };

  return (
    <DragDropContext onDragEnd={handleDragEndFile}>
      <Droppable droppableId="imageId" direction="horizontal" type="column">
        {
          (provided) => (
            <Stack
              {...provided.droppableProps}
              ref={provided.innerRef}
              spacing={1}
              direction="row"
              alignItems="flex-start"
              sx={{
                overflowY: 'hidden',
                ...hideScrollbarX,
              }}
            >
              {
                files?.map((item, index) => (
                  <DragImage key={index} dragKey={'image'} index={index} file={item?.image_file_path}
                             onRemove={handleRemoveFile}/>
                ))
              }
              {provided.placeholder}
            </Stack>
          )}
      </Droppable>
    </DragDropContext>
  )
}