import {Container, Fade, Typography} from "@mui/material";
import React from "react";
import {RECIPE_CREATE_COMMON} from "../../../../constant/recipe/Create";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import HeaderDialogLayout from "../../../../layouts/recipe/create/HeaderDialogLayout";

export default function ReturnSection({open, message, onClose}) {

  return (
    <>
      <MainDialogLayout
        fullScreen
        open={open}
        onClose={onClose}
        TransitionComponent={Fade}
        sx={{'& .MuiDialog-paper': {mx: 0}, zIndex: Z_INDEX.DIALOG}}
      >
        <HeaderDialogLayout
          title={RECIPE_CREATE_COMMON.TEXT.RETURN}
          showClose={true}
          onClose={onClose}
        >
          <Container disableGutters maxWidth={'xs'} sx={{mt: '30px', px: '20px'}}>
            <Typography sx={{whiteSpace: 'pre-wrap'}}>
              {message}
            </Typography>
          </Container>
        </HeaderDialogLayout>
      </MainDialogLayout>
    </>
  )
}

