import {Box, Stack, Typography} from '@mui/material';
import {useRecoilState} from "recoil";
import {
  createRecipeCookTimeSelector,
  createRecipeDifficultySelector,
  createRecipeHealthNoteSelector,
  createRecipeMonthlyAgeSelector,
} from "../../../../recoil/selector/recipe/createSelector";
import {RECIPE_CREATE_ADDITIONAL, RECIPE_CREATE_COMMON,} from "../../../../constant/recipe/Create";
import ToggleGroupButton from "../../../../components/recipe/create/button/ToggleGroupButton";
import {forMyPageEntrySelector} from "../../../../recoil/selector/auth/userSelector";
import TextFieldWrapper from "../../../../components/common/TextFieldWrapper";
import {removeLine} from "../../../../utils/formatString";
import {ALT_STRING} from "../../../../constant/common/AltString";
// ----------------------------------------------------------------------

AdditionalSection.propTypes = {};

export default function AdditionalSection() {
  const [cookTime, setCookTime] = useRecoilState(createRecipeCookTimeSelector)
  const [difficulty, setDifficulty] = useRecoilState(createRecipeDifficultySelector)
  const [monthlyAge, setMonthlyAge] = useRecoilState(createRecipeMonthlyAgeSelector)
  const [healthNote, setHealthNote] = useRecoilState(createRecipeHealthNoteSelector)
  const [user, setUser] = useRecoilState(forMyPageEntrySelector);

  const handleHealthNote = (event) => {
    let value = event.target.value?.substring(0, 200);
    setHealthNote(removeLine(value));
  };

  return (
    <Box sx={{mt: '40px', mb: '112px'}}>

      {/* 소요시간 */}
      <Box rowGap={'10px'} columnGap={2} display="grid">
        <ToggleGroupButton title={RECIPE_CREATE_ADDITIONAL.TEXT.COOK_TIME}
                           items={RECIPE_CREATE_ADDITIONAL.COOK_TIME_GROUP} selected={cookTime}
                           onChange={setCookTime}/>
      </Box>

      {/* 난이도 */}
      <Box rowGap={'10px'} columnGap={2} display="grid" sx={{pt: '50px'}}>
        <ToggleGroupButton title={RECIPE_CREATE_ADDITIONAL.TEXT.DIFFICULTY}
                           items={RECIPE_CREATE_ADDITIONAL.DIFFICULTY_GROUP} selected={difficulty}
                           onChange={setDifficulty}/>
      </Box>

      {/* 월령 */}
      <Box rowGap={'10px'} columnGap={2} display="grid" sx={{pt: '50px'}}>
        <ToggleGroupButton title={RECIPE_CREATE_ADDITIONAL.TEXT.MONTHLY_AGE}
                           items={RECIPE_CREATE_ADDITIONAL.MONTHLY_AGE_GROUP} selected={monthlyAge}
                           showSubTitle={true} onChange={setMonthlyAge}/>
      </Box>

      {/* 건강노트 */}
      <Box
        rowGap={'10px'}
        columnGap={2}
        display="grid"
        sx={{pt: '60px'}}
      >
        <Stack direction="row" spacing={'5px'}>
          <Typography sx={{fontSize: '14px', fontWeight: '700', lineHeight: '22px', color: '#888888'}}>
            {RECIPE_CREATE_ADDITIONAL.TEXT.HEALTH_NOTE(user?.nickname)}
          </Typography>
          <Typography sx={{fontSize: '14px', fontWeight: '400', lineHeight: '22px', color: '#888888'}}>
            {RECIPE_CREATE_COMMON.TEXT.OPTION}
          </Typography>
        </Stack>
        <TextFieldWrapper
          autoComplete='off'
          placeholder={RECIPE_CREATE_ADDITIONAL.TEXT.HEALTH_NOTE_PLACEHOLDER}
          value={healthNote}
          multiline
          minRows={5}
          onChange={handleHealthNote}
          inputProps={{title: ALT_STRING.RECIPE_CREATE.HEALTH_NOTE}}
          InputProps={{
            sx: {
              "& textarea": {
                fontSize: '16px', fontWeight: '500', lineHeight: '24px', color: '#222222'
              },
              '& textarea::placeholder': {
                fontSize: '16px', fontWeight: '500', lineHeight: '24px', color: '#CCCCCC'
              }
            }
          }}
        />
      </Box>
    </Box>
  );
}
