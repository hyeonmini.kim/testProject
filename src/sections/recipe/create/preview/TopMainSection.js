import {Box, Stack, Typography} from "@mui/material";
import PropTypes from "prop-types";
import {HEALTH_NOTE_TEXT} from "../../../../constant/recipe/detail/DetailConstants";
import TextMaxLineWithAccordion from "../../../../components/recipe/detail/text-max-line-with-accordion/TextMaxLineWithAccordion";
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import * as React from "react";
import {nvlString} from "../../../../utils/formatString";
import {REPLACE_RECIPE_AGE_GROUP} from "../../../../constant/recipe/Create";

TopMainSection.propTypes = {
  recipe: PropTypes.object,
  user: PropTypes.object,
  sx: PropTypes.object,
};

export default function TopMainSection({recipe, user, sx}) {
  if (!recipe) return
  if (!user) return

  return (
    <Box sx={{display: 'grid', ...sx}}>
      <Typography variant={'h1_24_b'} sx={{color: '#000000'}}>
        {nvlString(recipe?.recipe_name)}
      </Typography>

      <Typography variant={'b1_16_r'} sx={{mt: '8px', color: '#222222'}}>
        {nvlString(recipe?.recipe_desc)}
      </Typography>

      <Stack
        direction="row"
        alignItems="center"
        sx={{mt: '16px'}}
      >
        <SvgCommonIcons type={ICON_TYPE.COOK_TIME} sx={{mr: '3px'}}/>
        <Typography variant={'b2_14_r_1l'} sx={{color: '#666666'}}>
          {nvlString(recipe?.recipe_lead_time)}
        </Typography>

        <SvgCommonIcons type={ICON_TYPE.DOT} sx={{mx: '10px'}}/>

        <SvgCommonIcons type={ICON_TYPE.DIFFICULTY} sx={{mr: '3px'}}/>
        <Typography variant={'b2_14_r_1l'} sx={{color: '#666666'}}>
          {nvlString(recipe?.recipe_level)}
        </Typography>

        <SvgCommonIcons type={ICON_TYPE.DOT} sx={{mx: '10px'}}/>

        <SvgCommonIcons type={ICON_TYPE.BABY_FOOD} sx={{mr: '3px'}}/>
        <Typography variant={'b2_14_r_1l'} sx={{color: '#666666'}}>
          {nvlString(REPLACE_RECIPE_AGE_GROUP(recipe?.recipe_babyfood_step))}
        </Typography>
      </Stack>

      {recipe?.recipe_health_note &&
        <Box sx={{mt: '40px', pt: '25px', pb: '20px', px: '20px', backgroundColor: '#FAFAFA', borderRadius: '8px'}}>
          <Stack
            direction="row"
            sx={{
              alignItems: 'center',
            }}
          >
            <SvgCommonIcons type={ICON_TYPE.PENCIL} sx={{mr: '10px'}}/>
            <Typography variant={'b1_16_b_1l'} sx={{lineHeight: '20px', color: '#222222'}}>
              {nvlString(user?.nickname)}{HEALTH_NOTE_TEXT}
            </Typography>
          </Stack>


          <TextMaxLineWithAccordion line={3} persistent
                                    sx={{lineHeight: '24px', color: '#666666'}}>
            {nvlString(recipe?.recipe_health_note)}
          </TextMaxLineWithAccordion>
        </Box>
      }
    </Box>
  )
}
