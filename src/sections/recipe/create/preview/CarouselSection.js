import PropTypes from 'prop-types';
import {Avatar, Badge, Container, Skeleton, Stack, Typography} from '@mui/material';
import Carousel from 'react-slick'
import CarouselItem from "../../../../components/recipe/create/carouse/CarouselItem";
import CarouselDotsDetail from "../../../../components/recipe/create/carouse/CarouselDotsDetail";
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import * as React from "react";
import {BADGE_ICON_TYPE, BadgeIcons} from "../../../../constant/icons/Badge/BadgeIcons";
import {nvlString} from "../../../../utils/formatString";
import {ALT_STRING} from "../../../../constant/common/AltString";

// ----------------------------------------------------------------------

CarouselSection.propTypes = {
  recipe: PropTypes.object,
  user: PropTypes.object,
};

export default function CarouselSection({images, user}) {
  const carouselSettings = {
    dots: true,
    arrows: false,
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    edgeFriction: 0,
    ...CarouselDotsDetail({
      rounded: false,
      sx: {
        bottom: 0,
        zIndex: 10,
        width: '100%',
        display: 'flex',
        position: 'absolute',
        paddingBottom: '89px',
        alignItems: 'flex-end',
      },
    }),
  };

  return (
    <>
      <Container disableGutters sx={{position: 'relative', zIndex: 99}}>
        <Carousel {...carouselSettings}>
          {images &&
            (!images.length ? [...Array(5)] : images).map((image, index) =>
              image ? (
                <CarouselItem image={image} key={index}/>
              ) : (
                <Skeleton variant="rectangular" width="100%" sx={{height: 360}} key={index}/>
              ))}
        </Carousel>
        <Stack
          flexWrap="wrap"
          direction="row"
          justifyContent="flex-center"
          sx={{pl: '20px', position: 'absolute', bottom: 0, zIndex: 99, alignItems: 'center'}}
        >
          <Badge
            overlap="circular"
            anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
            badgeContent={
              user?.type === "NORMAL_MEMBER" && user?.grade === "LV1" ? (
                <BadgeIcons alt={ALT_STRING.COMMON.BADGE_LV1} type={BADGE_ICON_TYPE.BADGE_LV1_16PX} sx={{mb: '84px', width: '16px'}}/>
              ) : (
                user?.type === "NORMAL_MEMBER" && user?.grade === "LV2" ? (
                  <BadgeIcons alt={ALT_STRING.COMMON.BADGE_LV2} type={BADGE_ICON_TYPE.BADGE_LV2_16PX} sx={{mb: '84px', width: '16px'}}/>
                ) : (
                  user?.type === "NORMAL_MEMBER" && user?.grade === "LV3" ? (
                    <BadgeIcons alt={ALT_STRING.COMMON.BADGE_LV3} type={BADGE_ICON_TYPE.BADGE_LV3_16PX} sx={{mb: '84px', width: '16px'}}/>
                  ) : (
                    user?.type === "EXPERT" ? (
                      <BadgeIcons alt={ALT_STRING.COMMON.BADGE_LV4} type={BADGE_ICON_TYPE.BADGE_LV4_16PX} sx={{mb: '84px', width: '16px'}}/>
                    ) : (
                      <></>
                    )
                  )))
            }
          >
            {user?.profilePictureUrl ?
              (<Avatar
                alt={ALT_STRING.COMMON.NICKNAME(user?.nickname)}
                src={user?.profilePictureUrl}
                sx={{
                  width: '30px',
                  height: '30px',
                  mb: '49px',
                  border: 1,
                  borderColor: '#E6E6E6'
                }}
              />) : (
                <SvgCommonIcons
                  alt={ALT_STRING.COMMON.NICKNAME(user?.nickname)}
                  type={ICON_TYPE.PROFILE_NO_IMAGE_30PX}
                  sx={{
                    mb: '49px',
                    width: '30px',
                    height: '30px',
                  }}/>
              )}
            {/*<Avatar src={user.avatar?user.avatar:"/assets/icons/detail/no_avatar.svg"}*/}
            {/*        sx={{ width: '30px', height: '30px', mb: '49px', border: user.avatar?1:0, borderColor: user.avatar?'#E6E6E6':'inherit'}}/>*/}
          </Badge>
          <Typography fontWeight="400" fontSize="16px"
                      sx={{pl: '10px', mb: '45.5px', zIndex: 99, lineHeight: '24px', color: 'white',}}>
            {nvlString(user?.nickname)}
          </Typography>
        </Stack>
      </Container>
    </>
  );
}
