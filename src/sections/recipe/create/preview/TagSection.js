import {Box, Typography} from "@mui/material";
import PropTypes from "prop-types";
import IngredientsSection from "./IngredientsSection";

IngredientsSection.propTypes = {
  tags: PropTypes.array,
}

export default function TagSection({tags}) {
  return (
    <Box sx={{mx: '20px'}}>
      <Typography fontWeight="400" fontSize="14px" sx={{lineHeight: '20px', color: '#888888'}}>
        {tags.length > 0 && tags.map((tag, index) => (
          tag.length > 0 ? (index < tags.length - 1 ? '#' + tag + ',' : '#' + tag) : ''
        ))}
      </Typography>
    </Box>
  )
}