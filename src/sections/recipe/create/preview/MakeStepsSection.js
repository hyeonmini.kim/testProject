import {Box, Container, Skeleton, Stack, Typography} from "@mui/material";
import PropTypes from "prop-types";
import IngredientsSection from "./IngredientsSection";
import {fileData} from "../../../../components/file-thumbnail";
import Image from "../../../../components/image";
import * as React from "react";

IngredientsSection.propTypes = {
  steps: PropTypes.object,
  sx: PropTypes.object,
}

export default function MakeStepsSection({steps, sx}) {
  if (!steps) return
  return (
    <Box sx={{...sx}}>
      {steps &&
        (!steps.length ? [...Array(1)] : steps).map((step, index) =>
          step ? (
            <MakeStepsItem step={step} index={index} key={index}/>
          ) : (
            <SkeletonItem key={index}/>
          ),
        )}
    </Box>
  );
}

function MakeStepsItem({step, index}) {
  const preview = step?.image_file_path ? fileData(step.image_file_path).preview : undefined
  return (
    <Stack direction="column" sx={{py: '30px'}}>
      <Typography variant={'b1_16_m_1l'} sx={{pl: '20px', color: '#222222'}}>
        STEP {index + 1}
      </Typography>
      <Box sx={{my: '10px'}}>
        {preview && <Image alt={''} src={preview} style={{aspectRatio: '4/3'}}/>}
      </Box>
      <Typography variant={'b1_16_r'} sx={{pl: '20px', color: '#666666'}}>
        {step?.recipe_order_desc}
      </Typography>
    </Stack>
  )
}

function SkeletonItem() {
  return (
    <>
      <Skeleton variant="rectangular" width="100%"
                sx={{height: '270px', marginTop: '10px', marginBottom: '10px'}}/>
      <Container maxWidth={'md'} disableGutters sx={{px: '20px',}}>
        <Skeleton variant="rectangular" width="100%" sx={{height: '32px'}}/>
      </Container>
    </>
  )
}