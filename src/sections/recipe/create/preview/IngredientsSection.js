import {Box, Divider, Grid, Typography} from "@mui/material";
import PropTypes from "prop-types";
import {INGREDIENTS_TEXT} from "../../../../constant/recipe/detail/DetailConstants";
import {SkeletonPostItem} from "../../../../components/skeleton";
import {useRecoilState} from "recoil";
import {
  createRecipeBasicMaterialsSelector,
  createRecipeSeasoningMaterialsSelector
} from "../../../../recoil/selector/recipe/createSelector";
import {nvlString} from "../../../../utils/formatString";
import {nvlNumber} from "../../../../utils/formatNumber";
import TextMaxLine from "../../../../components/text-max-line";

IngredientsSection.propTypes = {
  recipe: PropTypes.object,
  sx: PropTypes.object,
}

export default function IngredientsSection({open, recipe, sx}) {
  if (!recipe) return

  const [basicMaterials, setBasicMaterials] = useRecoilState(createRecipeBasicMaterialsSelector)
  const [seasoningMaterials, setSeasoningMaterials] = useRecoilState(createRecipeSeasoningMaterialsSelector)

  return (
    <Box sx={{display: 'grid', ...sx}}>
      <Typography variant={'b_18_b'} sx={{color: '#222222'}}>
        {INGREDIENTS_TEXT.TITLE_FIRST(recipe?.recipe_servings)}
      </Typography>

      <Typography variant={'b_18_b'} sx={{color: '#222222'}}>
        {INGREDIENTS_TEXT.TITLE_FIXED}
      </Typography>

      <Box sx={{mt: '30px', mb: '20px'}}>
        <Typography variant={'b1_16_m_1l'} sx={{color: '#222222'}}>
          {INGREDIENTS_TEXT.SUB_TITLE.BASICS}
        </Typography>
      </Box>
      <IngredientsContents open={open} items={basicMaterials}/>

      {seasoningMaterials?.length > 0 &&
        <>
          <Divider sx={{maxWidth: "md", mt: '30px'}}/>
          <Box sx={{mt: '30px', mb: '20px'}}>
            <Typography variant={'b1_16_m_1l'} sx={{color: '#222222'}}>
              {INGREDIENTS_TEXT.SUB_TITLE.SEASONINGS}
            </Typography>
          </Box>
          <IngredientsContents open={open} items={seasoningMaterials}/>
        </>
      }
    </Box>
  );
}

function IngredientsContents({open, items}) {
  return (
    <>
      {items &&
        (!items.length ? [...Array(2)] : items).map((item, index) =>
          item ? (
            <IngredientsItem open={open} item={item} key={index}/>
          ) : (
            <SkeletonPostItem key={index}/>
          ),
        )}
    </>
  );
}

function IngredientsItem({open, item}) {
  return (
    <Grid container>
      <Grid item xs={6.3} sx={{mb: '15px'}}>
        <TextMaxLine open={open} variant={'b1_16_r'} line={3} sx={{whiteSpace: 'pre-wrap', color: '#666666'}}>
          {nvlString(item?.recipe_ingredient_name)}
        </TextMaxLine>
      </Grid>
      <Grid item xs={0.7}/>
      <Grid item xs={5}>
        <Typography variant={'b1_16_r'} sx={{whiteSpace: 'pre-wrap', color: '#666666'}}>
          {nvlNumber(item?.recipe_ingredient_amount)}{nvlString(item?.recipe_ingredient_main_countunit)}
        </Typography>
      </Grid>
    </Grid>
  )
}