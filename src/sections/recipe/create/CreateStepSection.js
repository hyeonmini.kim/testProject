import {RECIPE_CREATE_ACTIVE_STEP, RECIPE_CREATE_COMMON, RECIPE_IMAGE_TYPE} from "../../../constant/recipe/Create";
import {Alert, Box, Button, Container, Step, StepLabel, Stepper, Typography} from "@mui/material";
import BasicInfoSection from "./step/BasicInfoSection";
import AdditionalSection from "./step/AdditionalSection";
import MaterialSection from "./step/MaterialSection";
import StepSection from "./step/StepSection";
import HeaderTitleLayout from "../../../layouts/recipe/create/HeaderTitleLayout";
import {clickBorderNone, COMMON_DIALOG_TYPE, TOAST_TYPE} from "../../../constant/common/Common";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {useRecoilState, useResetRecoilState} from "recoil";
import {
  createActiveStepSelector,
  createEmptySelector,
  createOrderDeleteImagesSelector,
  createRecipeBasicMaterialsSelector,
  createRecipeDeleteImagesSelector,
  createRecipeImagesSelector,
  createRecipeKeySelector,
  createRecipeSelector,
  createShowMaterialAddSelector,
  createShowMaterialEditSelector,
  createShowMaterialInputSelector,
  createShowMeasurementUnitSelector,
  createShowPreviewSelector,
  createShowReturnSelector,
  createTemporaryRecipeSelector
} from "../../../recoil/selector/recipe/createSelector";
import {toastMessageSelector} from "../../../recoil/selector/common/toastSelector";
import {dialogSelector, dialogShowDialogSelector} from "../../../recoil/selector/common/dialogSelector";
import PreviewSection from "./PreviewSection";
import useStackNavigation from "../../../hooks/useStackNavigation";
import ReturnSection from "./step/ReturnSection";
import _ from 'lodash';
import {patchRecipeUploadMutate, postImageUploadMutate, postTempRecipeMutate} from "../../../api/recipeApi";
import {sxFixedCenterMainLayout} from "../../../layouts/main/MainLayout";
import {STACK} from "../../../constant/common/StackNavigation";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import LoadingScreen from "../../../components/common/LoadingScreen";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {useBefuAuthContext} from "../../../auth/useAuthContext";
import {useRouter} from "next/router";

export default function CreateStepSection({mainRef, onBackKey}) {
  const [isLoading, setIsLoading] = useState(false)
  const [isDisabled, setIsDisabled] = useState(true)
  const [buttonText, setButtonText] = useState('')
  const [showClose, setShowClose] = useState(false)

  const [empty, setEmpty] = useRecoilState(createEmptySelector)
  const [recipe, setRecipe] = useRecoilState(createRecipeSelector)
  const [recipeKey, setRecipeKey] = useRecoilState(createRecipeKeySelector)
  const [basicMaterials, setBasicMaterials] = useRecoilState(createRecipeBasicMaterialsSelector)
  const [recipeImages, setRecipeImages] = useRecoilState(createRecipeImagesSelector)
  const [deleteRecipeImages, setDeleteRecipeImages] = useRecoilState(createRecipeDeleteImagesSelector)
  const [deleteOrderImages, setDeleteOrderImages] = useRecoilState(createOrderDeleteImagesSelector)

  const resetTempRecipe = useResetRecoilState(createTemporaryRecipeSelector)
  const [activeStep, setActiveStep] = useRecoilState(createActiveStepSelector)
  const [toastMessage, setToastMessage] = useRecoilState(toastMessageSelector)

  const {asPath, push} = useRouter();
  const {isAuthenticated} = useBefuAuthContext();

  useEffect(() => {
    setShowClose(true)
    setIsDisabled(true)
    switch (activeStep) {
      case RECIPE_CREATE_ACTIVE_STEP.BASIC :
        setShowClose(false)
        setButtonText(RECIPE_CREATE_COMMON.BUTTON.NEXT)
        break;
      case RECIPE_CREATE_ACTIVE_STEP.ADDITIONAL :
        setButtonText(RECIPE_CREATE_COMMON.BUTTON.NEXT)
        break;
      case RECIPE_CREATE_ACTIVE_STEP.MATERIAL :
        setButtonText(RECIPE_CREATE_COMMON.BUTTON.NEXT)
        break;
      case RECIPE_CREATE_ACTIVE_STEP.STEP :
        setButtonText(RECIPE_CREATE_COMMON.BUTTON.PREVIEW)
        break;
    }
    setIsDisabled(!isCompletedStep(activeStep))
  }, [recipe, recipeImages, activeStep])

  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)
  const [showReturn, setShowReturn] = useRecoilState(createShowReturnSelector)
  const [showPreview, setShowPreview] = useRecoilState(createShowPreviewSelector)
  const [showMaterialEdit, setShowMaterialEdit] = useRecoilState(createShowMaterialEditSelector)
  const [showMaterialAdd, setShowMaterialAdd] = useRecoilState(createShowMaterialAddSelector)
  const [showMaterialInput, setShowMaterialInput] = useRecoilState(createShowMaterialInputSelector)
  const [showMeasurementUnit, setShowMeasurementUnit] = useRecoilState(createShowMeasurementUnitSelector)

  useEffect(() => {
    if (onBackKey) {
      if (showDialog) {
        setShowDialog(false)
      } else if (showReturn) {
        setShowReturn(false)
      } else if (showMaterialEdit) {
        setShowMaterialEdit(false)
      } else if (showMaterialAdd) {
        setShowMaterialAdd(false)
      } else if (showMaterialInput) {
        setShowMaterialInput(false)
      } else if (showMeasurementUnit) {
        setShowMeasurementUnit(false)
      } else if (showPreview) {
        setShowPreview(false)
      } else {
        handleBackButton()
      }
    }
  }, [onBackKey]);

  const [dialogMessage, setDialogMessage] = useRecoilState(dialogSelector)
  const handleClose = () => {
    if (recipe?.recipe_reject_msg || (!recipeImages?.length && _.isEqual(empty, recipe))) {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.TWO_BUTTON,
        message: RECIPE_CREATE_COMMON.TEXT.CLOSE_CONFIRM,
        leftButtonProps: {text: RECIPE_CREATE_COMMON.BUTTON.CANCEL},
        rightButtonProps: {text: RECIPE_CREATE_COMMON.BUTTON.CLOSE, onClick: handleBack},
      })
    } else {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.TWO_BUTTON,
        message: RECIPE_CREATE_COMMON.TEXT.CLOSE_SAVE_CONFIRM,
        leftButtonProps: {text: RECIPE_CREATE_COMMON.BUTTON.CANCEL},
        rightButtonProps: {text: RECIPE_CREATE_COMMON.BUTTON.CLOSE, onClick: handleSaveCloseButton},
      })
    }
  };

  const handleBack = () => {
    if (!isAuthenticated && asPath?.includes('d_inflow')) {
      push(PATH_DONOTS_PAGE?.GUEST);
    } else if (isAuthenticated && asPath?.includes('d_inflow')) {
      push(PATH_DONOTS_PAGE?.HOME);
    } else {
      navigation.clearBack(STACK.MY_SELF.TYPE)
    }
  }


  const handleBackButton = () => {
    if (activeStep === RECIPE_CREATE_ACTIVE_STEP.BASIC) {
      handleClose()
    } else {
      setActiveStep((prevActiveStep) => prevActiveStep - 1);
    }
  };

  const handleStepClick = (index) => {
    setActiveStep(index)
  }

  const handleReturnClick = () => {
    setShowReturn(true)
  }

  const isCompletedStep = (index) => {
    switch (index) {
      case RECIPE_CREATE_ACTIVE_STEP.BASIC :
        if (recipe?.recipe_name && recipe?.recipe_desc && recipe?.recipe_category && recipeImages?.length > 0) {
          return true
        }
        return false
      case RECIPE_CREATE_ACTIVE_STEP.ADDITIONAL :
        if (recipe?.recipe_lead_time && recipe?.recipe_level && recipe?.recipe_babyfood_step) {
          return true
        }
        return false
      case RECIPE_CREATE_ACTIVE_STEP.MATERIAL :
        if (recipe?.recipe_servings && basicMaterials?.length > 0) {
          return true
        }
        return false
      case RECIPE_CREATE_ACTIVE_STEP.STEP :
        let completed = true
        if (recipe?.recipe_order_list?.length < 2) {
          completed = false
        } else {
          recipe?.recipe_order_list?.map((step) => {
            if (step?.recipe_order_desc?.length === 0) {
              completed = false
            }
          })
        }
        return completed
    }
  }

  const {mutate: mutatePostImageUpload} = postImageUploadMutate()
  const updateRecipeImage = (recipeKey) => {
    const formData = new FormData()
    const files = recipeImages?.filter((file) => typeof file?.image_file_path === 'object')
    files.forEach((file) => {
      formData.append('recipe_image_list', file?.image_file_path)
    })
    formData.append('recipe_key', recipeKey ? recipeKey : '')
    formData.append('recipe_image_type', RECIPE_IMAGE_TYPE.RECIPE)
    formData.append('image_delete_list', JSON.stringify(deleteRecipeImages?.length ? deleteRecipeImages.map((file) => {
      return {
        image_delete_index: String(file?.index + 1),
        image_delete_key: file?.image_file_key
      }
    }) : []))
    formData.append('image_change_list', JSON.stringify(recipeImages?.length ? recipeImages.map((file, index) => {
      if (typeof file?.image_file_path === 'object') {
        return {
          image_change_index: String(index + 1),
          image_change_key: '',
        }
      } else {
        return {
          image_change_index: String(index + 1),
          image_change_key: file?.image_file_key
        }
      }
    }) : []))
    mutatePostImageUpload(formData)
  }

  const updateOrderImage = (recipeKey) => {
    const formData = new FormData()
    const files = recipe?.recipe_order_list?.filter((file) => typeof file?.image_file_path === 'object')
    files.forEach((file) => {
      formData.append('recipe_image_list', file?.image_file_path)
    })
    formData.append('recipe_key', recipeKey ? recipeKey : '')
    formData.append('recipe_image_type', RECIPE_IMAGE_TYPE.ORDER)
    formData.append('image_delete_list', JSON.stringify(deleteOrderImages?.length ? deleteOrderImages.map((file) => {
      return {
        image_delete_index: String(Number(file?.recipe_order)),
        image_delete_key: file?.image_file_key,
      }
    }) : []))
    formData.append('image_change_list', JSON.stringify(recipe?.recipe_order_list?.length ? recipe?.recipe_order_list.map((file, index) => {
      if (typeof file?.image_file_path === 'object') {
        return {
          image_change_index: String(index + 1),
          image_change_key: ''
        }
      } else if (file?.image_file_key) {
        return {
          image_change_index: String(index + 1),
          image_change_key: file?.image_file_key
        }
      }
      return null
    }).filter((file) => file) : []))
    mutatePostImageUpload(formData)
  }

  const {navigation} = useStackNavigation()

  const handleSaveCloseButton = () => {
    setIsLoading(true)
    saveTempRecipe(() => {
      setToastMessage({type: TOAST_TYPE.BOTTOM_HEADER, message: RECIPE_CREATE_COMMON.TOAST.TEMPORARY})
      resetTempRecipe()
      setTimeout(() => {
        setIsLoading(false)
        handleBack()
      }, 1000)
    })
  }

  const {mutate: mutatePostTempRecipe} = postTempRecipeMutate()
  const saveTempRecipe = (onSuccess) => {
    const recipes = {
      ...recipe,
      recipe_order_list: recipe?.recipe_order_list?.map((order, index) => {
        return {
          ...order,
          image_file_path: typeof order?.image_file_path === 'object' ? '' : order?.image_file_path,
          recipe_order: String(index + 1)
        }
      })
    }
    mutatePostTempRecipe(recipes, {
      onSuccess: (data) => {
        if (data) {
          setRecipeKey(data)
          updateRecipeImage(data)
          updateOrderImage(data)
        }
        onSuccess(data)
      }
    })
  }

  const {mutate: mutatePatchRecipeUpload} = patchRecipeUploadMutate()
  const handleCompleteButton = () => {
    mutatePatchRecipeUpload({
      recipe_key: recipeKey
    }, {
      onSuccess: () => {
        setToastMessage({type: TOAST_TYPE.BOTTOM_HEADER, message: RECIPE_CREATE_COMMON.TOAST.COMPLETE})
        resetTempRecipe()
        handleBack()
      }
    })
  };

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.WRITE_STEP1)
  }, [])

  useEffect(() => {
    bottomBtnRef?.current?.blur()
  }, [activeStep])

  const headerTitleRef = useRef(null)
  const bottomBtnRef = useRef(null)

  return (
    <>
      <HeaderTitleLayout
        title={RECIPE_CREATE_COMMON.TEXT.TITLE}
        onBack={handleBackButton}
        showClose={showClose}
        onClose={handleClose}
      >
        <Container disableGutters maxWidth={'xs'} sx={{mt: '10px', px: '20px'}}>

          {/* 스테퍼 */}
          <ActiveStepper onClick={handleStepClick} activeStep={activeStep} isCompletedStep={isCompletedStep}/>

          {/* 반려 메시지 알림 */}
          {recipe?.recipe_reject_msg && <ReturnAlert onClick={handleReturnClick}/>}

          {/* 1~4 섹션 */}
          {activeStep === RECIPE_CREATE_ACTIVE_STEP.BASIC && <BasicInfoSection/>}
          {activeStep === RECIPE_CREATE_ACTIVE_STEP.ADDITIONAL && <AdditionalSection/>}
          {activeStep === RECIPE_CREATE_ACTIVE_STEP.MATERIAL && <MaterialSection mainRef={mainRef}/>}
          {activeStep === RECIPE_CREATE_ACTIVE_STEP.STEP && <StepSection/>}

          {/* 하단 버튼 */}
          {!showPreview && !showMaterialInput && !showMaterialEdit &&
            <BottomFixedButton btnRef={bottomBtnRef} isDisabled={isDisabled} text={buttonText} isCompletedStep={isCompletedStep}
                               saveTempRecipe={saveTempRecipe}/>}

          {/* 로딩 스크린 */}
          {isLoading && <LoadingScreen />}

        </Container>
      </HeaderTitleLayout>

      {/* 반려 메시지 다이얼로그 */}
      {showReturn && <ReturnSection open={showReturn} message={recipe?.recipe_reject_msg} onClose={() => setShowReturn(false)}/>}

      {/* 미리보기 다이얼로그 */}
      {showPreview && <PreviewSection open={showPreview} onNext={handleCompleteButton} onBack={() => setShowPreview(false)}
                                      onClose={handleClose}/>}
    </>
  )
}

function ActiveStepper({onClick, isCompletedStep}) {
  const [activeStep, setActiveStep] = useRecoilState(createActiveStepSelector)
  const isActiveStep = (index) => {
    if (activeStep >= index) {
      return true
    }
    return false
  }

  return (
    <Stepper activeStep={activeStep} alternativeLabel sx={{mx: '-20px'}}>
      {RECIPE_CREATE_COMMON.STEPS.map((label, index) => (
        <Step key={label}>
          <StepLabel
            onClick={() => onClick(index)}
            sx={{'& .MuiStepIcon-text': {fontSize: '14px', fontWeight: '700', transform: 'translateY(-1px)'}}}
            StepIconProps={{
              completed: isCompletedStep(index),
              sx: {fill: isActiveStep(index) ? '#ECA548' : '#888888'}
            }}
            optional={isActiveStep(index)
              ? <Typography key={label} sx={{
                color: 'primary.main',
                fontSize: '14px',
                lineHeight: '22px',
                fontWeight: '700',
                mt: '6px'
              }}> {label} </Typography>
              : <Typography key={label} sx={{
                color: '#888888',
                fontSize: '14px',
                lineHeight: '22px',
                fontWeight: '400',
                mt: '6px'
              }}>{label}</Typography>}
          />
        </Step>
      ))}
    </Stepper>
  )
}

function ReturnAlert({onClick}) {
  return (
    <Alert
      tabIndex={0}
      severity="error"
      sx={{
        px: '11px',
        mt: '30px',
        mb: '50px',
        height: '38px',
        alignItems: 'center',
        backgroundColor: '#FFFAE6',
        '& .MuiAlert-icon': {
          ml: 0,
          mr: '7px',
        },
        '& .MuiSvgIcon-root': {
          width: '18px',
        }
      }}
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
    >
      <Box sx={{fontSize: '14px', fontWeight: 400, color: '#222222'}}>
        {RECIPE_CREATE_COMMON.TEXT.ALERT}
        <Box
          sx={{
            fontSize: '14px',
            fontWeight: 400,
            color: '#222222',
            textDecoration: 'underline',
            display: 'inline'
          }}>보기</Box>
      </Box>
    </Alert>
  )
}

function BottomFixedButton({btnRef, isDisabled, text, isCompletedStep, saveTempRecipe}) {
  const [activeStep, setActiveStep] = useRecoilState(createActiveStepSelector)
  const [showPreview, setShowPreview] = useRecoilState(createShowPreviewSelector)
  const [dialogMessage, setDialogMessage] = useRecoilState(dialogSelector)

  const isNotCompleted = () => {
    let notCompleted = ''
    RECIPE_CREATE_COMMON.STEPS.forEach((value, index) => {
      if (!isCompletedStep(index)) {
        if (notCompleted) {
          notCompleted += ', '
        }
        switch (index) {
          case 0:
            notCompleted += '① '
            break
          case 1:
            notCompleted += '② '
            break
          case 2:
            notCompleted += '③ '
            break
        }
        notCompleted += value
      }
    })
    return notCompleted
  }

  const handleNextButton = () => {
    if (activeStep === RECIPE_CREATE_ACTIVE_STEP.BASIC) {
      setHackleTrack(HACKLE_TRACK.WRITE_STEP2)
    } else if (activeStep === RECIPE_CREATE_ACTIVE_STEP.ADDITIONAL) {
      setHackleTrack(HACKLE_TRACK.WRITE_STEP3)
    } else if (activeStep === RECIPE_CREATE_ACTIVE_STEP.MATERIAL) {
      setHackleTrack(HACKLE_TRACK.WRITE_STEP4)
    }

    if (activeStep === RECIPE_CREATE_ACTIVE_STEP.STEP) {
      const notCompleted = isNotCompleted()
      if (notCompleted) {
        setDialogMessage({
          type: COMMON_DIALOG_TYPE.ONE_BUTTON,
          message: RECIPE_CREATE_COMMON.TEXT.INPUT_CONFIRM(notCompleted),
          handleButton1: {text: RECIPE_CREATE_COMMON.BUTTON.CONFIRM},
        })
      } else {
        setHackleTrack(HACKLE_TRACK.WRITE_PREVIEW)
        saveTempRecipe(() => setShowPreview(true))
      }
    } else {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };

  return (
    <Box
      sx={{
        px: '20px',
        py: '20px',
        position: 'fixed',
        bgcolor: 'white',
        zIndex: 1,
        bottom: 0,
        ...sxFixedCenterMainLayout,
      }}
    >
      <Container maxWidth={'xs'} disableGutters>
        <Button
          tabIndex={0}
          disableFocusRipple
          fullWidth variant="contained"
          onClick={handleNextButton}
          disabled={isDisabled}
          ref={btnRef}
          sx={{
            height: '52px',
            fontSize: '16px',
            fontWeight: '700',
            lineHeight: '28px',
            backgroundColor: isDisabled ? '#E6E6E6' : 'primary.main',
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: isDisabled ? '#E6E6E6' : 'primary.main',
            },
            ...clickBorderNone,
          }}
        >
          {text}
        </Button>
      </Container>
    </Box>
  )
}