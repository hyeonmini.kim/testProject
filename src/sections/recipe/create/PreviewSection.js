import {Box, Container, Divider, Fade} from "@mui/material";
import {useRecoilValue} from "recoil";
import {createRecipeSelector} from "../../../recoil/selector/recipe/createSelector";
import BottomFixedTwoButton from "../../../components/recipe/create/button/BottomFixedTwoButton";
import {RECIPE_CREATE_COMMON} from "../../../constant/recipe/Create";
import {forMyPageEntrySelector} from "../../../recoil/selector/auth/userSelector";
import CarouselSection from "./preview/CarouselSection";
import TopMainSection from "./preview/TopMainSection";
import IngredientsSection from "./preview/IngredientsSection";
import MakeStepsSection from "./preview/MakeStepsSection";
import TagSection from "./preview/TagSection";
import {createRecipeImages} from "../../../recoil/atom/recipe/create";
import {MainDialogLayout} from "../../../layouts/main/MainLayout";
import HeaderDialogLayout from "../../../layouts/recipe/create/HeaderDialogLayout";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";

export default function PreviewSection({open, onNext, onBack, onClose}) {
  const recipe = useRecoilValue(createRecipeSelector)
  const imageFilePath = useRecoilValue(createRecipeImages)
  const user = useRecoilValue(forMyPageEntrySelector)

  const handleBack = () => {
    setHackleTrack(HACKLE_TRACK.RECIPE_PREVIEW_MODIFY)
    onBack()
  }

  const handleUpload = () => {
    setHackleTrack(HACKLE_TRACK.RECIPE_UPLOAD)
    onNext()
  }

  return (
    <>
      <MainDialogLayout
        fullScreen
        open={open}
        onClose={onClose}
        TransitionComponent={Fade}
        sx={{'& .MuiDialog-paper': {mx: 0}}}
      >
        <HeaderDialogLayout
          title={RECIPE_CREATE_COMMON.TEXT.PREVIEW}
          showClose={true}
          onClose={onClose}
        >
          <Container disableGutters maxWidth={'xs'} sx={{position: 'relative'}}>
            <CarouselSection images={imageFilePath} user={user}/>
            <Box sx={{
              mt: '-34px',
              pb: '132px',
              width: '100%',
              position: 'absolute',
              backgroundColor: 'white',
              borderRadius: '30px',
              zIndex: 99
            }}>
              <TopMainSection recipe={recipe} user={user} sx={{mt: '30px', mx: '20px'}}/>
              <Divider sx={{maxWidth: "xs", px: '0px', mt: '50px'}}/>
              <IngredientsSection open={open} recipe={recipe} sx={{px: '20px', pt: '100px', mt: '-50px'}}/>
              <Divider sx={{maxWidth: "xs", px: '0px', mt: '50px', mb: '20px'}}/>
              <MakeStepsSection steps={recipe?.recipe_order_list} sx={{px: '0px', my: '20px'}}/>
              {recipe?.recipe_tag_desc && <TagSection tags={recipe?.recipe_tag_desc?.split(',')}/>}
            </Box>
          </Container>
          {open && <BottomFixedTwoButton leftButtonProps={{text: RECIPE_CREATE_COMMON.BUTTON.MODIFY, onClick: handleBack}}
                                         rightButtonProps={{text: RECIPE_CREATE_COMMON.BUTTON.UPLOAD, onClick: handleUpload}}/>}
        </HeaderDialogLayout>
      </MainDialogLayout>
    </>
  )
}

