import {Box, Grid, Typography} from "@mui/material";
import {RECIPE_SEARCH_HOME_CATEGORY,} from "../../../../constant/recipe/Search";
import {SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import {setHackleTrack} from "../../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../../constant/common/Hackle";
import {clickBorderNone} from "../../../../constant/common/Common";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";

export default function CategoryTab({onClick}) {
  const handleClick = (title) => {
    setHackleTrack(HACKLE_TRACK.SEARCH_CATEGORY,{
      search_category_name: title
    })
    onClick(title)
  }
  return (
    <Grid container columnSpacing={'18px'} sx={{mt: '20px', px: '1px'}}>
      {RECIPE_SEARCH_HOME_CATEGORY.ITEM.map((item, index) => (
        <Grid item xs={3} key={index} onClick={() => handleClick(item?.title)} sx={{mt: index < 4 ? '0px' : '22px'}}>
          <Box
            tabIndex={2}
            sx={{display: 'flex', flexDirection: 'column', alignItems: 'center', height: '73px', ...clickBorderNone}}
            onKeyDown={(e) => handleEnterPress(e, () => handleClick(item?.title))}
          >
            <Box sx={{flexGrow: 1, display: 'flex', alignItems: 'center'}}>
              <SvgCommonIcons type={item?.type}/>
            </Box>
            <Typography sx={{
              color: '#666666',
              fontSize: '14px',
              fontWeight: 400,
              lineHeight: '17px'
            }}>
              {item?.title}
            </Typography>
          </Box>
        </Grid>
      ))}
    </Grid>
  )
}