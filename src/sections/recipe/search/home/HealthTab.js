import {Box, Grid, Typography} from "@mui/material";
import {RECIPE_SEARCH_HOME_HEALTH} from "../../../../constant/recipe/Search";
import {SvgHealthKeywordIcons} from "../../../../constant/icons/healthKeywordIcons";
import {setHackleTrack} from "../../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../../constant/common/Hackle";
import {clickBorderNone} from "../../../../constant/common/Common";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";

export default function HealthTab({onClick}) {
  const handleClick = (title) => {
    setHackleTrack(HACKLE_TRACK.SEARCH_KEYWORD,{
      search_keyword_name: title
    })
    onClick(title)
  }

  return (
    <Grid container columnSpacing={'14px'} sx={{mt: '20px', px: '1px'}}>
      {RECIPE_SEARCH_HOME_HEALTH.ITEM.map((item, index) => (
        <Grid item xs={3} key={index} onClick={() => handleClick(item?.title)} sx={{mt: index < 4 ? '0px' : '16px'}}>
          <Box
            tabIndex={2}
            sx={{display: 'flex', flexDirection: 'column', alignItems: 'center', my: '10px', height: '65px'}}
            onKeyDown={(e) => handleEnterPress(e, () => handleClick(item?.title))}
          >
            <Box sx={{flexGrow: 1, display: 'flex', alignItems: 'center'}}>
              <SvgHealthKeywordIcons type={item?.type}/>
            </Box>
            <Typography sx={{
              mt: '5px',
              color: '#666666',
              letterSpacing: '-0.02em',
              fontSize: '14px',
              fontWeight: 400,
              lineHeight: '20px'
            }}>
              {item?.title}
            </Typography>
          </Box>
        </Grid>
      ))}
    </Grid>
  )
}