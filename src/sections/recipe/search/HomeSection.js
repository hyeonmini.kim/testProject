import {Box, Container, InputAdornment, Tab, Tabs} from "@mui/material";
import {
  RECIPE_SEARCH_COMMON,
  RECIPE_SEARCH_HOME_CATEGORY,
  RECIPE_SEARCH_HOME_HEALTH,
  RECIPE_SEARCH_HOME_MATERIAL
} from "../../../constant/recipe/Search";
import HealthTab from "./home/HealthTab";
import MaterialTab from "./home/MaterialTab";
import CategoryTab from "./home/CategoryTab";
import {
  searchActiveTabSelector,
  searchCategorySelector,
  searchIsSearchSelector,
  searchIsTitleSelector,
  searchRecipeResultSelector,
  searchRecipeSearchSelector
} from "../../../recoil/selector/recipe/searchSelector";
import {useRecoilState} from "recoil";
import {ICON_COLOR, SvgSearchIcon} from "../../../constant/icons/icons";
import {useBefuAuthContext} from "../../../auth/useAuthContext";
import {useEffect} from "react";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";
import TextFieldWrapper from "../../../components/common/TextFieldWrapper";

export default function HomeSection() {
  const [activeTab, setActiveTab] = useRecoilState(searchActiveTabSelector);
  const [isSearch, setIsSearch] = useRecoilState(searchIsSearchSelector);
  const [search, setSearch] = useRecoilState(searchRecipeSearchSelector);
  const [isTitle, setIsTitle] = useRecoilState(searchIsTitleSelector);
  const [result, setResult] = useRecoilState(searchRecipeResultSelector);
  const [category, setCategory] = useRecoilState(searchCategorySelector);

  const {isInitialized} = useBefuAuthContext();

  const handleSearchFocus = () => {
    setIsSearch(true)
  };

  const handleCategoryChange = (tab) => {
    setActiveTab(tab)
  };

  const handleCategoryItemClick = (title) => {
    setSearch(title)
    setResult(title)
    setIsTitle(true)
    setCategory(activeTab)
  }

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SEARCH)
  }, [])

  return (
    <Container disableGutters maxWidth={'xs'} sx={{pt: '7px', mb: '72px', px: '20px'}}>
      <Box sx={{display: 'flex', width: '100%'}} tabIndex={1} onKeyDown={(e) => handleEnterPress(e, handleSearchFocus)}>
        <TextFieldWrapper
          tabIndex={-1}
          placeholder={RECIPE_SEARCH_COMMON.TEXT.SEARCH_PLACEHOLDER}
          variant="outlined"
          onFocus={handleSearchFocus}
          sx={{flexGrow: 1}}
          inputProps={{title: ALT_STRING.SEARCH.INPUT_SEARCH}}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start" sx={{ml: '0px'}}>
                <SvgSearchIcon alt={ALT_STRING.SEARCH.ICON_SEARCH} color={ICON_COLOR.GREY}/>
              </InputAdornment>
            ),
            sx: {
              height: '46px',
              '& input': {fontSize: '16px', fontWeight: '500', color: '#222222'},
              '&.MuiInputBase-root': {pl: '10px'},
              '& input::placeholder': {fontSize: '16px', fontWeight: '500', color: '#cccccc'},
            }
          }}
        />
      </Box>
      <Box sx={{borderBottom: '1px solid #E6E6E6'}}>
        <Tabs
          variant="fullWidth"
          value={activeTab}
          onChange={(event, newValue) => handleCategoryChange(newValue)}
          sx={{
            height: '53px',
            mx: '0px',
            mt: '24px',
            '& .MuiTabs-indicator': {backgroundColor: '#ECA548'},
            '& .Mui-selected': {mx: 0, color: 'primary.main', fontSize: '16px', fontWeight: 700, lineHeight: '21px'},
          }}
        >
          {RECIPE_SEARCH_COMMON.TABS.map((tab, index) => (
            <Tab
              tabIndex={1}
              key={index}
              value={tab}
              label={tab}
              sx={{
                '&:not(:last-of-type)': {marginRight: '0px'},
                '&:not(.Mui-selected)': {color: '#222222', fontSize: '16px', fontWeight: 400, lineHeight: '21px'},
                ...clickBorderNone,
              }}/>
          ))}
        </Tabs>
      </Box>
      {activeTab === RECIPE_SEARCH_HOME_HEALTH.NAME && <HealthTab onClick={handleCategoryItemClick}/>}
      {activeTab === RECIPE_SEARCH_HOME_MATERIAL.NAME && <MaterialTab onClick={handleCategoryItemClick}/>}
      {activeTab === RECIPE_SEARCH_HOME_CATEGORY.NAME && <CategoryTab onClick={handleCategoryItemClick}/>}
    </Container>
  )
}