import {Container, Grid, Typography} from "@mui/material";
import ContainedTextButton from "../../../components/recipe/create/button/ContainedTextButton";
import {RECIPE_SEARCH_POPULAR} from "../../../constant/recipe/Search";
import {useRecoilState, useSetRecoilState} from "recoil";
import {searchRecipeResultSelector, searchRecipeSearchSelector} from "../../../recoil/selector/recipe/searchSelector";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";

export default function SearchPopularSection() {
  const setSearch = useSetRecoilState(searchRecipeSearchSelector);
  const [result, setResult] = useRecoilState(searchRecipeResultSelector);

  const handleClick = (item) => {
    setSearch(item)
    setResult(item)
    setHackleTrack(HACKLE_TRACK.SEARCH_POPULAR, {
      search_keyword: item
    })
  }

  return (
    <Container disableGutters maxWidth={'xs'} sx={{mt: '30px', mb: '72px', px: '20px'}}>
      <Typography sx={{fontSize: '14px', fontWeight: '700', lineHeight: '22px', color: '#888888'}}>
        {RECIPE_SEARCH_POPULAR.NAME}
      </Typography>
      <Grid container sx={{mt: '-10px'}}>
        {RECIPE_SEARCH_POPULAR.ITEM.map((item, index) => (
          <Grid key={index} item sx={{mt: '20px', mr: '10px'}}>
            <ContainedTextButton text={item} onClick={() => handleClick(item)}/>
          </Grid>
        ))}
      </Grid>
    </Container>
  )
}