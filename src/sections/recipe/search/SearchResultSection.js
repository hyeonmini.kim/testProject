import {Box, Container, Skeleton, Stack, Switch, Typography} from "@mui/material";
import {useRecoilState, useResetRecoilState} from "recoil";
import {
  searchActiveTabSelector,
  searchAgeGroupSelector,
  searchCategorySelector,
  searchFetchItemsSelector,
  searchIsAvoidSelector,
  searchIsCallSelector,
  searchIsSearchSelector,
  searchIsTitleSelector,
  searchRecipeResultSelector,
  searchRecipeSearchSelector,
  searchShowFilterSelector,
} from "../../../recoil/selector/recipe/searchSelector";
import * as React from "react";
import {useEffect, useState} from "react";
import {RECIPE_SEARCH_AGE_GROUP_TYPE, RECIPE_SEARCH_COMMON, RECIPE_SEARCH_TYPE} from "../../../constant/recipe/Search";
import WrapGroupButton from "../../../components/recipe/search/WrapGroupButton";
import MenuPopover from "../../../components/menu-popover";
import SearchRecipeItem from "../../../components/recipe/search/SearchRecipeItem";
import useStackNavigation from "../../../hooks/useStackNavigation";
import {STACK} from "../../../constant/common/StackNavigation";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {SvgInformationIcon} from "../../../constant/icons/icons";
import {
  getRecipeCustomBasedListMoreMutate,
  getRecipeUserBodyBasedListMoreMutate,
  getSearchRecipeMutate,
  getSeasonIngredientBasedRecipeListMoreMutate
} from "../../../api/searchApi";
import usePagination from "../../../hooks/usePagination";
import {PAGINATION, SEARCH_PAGINATION} from "../../../constant/common/Pagination";
import LoadingScreen from "../../../components/common/LoadingScreen";
import {getURIString} from "../../../utils/formatString";
import {authenticateSelector} from "../../../recoil/selector/auth/authenticateSelector";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";

export default function SearchResultSection({mainRef}) {
  const [fetchItems, setFetchItems] = useRecoilState(searchFetchItemsSelector);
  const resetFetchItems = useResetRecoilState(searchFetchItemsSelector);
  const [search, setSearch] = useRecoilState(searchRecipeSearchSelector);
  const [result, setResult] = useRecoilState(searchRecipeResultSelector);
  const [isCall, setIsCall] = useRecoilState(searchIsCallSelector)
  const [isSearch, setIsSearch] = useRecoilState(searchIsSearchSelector);
  const [isTitle, setIsTitle] = useRecoilState(searchIsTitleSelector);
  const [isAvoid, setIsAvoid] = useRecoilState(searchIsAvoidSelector);
  const [category, setCategory] = useRecoilState(searchCategorySelector);
  const [ageGroup, setAgeGroup] = useRecoilState(searchAgeGroupSelector);
  const [activeTab, setActiveTab] = useRecoilState(searchActiveTabSelector);
  const [showFilter, setShowFilter] = useRecoilState(searchShowFilterSelector)
  const [isAuthenticated, setIsAuthenticated] = useRecoilState(authenticateSelector);
  const [isTooltip, setIsTooltip] = useState(null)

  const getMutate = (isCall) => {
    switch (isCall) {
      case RECIPE_SEARCH_TYPE.USER:
        return getRecipeUserBodyBasedListMoreMutate
      case RECIPE_SEARCH_TYPE.CUSTOM:
        return getRecipeCustomBasedListMoreMutate
      case RECIPE_SEARCH_TYPE.SEASONING:
        return getSeasonIngredientBasedRecipeListMoreMutate
      default:
        return getSearchRecipeMutate
    }
  }

  const setSearchParam = () => {
    let param = {}

    if (category) {
      param = {
        ...param,
        category_main_name: category,
        category_name: result,
      }
    } else {
      param = {
        ...param,
        recipe_search_text: result,
      }
    }

    if (ageGroup && ageGroup !== RECIPE_SEARCH_AGE_GROUP_TYPE.ALL) {
      param = {
        ...param,
        recipe_babyfood_step: ageGroup
      }
    }

    if (isAvoid) {
      param = {
        ...param,
        except_ingredient_yn: 'Y'
      }
    } else {
      param = {
        ...param,
        except_ingredient_yn: 'N'
      }
    }
    return param
  }

  const getVariable = (isCall) => {
    switch (isCall) {
      case RECIPE_SEARCH_TYPE.USER:
        return {}
      case RECIPE_SEARCH_TYPE.CUSTOM:
        return {}
      case RECIPE_SEARCH_TYPE.SEASONING:
        return {}
      default:
        return {
          params: setSearchParam()
        }
    }
  }

  const {
    param,
    items,
    setItems,
    setTarget,
    pageOffset,
    hasNextOffset,
    initPagination,
    isLoading,
    getPaginationItems,
  } = usePagination(getMutate(isCall), SEARCH_PAGINATION.PAGE_SIZE)

  param.current = getVariable(isCall)

  useEffect(() => {
    if (result) {
      if (fetchItems?.length) {
        initPagination(fetchItems)
        resetFetchItems()
      } else {
        getPaginationItems()
      }
    }
  }, [result, ageGroup, isAvoid])

  if (!result) return

  const {navigation} = useStackNavigation()
  const handleClick = (user, title, id) => {
    if (!isTooltip) {
      navigation.push(STACK.RECIPE_SEARCH.TYPE, PATH_DONOTS_PAGE.RECIPE.DETAIL(user, getURIString(title), id), {
        ...STACK.RECIPE_SEARCH.DATA,
        search: search,
        result: result,
        isCall: isCall,
        isSearch: isSearch,
        isTitle: isTitle,
        isAvoid: isAvoid,
        category: category,
        ageGroup: ageGroup,
        activeTab: activeTab,
        showFilter: showFilter,
        items: items,
        scrollY: mainRef?.current?.scrollTop
      })
    }
  }

  const handleGroupButton = (age) => {
    if (ageGroup === age) return
    if (age === RECIPE_SEARCH_AGE_GROUP_TYPE.ALL) {
      setHackleTrack(HACKLE_TRACK.LIST_FILTER_ALL,{
        search_keyword: search
      })
    } else if (age === RECIPE_SEARCH_AGE_GROUP_TYPE.EARLY) {
      setHackleTrack(HACKLE_TRACK.LIST_FILTER_EARLY,{
        search_keyword: search
      })
    } else if (age === RECIPE_SEARCH_AGE_GROUP_TYPE.MIDDLE) {
      setHackleTrack(HACKLE_TRACK.LIST_FILTER_MID,{
        search_keyword: search
      })
    } else if (age === RECIPE_SEARCH_AGE_GROUP_TYPE.LAST) {
      setHackleTrack(HACKLE_TRACK.LIST_FILTER_LATE,{
        search_keyword: search
      })
    } else if (age === RECIPE_SEARCH_AGE_GROUP_TYPE.EARLY_CHILD) {
      setHackleTrack(HACKLE_TRACK.LIST_FILTER_CHILD_EARLY,{
        search_keyword: search
      })
    } else if (age === RECIPE_SEARCH_AGE_GROUP_TYPE.LAST_CHILD) {
      setHackleTrack(HACKLE_TRACK.LIST_FILTER_CHILD_LATE,{
        search_keyword: search
      })
    }
    setAgeGroup(age)
  }

  const handleAvoidButton = () => {
    setHackleTrack(HACKLE_TRACK.LIST_TOGGLE_DONOT_ON,{
      search_keyword: search
    })
    setIsAvoid(!isAvoid)
  }
  const onTrackSearchNoResult = () => {
    setHackleTrack(HACKLE_TRACK.SEARCH_NORESULT,{
      search_keyword: search
    })
  }

  return (
    <Container disableGutters maxWidth={'xs'} sx={{mt: '20px', px: '20px'}}>
      {showFilter && <WrapGroupButton items={RECIPE_SEARCH_COMMON.AGE_GROUP} selected={ageGroup}
                                      onClick={(age) => handleGroupButton(age)}/>}
      {(showFilter && isAuthenticated) &&
        <SwitchAvoidRecipe mainRef={mainRef} isAvoid={isAvoid} isTooltip={isTooltip} setIsTooltip={setIsTooltip}
                           onClick={handleAvoidButton}/>}
      {isLoading && <LoadingScreen/>}
      {
        items === PAGINATION.INIT_DATA
          ? <RecipeSkeleton/>
          : items?.length ? (
            <>
              <RecipeItems items={items} onClick={handleClick} search={search}/>
              {hasNextOffset.current && <Box ref={setTarget}/>}
            </>
          ) : <RecipeNotFound onClick={onTrackSearchNoResult} />
      }
    </Container>
  )
}

function RecipeItems({items, onClick, search}) {
  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SEARCH_LIST,{
      search_keyword: search
    })
  }, [])
  return (
    <>
      {
        items?.map((item, index) =>
          <SearchRecipeItem key={index} item={item} onClick={onClick}/>
        )
      }
    </>
  )
}

function RecipeNotFound({onClick}) {

  useEffect(() => {
    if(onClick){
      onClick()
    }
  }, [])

  return (
    <Box
      sx={{
        height: 'calc(100vh - 220px)',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <Typography align={'center'} sx={{fontSize: '16px', fontWeight: 500, color: '#666666', lineHeight: '24px'}}>
        {RECIPE_SEARCH_COMMON.TEXT.NO_SEARCH_RESULT_TITLE}
      </Typography>
      <Typography align={'center'} sx={{fontSize: '14px', fontWeight: 400, color: '#888888', lineHeight: '20px'}}>
        {RECIPE_SEARCH_COMMON.TEXT.NO_SEARCH_RESULT_SUB_TITLE}
      </Typography>
    </Box>
  )
}

function SwitchAvoidRecipe({mainRef, isAvoid, isTooltip, setIsTooltip, onClick}) {
  return (
    <Stack direction={'row'} columnGap={'5px'} sx={{alignItems: 'center'}}>
      <Switch
        tabIndex={0}
        checked={isAvoid}
        onChange={onClick}
        onKeyDown={(e) => handleEnterPress(e, onClick)}
        inputProps={{title: ALT_STRING.SEARCH.CHECK_AVOID}}
        sx={{
          mx: '-10px',
          boxShadow: 'none',
          ".MuiButtonBase-root": {
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: 'transparent',
            }
          },
        }}
      />
      <Typography sx={{fontSize: '14px', lineHeight: '17px', fontWeight: 400, color: '#888888'}}>
        {RECIPE_SEARCH_COMMON.TEXT.AVOID}
      </Typography>
      <SvgInformationIcon
        alt={ALT_STRING.COMMON.ICON_TOOLTIP}
        tabIndex={0}
        onClick={(event) => setIsTooltip(event.currentTarget)}
        onKeyDown={(e) => handleEnterPress(e, (event) => setIsTooltip(event.currentTarget))}
        sx={{...clickBorderNone}}
      />
      <MenuPopover
        mainRef={mainRef}
        open={isTooltip}
        setOpen={setIsTooltip}
        arrow='top-right'
        onClose={() => setIsTooltip(null)}
        sx={{
          mt: '10px',
          ml: '10px',
          px: '20px',
          pt: '20px',
          pb: '20px',
          backgroundColor: 'white',
          border: '1px solid #E6E6E6',
          borderRadius: '8px',
          maxWidth: 310
        }}
      >
        <Typography
          sx={{fontSize: '14px', fontWeight: 400, lineHeight: '20px', color: '#666666', whiteSpace: 'pre-wrap'}}>
          {RECIPE_SEARCH_COMMON.TEXT.TOOLTIP}
        </Typography>
      </MenuPopover>
    </Stack>
  )
}

function RecipeSkeleton() {
  return (
    <>
      {[...Array(10)].map((item, index) =>
        <RecipeItemSkeleton key={index}/>
      )}
    </>
  );
}

function RecipeItemSkeleton() {
  return (
    <Stack direction="row" spacing="8px" sx={{my: '12px'}}>
      <Box sx={{position: 'relative'}}>
        <Skeleton variant="rounded" sx={{borderRadius: '8px', width: '100px', height: '100px'}}/>
      </Box>
      <Stack rowGap={'5px'} sx={{flexGrow: 1}}>
        <Skeleton variant="rectangular" height={20} sx={{borderRadius: 1}}/>
        <Skeleton variant="rectangular" height={14} sx={{mt: '3px', borderRadius: 1}}/>
        <Skeleton variant="rectangular" height={14} sx={{width: '60%', borderRadius: 1}}/>
        <Box sx={{flexGrow: 1}}/>

        <Stack direction="row" alignItems={'center'}>
          <Skeleton variant="rectangular" sx={{width: '50px', height: '16px', mr: '15px', borderRadius: 1}}/>
          <Skeleton variant="rectangular" sx={{width: '50px', height: '16px', borderRadius: 1}}/>
        </Stack>
      </Stack>
    </Stack>
  );
}