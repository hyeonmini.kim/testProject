import * as React from "react";
import {useRef, useState} from "react";
import {Box, Container, Divider, IconButton, LinearProgress, linearProgressClasses, Skeleton, Stack, Typography} from "@mui/material";
import Image from "../../../components/image";
import MenuPopover from "../../../components/menu-popover";
import {CAUTION_MATERIAL, NUTRIENTS_TEXT} from "../../../constant/recipe/detail/DetailConstants";
import PropTypes from "prop-types";
import NutrientsCarouselDotsDetail from "../../../components/recipe/detail/carousel/NutrientsCarouselDotsDetail";
import Carousel from "../../../components/recipe/detail/carousel";
import {useRecoilState, useRecoilValue} from "recoil";
import {
  recipeDetailPopupDialogOpenStatusSelector,
  recipeDetailPopupDialogTypeSelector
} from "../../../recoil/selector/recipe/detail/detailSelector";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {division} from "../../../utils/arrayUtils";
import {nvlString} from "../../../utils/formatString";
import {nvlNumber} from "../../../utils/formatNumber";
import {useBefuAuthContext} from "../../../auth/useAuthContext";
import {forMyPageEntrySelector} from "../../../recoil/selector/auth/userSelector";
import ScrollContainer from "react-indiana-drag-scroll";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";

export default function NutrientsInformationSection({csr, mainRef}) {
  const {isAuthenticated, isInitialized} = useBefuAuthContext();
  const userInfo = useRecoilValue(forMyPageEntrySelector)

  const [customized, setCustomized] = useState(null);

  const [openPopupDialog, setOpenPopupDialog] = useRecoilState(recipeDetailPopupDialogOpenStatusSelector);
  const [popupDialogType, setPopupDialogType] = useRecoilState(recipeDetailPopupDialogTypeSelector);

  const handleOpenCustomized = (event) => {
    setCustomized(event?.currentTarget);
  };

  const handleCloseCustomized = () => {
    setCustomized(null);
  };

  const handleClickOpenPopupDialog = (value) => {
    setOpenPopupDialog(true);
    setPopupDialogType(value);
  };

  const handleClosePopupDialog = () => {
    setOpenPopupDialog(false);
    setPopupDialogType(null);
  };
  const onTrackRecipeDetailDoNotEat = () =>{
    setHackleTrack(HACKLE_TRACK.RECIPE_DETAIL_DONOTEAT,{
      recipeid: csr?.recipe_key,
      recipename: csr?.recipe_name
    })
  }

  const onTrackRecipeDetailAlergy = () =>{
    setHackleTrack(HACKLE_TRACK.RECIPE_DETAIL_ALERGY,{
      recipeid: csr?.recipe_key,
      recipename: csr?.recipe_name
    })
  }

  return (
    <>
      {(csr?.recipe_restriction_list?.length > 0 || csr?.recipe_allergy_list?.length > 0) &&
        <>
          <Container maxWidth={'xs'} disableGutters sx={{px: '20px', pt: '101px', mt: '-51px'}}>
            <Box sx={{mb: '20px'}}>
              <Typography variant={'b_18_b_1l'} sx={{color: '#222222'}}>
                {CAUTION_MATERIAL?.TITLE}
              </Typography>
            </Box>

            {csr?.recipe_restriction_list?.length > 0 &&
              <CautionList items={csr?.recipe_restriction_list} isAllergies={false} onClick={onTrackRecipeDetailDoNotEat}/>}

            {csr?.recipe_allergy_list?.length > 0 && <CautionList items={csr?.recipe_allergy_list} isAllergies={true} onClick={onTrackRecipeDetailAlergy}/>}

            <Stack
              sx={{
                mb: '20px'
              }}
            />
          </Container>
          <Container maxWidth={'xs'} disableGutters sx={{px: '0px',}}>
            <Divider sx={{maxWidth: "xs"}}/>
          </Container>
        </>}

      {(isInitialized && isAuthenticated && !((userInfo?.type === "NORMAL_MEMBER" && userInfo?.grade === "LV1") && csr?.recipe_writer_info?.recipe_writer_type === "EXPERT")) &&
        (csr?.recipe_nutrient_list?.length > 0 &&
          <Container maxWidth={'xs'} disableGutters sx={{px: '0px', pt: '101px', mt: '-51px'}}>
            <Container maxWidth={'xs'} disableGutters sx={{px: '20px',}}>
              <Typography variant={'b_18_b'} sx={{color: '#222222'}}>
                {NUTRIENTS_TEXT?.TITLE(nvlNumber(csr?.recipe_cal))}
              </Typography>
            </Container>

            <Container maxWidth={'xs'} disableGutters sx={{px: '20px',}}>
              <Stack
                direction="row"
                sx={{
                  mt: '30px',
                  alignItems: 'center',
                }}
              >
                <Typography variant={'b1_16_m_1l'} sx={{color: '#222222'}}>
                  {NUTRIENTS_TEXT?.SUB_TITLE?.GOOD}
                </Typography>
                <SvgCommonIcons
                  alt={ALT_STRING.COMMON.ICON_TOOLTIP}
                  tabIndex={7}
                  type={ICON_TYPE?.ALERT_16PX}
                  sx={{ml: '4px', ...clickBorderNone}}
                  onClick={handleOpenCustomized}
                  onKeyDown={(e) => handleEnterPress(e, handleOpenCustomized)}
                />
              </Stack>
              <MenuPopover
                mainRef={mainRef}
                open={customized}
                setOpen={setCustomized}
                onClose={handleCloseCustomized}
                arrow='top-left-detail'
                sx={{
                  mt: '12px',
                  ml: '4px',
                  px: '20px',
                  pt: '20px',
                  pb: '20px',
                  backgroundColor: 'white',
                  border: '1px solid #E6E6E6',
                  borderRadius: '8px',
                  maxWidth: 310,
                  zIndex: 9999,
                }}>
                <Box tabIndex={7} onKeyDown={(e) => handleEnterPress(e, handleCloseCustomized)} sx={{...clickBorderNone}}>
                  <Typography variant={'b2_14_r'} sx={{color: '#666666'}}>
                    {NUTRIENTS_TEXT?.TOOLTIP(nvlString(csr?.recipe_writer_info?.recipe_writer_baby_nickname))}
                  </Typography>
                  <Typography fontWeight="400" fontSize="14px"
                              sx={{lineHeight: '20px', color: '#666666', whiteSpace: 'pre-wrap'}}>
                    {NUTRIENTS_TEXT?.TOOLTIP_FIXED}
                  </Typography>
                </Box>
              </MenuPopover>
            </Container>

            <NutrientsChartsCarousel csr={csr}
                                     goodNutrientArr={division(csr?.recipe_nutrient_list?.slice(0, csr?.recipe_nutrient_list?.length - 2), 6)}/>

            <Container maxWidth={'xs'} disableGutters sx={{px: '20px',}}>
              <Box sx={{mt: '40px', mb: '20px'}}>
                <Typography variant={'b1_16_m_1l'} sx={{color: '#222222'}}>
                  {NUTRIENTS_TEXT?.SUB_TITLE?.BAD}
                </Typography>
              </Box>
            </Container>

            <NutrientsCharts csr={csr} badNutrientArr={csr?.recipe_nutrient_list?.slice(-2)}/>
          </Container>)}
    </>
  );
}

function CautionList({items, isAllergies, onClick}) {

  const [openPopupDialog, setOpenPopupDialog] = useRecoilState(recipeDetailPopupDialogOpenStatusSelector);
  const [popupDialogType, setPopupDialogType] = useRecoilState(recipeDetailPopupDialogTypeSelector);

  const handleClickOpenPopupDialog = (value) => {
    if(onClick){
      onClick()
    }

    setOpenPopupDialog(true);
    setPopupDialogType(value);
  };

  const handleClosePopupDialog = () => {
    setOpenPopupDialog(false);
    setPopupDialogType(null);
  };

  return (
    <Box
      tabIndex={6}
      onKeyDown={(e) => handleEnterPress(e, () => handleClickOpenPopupDialog(isAllergies ? "allergies" : "avoid"))}
      sx={{...clickBorderNone}}
    >
      <Stack
        flexWrap="wrap"
        direction="row"
        sx={{
          alignItems: 'center',
          width: 'fit-content'
        }}
        onClick={() => handleClickOpenPopupDialog(isAllergies ? "allergies" : "avoid")}
      >
        <Typography variant={'b1_16_m_1l'} sx={{mr: '4px', color: '#222222'}}>
          {isAllergies ? CAUTION_MATERIAL?.ALLERGENS : CAUTION_MATERIAL?.AVOID_INGREDIENTS}
        </Typography>
        <Typography variant={'b1_16_b_1l'} sx={{color: 'primary.main', mr: '4px'}}>
          {items?.length}
        </Typography>
        <IconButton tabIndex={-1} sx={{
          p: 0,
          backgroundColor: "#FFFFFF",
          '&:hover': {
            boxShadow: 'none',
            backgroundColor: '#FFFFFF',
          },
        }}>
          <SvgCommonIcons alt={ALT_STRING.RECIPE_DETAIL.BTN_MOVE_ALLERGY} type={ICON_TYPE?.ARROW_RIGHT_16PX}/>
        </IconButton>
      </Stack>

      <ScrollContainer className="scroll-container">
        <Stack
          direction="row"
          justifyContent="flex-start"
          sx={{
            mt: '20px',
            pl: '20px',
            alignItems: 'center',
            "::-webkit-scrollbar": {display: 'none'},
          }}
        >
          {items?.length > 4 && <GradationBox/>}
          {
            (!items?.length ? [...Array(5)] : items)?.map((item, index) =>
              item ? (
                <Stack
                  direction="column"
                  alignItems="center"
                  key={index}
                  sx={{position: 'relative', mr: '10px', mb: '30px'}}
                >
                  <Image
                    alt={''}
                    src={item?.ingredient_img_path}
                    sx={{width: '50px', height: '50px', mb: '2px',}}
                    isLazy={false}
                    onClick={() => handleClickOpenPopupDialog(isAllergies ? "allergies" : "avoid")}
                  />
                  {isAllergies && (
                    item?.ingredient_baby_allergy_yn && (
                      <Box sx={{
                        width: '41px',
                        height: '14px',
                        position: 'absolute',
                        mt: '36px',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#86D6B4',
                        borderRadius: '3px',
                        typography: 'bxs_10_b_1l',
                        textAlign: 'center',
                        lineHeight: '13px',
                        color: '#FFFFFF',
                      }}>
                        {NUTRIENTS_TEXT?.MY_BABY}
                      </Box>))}
                  <Typography variant={'b2_14_r_1l'} sx={{color: '#666666', mt: '2px'}}>
                    {item?.ingredient_name}
                  </Typography>
                </Stack>
              ) : (
                <Stack
                  direction="column"
                  alignItems="center"
                  key={index}
                  sx={{mr: '10px', mb: '30px'}}
                >
                  <Skeleton variant="circular" width="50px" sx={{width: '50px', height: '50px', mb: '2px',}}/>
                  <Skeleton key={index} variant="rectangular" width="100%" sx={{height: '14px'}}/>
                </Stack>
              ),)}
          {items?.length > 4 && <GradationBehindBox/>}
        </Stack>
      </ScrollContainer>
    </Box>
  );
}

function GradationBox() {
  return (
    <Box
      sx={{
        zIndex: '9999',
        position: 'absolute',
        width: '70px',
        height: '75px',
        mb: '30px',
        right: '20px',
        background: 'linear-gradient(270deg, #FFFFFF 45.93%, rgba(255, 255, 255, 0) 85.38%)'
      }}
    />
  )
}

function GradationBehindBox() {
  return (
    <Stack direction="column" alignItems="center" sx={{mr: '10px', mb: '30px'}}>
      <Box variant="circular" width="50px" sx={{width: '50px', height: '75px'}}/>
    </Stack>
  )
}

function NutrientsChartsCarousel({csr, goodNutrientArr}) {
  const carouselRef = useRef(null);

  const carouselSettings = {
    dots: true,
    arrows: false,
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    edgeFriction: 0,
    // rtl: Boolean(theme.direction === 'rtl'),
    ...NutrientsCarouselDotsDetail({
      rounded: false,
      sx: {
        bottom: 0,
        mt: '10px',
        zIndex: 10,
        width: '100%',
        display: 'flex',
        position: 'relative',
        alignItems: 'flex-end',
      },
    }),
  };

  return (
    <>
      <Carousel ref={carouselRef} {...carouselSettings}>
        {goodNutrientArr &&
          (goodNutrientArr)?.map((nameList, index) =>
            nameList ? (
              <ChartsCarouselItem nameList={nameList}
                                  key={index}/>
            ) : (<SkeletonItem key={index}/>))}
      </Carousel>
    </>
  );
}

// ----------------------------------------------------------------------

function ChartsCarouselItem({nameList}) {
  return (
    <>
      {nameList?.map((name, index) =>
        name ? (
          <NutrientsChartsItem isLimit={false} name={name?.recipe_nutrient_name}
                               content={Number(name?.recipe_nutrient_quantity)} key={index}
                               sx={{[`& .${linearProgressClasses?.bar}`]: {backgroundColor: '#86D6B4'},}}/>
        ) : (
          <SkeletonItem key={index}/>
        ))}
    </>
  )
}

NutrientsCharts.propTypes = {
  csr: PropTypes?.object,
};

function NutrientsCharts({csr, badNutrientArr}) {
  return (
    <>
      {badNutrientArr &&
        (badNutrientArr)?.map((nutrient, index) =>
          nutrient ? (
            <ChartsItem name={nutrient?.recipe_nutrient_name} content={Number(nutrient?.recipe_nutrient_quantity)}
                        key={index}/>
          ) : (<SkeletonItem key={index}/>))}
    </>
  );
}

// ----------------------------------------------------------------------

function ChartsItem({name, content}) {
  return (
    <>
      {
        name ? (
          <NutrientsChartsItem isLimit={true} name={name} content={content}/>
        ) : (
          <SkeletonItem/>
        )}
    </>
  )
}

function NutrientsChartsItem({isLimit, name, content, sx}) {

  return (
    <>
      <Stack spacing="32px" sx={{my: '20px', mx: '20px'}}>
        <Stack
          direction="row"
          sx={{
            alignItems: 'center',
          }}
        >
          <Typography variant={'b2_14_r_1l'} sx={{minWidth: '64px', mr: '20px', color: '#666666'}}>
            {nvlString(name)}
          </Typography>
          {!isLimit ? (
            <LinearProgress
              variant="determinate"
              value={nvlNumber(content)}
              sx={{
                height: '10px',
                bgcolor: '#E6E6E6',
                borderRadius: '99px',
                [`& .${linearProgressClasses?.bar}`]: {borderRadius: '99px',},
                width: 1, ...sx
              }}
            />
          ) : (
            <>
              <LinearProgress
                variant="determinate"
                value={content < 100 ? content : 100}
                sx={{
                  height: '10px',
                  bgcolor: '#E6E6E6',
                  borderRadius: '0px',
                  borderTopLeftRadius: '99px',
                  borderBottomLeftRadius: '99px',
                  '&.MuiLinearProgress-root .MuiLinearProgress-bar': {borderRadius: content <= 100 ? '99px' : '0px',},
                  width: 1, ...sx
                }}
              />
              {isLimit && (
                <Stack
                  direction="column"
                >
                  <Divider orientation="vertical" sx={{height: '18px', borderStyle: 'dashed', borderColor: '#FF0000'}}
                           flexItem/>
                  <Typography fontWeight="400" fontSize="10px"
                              sx={{
                                position: 'absolute',
                                lineHeight: '10px',
                                mt: '20px',
                                right: '79px',
                                color: '#222222'
                              }}>
                    {NUTRIENTS_TEXT?.LIMIT_TAKE}
                  </Typography>
                </Stack>
              )}
              <LinearProgress
                variant="determinate"
                value={content > 100 ? 100 : 0}
                sx={{
                  height: '10px',
                  bgcolor: '#E6E6E6',
                  borderRadius: '0px',
                  borderTopRightRadius: '99px',
                  borderBottomRightRadius: '99px',
                  '&.MuiLinearProgress-root .MuiLinearProgress-bar': {borderRadius: '0px',},
                  width: '36px', ...sx
                }}
              />
            </>
          )}
          <Typography variant={'b2_14_r_1l'} sx={{minWidth: '36px', ml: '20px', textAlign: 'left', color: '#666666'}}>
            {content < 0 ? `-` : `${nvlNumber(content)}%`}
          </Typography>
        </Stack>
      </Stack>
    </>
  )
}

function SkeletonItem() {
  return (
    <Stack spacing="32px" sx={{my: '20px', mx: '20px'}}>
      <Stack
        direction="row"
        sx={{
          alignItems: 'center',
        }}
      >
        <Skeleton variant="rectangular" width="64px" sx={{height: '14px', mr: '20px'}}/>
        <Skeleton variant="rounded" sx={{flexGrow: 0.8, height: '10px', borderRadius: '99px',}}/>
        <Skeleton variant="rectangular" width="36px" sx={{height: '14px', ml: '20px'}}/>
      </Stack>
    </Stack>
  )
}
