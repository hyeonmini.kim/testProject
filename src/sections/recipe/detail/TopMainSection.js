import {Container, Stack, Typography} from "@mui/material";
import Box from "@mui/material/Box";
import {HEALTH_NOTE_TEXT} from "../../../constant/recipe/detail/DetailConstants";
import TextMaxLineWithAccordion from "../../../components/recipe/detail/text-max-line-with-accordion/TextMaxLineWithAccordion";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import * as React from "react";
import {nvlString, removeNBSP} from "../../../utils/formatString";
import {REPLACE_RECIPE_AGE_GROUP} from "../../../constant/recipe/Create";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";

export default function TopMainSection({csr}) {
  const onTrackDetailHealth = () => {
    setHackleTrack(HACKLE_TRACK.RECIPE_DETAIL_HEALTHNOTE,{
      recipeid: csr?.recipe_key,
      recipename: csr?.recipe_name
    })
  }
  return (
    <Container maxWidth={'xs'} disableGutters sx={{px: '20px', display: 'grid'}}>
      <Typography variant={'h1_24_b'} sx={{color: '#000000'}}>
        {removeNBSP(csr?.recipe_name)}
      </Typography>

      <Typography variant={'b1_16_r'} sx={{mt: '8px', color: '#222222'}}>
        {removeNBSP(csr?.recipe_desc)}
      </Typography>

      <Stack
        direction="row"
        alignItems="center"
        sx={{mt: '16px', mb: '40px',}}
      >
        <SvgCommonIcons type={ICON_TYPE?.COOK_TIME} sx={{mr: '3px'}}/>
        <Typography variant={'b2_14_r_1l'} sx={{color: '#666666'}}>
          {nvlString(csr?.recipe_lead_time)}
        </Typography>

        <SvgCommonIcons type={ICON_TYPE?.DOT} sx={{mx: '10px'}}/>

        <SvgCommonIcons type={ICON_TYPE?.DIFFICULTY} sx={{mr: '3px'}}/>
        <Typography variant={'b2_14_r_1l'} sx={{color: '#666666'}}>
          {nvlString(csr?.recipe_level)}
        </Typography>

        <SvgCommonIcons type={ICON_TYPE?.DOT} sx={{mx: '10px'}}/>

        <SvgCommonIcons type={ICON_TYPE?.BABY_FOOD} sx={{mr: '3px'}}/>
        <Typography variant={'b2_14_r_1l'} sx={{color: '#666666'}}>
          {nvlString(REPLACE_RECIPE_AGE_GROUP(csr?.recipe_babyfood_step))}
        </Typography>
      </Stack>

      {csr?.recipe_health_note && (
        <Box sx={{mb: '20px', pt: '25px', pb: '20px', px: '20px', backgroundColor: '#FAFAFA', borderRadius: '8px'}}>
          <Stack
            direction="row"
            sx={{
              alignItems: 'center',
            }}
          >
            <SvgCommonIcons type={ICON_TYPE?.PENCIL} sx={{mr: '10px'}}/>
            <Typography variant={'b1_16_b_1l'} sx={{lineHeight: '20px', color: '#222222'}}>
              {nvlString(csr?.recipe_writer_info?.recipe_writer_nickname)}{HEALTH_NOTE_TEXT}
            </Typography>
          </Stack>

          <TextMaxLineWithAccordion line={3} persistent
                                    sx={{lineHeight: '24px', color: '#666666'}}
                                    onClick={onTrackDetailHealth}>
            {removeNBSP(csr?.recipe_health_note)}
          </TextMaxLineWithAccordion>
        </Box>)}
    </Container>
  );
}
