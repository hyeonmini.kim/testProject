import {Container, Skeleton, Stack, Typography} from "@mui/material";
import * as React from "react";
import {useEffect, useState} from "react";
import {INGREDIENTS_TEXT} from "../../../constant/recipe/detail/DetailConstants";
import {nvlString, removeNBSP} from "../../../utils/formatString";
import {ALT_STRING} from "../../../constant/common/AltString";

export default function MakeStepsSection({csr}) {

  return (
    <>
      {
        (!csr?.recipe_order_list ? [...Array(1)] : csr?.recipe_order_list)?.map((step, index) =>
          step ? (
            <MakeStepsItem step={step} key={index}/>
          ) : (
            <SkeletonItem key={index}/>
          ),
        )}
    </>
  );
}

function MakeStepsItem({step}) {

  const [imageWidth, setImageWidth] = useState(1);
  const [imageHeight, setImageHeight] = useState(1);

  useEffect(() => {
    const {innerWidth} = window;

    setImageWidth(innerWidth < 440 ? innerWidth : 440)
    setImageHeight(innerWidth < 440 ? innerWidth / 4 * 3 : 440 / 4 * 3)
  }, [])

  return (
    <>
      <Stack
        flexWrap="wrap"
        direction="column"
        justifyContent="center"
        sx={{
          py: '30px',
        }}
      >
        <Container maxWidth={'md'} disableGutters sx={{px: '20px',}}>
          <Typography variant={'b1_16_m_1l'} sx={{color: '#222222'}}>
            {INGREDIENTS_TEXT?.STEP}{nvlString(step?.recipe_order)}
          </Typography>
        </Container>
        {
          step?.image_file_path
          &&
          <ImageItem
            alt={ALT_STRING.RECIPE_DETAIL.RECIPE_STEP(nvlString(step?.recipe_order))}
            url={step?.image_file_path}
          />
        }
        <Container maxWidth={'md'} disableGutters sx={{px: '20px',}}>
          <Typography variant={'b1_16_r'} sx={{color: '#666666'}}>
            {removeNBSP(step?.recipe_order_desc)}
          </Typography>
        </Container>
      </Stack>
    </>
  )
}

function ImageItem({alt, url}) {
  return (
    <img
      alt={alt}
      src={url}
      style={{marginTop: '10px', marginBottom: '10px', width: '100%', aspectRatio: '4/3', objectFit: 'cover'}}
    />
  )
}

function SkeletonItem() {
  return (
    <>
      <Skeleton variant="rectangular" width="100%"
                sx={{height: '270px', marginTop: '10px', marginBottom: '10px'}}/>
      <Container maxWidth={'md'} disableGutters sx={{px: '20px',}}>
        <Skeleton variant="rectangular" width="100%" sx={{height: '32px'}}/>
      </Container>
    </>
  )
}