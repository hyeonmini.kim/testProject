import * as React from "react";
import {forwardRef, useEffect, useRef, useState} from "react";
import {AppBar, Box, Container, Divider, IconButton, Slide, Stack, Tab, Tabs, Toolbar, Typography} from "@mui/material";
import {useRecoilState} from "recoil";
import {
  recipeDetailPopupDialogOpenStatusSelector,
  recipeDetailPopupDialogTypeSelector
} from "../../../recoil/selector/recipe/detail/detailSelector";
import Image from "../../../components/image";
import {
  CAUTION_MATERIAL,
  NUTRIENTS_POPUP_FATVITAMINS_TAB_LIST,
  NUTRIENTS_POPUP_MACRONUTRIENT_TAB_LIST,
  NUTRIENTS_POPUP_MINERALS_TAB_LIST,
  NUTRIENTS_POPUP_WATERVITAMINS_TAB_LIST,
  NUTRIENTS_TEXT,
  TABS_NUTRIENTS
} from "../../../constant/recipe/detail/DetailConstants";
import {ICON_COLOR, SvgCloseIcon} from "../../../constant/icons/icons";
import {SkeletonPostItem} from "../../../components/skeleton";
import {nvlNumber} from "../../../utils/formatNumber";
import {nvlString} from "../../../utils/formatString";
import {MainDialogLayout} from "../../../layouts/main/MainLayout";
import {clickBorderNone} from "../../../constant/common/Common";
import {ALT_STRING} from "../../../constant/common/AltString";

const Transition = forwardRef((props, ref) => <Slide direction="up" ref={ref} {...props} />);

export default function PopupDialog({csr}) {

  const [openPopupDialog, setOpenPopupDialog] = useRecoilState(recipeDetailPopupDialogOpenStatusSelector);
  const [popupDialogType, setPopupDialogType] = useRecoilState(recipeDetailPopupDialogTypeSelector);
  const [ssgItemType, setSsgItemType] = useState([]);
  const [nutrientsCurrentTab, setNutrientsCurrentTab] = useState('macronutrient');

  const topRef = useRef(null);

  const handleClosePopupDialog = () => {
    setOpenPopupDialog(false);
    setPopupDialogType(null);
  };

  const handleNutreintsTabClick = (value) => {
    switch (value) {
      case 'macronutrient' :
        setSsgItemType(NUTRIENTS_POPUP_MACRONUTRIENT_TAB_LIST)
        break;
      case 'minerals' :
        setSsgItemType(NUTRIENTS_POPUP_MINERALS_TAB_LIST)
        break;
      case 'waterVitamins' :
        setSsgItemType(NUTRIENTS_POPUP_WATERVITAMINS_TAB_LIST)
        break;
      case 'fatVitamins' :
        setSsgItemType(NUTRIENTS_POPUP_FATVITAMINS_TAB_LIST)
        break;
    }
  };

  useEffect(() => {
    if (topRef?.current) {
      topRef?.current?.scrollIntoView()
    }
  }, [nutrientsCurrentTab]);

  useEffect(() => {
    setNutrientsCurrentTab('macronutrient');
  }, [openPopupDialog]);

  useEffect(() => {
    popupDialogType === "avoid" ? setSsgItemType(csr?.recipe_restriction_list) : (popupDialogType === "allergies" ? setSsgItemType(csr?.recipe_allergy_list) : setSsgItemType(NUTRIENTS_POPUP_MACRONUTRIENT_TAB_LIST))
  }, [popupDialogType]);

  return (
    <MainDialogLayout fullScreen open={openPopupDialog} onClose={handleClosePopupDialog} TransitionComponent={Transition}
                      sx={{'& .MuiDialog-paper': {mx: 0, height: '100%', backgroundColor: 'white'}}}>
      <AppBar elevation={0} sx={{height: '58px', backgroundColor: 'white', position: 'fixed'}}>
        <Container maxWidth={'xs'} disableGutters>
          <Toolbar sx={{justifyContent: 'space-between', backgroundColor: 'white',}}>
            <Typography fontWeight="400" fontSize="20px" sx={{lineHeight: '26px', color: 'black', pl: '4px'}}>
              {popupDialogType === "nutrients" && CAUTION_MATERIAL?.NUTRIENTS}
            </Typography>
            <IconButton
              tabIndex={-1}
              disableFocusRipple
              onClick={handleClosePopupDialog}
              sx={{
              m: '-8px',
              backgroundColor: "#FFFFFF",
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: '#FFFFFF',
              },
            }}>
              <SvgCloseIcon alt={ALT_STRING.COMMON.BTN_CLOSE} tabIndex={0} color={ICON_COLOR?.BLACK} sx={{...clickBorderNone}}/>
            </IconButton>
          </Toolbar>
        </Container>

        {popupDialogType === 'nutrients' && (
          <Container maxWidth={'xs'} disableGutters sx={{px: '0px', backgroundColor: 'white'}}>
            <Tabs sx={{
              borderTop: 'none',
              height: '56px',
              borderBottom: '1px solid #E6E6E6',
              '& .MuiTabs-indicator': {backgroundColor: '#ECA548'},
              '& .MuiTab-root': {color: '#ECA548'},
              '& .Mui-selected': {color: '#ECA548'},
              '& .MuiButtonBase-root.MuiTab-root.Mui-selected': {borderBottom: '1px solid #ECA548'},
            }} variant="fullWidth" value={nutrientsCurrentTab}
                  onChange={(event, newValue) => setNutrientsCurrentTab(newValue)}>
              {TABS_NUTRIENTS?.slice(0, 4)?.map((tab, index) => (
                <Tab tabIndex={0} key={tab?.value} value={tab?.value} label={tab?.label} sx={{
                  minWidth: index !== 1 ? '100px' : '60px',
                  width: index !== 1 ? '100px' : '60px',
                  p: '16px 0px',
                  fontWeight: '700',
                  fontSize: '16px',
                  lineHeight: '24px',
                  '&:not(:last-of-type)': {marginRight: '0px',},
                  '&:not(.Mui-selected)': {fontWeight: '400', color: '#222222',},
                  ...clickBorderNone,
                }} onClick={(value) => handleNutreintsTabClick(tab?.value)}/>
              ))}
            </Tabs>
          </Container>
        )}
      </AppBar>

      {popupDialogType === 'nutrients' && (
        <Container maxWidth={'xs'} disableGutters sx={{px: '20px',}} ref={topRef}>
        </Container>
      )}

      <Container maxWidth={'xs'} disableGutters
                 sx={{mt: popupDialogType !== 'nutrients' ? '58px' : '114px', mb: '20px'}}>
        {popupDialogType !== 'nutrients' && (
          <Container maxWidth={'xs'} disableGutters sx={{px: '20px', py: '10px'}}>
            <Typography fontWeight="700" fontSize="20px"
                        sx={{lineHeight: '26px', color: 'black', whiteSpace: 'pre-wrap'}}>
              {popupDialogType === "avoid" ? NUTRIENTS_TEXT?.NUTRIENTS_AVOID_TITLE(nvlNumber(csr?.recipe_restriction_list?.length)) : NUTRIENTS_TEXT?.NUTRIENTS_ALLERGIES_TITLE(nvlNumber(csr?.recipe_allergy_list?.length))}
            </Typography>
          </Container>
        )}
        <Box>
          {ssgItemType &&
            (!ssgItemType?.length ? [...Array(3)] : ssgItemType)?.map((item, index) =>
              item ? (
                <PopupDialogContents item={item} key={index} isLast={index === ssgItemType?.length - 1}/>
              ) : (<SkeletonPostItem key={index}/>))}
        </Box>
      </Container>
    </MainDialogLayout>
  );
}

function PopupDialogContents({item, isLast}) {
  const [popupDialogType, setPopupDialogType] = useRecoilState(recipeDetailPopupDialogTypeSelector);

  return (
    <Container maxWidth={'xs'} disableGutters sx={{pt: '30px', px: '20px',}}>
      <Stack
        direction="row"
        sx={{
          mb: '20px',
          alignItems: 'center',
        }}
      >
        {popupDialogType !== "nutrients" && (
          <Image alt={''} src={nvlString(item?.ingredient_img_path)} sx={{width: '50px', height: '50px', mr: '10px'}}
                 isLazy={false}/>
        )}
        <Stack
          direction="column"
          alignItems="flex-start"
          spacing="5px"
        >
          <Typography fontWeight="700" fontSize="18px" sx={{lineHeight: '24px',}}>
            {nvlString(item?.ingredient_name)}
          </Typography>
          {popupDialogType === "avoid" ? (
            <Typography fontWeight="400" fontSize="16px" sx={{lineHeight: '24px',}}>
              {nvlString(item?.ingredient_age)}
            </Typography>) : (<></>)}
        </Stack>
      </Stack>
      <Typography fontWeight="400" fontSize="16px" sx={{lineHeight: '24px', mb: '30px', color: '#666666'}}>
        {nvlString(item?.ingredient_desc)}
      </Typography>
      {!isLast && <Divider sx={{maxWidth: "xs"}}/>}
    </Container>
  );
}