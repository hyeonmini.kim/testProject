import * as React from "react";
import {useEffect, useState} from "react";
import {Box, Container, IconButton, Skeleton, Stack, Typography} from "@mui/material";
import {REVIEW_ARRAY, REVIEW_TEXT} from "../../../constant/recipe/detail/DetailConstants";
import PropTypes from "prop-types";
import {SkeletonPostItem} from "../../../components/skeleton";
import {getNumberHundredThousand, nvlNumber} from "../../../utils/formatNumber";
import {SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import LoadingScreen from "../../../components/common/LoadingScreen";
import {postRegistRecipeReviewMutate} from "../../../api/detailApi";
import {useQueryClient} from "react-query";
import {debounce} from "lodash";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {useBefuAuthContext} from "../../../auth/useAuthContext";

export default function ReviewSection({csr, handlePushLoginPage}) {
  return (
    <>
      <Box sx={{mb: '20px'}}>
        <Typography variant={'b_18_b_1l'} sx={{color: '#222222'}}>
          {REVIEW_TEXT?.TITLE}
        </Typography>
      </Box>

      <Stack
        direction="row"
        justifyContent="space-evenly"
      >
        <ReviewCharts csr={csr} handlePushLoginPage={handlePushLoginPage}/>
      </Stack>

      <Container maxWidth={'xs'} disableGutters sx={{pb: '160px'}}>
      </Container>
    </>
  );
}

ReviewCharts.propTypes = {
  ssg: PropTypes?.object,
};

function ReviewCharts({csr, handlePushLoginPage}) {
  return (
    <>
      {REVIEW_ARRAY &&
        (REVIEW_ARRAY)?.map((item, index) =>
          item ? (
            <ReviewItem csr={csr} name={item?.name} image={item?.image} example={item?.example}
                        feedback={csr?.recipe_review_cnt_info?.[`recipe_review_cnt_${item?.example}`]}
                        isReviewed={csr?.recipe_review_cnt_info?.[`recipe_review_cnt_${item?.example}_user`]}
                        key={index} handlePushLoginPage={handlePushLoginPage}/>
          ) : (<SkeletonPostItem key={index}/>))}
    </>
  );
}

// ----------------------------------------------------------------------

function ReviewItem({csr, name, image, feedback, isReviewed, handlePushLoginPage}) {
  return (
    <>
      {
        name ? (
          <>
            <ReviewChartsItem csr={csr} name={name} image={image} feedback={Number(feedback)} isReviewed={isReviewed}
                              handlePushLoginPage={handlePushLoginPage}/>
          </>
        ) : (
          <SkeletonItem/>
        )}
    </>
  )
}

function ReviewChartsItem({csr, name, image, feedback, isReviewed, handlePushLoginPage}) {
  const [isReviewedValue, setIsReviewedValue] = useState(0);
  const [newFeedback, setNewFeedback] = useState(0);
  const queryClient = useQueryClient()
  const {isAuthenticated, isInitialized} = useBefuAuthContext();

  useEffect(() => {
    setIsReviewedValue(isReviewed)
    setNewFeedback(feedback)
  },)

  const {mutate: registRecipeReview, isLoading} = postRegistRecipeReviewMutate()
  const handleClickCheck = debounce(() => {
    if (isAuthenticated) {
      setHackleTrack(HACKLE_TRACK.RECIPE_DETAIL_REVIEW, {
        recipeid: csr?.recipe_key,
        recipename: csr?.recipe_name
      })
      registRecipeReview({
        recipe_key: csr?.recipe_key,
        recipe_select_review: name,
        recipe_review_yn: isReviewedValue ? "N" : "Y"
      }, {
        onSuccess: (data) => {
          queryClient?.invalidateQueries('getRecipeDetail')
        }
      })
    } else {
      handlePushLoginPage()
    }
  }, 300);

  return (
    <>
      <Stack
        tabIndex={10}
        direction="column"
        sx={{
          mx: '14.5px',
          alignItems: 'center',
          ...clickBorderNone
        }}
        onClick={handleClickCheck}
        onKeyDown={(e) => handleEnterPress(e, handleClickCheck)}
      >
        <IconButton tabIndex={-1} sx={{
          m: '-8px',
          backgroundColor: "#FFFFFF",
          '&:hover': {
            boxShadow: 'none',
            backgroundColor: '#FFFFFF',
          },
        }}>
          <SvgCommonIcons type={image}/>
        </IconButton>
        <Typography variant={'b2_14_r_1l'} sx={{mt: '8px', mb: '4px', color: '#222222'}}>
          {name}
        </Typography>
        <Typography variant={'b1_16_m_1l'}
                    sx={{color: isReviewedValue === 1 ? '#ECA548' : '#222222', fontWeight: isReviewedValue === 1 ? 700 : 400}}>
          {getNumberHundredThousand(nvlNumber(newFeedback))}
        </Typography>
      </Stack>
      {isLoading && <LoadingScreen/>}
    </>
  )
}

function SkeletonItem() {
  return (
    <Stack
      direction="column"
      sx={{
        mx: '14.5px',
        alignItems: 'center',
      }}
    >
      <Skeleton variant="circular" sx={{width: 30, height: 30}}/>
      <Skeleton variant="rounded" sx={{mt: '8px', mb: '4px', width: '50px', height: '14px'}}/>
      <Skeleton variant="rounded" sx={{width: '9px', height: '16px'}}/>
    </Stack>
  );
}