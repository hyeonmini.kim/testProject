import PropTypes from 'prop-types';
import * as React from 'react';
import {forwardRef, useEffect, useRef, useState} from 'react';
import {AppBar, Avatar, Badge, Box, Container, Paper, Skeleton, Slide, Stack, Toolbar, Typography} from '@mui/material';
import Carousel, {CarouselDotsDetail} from '../../../../components/recipe/detail/carousel';
import PictureCarouselDetail from "./PictureCarouselDetail";
import useStackNavigation from "../../../../hooks/useStackNavigation";
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import {ICON_COLOR, SvgCloseIcon} from "../../../../constant/icons/icons";
import {PATH_DONOTS_PAGE} from "../../../../routes/paths";
import {nvlString} from "../../../../utils/formatString";
import {STACK} from "../../../../constant/common/StackNavigation";
import {BADGE_ICON_TYPE, BadgeIcons} from "../../../../constant/icons/Badge/BadgeIcons";
import {useRecoilState, useRecoilValue, useSetRecoilState} from "recoil";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import {forMyPageEntrySelector} from "../../../../recoil/selector/auth/userSelector";
import {useBefuAuthContext} from "../../../../auth/useAuthContext";
import {clickBorderNone, COMMON_DIALOG_TYPE} from "../../../../constant/common/Common";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../../constant/common/AltString";
import {dialogSelector} from "../../../../recoil/selector/common/dialogSelector";
import {SIGN_UP_DIALOG} from "../../../../constant/sign-up/SignUp";
import {WITHDRAW_MEMBER_POPUP_TEXT} from "../../../../constant/recipe/detail/DetailConstants";
import {recipeDetailSlideIndexSelector} from "../../../../recoil/selector/recipe/detail/detailSelector";

// ----------------------------------------------------------------------

CarouselDetail.propTypes = {
  csr: PropTypes.object,
};

const Transition = forwardRef((props, ref) => <Slide direction="up" ref={ref} {...props} />);

export default function CarouselDetail({csr, open, setOpen, handlePushLoginPage}) {
  const {isAuthenticated, isInitialized} = useBefuAuthContext();
  const userInfo = useRecoilValue(forMyPageEntrySelector)
  const [slideIndex, setSlideIndex] = useRecoilState(recipeDetailSlideIndexSelector)
  const setDialogMessage = useSetRecoilState(dialogSelector)

  const carouselRef = useRef(null);

  const [linkTo, setLinkTo] = useState('')
  const [isDragging, setIsDragging] = useState(false)
  const [isDraggingPicture, setIsDraggingPicture] = useState(false)

  useEffect(() => {
    csr?.recipe_writer_info?.recipe_writer_type === 'NORMAL_MEMBER' ? setLinkTo(PATH_DONOTS_PAGE?.MY?.OTHERS(csr?.recipe_writer_info?.recipe_user_key?.toString())) : (csr?.recipe_writer_info?.recipe_writer_type === 'EXPERT' ? setLinkTo(PATH_DONOTS_PAGE?.MY?.EXPERTS(csr?.recipe_writer_info?.recipe_user_key?.toString())) : setLinkTo(""))

    return () => {
      setSlideIndex(0)
    }
  }, [])

  useEffect(() => {
    if (carouselRef?.current) {
      setIsDragging(false)
      carouselRef?.current?.slickGoTo(slideIndex, true);
    }
  }, [carouselRef?.current, slideIndex])

  const {navigation} = useStackNavigation()
  const handleClickAvatar = () => {
    if (isAuthenticated) {
      if (userInfo?.key?.toString() === csr?.recipe_writer_info?.recipe_user_key?.toString()) {
        navigation?.push(STACK?.RECIPE_DETAIL?.TYPE, PATH_DONOTS_PAGE?.MY?.SELF, {
          ...STACK?.RECIPE_DETAIL?.DATA,
          user_key: csr?.recipe_writer_info?.recipe_user_key
        })
      } else {
        csr?.recipe_writer_info?.recipe_writer_type === 'NORMAL_MEMBER' ? (
          navigation?.push(STACK?.RECIPE_DETAIL?.TYPE, PATH_DONOTS_PAGE?.MY?.OTHERS(csr?.recipe_writer_info?.recipe_user_key?.toString()), {
            ...STACK?.RECIPE_DETAIL?.DATA,
            user_key: csr?.recipe_writer_info?.recipe_user_key
          })) : (csr?.recipe_writer_info?.recipe_writer_type === 'EXPERT' ? (
            navigation?.push(STACK?.RECIPE_DETAIL?.TYPE, PATH_DONOTS_PAGE?.MY?.EXPERTS(csr?.recipe_writer_info?.recipe_user_key?.toString()), {
              ...STACK?.RECIPE_DETAIL?.DATA,
              user_key: csr?.recipe_writer_info?.recipe_user_key
            })) :
          setDialogMessage({
            type: COMMON_DIALOG_TYPE.ONE_BUTTON,
            message: WITHDRAW_MEMBER_POPUP_TEXT.TITLE,
            handleButton1: {text: SIGN_UP_DIALOG.CONFIRM},
          }))
      }
    } else {
      handlePushLoginPage()
    }
  }

  const handleClickOpen = () => {
    document?.activeElement?.blur()
    if (!isDragging) {
      setTimeout(() => {
        setOpen(true);
      }, 300)
    } else {
      setIsDragging(false)
    }
  };

  const handleClose = () => {
    if (!isDraggingPicture) {
      setTimeout(() => {
        setOpen(false);
      }, 300)
    } else {
      setIsDraggingPicture(false)
    }
  };

  const carouselSettings = {
    dots: true,
    arrows: false,
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    edgeFriction: 0,
    // initialSlide: slideIndex,
    beforeChange: (index, nextIndex) => {
      // setSlideIndex(nextIndex)
      setIsDragging(true)
    },
    afterChange: (index) => {
      setSlideIndex(index)
      setIsDragging(false)
    },
    // rtl: Boolean(theme.direction === 'rtl'),
    ...CarouselDotsDetail({
      rounded: false,
      sx: {
        bottom: 0,
        zIndex: 10,
        width: '100%',
        display: 'flex',
        position: 'absolute',
        paddingBottom: '89px',
        alignItems: 'flex-end',
      },
    }),
  };

  return (
    <>
      <Container maxWidth={'xs'} disableGutters sx={{px: '0px', position: 'relative', zIndex: 99}}>
        <Carousel ref={carouselRef} {...carouselSettings}>
          {(!csr?.image_file_list?.length ? [...Array(5)] : csr?.image_file_list)?.map((item, index) =>
            item ? (
              <CarouselItem url={nvlString(item?.image_file_path)} key={index} onClick={handleClickOpen}/>
            ) : (
              <Skeleton variant="rectangular" width="100%" sx={{height: 360}} key={index}/>
            ))}
        </Carousel>
      </Container>
      <Container maxWidth={'xs'} disableGutters sx={{px: '20px', position: 'absolute', zIndex: 99}}
                 onClick={handleClickAvatar}>
        <Stack
          tabIndex={2}
          onKeyDown={(e) => handleEnterPress(e, handleClickAvatar)}
          flexWrap="wrap"
          direction="row"
          justifyContent="flex-center"
          sx={{
            position: 'absolute',
            bottom: 0,
            zIndex: 99,
            alignItems: 'center',
            ...clickBorderNone
          }}
        >
          <Badge
            overlap="circular"
            anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
            badgeContent={
              csr?.recipe_writer_info?.recipe_writer_type === "NORMAL_MEMBER" && csr?.recipe_writer_info?.recipe_writer_grade === "LV1" ? (
                <BadgeIcons alt={ALT_STRING.COMMON.BADGE_LV1} type={BADGE_ICON_TYPE?.BADGE_LV1_16PX} sx={{mb: '84px', width: '16px'}}/>
              ) : (
                csr?.recipe_writer_info?.recipe_writer_type === "NORMAL_MEMBER" && csr?.recipe_writer_info?.recipe_writer_grade === "LV2" ? (
                  <BadgeIcons alt={ALT_STRING.COMMON.BADGE_LV2} type={BADGE_ICON_TYPE?.BADGE_LV2_16PX} sx={{mb: '84px', width: '16px'}}/>
                ) : (
                  csr?.recipe_writer_info?.recipe_writer_type === "NORMAL_MEMBER" && csr?.recipe_writer_info?.recipe_writer_grade === "LV3" ? (
                    <BadgeIcons alt={ALT_STRING.COMMON.BADGE_LV3} type={BADGE_ICON_TYPE?.BADGE_LV3_16PX} sx={{mb: '84px', width: '16px'}}/>
                  ) : (
                    csr?.recipe_writer_info?.recipe_writer_type === "EXPERT" ? (
                      <BadgeIcons alt={ALT_STRING.COMMON.BADGE_LV4} type={BADGE_ICON_TYPE?.BADGE_LV4_16PX}
                                  sx={{mb: '84px', width: '16px'}}/>
                    ) : (
                      <></>
                    )
                  )))
            }
          >
            {csr?.recipe_writer_info?.recipe_writer_thumbnail_path ?
              (<Avatar
                alt={ALT_STRING.COMMON.NICKNAME(nvlString(csr?.recipe_writer_info?.recipe_writer_nickname))}
                src={nvlString(csr?.recipe_writer_info?.recipe_writer_thumbnail_path)}
                sx={{
                  width: '30px',
                  height: '30px',
                  mb: '49px',
                  border: 1,
                  borderColor: '#E6E6E6'
                }}
              />) : (
                <SvgCommonIcons
                  alt={ALT_STRING.COMMON.NICKNAME(nvlString(csr?.recipe_writer_info?.recipe_writer_nickname))}
                  type={ICON_TYPE?.PROFILE_NO_IMAGE_30PX}
                  sx={{
                    mb: '49px',
                    width: '30px',
                    height: '30px',
                  }}/>
              )}
          </Badge>

          <Typography variant={'b1_16_r_1l'} sx={{pl: '10px', mb: '50.5px', zIndex: 99, color: '#FFFFFF'}}>
            {nvlString(csr?.recipe_writer_info?.recipe_writer_nickname)}
          </Typography>
        </Stack>
      </Container>

      <MainDialogLayout fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}
                        sx={{'& .MuiDialog-paper': {mx: 0, height: '100%', backgroundColor: 'black'}}}>
        <PictureCarouselDetail csr={csr} setIsDraggingPicture={setIsDraggingPicture}/>
        <AppBar position="absolute" elevation={0} sx={{height: '58px', backgroundColor: 'black'}}>
          <Container maxWidth={'xs'} disableGutters>
            <Toolbar
              disableGutters
              sx={{
                justifyContent: 'end',
                px: '20px'
              }}>
              <SvgCloseIcon alt={ALT_STRING.COMMON.BTN_CLOSE} color={ICON_COLOR?.WHITE} onClick={handleClose}/>
            </Toolbar>
          </Container>
        </AppBar>
      </MainDialogLayout>
    </>
  );
}

// ----------------------------------------------------------------------

CarouselItem.propTypes = {
  url: PropTypes?.string,
};

const mystyleDown = {
  background: 'linear-gradient(180deg, rgba(0, 0, 0, 0.8) 11.38%, rgba(217, 217, 217, 0) 91.87%);'
}

const mystyleUp = {
  background: 'linear-gradient(0deg, rgba(0, 0, 0, 0.8) 11.38%, rgba(217, 217, 217, 0) 91.87%);'
}

function CarouselItem({url, onClick}) {

  return (
    <Paper
      sx={{
        borderRadius: 0,
        overflow: 'hidden',
        position: 'relative',
      }}
      onClick={onClick}
    >
      <img
        alt={ALT_STRING.RECIPE_DETAIL.RECIPE_FIRST}
        src={url}
        style={{width: '100%', aspectRatio: '1/1', objectFit: 'cover'}}
      />

      <Box
        sx={{
          bottom: 0,
          zIndex: 9,
          width: '100%',
          height: '130px',
          textAlign: 'left',
          position: 'absolute',
          color: 'common.white',
          ...mystyleUp
        }}
      >
      </Box>
      <Box
        sx={{
          top: 0,
          zIndex: 9,
          width: '100%',
          height: '58px',
          textAlign: 'left',
          position: 'absolute',
          color: 'common.white',
          ...mystyleDown
        }}
      >
      </Box>
    </Paper>
  )
}
