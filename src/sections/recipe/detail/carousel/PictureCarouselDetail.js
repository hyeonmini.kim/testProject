import PropTypes from 'prop-types';
import * as React from 'react';
import {useEffect, useRef} from 'react';
// @mui
import {Box, Container, Skeleton,} from '@mui/material';
// components
import Carousel, {CarouselDotsDetail} from '../../../../components/recipe/detail/carousel';
import {nvlString} from "../../../../utils/formatString";
import {useRecoilState} from "recoil";
import {ALT_STRING} from "../../../../constant/common/AltString";
import {recipeDetailSlideIndexSelector} from "../../../../recoil/selector/recipe/detail/detailSelector";

// ----------------------------------------------------------------------

PictureCarouselDetail.propTypes = {
  csr: PropTypes?.object,
};

export default function PictureCarouselDetail({csr, setIsDraggingPicture}) {
  const [slideIndex, setSlideIndex] = useRecoilState(recipeDetailSlideIndexSelector)

  const carouselRef = useRef(null);

  useEffect(() => {
    if (carouselRef?.current) {
      setIsDraggingPicture(false)
      carouselRef?.current?.slickGoTo(slideIndex, true);
    }
  }, [carouselRef?.current, slideIndex])

  const carouselSettings = {
    dots: true,
    arrows: false,
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    edgeFriction: 0,
    // initialSlide: slideIndex,
    beforeChange: (index, nextIndex) => {
      // setSlideIndex(nextIndex)
      setIsDraggingPicture(true)
    },
    afterChange: (index) => {
      setSlideIndex(index)
      setIsDraggingPicture(false)
    },
    // rtl: Boolean(theme.direction === 'rtl'),
    ...CarouselDotsDetail({
      rounded: false,
      sx: {
        bottom: 0,
        zIndex: 10,
        display: 'flex',
        position: 'fixed',
        paddingBottom: '60px',
        alignItems: 'flex-end',
        margin: '0 auto',
        left: 0,
        right: 0,
      },
    }),
  };

  return (
    <>
      <Container maxWidth={'xs'} disableGutters
                 sx={{px: '0px', height: '100%', bgcolor: 'black', position: 'relative', zIndex: 99, m: 'auto'}}>
        <Carousel ref={carouselRef} {...carouselSettings}>
          {(!csr?.image_file_list?.length ? [...Array(5)] : csr?.image_file_list)?.map((item, index) =>
            item ? csr?.image_file_list?.length > 1 ? (
              <CarouselItem url={nvlString(item?.image_file_path)} key={index} num={(index + 1).toString()}/>
            ) : (
              <CarouselItem url={nvlString(item?.image_file_path)} key={index} num={''}/>
            ) : (
              <Skeleton variant="rectangular" width="100%" sx={{height: 360}} key={index}/>
            ))}
        </Carousel>
      </Container>
    </>
  );
}

// ----------------------------------------------------------------------

CarouselItem.propTypes = {
  url: PropTypes?.string,
};

function CarouselItem({url, num}) {

  return (
    <Box
      sx={{
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <img alt={ALT_STRING.RECIPE_DETAIL.RECIPE_IMAGE(num)} src={url} style={{width: '100%', aspectRatio: '1/1', objectFit: 'cover'}}/>
    </Box>
  )
}
