import {Box, Grid, Skeleton, Typography} from "@mui/material";
import {INGREDIENTS_TEXT} from "../../../constant/recipe/detail/DetailConstants";
import {nvlNumber} from "../../../utils/formatNumber";
import {nvlString} from "../../../utils/formatString";
import TextMaxLine from "../../../components/text-max-line";
import * as React from "react";

export default function IngredientsBasicsSection({csr}) {

  return (
    <Box sx={{display: 'grid'}}>
      <Typography variant={'b_18_b'} sx={{color: '#222222'}}>
        {INGREDIENTS_TEXT?.TITLE_FIRST(nvlNumber(csr?.recipe_servings))}
      </Typography>

      <Typography variant={'b_18_b'} sx={{color: '#222222'}}>
        {INGREDIENTS_TEXT?.TITLE_FIXED}
      </Typography>

      <Box sx={{mt: '30px', mb: '20px'}}>
        <Typography variant={'b1_16_m_1l'} sx={{color: '#222222'}}>
          {INGREDIENTS_TEXT?.SUB_TITLE?.BASICS}
        </Typography>
      </Box>

      <IngredientsContents contents={csr?.recipe_ingredient_list}/>
    </Box>
  );
}

function IngredientsContents({contents}) {

  return (
    <>
      {
        (!contents?.length ? [...Array(2)] : contents)?.map((content, index) =>
          content ? (
            content?.recipe_ingredient_category === "기본재료" && (
              <IngredientsItem name={content?.recipe_ingredient_name} amount={content?.recipe_ingredient_amount}
                               countUnit={content?.recipe_ingredient_main_countunit} key={index}/>)
          ) : (
            <SkeletonItem key={index}/>
          ),
        )}
    </>
  );
}

function IngredientsItem({name, amount, countUnit}) {

  return (
    <Grid container>
      <Grid item xs={6.3} sx={{mb: '15px'}}>
        <TextMaxLine open={true} variant={'b1_16_r'} line={3} sx={{whiteSpace: 'pre-wrap', color: '#666666'}}>
          {nvlString(name)}
        </TextMaxLine>
      </Grid>
      <Grid item xs={0.7}/>
      <Grid item xs={5}>
        <Typography open={true} variant={'b1_16_r'} sx={{whiteSpace: 'pre-wrap', color: '#666666'}}>
          {nvlNumber(amount)}{nvlString(countUnit)}
        </Typography>
      </Grid>
    </Grid>
  )
}

function SkeletonItem() {
  return (
    <Grid container>
      <Grid item xs={6.3} sx={{mb: '15px'}}>
        <Skeleton variant="rectangular" width="30px" sx={{height: '16px'}}/>
      </Grid>
      <Grid item xs={0.7}/>
      <Grid item xs={5}>
        <Skeleton variant="rectangular" width="30px" sx={{height: '16px'}}/>
      </Grid>
    </Grid>
  )
}