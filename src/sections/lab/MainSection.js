import {Box, Container} from "@mui/material";
import Image from "../../components/image";
import {ICON_COLOR, SvgBackIcon} from "../../constant/icons/icons";
import {ALT_STRING} from "../../constant/common/AltString";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../constant/common/Common";
import * as React from "react";
import {LAB_TYPE} from "../../constant/lab/LabCommon";
import {PATH_DONOTS_STATIC_PAGE} from "../../routes/paths";

export default function MainSection({type, backButtonClick}) {
  return (
    <>
      <Container maxWidth={'xs'} disableGutters sx={{px: '0px', position: 'absolute', left: 0, top: 0, zIndex: 999}}>
        <Box sx={{width: '48px', height: '48px', background: 'transparent', pl: '20px', pr: '48px', pb: '68px'}}
             onClick={backButtonClick}>
          <SvgBackIcon
            alt={ALT_STRING.COMMON.BTN_BACK}
            tabIndex={0}
            onKeyDown={(e) => handleEnterPress(e, backButtonClick)}
            color={ICON_COLOR?.BLACK}
            sx={{position: 'absolute', mt: '17px', zIndex: 99, ...clickBorderNone}}/>
        </Box>
      </Container>

      <Box sx={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', bgcolor: '#FFE0B7'}}>
        <img
          alt={type === LAB_TYPE.BABY_FOOD ? ALT_STRING.LAB.MAIN.BABY_FOOD : ALT_STRING.LAB.MAIN.NUTRITION}
          src={type === LAB_TYPE.BABY_FOOD ? PATH_DONOTS_STATIC_PAGE.ICONS('/images/icons/lab/babyfood_main.png') : PATH_DONOTS_STATIC_PAGE.ICONS('/images/icons/lab/nutrition_main.png')}
          // isLazy={false}
          style={{width: '100%', aspectRatio: '360/342', objectFit: 'cover'}}
        />
      </Box>
    </>
  )
}