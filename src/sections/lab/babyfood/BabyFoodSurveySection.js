import {useRecoilState} from "recoil";
import {
  labBabyFoodActiveStepSelector,
  labBabyFoodListSelector,
  labBabyFoodReasonListSelector,
  labBabyFoodSelectBrandCdSelector,
  labBabyFoodSelectBrandNameSelector,
  labBabyFoodSelectReasonListSelector,
  labIsBabyFoodEtcSelectedSelector
} from "../../../recoil/selector/lab/labSelector";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {BABYFOOD, BABYFOOD_ACTIVE_STEP, LAB_TYPE, SURVEY_COMMON, SURVEY_TYPE} from "../../../constant/lab/LabCommon";
import {postSurveyRegistMutate} from "../../../api/labApi";
import {Box, MobileStepper} from "@mui/material";
import {useQueryClient} from "react-query";
import BrandSelectSection from "../BrandSelectSection";
import CheckItemsSection from "../CheckItemsSection";
import LabBottomFixedButton from "../../../components/lab/LabBottomFixedButton";
import {replaceItemAtIndex} from "../../../utils/arrayUtils";
import LoadingScreen from "../../../components/common/LoadingScreen";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";

export default function BabyFoodSurveySection({onClose}) {
  const [labBabyFoodActiveStep, setLabBabyFoodActiveStep] = useRecoilState(labBabyFoodActiveStepSelector)
  const [labBabyFoodList, setLabBabyFoodList] = useRecoilState(labBabyFoodListSelector)
  const [labIsBabyFoodEtcSelected, setLabIsBabyFoodEtcSelected] = useRecoilState(labIsBabyFoodEtcSelectedSelector)
  const [labBabyFoodSelectBrandCd, setLabBabyFoodSelectBrandCd] = useRecoilState(labBabyFoodSelectBrandCdSelector)
  const [labBabyFoodSelectBrandName, setLabBabyFoodSelectBrandName] = useRecoilState(labBabyFoodSelectBrandNameSelector)
  const [labBabyFoodReasonList, setLabBabyFoodReasonList] = useRecoilState(labBabyFoodReasonListSelector)
  const [labBabyFoodSelectReasonList, setLabBabyFoodSelectReasonList] = useRecoilState(labBabyFoodSelectReasonListSelector)

  const [isDisabled, setIsDisabled] = useState(true)
  const [buttonText, setButtonText] = useState('')

  const queryClient = useQueryClient()

  const bottomBtnRef = useRef(null)

  const brandHandleClick = (id) => {
    let newList = labBabyFoodList;

    const selectedIndex = labBabyFoodList?.findIndex((listItem) => listItem?.value === true)
    const index = labBabyFoodList?.findIndex((listItem) => listItem?.text === id)

    if (selectedIndex >= 0 && index !== selectedIndex) {
      newList = replaceItemAtIndex(newList, selectedIndex, {
        text: newList[selectedIndex]?.text,
        value: false,
        image: newList[selectedIndex]?.image,
        code: newList[selectedIndex]?.code,
      })

      newList = replaceItemAtIndex(newList, index, {
        text: id,
        value: !newList[index]?.value,
        image: newList[index]?.image,
        code: newList[index]?.code,
      })
    } else {
      const index = labBabyFoodList?.findIndex((listItem) => listItem?.text === id)

      newList = replaceItemAtIndex(newList, index, {
        text: id,
        value: !newList[index]?.value,
        image: newList[index]?.image,
        code: newList[index]?.code,
      })
    }

    const idx = newList?.findIndex((listItem) => listItem?.value === true)
    if (idx >= 0) {
      setLabBabyFoodSelectBrandCd(labBabyFoodList[index]?.code)
      setLabBabyFoodSelectBrandName(labBabyFoodList[index]?.text)
    } else {
      setLabBabyFoodSelectBrandCd('')
      setLabBabyFoodSelectBrandName('')
    }

    setLabBabyFoodList(newList)

    const lastSelectedIndex = newList?.findIndex((listItem) => listItem?.value === true)

    if (lastSelectedIndex === 7) {
      setButtonText(SURVEY_COMMON.BUTTON.SHOW_RESULT)
      setLabIsBabyFoodEtcSelected(true)
    } else {
      setButtonText(SURVEY_COMMON.BUTTON.NEXT)
      setLabIsBabyFoodEtcSelected(false)
    }
  }

  const reasonHandleClick = (code) => {
    let newList = labBabyFoodReasonList;

    const index = labBabyFoodReasonList?.findIndex((listItem) => listItem?.code === code)
    newList = replaceItemAtIndex(newList, index, {
      text: newList[index]?.text,
      value: !newList[index]?.value,
      code: code,
    })

    const idx = labBabyFoodSelectReasonList?.findIndex((listItem) => listItem?.select_reason_cd === code)
    if (idx >= 0) {
      // 있으면 제거
      setLabBabyFoodSelectReasonList(labBabyFoodSelectReasonList?.filter(listItem => listItem?.select_reason_cd !== code))
    } else {
      // 없으므로 추가
      const newReason = {
        select_reason_cd: code,
        select_reason: labBabyFoodReasonList[index]?.text
      }
      setLabBabyFoodSelectReasonList([...labBabyFoodSelectReasonList, newReason])
    }

    setLabBabyFoodReasonList(newList)
  }

  const handleNextButton = () => {
    if (buttonText === SURVEY_COMMON.BUTTON.SHOW_RESULT) {
      if(labBabyFoodActiveStep === 1){
        setHackleTrack(HACKLE_TRACK.BABYFOOD_SELECT_STEP1)
      }else{
        setHackleTrack(HACKLE_TRACK.BABYFOOD_SELECT_STEP2)
      }
      registSurvey()
    } else {
      if(labBabyFoodActiveStep === 1){
        setHackleTrack(HACKLE_TRACK.BABYFOOD_SELECT_STEP1)
      }

      setLabBabyFoodActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };

  useEffect(() => {
    switch (labBabyFoodActiveStep) {
      case BABYFOOD_ACTIVE_STEP.BRAND :
        setButtonText(SURVEY_COMMON.BUTTON.NEXT)
        break;
      case BABYFOOD_ACTIVE_STEP.REASON :
        setButtonText(SURVEY_COMMON.BUTTON.SHOW_RESULT)
        break;
    }
  }, [labBabyFoodActiveStep])

  const {mutate: mutatePostSurveyRegist, isLoading} = postSurveyRegistMutate()

  const registSurvey = () => {
    mutatePostSurveyRegist({
      survey_list: [
        {
          survey_type: SURVEY_TYPE.SUR02.code,
          brand_cd: labBabyFoodSelectBrandCd,
          brand_name: labBabyFoodSelectBrandName,
          select_reason_list: labBabyFoodSelectReasonList,
        },
      ]
    }, {
      onSuccess: () => {
        queryClient?.invalidateQueries('getRegistSurveyYn')
        onClose()
      }
    })
  }

  return (
    <Box sx={{mx: '20px'}}>

      <MobileStepper
        variant="progress"
        steps={labIsBabyFoodEtcSelected ? 2 : 3}
        // position="static"
        activeStep={labBabyFoodActiveStep}
        sx={{
          position: 'fixed',
          height: '78px',
          pt: '65px',
          pb: '30px',
          top: 0,
          left: '20px',
          right: '20px',
          maxWidth: '400px',
          flexGrow: 1,
          px: '0px',
          '& .MuiMobileStepper-progress': {width: '100%', height: '10px', background: '#F5F5F5', borderRadius: '50px',},
          '& .MuiLinearProgress-bar': {borderRadius: '50px',}
        }}
      />

      {labBabyFoodActiveStep === BABYFOOD_ACTIVE_STEP.BRAND &&
        <BrandSelectSection
          type={LAB_TYPE.BABY_FOOD}
          handleClick={brandHandleClick}
          setIsDisabled={setIsDisabled}
          list={labBabyFoodList}
          title={SURVEY_COMMON.TEXT.BABYFOOD_BRAND_TITLE}
          etcText={BABYFOOD.BOB08.text}
        />}

      {labBabyFoodActiveStep === BABYFOOD_ACTIVE_STEP.REASON &&
        <CheckItemsSection
          handleClick={reasonHandleClick}
          setIsDisabled={setIsDisabled}
          list={labBabyFoodReasonList}
          title={SURVEY_COMMON.TEXT.REASON_TITLE}
        />}

      <LabBottomFixedButton btnRef={bottomBtnRef} handleNextButton={handleNextButton} isDisabled={isDisabled} text={buttonText}/>

      {isLoading && <LoadingScreen/>}

    </Box>
  )
}