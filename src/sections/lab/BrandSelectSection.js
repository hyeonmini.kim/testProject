import * as React from "react";
import {useEffect} from "react";
import {Box} from "@mui/material";
import LabSurveyTitle from "../../components/lab/LabSurveyTitle";
import LabBrandItems from "../../components/lab/LabBrandItems";

export default function BrandSelectSection({type, handleClick, setIsDisabled, list, title, etcText, surveyType}) {

  useEffect(() => {
    const index = list?.findIndex((listItem) => listItem?.value === true)
    setIsDisabled(index < 0)
  }, [list])

  return (
    <Box sx={{mt: '55px', marginBottom: '102px'}}>

      <LabSurveyTitle title={title}/>

      <LabBrandItems list={list} handleClick={handleClick} type={type} etcText={etcText} surveyType={surveyType}/>

    </Box>
  )
}