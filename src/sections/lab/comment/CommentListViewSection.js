import {MainDialogLayout} from "../../../layouts/main/MainLayout";
import {Box, Container, Divider, Fade, Typography} from "@mui/material";
import HeaderDialogLayout from "../../../layouts/lab/HeaderDialogLayout";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {ACTION_TYPE, CONTENTS_TYPE, LAB_TEXT, POST_TYPE, TTF_TYPE} from "../../../constant/lab/LabCommon";
import TransitionTextField from "../../../components/common/TransitionTextField";
import {
  getCommentListMutate,
  getFirstCommentListMutate,
  postCommentRegistMutate,
  postDeclareMutate,
  postPostLikeMutate,
  putCommentUpdateMutate
} from "../../../api/labApi";
import {DEFAULT_PAGE_PARAMS, PAGINATION} from "../../../constant/common/Pagination";
import usePagination from "../../../hooks/usePagination";
import LoadingScreen from "../../../components/common/LoadingScreen";
import CommentItem, {CommentItemSkeleton} from "./CommentItem";
import {useRecoilState, useSetRecoilState} from "recoil";
import {toastMessageSelector} from "../../../recoil/selector/common/toastSelector";
import {TOAST_TYPE} from "../../../constant/common/Common";
import {Z_INDEX} from "../../../constant/common/ZIndex";
import LabGoToTopButton from "../../../components/lab/LabGoToTopButton";
import {labCommentCommentKeySelector} from "../../../recoil/selector/lab/labSelector";

export default function CommentListViewSection({open, item, onChange, onModify, onDelete, onLike, onDeclare, onClose}) {
  const mainRef = useRef(null)

  const [text, setText] = useState('')
  const [count, setCount] = useState(0)
  const [refresh, setRefresh] = useState(false)

  const setToastMessage = useSetRecoilState(toastMessageSelector)

  const {
    param,
    items,
    setItems,
    setTarget,
    pageOffset,
    hasNextOffset,
    initPagination,
    isLoading,
    getPaginationItems,
  } = usePagination(getCommentListMutate, 15)

  param.current = {
    params: {
      post_key: item?.post_key
    }
  }

  // 초기 댓글 리스트 가져오기
  const {mutate: mutateFirstPostList, isLoading: isLoadingFirstCommentList} = getFirstCommentListMutate()
  useEffect(() => {
    mutateFirstPostList({
      params: {
        ...DEFAULT_PAGE_PARAMS.params,
        ...param.current?.params,
      }
    }, {
      onSuccess: (data) => {
        setCount(data?.totalElements)
        initPagination(data?.comment_list)
      }
    })
  }, [refresh])

  // 댓글 등록 클로저
  const {mutate: mutateCommentRegist, isLoading: isLoadingCommentRegist} = postCommentRegistMutate()
  const handleClickRegist = (text) => {
    mutateCommentRegist({
      post_key: item?.post_key,
      comment: text,
    }, {
      onSuccess: () => {
        setRefresh(!refresh)
        if (onChange) {
          onChange(item?.post_key, {
            comment_cnt: item?.comment_cnt + 1
          })
        }
      }
    })
  }

  // 댓글 수정 클로저
  const {mutate: mutatePutCommentUpdate, isLoading: isLoadingPutCommentUpdate} = putCommentUpdateMutate()
  const handleClickModify = (item, text, callback) => {
    mutatePutCommentUpdate({
      comment_key: item?.comment_key,
      comment: text,
      update_delete: ACTION_TYPE.MODIFY,
    }, {
      onSuccess: () => {
        handleChangeItems(item?.comment_key, {
          comment: text
        })
        if (callback) {
          callback()
        }
      }
    })
  }

  // 댓글 삭제 클로저
  const handleClickDelete = (item, callback) => {
    mutatePutCommentUpdate({
      comment_key: item?.comment_key,
      update_delete: ACTION_TYPE.DELETE,
    }, {
      onSuccess: () => {
        handleDeleteItems(item?.comment_key)
        if (callback) {
          callback()
        }
      }
    })
  }

  // 댓글 좋아요 클로저
  const {mutate: mutatePostPostLike, isLoading: isLoadingPostLike} = postPostLikeMutate()
  const handleClickLike = (item) => {
    mutatePostPostLike({
      content_type: CONTENTS_TYPE.COMMENT,
      content_key: item?.comment_key,
      regist_cancel: item?.great_yn === 'Y' ? ACTION_TYPE.CANCEL : ACTION_TYPE.REGIST,
    }, {
      onSuccess: () => {
        if (item?.great_yn === 'Y' && item?.great_cnt > 0) {
          handleChangeItems(item?.comment_key, {
            great_yn: 'N',
            great_cnt: item?.great_cnt - 1
          })
        } else {
          handleChangeItems(item?.comment_key, {
            great_yn: 'Y',
            great_cnt: item?.great_cnt + 1
          })
        }
      }
    })
  }

  // 댓글 신고
  const {mutate: mutatePostDeclare, isLoading: isLoadingPostDeclare} = postDeclareMutate()
  const handleClickDeclare = (item, code, callback) => {
    mutatePostDeclare({
      content_type: CONTENTS_TYPE.COMMENT,
      content_key: item?.comment_key,
      declare_content_cd: code?.value,
    }, {
      onSuccess: () => {
        setToastMessage({
          type: TOAST_TYPE.BOTTOM_HEADER,
          message: LAB_TEXT.COMMENT.DECLARE.MESSAGE,
        })
      },
      onSettled: () => {
        if (callback) {
          callback()
        }
      }
    })
  }

  const handleDeleteItems = (key) => {
    setCount((prev) => prev - 1)
    setItems(items.filter((item) => item?.comment_key !== key))

    if (onChange) {
      onChange(item?.post_key, {
        comment_cnt: item?.comment_cnt - 1
      })
    }
  }

  const handleChangeItems = (key, value) => {
    const newItems = [...items]
    const index = newItems.findIndex((item) => item?.comment_key === key)
    if (index != -1) {
      newItems[index] = {
        ...newItems[index],
        ...value
      }
    }
    setItems(newItems)
  }

  const isLoadingScreen = () => {
    if (isLoading || isLoadingFirstCommentList || isLoadingCommentRegist
      || isLoadingPostLike || isLoadingPostDeclare || isLoadingPutCommentUpdate) {
      return true
    }
    return false
  }


  return (
    <MainDialogLayout
      mainRef={mainRef}
      fullScreen
      open={!!open}
      onClose={onClose}
      TransitionComponent={Fade}
      bgcolor={'#FAFAFA'}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <HeaderDialogLayout onBack={onClose}>
        <Container disableGutters maxWidth={'xs'}>
          <Container disableGutters sx={{bgcolor: 'white', px: '20px'}}>

            {/* 게시글 상세 */}
            <CommentItem
              type={POST_TYPE.VIEW}
              item={{
                ...item,
                key: item?.post_key,
                contents: item?.post_contents,
              }}
              onModify={onModify}
              onDelete={onDelete}
              onLike={onLike}
              onDeclare={onDeclare}
            />

          </Container>

          <Divider/>

          <Box sx={{mx: '20px'}}>

            {/* 댓글 입력 */}
            <TransitionTextField
              type={TTF_TYPE.REGIST}
              text={text}
              onRegist={handleClickRegist}
              placeholder={{
                fold: LAB_TEXT.COMMENT.PLACEHOLDER.COMMENT,
                expand: LAB_TEXT.COMMENT.PLACEHOLDER.COMMENT_EXPAND
              }}
              sx={{mt: '20px', mb: '20px'}}
            />

            {/* 댓글 리스트 */}
            {
              items === PAGINATION.INIT_DATA
                ? <CommentSkeleton/>
                : items?.length ? (
                  <>
                    <CommentList
                      items={items}
                      onModify={handleClickModify}
                      onDelete={handleClickDelete}
                      onLike={handleClickLike}
                      onDeclare={handleClickDeclare}
                    />
                    {hasNextOffset.current && <Box ref={setTarget}/>}
                  </>
                ) : <CommentNotFound/>
            }
          </Box>

          {/* 로딩 스크린 */}
          {isLoadingScreen() && <LoadingScreen/>}

          {/* 탑 버튼 */}
          <LabGoToTopButton mainRef={mainRef}/>

        </Container>
      </HeaderDialogLayout>
    </MainDialogLayout>
  )
}


function CommentList({items, onModify, onDelete, onLike, onDeclare}) {
  const [commentKey, setCommentKey] = useRecoilState(labCommentCommentKeySelector)

  return (
    <>
      {
        items?.map((item, index) =>
          <React.Fragment key={item?.comment_key}>
            <CommentItem
              focus={item?.comment_key === commentKey}
              type={POST_TYPE.COMMENT}
              item={{
                ...item,
                key: item?.comment_key,
                contents: item?.comment,
              }}
              onModify={onModify}
              onDelete={onDelete}
              onLike={onLike}
              onDeclare={onDeclare}
            />
            {index < items?.length - 1 && <Divider sx={{mx: '-20px', my: '10px'}}/>}
          </React.Fragment>
        )
      }
    </>
  )
}

function CommentSkeleton() {
  return (
    <>
      {[...Array(5)].map((item, index) =>
        <React.Fragment key={index}>
          <CommentItemSkeleton/>
          <Divider/>
        </React.Fragment>
      )}
    </>
  );
}

function CommentNotFound() {
  return (
    <Box
      sx={{
        height: '100px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <Typography align={'center'} sx={{color: '#222222', fontWeight: 400, fontSize: '16px', lineHeight: '16px'}}>
        {LAB_TEXT.COMMENT.NOT_FOUND_COMMENT}
      </Typography>

    </Box>
  )
}