import * as React from "react";
import {useState} from "react";
import {useRecoilState, useSetRecoilState} from "recoil";
import {forMyPageEntrySelector} from "../../../recoil/selector/auth/userSelector";
import {dialogSelector} from "../../../recoil/selector/common/dialogSelector";
import {LAB_TEXT, POST_TYPE} from "../../../constant/lab/LabCommon";
import {clickBorderNone, COMMON_DIALOG_TYPE} from "../../../constant/common/Common";
import {Box, Divider, Skeleton, Stack, Typography} from "@mui/material";
import LikeIconButton from "../../../components/common/LikeIconButton";
import CommentModifySection from "./CommentModifySection";
import BottomDialog from "../../../layouts/lab/BottomDialog";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import AvatarProfile from "../../../components/lab/AvatarProfile";
import {getMMDDFromDate} from "../../../utils/formatString";
import {getRandom} from "../../../utils/formatNumber";
import {labCommentShowCommentModifySelector} from "../../../recoil/selector/lab/labSelector";

export default function CommentItem({type = POST_TYPE.LIST, item, onModify, onDelete, onLike, onDeclare, onOpenView, focus, sx}) {
  const [openModify, setOpenModify] = useState(false)

  const [user, setUser] = useRecoilState(forMyPageEntrySelector)
  const [showModify, setShowModify] = useRecoilState(labCommentShowCommentModifySelector)
  const setDialogMessage = useSetRecoilState(dialogSelector)

  const handleCloseModify = () => {
    setOpenModify(false)
  }

  const handleModify = (text) => {
    if (onModify) {
      onModify(item, text, handleCloseModify)
    }
  }

  const handleDeleteDialog = () => {
    setDialogMessage({
      type: COMMON_DIALOG_TYPE.TWO_BUTTON,
      message: LAB_TEXT.COMMENT.DIALOG.DELETE,
      leftButtonProps: {
        text: LAB_TEXT.COMMENT.BUTTON.CANCEL,
      },
      rightButtonProps: {
        text: LAB_TEXT.COMMENT.BUTTON.CONFIRM,
        onClick: () => onDelete(item)
      },
    })
  }

  const isMyPost = () => {
    return item?.recipe_writer_info?.recipe_user_key == user?.key
  }

  const handleOpenView = () => {
    if (type === POST_TYPE.LIST) {
      if (onOpenView) {
        onOpenView(item?.post_key)
      }
    }
  }

  const handleLike = () => {
    if (onLike) {
      onLike(item)
    }
  }

  const handleDeclare = (code, callback) => {
    if (onDeclare) {
      onDeclare(item, code, callback)
    }
  }

  const sxFocusItem = {
    bgcolor: 'rgba(255,200,20,0.06)',
    mx: '-20px',
    px: '20px',
    py: '30px',
    my: '-10px',
  }

  const handleClickModify = () => {
    setOpenModify(true)
    setShowModify(true)
  }

  return (
    <Stack spacing={'16px'} sx={focus ? {...sxFocusItem, ...sx} : {py: '20px', ...sx}}>

      {/* 게시글 아바타 */}
      <AvatarProfile
        avatar={item?.recipe_writer_info?.recipe_writer_thumbnail_path}
        type={item?.recipe_writer_info?.recipe_writer_type}
        grade={item?.recipe_writer_info?.recipe_writer_grade}
        nickname={item?.recipe_writer_info?.recipe_writer_nickname}
        date={getMMDDFromDate(item?.created_datetime)}
      />

      {/* 게시글 */}
      <Typography
        sx={{fontWeight: 400, fontSize: '16px', lineHeight: '24px', color: '#222222', whiteSpace: 'pre-wrap', wordBreak: 'break-all'}}>
        {item?.contents}
      </Typography>

      {/* 게시글 하단 버튼 모음 */}
      <Stack direction={'row'} sx={{height: '24px', justifyContent: 'space-between'}}>

        <Stack spacing={'16px'} direction={'row'}>

          {/* 댓글 달기 버튼 */}
          <CommentButton type={type} count={item?.comment_cnt} onClick={handleOpenView}/>

          {/* 좋아요 버튼 */}
          <LikeIconButton like={item?.great_cnt} isCheck={item?.great_yn === 'Y'} onClick={handleLike}/>

        </Stack>

        {/* 수정, 삭제, 신고 버튼*/}
        {
          isMyPost()
            ? <CommentModifyDelete onModify={handleClickModify} onDelete={handleDeleteDialog}/>
            : <CommentDeclaration onDeclare={handleDeclare}/>
        }

      </Stack>

      {/* 수정 다이얼로그 */}
      {openModify && <CommentModifySection open={openModify} text={item?.contents} onModify={handleModify} onClose={handleCloseModify}/>}

    </Stack>
  )
}

function CommentButton({type, count, onClick}) {
  switch (type) {
    case POST_TYPE.LIST:
      return <CommentList onClick={onClick} count={count}/>
    case POST_TYPE.VIEW:
      return <CommentView count={count}/>
    case POST_TYPE.COMMENT:
      return <></>
  }
}

function CommentView({count}) {
  return (
    <Typography sx={{fontWeight: 700, fontSize: '14px', lineHeight: '24px', color: '#666666'}}>
      {LAB_TEXT.COMMENT.BUTTON.COUNT(count)}
    </Typography>
  )
}

function CommentList({onClick, count}) {
  return (
    <Typography
      tabIndex={0}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
      onClick={onClick}
      sx={{fontWeight: 700, fontSize: '14px', lineHeight: '24px', color: '#666666', cursor: 'pointer'}}
    >
      {count > 0 ? LAB_TEXT.COMMENT.BUTTON.COUNT(count) : LAB_TEXT.COMMENT.BUTTON.ADD}
    </Typography>
  )
}


function CommentModifyDelete({onModify, onDelete}) {
  return (
    <Stack direction={'row'} spacing={'16px'}>
      {/* 수정 */}
      <Typography
        tabIndex={0}
        onKeyDown={(e) => handleEnterPress(e, onModify)}
        onClick={onModify}
        sx={{fontWeight: 400, fontSize: '14px', lineHeight: '24px', color: '#666666', cursor: 'pointer'}}
      >
        {LAB_TEXT.COMMENT.BUTTON.MODIFY}
      </Typography>

      {/* 삭제 */}
      <Typography
        tabIndex={0}
        onKeyDown={(e) => handleEnterPress(e, onDelete)}
        onClick={onDelete}
        sx={{fontWeight: 400, fontSize: '14px', lineHeight: '24px', color: '#666666', cursor: 'pointer'}}
      >
        {LAB_TEXT.COMMENT.BUTTON.DELETE}
      </Typography>
    </Stack>
  )
}

function CommentDeclaration({onDeclare}) {
  const [open, setOpen] = useState(false)

  const handleClose = () => {
    setOpen(false)
  }

  const handleDeclare = (code) => {
    if (onDeclare) {
      onDeclare(code, handleClose)
    }
  }

  return (
    <>
      <Typography
        tabIndex={0}
        onKeyDown={(e) => handleEnterPress(e, () => setOpen(true))}
        onClick={() => setOpen(true)}
        sx={{fontWeight: 400, fontSize: '14px', lineHeight: '24px', color: '#666666'}}
      >
        {LAB_TEXT.COMMENT.BUTTON.DECLARATION}
      </Typography>

      {/* 하단 정렬 다이얼로그 */}
      <BottomDialog open={open} onClose={handleClose}>
        <CommentDeclarationView onClick={handleDeclare}/>
      </BottomDialog>
    </>
  )
}

function CommentDeclarationView({onClick}) {
  const handleClick = (code) => {
    if (onClick) {
      onClick(code)
    }
  }

  return (
    <Box sx={{mt: '6px', textAlign: 'start'}}>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 500,
          fontSize: '16px',
          lineHeight: '24px',
          color: '#222222',
          paddingY: '20px'
        }}
      >
        {LAB_TEXT.COMMENT.DECLARE.TITLE}
      </Typography>
      <Divider sx={{height: '1px', backgroundColor: '#E6E6E6'}}/>

      {LAB_TEXT.COMMENT.DECLARE.LIST.map((item, index) => (
        <Typography
          tabIndex={1}
          key={index}
          sx={{
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
            paddingY: '20px',
            ...clickBorderNone
          }}
          onClick={() => handleClick(item)}
          onKeyDown={(e) => handleEnterPress(e, () => handleClick(item))}
        >
          {item?.title}
        </Typography>))}
    </Box>
  )
}

export function CommentItemSkeleton() {
  return (
    <Stack spacing={'16px'} sx={{py: '20px'}}>

      {/* 아바타 스켈레톤 */}
      <Stack spacing={'10px'} direction={'row'} sx={{alignItems: 'center', position: 'relative'}}>
        <Skeleton variant="circular" sx={{height: '40px', width: '40px'}}/>
        <Stack spacing={'8px'} sx={{height: '40px', display: 'flex', flexDirection: 'column'}}>
          <Skeleton variant="rectangular" sx={{height: '16px', width: '100px', borderRadius: '10px'}}/>
          <Skeleton variant="rectangular" sx={{height: '16px', width: '50px', borderRadius: '10px'}}/>
        </Stack>
      </Stack>

      {/* 게시글 스켈레톤 */}
      <Stack spacing={'8px'} sx={{py: '4px'}}>
        <Skeleton variant="rectangular" sx={{height: '16px', width: getRandom(200) + 100, borderRadius: '10px'}}/>
        <Skeleton variant="rectangular" sx={{height: '16px', width: getRandom(100) + 200, borderRadius: '10px'}}/>
      </Stack>

      {/* 게시글 하단 스켈레톤 */}
      <Stack direction={'row'} sx={{height: '24px', justifyContent: 'space-between'}}>
        <Stack spacing={'16px'} direction={'row'}>
          {/* 댓글 달기 버튼 */}
          <Skeleton variant="rectangular" sx={{height: '24px', width: '40px', borderRadius: '10px'}}/>
          {/* 좋아요 버튼 */}
          <Skeleton variant="rectangular" sx={{height: '24px', width: '40px', borderRadius: '10px'}}/>
        </Stack>
        {/* 신고 버튼*/}
        <Skeleton variant="rectangular" sx={{height: '24px', width: '40px', borderRadius: '10px'}}/>
      </Stack>

    </Stack>
  )
}