import * as React from "react";
import {useEffect, useState} from "react";
import {useRecoilState, useSetRecoilState} from "recoil";
import {ACTION_TYPE, CALLBACK_TYPE, CONTENTS_TYPE, LAB_TEXT} from "../../../constant/lab/LabCommon";
import {toastMessageSelector} from "../../../recoil/selector/common/toastSelector";
import {getPostDetailMutate, postDeclareMutate, postPostLikeMutate, putPostUpdateMutate} from "../../../api/labApi";
import {TOAST_TYPE} from "../../../constant/common/Common";
import CommentListViewSection from "./CommentListViewSection";
import LoadingScreen from "../../../components/common/LoadingScreen";
import {labCommentItemSelector, labCommentPostKeySelector} from "../../../recoil/selector/lab/labSelector";
import {ERRORS, GET_FRONT_ERROR_MESSAGE} from "../../../constant/common/Error";

export default function CommentView({onBack}) {
  const [refresh, setRefresh] = useState(false)

  const [item, setItem] = useRecoilState(labCommentItemSelector)
  const [postKey, setPostKey] = useRecoilState(labCommentPostKeySelector)

  const setToastMessage = useSetRecoilState(toastMessageSelector)

  const {mutate: mutatePostDetail, isLoading: isLoadingPostDetail} = getPostDetailMutate()
  useEffect(() => {
    if (postKey) {
      mutatePostDetail({
        post_key: postKey
      }, {
        onSuccess: (item) => {
          if (!item?.post_key) {
            handleError()
          }
          setItem(item)
        },
        onError: () => {
          handleError()
        }
      })
    }
  }, [postKey, refresh])

  const handleError = () => {
    setToastMessage({
      type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
      message: GET_FRONT_ERROR_MESSAGE(ERRORS.ECONNABORTED)
    })
    onBack({
      type: CALLBACK_TYPE.ERROR
    })
  }

  // 게시글 수정 클로저
  const {mutate: mutatePutPostUpdate, isLoading: isLoadingPutPostUpdate} = putPostUpdateMutate()
  const handleClickModify = (item, text, callback) => {
    mutatePutPostUpdate({
      post_key: item?.post_key,
      post_contents: text,
      update_delete: ACTION_TYPE.MODIFY,
    }, {
      onSuccess: () => {
        setRefresh(!refresh)
        if (callback) {
          callback()
        }
      }
    })
  }

  // 게시글 삭제 클로저
  const handleClickDelete = (item, callback) => {
    mutatePutPostUpdate({
      post_key: item?.post_key,
      update_delete: ACTION_TYPE.DELETE,
    }, {
      onSuccess: () => {
        onBack({
          type: CALLBACK_TYPE.DELETE,
          item: item
        })
        if (callback) {
          callback()
        }
      }
    })
  }

  // 게시글 좋아요
  const {mutate: mutatePostPostLike, isLoading: isLoadingPostLike} = postPostLikeMutate()
  const handleClickLike = (item) => {
    mutatePostPostLike({
      content_type: CONTENTS_TYPE.POST,
      content_key: item?.post_key,
      regist_cancel: item?.great_yn === 'Y' ? ACTION_TYPE.CANCEL : ACTION_TYPE.REGIST,
    }, {
      onSuccess: () => {
        setRefresh(!refresh)
      }
    })
  }

  // 게시글 신고
  const {mutate: mutatePostDeclare, isLoading: isLoadingPostDeclare} = postDeclareMutate()
  const handleClickDeclare = (item, code, callback) => {
    mutatePostDeclare({
      content_type: CONTENTS_TYPE.POST,
      content_key: item?.post_key,
      declare_content_cd: code?.value,
    }, {
      onSuccess: () => {
        setToastMessage({
          type: TOAST_TYPE.BOTTOM_HEADER,
          message: LAB_TEXT.COMMENT.DECLARE.MESSAGE,
        })
      },
      onSettled: () => {
        if (callback) {
          callback()
        }
      }
    })
  }

  const handleChangeItem = (key, value) => {
    const newItem = {
      ...item
      , ...value
    }
    setItem(newItem)
  }

  const isLoadingScreen = () => {
    if (isLoadingPostDetail || isLoadingPostLike || isLoadingPostDeclare || isLoadingPutPostUpdate) {
      return true
    }
    return false
  }

  if (item) {
    return (
      <>
        <CommentListViewSection
          open={!!item}
          item={item}
          onChange={handleChangeItem}
          onModify={handleClickModify}
          onDelete={handleClickDelete}
          onLike={handleClickLike}
          onDeclare={handleClickDeclare}
          onClose={() => onBack({
            type: CALLBACK_TYPE.CHANGE,
            item: item,
          })}
        />

        {/* 로딩 스크린 */}
        {isLoadingScreen() && <LoadingScreen/>}
      </>
    )
  }

}