import {MainDialogLayout} from "../../../layouts/main/MainLayout";
import {Container, Fade} from "@mui/material";
import HeaderDialogLayout from "../../../layouts/lab/HeaderDialogLayout";
import {LAB_TEXT, TTF_TYPE} from "../../../constant/lab/LabCommon";
import TransitionTextField from "../../../components/common/TransitionTextField";
import {useEffect, useState} from "react";
import BottomFixedButton from "../../../components/common/BottomFixedButton";
import {useRecoilState, useSetRecoilState} from "recoil";
import {COMMON_DIALOG_TYPE} from "../../../constant/common/Common";
import {dialogSelector} from "../../../recoil/selector/common/dialogSelector";
import {Z_INDEX} from "../../../constant/common/ZIndex";
import {labCommentShowCommentModifySelector} from "../../../recoil/selector/lab/labSelector";

export default function CommentModifySection({open, text = '', onModify, onClose}) {
  const [temp, setTemp] = useState(text)
  const [showModify, setShowModify] = useRecoilState(labCommentShowCommentModifySelector)

  useEffect(() => {
    if (!showModify) {
      onClose()
    }
  }, [showModify])

  const setDialogMessage = useSetRecoilState(dialogSelector)
  const handleModify = () => {
    if (text?.length < 2) {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.ONE_BUTTON,
        message: LAB_TEXT.COMMENT.DIALOG.MIN,
        handleButton1: {
          text: LAB_TEXT.COMMENT.BUTTON.CONFIRM,
        }
      })
    } else {
      if (onModify) {
        onModify(temp)
      }
    }
  }

  return (
    <MainDialogLayout
      fullScreen
      open={!!open}
      onClose={onClose}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <HeaderDialogLayout title={LAB_TEXT.COMMENT.MODIFY} onClose={onClose}>
        <Container disableGutters maxWidth={'xs'} sx={{px: '20px'}}>
          <TransitionTextField
            type={TTF_TYPE.MODIFY}
            text={temp}
            onChange={(value) => setTemp(value)}
            sx={{mt: '20px'}}
          />
          <BottomFixedButton text={LAB_TEXT.COMMENT.MODIFY} onClick={handleModify}/>
        </Container>
      </HeaderDialogLayout>
    </MainDialogLayout>
  )
}