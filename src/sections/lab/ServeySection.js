import {MainDialogLayout} from "../../layouts/main/MainLayout";
import {Container, Fade} from "@mui/material";
import * as React from "react";
import {useEffect} from "react";
import HeaderDialogLayout from "../../layouts/lab/HeaderDialogLayout";
import {LAB_TYPE} from "../../constant/lab/LabCommon";
import {useResetRecoilState} from "recoil";
import {
  labBabyFoodActiveStepSelector,
  labBabyFoodListSelector,
  labBabyFoodReasonListSelector,
  labBabyFoodSelectBrandCdSelector,
  labBabyFoodSelectBrandNameSelector,
  labBabyFoodSelectReasonListSelector,
  labIsBabyFoodEtcSelectedSelector,
  labIsNutritionSUR03EtcSelectedSelector,
  labIsNutritionSUR04EtcSelectedSelector,
  labIsNutritionSUR05EtcSelectedSelector,
  labIsSUR03SelectedSelector,
  labIsSUR04SelectedSelector,
  labIsSUR05SelectedSelector,
  labNutritionActiveStepListSelector,
  labNutritionActiveStepSelector,
  labNutritionSelectedLengthSelector,
  labNutritionSelectListSelector,
  labNutritionSUR03ListSelector,
  labNutritionSUR03ReasonListSelector,
  labNutritionSUR03SelectBrandCdSelector,
  labNutritionSUR03SelectBrandNameSelector,
  labNutritionSUR03SelectReasonListSelector,
  labNutritionSUR04ListSelector,
  labNutritionSUR04ReasonListSelector,
  labNutritionSUR04SelectBrandCdSelector,
  labNutritionSUR04SelectBrandNameSelector,
  labNutritionSUR04SelectReasonListSelector,
  labNutritionSUR05ListSelector,
  labNutritionSUR05ReasonListSelector,
  labNutritionSUR05SelectBrandCdSelector,
  labNutritionSUR05SelectBrandNameSelector,
  labNutritionSUR05SelectReasonListSelector
} from "../../recoil/selector/lab/labSelector";
import BabyFoodSurveySection from "./babyfood/BabyFoodSurveySection";
import NutritionSurveySection from "./nutrition/NutritionSurveySection";

export default function ServeySection({open, onClose, handleBackButton}) {
  const resetLabBabyFoodActiveStep = useResetRecoilState(labBabyFoodActiveStepSelector)
  const resetLabIsBabyFoodEtcSelected = useResetRecoilState(labIsBabyFoodEtcSelectedSelector)
  const resetLabBabyFoodList = useResetRecoilState(labBabyFoodListSelector)
  const resetLabBabyFoodReasonList = useResetRecoilState(labBabyFoodReasonListSelector)
  const resetLabBabyFoodSelectBrandCd = useResetRecoilState(labBabyFoodSelectBrandCdSelector)
  const resetLabBabyFoodSelectBrandName = useResetRecoilState(labBabyFoodSelectBrandNameSelector)
  const resetLabBabyFoodSelectReasonList = useResetRecoilState(labBabyFoodSelectReasonListSelector)

  const resetLabNutritionActiveStep = useResetRecoilState(labNutritionActiveStepSelector)
  const resetLabNutritionActiveStepList = useResetRecoilState(labNutritionActiveStepListSelector)
  const resetLabNutritionSelectList = useResetRecoilState(labNutritionSelectListSelector)
  const resetLabNutritionSelectedLength = useResetRecoilState(labNutritionSelectedLengthSelector)

  const resetLabIsSUR03Selected = useResetRecoilState(labIsSUR03SelectedSelector)
  const resetLabIsSUR04Selected = useResetRecoilState(labIsSUR04SelectedSelector)
  const resetLabIsSUR05Selected = useResetRecoilState(labIsSUR05SelectedSelector)

  const resetLabNutritionSUR03List = useResetRecoilState(labNutritionSUR03ListSelector)
  const resetLabIsNutritionSUR03EtcSelected = useResetRecoilState(labIsNutritionSUR03EtcSelectedSelector)
  const resetLabNutritionSUR03SelectBrandCd = useResetRecoilState(labNutritionSUR03SelectBrandCdSelector)
  const resetLabNutritionSUR03SelectBrandName = useResetRecoilState(labNutritionSUR03SelectBrandNameSelector)
  const resetLabNutritionSUR03ReasonList = useResetRecoilState(labNutritionSUR03ReasonListSelector)
  const resetLabNutritionSUR03SelectReasonList = useResetRecoilState(labNutritionSUR03SelectReasonListSelector)

  const resetLabNutritionSUR04List = useResetRecoilState(labNutritionSUR04ListSelector)
  const resetLabIsNutritionSUR04EtcSelected = useResetRecoilState(labIsNutritionSUR04EtcSelectedSelector)
  const resetLabNutritionSUR04SelectBrandCd = useResetRecoilState(labNutritionSUR04SelectBrandCdSelector)
  const resetLabNutritionSUR04SelectBrandName = useResetRecoilState(labNutritionSUR04SelectBrandNameSelector)
  const resetLabNutritionSUR04ReasonList = useResetRecoilState(labNutritionSUR04ReasonListSelector)
  const resetLabNutritionSUR04SelectReasonList = useResetRecoilState(labNutritionSUR04SelectReasonListSelector)

  const resetLabNutritionSUR05List = useResetRecoilState(labNutritionSUR05ListSelector)
  const resetLabIsNutritionSUR05EtcSelected = useResetRecoilState(labIsNutritionSUR05EtcSelectedSelector)
  const resetLabNutritionSUR05SelectBrandCd = useResetRecoilState(labNutritionSUR05SelectBrandCdSelector)
  const resetLabNutritionSUR05SelectBrandName = useResetRecoilState(labNutritionSUR05SelectBrandNameSelector)
  const resetLabNutritionSUR05ReasonList = useResetRecoilState(labNutritionSUR05ReasonListSelector)
  const resetLabNutritionSUR05SelectReasonList = useResetRecoilState(labNutritionSUR05SelectReasonListSelector)

  useEffect(() => {
    return () => {
      if (open === LAB_TYPE.BABY_FOOD) {
        resetLabBabyFoodActiveStep()
        resetLabIsBabyFoodEtcSelected()
        resetLabBabyFoodList()
        resetLabBabyFoodReasonList()
        resetLabBabyFoodSelectBrandCd()
        resetLabBabyFoodSelectBrandName()
        resetLabBabyFoodSelectReasonList()
      } else {
        resetLabNutritionActiveStep()
        resetLabNutritionActiveStepList()
        resetLabNutritionSelectList()
        resetLabNutritionSelectedLength()

        resetLabIsSUR03Selected()
        resetLabNutritionSUR03List()
        resetLabIsNutritionSUR03EtcSelected()
        resetLabNutritionSUR03SelectBrandCd()
        resetLabNutritionSUR03SelectBrandName()
        resetLabNutritionSUR03ReasonList()
        resetLabNutritionSUR03SelectReasonList()

        resetLabIsSUR04Selected()
        resetLabNutritionSUR04List()
        resetLabIsNutritionSUR04EtcSelected()
        resetLabNutritionSUR04SelectBrandCd()
        resetLabNutritionSUR04SelectBrandName()
        resetLabNutritionSUR04ReasonList()
        resetLabNutritionSUR04SelectReasonList()

        resetLabIsSUR05Selected()
        resetLabNutritionSUR05List()
        resetLabIsNutritionSUR05EtcSelected()
        resetLabNutritionSUR05SelectBrandCd()
        resetLabNutritionSUR05SelectBrandName()
        resetLabNutritionSUR05ReasonList()
        resetLabNutritionSUR05SelectReasonList()
      }
    }
  }, [open])

  return (
    <MainDialogLayout
      fullScreen
      open={!!open}
      onClose={onClose}
      TransitionComponent={Fade}
      sx={{'& .MuiDialog-paper': {mx: 0}}}
    >
      <HeaderDialogLayout
        onBack={handleBackButton}
      >
        <Container disableGutters maxWidth={'xs'}>
          {open === LAB_TYPE.BABY_FOOD ? <BabyFoodSurveySection onClose={onClose}/> : <NutritionSurveySection onClose={onClose}/>}
        </Container>
      </HeaderDialogLayout>
    </MainDialogLayout>
  )
}
