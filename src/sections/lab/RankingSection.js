import {Box, Card, CardContent, Container, LinearProgress, linearProgressClasses, Skeleton, Stack, Typography} from "@mui/material";
import {Swiper, SwiperSlide} from "swiper/react";
import {Pagination} from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {getNumberTenThousand} from "../../utils/formatNumber";
import {getSurveyList} from "../../api/labApi";
import {SvgLabIcons} from "../../constant/icons/lab/LabIcons";
import {removePercent} from "../../utils/formatString";
import {LAB_TYPE} from "../../constant/lab/LabCommon";
import {useRecoilState} from "recoil";
import {labSelectedSurveyTypeSelector} from "../../recoil/selector/lab/labSelector";
import LabRankingCardSkeletonItem from "../../components/lab/LabRankingCardSkeletonItem";
import {handleEnterPress} from "../../utils/onKeyDownUtils";

export default function RankingSection({type, surveyTypeParam}) {
  const swiperRef = useRef(null)
  const [swiperIndex, setSwiperIndex] = useState(0);
  const [selectedSurveyType, setSelectedSurveyType] = useRecoilState(labSelectedSurveyTypeSelector);

  useEffect(() => {
    setSwiperIndex(0)
    swiperRef?.current?.slideTo(0, 0)
  }, [selectedSurveyType])

  const {data: surveyList} = getSurveyList({survey_type: surveyTypeParam})

  const handleSwiper = (orderNum) => {
    setSwiperIndex(parseInt(orderNum))
    swiperRef?.current?.slideTo(parseInt(orderNum), 0)
  }

  return (
    <>
      <Container disableGutters sx={{mt: '40px'}}>
        <Swiper
          onSwiper={(swiper) => {
            swiperRef.current = swiper
          }}
          onActiveIndexChange={(swiperCore) => {
            setSwiperIndex(swiperCore.activeIndex);
          }}
          slidesPerView={1.03}
          modules={[Pagination]}
          spaceBetween={20}
          style={{
            paddingTop: '40px',
            marginTop: '-40px',
            paddingLeft: '87px',
            // marginRight: '-87px',
            paddingRight: '87px',
            paddingBottom: '20px',
            marginBottom: '-20px',

          }}
        >
          {(surveyList?.length ? surveyList.slice(0, 5) : [...Array(5)]).map((item, index) =>
            item ? (
              <SwiperSlide key={index}>
                <RankingCardItem key={index} item={item} onKeyDown={(e) => handleEnterPress(e, () => handleSwiper(index))}/>
              </SwiperSlide>
            ) : (
              <SwiperSlide key={index}>
                <LabRankingCardSkeletonItem/>
              </SwiperSlide>
            )
          )}
        </Swiper>

        <Box sx={{height: '10px'}}/>

        <RankingChartItem surveyList={surveyList} swiperIndex={swiperIndex} type={type}/>

      </Container>
    </>
  )
}

function RankingCardItem({item, onKeyDown}) {
  return (
    <Card
      tabIndex={0}
      onKeyDown={(e) => onKeyDown(e)}
      sx={{
        // width: '145px',
        p: '20px',
        borderRadius: '8px',
        boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.08)',
      }}
    >
      <Typography fontFamily="Quantico" sx={{fontSize: '30px', fontWeight: 400, lineHeight: '30px', color: '#3F2404'}}>
        {item?.survey_order}
      </Typography>
      <Box>
        <SvgLabIcons type={item?.brand_cd} sx={{width: '107px', height: '92px', aspectRatio: '1/1', background: 'white', m: '0 auto'}}/>
      </Box>
      <CardContent sx={{mt: '16px', p: 0, "&:last-child": {pb: 0}}}>
        <Typography sx={{fontSize: '16px', fontWeight: 700, lineHeight: '24px', color: '#3F2404', mb: '4px'}}>
          {item?.brand_name}
        </Typography>
        <Typography sx={{fontSize: '14px', fontWeight: 400, lineHeight: '20px', color: '#3F2404'}}>
          {getNumberTenThousand(item?.survey_num)}명이 선택 ({item?.survey_rate})
        </Typography>
      </CardContent>
    </Card>
  )
}

function RankingChartItem({surveyList, swiperIndex, type}) {
  return (
    <>
      {(surveyList?.length && surveyList?.length > swiperIndex ? surveyList[swiperIndex]?.select_reason_list : [...Array(type === LAB_TYPE.BABY_FOOD ? 7 : 6)]).map((item, index) =>
        item ? (
          <Stack direction="row" key={index} sx={{position: 'relative', mx: '20px', my: '10px'}}>
            <LinearProgress
              variant="determinate"
              value={Number(removePercent(item?.select_reason_rate))}
              sx={{
                height: '38px',
                bgcolor: '#F5F5F5',
                borderRadius: '4px',
                [`& .${linearProgressClasses?.bar}`]: {backgroundColor: index === 0 ? '#FFE0B7' : '#E6E6E6', borderRadius: '4px',},
                width: 1,
              }}
            />
            <Typography sx={{
              position: 'absolute',
              left: '16px',
              my: '11px',
              fontSize: '14px',
              fontWeight: index === 0 ? 700 : 400,
              lineHeight: '16px',
              color: '#222222'
            }}>
              {item?.select_reason}
            </Typography>
            <Typography sx={{
              position: 'absolute',
              right: '16px',
              my: '7px',
              fontSize: '14px',
              fontWeight: 700,
              lineHeight: '24px',
              color: '#222222'
            }}>
              {item?.select_reason_rate}
            </Typography>
          </Stack>
        ) : (
          <Stack direction="row" key={index} sx={{position: 'relative', mx: '20px', my: '10px'}}>
            <Skeleton variant="rectangular" sx={{width: '100%', height: '38px', borderRadius: '5px'}}/>
          </Stack>
        )
      )}
    </>
  )
}
