import {Box, Checkbox, Divider, Stack, Typography} from "@mui/material";
import {ACTION_TYPE, CALLBACK_TYPE, CONTENTS_TYPE, LAB_TEXT, POST_TYPE, TTF_TYPE} from "../../constant/lab/LabCommon";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import * as React from "react";
import {useEffect, useState} from "react";
import TransitionTextField from "../../components/common/TransitionTextField";
import {ICON_COLOR, ICON_SIZE, SvgCheckRoundIcon} from "../../constant/icons/icons";
import {ALT_STRING} from "../../constant/common/AltString";
import BottomDialog from "../../layouts/lab/BottomDialog";
import {clickBorderNone, INIT, TOAST_TYPE} from "../../constant/common/Common";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import usePagination from "../../hooks/usePagination";
import {
  getFirstPostListMutate,
  getPostListMutate,
  postDeclareMutate,
  postPostLikeMutate,
  postPostRegistMutate,
  putPostUpdateMutate
} from "../../api/labApi";
import LoadingScreen from "../../components/common/LoadingScreen";
import {DEFAULT_PAGE_PARAMS, PAGINATION} from "../../constant/common/Pagination";
import {useRecoilState, useSetRecoilState} from "recoil";
import {forMyPageEntrySelector} from "../../recoil/selector/auth/userSelector";
import AvatarProfile from "../../components/lab/AvatarProfile";
import CommentItem, {CommentItemSkeleton} from "./comment/CommentItem";
import {toastMessageSelector} from "../../recoil/selector/common/toastSelector";
import {STACK} from "../../constant/common/StackNavigation";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import useStackNavigation from "../../hooks/useStackNavigation";
import {labCommentCheckSelector, labCommentFetchSelector, labCommentOrderSelector} from "../../recoil/selector/lab/labSelector";
import LabGoToTopButton from "../../components/lab/LabGoToTopButton";
import {sxFixedCenterMainLayout} from "../../layouts/main/MainLayout";

export default function CommentSection({mainRef, type}) {
  const [text, setText] = useState('')
  const [count, setCount] = useState(0)
  const [refresh, setRefresh] = useState(false)

  const [check, setCheck] = useRecoilState(labCommentCheckSelector)
  const [order, setOrder] = useRecoilState(labCommentOrderSelector)

  const [user, setUser] = useRecoilState(forMyPageEntrySelector)
  const setToastMessage = useSetRecoilState(toastMessageSelector)

  const [fetch, setFetch] = useRecoilState(labCommentFetchSelector)

  const {
    param,
    items,
    setItems,
    setTarget,
    pageOffset,
    hasNextOffset,
    initPagination,
    isLoading,
    getPaginationItems,
  } = usePagination(getPostListMutate, 15)

  param.current = {
    params: {
      survey_type: type,
      writer_yn: check ? 'Y' : 'N',
      sort: order?.value
    }
  }

  // 초기 게시글 리스트 가져오기
  const {mutate: mutateFirstPostList, isLoading: isLoadingFirstPostList} = getFirstPostListMutate()
  useEffect(() => {
    if (fetch === INIT) return
    else if (fetch) {

      if (fetch?.item) {
        if (fetch?.type === CALLBACK_TYPE.DELETE) {
          setCount(fetch?.count - 1)
          initPagination(fetch?.items?.filter((item) => item?.post_key !== fetch?.postKey))
        } else {
          setCount(fetch?.count)
        }
        if (fetch?.type === CALLBACK_TYPE.CHANGE) {
          const newItems = [...fetch?.items]
          const index = newItems.findIndex((item) => item?.post_key === fetch?.postKey)
          if (index != -1) {
            newItems[index] = {
              ...fetch?.item
            }
          }
          initPagination(newItems)
        }
      } else {
        initPagination(fetch?.items)
      }

      setTimeout(() => {
        mainRef?.current?.scrollTo(0, fetch?.scrollY)
      }, 100)

      setFetch(null)

    } else {
      mutateFirstPostList({
        params: {
          ...DEFAULT_PAGE_PARAMS.params,
          ...param.current?.params,
        }
      }, {
        onSuccess: (data) => {
          setCount(data?.totalElements)
          initPagination(data?.post_list)
        }
      })
    }
  }, [check, order, refresh])


  // 게시글 등록 클로저
  const {mutate: mutatePostRegist, isLoading: isLoadingPostRegist} = postPostRegistMutate()
  const handleClickRegist = (text) => {
    mutatePostRegist({
      survey_type: type,
      post_contents: text,
    }, {
      onSuccess: () => {
        setRefresh(!refresh)
      }
    })
  }

  // 게시글 수정 클로저
  const {mutate: mutatePutPostUpdate, isLoading: isLoadingPutPostUpdate} = putPostUpdateMutate()
  const handleClickModify = (item, text, callback) => {
    mutatePutPostUpdate({
      post_key: item?.post_key,
      post_contents: text,
      update_delete: ACTION_TYPE.MODIFY,
    }, {
      onSuccess: () => {
        handleChangeItems(item?.post_key, {
          post_contents: text
        })
        if (callback) {
          callback()
        }
      }
    })
  }

  // 게시글 삭제 클로저
  const handleClickDelete = (item, callback) => {
    mutatePutPostUpdate({
      post_key: item?.post_key,
      update_delete: ACTION_TYPE.DELETE,
    }, {
      onSuccess: () => {
        handleDeleteItems(item?.post_key)
        if (callback) {
          callback()
        }
      }
    })
  }

  // 게시글 좋아요
  const {mutate: mutatePostPostLike, isLoading: isLoadingPostLike} = postPostLikeMutate()
  const handleClickLike = (item) => {
    mutatePostPostLike({
      content_type: CONTENTS_TYPE.POST,
      content_key: item?.post_key,
      regist_cancel: item?.great_yn === 'Y' ? ACTION_TYPE.CANCEL : ACTION_TYPE.REGIST,
    }, {
      onSuccess: () => {
        if (item?.great_yn === 'Y' && item?.great_cnt > 0) {
          handleChangeItems(item?.post_key, {
            great_yn: 'N',
            great_cnt: item?.great_cnt - 1
          })
        } else {
          handleChangeItems(item?.post_key, {
            great_yn: 'Y',
            great_cnt: item?.great_cnt + 1
          })
        }
      }
    })
  }

  // 게시글 신고
  const {mutate: mutatePostDeclare, isLoading: isLoadingPostDeclare} = postDeclareMutate()
  const handleClickDeclare = (item, code, callback) => {
    mutatePostDeclare({
      content_type: CONTENTS_TYPE.POST,
      content_key: item?.post_key,
      declare_content_cd: code?.value,
    }, {
      onSuccess: () => {
        setToastMessage({
          type: TOAST_TYPE.BOTTOM_HEADER,
          message: LAB_TEXT.COMMENT.DECLARE.MESSAGE,
        })
      },
      onSettled: () => {
        if (callback) {
          callback()
        }
      }
    })
  }

  const {navigation, preFetch} = useStackNavigation()
  const handleOpenView = (postKey) => {
    navigation.push(STACK.LAB.TYPE, PATH_DONOTS_PAGE.LAB.COMMENT_DETAIL(postKey), {
      ...STACK.LAB.DATA,
      scrollY: mainRef?.current?.scrollTop,
      postKey: postKey,
      count: count,
      check: check,
      order: order,
      items: items
    })
  }

  const handleDeleteItems = (key) => {
    setCount((prev) => prev - 1)
    setItems(items.filter((item) => item?.post_key !== key))
  }

  const handleChangeItems = (key, value) => {
    const newItems = [...items]
    const index = newItems.findIndex((item) => item?.post_key === key)
    if (index != -1) {
      newItems[index] = {
        ...newItems[index],
        ...value
      }
    }
    setItems(newItems)
  }

  const isLoadingScreen = () => {
    if (isLoading || isLoadingFirstPostList || isLoadingPostRegist
      || isLoadingPostLike || isLoadingPostDeclare || isLoadingPutPostUpdate) {
      return true
    }
    return false
  }

  return (
    <Box sx={{mt: '50px', mx: '20px'}}>

      {/* 타이틀 */}
      <Typography
        sx={{
          color: '#000000',
          fontSize: '20px',
          fontWeight: 700,
          lineHeight: '26px',
          whiteSpace: 'pre-wrap'
        }}
      >
        {LAB_TEXT.COMMENT.TITLE(type)}
      </Typography>


      {/* 아바타 */}
      <AvatarProfile
        avatar={user?.profilePictureUrl}
        type={user?.type}
        grade={user?.grade}
        nickname={user?.nickname}
        sx={{mt: '30px'}}
      />

      {/* 게시글 입력 */}
      <TransitionTextField
        type={TTF_TYPE.REGIST}
        text={text}
        onRegist={handleClickRegist}
        placeholder={{
          fold: LAB_TEXT.COMMENT.PLACEHOLDER.POST,
          expand: LAB_TEXT.COMMENT.PLACEHOLDER.POST_EXPAND
        }}
        sx={{mt: '9px'}}
      />

      {/* 게시글 리스트 헤더 */}
      <PostHeader
        count={count}
        check={check}
        onClickCheck={(value) => setCheck(value)}
        order={order}
        onClickOrder={(value) => setOrder(value)}
        sx={{mt: '40px'}}
      />

      {/* 게시글 리스트 */}
      {
        items === PAGINATION.INIT_DATA
          ? <PostSkeleton/>
          : items?.length ? (
            <>
              <PostList
                items={items}
                onModify={handleClickModify}
                onDelete={handleClickDelete}
                onLike={handleClickLike}
                onDeclare={handleClickDeclare}
                onOpenView={handleOpenView}
              />
              {hasNextOffset.current && <Box ref={setTarget}/>}
            </>
          ) : <PostNotFound/>
      }

      {/* 로딩 스크린 */}
      {isLoadingScreen() && <LoadingScreen/>}

      {/* 탑버튼 */}
      <LabGoToTopButton dialog mainRef={mainRef}/>

    </Box>
  )
}

function PostSkeleton() {
  return (
    <>
      {[...Array(5)].map((item, index) =>
        <React.Fragment key={index}>
          <CommentItemSkeleton/>
          <Divider/>
        </React.Fragment>
      )}
    </>
  );
}

function PostNotFound() {
  return (
    <Box
      sx={{
        height: '100px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <Typography align={'center'} sx={{color: '#222222', fontWeight: 400, fontSize: '16px', lineHeight: '16px'}}>
        {LAB_TEXT.COMMENT.NOT_FOUND_LIST}
      </Typography>

    </Box>
  )
}

function PostHeader({count, check, onClickCheck, order, onClickOrder, sx}) {
  const [open, setOpen] = useState(false)

  const handleClickCheck = () => {
    onClickCheck(!check)
  }
  const handleClickOrder = () => {
    setOpen(true)
  }
  const handleCloseOrder = () => {
    setOpen(false)
  }
  const handleSelectOrder = (item) => {
    onClickOrder(item)
    setOpen(false)
  }

  return (
    <>
      <Stack spacing={'6px'} sx={{height: '76px', ...sx,}}>

        {/* 게시글 갯수 */}
        <Stack direction={'row'} sx={{height: '26px'}}>
          <Typography align={'left'} sx={{fontWeight: 700, fontSize: '16px', lineHeight: '16px'}}>
            {count}
          </Typography>
          <Typography align={'left'} sx={{fontWeight: 400, fontSize: '16px', lineHeight: '16px'}}>
            {LAB_TEXT.COMMENT.COUNT}
          </Typography>
        </Stack>

        <Stack direction={'row'} sx={{height: '44px', alignItems: 'center', justifyContent: 'space-between'}}>

          {/* 내가 쓴 글만 보기 체크박스 */}
          <Stack
            tabIndex={0}
            direction={'row'}
            spacing={'6px'}
            sx={{alignItems: 'center'}}
            onKeyDown={(e) => handleEnterPress(e, handleClickCheck)}
            onClick={handleClickCheck}
          >
            <Checkbox
              tabIndex={-1}
              checked={check}
              disableRipple
              icon={<SvgCheckRoundIcon color={ICON_COLOR.GREY} size={ICON_SIZE.PX24}/>}
              checkedIcon={<SvgCheckRoundIcon color={ICON_COLOR.PRIMARY}/>}
              sx={{width: '24px', height: '24px'}}
              inputProps={{title: ALT_STRING.LAB.COMMENT.CHECK}}
            />
            <Typography align={'left'} sx={{color: '#666666', fontWeight: 400, fontSize: '14px', lineHeight: '14px'}}>
              {LAB_TEXT.COMMENT.MY_WRITING}
            </Typography>
          </Stack>

          {/* 정렬 콤보박스 */}
          <Stack
            tabIndex={0}
            direction={'row'}
            spacing={'4px'}
            onKeyDown={(e) => handleEnterPress(e, handleClickOrder)}
            onClick={handleClickOrder}
            sx={{alignItems: 'center', cursor: 'pointer'}}
          >
            <Typography align={'left'} sx={{color: '#666666', fontWeight: 400, fontSize: '14px', lineHeight: '14px'}}>
              {order?.title}
            </Typography>
            <SvgCommonIcons alt={ALT_STRING.SIGN_UP.BTN_ARROW_DOWN} type={ICON_TYPE.ARROW_DOWN}/>
          </Stack>
        </Stack>

      </Stack>

      {/* 하단 정렬 다이얼로그 */}
      <BottomDialog open={open} onClose={handleCloseOrder}>
        <OrderContents onClick={handleSelectOrder}/>
      </BottomDialog>
    </>
  )
}


function PostList({items, onModify, onDelete, onLike, onDeclare, onOpenView}) {
  return (
    <>
      {
        items?.map((item, index) =>
          <React.Fragment key={item?.post_key}>
            <CommentItem
              type={POST_TYPE.LIST}
              item={{
                ...item,
                key: item?.post_key,
                contents: item?.post_contents,
              }}
              onModify={onModify}
              onDelete={onDelete}
              onLike={onLike}
              onDeclare={onDeclare}
              onOpenView={onOpenView}
            />
            {index < items?.length - 1 && <Divider sx={{mx: '-20px', my: '10px'}}/>}
          </React.Fragment>
        )
      }
    </>
  )
}

function OrderContents({onClick}) {

  const handleClick = (item) => {
    if (onClick) {
      onClick(item)
    }
  }

  return (
    <Box sx={{mt: '6px', textAlign: 'start'}}>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 500,
          fontSize: '16px',
          lineHeight: '24px',
          color: '#222222',
          paddingY: '20px'
        }}
      >
        {LAB_TEXT.COMMENT.ORDER.TITLE}
      </Typography>
      <Divider sx={{height: '1px', backgroundColor: '#E6E6E6'}}/>

      {LAB_TEXT.COMMENT.ORDER.LIST.map((item, index) => (
        <Typography
          tabIndex={1}
          key={index}
          sx={{
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
            paddingY: '20px',
            ...clickBorderNone
          }}
          onClick={() => handleClick(item)}
          onKeyDown={(e) => handleEnterPress(e, () => handleClick(item))}
        >
          {item?.title}
        </Typography>))}
    </Box>
  )
}