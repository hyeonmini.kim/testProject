import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../constant/common/Common";
import {Box, Button} from "@mui/material";
import {LAB_TEXT, LAB_TYPE} from "../../constant/lab/LabCommon";
import Image from "../../components/image";
import * as React from "react";
import {PATH_DONOTS_STATIC_PAGE} from "../../routes/paths";
import {ALT_STRING} from "../../constant/common/AltString";

export default function RankingScreenSection({type, onClick}) {
  return (
    <Box sx={{position: 'relative'}}>
      <Box sx={{height: '30px'}}/>
      <Box sx={{width: '100%', height: '785px', display: 'flex', justifyContent: 'center', bgcolor: '#FFFFFF'}}>
        <img
          alt={type === LAB_TYPE.BABY_FOOD ? ALT_STRING.LAB.PREVIEW.BABY_FOOD : ALT_STRING.LAB.PREVIEW.NUTRITION}
          src={type === LAB_TYPE.BABY_FOOD ? PATH_DONOTS_STATIC_PAGE.ICONS('/images/icons/lab/babyfood_ranking_blur.png') : PATH_DONOTS_STATIC_PAGE.ICONS('/images/icons/lab/nutrition_ranking_blur.png')}
          // isLazy={false}
          style={{width: '100%', aspectRatio: type === LAB_TYPE.BABY_FOOD ? '360/642' : '360/615', objectFit: 'cover'}}
        />
      </Box>
      <Button
        tabIndex={0}
        disableFocusRipple
        variant={'contained'}
        fullWidth={true}
        onClick={() => onClick()}
        onKeyDown={(e) => handleEnterPress(e, () => onClick())}
        sx={{
          position: 'absolute',
          width: '258px',
          height: '48px',
          left: 0, right: 0, top: type === LAB_TYPE.BABY_FOOD ? 117 : 145, margin: 'auto',
          borderRadius: '99px',
          bgcolor: '#B51A5B',
          boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.1)',
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '16px',
          lineHeight: '16px',
          letterSpacing: '-0.02em',
          color: '#FFFFFF',
          '&:hover': {
            boxShadow: 'none',
            backgroundColor: '#B51A5B',
          },
          ...clickBorderNone,
        }}
      >
        {LAB_TEXT.SCREEN_BUTTON(type)}
      </Button>
    </Box>
  )
}
