import {removeLine} from "../../utils/formatString";
import * as React from "react";
import {useEffect} from "react";
import {Box} from "@mui/material";
import {SURVEY_COMMON} from "../../constant/lab/LabCommon";
import LabSurveyTitle from "../../components/lab/LabSurveyTitle";
import LabEtcReasonInputTextField from "../../components/lab/LabEtcReasonInputTextField";

export default function EtcReasonSection({setIsDisabled, brandName, setBrandName, placeHolder}) {

  const handleBrandName = (event) => {
    let value = event.target.value?.substring(0, 30);
    setBrandName(removeLine(value));
  };

  useEffect(() => {
    if (brandName === '') {
      setIsDisabled(true)
    } else {
      setIsDisabled(false)
    }
  }, [brandName])

  return (
    <Box sx={{mt: '55px', marginBottom: '102px'}}>

      <LabSurveyTitle title={SURVEY_COMMON.TEXT.NUTRITION_ETC_REASON_TITLE}/>

      <LabEtcReasonInputTextField placeHolder={placeHolder} brandName={brandName} handleBrandName={handleBrandName}/>

    </Box>
  )
}