import {useRecoilState} from "recoil";
import {
  labIsNutritionSUR03EtcSelectedSelector,
  labIsNutritionSUR04EtcSelectedSelector,
  labIsNutritionSUR05EtcSelectedSelector,
  labIsSUR03SelectedSelector,
  labIsSUR04SelectedSelector,
  labIsSUR05SelectedSelector,
  labIsSUR06SelectedSelector,
  labNutritionActiveStepListSelector,
  labNutritionActiveStepSelector,
  labNutritionSelectedLengthSelector,
  labNutritionSelectListSelector,
  labNutritionSUR03ListSelector,
  labNutritionSUR03ReasonListSelector,
  labNutritionSUR03SelectBrandCdSelector,
  labNutritionSUR03SelectBrandNameSelector,
  labNutritionSUR03SelectReasonListSelector,
  labNutritionSUR04ListSelector,
  labNutritionSUR04ReasonListSelector,
  labNutritionSUR04SelectBrandCdSelector,
  labNutritionSUR04SelectBrandNameSelector,
  labNutritionSUR04SelectReasonListSelector,
  labNutritionSUR05ListSelector,
  labNutritionSUR05ReasonListSelector,
  labNutritionSUR05SelectBrandCdSelector,
  labNutritionSUR05SelectBrandNameSelector,
  labNutritionSUR05SelectReasonListSelector
} from "../../../recoil/selector/lab/labSelector";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {LAB_TYPE, NUTRITION, NUTRITION_ACTIVE_STEP, SURVEY_COMMON, SURVEY_TYPE} from "../../../constant/lab/LabCommon";
import {postSurveyRegistMutate} from "../../../api/labApi";
import {Box, MobileStepper} from "@mui/material";
import {useQueryClient} from "react-query";
import BrandSelectSection from "../BrandSelectSection";
import EtcReasonSection from "../EtcReasonSection";
import CheckItemsSection from "../CheckItemsSection";
import LabBottomFixedButton from "../../../components/lab/LabBottomFixedButton";
import {replaceItemAtIndex} from "../../../utils/arrayUtils";
import LoadingScreen from "../../../components/common/LoadingScreen";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";

export default function NutritionSurveySection({onClose}) {
  const [labNutritionActiveStep, setLabNutritionActiveStep] = useRecoilState(labNutritionActiveStepSelector)
  const [labNutritionActiveStepList, setLabNutritionActiveStepList] = useRecoilState(labNutritionActiveStepListSelector)
  const [labNutritionSelectList, setLabNutritionSelectList] = useRecoilState(labNutritionSelectListSelector)
  const [labNutritionSelectedLength, setLabNutritionSelectedLength] = useRecoilState(labNutritionSelectedLengthSelector)

  const [labNutritionSUR03List, setLabNutritionSUR03List] = useRecoilState(labNutritionSUR03ListSelector)
  const [labNutritionSUR04List, setLabNutritionSUR04List] = useRecoilState(labNutritionSUR04ListSelector)
  const [labNutritionSUR05List, setLabNutritionSUR05List] = useRecoilState(labNutritionSUR05ListSelector)

  const [labIsSUR03Selected, setLabIsSUR03Selected] = useRecoilState(labIsSUR03SelectedSelector)
  const [labIsSUR04Selected, setLabIsSUR04Selected] = useRecoilState(labIsSUR04SelectedSelector)
  const [labIsSUR05Selected, setLabIsSUR05Selected] = useRecoilState(labIsSUR05SelectedSelector)
  const [labIsSUR06Selected, setLabIsSUR06Selected] = useRecoilState(labIsSUR06SelectedSelector)

  const [labIsNutritionSUR03EtcSelected, setLabIsNutritionSUR03EtcSelected] = useRecoilState(labIsNutritionSUR03EtcSelectedSelector)
  const [labIsNutritionSUR04EtcSelected, setLabIsNutritionSUR04EtcSelected] = useRecoilState(labIsNutritionSUR04EtcSelectedSelector)
  const [labIsNutritionSUR05EtcSelected, setLabIsNutritionSUR05EtcSelected] = useRecoilState(labIsNutritionSUR05EtcSelectedSelector)

  const [labNutritionSUR03ReasonList, setLabNutritionSUR03ReasonList] = useRecoilState(labNutritionSUR03ReasonListSelector)
  const [labNutritionSUR04ReasonList, setLabNutritionSUR04ReasonList] = useRecoilState(labNutritionSUR04ReasonListSelector)
  const [labNutritionSUR05ReasonList, setLabNutritionSUR05ReasonList] = useRecoilState(labNutritionSUR05ReasonListSelector)

  const [labNutritionSUR03SelectBrandCd, setLabNutritionSUR03SelectBrandCd] = useRecoilState(labNutritionSUR03SelectBrandCdSelector)
  const [labNutritionSUR03SelectBrandName, setLabNutritionSUR03SelectBrandName] = useRecoilState(labNutritionSUR03SelectBrandNameSelector)
  const [labNutritionSUR03SelectReasonList, setLabNutritionSUR03SelectReasonList] = useRecoilState(labNutritionSUR03SelectReasonListSelector)

  const [labNutritionSUR04SelectBrandCd, setLabNutritionSUR04SelectBrandCd] = useRecoilState(labNutritionSUR04SelectBrandCdSelector)
  const [labNutritionSUR04SelectBrandName, setLabNutritionSUR04SelectBrandName] = useRecoilState(labNutritionSUR04SelectBrandNameSelector)
  const [labNutritionSUR04SelectReasonList, setLabNutritionSUR04SelectReasonList] = useRecoilState(labNutritionSUR04SelectReasonListSelector)

  const [labNutritionSUR05SelectBrandCd, setLabNutritionSUR05SelectBrandCd] = useRecoilState(labNutritionSUR05SelectBrandCdSelector)
  const [labNutritionSUR05SelectBrandName, setLabNutritionSUR05SelectBrandName] = useRecoilState(labNutritionSUR05SelectBrandNameSelector)
  const [labNutritionSUR05SelectReasonList, setLabNutritionSUR05SelectReasonList] = useRecoilState(labNutritionSUR05SelectReasonListSelector)

  const [isDisabled, setIsDisabled] = useState(true)
  const [buttonText, setButtonText] = useState('')
  const [isEtcSelected, setIsEtcSelected] = useState(false)

  const queryClient = useQueryClient()

  const bottomBtnRef = useRef(null)

  const nutritionHandleClick = (code) => {
    let newList = labNutritionSelectList;

    const index = labNutritionSelectList?.findIndex((listItem) => listItem?.code === code)
    if (code === SURVEY_TYPE.SUR06.code && !labNutritionSelectList[3].value) {
      let index = 0
      while (index < labNutritionSelectList?.length) {
        newList = replaceItemAtIndex(newList, index, {
          text: labNutritionSelectList[index]?.text,
          value: false,
          code: labNutritionSelectList[index]?.code,
        })
        index++
      }
      newList = replaceItemAtIndex(newList, 3, {
        text: labNutritionSelectList[3]?.text,
        value: true,
        code: labNutritionSelectList[3]?.code,
      })
    } else {
      if (code !== SURVEY_TYPE.SUR06.code) {
        newList = replaceItemAtIndex(newList, 3, {
          text: newList[3]?.text,
          value: false,
          code: newList[3]?.code,
        })
        const index = labNutritionSelectList?.findIndex((listItem) => listItem?.code === code)
        newList = replaceItemAtIndex(newList, index, {
          text: newList[index]?.text,
          value: !newList[index]?.value,
          code: code,
        })
      } else {
        newList = replaceItemAtIndex(newList, 3, {
          text: newList[3]?.text,
          value: !newList[3]?.value,
          code: newList[3]?.code,
        })
      }
    }

    setLabNutritionSelectList(newList)

    const lastSelectedIndex = newList?.findIndex((listItem) => listItem?.value === true)

    if (lastSelectedIndex === 3) {
      setButtonText(SURVEY_COMMON.BUTTON.SHOW_RESULT)
    } else {
      setButtonText(SURVEY_COMMON.BUTTON.NEXT)
    }

    newList.forEach((item) => {
      switch (item?.code) {
        case SURVEY_TYPE.SUR03.code :
          if (item?.value) {
            setLabIsSUR03Selected(true)
          } else {
            setLabIsSUR03Selected(false)
          }
          break;
        case SURVEY_TYPE.SUR04.code :
          if (item?.value) {
            setLabIsSUR04Selected(true)
          } else {
            setLabIsSUR04Selected(false)
          }
          break;
        case SURVEY_TYPE.SUR05.code :
          if (item?.value) {
            setLabIsSUR05Selected(true)
          } else {
            setLabIsSUR05Selected(false)
          }
          break;
        case SURVEY_TYPE.SUR06.code :
          if (item?.value) {
            setLabIsSUR06Selected(true)
          } else {
            setLabIsSUR06Selected(false)
          }
          break;
      }
    })

    let selectedLengthList = newList.slice(0, 3).filter((listItem) => listItem?.value === true)

    setLabNutritionSelectedLength(selectedLengthList?.length)
  }

  const brandHandleClick = (id, surveyType) => {

    if (surveyType === SURVEY_TYPE.SUR03.code) {
      brandChange(id, labNutritionSUR03List, setLabNutritionSUR03List, setLabIsNutritionSUR03EtcSelected, setLabNutritionSUR03SelectBrandCd, setLabNutritionSUR03SelectBrandName)
    } else if (surveyType === SURVEY_TYPE.SUR04.code) {
      brandChange(id, labNutritionSUR04List, setLabNutritionSUR04List, setLabIsNutritionSUR04EtcSelected, setLabNutritionSUR04SelectBrandCd, setLabNutritionSUR04SelectBrandName)
    } else if (surveyType === SURVEY_TYPE.SUR05.code) {
      brandChange(id, labNutritionSUR05List, setLabNutritionSUR05List, setLabIsNutritionSUR05EtcSelected, setLabNutritionSUR05SelectBrandCd, setLabNutritionSUR05SelectBrandName)
    }

  }

  const brandChange = (id, list, setList, setEtcSelected, setBrandCd, setBrandName) => {
    let newList = list;

    const selectedIndex = list?.findIndex((listItem) => listItem?.value === true)
    const index = list?.findIndex((listItem) => listItem?.text === id)

    if (selectedIndex >= 0 && index !== selectedIndex) {
      newList = replaceItemAtIndex(newList, selectedIndex, {
        text: newList[selectedIndex]?.text,
        example: newList[selectedIndex]?.example,
        value: false,
        image: newList[selectedIndex]?.image,
        code: newList[selectedIndex]?.code,
      })

      newList = replaceItemAtIndex(newList, index, {
        text: id,
        example: newList[index]?.example,
        value: !newList[index]?.value,
        image: newList[index]?.image,
        code: newList[index]?.code,
      })
    } else {
      const index = list?.findIndex((listItem) => listItem?.text === id)

      newList = replaceItemAtIndex(newList, index, {
        text: id,
        example: newList[index]?.example,
        value: !newList[index]?.value,
        image: newList[index]?.image,
        code: newList[index]?.code,
      })
    }

    const idx = newList?.findIndex((listItem) => listItem?.value === true)
    if (idx >= 0 && idx !== 11) {
      setBrandCd(list[index]?.code)
      setBrandName(list[index]?.text)
    } else if (idx >= 0 && idx === 11) {
      setBrandCd(list[index]?.code)
      setBrandName('')
    } else {
      setBrandCd('')
      setBrandName('')
    }

    setList(newList)

    const lastSelectedIndex = newList?.findIndex((listItem) => listItem?.value === true)

    if (lastSelectedIndex === 11) {
      setEtcSelected(true)
    } else {
      setEtcSelected(false)
    }
  }

  const reasonHandleClick = (code, surveyType) => {

    if (surveyType === SURVEY_TYPE.SUR03.code) {
      reasonChange(code, labNutritionSUR03ReasonList, setLabNutritionSUR03ReasonList, labNutritionSUR03SelectReasonList, setLabNutritionSUR03SelectReasonList)
    } else if (surveyType === SURVEY_TYPE.SUR04.code) {
      reasonChange(code, labNutritionSUR04ReasonList, setLabNutritionSUR04ReasonList, labNutritionSUR04SelectReasonList, setLabNutritionSUR04SelectReasonList)
    } else if (surveyType === SURVEY_TYPE.SUR05.code) {
      reasonChange(code, labNutritionSUR05ReasonList, setLabNutritionSUR05ReasonList, labNutritionSUR05SelectReasonList, setLabNutritionSUR05SelectReasonList)
    }

  }

  const reasonChange = (code, list, setList, selectList, setSelectList) => {
    let newList = list;

    const index = list?.findIndex((listItem) => listItem?.code === code)
    newList = replaceItemAtIndex(newList, index, {
      text: newList[index]?.text,
      value: !newList[index]?.value,
      code: code,
    })

    const idx = selectList?.findIndex((listItem) => listItem?.select_reason_cd === code)
    if (idx >= 0) {
      // 있으면 제거
      setSelectList(selectList?.filter(listItem => listItem?.select_reason_cd !== code))
    } else {
      // 없으므로 추가
      const newReason = {
        select_reason_cd: code,
        select_reason: list[index]?.text
      }
      setSelectList([...selectList, newReason])
    }

    setList(newList)
  }

  const handleNextButton = () => {
    if (buttonText === SURVEY_COMMON.BUTTON.SHOW_RESULT) {
      switch (labNutritionActiveStep) {
        case labNutritionActiveStepList.SUR03_REASON :
          setHackleTrack(HACKLE_TRACK.NUTRIENTS_SELECT_LOCTO_STEP2)
          break
        case labNutritionActiveStepList.SUR04_REASON :
          setHackleTrack(HACKLE_TRACK.NUTRIENTS_SELECT_VITAMIN_STEP2)
          break
        case labNutritionActiveStepList.SUR05_REASON :
          setHackleTrack(HACKLE_TRACK.NUTRIENTS_SELECT_IRON_STEP2)
          break
        default:
          setHackleTrack(HACKLE_TRACK.NUTRIENTS_SELECT)
      }

      registSurvey()
    } else {
      switch (labNutritionActiveStep) {
        case labNutritionActiveStepList.SELECT :
          setHackleTrack(HACKLE_TRACK.NUTRIENTS_SELECT)
          if (labIsSUR03Selected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR03_BRAND);
          } else if (labIsSUR04Selected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR04_BRAND);
          } else if (labIsSUR05Selected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR05_BRAND);
          }
          break;
        case labNutritionActiveStepList.SUR03_BRAND :
          setHackleTrack(HACKLE_TRACK.NUTRIENTS_SELECT_LOCTO_STEP1)
          if (labIsNutritionSUR03EtcSelected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR03_ETC_REASON);
          } else {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR03_REASON);
          }
          break;
        case labNutritionActiveStepList.SUR03_ETC_REASON :
          setLabNutritionActiveStep(labNutritionActiveStepList.SUR03_REASON);
          break;
        case labNutritionActiveStepList.SUR03_REASON :
          setHackleTrack(HACKLE_TRACK.NUTRIENTS_SELECT_LOCTO_STEP2)
          if (labIsSUR04Selected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR04_BRAND)
          } else if (labIsSUR05Selected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR05_BRAND)
          }
          break;
        case labNutritionActiveStepList.SUR04_BRAND :
          setHackleTrack(HACKLE_TRACK.NUTRIENTS_SELECT_VITAMIN_STEP1)
          if (labIsNutritionSUR04EtcSelected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR04_ETC_REASON);
          } else {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR04_REASON);
          }
          break;
        case labNutritionActiveStepList.SUR04_ETC_REASON :
          setLabNutritionActiveStep(labNutritionActiveStepList.SUR04_REASON);
          break;
        case labNutritionActiveStepList.SUR04_REASON :
          setHackleTrack(HACKLE_TRACK.NUTRIENTS_SELECT_VITAMIN_STEP2)
          setLabNutritionActiveStep(labNutritionActiveStepList.SUR05_BRAND);
          break;
        case labNutritionActiveStepList.SUR05_BRAND :
          setHackleTrack(HACKLE_TRACK.NUTRIENTS_SELECT_IRON_STEP1)
          if (labIsNutritionSUR05EtcSelected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR05_ETC_REASON);
          } else {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR05_REASON);
          }
          break;
        case labNutritionActiveStepList.SUR05_ETC_REASON :
          setHackleTrack(HACKLE_TRACK.NUTRIENTS_SELECT_IRON_STEP3)
          setLabNutritionActiveStep(labNutritionActiveStepList.SUR05_REASON);
          break;
      }
    }
  };

  useEffect(() => {
    switch (labNutritionActiveStep) {
      case labNutritionActiveStepList.SELECT :
      case labNutritionActiveStepList.SUR03_BRAND :
      case labNutritionActiveStepList.SUR03_ETC_REASON :
      case labNutritionActiveStepList.SUR04_BRAND :
      case labNutritionActiveStepList.SUR04_ETC_REASON :
      case labNutritionActiveStepList.SUR05_BRAND :
      case labNutritionActiveStepList.SUR05_ETC_REASON :
        setButtonText(SURVEY_COMMON.BUTTON.NEXT)
        break;
      case labNutritionActiveStepList.SUR03_REASON :
        if (labIsSUR04Selected || labIsSUR05Selected) {
          setButtonText(SURVEY_COMMON.BUTTON.NEXT)
        } else {
          setButtonText(SURVEY_COMMON.BUTTON.SHOW_RESULT)
        }
        break;
      case labNutritionActiveStepList.SUR04_REASON :
        if (labIsSUR05Selected) {
          setButtonText(SURVEY_COMMON.BUTTON.NEXT)
        } else {
          setButtonText(SURVEY_COMMON.BUTTON.SHOW_RESULT)
        }
        break;
      case labNutritionActiveStepList.SUR05_REASON :
        setButtonText(SURVEY_COMMON.BUTTON.SHOW_RESULT)
        break;
    }
  }, [labNutritionActiveStep])

  useEffect(() => {
    if (labNutritionSelectedLength === 1 && labIsSUR03Selected) {
      setLabNutritionActiveStepList({
        ...NUTRITION_ACTIVE_STEP,
        SUR03_ETC_REASON: 45,
      })
    } else if (labNutritionSelectedLength === 1 && labIsSUR04Selected) {
      setLabNutritionActiveStepList({
        ...NUTRITION_ACTIVE_STEP,
        SUR04_BRAND: 40,
        SUR04_ETC_REASON: 45,
        SUR04_REASON: 60,
        SUR03_BRAND: 80,
        SUR03_ETC_REASON: 88,
        SUR03_REASON: 100
      })
    } else if (labNutritionSelectedLength === 1 && labIsSUR05Selected) {
      setLabNutritionActiveStepList({
        ...NUTRITION_ACTIVE_STEP,
        SUR05_BRAND: 40,
        SUR05_ETC_REASON: 45,
        SUR05_REASON: 60,
        SUR03_BRAND: 120,
        SUR03_ETC_REASON: 126,
        SUR03_REASON: 140
      })
    } else if (labNutritionSelectedLength === 2) {
      if (labIsSUR03Selected && labIsSUR04Selected) {
        setLabNutritionActiveStepList({
          ...NUTRITION_ACTIVE_STEP,
          SUR04_ETC_REASON: 86,
        })
      } else if (labIsSUR03Selected && labIsSUR05Selected) {
        setLabNutritionActiveStepList({
          ...NUTRITION_ACTIVE_STEP,
          SUR05_BRAND: 80,
          SUR05_ETC_REASON: 86,
          SUR05_REASON: 100,
          SUR04_BRAND: 120,
          SUR04_ETC_REASON: 126,
          SUR04_REASON: 140
        })
      } else if (labIsSUR04Selected && labIsSUR05Selected) {
        setLabNutritionActiveStepList({
          ...NUTRITION_ACTIVE_STEP,
          SUR04_BRAND: 40,
          SUR04_ETC_REASON: 48,
          SUR04_REASON: 60,
          SUR05_BRAND: 80,
          SUR05_ETC_REASON: 86,
          SUR05_REASON: 100,
          SUR03_BRAND: 120,
          SUR03_ETC_REASON: 126,
          SUR03_REASON: 140
        })
      }
    } else {
      setLabNutritionActiveStepList({
        ...NUTRITION_ACTIVE_STEP,
      })
    }
  }, [labNutritionSelectedLength])

  const {mutate: mutatePostSurveyRegist, isLoading} = postSurveyRegistMutate()

  const registSurvey = () => {
    let list = [];

    if (labIsSUR03Selected) {
      const sur03 = [{
        survey_type: SURVEY_TYPE.SUR03.code,
        brand_cd: labNutritionSUR03SelectBrandCd,
        brand_name: labNutritionSUR03SelectBrandName,
        select_reason_list: labNutritionSUR03SelectReasonList,
      }]
      list = list.concat(sur03)
    }
    if (labIsSUR04Selected) {
      const sur04 = [{
        survey_type: SURVEY_TYPE.SUR04.code,
        brand_cd: labNutritionSUR04SelectBrandCd,
        brand_name: labNutritionSUR04SelectBrandName,
        select_reason_list: labNutritionSUR04SelectReasonList,
      }]
      list = list.concat(sur04)
    }
    if (labIsSUR05Selected) {
      const sur05 = [{
        survey_type: SURVEY_TYPE.SUR05.code,
        brand_cd: labNutritionSUR05SelectBrandCd,
        brand_name: labNutritionSUR05SelectBrandName,
        select_reason_list: labNutritionSUR05SelectReasonList,
      }]
      list = list.concat(sur05)
    }
    if (labIsSUR06Selected) {
      const sur06 = [{
        survey_type: SURVEY_TYPE.SUR06.code,
      }]
      list = list.concat(sur06)
    }

    mutatePostSurveyRegist({
      survey_list: list
    }, {
      onSuccess: () => {
        queryClient?.invalidateQueries('getRegistSurveyYn')
        onClose()
      }
    })
  }

  useEffect(() => {
    if ((labNutritionActiveStep === labNutritionActiveStepList.SUR03_BRAND && labIsNutritionSUR03EtcSelected) || (labNutritionActiveStep === labNutritionActiveStepList.SUR04_BRAND && labIsNutritionSUR04EtcSelected) || (labNutritionActiveStep === labNutritionActiveStepList.SUR05_BRAND && labIsNutritionSUR05EtcSelected)) {
      setIsEtcSelected(true)
    } else {
      setIsEtcSelected(false)
    }
  }, [labNutritionActiveStep, labIsNutritionSUR03EtcSelected, labIsNutritionSUR04EtcSelected, labIsNutritionSUR05EtcSelected])

  return (
    <Box sx={{mx: '20px'}}>

      <MobileStepper
        variant="progress"
        steps={
          labNutritionSelectList[3]?.value
            ? 21
            : labNutritionSelectedLength === 0
              ? 61
              : labNutritionSelectedLength === 1
                ? isEtcSelected
                  ? 81
                  : 61
                : labNutritionSelectedLength === 2
                  ? isEtcSelected
                    ? 113
                    : 101
                  : isEtcSelected
                    ? 150
                    : 141
        }
        // position="static"
        activeStep={labNutritionActiveStep}
        sx={{
          position: 'fixed',
          height: '78px',
          pt: '65px',
          pb: '30px',
          top: 0,
          left: '20px',
          right: '20px',
          maxWidth: '400px',
          flexGrow: 1,
          px: '0px',
          '& .MuiMobileStepper-progress': {width: '100%', height: '10px', background: '#F5F5F5', borderRadius: '50px',},
          '& .MuiLinearProgress-bar': {borderRadius: '50px',}
        }}
      />

      {labNutritionActiveStep === labNutritionActiveStepList.SELECT &&
        <CheckItemsSection
          handleClick={nutritionHandleClick}
          setIsDisabled={setIsDisabled}
          list={labNutritionSelectList}
          title={SURVEY_COMMON.TEXT.NUTRITION_SELECT_TITLE}
        />}

      {labIsSUR03Selected && labNutritionActiveStep === labNutritionActiveStepList.SUR03_BRAND &&
        <BrandSelectSection
          type={LAB_TYPE.NUTRITION}
          handleClick={brandHandleClick}
          setIsDisabled={setIsDisabled}
          list={labNutritionSUR03List}
          title={SURVEY_COMMON.TEXT.NUTRITION_SUR03_BRAND_TITLE}
          etcText={NUTRITION.LAC12.text}
          surveyType={SURVEY_TYPE.SUR03.code}
        />}

      {labIsSUR03Selected && labNutritionActiveStep === labNutritionActiveStepList.SUR03_ETC_REASON &&
        <EtcReasonSection
          setIsDisabled={setIsDisabled}
          brandName={labNutritionSUR03SelectBrandName}
          setBrandName={setLabNutritionSUR03SelectBrandName}
          placeHolder={SURVEY_COMMON.TEXT.NUTRITION_SUR03_ETC_REASON_PLACEHOLDER}
        />}

      {labIsSUR03Selected && labNutritionActiveStep === labNutritionActiveStepList.SUR03_REASON &&
        <CheckItemsSection
          handleClick={reasonHandleClick}
          setIsDisabled={setIsDisabled}
          list={labNutritionSUR03ReasonList}
          title={SURVEY_COMMON.TEXT.REASON_TITLE}
          surveyType={SURVEY_TYPE.SUR03.code}
        />}

      {labIsSUR04Selected && labNutritionActiveStep === labNutritionActiveStepList.SUR04_BRAND &&
        <BrandSelectSection
          type={LAB_TYPE.NUTRITION}
          handleClick={brandHandleClick}
          setIsDisabled={setIsDisabled}
          list={labNutritionSUR04List}
          title={SURVEY_COMMON.TEXT.NUTRITION_SUR04_BRAND_TITLE}
          etcText={NUTRITION.VIT12.text}
          surveyType={SURVEY_TYPE.SUR04.code}
        />}

      {labIsSUR04Selected && labNutritionActiveStep === labNutritionActiveStepList.SUR04_ETC_REASON &&
        <EtcReasonSection
          setIsDisabled={setIsDisabled}
          brandName={labNutritionSUR04SelectBrandName}
          setBrandName={setLabNutritionSUR04SelectBrandName}
          placeHolder={SURVEY_COMMON.TEXT.NUTRITION_SUR04_ETC_REASON_PLACEHOLDER}
        />}

      {labIsSUR04Selected && labNutritionActiveStep === labNutritionActiveStepList.SUR04_REASON &&
        <CheckItemsSection
          handleClick={reasonHandleClick}
          setIsDisabled={setIsDisabled}
          list={labNutritionSUR04ReasonList}
          title={SURVEY_COMMON.TEXT.REASON_TITLE}
          surveyType={SURVEY_TYPE.SUR04.code}
        />}

      {labIsSUR05Selected && labNutritionActiveStep === labNutritionActiveStepList.SUR05_BRAND &&
        <BrandSelectSection
          type={LAB_TYPE.NUTRITION}
          handleClick={brandHandleClick}
          setIsDisabled={setIsDisabled}
          list={labNutritionSUR05List}
          title={SURVEY_COMMON.TEXT.NUTRITION_SUR05_BRAND_TITLE}
          etcText={NUTRITION.IRO12.text}
          surveyType={SURVEY_TYPE.SUR05.code}
        />}

      {labIsSUR05Selected && labNutritionActiveStep === labNutritionActiveStepList.SUR05_ETC_REASON &&
        <EtcReasonSection
          setIsDisabled={setIsDisabled}
          brandName={labNutritionSUR05SelectBrandName}
          setBrandName={setLabNutritionSUR05SelectBrandName}
          placeHolder={SURVEY_COMMON.TEXT.NUTRITION_SUR05_ETC_REASON_PLACEHOLDER}
        />}

      {labIsSUR05Selected && labNutritionActiveStep === labNutritionActiveStepList.SUR05_REASON &&
        <CheckItemsSection
          handleClick={reasonHandleClick}
          setIsDisabled={setIsDisabled}
          list={labNutritionSUR05ReasonList}
          title={SURVEY_COMMON.TEXT.REASON_TITLE}
          surveyType={SURVEY_TYPE.SUR05.code}
        />}

      <LabBottomFixedButton btnRef={bottomBtnRef} handleNextButton={handleNextButton} isDisabled={isDisabled} text={buttonText}/>

      {isLoading && <LoadingScreen/>}

    </Box>
  )
}