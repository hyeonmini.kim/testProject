import * as React from "react";
import {useEffect} from "react";
import {Box} from "@mui/material";
import LabCheckItemsList from "../../components/lab/LabCheckItemsList";
import LabSurveyTitle from "../../components/lab/LabSurveyTitle";

export default function CheckItemsSection({handleClick, setIsDisabled, list, title, surveyType}) {

  useEffect(() => {
    const index = list?.findIndex((listItem) => listItem?.value === true)
    setIsDisabled(index < 0)
  }, [list])

  return (
    <Box sx={{mt: '55px', marginBottom: '102px'}}>

      <LabSurveyTitle title={title}/>

      <LabCheckItemsList list={list} handleClick={handleClick} surveyType={surveyType}/>

    </Box>
  )
}