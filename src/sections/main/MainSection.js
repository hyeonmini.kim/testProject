import {useRecoilState, useSetRecoilState} from "recoil";
import {authUuidSelector} from "../../recoil/selector/sign-up/signUpSelector";
import {toastMessageSelector} from "../../recoil/selector/common/toastSelector";
import {postGetUuidMutate} from "../../api/authApi";
import {useRouter} from "next/router";
import {clickBorderNone, COMMON_STR, TOAST_TYPE} from "../../constant/common/Common";
import {ERRORS, GET_FRONT_ERROR_MESSAGE} from "../../constant/common/Error";
import {setItemSnsType} from "../../auth/utils";
import {setCookie} from "../../utils/storageUtils";
import {PATH_DONOTS_API, PATH_DONOTS_PAGE} from "../../routes/paths";
import {getEnv} from "../../utils/envUtils";
import {Box, Typography} from "@mui/material";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {MAIN} from "../../constant/sign-up/SignUp";
import NextLink from "next/link";
import IFrameDialog from "../../components/common/IFrameDialog";
import React, {useEffect, useState} from "react";
import {settingShowPersonalTermsSelector, settingShowServiceTermsSelector} from "../../recoil/selector/my/settingSelector";
import {MY_SETTING} from "../../constant/my/Setting";
import {getTermsOfUseByTitleMutate} from "../../api/mySettingApi";
import useNativeBackKey from "../../hooks/useNativeBackKey";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../constant/common/AltString";

export default function MainSection({onClickSignUp}) {
  const [uuid, setUuid] = useRecoilState(authUuidSelector)
  const setToastMessage = useSetRecoilState(toastMessageSelector)

  const [showServiceTerms, setShowServiceTerms] = useRecoilState(settingShowServiceTermsSelector)
  const [showPersonalTerms, setShowPersonalTerms] = useRecoilState(settingShowPersonalTermsSelector)

  const [serviceContents, setServiceContents] = useState('')
  const [personalContents, setPersonalContents] = useState('')

  const {mutate: termsOfUseByTitle} = getTermsOfUseByTitleMutate()
  const {mutate: mutatePostGetUuid} = postGetUuidMutate()

  const router = useRouter()

  const snsSignUpError = () => {
    setToastMessage({
      type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
      message: GET_FRONT_ERROR_MESSAGE(ERRORS.EXTERNAL_SERVER_SNS),
    })
  }

  const handleClickSignUpSns = (e) => {
    let snsType = e.target.id ? e.target.id : e.target.parentNode.id
    setItemSnsType(snsType)

    mutatePostGetUuid({}, {
      onSuccess: (uuid) => {
        setUuid(uuid)
        setCookie(COMMON_STR.UUID, uuid, 1)
        setCookie(COMMON_STR.CALLBACK_TYPE, COMMON_STR.ID, 1)

        if (snsType && uuid) {
          router.push(PATH_DONOTS_PAGE.AUTH.SNS(
            getEnv(), {
              type: snsType,
              redirectUrl: PATH_DONOTS_API.AUTH.SNS_CALLBACK(getEnv()),
              // redirectUrl: window.location.href,
              uuid: uuid
            }))
        } else {
          snsSignUpError()
        }
      },
      onError: (error) => {
        snsSignUpError()
      }
    })
  }

  const handlePersonalTerms = () => {
    termsOfUseByTitle({
      title: encodeURIComponent(MY_SETTING.TERMS.PERSONAL.VALUE)
    }, {
      onSuccess: (result) => {
        setPersonalContents(result?.bodyHtmlFileUrl)
        setShowPersonalTerms(true)
      }
    })
  }

  const handleServiceTerms = () => {
    termsOfUseByTitle({
      title: encodeURIComponent(MY_SETTING.TERMS.SERVICE.VALUE)
    }, {
      onSuccess: (result) => {
        setServiceContents(result?.bodyHtmlFileUrl)
        setShowServiceTerms(true)
      }
    })
  }

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      if (showPersonalTerms) setShowPersonalTerms(false)
      if (showServiceTerms) setShowServiceTerms(false)
    }
  }, [handleBackKey])

  useEffect(() => {
    router.beforePopState(({as}) => {
      if (as !== router.asPath) {
        setShowPersonalTerms(false)
        setShowServiceTerms(false)
      }
      return true
    })
  }, [router])

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.LANDING)
  }, [])

  const handleLoginKeyDown = (e) => {
    if (e.key === 'Enter') {
      router?.push(PATH_DONOTS_PAGE.AUTH.LOGIN)
    }
  }

  const handleGuestKeyDown = (e) => {
    if (e.key === 'Enter') {
      router?.push(PATH_DONOTS_PAGE.GUEST)
    }
  }

  return (
    <>
      <Box sx={{position: 'absolute', display: 'grid', top: '30%', px: '30px'}}>
        <Box sx={{display: 'flex', mb: '15px'}}>
          <SvgCommonIcons type={ICON_TYPE.MAIN_DONOTS}/>
          <SvgCommonIcons alt={ALT_STRING.MAIN.DONOTS_LOGO} type={ICON_TYPE.DONOTS_44PX} sx={{ml: '12px'}}/>
        </Box>
        <Typography variant={'b_18_m'} sx={{color: '#FFFFFF'}}>
          {MAIN.TITLE}
        </Typography>
      </Box>

      {/* 하단 */}
      <Box sx={{position: 'absolute', bottom: 0, left: '50%', transform: 'translate(-50%, 0)'}}>
        {/*<Box sx={{display: 'flex', justifyContent: 'center', mb: '20px'}}>*/}
        {/*  <SvgCommonIcons id={PROVIDER_TYPE.NAVER} onClick={handleClickSignUpSns} type={ICON_TYPE.LOGIN_NAVER} sx={{mx: '10px'}}/>*/}
        {/*  <SvgCommonIcons id={PROVIDER_TYPE.KAKAO} onClick={handleClickSignUpSns} type={ICON_TYPE.LOGIN_KAKAO} sx={{mx: '10px'}}/>*/}
        {/*  <SvgCommonIcons id={PROVIDER_TYPE.GOOGLE} onClick={handleClickSignUpSns} type={ICON_TYPE.LOGIN_GOOGLE}*/}
        {/*                  sx={{mx: '10px'}}/>*/}
        {/*</Box>*/}

        <Box sx={{textAlign: 'center', mb: '30px'}}>
          <Box sx={{display: 'flex', justifyContent: 'center', mb: '24px'}}>
            <NextLink href={PATH_DONOTS_PAGE.AUTH.LOGIN} passHref>
              <Typography
                tabIndex={0}
                onKeyDown={handleLoginKeyDown}
                variant={'b1_16_r_1l'}
                sx={{color: '#FFFFFF', cursor: 'pointer'}}
              >
                {MAIN.LOGIN}
              </Typography>
            </NextLink>
            <Typography variant={'b1_16_r_1l'} sx={{color: '#FFFFFF', px: '12px'}}>
              {MAIN.DIVIDER}
            </Typography>
            <Typography
              tabIndex={0}
              onClick={onClickSignUp}
              onKeyDown={(e) => handleEnterPress(e, onClickSignUp)}
              variant={'b1_16_r_1l'}
              sx={{color: '#FFFFFF', cursor: 'pointer'}}
            >
              {MAIN.SIGN_UP}
            </Typography>
          </Box>
          <NextLink href={PATH_DONOTS_PAGE.GUEST} passHref>
            <Typography
              tabIndex={0}
              onKeyDown={handleGuestKeyDown}
              variant={'b2_14_r_1l'}
              sx={{color: '#FFFFFF', textDecoration: 'underline', cursor: 'pointer'}}
            >
              {MAIN.GUEST}
            </Typography>
          </NextLink>
          <Box sx={{display: 'flex', justifyContent: 'center', mt: '60px'}}>
            <Typography
              tabIndex={0}
              onClick={handlePersonalTerms}
              onKeyDown={(e) => handleEnterPress(e, handlePersonalTerms)}
              variant={'b2_14_b_1l'}
              sx={{color: '#FFFFFF', opacity: 0.5, cursor: 'pointer'}}
            >
              {MAIN.PERSONAL}
            </Typography>
            <Typography variant={'b2_14_r_1l'} sx={{color: '#FFFFFF', opacity: 0.5, px: '12px'}}>
              {MAIN.DIVIDER}
            </Typography>
            <Typography
              tabIndex={0}
              onClick={handleServiceTerms}
              onKeyDown={(e) => handleEnterPress(e, handleServiceTerms)}
              variant={'b2_14_r_1l'}
              sx={{color: '#FFFFFF', opacity: 0.5, cursor: 'pointer'}}
            >
              {MAIN.SERVICE}
            </Typography>
          </Box>
        </Box>
      </Box>

      {showServiceTerms &&
        <IFrameDialog title={ALT_STRING.MAIN.PRIVACY} open={showServiceTerms} url={serviceContents} onBack={() => setShowServiceTerms(false)}/>}
      {showPersonalTerms &&
        <IFrameDialog title={ALT_STRING.MAIN.PERSONAL} open={showPersonalTerms} url={personalContents} onBack={() => setShowPersonalTerms(false)}/>}
    </>
  )
}