import PropTypes from 'prop-types';
import {Box, Stack, Typography} from '@mui/material';
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {HOME_GUEST_COMMON} from "../../constant/home/Guest";
import {handleEnterPress} from "../../utils/onKeyDownUtils";

ServiceMsg.propTypes = {
  handleClick: PropTypes.any,
};

export default function ServiceMsg({handleClick}) {

  return (
    <Stack tabIndex={1} direction="row" alignItems="center" sx={{mb: '36px'}} onKeyDown={(e) => handleEnterPress(e, handleClick)}>
      <SvgCommonIcons type={ICON_TYPE.LOGO_CHOCO_46PX}/>
      <Box sx={{ml: '12px'}} onClick={handleClick}>
        <Typography
          sx={{
            color: '#303030',
            lineHeight: '18px',
            fontSize: '18px',
            fontWeight: 500,
            background: `linear-gradient(to top, #FFDEB3 40%, white 40%)`
          }}
        >
          {HOME_GUEST_COMMON.SERVICE_MESSAGE.TITLE}
        </Typography>
      </Box>
    </Stack>
  );
}
