// @mui
import {Button, Dialog, DialogActions, DialogTitle,} from '@mui/material';
import {useRecoilState, useSetRecoilState} from "recoil";
import {guestModalSelector} from "../../recoil/selector/guest/guestModalOpenSelector";
import {guestInfoSelector} from "../../recoil/selector/guest/guestInfoSelector";

// ----------------------------------------------------------------------

export default function TBDmodal() {
  const [guestModal, setGuestModal] = useRecoilState(guestModalSelector)
  const setGuestInfo = useSetRecoilState(guestInfoSelector); // useSetRecoilState 를 통해 setter 반환
  let title

  const handleClose = () => {
    setGuestModal({isOpen: false})
    setTimeout(() => {
      setGuestModal({type: ''})
    }, 100);
  };

  switch (guestModal.type) {
    case "bn1":
      title = "[ 키 & 몸무게 작성화면 이동예정 ]"
      break;
    case "bn2":
      title = "[ 알레르기 선택화면 이동예정 ]"
      break;
    case "bn3":
      title = "[ 건강태그 선택화면 이동예정 ]"
      break;
    default:
      title = "[ TBD 화면이동 예정 ]"
  }

  const handleConfirm = () => {
    switch (guestModal.type) {
      case "bn1":
        setGuestInfo({height: '130', weight: '30'})
        break;
      case "bn2":
        setGuestInfo({allergies: ['egg']})
        break;
      case "bn3":
        setGuestInfo({healthKeywords: ['weight']})
        break;
    }

    handleClose();
  }


  return (
    <div>
      <Dialog open={guestModal.isOpen} onClose={handleClose} sx={{p: '10px'}}>
        <DialogTitle sx={{pb: '15px'}}>{title}</DialogTitle>

        <DialogActions>
          <Button onClick={handleClose} color="inherit">
            취소
          </Button>
          <Button onClick={handleConfirm} variant="contained">
            {guestModal.type !== '' ? '입력완료' : '확인'}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
