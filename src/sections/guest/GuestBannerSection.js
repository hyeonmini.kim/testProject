import {useRecoilValue} from "recoil";
import CardListPosts from "../home/personalization/CardListPosts";
import {
  authChildAllergyState,
  authChildBirthState,
  authChildGenderState,
  authChildHeightState,
  authChildKeywordState,
  authChildWeightState
} from "../../recoil/atom/sign-up/auth";
import CardSwipePosts from "../home/personalization/CardSwipePosts";
import {
  getNoUserAllergyBasedRecipeListMutate,
  getNoUserBodyBasedRecipeListMutate,
  getNoUserHealthBasedRecipeListMutate
} from "../../api/guestApi";
import {HOME_GUEST_COMMON} from "../../constant/home/Guest";
import {useEffect, useState} from "react";
import {Box, Card, Stack, Typography} from "@mui/material";
import HeaderBreadcrumbs from "../../components/home/HeaderBreadcrumbs";
import {useRouter} from "next/router";
import {PATH_DONOTS_PAGE, PATH_DONOTS_STATIC_PAGE} from "../../routes/paths";
import {FROM_TYPE} from "../../constant/sign-up/SignUp";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import Image from "../../components/image";
import {HOME_PERSONAL_COMMON} from "../../constant/home/Personalization";

export default function GuestBannerSection({mainRef}) {
  const childBirth = useRecoilValue(authChildBirthState)
  const childHeight = useRecoilValue(authChildHeightState)
  const childWeight = useRecoilValue(authChildWeightState)
  const childGender = useRecoilValue(authChildGenderState)
  const childAllergy = useRecoilValue(authChildAllergyState)
  const childKeyword = useRecoilValue(authChildKeywordState)

  const isHealthData = (childBirth !== '' && childHeight !== '' && childWeight !== '' && childGender !== '')

  const {mutate: mutateGetNoUserBodyBasedRecipeList} = getNoUserBodyBasedRecipeListMutate()
  const {mutate: mutateGetNoUserAllergyBasedRecipeList} = getNoUserAllergyBasedRecipeListMutate()
  const {mutate: mutateGetNoUserHealthBasedRecipeList} = getNoUserHealthBasedRecipeListMutate()

  const [body, setBody] = useState([])
  const [allergy, setAllergy] = useState([])
  const [keyword, setKeyword] = useState([])

  useEffect(() => {
    if (isHealthData) {
      mutateGetNoUserBodyBasedRecipeList({
        birthday: childBirth.replaceAll('.', '-'),
        gender: childGender === 'MALE' ? '0' : '1',
        height: childHeight,
        weight: childWeight,
      }, {
        onSuccess: (data) => {
          setBody(data)
        }
      })
    }
  }, [isHealthData])

  useEffect(() => {
    let param = {}
    if (childAllergy?.length > 0) {
      param = {
        params: {
          baby_allergy_ingredient_key: childAllergy.map(item => item?.key).join(',')
        }
      }
    }
    mutateGetNoUserAllergyBasedRecipeList(param, {
      onSuccess: (data) => {
        setAllergy(data)
      }
    })
  }, [childAllergy])

  useEffect(() => {
    if (childKeyword?.length > 0) {
      mutateGetNoUserHealthBasedRecipeList({
        params: {
          baby_concern_key: childKeyword.map(item => item?.key).join(',')
        }
      }, {
        onSuccess: (data) => {
          setKeyword(data)
        }
      })
    }
  }, [childKeyword])

  const router = useRouter()
  const handleUserBanner = () => {
    setHackleTrack(HACKLE_TRACK.INDEX_LOGOUT_BN01)
    router?.push({
      pathname: PATH_DONOTS_PAGE.AUTH.SIGNUP,
      query: {from: FROM_TYPE.GUEST_CHILD_PROFILE}
    })
  }

  const handleCustomBanner = () => {
    setHackleTrack(HACKLE_TRACK.INDEX_LOGOUT_BN02)
    router?.push({
      pathname: PATH_DONOTS_PAGE.AUTH.SIGNUP,
      query: {from: FROM_TYPE.GUEST_CHILD_ALLERGY}
    })
  }

  const handleKeywordBanner = () => {
    setHackleTrack(HACKLE_TRACK.INDEX_LOGOUT_BN03)
    router?.push({
      pathname: PATH_DONOTS_PAGE.AUTH.SIGNUP,
      query: {from: FROM_TYPE.GUEST_CHILD_KEYWORD}
    })
  }

  const isAllCheckBanner = () => {
    if (isHealthData && childAllergy && childKeyword?.length) {
      return true
    }
    return false
  }

  const isNotAllCheckBanner = () => {
    if (!isHealthData && !childAllergy && !childKeyword?.length) {
      return true
    }
    return false
  }


  return (
    <Box sx={{mt: '10px'}}>

      {/* 헤더 타이틀 */}
      {(isNotAllCheckBanner()) &&
        <HeaderBreadcrumbs heading={HOME_GUEST_COMMON.BANNER.TITLE} sx={{pt: '30px', pb: '10px'}}/>
      }

      {/* 성장 도움 레시피 */}
      {isHealthData &&
        <CardSwipePosts
          title={HOME_GUEST_COMMON.USER.TITLE}
          posts={body}
          sx={{pt: '30px', pb: '20px'}}
        />
      }

      {/* 알레르기 맞춤 레시피 */}
      {childAllergy &&
        <CardListPosts
          title={HOME_GUEST_COMMON.CUSTOM.TITLE}
          mainRef={mainRef}
          posts={allergy}
          sx={{pt: '30px', pb: '20px'}}
        />}

      {/* 키워드 맞춤 레시피 */}
      {childKeyword?.length > 0 &&
        <CardListPosts
          title={HOME_GUEST_COMMON.KEYWORD.TITLE}
          posts={keyword}
          sx={{pt: '30px', pb: '20px'}}
        />
      }

      {!isAllCheckBanner() &&
        <Stack spacing={'10px'} sx={{pt: '10px', pb: '20px'}}>
          {/* 배너 */}
          {!isHealthData &&
            <GuestBanner
              title={HOME_GUEST_COMMON.USER.BANNER}
              background={'#FFD6B9'}
              icon={PATH_DONOTS_STATIC_PAGE.ICONS('/images/icons/guest/banner_user.png')}
              iconSx={{width: '92px', right: '28px'}}
              onClick={handleUserBanner}
            />
          }
          {!childAllergy &&
            <GuestBanner
              title={HOME_GUEST_COMMON.CUSTOM.BANNER}
              background={'#FFD294'}
              icon={PATH_DONOTS_STATIC_PAGE.ICONS('/images/icons/guest/banner_custom.png')}
              iconSx={{width: '127px', right: '10px', top: '6px'}}
              onClick={handleCustomBanner}
            />
          }
          {!childKeyword?.length > 0 &&
            <GuestBanner
              title={HOME_GUEST_COMMON.KEYWORD.BANNER}
              background={'#FFE58A'}
              icon={PATH_DONOTS_STATIC_PAGE.ICONS('/images/icons/guest/banner_keyword.png')}
              iconSx={{width: '85px', height: '85px', right: '32px', top: '10px'}}
              onClick={handleKeywordBanner}
            />
          }
        </Stack>
      }
    </Box>
  )
}

function GuestBanner({title, onClick, background = '#FFD6B9', icon, iconSx}) {
  return (
    <Card
      tabIndex={1}
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
      sx={{
        p: '30px 0px 26px 20px',
        display: 'flex',
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: '8px',
        bgcolor: background,
        height: '100px',
        zIndex: 1
      }}
    >
      <Typography
        aria-hidden={true}
        sx={{
          fontSize: '16px',
          fontWeight: 700,
          lineHeight: '22px',
          whiteSpace: 'pre-line'
        }}
      >
        {title}
      </Typography>

      {icon &&
        <Image
          alt={title}
          src={icon}
          sx={{position: 'absolute', width: '92px', right: '28px', top: '10px', ...iconSx}}
        />
      }
    </Card>
  );
}