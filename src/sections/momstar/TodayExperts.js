import {Box, Divider, Grid, Skeleton, Stack} from "@mui/material";
import PropTypes from "prop-types";
import {MOMSTAR_COMMON} from "../../constant/momstar/Momstar";
import {useRecoilState} from "recoil";
import {momStarIsScrollSelector} from "../../recoil/selector/momstar/momstarSelector";
import * as React from "react";
import {useEffect} from "react";
import ExpertItem from "../../components/momstar/ExpertItem";
import LoadingScreen from "../../components/common/LoadingScreen";
import usePagination from "../../hooks/usePagination";
import {getExpertListMutate} from "../../api/rankingApi";
import useScroll from "../../hooks/useScroll";
import {PAGINATION, RANKING_PAGINATION} from "../../constant/common/Pagination";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import {STACK} from "../../constant/common/StackNavigation";
import useStackNavigation from "../../hooks/useStackNavigation";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";

TodayExperts.propTypes = {
  items: PropTypes.array,
  onClick: PropTypes.func,
}

export default function TodayExperts({mainRef, fetchItems}) {
  const {navigation, preFetch} = useStackNavigation()
  const [isScroll, setIsScroll] = useRecoilState(momStarIsScrollSelector)
  const {
    param,
    items,
    setItems,
    setTarget,
    pageOffset,
    hasNextOffset,
    initPagination,
    isLoading,
    getPaginationItems,
  } = usePagination(getExpertListMutate, RANKING_PAGINATION.PAGE_SIZE)

  const {setTarget: setScrollTarget} = useScroll(
    () => setIsScroll(false),
    () => setIsScroll(true)
  )

  useEffect(() => {
    if (fetchItems) {
      initPagination(fetchItems)
    }
  }, [fetchItems])

  const handleClick = (item) => {
    let linkTo = PATH_DONOTS_PAGE.MY.EXPERTS(item?.memberKey?.toString());

    navigation.push(STACK.MOMSTAR.TYPE, linkTo, {
      ...STACK.MOMSTAR.DATA,
      activeTab: MOMSTAR_COMMON.EXPERT.TAB,
      isScroll: isScroll,
      itemExpert: items,
      scrollY: mainRef?.current?.scrollTop
    })
  }

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.RANKING_EXPERT)
  }, [])

  return (
    <>
      <Box ref={setScrollTarget} sx={{mt: '16px'}}/>
      <Grid container sx={{p: 0}}>
        {isLoading && <LoadingScreen/>}
        {
          items === PAGINATION.INIT_DATA
            ? <ExpertSkeleton/>
            : items?.length ? (
              <>
                <ExpertItems items={items} onClick={handleClick}/>
                {hasNextOffset.current && <Box ref={setTarget}/>}
              </>
            ) : <ExpertSkeleton/>
        }
      </Grid>
    </>
  )
}

function ExpertItems({items, onClick}) {
  return (
    <>
      {
        items?.map((item, index) =>
          <Grid item key={index} xs={12}>
            {index > 0 && <Divider sx={{my: '20px'}}/>}
            <ExpertItem item={item} rank={index + 1} onClick={onClick}/>
          </Grid>
        )
      }
    </>
  )
}

function ExpertSkeleton() {
  return (
    <>
      {[...Array(10)].map((item, index) =>
        <Grid item key={index} xs={12}>
          {index > 0 && <Divider sx={{my: '20px'}}/>}
          <Stack key={index} spacing={'15px'} direction="row" alignItems="center" sx={{height: '65px'}}>
            <Skeleton variant="circular" width={60} height={60}/>
            <Skeleton variant="rectangular" height={60} sx={{flexGrow: 1, borderRadius: 2}}/>
          </Stack>
        </Grid>
      )}
    </>
  );
}