import {Box, Divider, Skeleton, Stack, Tab, Tabs, Typography} from "@mui/material";
import {MY_COMMON, MY_TABS} from "../../../constant/my/MyConstants";
import PropTypes from "prop-types";
import {useRecoilState} from "recoil";
import * as React from "react";
import {useEffect} from "react";
import {othersActiveTabSelector} from "../../../recoil/selector/my/information/othersStateSelector";
import {getSearchOthersRecipes, getSearchOthersScrapRecipesMutate, getSearchOthersWriteRecipesMutate} from "../../../api/myApi";
import {PAGINATION} from "../../../constant/common/Pagination";
import MyOthersMembershipInfoSection from "./MyOthersMembershipInfoSection";
import usePagination from "../../../hooks/usePagination";
import RecipeItemOthers from "../../../components/my/others/RecipeItemOthers";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {STACK} from "../../../constant/common/StackNavigation";
import useStackNavigation from "../../../hooks/useStackNavigation";
import {getURIString} from "../../../utils/formatString";
import LoadingScreen from "../../../components/common/LoadingScreen";
import {clickBorderNone} from "../../../constant/common/Common";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";

MyOthersMembershipInfoSectionAndTabSection.propTypes = {
  csr: PropTypes.object,
  viewsCount: PropTypes.number,
  scrapCount: PropTypes.number,
};

export default function MyOthersMembershipInfoSectionAndTabSection({user_key, mainRef, hackleProperty}) {
  const {data: userRecipeInfo} = getSearchOthersRecipes({
    params: {
      user_key: user_key,
    }
  })

  return (
    <>
      <MyOthersMembershipInfoSection userRecipeInfo={userRecipeInfo}/>
      <Divider sx={{px: '0px', mt: '20px', height: '6px', background: '#F5F5F5', borderBottomWidth: 'inherit'}}/>
      <MyOthersTabSection user_key={user_key} userRecipeInfo={userRecipeInfo} mainRef={mainRef} hackleProperty={hackleProperty}/>
    </>
  )
}

function MyOthersTabSection({user_key, userRecipeInfo, mainRef, hackleProperty}) {
  const [activeTab, setActiveTab] = useRecoilState(othersActiveTabSelector);

  const handleTabClick = (value) => {
    if (value === MY_COMMON.MY_TAB_WRITE && hackleProperty) {
      setHackleTrack(HACKLE_TRACK.CRUNCH_TAB_RECIPE, {
        crunch_name: hackleProperty
      })
    } else if (value === MY_COMMON.MY_TAB_SCRAP && hackleProperty) {
      setHackleTrack(HACKLE_TRACK.CRUNCH_TAB_SCRAP, {
        crunch_name: hackleProperty
      })
    }

    setActiveTab(value);
  };

  const {
    param: paramWrite,
    items: itemsWrite,
    setItems: setItemsWrite,
    setTarget: setTargetWrite,
    pageOffset: pageOffsetWrite,
    hasNextOffset: hasNextOffsetWrite,
    initPagination: initPaginationWrite,
    isLoading: isLoadingWrite,
    getPaginationItems: getPaginationItemsWrite,
  } = usePagination(getSearchOthersWriteRecipesMutate, PAGINATION.PAGE_SIZE)

  paramWrite.current = {
    params: {
      user_key: user_key,
      paging_type: MY_COMMON.MY_TAB_WRITE
    }
  }

  const {
    param: paramScrap,
    items: itemsScrap,
    setItems: setItemsScrap,
    setTarget: setTargetScrap,
    pageOffset: pageOffsetScrap,
    hasNextOffset: hasNextOffsetScrap,
    initPagination: initPaginationScrap,
    isLoading: isLoadingScrap,
    getPaginationItems: getPaginationItemsScrap,
  } = usePagination(getSearchOthersScrapRecipesMutate, PAGINATION.PAGE_SIZE)

  paramScrap.current = {
    params: {
      user_key: user_key,
      paging_type: MY_COMMON.MY_TAB_SCRAP
    }
  }

  const {navigation, preFetch} = useStackNavigation()

  useEffect(() => {
    const fetch = preFetch(STACK.MY_OTHERS.TYPE)
    if (fetch) {
      if (activeTab === MY_COMMON.MY_TAB_WRITE) {
        initPaginationWrite(fetch?.othersWriteList)
      }

      if (activeTab === MY_COMMON.MY_TAB_SCRAP) {
        initPaginationScrap(fetch?.othersScrapList)
      }
    } else {
      if (activeTab === MY_COMMON.MY_TAB_WRITE) {
        getPaginationItemsWrite()
      }

      if (activeTab === MY_COMMON.MY_TAB_SCRAP) {
        getPaginationItemsScrap()
      }
    }
  }, [activeTab])

  const handleClick = (recipe_user_name, recipe_name, recipe_key) => {
    let linkTo = PATH_DONOTS_PAGE.RECIPE.DETAIL(recipe_user_name, getURIString(recipe_name), recipe_key)

    navigation.push(STACK.MY_OTHERS.TYPE, linkTo, {
      ...STACK.MY_OTHERS.DATA,
      activeTab: activeTab,
      othersWriteList: itemsWrite,
      othersScrapList: itemsScrap,
      scrollY: mainRef?.current?.scrollTop
    })
  }

  function getCount(value) {
    if (value === MY_COMMON.MY_TAB_WRITE) {
      return userRecipeInfo?.recipe_writer_cnt || 0
    } else {
      return userRecipeInfo?.recipe_scrap_cnt || 0
    }
  }

  return (
    <>
      <Box sx={{height: '48px', borderBottom: '2px solid #F5F5F5'}}>
        <Tabs
          sx={{'& .MuiTabs-indicator': {backgroundColor: 'black'}, '& .Mui-selected': {color: 'black'}}}
          variant="fullWidth"
          value={activeTab}
          onChange={(event, newValue) => handleTabClick(newValue)}
        >
          {MY_TABS.slice(0, 2).map((tab, index) => (
            <Tab
              tabIndex={3}
              key={tab.value}
              value={tab.value}
              label={
                <Stack direction="row" spacing="8px">
                  <Typography fontWeight="400" fontSize="16px" sx={{lineHeight: '24px', color: '#000000'}}>
                    {tab.label}
                  </Typography>
                  <Typography fontWeight="400" fontSize="16px" sx={{lineHeight: '24px', color: '#888888'}}>
                    {getCount(tab.value)}
                  </Typography>
                </Stack>
              }
              sx={{'&:not(:last-of-type)': {marginRight: '0px',}, ...clickBorderNone}}
            />
          ))}
        </Tabs>
      </Box>

      <Box sx={{px: '20px', mt: '20px',}}>
        {isLoadingWrite && <LoadingScreen/>}
        {
          activeTab === MY_COMMON.MY_TAB_WRITE && (
            itemsWrite === PAGINATION.INIT_DATA
              ? <TabSkeleton/>
              : itemsWrite?.length ? (
                <>
                  <TabSectionItems items={itemsWrite} onClick={handleClick}/>
                  {hasNextOffsetWrite.current && <Box ref={setTargetWrite}/>}
                </>
              ) : <NoItems activeTab={activeTab}/>
          )
        }
        {isLoadingScrap && <LoadingScreen/>}
        {
          activeTab === MY_COMMON.MY_TAB_SCRAP && (
            itemsScrap === PAGINATION.INIT_DATA
              ? <TabSkeleton/>
              : itemsScrap?.length ? (
                <>
                  <TabSectionItems items={itemsScrap} onClick={handleClick}/>
                  {hasNextOffsetScrap.current && <Box ref={setTargetScrap}/>}
                </>
              ) : <NoItems activeTab={activeTab}/>
          )
        }
      </Box>
    </>
  )
}

function TabSkeleton() {
  return (
    <>
      {[...Array(10)].map((item, index) =>
        <SkeletonItem key={index}/>
      )}
    </>
  );
}

function TabSectionItems({items, onClick}) {
  return (
    <>
      {
        items?.map((item, index) =>
          <RecipeItemOthers key={index} item={{...item}} isLazy={index < 10 ? false : true} onClick={onClick}/>
        )
      }
    </>
  )
}

function SkeletonItem() {
  return (
    <Stack direction="row" spacing="8px" sx={{my: '12px'}}>
      <Box sx={{position: 'relative'}}>
        <Skeleton variant="rounded" sx={{borderRadius: '8px', width: '100px', height: '100px'}}/>
      </Box>
      <Box sx={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between', flexGrow: 1}}>
        <Skeleton sx={{height: '24px'}}>
        </Skeleton>
        <Skeleton sx={{height: '20px'}}>
        </Skeleton>
        <Box sx={{flexGrow: 1}}/>
        <Stack direction="row" alignItems={'center'}>
          <Skeleton variant="rectangular" sx={{width: '50px', height: '16px', mr: '15px', borderRadius: 1}}/>
          <Skeleton variant="rectangular" sx={{width: '50px', height: '16px', borderRadius: 1}}/>
        </Stack>
      </Box>
    </Stack>
  );
}

function NoItems({activeTab}) {
  return (
    <Box
      sx={{
        height: `calc(100vh - 378px)`,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Typography fontWeight="500" fontSize="16px" sx={{lineHeight: '24px', color: '#666666'}}>
        {getNoItemText(activeTab)}
      </Typography>
    </Box>
  )
}

function getNoItemText(activeTab) {
  if (activeTab === MY_COMMON.MY_TAB_WRITE) {
    return MY_COMMON.RECIPE_NO_ITEM
  } else {
    return MY_COMMON.SCRAP_NO_ITEM
  }
}
