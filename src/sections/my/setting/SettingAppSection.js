import {useRecoilState} from "recoil";
import {
  settingIsAutoLoginSelector,
  settingIsScreenLockSelector,
  settingShowPushSelector
} from "../../../recoil/selector/my/settingSelector";
import * as React from "react";
import {useEffect} from "react";
import {MY_SETTING, MY_SETTING_OPTION} from "../../../constant/my/Setting";
import {Box, Divider, Typography} from "@mui/material";
import SettingItem from "../../../components/my/setting/SettingItem";
import {getNativeOpt, setNativeOpt} from "../../../channels/commonChannel";

export default function SettingAppSection() {
  const [showPush, setShowPush] = useRecoilState(settingShowPushSelector)
  const [autoLogin, setAutoLogin] = useRecoilState(settingIsAutoLoginSelector)
  const [screenLock, setScreenLock] = useRecoilState(settingIsScreenLockSelector)

  useEffect(() => {
    try {
      getNativeOpt(MY_SETTING_OPTION.AUTO_LOGIN, (result) => {
        setAutoLogin(JSON.parse(result.val))
      })
    } catch (e) {
      setAutoLogin(false)
    }
    try {
      getNativeOpt(MY_SETTING_OPTION.SCREEN_LOCK, (result) => {
        setScreenLock(JSON.parse(result.val))
      })
    } catch (e) {
      setScreenLock(false)
    }
  }, [])

  const handleAutoLogin = () => {
    setNativeOpt({type: MY_SETTING_OPTION.AUTO_LOGIN, value: String(!autoLogin)})
    setAutoLogin(!autoLogin)
  }

  const handleScreenLock = () => {
    setNativeOpt({type: MY_SETTING_OPTION.SCREEN_LOCK, value: String(!screenLock)})
    setScreenLock(!screenLock)
  }

  return (
    <>
      <Box sx={{display: 'flex', alignItems: 'center', height: '56px'}}>
        <Typography sx={{fontSize: '14px', fontWeight: '400', lineHeight: '14px', color: '#888888'}}>
          {MY_SETTING.APP_SETTING}
        </Typography>
      </Box>
      {/* MVP1 미구현. MVP2 예정 */}
      {/*<SettingItem title={MY_SETTING.PUSH.MENU_TITLE} onClick={() => setShowPush(true)}/>*/}
      <Divider sx={{borderColor: '#F5F5F5'}}/>
      <SettingItem title={MY_SETTING.AUTO_LOGIN} toggle={autoLogin} onClick={handleAutoLogin}/>
      <Divider sx={{borderColor: '#F5F5F5'}}/>
      <SettingItem title={MY_SETTING.SCREEN_LOCK} toggle={screenLock} onClick={handleScreenLock}/>
    </>
  )
}
