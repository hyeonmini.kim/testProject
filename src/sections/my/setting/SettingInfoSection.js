import {useRecoilState} from "recoil";
import {
  licenseUrlSelector,
  settingShowFAQSelector,
  settingShowLicenseSelector,
  settingShowNoticeSelector,
  settingShowOneOnOneInquiriesSelector,
  settingShowTermsSelector,
  settingShowVersionSelector
} from "../../../recoil/selector/my/settingSelector";
import {Box, Divider, Typography} from "@mui/material";
import {MY_SETTING} from "../../../constant/my/Setting";
import SettingItem from "../../../components/my/setting/SettingItem";
import * as React from "react";
import {isNative} from "../../../utils/envUtils";
import {getTermsOfUseByTitleMutate} from "../../../api/mySettingApi";

export default function SettingInfoSection() {
  const [showNotice, setShowNotice] = useRecoilState(settingShowNoticeSelector)
  const [showTerms, setShowTerms] = useRecoilState(settingShowTermsSelector)
  const [showLicense, setShowLicense] = useRecoilState(settingShowLicenseSelector)
  const [showFAQ, setShowFAQ] = useRecoilState(settingShowFAQSelector)
  const [showVersion, setShowVersion] = useRecoilState(settingShowVersionSelector)
  const [showOneOnOneInquiries, setShowOneOnOneInquiries] = useRecoilState(settingShowOneOnOneInquiriesSelector)

  const [licenseUrl, setLicenseUrl] = useRecoilState(licenseUrlSelector)

  const {mutate: termsOfUseByTitle} = getTermsOfUseByTitleMutate()

  const onClickLicense = () => {
    termsOfUseByTitle({
      title: encodeURIComponent(MY_SETTING.LICENSE.HEADER_TITLE)
    }, {
      onSuccess: (result) => {
        setLicenseUrl(result?.bodyHtmlFileUrl)
        setShowLicense(true)
      }
    })
  }

  return (
    <>
      <Box sx={{mt: '10px', display: 'flex', alignItems: 'center', height: '56px'}}>
        <Typography sx={{fontSize: '14px', fontWeight: '400', lineHeight: '14px', color: '#888888'}}>
          {MY_SETTING.APP_INFO}
        </Typography>
      </Box>
      <SettingItem title={MY_SETTING.NOTICE.MENU_TITLE} onClick={() => setShowNotice(true)}/>
      <Divider sx={{borderColor: '#F5F5F5'}}/>
      <SettingItem title={MY_SETTING.TERMS.MENU_TITLE} onClick={() => setShowTerms(true)}/>
      <Divider sx={{borderColor: '#F5F5F5'}}/>
      <SettingItem title={MY_SETTING.LICENSE.MENU_TITLE} onClick={onClickLicense}/>
      <Divider sx={{borderColor: '#F5F5F5'}}/>
      <SettingItem title={MY_SETTING.FAQ.MENU_TITLE} onClick={() => setShowFAQ(true)}/>
      <Divider sx={{borderColor: '#F5F5F5'}}/>
      <SettingItem title={MY_SETTING.VERSION.MENU_TITLE} onClick={() => setShowVersion(true)}/>
      <Divider sx={{borderColor: '#F5F5F5'}}/>
      <SettingItem title={MY_SETTING.ONE_ON_ONE_INQUIRIES.MENU_TITLE} onClick={() => setShowOneOnOneInquiries(true)}/>
      <Divider sx={{borderColor: '#F5F5F5'}}/>
    </>
  )
}