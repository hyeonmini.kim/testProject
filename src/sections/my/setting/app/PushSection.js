import PropTypes from "prop-types";
import {Container, Divider, Fade} from "@mui/material";
import {MY_SETTING, MY_SETTING_OPTION} from "../../../../constant/my/Setting";
import SettingItem from "../../../../components/my/setting/SettingItem";
import {settingIsEventInfoSelector, settingIsRecipeResultSelector} from "../../../../recoil/selector/my/settingSelector";
import {useRecoilState} from "recoil";
import * as React from "react";
import {useEffect} from "react";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {getNativeOpt, setNativeOpt} from "../../../../channels/commonChannel";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../layouts/my/MyHeaderDialogLayout";

PushSection.propTypes = {
  open: PropTypes.bool,
  onBack: PropTypes.func,
};

export default function PushSection({open, onBack}) {
  const [isEventInfo, setIsEventInfo] = useRecoilState(settingIsEventInfoSelector)
  const [isRecipeResult, setIsRecipeResult] = useRecoilState(settingIsRecipeResultSelector)

  useEffect(() => {
    getNativeOpt(MY_SETTING_OPTION.EVENT_INFO, (result) => {
      setIsEventInfo(JSON.parse(result.val))
    })
    getNativeOpt(MY_SETTING_OPTION.RECIPE_RESULT, (result) => {
      setIsRecipeResult(JSON.parse(result.val))
    })
  }, [])

  const handleEventInfo = () => {
    setNativeOpt({type: MY_SETTING_OPTION.EVENT_INFO, value: String(!isEventInfo)})
    setIsEventInfo(!isEventInfo)
  }

  const handleRecipeResult = () => {
    setNativeOpt({type: MY_SETTING_OPTION.RECIPE_RESULT, value: String(!isRecipeResult)})
    setIsRecipeResult(!isRecipeResult)
  }

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <MyHeaderDialogLayout
        title={MY_SETTING.PUSH.HEADER_TITLE}
        onBack={onBack}
      >
        <Container disableGutters maxWidth={'xs'} sx={{position: 'relative', mt: '20px', px: '20px'}}>
          <SettingItem title={MY_SETTING.PUSH.EVENT_INFO} toggle={isEventInfo} onClick={handleEventInfo}/>
          <Divider sx={{borderColor: '#F5F5F5'}}/>
          <SettingItem title={MY_SETTING.PUSH.RECIPE_RESULT} toggle={isRecipeResult} onClick={handleRecipeResult}/>
          <Divider sx={{borderColor: '#F5F5F5'}}/>
        </Container>
      </MyHeaderDialogLayout>
    </MainDialogLayout>
  )
}