import {Box, Container, Divider, Fade, Skeleton, Typography} from "@mui/material";
import {MY_SETTING} from "../../../../../constant/my/Setting";
import TextMaxLine from "../../../../../components/text-max-line";
import * as React from "react";
import Image from "../../../../../components/image";
import {Z_INDEX} from "../../../../../constant/common/ZIndex";
import {getFaqDetail} from "../../../../../api/mySettingApi";
import {getYYMMDDFromDate, nvlString} from "../../../../../utils/formatString";
import LoadingScreen from "../../../../../components/common/LoadingScreen";
import MyHeaderDialogLayout from "../../../../../layouts/my/MyHeaderDialogLayout";
import {MainDialogLayout} from "../../../../../layouts/main/MainLayout";
import {ALT_STRING} from "../../../../../constant/common/AltString";

export default function FAQDetail({open, onBack}) {
  const {data: item, isLoading} = getFaqDetail({key: open})
  return (
    <MainDialogLayout
      fullScreen
      open={!!open}
      TransitionComponent={Fade}
      sx={{
        zIndex: Z_INDEX.DIALOG,
        '& .MuiDialog-paper': {mx: 0},
      }}
    >
      <MyHeaderDialogLayout
        title={MY_SETTING.FAQ.HEADER_TITLE}
        onBack={onBack}
      >
        <Container disableGutters maxWidth={'xs'} sx={{pt: '20px', px: '20px'}}>
          {isLoading && <LoadingScreen dialog/>}
          {
            item ? (
              <>
                <FAQTitle item={item}/>
                <Divider sx={{borderColor: '#666666'}}/>
                <FAQContents item={item}/>
              </>
            ) : (
              <FAQSkeleton/>
            )
          }
        </Container>
      </MyHeaderDialogLayout>
    </MainDialogLayout>
  )
}

function FAQTitle({item}) {
  return (
    <>
      <TextMaxLine line={2} sx={{fontSize: '18px', fontWeight: 500, lineHeight: '24px', color: '#000000'}}>
        {nvlString(item?.question)}
      </TextMaxLine>
      <Typography sx={{my: '8px', fontSize: '14px', fontWeight: 400, lineHeight: '20px', color: '#888888'}}>
        {getYYMMDDFromDate(item?.createdDatetime)}
      </Typography>
    </>
  )
}

function FAQContents({item}) {
  return (
    <Box sx={{mt: '30px'}}>
      {item?.representativeImgUrl &&
        <Image alt={ALT_STRING.SETTING.CONTENT_DETAIL(item?.question)} isLazy={false} src={item?.representativeImgUrl}/>}
      <Typography sx={{
        mt: '20px',
        whiteSpace: 'pre-line',
        fontSize: '14px',
        fontWeight: 400,
        lineHeight: '20px',
        color: '#666666'
      }}>
        {nvlString(item?.answer)}
      </Typography>
    </Box>
  )
}

function FAQSkeleton() {
  return (
    <>
      <Skeleton variant="rectangular" sx={{my: '8px', height: '40px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{my: '11px', height: '18px', width: '30%', borderRadius: '10px'}}/>
      <Divider sx={{borderColor: '#666666'}}/>
      <Skeleton variant="rectangular" sx={{mt: '30px', height: '235px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '22px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', width: '40%', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '22px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', width: '60%', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '22px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', width: '30%', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '22px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
    </>
  )
}