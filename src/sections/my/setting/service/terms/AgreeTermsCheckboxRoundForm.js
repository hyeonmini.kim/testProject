import {Box, Checkbox, FormControlLabel, FormGroup, Typography} from "@mui/material";
import PropTypes from "prop-types";
import {ICON_COLOR, ICON_SIZE, SvgCheckRoundIcon} from "../../../../../constant/icons/icons";
import {clickBorderNone} from "../../../../../constant/common/Common";
import {handleEnterPress} from "../../../../../utils/onKeyDownUtils";

AgreeTermsCheckboxRoundForm.propTypes = {
  labelText: PropTypes.string.isRequired,
  checked: PropTypes.any.isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default function AgreeTermsCheckboxRoundForm({labelText, checked, handleChange}) {
  return (
    <FormGroup sx={{mr: '16px', mt: '10px'}}>
      <Box
        tabIndex={1}
        sx={{display: 'flex', ...clickBorderNone}}
        onKeyDown={(e) => checked ? null : handleEnterPress(e, handleChange)}
      >
        <FormControlLabel
          control={
            <Checkbox
              onChange={handleChange}
              checked={checked}
              icon={<SvgCheckRoundIcon color={ICON_COLOR.GREY} size={ICON_SIZE.PX24}/>}
              checkedIcon={<SvgCheckRoundIcon color={ICON_COLOR.PRIMARY} size={ICON_SIZE.PX24}/>}
              sx={{
                width: '24px',
                height: '24px',
                '&.MuiCheckbox-root svg': {
                  width: '24px',
                  height: '24px',
                }
              }}
            />
          }
          label={
            <Typography variant={'b1_16_r_1l'} sx={{ml: '9px', color: '#222222'}}>
              {labelText}
            </Typography>
          }
          sx={{alignItems: 'center', m: 0}}
        />
      </Box>
    </FormGroup>
  )
}
