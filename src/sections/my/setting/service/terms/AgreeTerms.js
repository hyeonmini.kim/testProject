import PropTypes from "prop-types";
import {Box, Button, Container, Slide, Typography} from "@mui/material";
import {MY_SETTING} from "../../../../../constant/my/Setting";
import {Z_INDEX} from "../../../../../constant/common/ZIndex";
import AgreeTermsCheckboxForm from "./AgreeTermsCheckboxForm";
import AgreeTermsCheckboxRoundForm from "./AgreeTermsCheckboxRoundForm";
import {useRecoilState, useSetRecoilState} from "recoil";
import {forwardRef, useEffect, useState} from "react";
import {dialogSelector, dialogShowDialogSelector} from "../../../../../recoil/selector/common/dialogSelector";
import {clickBorderNone, COMMON_DIALOG_TYPE} from "../../../../../constant/common/Common";
import {MY_COMMON} from "../../../../../constant/my/MyConstants";
import {SIGN_UP_DIALOG} from "../../../../../constant/sign-up/SignUp";
import {putAgreeMarketingMutate} from "../../../../../api/mySettingApi";
import {MainDialogLayout} from "../../../../../layouts/main/MainLayout";
import IFrameBodyLayout from "../../../../../layouts/common/IFrameBodyLayout";
import HeaderDialogLayout from "../../../../../layouts/auth/HeaderDialogLayout";
import useNativeBackKey from "../../../../../hooks/useNativeBackKey";
import {ALT_STRING} from "../../../../../constant/common/AltString";

AgreeTerms.propTypes = {
  open: PropTypes.bool,
  onBack: PropTypes.func,
};

const Transition = forwardRef((props, ref) => <Slide direction="up" ref={ref} {...props} />);

export default function AgreeTerms({open, contents, agreeChecked, onBack}) {
  const setDialogMessage = useSetRecoilState(dialogSelector)

  const [agreeMkt, setAgreeMkt] = useState(agreeChecked?.isTermsCollectingPersonalDataMarketingAgreed)
  const [disagreeMkt, setDisagreeMkt] = useState(!agreeChecked?.isTermsCollectingPersonalDataMarketingAgreed)
  const [agreeReceive, setAgreeReceive] = useState(agreeChecked?.isTextMessageReciveAgreed || agreeChecked?.isEmailReceiveAgreed)
  const [agreeSms, setAgreeSms] = useState(agreeChecked?.isTextMessageReciveAgreed)
  const [agreeEmail, setAgreeEmail] = useState(agreeChecked?.isEmailReceiveAgreed)
  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)

  const date = new Date()

  const {mutate: updateAgreeMkt} = putAgreeMarketingMutate()

  useEffect(() => {
    if (agreeChecked) {
      setAgreeMkt(agreeChecked?.isTermsCollectingPersonalDataMarketingAgreed)
      setDisagreeMkt(!agreeChecked?.isTermsCollectingPersonalDataMarketingAgreed)
      setAgreeReceive(agreeChecked?.isTextMessageReciveAgreed || agreeChecked?.isEmailReceiveAgreed)
      setAgreeSms(agreeChecked?.isTextMessageReciveAgreed)
      setAgreeEmail(agreeChecked?.isEmailReceiveAgreed)
    }
  }, [agreeChecked])

  useEffect(() => {
    if (agreeSms || agreeEmail) {
      setAgreeReceive(true)
    } else {
      setAgreeReceive(false)
    }
  }, [agreeSms, agreeEmail])

  const handleAgreeMkt = () => {
    setAgreeMkt(!agreeMkt)
    setDisagreeMkt(!disagreeMkt)

    setDialogMessage({
      type: COMMON_DIALOG_TYPE.ONE_BUTTON,
      message:
        !agreeMkt ?
          MY_COMMON.TEXT.AGREE_MKT(date.getFullYear(), date.getMonth() + 1, date.getDate())
          :
          MY_COMMON.TEXT.DISAGREE_MKT(date.getFullYear(), date.getMonth() + 1, date.getDate()),
      handleButton1: {text: SIGN_UP_DIALOG.CONFIRM},
    })
  }

  const handleAgreeReceive = () => {
    setAgreeReceive(true)
    setAgreeSms(true)
    setAgreeEmail(true)

    if (!agreeReceive) {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.ONE_BUTTON,
        message: MY_COMMON.TEXT.AGREE_RECEIVE(date.getFullYear(), date.getMonth() + 1, date.getDate()),
        handleButton1: {text: SIGN_UP_DIALOG.CONFIRM},
      })
    }
  }

  const handleDisagreeReceive = () => {
    setAgreeReceive(false)
    setAgreeSms(false)
    setAgreeEmail(false)

    if (agreeReceive) {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.ONE_BUTTON,
        message: MY_COMMON.TEXT.DISAGREE_RECEIVE(date.getFullYear(), date.getMonth() + 1, date.getDate()),
        handleButton1: {text: SIGN_UP_DIALOG.CONFIRM},
      })
    }
  }

  const handleAgreeSms = () => {
    setAgreeSms(!agreeSms)

    if (agreeEmail || !agreeSms) {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.ONE_BUTTON,
        message: MY_COMMON.TEXT.AGREE_RECEIVE(date.getFullYear(), date.getMonth() + 1, date.getDate()),
        handleButton1: {text: SIGN_UP_DIALOG.CONFIRM},
      })
    } else {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.ONE_BUTTON,
        message: MY_COMMON.TEXT.DISAGREE_RECEIVE(date.getFullYear(), date.getMonth() + 1, date.getDate()),
        handleButton1: {text: SIGN_UP_DIALOG.CONFIRM},
      })
    }
  }

  const handleAgreeEmail = () => {
    setAgreeEmail(!agreeEmail)

    if (agreeSms || !agreeEmail) {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.ONE_BUTTON,
        message: MY_COMMON.TEXT.AGREE_RECEIVE(date.getFullYear(), date.getMonth() + 1, date.getDate()),
        handleButton1: {text: SIGN_UP_DIALOG.CONFIRM},
      })
    } else {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.ONE_BUTTON,
        message: MY_COMMON.TEXT.DISAGREE_RECEIVE(date.getFullYear(), date.getMonth() + 1, date.getDate()),
        handleButton1: {text: SIGN_UP_DIALOG.CONFIRM},
      })
    }
  }

  const handleMarketing = () => {
    updateAgreeMkt({
      isTermsCollectingPersonalDataMarketingAgreed: agreeMkt,
      isTextMessageReciveAgreed: agreeSms,
      isEmailReceiveAgreed: agreeEmail,
    }, {
      onSuccess: () => {
        // 다이얼로그 종료
        onBack()
      }
    })
  }

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      if (showDialog) {
        setShowDialog(false)
      }
    }
  }, [handleBackKey])

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      TransitionComponent={Transition}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <HeaderDialogLayout handleClick={onBack}/>

      <Container disableGutters maxWidth={'xs'} sx={{display: 'flex', flexDirection: 'column', height: '100%'}}>

        <IFrameBodyLayout noToolbar={false}>
          <iframe title={ALT_STRING.SETTING.AGREE.MARKETING} src={contents} style={{width: '100%', height: '100%', border: 'none'}}/>
        </IFrameBodyLayout>

        {/* 동의/철회 선택 */}
        <Box sx={{marginX: '20px', marginTop: '30px'}}>
          {/* 개인정보 수집/이용 동의(동의철회) */}
          <Box>
            <Typography variant={'b1_16_r_1l'} sx={{color: '#222222'}}>
              {MY_SETTING.TERMS.AGREE.SUB_TITLE_1}
            </Typography>
            <Box sx={{display: 'flex'}}>
              <AgreeTermsCheckboxRoundForm
                labelText={MY_SETTING.TERMS.AGREE.AGREE}
                checked={agreeMkt === undefined ? false : agreeMkt}
                handleChange={handleAgreeMkt}
              />
              <AgreeTermsCheckboxRoundForm
                labelText={MY_SETTING.TERMS.AGREE.DISAGREE}
                checked={disagreeMkt === undefined ? false : disagreeMkt}
                handleChange={handleAgreeMkt}
              />
            </Box>
          </Box>

          {/* 전송매체별 광고성 정보의 수신 동의 */}
          <Box sx={{marginTop: '26px'}}>
            <Typography variant={'b1_16_r_1l'} sx={{color: '#222222'}}>
              {MY_SETTING.TERMS.AGREE.SUB_TITLE_2}
            </Typography>
            <Box sx={{display: 'flex'}}>
              <AgreeTermsCheckboxRoundForm
                labelText={MY_SETTING.TERMS.AGREE.AGREE}
                checked={agreeReceive === undefined ? false : agreeReceive}
                handleChange={handleAgreeReceive}
              />
              <AgreeTermsCheckboxRoundForm
                labelText={MY_SETTING.TERMS.AGREE.DISAGREE}
                checked={agreeReceive === undefined ? false : !agreeReceive}
                handleChange={handleDisagreeReceive}
              />
            </Box>
            <Box sx={{display: 'flex', ml: '34px', mt: '14px'}}>
              <AgreeTermsCheckboxForm
                labelText={MY_SETTING.TERMS.AGREE.SMS}
                checked={agreeSms === undefined ? false : agreeSms}
                handleChange={handleAgreeSms}
              />
              <AgreeTermsCheckboxForm
                labelText={MY_SETTING.TERMS.AGREE.EMAIL}
                checked={agreeEmail === undefined ? false : agreeEmail}
                handleChange={handleAgreeEmail}
              />
            </Box>
          </Box>

          {/* 개인정보 수집/이용 동의시 최소 1개 이상 선택 필수 */}
          <Typography variant={'b2_14_r_1l'} sx={{color: '#888888'}}>
            {MY_SETTING.TERMS.AGREE.REFER}
          </Typography>
        </Box>

        {/* 확인 버튼 */}
        <Box sx={{m: '20px'}}>
          <Button
            tabIndex={1}
            disableFocusRipple
            fullWidth
            variant="contained"
            onClick={handleMarketing}
            sx={{
              height: '52px',
              fontStyle: 'normal',
              fontWeight: 700,
              fontSize: '16px',
              lineHeight: '28px',
              textAlign: 'center',
              color: '#FFFFFF',
              '&.Mui-disabled': {
                color: '#B3B3B3',
                backgroundColor: '#E6E6E6',
              },
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: 'primary.main',
              },
              ...clickBorderNone
            }}
          >
            {'확인'}
          </Button>
        </Box>

      </Container>
    </MainDialogLayout>
  )
}
