import {Box, Checkbox, FormControlLabel, FormGroup, Typography} from "@mui/material";
import PropTypes from "prop-types";
import {ICON_COLOR, ICON_SIZE, SvgCheckIcon} from "../../../../../constant/icons/icons";
import {clickBorderNone} from "../../../../../constant/common/Common";
import {handleEnterPress} from "../../../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../../../constant/common/AltString";

AgreeTermsCheckboxForm.propTypes = {
  labelText: PropTypes.string.isRequired,
  checked: PropTypes.any.isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default function AgreeTermsCheckboxForm({labelText, checked, handleChange}) {
  return (
    <FormGroup sx={{mr: '20px', mb: '23px'}}>
      <Box
        tabIndex={1}
        sx={{display: 'flex', ...clickBorderNone}}
        onKeyDown={(e) => handleEnterPress(e, handleChange)}
      >
        <FormControlLabel
          control={
            <Checkbox
              onChange={handleChange}
              checked={checked}
              icon={<SvgCheckIcon alt={ALT_STRING.COMMON.ICON_CHECK} color={ICON_COLOR.GREY} size={ICON_SIZE.PX16}/>}
              checkedIcon={<SvgCheckIcon alt={ALT_STRING.COMMON.ICON_CHECK} color={ICON_COLOR.PRIMARY} size={ICON_SIZE.PX16}/>}
              sx={{
                width: '16px',
                height: '16px',
                '&.MuiCheckbox-root svg': {
                  width: '16px',
                  height: '16px',
                }
              }}
            />
          }
          label={
            <Typography variant={'b2_14_r_1l'} sx={{ml: '4px', color: '#222222'}}>
              {labelText}
            </Typography>
          }
          sx={{alignItems: 'center', m: 0}}
        />
      </Box>
    </FormGroup>
  )
}