import PropTypes from "prop-types";
import {Box, Button, Container, Divider, Fade, Skeleton, Stack, Typography} from "@mui/material";
import {MY_SETTING} from "../../../../constant/my/Setting";
import * as React from "react";
import {useEffect} from "react";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {useRecoilState} from "recoil";
import {
  settingIsChangedOneOnOneInquiriesSelector,
  settingShowOneOnOneInquiriesDetailSelector,
  settingShowRegistOneOnOneInquirySelector
} from "../../../../recoil/selector/my/settingSelector";
import {getOneOnOneInquiriesMutate} from "../../../../api/mySettingApi";
import LoadingScreen from "../../../../components/common/LoadingScreen";
import OneOnOneInquiriesItem from "../../../../components/my/setting/OneOnOneInquiriesItem";
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../layouts/my/MyHeaderDialogLayout";
import usePagination from "../../../../hooks/usePagination";
import {PAGINATION} from "../../../../constant/common/Pagination";
import {getRandom} from "../../../../utils/formatNumber";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../../../constant/common/Common";

OneOnOneInquiriesSection.propTypes = {
  onBack: PropTypes.func,
};

export default function OneOnOneInquiriesSection({open, onBack}) {
  const [showOneOnOneInquiriesDetail, setShowOneOnOneInquiriesDetail] = useRecoilState(settingShowOneOnOneInquiriesDetailSelector)
  const [showRegistOneOnOneInquiry, setShowRegistOneOnOneInquiry] = useRecoilState(settingShowRegistOneOnOneInquirySelector)
  const [isChangedOneOnOneInquiries, setIsChangedOneOnOneInquiries] = useRecoilState(settingIsChangedOneOnOneInquiriesSelector)

  const {
    param,
    items,
    setItems,
    setTarget,
    pageOffset,
    hasNextOffset,
    initPagination,
    isLoading,
    getPaginationItems,
  } = usePagination(getOneOnOneInquiriesMutate)

  useEffect(() => {
    if (open) {
      getPaginationItems()
    } else {
      setShowOneOnOneInquiriesDetail(false)
    }
  }, [open])

  useEffect(() => {
    if (isChangedOneOnOneInquiries) {
      getPaginationItems()
      setIsChangedOneOnOneInquiries(false)
    }
  }, [isChangedOneOnOneInquiries])

  const handleClick = (item) => {
    setShowOneOnOneInquiriesDetail(item?.key)
  }

  const handleClickBottomButton = () => {
    setShowRegistOneOnOneInquiry(true)
  }

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <MyHeaderDialogLayout
        title={MY_SETTING.ONE_ON_ONE_INQUIRIES.HEADER_TITLE}
        onBack={onBack}
      >
        <Container disableGutters maxWidth={'xs'}
                   sx={{position: 'relative', height: '100%', px: '20px', mt: '10px', pb: '92px'}}>
          {isLoading && <LoadingScreen dialog/>}
          {
            items === PAGINATION.INIT_DATA
              ? <OneOnOneInquiriesSkeleton/>
              : items?.length ? (
                <>
                  <OneOnOneInquiriesItems items={items} onClick={handleClick}/>
                  {hasNextOffset.current && <Box ref={setTarget}/>}
                </>
              ) : <OneOnOneInquiriesNotFound/>
          }

          <Box
            sx={{
              p: '20px',
              position: 'fixed',
              zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
              bgcolor: 'white',
              bottom: 0,
              left: 0,
              right: 0,
            }}
          >
            <Container maxWidth={'xs'} disableGutters>
              <Button
                tabIndex={1}
                disableFocusRipple
                fullWidth variant="contained"
                onClick={handleClickBottomButton}
                onKeyDown={(e) => handleEnterPress(e, handleClickBottomButton)}
                sx={{
                  height: '52px',
                  fontSize: '16px',
                  fontWeight: '700',
                  lineHeight: '24px',
                  backgroundColor: 'primary.main',
                  '&:hover': {
                    boxShadow: 'none',
                    backgroundColor: 'primary.main',
                  },
                  ...clickBorderNone
                }}
              >
                1:1 문의하기
              </Button>
            </Container>
          </Box>
        </Container>
      </MyHeaderDialogLayout>
    </MainDialogLayout>
  )
}

function OneOnOneInquiriesSkeleton() {
  return (
    <>
      {[...Array(20)].map((item, index) =>
        <React.Fragment key={index}>
          <SkeletonItem key={index}/>
          <Divider sx={{borderColor: '#F5F5F5'}}/>
        </React.Fragment>
      )}
    </>
  );
}

function OneOnOneInquiriesItems({items, onClick}) {
  return (
    <>
      {
        items?.map((item, index) =>
          <React.Fragment key={index}>
            <OneOnOneInquiriesItem key={index} item={{...item}} onClick={() => onClick(item)}/>
            <Divider sx={{borderColor: '#F5F5F5'}}/>
          </React.Fragment>
        )
      }
    </>
  )
}

function SkeletonItem() {
  return (
    <Stack sx={{flexDirection: 'row', height: '48px', alignItems: 'center'}}>
      <Box sx={{
        height: '24px',
        minWidth: 'fit-content',
        alignItems: 'center',
        borderRadius: '20px',
        mr: '8px'
      }}>
        <Skeleton variant="rounded" sx={{borderRadius: '8px', width: '70px', height: '24px'}}/>
      </Box>
      <Skeleton variant="rounded" sx={{borderRadius: '8px', width: `calc(80% - ${getRandom(60)}%)`, height: '24px'}}/>
    </Stack>
  );
}

function OneOnOneInquiriesNotFound() {
  return (
    <Box
      sx={{
        height: 'calc(100vh - 180px)',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Stack alignItems="center">
        <SvgCommonIcons type={ICON_TYPE.COMMON_NO}/>
        <Typography
          sx={{
            mt: '30px',
            textAlign: 'center',
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '18px',
            lineHeight: '24px',
            color: '#222222',
            whiteSpace: 'pre-wrap',
          }}
        >
          1:1 문의 내역이 없어요.
        </Typography>
      </Stack>
    </Box>
  )
}