import PropTypes from "prop-types";
import {Fade} from "@mui/material";
import {MY_SETTING} from "../../../../constant/my/Setting";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../layouts/my/MyHeaderDialogLayout";
import IFrameDialog from "../../../../components/common/IFrameDialog";
import React, {useState} from "react";

LicenseSection.propTypes = {
  open: PropTypes.bool,
  onBack: PropTypes.func,
};

export default function LicenseSection({open, onBack}) {
  const [showDialog, setShowDialog] = useState(false)
  const [dataUrl, setDataUrl] = useState('')

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <MyHeaderDialogLayout
        title={MY_SETTING.LICENSE.HEADER_TITLE}
        onBack={onBack}
      >
        {dataUrl && <IFrameDialog open={showDialog} title={MY_SETTING.TERMS.PERSONAL.HEADER_TITLE} url={dataUrl}
                                  onBack={() => setShowDialog(false)}/>}
      </MyHeaderDialogLayout>
    </MainDialogLayout>
  )
}
