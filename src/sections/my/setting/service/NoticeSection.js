import {Box, Container, Divider, Fade, Skeleton, Typography} from "@mui/material";
import {MY_SETTING} from "../../../../constant/my/Setting";
import * as React from "react";
import {useEffect} from "react";
import NoticeItem from "../../../../components/my/setting/NoticeItem";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {useRecoilState} from "recoil";
import {settingShowNoticeDetailSelector} from "../../../../recoil/selector/my/settingSelector";
import {getNoticeListMutate} from "../../../../api/mySettingApi";
import LoadingScreen from "../../../../components/common/LoadingScreen";
import usePagination from "../../../../hooks/usePagination";
import {PAGINATION} from "../../../../constant/common/Pagination";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../layouts/my/MyHeaderDialogLayout";

export default function NoticeSection({open, onBack}) {
  const [showNoticeDetail, setShowNoticeDetail] = useRecoilState(settingShowNoticeDetailSelector)

  const {
    param,
    items,
    setItems,
    setTarget,
    pageOffset,
    hasNextOffset,
    initPagination,
    isLoading,
    getPaginationItems,
  } = usePagination(getNoticeListMutate)

  useEffect(() => {
    if (open) {
      getPaginationItems()
    } else {
      setShowNoticeDetail(null)
    }
  }, [open])

  const handleClick = (item) => {
    setShowNoticeDetail(item?.noticePostKey)
  }

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <MyHeaderDialogLayout
        title={MY_SETTING.NOTICE.HEADER_TITLE}
        onBack={onBack}
      >
        <Container disableGutters maxWidth={'xs'} sx={{position: 'relative', pt: '20px', px: '20px'}}>
          {isLoading && <LoadingScreen dialog/>}
          {
            items === PAGINATION.INIT_DATA
              ? <NoticeSkeleton/>
              : items?.length ? (
                <>
                  <NoticeItems items={items} onClick={handleClick}/>
                  {hasNextOffset.current && <Box ref={setTarget}/>}
                </>
              ) : <NoticeNotFound/>
          }
        </Container>
      </MyHeaderDialogLayout>
    </MainDialogLayout>
  )
}

function NoticeItems({items, onClick}) {
  return (
    <>
      {
        items?.map((item, index) =>
          <React.Fragment key={index}>
            <NoticeItem item={item} onClick={() => onClick(item)}/>
            <Divider/>
          </React.Fragment>
        )
      }
    </>
  )
}

function NoticeSkeleton() {
  return (
    <>
      {[...Array(10)].map((item, index) =>
        <React.Fragment key={index}>
          <Skeleton key={index} variant="rectangular" width="100%"
                    sx={{height: '76px', mt: '10px', mb: '12px', borderRadius: '10px'}}/>
          <Divider/>
        </React.Fragment>
      )}
    </>
  );
}

function NoticeNotFound() {
  return (
    <Box
      sx={{
        height: 'calc(100vh - 220px)',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <Typography align={'center'} sx={{fontSize: '16px', fontWeight: 500, color: '#666666', lineHeight: '24px'}}>
        {MY_SETTING.NOT_FOUND_LIST}
      </Typography>
    </Box>
  )
}