import PropTypes from "prop-types";
import {Box, Container, Divider, Fade, Skeleton, Typography} from "@mui/material";
import {MY_SETTING} from "../../../../constant/my/Setting";
import * as React from "react";
import {useEffect, useState} from "react";
import FAQItem from "../../../../components/my/setting/FAQItem";
import {FAQ_CATEGORY, FAQ_CATEGORY_ITEMS} from "../../../../constant/my/mock/FAQ";
import WrapGroupButton from "../../../../components/my/setting/WrapGroupButton";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {useRecoilState} from "recoil";
import {settingShowFAQDetailSelector} from "../../../../recoil/selector/my/settingSelector";
import {getFaqListMutate} from "../../../../api/mySettingApi";
import LoadingScreen from "../../../../components/common/LoadingScreen";
import usePagination from "../../../../hooks/usePagination";
import {PAGINATION} from "../../../../constant/common/Pagination";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../layouts/my/MyHeaderDialogLayout";

FAQSection.propTypes = {
  open: PropTypes.bool,
  onBack: PropTypes.func,
};

export default function FAQSection({open, onBack}) {
  const [categoryItems, setCategoryItems] = useState(FAQ_CATEGORY.ALL)
  const [showFAQDetail, setShowFAQDetail] = useRecoilState(settingShowFAQDetailSelector)

  const {
    param,
    items,
    setItems,
    setTarget,
    pageOffset,
    hasNextOffset,
    initPagination,
    isLoading,
    getPaginationItems,
  } = usePagination(getFaqListMutate)

  param.current = {
    category: categoryItems?.category
  }

  useEffect(() => {
    if (!open) {
      setShowFAQDetail(null)
    }
  }, [open])

  useEffect(() => {
    getPaginationItems()
  }, [categoryItems])

  const handleClick = (item) => {
    setShowFAQDetail(item?.key)
  }

  const handleCategoryClick = (item) => {
    if (categoryItems === item) return
    setCategoryItems(item)
  }

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      TransitionComponent={Fade}
      sx={{
        zIndex: Z_INDEX.DIALOG,
        '& .MuiDialog-paper': {mx: 0}
      }}
    >
      <MyHeaderDialogLayout
        title={MY_SETTING.FAQ.HEADER_TITLE}
        onBack={onBack}
      >
        <Container disableGutters maxWidth={'xs'} sx={{position: 'relative', pt: '20px', px: '20px'}}>
          <WrapGroupButton items={FAQ_CATEGORY_ITEMS} selected={categoryItems?.category}
                           onClick={(item) => handleCategoryClick(item)}/>
          <Box sx={{mt: '10px'}}>
            {isLoading && <LoadingScreen dialog/>}
            {
              items === PAGINATION.INIT_DATA
                ? <FAQSkeleton/>
                : items?.length ? (
                  <>
                    <FAQItems items={items} onClick={handleClick}/>
                    {hasNextOffset.current && <Box ref={setTarget}/>}
                  </>
                ) : <FAQNotFound/>
            }
          </Box>
        </Container>
      </MyHeaderDialogLayout>
    </MainDialogLayout>
  )
}

function FAQItems({items, onClick}) {
  return (
    <>
      {
        items?.map((item, index) =>
          <React.Fragment key={index}>
            <FAQItem item={item} onClick={() => onClick(item)}/>
          </React.Fragment>
        )
      }
    </>
  )
}

function FAQSkeleton() {
  return (
    <>
      {[...Array(10)].map((item, index) =>
        <React.Fragment key={index}>
          <Skeleton variant="rectangular" width="100%"
                    sx={{height: '76px', mt: '10px', mb: '12px', borderRadius: '10px'}}/>
          <Divider/>
        </React.Fragment>
      )}
    </>
  );
}

function FAQNotFound() {
  return (
    <Box
      sx={{
        height: 'calc(100vh - 220px)',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <Typography align={'center'} sx={{fontSize: '16px', fontWeight: 500, color: '#666666', lineHeight: '24px'}}>
        {MY_SETTING.NOT_FOUND_LIST}
      </Typography>
    </Box>
  )
}