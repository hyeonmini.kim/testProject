import PropTypes from "prop-types";
import {Box, Button, Container, Fade, Typography} from "@mui/material";
import {MY_SETTING} from "../../../../constant/my/Setting";
import {useEffect, useState} from "react";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import useChannel from "../../../../hooks/useChannel";
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../layouts/my/MyHeaderDialogLayout";
import {isNative} from "../../../../utils/envUtils";
import {WEB_VERSION} from "../../../../config";
import {ALT_STRING} from "../../../../constant/common/AltString";

VersionSection.propTypes = {
  open: PropTypes.bool,
  onBack: PropTypes.func,
};

export default function VersionSection({open, onBack}) {
  const [version, setVersion] = useState('')
  const [newVersion, setNewVersion] = useState('')
  const [hasUpdate, setHasUpdate] = useState(false)
  const {getNativeEnv, openNativeAppUpdate} = useChannel()

  useEffect(() => {
    if (isNative()) {
      const env = getNativeEnv()
      if (env) {
        setVersion(env?.version)
        setNewVersion(env?.newVersion)
        if (env?.version < env?.newVersion) {
          setHasUpdate(true)
        }
      }
    }
  }, [])

  const doUpdate = () => {
    openNativeAppUpdate()
  }

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <MyHeaderDialogLayout
        title={MY_SETTING.VERSION.HEADER_TITLE}
        onBack={onBack}
      >
        <Container disableGutters maxWidth={'xs'} sx={{position: 'relative', pt: '20px', px: '20px'}}>
          <Box sx={{
            height: 'calc(100vh - 220px)',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center'
          }}>
            <SvgCommonIcons alt={ALT_STRING.SETTING.DONOTS_LOGO} type={ICON_TYPE.DONOTS_ICON} sx={{width: '80px', height: '80px', mb: '10px'}}/>
            {isNative() && (
              <Typography align={'center'}
                          sx={{mt: '10px', fontSize: '16px', fontWeight: 500, color: '#000000', lineHeight: '16px'}}>
                {hasUpdate ? MY_SETTING.VERSION.UPDATE_MESSAGE : MY_SETTING.VERSION.CURRENT_MESSAGE}
              </Typography>
            )}

            <Typography
              align={'center'}
              sx={{
                mt: '10px',
                fontSize: '14px',
                fontWeight: 400,
                color: '#888888',
                lineHeight: '14px'
              }}>
              {
                isNative()
                  ? MY_SETTING.VERSION.CURRENT_VERSION(version, WEB_VERSION)
                  : MY_SETTING.VERSION.CURRENT_WEB_VERSION(WEB_VERSION)
              }
            </Typography>
            {hasUpdate &&
              <Button
                variant={'contained'}
                onClick={() =>
                  doUpdate()
                }
                sx={{
                  px: 0,
                  mt: '30px',
                  height: '42px',
                  lineHeight: '26px',
                  color: '#666666',
                  borderRadius: '99px',
                  backgroundColor: '#F5F5F5',
                  '&:hover': {
                    boxShadow: 'none',
                    backgroundColor: '#F5F5F5',
                  },
                }}
              >
                <Typography noWrap sx={{mx: '20px', fontSize: '14px', fontWeight: 500, lineHeight: '22px'}}>
                  {MY_SETTING.VERSION.UPDATE_TITLE}
                </Typography>
              </Button>
            }
          </Box>
        </Container>
      </MyHeaderDialogLayout>
    </MainDialogLayout>
  )
}