import PropTypes from "prop-types";
import {Container, Divider, Fade} from "@mui/material";
import {MY_SETTING} from "../../../../constant/my/Setting";
import SettingItem from "../../../../components/my/setting/SettingItem";
import React, {useEffect, useState} from "react";
import {useRecoilState, useRecoilValue} from "recoil";
import {
  settingAgreeTermsDetailSelector,
  settingShowAgreeTermsSelector,
  settingShowPersonalTermsSelector,
  settingShowServiceTermsSelector
} from "../../../../recoil/selector/my/settingSelector";
import AgreeTerms from "./terms/AgreeTerms";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {getAgreeMarketingMutate, getTermsOfUseByTitleMutate} from "../../../../api/mySettingApi";
import {memberInformationSelector} from "../../../../recoil/selector/auth/userSelector";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../layouts/my/MyHeaderDialogLayout";
import IFrameDialog from "../../../../components/common/IFrameDialog";
import useNativeBackKey from "../../../../hooks/useNativeBackKey";
import {useRouter} from "next/router";
import {ALT_STRING} from "../../../../constant/common/AltString";

TermsSection.propTypes = {
  onBack: PropTypes.func,
};

export default function TermsSection({open, onBack}) {
  const [showServiceTerms, setShowServiceTerms] = useRecoilState(settingShowServiceTermsSelector)
  const [showPersonalTerms, setShowPersonalTerms] = useRecoilState(settingShowPersonalTermsSelector)
  const [showAgreeTerms, setShowAgreeTerms] = useRecoilState(settingShowAgreeTermsSelector)

  const [agreeTermsDetail, setAgreeTermsDetail] = useRecoilState(settingAgreeTermsDetailSelector)

  const [serviceContents, setServiceContents] = useState('')
  const [personalContents, setPersonalContents] = useState('')
  const [agreeContents, setAgreeContents] = useState('')

  const {mutate: termsOfUseByTitle} = getTermsOfUseByTitleMutate()
  const {mutate: agreeMarketing} = getAgreeMarketingMutate()

  const router = useRouter()

  useEffect(() => {
    if (!open) {
      setShowServiceTerms(false)
      setShowPersonalTerms(false)
      setShowAgreeTerms(false)
    }
  }, [open]);

  useEffect(() => {
    if (agreeContents !== '') {
      agreeMarketing(null, {
        onSuccess: (result) => {
          setAgreeTermsDetail(result?.databody)
          setShowAgreeTerms(true)
        }
      })
    }
  }, [agreeContents])

  const handleServiceTerms = () => {
    termsOfUseByTitle({
      title: encodeURIComponent(MY_SETTING.TERMS.SERVICE.VALUE)
    }, {
      onSuccess: (result) => {
        setServiceContents(result?.bodyHtmlFileUrl)
        setShowServiceTerms(true)
      }
    })
  }

  const handlePersonalTerms = () => {
    termsOfUseByTitle({
      title: encodeURIComponent(MY_SETTING.TERMS.PERSONAL.VALUE)
    }, {
      onSuccess: (result) => {
        setPersonalContents(result?.bodyHtmlFileUrl)
        setShowPersonalTerms(true)
      }
    })
  }

  const handleAgreeTerms = () => {
    termsOfUseByTitle({
      title: encodeURIComponent(MY_SETTING.TERMS.AGREE.VALUE)
    }, {
      onSuccess: (result) => {
        setAgreeContents(result?.bodyHtmlFileUrl)
        setShowAgreeTerms(true)
      }
    })
  }

  const handleOnBackAgreeTerms = () => {
    setShowAgreeTerms(false)
    setAgreeContents('')
  }

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      if (showServiceTerms) {
        setShowServiceTerms(false)
      } else if (showPersonalTerms) {
        setShowPersonalTerms(false)
      } else if (showAgreeTerms) {
        setShowAgreeTerms(false)
      } else {
        router.back()
      }
    }
  }, [handleBackKey])

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <MyHeaderDialogLayout
        title={MY_SETTING.TERMS.HEADER_TITLE}
        onBack={onBack}
      >
        <Container disableGutters maxWidth={'xs'} sx={{position: 'relative', pt: '20px', px: '20px'}}>
          <SettingItem title={MY_SETTING.TERMS.SERVICE.MENU_TITLE} onClick={handleServiceTerms}/>
          <Divider sx={{borderColor: '#F5F5F5'}}/>
          <SettingItem title={MY_SETTING.TERMS.PERSONAL.MENU_TITLE} onClick={handlePersonalTerms}/>
          <Divider sx={{borderColor: '#F5F5F5'}}/>
          <SettingItem title={MY_SETTING.TERMS.AGREE.MENU_TITLE} onClick={handleAgreeTerms}/>
          <Divider sx={{borderColor: '#F5F5F5'}}/>
        </Container>
      </MyHeaderDialogLayout>

      {showServiceTerms &&
        <IFrameDialog title={ALT_STRING.SETTING.AGREE.SERVICE} open={showServiceTerms} url={serviceContents} onBack={() => setShowServiceTerms(false)}/>}
      {showPersonalTerms &&
        <IFrameDialog title={ALT_STRING.SETTING.AGREE.PERSONAL} open={showPersonalTerms} url={personalContents} onBack={() => setShowPersonalTerms(false)}/>}
      {showAgreeTerms &&
        <AgreeTerms open={showAgreeTerms} contents={agreeContents} agreeChecked={agreeTermsDetail} onBack={handleOnBackAgreeTerms}/>}

    </MainDialogLayout>
  )
}
