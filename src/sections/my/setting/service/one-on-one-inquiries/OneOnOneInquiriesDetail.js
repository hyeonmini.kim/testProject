import PropTypes from "prop-types";
import {Box, Button, Container, Divider, Fade, Skeleton, Typography} from "@mui/material";
import {MY_SETTING, QNA_CATEGORY_TEXT} from "../../../../../constant/my/Setting";
import TextMaxLine from "../../../../../components/text-max-line";
import * as React from "react";
import {Z_INDEX} from "../../../../../constant/common/ZIndex";
import {getYYMMDDHHMMFromDate, nvlString} from "../../../../../utils/formatString";
import {useRecoilState, useSetRecoilState} from "recoil";
import {
  settingIsChangedOneOnOneInquiriesSelector,
  settingShowOneOnOneInquiriesDetailSelector
} from "../../../../../recoil/selector/my/settingSelector";
import {clickBorderNone, COMMON_DIALOG_TYPE} from "../../../../../constant/common/Common";
import {MY_BTN_TEXT} from "../../../../../constant/my/MyConstants";
import {dialogSelector} from "../../../../../recoil/selector/common/dialogSelector";
import SingleImagePreview from "../../../../../components/my/image/SingleImagePreview";
import {MainDialogLayout} from "../../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../../layouts/my/MyHeaderDialogLayout";
import {delOneOnOneInquiryMutate, getOneOnOneInquiryDetail} from "../../../../../api/mySettingApi";
import LoadingScreen from "../../../../../components/common/LoadingScreen";
import {ALT_STRING} from "../../../../../constant/common/AltString";

OneOnOneInquiriesDetail.propTypes = {
  onBack: PropTypes.func,
};

export default function OneOnOneInquiriesDetail({open, onBack}) {
  const [showOneOnOneInquiriesDetail, setShowOneOnOneInquiriesDetail] = useRecoilState(settingShowOneOnOneInquiriesDetailSelector)
  const [isChangedOneOnOneInquiries, setIsChangedOneOnOneInquiries] = useRecoilState(settingIsChangedOneOnOneInquiriesSelector)
  const setDialogMessage = useSetRecoilState(dialogSelector)

  const {data: item, isLoading: isLoadingGetOneOnOneInquiryDetail} = getOneOnOneInquiryDetail({key: open})
  const {mutate: mutateDelOneOnOneInquiry, isLoading: isLoadingDelOneOnOneInquiry} = delOneOnOneInquiryMutate()

  const handleClickBottomButton = () => {
    setShowOneOnOneInquiriesDetail(false)
  };

  const handleClickRemove = () => {
    setDialogMessage({
      type: COMMON_DIALOG_TYPE.TWO_BUTTON,
      message: MY_SETTING.POPUP_COMMENT.REMOVE_INQUIRY_ITEM,
      leftButtonProps: {text: MY_BTN_TEXT.CANCEL},
      rightButtonProps: {text: MY_BTN_TEXT.CONFIRM, onClick: () => handleClickRemoveInquiryItem(open)},
    })
  };

  const handleClickRemoveInquiryItem = (open) => {
    mutateDelOneOnOneInquiry({
      key: open
    }, {
      onSuccess: () => {
        setIsChangedOneOnOneInquiries(true)
        setShowOneOnOneInquiriesDetail(false)
      }
    })
  };

  return (
    <MainDialogLayout
      fullScreen
      open={!!open}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <MyHeaderDialogLayout
        title={MY_SETTING.ONE_ON_ONE_INQUIRIES.HEADER_TITLE}
        onBack={onBack}
        showDelete={item?.status === "ANSWER_COMPLETED" ? false : true}
        onRemove={handleClickRemove}
      >
        <Container disableGutters maxWidth={'xs'} sx={{pt: '10px', px: '20px', mb: '112px'}}>
          {(isLoadingGetOneOnOneInquiryDetail || isLoadingDelOneOnOneInquiry) && <LoadingScreen dialog/>}
          {
            item ? (
              <>
                <OneOnOneInquiryTitle item={item}/>
                <Divider sx={{borderColor: '#222222'}}/>
                <OneOnOneInquiryQuestionContents item={item}/>
                <Divider sx={{borderColor: '#F5F5F5'}}/>
                {item?.status === "ANSWER_COMPLETED" && (
                  <>
                    <OneOnOneInquiryAnswerContents item={item}/>
                    <Divider sx={{borderColor: '#F5F5F5'}}/>
                  </>)}
              </>
            ) : (
              <OneOnOneInquirySkeleton/>
            )
          }

          <Box
            sx={{
              p: '20px',
              position: 'fixed',
              zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
              bgcolor: 'white',
              bottom: 0,
              left: 0,
              right: 0,
            }}
          >
            <Container maxWidth={'xs'} disableGutters>
              <Button
                tabIndex={1}
                disableFocusRipple
                fullWidth variant="contained"
                onClick={handleClickBottomButton}
                sx={{
                  height: '52px',
                  fontSize: '16px',
                  fontWeight: '700',
                  lineHeight: '24px',
                  backgroundColor: 'primary.main',
                  '&:hover': {
                    boxShadow: 'none',
                    backgroundColor: 'primary.main',
                  },
                  ...clickBorderNone
                }}
              >
                확인
              </Button>
            </Container>
          </Box>
        </Container>
      </MyHeaderDialogLayout>
    </MainDialogLayout>
  )
}

function OneOnOneInquiryTitle({item}) {
  return (
    <>
      <TextMaxLine line={1} sx={{
        mt: '8px',
        mb: '12px',
        fontSize: '16px',
        fontWeight: 400,
        lineHeight: '24px',
        color: '#222222'
      }}>
        {QNA_CATEGORY_TEXT(item?.category)} {nvlString(item?.inquiryTitle)}
      </TextMaxLine>
    </>
  )
}

function OneOnOneInquiryQuestionContents({item}) {
  return (
    <Box sx={{mt: '20px'}}>
      <Typography sx={{
        whiteSpace: 'pre-line',
        fontSize: '16px',
        fontWeight: 400,
        lineHeight: '24px',
        color: '#222222'
      }}>
        {nvlString(item?.inquiryBody)}
      </Typography>
      {item?.attachmentFileUrl && (
        <Box sx={{py: '10px', width: '80px'}}>
          <Box sx={{position: 'relative'}}>
            <SingleImagePreview alt={ALT_STRING.SETTING.CONTENT_DETAIL(item?.inquiryTitle)} file={item?.attachmentFileUrl}/>
          </Box>
        </Box>)}
      <Typography
        sx={{mt: '10px', mb: '20px', fontSize: '14px', fontWeight: 400, lineHeight: '20px', color: '#CCCCCC'}}>
        {getYYMMDDHHMMFromDate(item?.inquiredDatetime)}
      </Typography>
    </Box>
  )
}

function OneOnOneInquiryAnswerContents({item}) {
  return (
    <Box sx={{mt: '20px'}}>
      <Box sx={{
        height: '24px',
        width: 'fit-content',
        padding: '4px 10px 6px 10px',
        alignItems: 'center',
        backgroundColor: item?.status === "ANSWER_COMPLETED" ? '#ECA548' : '#CCCCCC',
        borderRadius: '20px',
        mb: '10px'
      }}>
        <Typography fontWeight="500" fontSize="14px"
                    sx={{lineHeight: '14px', color: 'white'}}>
          {item?.status === "ANSWER_COMPLETED" ? '답변완료' : '미답변'}
        </Typography>
      </Box>

      <Typography sx={{
        whiteSpace: 'pre-line',
        fontSize: '16px',
        fontWeight: 400,
        lineHeight: '24px',
        color: '#222222'
      }}>
        {nvlString(item?.answer)}
      </Typography>
      <Typography
        sx={{mt: '10px', mb: '20px', fontSize: '14px', fontWeight: 400, lineHeight: '20px', color: '#CCCCCC'}}>
        {getYYMMDDHHMMFromDate(item?.answerCompletedDatetime)}
      </Typography>
    </Box>
  )
}

function OneOnOneInquirySkeleton() {
  return (
    <>
      <Skeleton variant="rectangular" sx={{my: '12px', height: '24px', borderRadius: '10px'}}/>
      <Divider sx={{borderColor: '#666666'}}/>
      <Skeleton variant="rectangular" sx={{mt: '20px', mb: '10px', height: '216px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mb: '20px', height: '14px', width: '104px', borderRadius: '10px'}}/>
      <Divider sx={{borderColor: '#F5F5F5'}}/>
      <Skeleton variant="rectangular"
                sx={{mt: '20px', mb: '10px', height: '24px', width: '71px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mb: '10px', height: '216px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mb: '20px', height: '14px', width: '104px', borderRadius: '10px'}}/>
    </>
  )
}