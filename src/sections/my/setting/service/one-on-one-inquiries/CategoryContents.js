import {Box, Divider, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import * as React from "react";
import {myInformationBottomDialogState} from "../../../../../recoil/atom/my/information/myBottomDialog";
import {myDataSelector} from "../../../../../recoil/selector/my/myDataSelector";
import {QNA_CATEGORY_ITEMS} from "../../../../../constant/my/Setting";
import {settingOneOnOneInquiryCategorySelector} from "../../../../../recoil/selector/my/setting/one-on-one-inquiry/oneOnOneInquirySelector";
import {clickBorderNone} from "../../../../../constant/common/Common";
import {handleEnterPress} from "../../../../../utils/onKeyDownUtils";

export default function CategoryContents() {
  const [category, setCategory] = useRecoilState(settingOneOnOneInquiryCategorySelector)
  const setIsOpen = useSetRecoilState(myInformationBottomDialogState);
  const [myData, setMyData] = useRecoilState(myDataSelector);

  const handleClick = (item) => {
    setCategory(item)
    setIsOpen(false)
  }

  return (
    <Box sx={{mt: '6px', textAlign: 'start'}}>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 500,
          fontSize: '16px',
          lineHeight: '24px',
          color: '#222222',
          paddingY: '20px'
        }}
      >
        문의유형
      </Typography>
      <Divider sx={{height: '1px', backgroundColor: '#E6E6E6'}}/>

      {QNA_CATEGORY_ITEMS?.map((item, index) => (
        <Typography
          tabIndex={1}
          key={index}
          value={item?.category}
          sx={{
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
            paddingY: '20px',
            ...clickBorderNone
          }}
          onClick={() => handleClick(item?.text)}
          onKeyDown={(e) => handleEnterPress(e, () => handleClick(item?.text))}
        >
          {item?.text}
        </Typography>))}
    </Box>
  );
}