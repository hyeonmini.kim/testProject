import PropTypes from "prop-types";
import {Box, Button, Container, Fade, Grid, Typography} from "@mui/material";
import {MY_SETTING, QNA_CATEGORY_CODE} from "../../../../../constant/my/Setting";
import * as React from "react";
import {useEffect, useState} from "react";
import {Z_INDEX} from "../../../../../constant/common/ZIndex";
import {useRecoilState, useSetRecoilState} from "recoil";
import {postOneOnOneInquiryMutate} from "../../../../../api/mySettingApi";
import LoadingScreen from "../../../../../components/common/LoadingScreen";
import {ICON_TYPE, SvgCommonIcons} from "../../../../../constant/icons/ImageIcons";
import {
  settingOneOnOneInquiryAttachmentFileSelector,
  settingOneOnOneInquiryBodySelector,
  settingOneOnOneInquiryCategorySelector,
  settingOneOnOneInquiryIsTrueSelector,
  settingOneOnOneInquiryTitleSelector
} from "../../../../../recoil/selector/my/setting/one-on-one-inquiry/oneOnOneInquirySelector";
import MyBottomDialog from "../../../self/my-information/MyBottomDialog";
import {myInformationBottomDialogState} from "../../../../../recoil/atom/my/information/myBottomDialog";
import CategoryContents from "./CategoryContents";
import {isNative} from "../../../../../utils/envUtils";
import SingleImage from "../../../../../components/my/image/SingleImage";
import {bytesToImageFile} from "../../../../../utils/fileUtils";
import {callOpenNativeGalleryChannel} from "../../../../../channels/photoChannel";
import {dialogSelector} from "../../../../../recoil/selector/common/dialogSelector";
import {clickBorderNone, COMMON_DIALOG_TYPE, IMAGE_COMPRESSION_OPTIONS} from "../../../../../constant/common/Common";
import {MY_BTN_TEXT} from "../../../../../constant/my/MyConstants";
import {MainDialogLayout} from "../../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../../layouts/my/MyHeaderDialogLayout";
import imageCompression from "browser-image-compression";
import {
  settingIsChangedOneOnOneInquiriesSelector,
  settingShowRegistOneOnOneInquirySelector
} from "../../../../../recoil/selector/my/settingSelector";
import {handleEnterPress} from "../../../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../../../constant/common/AltString";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";

RegistOneOnOneInquirySection.propTypes = {
  onBack: PropTypes.func,
};

export default function RegistOneOnOneInquirySection({open, onBack}) {
  const [showRegistOneOnOneInquiry, setShowRegistOneOnOneInquiry] = useRecoilState(settingShowRegistOneOnOneInquirySelector)
  const [isChangedOneOnOneInquiries, setIsChangedOneOnOneInquiries] = useRecoilState(settingIsChangedOneOnOneInquiriesSelector)
  const [category, setCategory] = useRecoilState(settingOneOnOneInquiryCategorySelector)
  const [title, setTitle] = useRecoilState(settingOneOnOneInquiryTitleSelector)
  const [body, setBody] = useRecoilState(settingOneOnOneInquiryBodySelector)
  const [attachmentFile, setAttachmentFile] = useRecoilState(settingOneOnOneInquiryAttachmentFileSelector)
  const [isOpen, setIsOpen] = useRecoilState(myInformationBottomDialogState);
  const [isTrue, setIsTrue] = useRecoilState(settingOneOnOneInquiryIsTrueSelector);
  const setDialogMessage = useSetRecoilState(dialogSelector)
  const [isCompress, setIsCompress] = useState(false)
  const [isFocusedTitle, setIsFocusedTitle] = useState(false)

  const {mutate: mutatePostOneOnOneInquiry, isLoading} = postOneOnOneInquiryMutate()

  useEffect(() => {
    return () => {
      setCategory('')
      setTitle('')
      setBody('')
      setAttachmentFile(null)
    }
  }, [])

  useEffect(() => {
    if (category !== '' && title !== '' && body !== '') {
      setIsTrue(false)
    } else {
      setIsTrue(true)
    }
  }, [category, title, body])

  const handleClick = () => {
    setIsOpen(true);
  }

  const handleTitle = (event) => {
    let value = event.target.value?.substring(0, 18);
    setTitle(value);
  };

  const handleQuestion = (event) => {
    let value = event.target.value?.substring(0, 200);
    setBody(value);
  };

  const handleClickBottomButton = () => {
    const formData = new FormData()
    if (attachmentFile) {
      formData.append('attachmentFile', attachmentFile)
    }
    formData.append('category', QNA_CATEGORY_CODE(category))
    formData.append('inquiryTitle', title)
    formData.append('inquiryBody', body)

    mutatePostOneOnOneInquiry(formData, {
      onSuccess: () => {
        setIsChangedOneOnOneInquiries(true)
        setShowRegistOneOnOneInquiry(false);
      }
    })
  };

  const setFileWithImage = async (image) => {
    if (isNative()) {
      image = bytesToImageFile(image)
    } else {
      setIsCompress(true)
      const resizingBlob = await imageCompression(image, IMAGE_COMPRESSION_OPTIONS);
      image = new File([resizingBlob], image.name, {type: image.type});
      setIsCompress(false)
    }
    const newImage = Object.assign(image, {
      preview: URL.createObjectURL(image),
    })
    setAttachmentFile(newImage)
  }

  const handleDrop = (images) => {
    setFileWithImage(images[0])
  }

  const handleGalleryChannel = () => {
    document.activeElement.blur()
    callOpenNativeGalleryChannel(0, 1, (images) => setFileWithImage(images[0]))
  }

  const handleRemoveImage = () => {
    setDialogMessage({
      type: COMMON_DIALOG_TYPE.TWO_BUTTON,
      message: MY_SETTING.POPUP_COMMENT.REMOVE_PICTURE_REGIST_INQUIRY,
      leftButtonProps: {text: MY_BTN_TEXT.CANCEL},
      rightButtonProps: {text: MY_BTN_TEXT.DO_DELETE, onClick: () => setAttachmentFile(null)},
    })
  }

  return (
    <MainDialogLayout
      fullScreen
      open={open}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <MyHeaderDialogLayout
        title={MY_SETTING.ONE_ON_ONE_INQUIRIES.HEADER_TITLE}
        onBack={onBack}
      >
        {isLoading && <LoadingScreen dialog/>}
        <Container maxWidth={'xs'} sx={{mt: '10px', px: '20px', mb: '112px'}}>
          <Grid item xs={12}>
            <Box display="grid">
              <TextFieldWrapper
                tabIndex={1}
                autoComplete='off'
                fullWidth
                value={category}
                placeholder={MY_SETTING.ONE_ON_ONE_INQUIRIES.CATEGORY_PLACEHOLDER}
                onClick={handleClick}
                onKeyDown={(e) => handleEnterPress(e, handleClick)}
                variant="standard"
                inputProps={{style: {padding: 0}, title: ALT_STRING.SETTING.ONE_ON_ONE.SELECT_INQUIRY}}
                InputProps={{
                  endAdornment: (
                    <SvgCommonIcons alt={ALT_STRING.SETTING.BTN_ARROW_DOWN} type={ICON_TYPE.ARROW_BOTTOM_24PX}/>
                  ),
                  style: {
                    fontStyle: 'normal',
                    fontWeight: 400,
                    fontSize: '16px',
                    lineHeight: '24px',
                    color: '#222222',
                  },
                  sx: {
                    height: '48px',
                    '& input': {fontSize: '16px', fontWeight: '500', lineHeight: '24px', color: '#222222'},
                    '& input::placeholder': {fontSize: '16px', fontWeight: '500', color: '#CCCCCC'},
                  },
                  readOnly: true,
                }}
                sx={{
                  '& input::placeholder': {
                    color: '#CCCCCC',
                    style: {
                      fontStyle: 'normal',
                      fontWeight: 500,
                      fontSize: '16px',
                      lineHeight: '24px',
                      color: '#CCCCCC',
                    },
                  },
                  '& .MuiInput-root:before': {
                    borderBottom: '1px solid #F5F5F5',
                  },
                  '& .MuiInput-root:hover:before': {
                    borderBottom: '1px solid #F5F5F5',
                  },
                  '& .MuiInput-root:hover:after': {
                    borderBottom: '1px solid #F5F5F5',
                  },
                  '& .MuiInput-root:hover.Mui-disabled:before': {
                    borderBottom: '1px solid #F5F5F5',
                  },
                  '& .MuiInput-root:after': {
                    borderBottom: '1px solid #F5F5F5',
                  },
                  ...clickBorderNone
                }}
              />
              <TextFieldWrapper
                autoComplete='off'
                placeholder={MY_SETTING.ONE_ON_ONE_INQUIRIES.TITLE_PLACEHOLDER}
                value={title}
                variant="standard"
                onChange={handleTitle}
                inputProps={{tabIndex: 1, style: {padding: 0}, title: ALT_STRING.SETTING.ONE_ON_ONE.INPUT_TITLE}}
                InputProps={{
                  sx: {
                    height: '48px',
                    '& input': {fontSize: '16px', fontWeight: '500', lineHeight: '24px', color: '#222222'},
                    '& input::placeholder': {fontSize: '16px', fontWeight: '500', color: '#CCCCCC'},
                  }
                }}
                sx={{
                  '& input::placeholder': {
                    color: '#CCCCCC',
                    style: {
                      fontStyle: 'normal',
                      fontWeight: 500,
                      fontSize: '16px',
                      lineHeight: '24px',
                      color: '#CCCCCC',
                    },
                  },
                  '& .MuiInput-root:before': {
                    borderBottom: title === '' ? '1px solid #F5F5F5' : isTrue ? '1px solid #CCCCCC' : '1px solid #F5F5F5',
                  },
                  '& .MuiInput-root:hover:before': {
                    borderBottom: title === '' ? '1px solid #F5F5F5' : isTrue ? '1px solid #CCCCCC' : '1px solid #F5F5F5',
                  },
                  // '& .MuiInput-root:hover:after': {
                  //   borderBottom: title === '' ? '1px solid #F5F5F5' : isTrue ? '1px solid #CCCCCC' : '1px solid #F5F5F5',
                  // },
                  '& .MuiInput-root:hover.Mui-disabled:before': {
                    borderBottom: title === '' ? '1px solid #F5F5F5' : isTrue ? '1px solid #CCCCCC' : '1px solid #F5F5F5',
                  },
                  // '& .MuiInput-root:after': {
                  //   borderBottom: title === '' ? '1px solid #F5F5F5' : isTrue ? '1px solid #CCCCCC' : '1px solid #F5F5F5',
                  // },
                }}
              />
            </Box>
            <Box sx={{mt: '20px'}}>
              <Grid
                item xs={12}
              >
                <Box
                  rowGap={'5px'}
                  display="grid"
                >
                  <TextFieldWrapper
                    autoComplete='off'
                    placeholder={MY_SETTING.ONE_ON_ONE_INQUIRIES.QUESTION_PLACEHOLDER}
                    value={body}
                    multiline
                    minRows={9}
                    onChange={handleQuestion}
                    sx={{borderColor: 'red'}}
                    inputProps={{tabIndex: 1, style: {padding: 0}, title: ALT_STRING.SETTING.ONE_ON_ONE.INPUT_DETAILS}}
                    InputProps={{
                      sx: {
                        '& textarea': {
                          fontSize: '16px', fontWeight: '400', lineHeight: '24px', color: '#222222'
                        },
                        '& textarea::placeholder': {
                          fontSize: '16px', fontWeight: '500', lineHeight: '24px', color: '#CCCCCC'
                        }
                      }
                    }}
                    sx={{
                      "& .MuiOutlinedInput-root": {
                        padding: '10px',
                        '& fieldset': {
                          border: '1px solid #F5F5F5',
                        },
                        // border: '1px solid #F5F5F5'
                      },
                      '& input::placeholder': {
                        color: '#CCCCCC',
                        style: {
                          fontStyle: 'normal',
                          fontWeight: 500,
                          fontSize: '16px',
                          lineHeight: '24px',
                          color: '#CCCCCC',
                        },
                      },
                    }}
                  />
                </Box>
              </Grid>
            </Box>

            <Box sx={{mt: '20px'}}>
              <Typography
                sx={{fontSize: '14px', fontWeight: 400, lineHeight: '14px', color: '#222222'}}>
                첨부이미지 ({attachmentFile ? 1 : 0}/1)
              </Typography>

              <Box sx={{mt: '10px', width: '80px'}}>
                {isNative()
                  ? <SingleImage onClick={() => handleGalleryChannel()}
                                 onDelete={() => handleRemoveImage()}/>
                  : <SingleImage onDrop={(images) => handleDrop(images)}
                                 onDelete={() => handleRemoveImage()}/>
                }
              </Box>
            </Box>
          </Grid>

          <Box
            sx={{
              p: '20px',
              position: 'fixed',
              zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
              bgcolor: 'white',
              bottom: 0,
              left: 0,
              right: 0,
            }}
          >
            <Container maxWidth={'xs'} disableGutters>
              <Button
                tabIndex={1}
                disableFocusRipple
                fullWidth variant="contained"
                onClick={handleClickBottomButton}
                onKeyDown={(e) => handleEnterPress(e, handleClickBottomButton)}
                disabled={isTrue}
                sx={{
                  height: '52px',
                  fontSize: '16px',
                  fontWeight: '700',
                  lineHeight: '24px',
                  backgroundColor: 'primary.main',
                  '&.Mui-disabled': {
                    color: '#B3B3B3',
                    backgroundColor: '#E6E6E6',
                  },
                  '&.Mui-disabled:hover': {
                    backgroundColor: '#E6E6E6',
                  },
                  '&:hover': {
                    boxShadow: 'none',
                    backgroundColor: 'primary.main',
                  },
                  ...clickBorderNone
                }}
              >
                등록하기
              </Button>
            </Container>
          </Box>

          <MyBottomDialog>
            <CategoryContents/>
          </MyBottomDialog>
        </Container>
      </MyHeaderDialogLayout>
      {isCompress && <LoadingScreen dialog/>}
    </MainDialogLayout>
  )
}