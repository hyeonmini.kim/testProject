import PropTypes from "prop-types";
import {Box, Container, Divider, Fade, Skeleton, Typography} from "@mui/material";
import {MY_SETTING} from "../../../../../constant/my/Setting";
import TextMaxLine from "../../../../../components/text-max-line";
import * as React from "react";
import Image from "../../../../../components/image";
import {Z_INDEX} from "../../../../../constant/common/ZIndex";
import {getNoticeDetail} from "../../../../../api/mySettingApi";
import LoadingScreen from "../../../../../components/common/LoadingScreen";
import {getYYMMDDFromDate, nvlString} from "../../../../../utils/formatString";
import {MainDialogLayout} from "../../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../../layouts/my/MyHeaderDialogLayout";
import {ALT_STRING} from "../../../../../constant/common/AltString";

NoticeDetail.propTypes = {
  onBack: PropTypes.func,
};

export default function NoticeDetail({open, onBack}) {
  const {data: item, isLoading} = getNoticeDetail({
    noticePostKey: open
  })
  return (
    <MainDialogLayout
      fullScreen
      open={!!open}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <MyHeaderDialogLayout
        title={MY_SETTING.NOTICE.HEADER_TITLE}
        onBack={onBack}
      >
        <Container disableGutters maxWidth={'xs'} sx={{pt: '20px', px: '20px'}}>
          {isLoading && <LoadingScreen dialog/>}
          {
            item ? (
              <>
                <NoticeTitle item={item}/>
                <Divider sx={{borderColor: '#666666'}}/>
                <NoticeContents item={item}/>
              </>
            ) : (
              <NoticeSkeleton/>
            )
          }
        </Container>
      </MyHeaderDialogLayout>
    </MainDialogLayout>
  )
}

function NoticeTitle({item}) {
  return (
    <>
      <TextMaxLine line={2} sx={{fontSize: '18px', fontWeight: 500, lineHeight: '24px', color: '#000000'}}>
        {nvlString(item?.title)}
      </TextMaxLine>
      <Typography sx={{mt: '8px', mb: '12px', fontSize: '14px', fontWeight: 400, lineHeight: '20px', color: '#888888'}}>
        {getYYMMDDFromDate(item?.createdDate)}
      </Typography>
    </>
  )
}

function NoticeContents({item}) {
  return (
    <Box sx={{mt: '30px'}}>
      {item?.imgUrl && <Image alt={ALT_STRING.SETTING.CONTENT_DETAIL(item?.title)} src={item?.imgUrl} isLazy={false}/>}
      <Typography sx={{
        mt: '20px',
        whiteSpace: 'pre-line',
        fontSize: '14px',
        fontWeight: 400,
        lineHeight: '20px',
        color: '#666666'
      }}>
        {nvlString(item?.body)}
      </Typography>
    </Box>
  )
}

function NoticeSkeleton() {
  return (
    <>
      <Skeleton variant="rectangular" sx={{my: '8px', height: '40px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{my: '11px', height: '18px', width: '30%', borderRadius: '10px'}}/>
      <Divider sx={{borderColor: '#666666'}}/>
      <Skeleton variant="rectangular" sx={{mt: '30px', height: '235px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '22px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', width: '40%', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '22px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', width: '60%', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '22px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', width: '30%', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '22px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
      <Skeleton variant="rectangular" sx={{mt: '5px', height: '18px', borderRadius: '10px'}}/>
    </>
  )
}