import {Button, Container, Fade} from "@mui/material";
import {useRecoilState, useRecoilValue, useResetRecoilState, useSetRecoilState} from "recoil";
import ChildrenAllergyInputForm from "./input-form/ChildrenAllergyInputForm";
import ChildrenKeywordInputForm from "./input-form/ChildrenKeywordInputForm";
import {
  myOpenAddChildrenBtnMarginSelector,
  myOpenAddChildrenBtnPositionSelector,
  myOpenAddChildrenBtnTextSelector,
  myOpenAddChildrenPageSelector,
  myOpenAddChildrenSelector,
  myOpenChildrenSelector,
  myOpenResultAddSelector
} from "../../../../recoil/selector/my/popup/myPopupSelector";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {myDataSelector} from "../../../../recoil/selector/my/myDataSelector";
import {authBottomButtonState} from "../../../../recoil/atom/sign-up/auth";
import {putMyChildrenAllergyIngredientsMutate, putMyChildrenConcernsMutate} from "../../../../api/myApi";
import {
  childrenAllergyIngredientsListSelector,
  childrenAllergyIngredientsSelector,
  childrenConcernsListSelector,
  childrenConcernsSelector
} from "../../../../recoil/selector/my/childrenDataSelector";
import {useQueryClient} from "react-query";
import LoadingScreen from "../../../../components/common/LoadingScreen";
import * as React from "react";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../layouts/my/MyHeaderDialogLayout";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../../../constant/common/Common";

export default function ChildrenAddInformationDialog({userInfo, index}) {
  const [myData, setMyData] = useRecoilState(myDataSelector);
  const setOpenChildren = useSetRecoilState(myOpenChildrenSelector)
  const [openAddChildren, setOpenAddChildren] = useRecoilState(myOpenAddChildrenSelector)
  const setOpenResultAdd = useSetRecoilState(myOpenResultAddSelector)
  const isBottomBtnDisabled = useRecoilValue(authBottomButtonState)
  const [page, setPage] = useRecoilState(myOpenAddChildrenPageSelector)

  const [childrenAllergyIngredients, setChildrenAllergyIngredients] = useRecoilState(childrenAllergyIngredientsSelector)
  const resetChildrenAllergyIngredients = useResetRecoilState(childrenAllergyIngredientsSelector)
  const resetChildrenAllergyIngredientsList = useResetRecoilState(childrenAllergyIngredientsListSelector)
  const [childrenConcerns, setChildrenConcerns] = useRecoilState(childrenConcernsSelector)
  const resetChildrenConcerns = useResetRecoilState(childrenConcernsSelector)
  const resetChildrenConcernsList = useResetRecoilState(childrenConcernsListSelector)

  // const [allergyIngredients, setAllergyIngredients] = useState(userInfo?.babies?.length > 0 ? myData?.userInfo[index]?.allergyIngredients : [])
  // const [concerns, setConcerns] = useState(userInfo?.babies?.length > 0 ? userInfo?.babies[index]?.concerns : [])

  const [btnPosition, setBtnPosition] = useRecoilState(myOpenAddChildrenBtnPositionSelector)
  const [btnMargin, setBtnMargin] = useRecoilState(myOpenAddChildrenBtnMarginSelector)
  const [btnText, setBtnText] = useRecoilState(myOpenAddChildrenBtnTextSelector)

  const queryClient = useQueryClient()

  const {
    mutate: mutatePutMyChildrenAllergyIngredients,
    isLoading: isLodingPutMyChildrenAllergyIngredients
  } = putMyChildrenAllergyIngredientsMutate()
  const {mutate: mutatePutMyChildrenConcerns, isLoading: isLodingPutMyChildrenConcernsMutate} = putMyChildrenConcernsMutate()

  const handleClickOpenPopupDialog = () => setOpenAddChildren(false)

  const handleBackButton = () => {
    if (btnText === '다음') {
      setOpenAddChildren(false)

      setTimeout(() => {
        resetChildrenAllergyIngredientsList()
        resetChildrenAllergyIngredients()
        resetChildrenConcernsList()
        resetChildrenConcerns()
      }, 300)
    } else {
      setPage(0)

      setBtnText('다음')
      setBtnPosition('initial')
      setBtnMargin('')

      setTimeout(() => {
        resetChildrenConcernsList()
        resetChildrenConcerns()
      }, 300)
    }
  }

  const handleConfirmBtnClick = () => {
    if (btnText === '다음') {
      setPage(1)

      setBtnText('확인')
      setBtnPosition('fixed')
      setBtnMargin('20px')
    } else {
      // 변경된 사항 저장
      mutatePutMyChildrenAllergyIngredients({
        key: userInfo?.babies[index]?.key,
        bodyArray: childrenAllergyIngredients,
      }, {
        onSuccess: () => {
          mutatePutMyChildrenConcerns({
            key: userInfo?.babies[index]?.key,
            bodyArray: childrenConcerns,
          }, {
            onSuccess: () => {
              queryClient.invalidateQueries('getMyInfo')

              // 팝업 종료
              setOpenResultAdd(true)

              setTimeout(function () {
                setPage(0)

                setBtnText('다음')
                setBtnPosition('initial')
                setBtnMargin('')

                setOpenAddChildren(false)
              }, 300)

              setTimeout(function () {
                resetChildrenAllergyIngredientsList()
                resetChildrenAllergyIngredients()
                resetChildrenConcernsList()
                resetChildrenConcerns()
              }, 300)
            }
          })
        }
      })
    }
  }

  function replaceItemAtIndex(arr, index, newValue) {
    return [...arr.slice(0, index), newValue, ...arr.slice(index + 1)];
  }

  return (
    <MainDialogLayout
      fullScreen
      open={openAddChildren}
      onClose={handleClickOpenPopupDialog}
      TransitionComponent={Fade}
      sx={{
        '& .MuiDialog-paper': {
          mx: 0,
          height: '100%',
          backgroundColor: 'white',
        },
        zIndex: Z_INDEX.DIALOG,
      }}>
      <MyHeaderDialogLayout onBack={handleBackButton}>

        <Container maxWidth={'xs'} disableGutters sx={{px: '20px', pt: '20px', pb: '40px'}}>
          {page === 0 && <ChildrenAllergyInputForm userInfo={userInfo} index={index}/>}
          {page === 1 && <ChildrenKeywordInputForm userInfo={userInfo} index={index}/>}
        </Container>

        <Container
          disableGutters
          maxWidth={'xs'}
          sx={{
            position: 'fixed',
            bottom: 0,
            left: 0,
            right: 0,
            bgcolor: 'white',
            p: '20px',
          }}
        >
          <Button
            tabIndex={1}
            disableFocusRipple
            variant={'contained'}
            disabled={isBottomBtnDisabled}
            onClick={handleConfirmBtnClick}
            onKeyDown={(e) => handleEnterPress(e, handleConfirmBtnClick)}
            fullWidth
            sx={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              p: '12px 50px',
              height: '52px',
              fontStyle: 'normal',
              fontWeight: '700',
              fontSize: '16px',
              lineHeight: '28px',
              textAlign: 'center',
              color: '#FFFFFF',
              '&.Mui-disabled': {
                color: '#B3B3B3',
                backgroundColor: '#E8E8E8',
              },
              '&.Mui-disabled:hover': {
                backgroundColor: '#E6E6E6',
              },
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: 'primary.main',
              },
              ...clickBorderNone
            }}
          >
            {btnText}
          </Button>
        </Container>

      </MyHeaderDialogLayout>

      {(isLodingPutMyChildrenAllergyIngredients || isLodingPutMyChildrenConcernsMutate) && <LoadingScreen dialog/>}
    </MainDialogLayout>
  )
}