import {Box, Button, Container, Fade, Typography} from "@mui/material";
import {useRecoilState} from "recoil";
import {myOpenResultAddSelector} from "../../../../recoil/selector/my/popup/myPopupSelector";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {CONFIRM_SIGN_UP} from "../../../../constant/sign-up/SignUp";
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../../../constant/common/Common";

export default function ChildrenResultAddDialog() {
  const [openResultAdd, setOpenResultAdd] = useRecoilState(myOpenResultAddSelector)

  const onClick = () => setOpenResultAdd(false)

  return (
    <MainDialogLayout
      fullScreen
      open={openResultAdd}
      onClose={onClick}
      TransitionComponent={Fade}
      sx={{
        '& .MuiDialog-paper': {
          mx: 0,
          height: '100%',
          backgroundColor: 'white',
        },
        zIndex: Z_INDEX.DIALOG,
      }}>
      <Box sx={{display: 'flex', height: '100%'}}>
        {/* Contents */}
        <Box sx={{px: '20px', margin: 'auto', textAlign: '-webkit-center'}}>
          <SvgCommonIcons type={ICON_TYPE.COMMON_CHECK}/>
          <Box sx={{marginTop: '30px', color: '#222222'}}>
            <Typography variant={'b_18_m_1l'}>
              {CONFIRM_SIGN_UP.ADD_CHILDREN}
            </Typography>
          </Box>
        </Box>

        {/* Footer */}
        <Container
          maxWidth={'xs'}
          disableGutters
          sx={{
            position: 'fixed',
            bottom: '0',
            left: '0',
            right: '0',
            padding: '20px'
          }}
        >
          <Button
            tabIndex={1}
            disableFocusRipple
            fullWidth
            type="submit"
            variant="contained"
            sx={{
              height: '52px',
              fontSize: '16px',
              fontWeight: '700',
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: 'primary.main',
              },
              ...clickBorderNone
            }}
            onClick={onClick}
            onKeyDown={(e) => handleEnterPress(e, onClick)}
          >
            확인
          </Button>
        </Container>
      </Box>
    </MainDialogLayout>
  )
}