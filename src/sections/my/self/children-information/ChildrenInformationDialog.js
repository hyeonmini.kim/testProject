import * as React from "react";
import {forwardRef, useEffect, useState} from "react";
import {Avatar, Badge, Box, Button, Container, Fade, Slide} from "@mui/material";
import {MY_COMMON} from "../../../../constant/my/MyConstants";
import {useRecoilState, useSetRecoilState} from "recoil";
import {myChildrenBottomDialogState,} from "../../../../recoil/atom/my/information/myBottomDialog";
import ChildrenBirthInputForm from "./input-form/ChildrenBirthInputForm";
import ChildrenNicknameInputForm from "./input-form/ChildrenNicknameInputForm";
import ChildrenHeightInputForm from "./input-form/ChildrenHeightInputForm";
import ChildrenWeightInputForm from "./input-form/ChildrenWeightInputForm";
import ChildrenGenderInputForm from "./input-form/ChildrenGenderInputForm";
import {myOpenChildrenSelector} from "../../../../recoil/selector/my/popup/myPopupSelector";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import MyChildrenBottomDialog from "../my-information/MyChildrenBottomDialog";
import ChildrenBottomContents from "../my-information/ChildrenBottomContents";
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import {
  myBabyIsProfilePictureUrlDeletedSelector,
  myCallSelector,
  myDataSelector,
  tempMyBabyBirthdateSelector,
  tempMyBabyGenderSelector,
  tempMyBabyHeightSelector,
  tempMyBabyNicknameSelector,
  tempMyBabyProfilePictureUrlSelector,
  tempMyBabyWeightSelector
} from "../../../../recoil/selector/my/myDataSelector";
import {getYYMMDDFromHyphen} from "../../../../utils/formatString";
import {
  delMyChildrenProfilePictureMutate,
  getMyInfo,
  patchMyChildrenDetailsMutate,
  putMyChildrenProfilePictureMutate
} from "../../../../api/myApi";
import {useQueryClient} from "react-query";
import {fileData} from "../../../../components/file-thumbnail";
import LoadingScreen from "../../../../components/common/LoadingScreen";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../layouts/my/MyHeaderDialogLayout";
import useStackNavigation from "../../../../hooks/useStackNavigation";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../../../constant/common/Common";
import {ALT_STRING} from "../../../../constant/common/AltString";
import {getFloatFixed} from "../../../../utils/formatNumber";

const Transition = forwardRef((props, ref) => {
  <Slide direction="up" ref={ref} {...props} />
})

export default function ChildrenInformationDialog({userInfo, index, handleClick}) {
  const [myData, setMyData] = useRecoilState(myDataSelector);
  const [myBabyIsProfilePictureUrlDeleted, setMyBabyIsProfilePictureUrlDeleted] = useRecoilState(myBabyIsProfilePictureUrlDeletedSelector);
  const setIsChildrenBottomOpen = useSetRecoilState(myChildrenBottomDialogState)
  const [openChildren, setOpenChildren] = useRecoilState(myOpenChildrenSelector)

  const [isTrueNick, setIsTrueNick] = useState(true)
  const [isTrueBirth, setIsTrueBirth] = useState(true)
  const [isTrueHeight, setIsTrueHeight] = useState(true)
  const [isTrueWeight, setIsTrueWeight] = useState(true)

  const [nickname, setNickname] = useState(userInfo?.babies?.length ? userInfo?.babies[index]?.nickname : '')
  const [birthdate, setBirthdate] = useState(userInfo?.babies?.length ? getYYMMDDFromHyphen(userInfo?.babies[index]?.birthdate) : '')
  const [height, setHeight] = useState(userInfo?.babies?.length ? userInfo?.babies[index]?.height : '')
  const [weight, setWeight] = useState(userInfo?.babies?.length ? userInfo?.babies[index]?.weight : '')
  const [gender, setGender] = useState(userInfo?.babies?.length ? userInfo?.babies[index]?.gender : '')
  const [profilePictureUrl, setProfilePictureUrl] = useState(userInfo?.babies?.length ? userInfo?.babies[index]?.profilePictureUrl : '')

  const [tempMyBabyNickname, setTempMyBabyNickname] = useRecoilState(tempMyBabyNicknameSelector)
  const [tempMyBabyBirthdate, setTempMyBabyBirthdate] = useRecoilState(tempMyBabyBirthdateSelector)
  const [tempMyBabyHeight, setTempMyBabyHeight] = useRecoilState(tempMyBabyHeightSelector)
  const [tempMyBabyWeight, setTempMyBabyWeight] = useRecoilState(tempMyBabyWeightSelector)
  const [tempMyBabyGender, setTempMyBabyGender] = useRecoilState(tempMyBabyGenderSelector)
  const [tempMyBabyProfilePictureUrl, setTempMyBabyProfilePictureUrl] = useRecoilState(tempMyBabyProfilePictureUrlSelector)

  const [myCall, setMyCall] = useRecoilState(myCallSelector)

  const {navigation} = useStackNavigation()

  const queryClient = useQueryClient()

  const {
    mutate: mutatePatchMyChildrenDetails,
    isLoading: isLoadingPatchMyChildrenDetails
  } = patchMyChildrenDetailsMutate()
  const {
    mutate: mutatePutMyChildrenProfilePicture,
    isLoading: isLoadingPutMyChildrenProfilePicture
  } = putMyChildrenProfilePictureMutate()
  const {
    mutate: mutateDelMyChildrenProfilePicture,
    isLoading: isLoadingDelMyChildrenProfilePicture
  } = delMyChildrenProfilePictureMutate()

  const [isBtnDisabled, setIsBtnDisabled] = useState(true)

  const handleBackButton = () => {
    handleClose()
    setMyBabyIsProfilePictureUrlDeleted(false);
  }

  const handleGalleryClick = () => {
    setIsChildrenBottomOpen(true)
  }

  const handleClose = () => {
    if (myCall?.type) {
      navigation?.back()
    } else {
      setOpenChildren(false)
    }
  }

  const handleConfirmBtnClick = () => {
    if (tempMyBabyNickname === nickname && tempMyBabyBirthdate === birthdate && tempMyBabyHeight === height && tempMyBabyWeight === weight && tempMyBabyGender === gender && tempMyBabyProfilePictureUrl === profilePictureUrl) {
      handleClose()
      return
    }

    const parseHeight = getFloatFixed(height, 1)
    const parseWeight = getFloatFixed(weight, 1)

    // 변경된 사항 저장
    mutatePatchMyChildrenDetails({
      key: userInfo?.babies[index]?.key,
      nickname: nickname,
      birthdate: setHyphenBirthdate(birthdate),
      height: parseHeight,
      weight: parseWeight,
      gender: gender
    }, {
      onSuccess: () => {
        if (tempMyBabyProfilePictureUrl !== profilePictureUrl) {
          if (myBabyIsProfilePictureUrlDeleted) {
            mutateDelMyChildrenProfilePicture({
              key: userInfo?.babies[index]?.key,
            }, {
              onSuccess: () => {
                queryClient.invalidateQueries('getMyInfo')
                handleClose()
                setMyBabyIsProfilePictureUrlDeleted(false)
              }
            })
          } else {
            if (typeof profilePictureUrl === 'object') {
              const formData = new FormData()
              formData.append('multipartFile', profilePictureUrl)
              mutatePutMyChildrenProfilePicture({
                key: userInfo?.babies[index]?.key,
                formData: formData
              }, {
                onSuccess: () => {
                  queryClient.invalidateQueries('getMyInfo')
                  handleClose()
                  setMyBabyIsProfilePictureUrlDeleted(false)
                }
              })
            }
          }
        } else {
          queryClient.invalidateQueries('getMyInfo')
          // queryClient.removeQueries('getUserBabyInfo')
          queryClient.invalidateQueries('getUserBabyInfo')
          handleClose()
          setMyBabyIsProfilePictureUrlDeleted(false)
        }
      }
    })
  }

  function setHyphenBirthdate(value) {
    value = value.replaceAll('.', '-')
    value = "20" + value
    return value
  }

  useEffect(() => {
    if (openChildren) {
      setProfilePictureUrl(userInfo?.babies?.length ? userInfo?.babies[index]?.profilePictureUrl : '')
      setNickname(userInfo?.babies?.length ? userInfo?.babies[index]?.nickname : '')
      setBirthdate(userInfo?.babies?.length ? getYYMMDDFromHyphen(userInfo?.babies[index]?.birthdate) : '')
      setHeight(userInfo?.babies?.length ? userInfo?.babies[index]?.height : '')
      setWeight(userInfo?.babies?.length ? userInfo?.babies[index]?.weight : '')
      setGender(userInfo?.babies?.length ? userInfo?.babies[index]?.gender : '')

      setTempMyBabyProfilePictureUrl(userInfo?.babies?.length ? userInfo?.babies[index]?.profilePictureUrl : '')
      setTempMyBabyNickname(userInfo?.babies?.length ? userInfo?.babies[index]?.nickname : '')
      setTempMyBabyBirthdate(userInfo?.babies?.length ? getYYMMDDFromHyphen(userInfo?.babies[index]?.birthdate) : '')
      setTempMyBabyHeight(userInfo?.babies?.length ? userInfo?.babies[index]?.height : '')
      setTempMyBabyWeight(userInfo?.babies?.length ? userInfo?.babies[index]?.weight : '')
      setTempMyBabyGender(userInfo?.babies?.length ? userInfo?.babies[index]?.gender : '')
    }

    return () => {
      setMyBabyIsProfilePictureUrlDeleted(false);
    }
  }, [index, openChildren])

  useEffect(() => {
    setIsBtnDisabled(!(isTrueNick && isTrueBirth && isTrueHeight && isTrueWeight))
  }, [isTrueNick, isTrueBirth, isTrueHeight, isTrueWeight])

  return (
    <>
      <MainDialogLayout
        fullScreen
        open={openChildren}
        onClose={handleClose}
        TransitionComponent={Fade}
        sx={{
          '& .MuiDialog-paper': {
            mx: 0,
            height: '100%',
            backgroundColor: 'white',
          },
          zIndex: Z_INDEX.DIALOG,
        }}>
        <MyHeaderDialogLayout
          title={MY_COMMON.TEXT.CHILDREN_INFORM}
          onBack={handleBackButton}
        >
          <Container maxWidth={'xs'} disableGutters sx={{px: '20px', mt: '39px', mb: '34px', textAlign: 'center'}}>

            <Badge
              tabIndex={1}
              overlap="circular"
              anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
              badgeContent={
                <SvgCommonIcons alt={ALT_STRING.MY.BTN_CHANGE_CHILDREN_PROFILE} type={ICON_TYPE.PHOTO_ADD}/>
              }
              onClick={handleGalleryClick}
              onKeyDown={(e) => handleEnterPress(e, handleGalleryClick)}
              sx={{
                width: '88px',
                height: '88px',
                margin: 'auto',
                display: 'flex',
                cursor: 'pointer',
                overflow: 'hidden',
                position: 'relative',
                justifyContent: 'center',
                textDecoration: 'none',
                WebkitTapHighlightColor: 'transparent',
                ...clickBorderNone
              }}
            >
              {
                profilePictureUrl
                  ? <Avatar
                    alt={ALT_STRING.MY.PROFILE_CHILDREN}
                    src={fileData(profilePictureUrl)?.preview}
                    sx={{width: '88px', height: '88px'}}
                  />
                  : getDefaultChildrenAvatar(index, gender)
              }
            </Badge>

            <Button
              tabIndex={1}
              disableFocusRipple
              variant={'text'}
              startIcon={<SvgCommonIcons type={ICON_TYPE.PLUS_20PX} sx={{marginRight: '-4px'}}/>}
              onClick={handleClick}
              sx={{
                fontStyle: 'normal',
                fontWeight: 400,
                fontSize: '14px',
                lineHeight: '14px',
                letterSpacing: '-0.02em',
                color: '#222222',
                p: 0,
                mt: '20px',
                '&:hover': {boxShadow: 'none', backgroundColor: '#FFFFFF'},
                ...clickBorderNone
              }}
            >
              추가 정보 입력
            </Button>

          </Container>

          <Container maxWidth={'xs'} disableGutters sx={{px: '20px', pb: '94px'}}>

            <ChildrenNicknameInputForm nickname={nickname} setNickname={setNickname} isTrue={isTrueNick}
                                       setIsTrue={setIsTrueNick}/>
            <ChildrenBirthInputForm birthdate={birthdate} setBirthdate={setBirthdate} isTrue={isTrueBirth}
                                    setIsTrue={setIsTrueBirth}/>
            <ChildrenHeightInputForm height={height} setHeight={setHeight} isTrue={isTrueHeight}
                                     setIsTrue={setIsTrueHeight}/>
            <ChildrenWeightInputForm weight={weight} setWeight={setWeight} isTrue={isTrueWeight}
                                     setIsTrue={setIsTrueWeight}/>
            <ChildrenGenderInputForm childGender={gender} setGender={setGender}/>

          </Container>

          <BottomFixedButton text={'확인'} isDisabled={isBtnDisabled} onClick={handleConfirmBtnClick}/>

        </MyHeaderDialogLayout>
      </MainDialogLayout>
      <MyChildrenBottomDialog>
        <ChildrenBottomContents profilePictureUrl={profilePictureUrl} setProfilePictureUrl={setProfilePictureUrl}/>
      </MyChildrenBottomDialog>

      {(isLoadingPatchMyChildrenDetails || isLoadingPutMyChildrenProfilePicture || isLoadingDelMyChildrenProfilePicture) &&
        <LoadingScreen/>}
    </>
  )
}

function getDefaultChildrenAvatar(index, childGender) {
  let avatarImage = '';

  if (index === 0) {
    if (childGender === 'MALE') {
      return <SvgCommonIcons alt={ALT_STRING.MY.BTN_CHANGE_CHILDREN_PROFILE} type={ICON_TYPE.PROFILE_BOY_FIRST} sx={{width: '88px', height: '88px'}}/>
      // avatarImage = '/assets/icons/my/boy_first.svg';
    } else {
      return <SvgCommonIcons alt={ALT_STRING.MY.BTN_CHANGE_CHILDREN_PROFILE} type={ICON_TYPE.PROFILE_GIRL_FIRST} sx={{width: '88px', height: '88px'}}/>
      // avatarImage = '/assets/icons/my/girl_first.svg';
    }
  } else if (index === 1) {
    if (childGender === 'MALE') {
      return <SvgCommonIcons alt={ALT_STRING.MY.BTN_CHANGE_CHILDREN_PROFILE} type={ICON_TYPE.PROFILE_BOY_SECOND} sx={{width: '88px', height: '88px'}}/>
      // avatarImage = '/assets/icons/my/boy_second.svg';
    } else {
      return <SvgCommonIcons alt={ALT_STRING.MY.BTN_CHANGE_CHILDREN_PROFILE} type={ICON_TYPE.PROFILE_GIRL_SECOND} sx={{width: '88px', height: '88px'}}/>
      // avatarImage = '/assets/icons/my/girl_second.svg';
    }
  } else if (index === 2) {
    if (childGender === 'MALE') {
      return <SvgCommonIcons alt={ALT_STRING.MY.BTN_CHANGE_CHILDREN_PROFILE} type={ICON_TYPE.PROFILE_BOY_THIRD} sx={{width: '88px', height: '88px'}}/>
      // avatarImage = '/assets/icons/my/boy_third.svg';
    } else {
      return <SvgCommonIcons alt={ALT_STRING.MY.BTN_CHANGE_CHILDREN_PROFILE} type={ICON_TYPE.PROFILE_GIRL_THIRD} sx={{width: '88px', height: '88px'}}/>
      // avatarImage = '/assets/icons/my/girl_third.svg';
    }
  }

  return avatarImage;
}

function BottomFixedButton({text, onClick, isDisabled = false, sx}) {
  return (
    <Box
      sx={{
        p: '20px',
        position: 'fixed',
        zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
        bgcolor: 'white',
        bottom: 0,
        left: 0,
        right: 0,
        ...sx,
      }}
    >
      <Container maxWidth={'xs'} disableGutters>
        <Button
          tabIndex={1}
          disableFocusRipple
          fullWidth
          variant="contained"
          onClick={onClick}
          disabled={isDisabled}
          sx={{
            height: '52px',
            fontSize: '16px',
            fontWeight: '700',
            lineHeight: '28px',
            backgroundColor: isDisabled ? '#E6E6E6' : 'primary.main',
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: isDisabled ? '#E6E6E6' : 'primary.main',
            },
            ...clickBorderNone
          }}
        >
          {text}
        </Button>
      </Container>
    </Box>
  )
}