import {Box, InputAdornment, TextField, Typography} from "@mui/material";
import {MY_COMMON} from "../../../../../constant/my/MyConstants";
import {useSetRecoilState} from "recoil";
import {authBottomButtonState} from "../../../../../recoil/atom/sign-up/auth";
import {useEffect, useState} from "react";
import {SIGN_UP_CHILD_BIRTH} from "../../../../../constant/sign-up/SignUp";
import {ICON_TYPE, SvgCommonIcons} from "../../../../../constant/icons/ImageIcons";
import {checkChildBirthValidation} from "../../../../../utils/customValidation";
import {ALT_STRING} from "../../../../../constant/common/AltString";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";

export default function ChildrenBirthInputForm({birthdate, setBirthdate, isTrue, setIsTrue}) {
  // 입력 상태 state
  const [helperText, setHelperText] = useState(SIGN_UP_CHILD_BIRTH.HELPER_TEXT)
  const [cancelIcon, setCancelIcon] = useState(false)

  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  // Child Birth 값 변경 제어
  const handleChangeValue = (e) => {
    // 숫자 6자리 정규식
    const regex = /[^0-9]/g
    let value = e.target.value
    let valueNum = value.replace(regex, '').substring(0, 6)

    checkChildBirthValidation(valueNum, setIsTrue, setIsBottomBtnDisabled, setHelperText)

    // 생년월일 사이에 콤마를 넣기위한 처리
    if (valueNum.length <= 2) {
      value = valueNum
    }
    if (valueNum.length > 2) {
      value = valueNum.slice(0, 2) + "." + valueNum.slice(2)
    }
    if (valueNum.length > 4) {
      value = valueNum.slice(0, 2) + "." + valueNum.slice(2, 4) + "." + valueNum.slice(4)
    }

    setBirthdate(value)
  }

  useEffect(() => {
    if (birthdate.length === 0) {
      setIsTrue(true)
      setHelperText(SIGN_UP_CHILD_BIRTH.HELPER_TEXT)
      setIsBottomBtnDisabled(true)
    }
  }, [birthdate])

  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true)
  const handleBlur = () => setCancelIcon(false)
  const handleCancel = () => setBirthdate('')

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '14px',
          lineHeight: '22px',
          color: '#888888',
          mb: '10px',
        }}
      >
        {MY_COMMON.TEXT.BIRTH}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={birthdate}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        type="tel"
        helperText={birthdate === '' ? helperText : isTrue ? ' ' : helperText}
        inputProps={{tabIndex: 1, style: {padding: 0}, title: ALT_STRING.MY.CHILDREN.INPUT_BIRTH}}
        InputProps={{
          endAdornment: birthdate && cancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
            </InputAdornment>
          ),
          style: {
            paddingTop: '15px',
            paddingBottom: '13px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
          },
        }}
        FormHelperTextProps={{
          style: {
            marginTop: '10px',
            marginBottom: '18px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '12px',
            letterSpacing: '-0.03em',
            color: birthdate === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          '& .MuiInput-root:before': {
            borderBottom: birthdate === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: birthdate === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: birthdate === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: birthdate === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: birthdate === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  );
}