import {Box, IconButton, Typography} from "@mui/material";
import * as React from "react";
import {useEffect, useState} from "react";
import {MY_COMMON} from "../../../../../constant/my/MyConstants";
import {CHILD_WEIGHT, SIGN_UP_CHILD_WEIGHT} from "../../../../../constant/sign-up/SignUp";
import {ICON_TYPE, SvgCommonIcons} from "../../../../../constant/icons/ImageIcons";
import {ALT_STRING} from "../../../../../constant/common/AltString";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";

export default function ChildrenWeightInputForm({weight, setWeight, isTrue, setIsTrue}) {
  // 입력 상태 state
  const [helperText, setHelperText] = useState(SIGN_UP_CHILD_WEIGHT.HELPER_TXT)
  const [cancelIcon, setCancelIcon] = useState(false)
  const [hasDot, setHasDot] = useState(false)
  const [hasDotTwoMore, setHasDotTwoMore] = useState(false)
  // Child Weight 값 변경 제어
  const handleChangeValue = (e) => {
    const weight = (e.target.value.length === 1 && e.target.value === '0') ? '' : e.target.value
    setWeight(weight)
  }
  // Child Height 정규식 체크
  const checkChildWeightValidation = () => {
    // 소수점 첫째 자리 정규식
    const regex = /^\d*[.]?$/
    const regexOnlyDot = /^[.]$/
    const regexLastDot = /^\d*[.]$/
    const regexHasDot = /^\d*[.]\d$/

    if (weight !== '') {
      if (weight > CHILD_WEIGHT.MAX) {
        setErrorHelperText(SIGN_UP_CHILD_WEIGHT.HELPER_TXT_MAX_WEIGHT)
        setIsTrue(false)
      } else if (weight < CHILD_WEIGHT.MIN) {
        setErrorHelperText(SIGN_UP_CHILD_WEIGHT.HELPER_TXT_MIN_WEIGHT)
        setIsTrue(false)
      } else {
        if (regex.test(weight) && !regexOnlyDot.test(weight) && !regexLastDot.test(weight)) {
          setErrorHelperText(SIGN_UP_CHILD_WEIGHT.HELPER_TXT)
          setIsTrue(true)
          setHasDot(false)
        } else {
          if (regexHasDot.test(weight)) {
            setIsTrue(true)
            setHasDot(true)
            setHasDotTwoMore(false)
          } else {
            setErrorHelperText(SIGN_UP_CHILD_WEIGHT.HELPER_TXT)
            setIsTrue(false)
            setHasDot(true)
            setHasDotTwoMore(true)
          }
        }
      }
    } else {
      setErrorHelperText(SIGN_UP_CHILD_WEIGHT.HELPER_TXT)
      setIsTrue(false)
    }
  }

  const setErrorHelperText = (helperText) => {
    if (helperText === SIGN_UP_CHILD_WEIGHT.HELPER_TXT && !hasDotTwoMore) {
      setIsTrue(true)
      setHelperText(helperText)
    } else {
      setIsTrue(false)
      setHelperText(helperText)
    }
  }

  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true)
  const handleBlur = () => setCancelIcon(false)
  const handleCancel = () => setWeight('')

  useEffect(() => {
    checkChildWeightValidation()
  }, [weight])

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '14px',
          lineHeight: '22px',
          color: '#888888',
          mb: '10px',
        }}
      >
        {MY_COMMON.TEXT.WEIGHT}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={weight}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        helperText={weight === '' ? helperText : isTrue && hasDot ? ' ' : helperText}
        inputProps={{tabIndex: 1, style: {padding: 0}, inputMode: 'decimal', title: ALT_STRING.MY.CHILDREN.INPUT_WEIGHT}}
        InputProps={{
          endAdornment: (
            weight === '' ?
              <Typography
                sx={{
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '22px',
                  letterSpacing: '-0.03em',
                  color: '#000000',
                  marginRight: '14px',
                }}
              >
                kg
              </Typography>
              :
              <Box display="flex" marginRight="14px">
                {weight && cancelIcon && (
                  <IconButton onClick={handleCancel} sx={{p: 0, marginRight: '8px'}}>
                    <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
                  </IconButton>
                )}
                <Typography
                  sx={{
                    fontStyle: 'normal',
                    fontWeight: 500,
                    fontSize: '16px',
                    lineHeight: '22px',
                    letterSpacing: '-0.03em',
                    color: '#000000',
                  }}
                >
                  kg
                </Typography>
              </Box>
          ),
          style: {
            paddingTop: '15px',
            paddingBottom: '13px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#222222',
          },
        }}
        FormHelperTextProps={{
          style: {
            marginTop: '10px',
            marginBottom: '18px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '12px',
            letterSpacing: '-0.03em',
            color: weight === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          '& .MuiInput-root:before': {
            borderBottom: weight === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: weight === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: weight === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: weight === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: weight === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  );
}