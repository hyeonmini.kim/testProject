import {Box, Grid, Typography} from "@mui/material";
import {SIGN_UP_SUBTITLE, SIGN_UP_TITLE} from "../../../../../constant/sign-up/SignUp";
import {useRecoilState, useResetRecoilState, useSetRecoilState} from "recoil";
import {myDataSelector} from "../../../../../recoil/selector/my/myDataSelector";
import {childrenConcernsListSelector, childrenConcernsSelector} from "../../../../../recoil/selector/my/childrenDataSelector";
import {useEffect} from "react";
import {authBottomButtonState} from "../../../../../recoil/atom/sign-up/auth";
import {clickBorderNone} from "../../../../../constant/common/Common";
import {handleEnterPress} from "../../../../../utils/onKeyDownUtils";

export default function ChildrenKeywordInputForm({userInfo, index}) {
  const [myData, setMyData] = useRecoilState(myDataSelector);
  const [childrenConcernsList, setChildrenConcernsList] = useRecoilState(childrenConcernsListSelector)
  const resetChildrenConcernsList = useResetRecoilState(childrenConcernsListSelector)
  const [childrenConcerns, setChildrenConcerns] = useRecoilState(childrenConcernsSelector)
  const resetChildrenConcerns = useResetRecoilState(childrenConcernsSelector)
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)

  useEffect(() => {
    if (userInfo?.babies?.length > 0) {
      let newList = childrenConcernsList;
      let newConcernsInfoList = [];
      if (userInfo?.babies[index]?.concerns?.length > 0) {
        userInfo?.babies[index]?.concerns?.map((item) => {
          const index = childrenConcernsList?.findIndex((listItem) => listItem?.health_name === item?.name);
          newList = replaceItemAtIndex(newList, index, {
            health_name: item?.name,
            clicked: true,
            health_key: newList[index]?.health_key,
          });
          // 아이 키워드 추가/삭제
          // const idx = childrenConcerns?.findIndex((listItem) => listItem?.health_name === item?.name)
          // if (idx >= 0) {
          //   // 있으면 제거
          //   setChildrenConcerns(childrenConcerns?.filter(keyword => keyword?.name !== item?.name))
          // } else {
          //   // 없으므로 추가
          const newKeyword = {
            name: item?.name,
            key: newList[index]?.health_key
          }
          newConcernsInfoList = newConcernsInfoList.concat([newKeyword])
          // setChildrenConcerns([...childrenConcerns, newKeyword])
          // }
        })
        setChildrenConcernsList(newList);
        setChildrenConcerns(newConcernsInfoList);
      } else {
        resetChildrenConcernsList()
        resetChildrenConcerns()
      }
    }
  }, [])

  // Child Keyword 값 변경 제어
  const handleClick = (e) => {
    let count = 0;
    const index = childrenConcernsList?.findIndex((listItem) => listItem?.health_name === e.target.id);
    const newList = replaceItemAtIndex(childrenConcernsList, index, {
      health_name: e.target.id,
      clicked: !childrenConcernsList[index]?.clicked,
      health_key: childrenConcernsList[index]?.health_key,
    });

    newList.map(obj => {
      if (obj.clicked === true) {
        count = count + 1;
      }
    });

    if (count < 4) {
      setChildrenConcernsList(newList);

      // 아이 키워드 추가/삭제
      const idx = childrenConcerns?.findIndex((listItem) => listItem?.name === e.target.id)
      if (idx >= 0) {
        // 있으면 제거
        setChildrenConcerns(childrenConcerns?.filter(key => key?.name !== e.target.id))
      } else {
        // 없으므로 추가
        const newKeyword = {
          name: e.target.id,
          key: childrenConcernsList[index]?.health_key
        }
        setChildrenConcerns([...childrenConcerns, newKeyword])
      }
    }
  }

  function replaceItemAtIndex(arr, index, newValue) {
    return [...arr?.slice(0, index), newValue, ...arr?.slice(index + 1)];
  }

  useEffect(() => {
    const index = childrenConcernsList.findIndex((listItem) => listItem.clicked === true);
    setIsBottomBtnDisabled(index < 0);
  }, [childrenConcernsList]);

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          color: '#000000',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.CHILD_KEYWORD}
      </Typography>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 400,
          fontSize: '16px',
          lineHeight: '24px',
          letterSpacing: '-0.05em',
          color: '#666666',
          whiteSpace: 'pre-wrap',
          marginTop: '10px',
        }}
      >
        {SIGN_UP_SUBTITLE.CHILD_KEYWORD}
      </Typography>
      <Box sx={{flexGrow: 1}}>
        <Grid container rowSpacing="20px" columnSpacing="10px" sx={{marginTop: '10px', textAlign: 'center'}}
              columns={{xs: 3}}>
          {childrenConcernsList.map(function (obj, idx) {
            return (
              <Grid
                item
                key={idx}
                xs={1}
                sx={{
                  display: 'table',
                  height: '62px',
                  flexBasis: 'auto',
                  maxWidth: 'none',
                  '& .MuiGrid-root.MuiGrid-item': {
                    padding: 0,
                  }
                }}
              >
                <Box
                  tabIndex={1}
                  id={obj.health_name}
                  clicked={obj.clicked.toString()}
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    height: '100%',
                    border: 'none',
                    borderRadius: '30px',
                    backgroundColor: obj.clicked ? 'primary.main' : '#F6F6F6',
                    fontStyle: 'normal',
                    fontWeight: obj.clicked ? 700 : 500,
                    fontSize: '14px',
                    lineHeight: '22px',
                    letterSpacing: '-0.02em',
                    color: obj.clicked ? '#FFFFFF' : '#666666',
                    paddingX: '20px',
                    justifyContent: 'center',
                    ...clickBorderNone
                  }}
                  onClick={handleClick}
                  onKeyDown={(e) => handleEnterPress(e, handleClick)}
                >
                  {obj.health_name}
                </Box>
              </Grid>
            );
          })}
        </Grid>
      </Box>
    </Box>
  );
}