import {Box, InputAdornment, Typography} from "@mui/material";
import * as React from "react";
import {useEffect, useState} from "react";
import {useSetRecoilState} from "recoil";
import {SIGN_UP_NICKNAME} from "../../../../../constant/sign-up/SignUp";
import {MY_COMMON} from "../../../../../constant/my/MyConstants";
import {myInformationFooterBottomButtonState} from "../../../../../recoil/atom/my/information/footer";
import {ICON_TYPE, SvgCommonIcons} from "../../../../../constant/icons/ImageIcons";
import {ALT_STRING} from "../../../../../constant/common/AltString";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";

export default function ChildrenNicknameInputForm({nickname, setNickname, isTrue, setIsTrue}) {
  // 입력 상태 state
  const [helperText, setHelperText] = useState(SIGN_UP_NICKNAME.HELPER_TEXT)
  const [cancelIcon, setCancelIcon] = useState(false)
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(myInformationFooterBottomButtonState)
  // Nickname 값 변경 제어
  const handleChangeValue = (e) => {
    const value = e.target.value.substring(0, 10)
    checkNicknameValidation(value)

    setNickname(value)
  }
  // Nickname 정규식 체크
  const checkNicknameValidation = (value) => {
    // 영문/한글/숫자 포함 10자리 이내 정규식
    const regex = /^[ㄱ-ㅎ가-힣a-zA-Z0-9]{2,10}$/g
    if (regex.test(value)) {
      setIsTrue(true)
      setIsBottomBtnDisabled(false)
    } else {
      setIsTrue(false)
      setHelperText(SIGN_UP_NICKNAME.HELPER_TEXT)
      setIsBottomBtnDisabled(true)
    }
  }
  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true)
  const handleBlur = () => setCancelIcon(false)
  const handleCancel = () => setNickname('')

  useEffect(() => {
    checkNicknameValidation(nickname)
  }, [nickname])

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '14px',
          lineHeight: '22px',
          color: '#888888',
          mb: '10px',
        }}
      >
        {MY_COMMON.TEXT.NAME_NICKNAME}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={nickname}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        helperText={nickname === '' ? ' ' : isTrue ? ' ' : helperText}
        inputProps={{tabIndex: 1, style: {padding: 0}, title: ALT_STRING.MY.CHILDREN.INPUT_NICKNAME}}
        InputProps={{
          endAdornment: nickname && cancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
            </InputAdornment>
          ),
          style: {
            paddingTop: '15px',
            paddingBottom: '13px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '24px',
            letterSpacing: '-0.02em',
            color: '#222222',
          },
        }}
        FormHelperTextProps={{
          style: {
            marginTop: '10px',
            marginBottom: '18px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '12px',
            letterSpacing: '-0.03em',
            color: nickname === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          '& .MuiInput-root:before': {
            borderBottom: nickname === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: nickname === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: nickname === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: nickname === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: nickname === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  );
}