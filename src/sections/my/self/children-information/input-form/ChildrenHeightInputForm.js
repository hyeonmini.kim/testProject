import {Box, IconButton, Typography} from "@mui/material";
import * as React from "react";
import {useEffect, useState} from "react";
import {CHILD_HEIGHT, SIGN_UP_CHILD_HEIGHT} from "../../../../../constant/sign-up/SignUp";
import {MY_COMMON} from "../../../../../constant/my/MyConstants";
import {ICON_TYPE, SvgCommonIcons} from "../../../../../constant/icons/ImageIcons";
import {ALT_STRING} from "../../../../../constant/common/AltString";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";

export default function ChildrenHeightInputForm({height, setHeight, isTrue, setIsTrue}) {
  // 입력 상태 state
  const [helperText, setHelperText] = useState(SIGN_UP_CHILD_HEIGHT.HELPER_TXT)
  const [cancelIcon, setCancelIcon] = useState(false)
  const [hasDot, setHasDot] = useState(false)
  const [hasDotTwoMore, setHasDotTwoMore] = useState(false)
  // Child Height 값 변경 제어
  const handleChangeValue = (e) => {
    const height = (e.target.value.length === 1 && e.target.value === '0') ? '' : e.target.value
    setHeight(height)
  }
  // Child Height 정규식 체크
  const checkChildHeightValidation = () => {
    // 소수점 첫째 자리 정규식
    const regex = /^\d*[.]?$/
    const regexOnlyDot = /^[.]$/
    const regexLastDot = /^\d*[.]$/
    const regexHasDot = /^\d*[.]\d$/

    if (height !== '') {
      if (height > CHILD_HEIGHT.MAX) {
        setErrorHelperText(SIGN_UP_CHILD_HEIGHT.HELPER_TXT_MAX_HEIGHT)
        setIsTrue(false)
      } else if (height < CHILD_HEIGHT.MIN) {
        setErrorHelperText(SIGN_UP_CHILD_HEIGHT.HELPER_TXT_MIN_HEIGHT)
        setIsTrue(false)
      } else {
        if (regex.test(height) && !regexOnlyDot.test(height) && !regexLastDot.test(height)) {
          setErrorHelperText(SIGN_UP_CHILD_HEIGHT.HELPER_TXT)
          setIsTrue(true)
          setHasDot(false)
        } else {
          if (regexHasDot.test(height)) {
            setIsTrue(true)
            setHasDot(true)
            setHasDotTwoMore(false)
          } else {
            setErrorHelperText(SIGN_UP_CHILD_HEIGHT.HELPER_TXT)
            setIsTrue(false)
            setHasDot(true)
            setHasDotTwoMore(true)
          }
        }
      }
    } else {
      setErrorHelperText(SIGN_UP_CHILD_HEIGHT.HELPER_TXT)
      setIsTrue(false)
    }
  }

  const setErrorHelperText = (helperText) => {
    if (helperText === SIGN_UP_CHILD_HEIGHT.HELPER_TXT && !hasDotTwoMore) {
      setIsTrue(true)
      setHelperText(helperText)
    } else {
      setIsTrue(false)
      setHelperText(helperText)
    }
  }

  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true)
  const handleBlur = () => setCancelIcon(false)
  const handleCancel = () => setHeight('')

  useEffect(() => {
    checkChildHeightValidation()
  }, [height])

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '14px',
          lineHeight: '22px',
          color: '#888888',
          mb: '10px',
        }}
      >
        {MY_COMMON.TEXT.HEIGHT}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={height}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        helperText={height === '' ? helperText : isTrue && hasDot ? ' ' : helperText}
        inputProps={{tabIndex: 1, style: {padding: 0}, inputMode: 'decimal', title: ALT_STRING.MY.CHILDREN.INPUT_HEIGHT}}
        InputProps={{
          endAdornment: (
            height === '' ?
              <Typography
                sx={{
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '22px',
                  letterSpacing: '-0.03em',
                  color: '#000000',
                  marginRight: '14px',
                }}
              >
                cm
              </Typography>
              :
              <Box display="flex" marginRight="14px">
                {height && cancelIcon && (
                  <IconButton sx={{p: 0, marginRight: '8px'}}>
                    <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
                  </IconButton>
                )}
                <Typography
                  sx={{
                    fontStyle: 'normal',
                    fontWeight: 500,
                    fontSize: '16px',
                    lineHeight: '22px',
                    letterSpacing: '-0.03em',
                    color: '#000000',
                  }}
                >
                  cm
                </Typography>
              </Box>
          ),
          style: {
            paddingTop: '15px',
            paddingBottom: '13px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#222222',
          },
        }}
        FormHelperTextProps={{
          style: {
            marginTop: '10px',
            marginBottom: '18px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '12px',
            letterSpacing: '-0.03em',
            color: height === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          '& .MuiInput-root:before': {
            borderBottom: height === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: height === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: height === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: height === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: height === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  );
}