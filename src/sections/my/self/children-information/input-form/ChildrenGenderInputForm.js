import {Box, Typography} from "@mui/material";
import {SIGN_UP_CHILD_GENDER} from "../../../../../constant/sign-up/SignUp";
import {MY_COMMON} from "../../../../../constant/my/MyConstants";
import {clickBorderNone} from "../../../../../constant/common/Common";
import {handleEnterPress} from "../../../../../utils/onKeyDownUtils";

export default function ChildrenGenderInputForm({childGender, setGender}) {
  // Child Gender 값 변경 제어
  const handleClick = (e) => setGender(e.target.id === 'MALE' ? 'MALE' : 'FEMALE')

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '14px',
          lineHeight: '22px',
          color: '#888888',
          mb: '10px',
        }}
      >
        {MY_COMMON.TEXT.GENDER}
      </Typography>
      <Box sx={{display: 'flex', justifyContent: 'center'}}>
        <Box
          tabIndex={1}
          id={'MALE'}
          onClick={handleClick}
          onKeyDown={(e) => handleEnterPress(e, handleClick)}
          sx={{
            width: '100%',
            maxWidth: '155px',
            height: '50px',
            padding: 0,
            textAlign: 'center',
            border: childGender === 'MALE' ? '1px solid #ECA548' : '1px solid #CCCCCC',
            borderRadius: '8px',
            marginRight: '5px',
            ...clickBorderNone
          }}
        >
          <Typography
            id={'MALE'}
            sx={{
              fontStyle: 'normal',
              fontWeight: childGender === 'MALE' ? 700 : 400,
              fontSize: '16px',
              lineHeight: '48px',
              letterSpacing: '-0.02em',
              color: childGender === 'MALE' ? '#ECA548' : '#666666',
            }}
          >
            {SIGN_UP_CHILD_GENDER.BOY}
          </Typography>
        </Box>
        <Box
          tabIndex={1}
          id={'FEMALE'}
          onClick={handleClick}
          onKeyDown={(e) => handleEnterPress(e, handleClick)}
          sx={{
            width: '100%',
            maxWidth: '155px',
            height: '50px',
            padding: 0,
            textAlign: 'center',
            border: childGender === 'MALE' ? '1px solid #CCCCCC' : '1px solid #ECA548',
            borderRadius: '8px',
            marginLeft: '5px',
            ...clickBorderNone
          }}
        >
          <Typography
            id={'FEMALE'}
            sx={{
              fontStyle: 'normal',
              fontWeight: childGender === 'MALE' ? 400 : 700,
              fontSize: '16px',
              lineHeight: '48px',
              letterSpacing: '-0.02em',
              color: childGender === 'MALE' ? '#666666' : '#ECA548',
            }}
          >
            {SIGN_UP_CHILD_GENDER.GIRL}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
}