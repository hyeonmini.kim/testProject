import {Box, Grid, Typography} from "@mui/material";
import {SIGN_UP_SUBTITLE, SIGN_UP_TITLE} from "../../../../../constant/sign-up/SignUp";
import {SvgAllergyIcons} from "../../../../../constant/icons/AllergyIcons";
import {useRecoilState, useResetRecoilState, useSetRecoilState} from "recoil";
import {
  childrenAllergyIngredientsListSelector,
  childrenAllergyIngredientsSelector
} from "../../../../../recoil/selector/my/childrenDataSelector";
import {useEffect} from "react";
import {authBottomButtonMarginState, authBottomButtonPositionState, authBottomButtonState} from "../../../../../recoil/atom/sign-up/auth";
import {clickBorderNone} from "../../../../../constant/common/Common";
import {handleEnterPress} from "../../../../../utils/onKeyDownUtils";

export default function ChildrenAllergyInputForm({userInfo, index}) {
  const [childrenAllergyIngredientsList, setChildrenAllergyIngredientsList] = useRecoilState(childrenAllergyIngredientsListSelector)
  const resetChildrenAllergyIngredientsList = useResetRecoilState(childrenAllergyIngredientsListSelector)
  const [childrenAllergyIngredients, setChildrenAllergyIngredients] = useRecoilState(childrenAllergyIngredientsSelector)
  const resetChildrenAllergyIngredients = useResetRecoilState(childrenAllergyIngredientsSelector)
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  const setButtonPosition = useSetRecoilState(authBottomButtonPositionState)
  const setButtonMargin = useSetRecoilState(authBottomButtonMarginState)

  useEffect(() => {
    setButtonPosition('initial')
    setButtonMargin('0px')
    if (userInfo?.babies?.length > 0) {
      let newList = childrenAllergyIngredientsList;
      let newAllergyIngredientsInfoList = [];
      if (userInfo?.babies[index]?.allergyIngredients?.length > 0) {
        userInfo?.babies[index]?.allergyIngredients?.map((item) => {
          newList = replaceItemAtIndex(newList, 0, {
            alg_name: childrenAllergyIngredientsList[0]?.alg_name,
            value: false,
            image: childrenAllergyIngredientsList[0]?.image,
            alg_key: childrenAllergyIngredientsList[0]?.alg_key,
          })
          const index = childrenAllergyIngredientsList?.findIndex((listItem) => listItem?.alg_name === item?.name)
          newList = replaceItemAtIndex(newList, index, {
            alg_name: item?.name,
            value: true,
            image: newList[index]?.image,
            alg_key: newList[index]?.alg_key,
          })
          // 아이 알러지 추가/삭제
          // const idx = childrenAllergyIngredients?.findIndex((listItem) => listItem?.alg_name === item?.name)
          // if (idx >= 0) {
          //   // 있으면 제거
          //   setChildrenAllergyIngredients(childrenAllergyIngredients?.filter(alg => alg?.name !== item?.name))
          // } else {
          //   // 없으므로 추가
          const newAlg = {
            name: item?.name,
            key: newList[index]?.alg_key
          }
          newAllergyIngredientsInfoList = newAllergyIngredientsInfoList.concat([newAlg])
          // setChildrenAllergyIngredients([...childrenAllergyIngredients, newAlg])
          // }
        })
        setChildrenAllergyIngredientsList(newList)
        setChildrenAllergyIngredients(newAllergyIngredientsInfoList)
      } else {
        resetChildrenAllergyIngredientsList()
        resetChildrenAllergyIngredients()
      }
    }

    return () => {
      setButtonPosition('fixed')
      setButtonMargin('20px')
    }
  }, [])

  // Child Allergy 값 변경 제어
  const handleClick = (id) => {
    let newList = childrenAllergyIngredientsList;

    if (id === '없음') {
      let index = 0
      while (index < childrenAllergyIngredientsList?.length) {
        newList = replaceItemAtIndex(newList, index, {
          alg_name: childrenAllergyIngredientsList[index]?.alg_name,
          value: false,
          image: childrenAllergyIngredientsList[index]?.image,
          alg_key: childrenAllergyIngredientsList[index]?.alg_key,
        })
        index++
      }
      newList = replaceItemAtIndex(newList, 0, {
        alg_name: childrenAllergyIngredientsList[0]?.alg_name,
        value: true,
        image: childrenAllergyIngredientsList[0]?.image,
        alg_key: childrenAllergyIngredientsList[0]?.alg_key,
      })
      // 아이 알러지 초기화
      resetChildrenAllergyIngredients()
    } else {
      newList = replaceItemAtIndex(newList, 0, {
        alg_name: childrenAllergyIngredientsList[0]?.alg_name,
        value: false,
        image: childrenAllergyIngredientsList[0]?.image,
        alg_key: childrenAllergyIngredientsList[0]?.alg_key,
      })
      const index = childrenAllergyIngredientsList?.findIndex((listItem) => listItem?.alg_name === id)
      newList = replaceItemAtIndex(newList, index, {
        alg_name: id,
        value: !newList[index]?.value,
        image: newList[index]?.image,
        alg_key: newList[index]?.alg_key,
      })
      // 아이 알러지 추가/삭제
      const idx = childrenAllergyIngredients?.findIndex((listItem) => listItem?.name === id)
      if (idx >= 0) {
        // 있으면 제거
        setChildrenAllergyIngredients(childrenAllergyIngredients?.filter(alg => alg?.name !== id))
      } else {
        // 없으므로 추가
        const newAlg = {
          name: id,
          key: childrenAllergyIngredientsList[index]?.alg_key
        }
        setChildrenAllergyIngredients([...childrenAllergyIngredients, newAlg])
      }
    }

    setChildrenAllergyIngredientsList(newList)
  }

  function replaceItemAtIndex(arr, index, newValue) {
    return [...arr?.slice(0, index), newValue, ...arr?.slice(index + 1)];
  }

  useEffect(() => {
    const index = childrenAllergyIngredientsList.findIndex((listItem) => listItem.value === true)
    setIsBottomBtnDisabled(index < 0)
  }, [childrenAllergyIngredientsList])

  return (
    <Box sx={{marginBottom: '40px'}}>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          color: '#000000',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.CHILD_ALLERGY}
      </Typography>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 400,
          fontSize: '16px',
          lineHeight: '24px',
          letterSpacing: '-0.05em',
          color: '#666666',
          whiteSpace: 'pre-wrap',
          marginTop: '10px',
        }}
      >
        {SIGN_UP_SUBTITLE.CHILD_ALLERGY}
      </Typography>
      <Box sx={{flexGrow: 1}}>
        <Grid
          container
          columnSpacing="10px"
          rowSpacing="8px"
          sx={{marginTop: '22px', textAlign: 'center'}}
        >
          {childrenAllergyIngredientsList.map((obj, idx) => (
            <Grid item key={idx} xs={4} sx={{height: '150px', '& .MuiGrid-root.MuiGrid-item': {padding: 0}}}>
              <Box
                tabIndex={1}
                sx={{
                  height: '100%',
                  border: obj.value ? '1.4px solid #ECA548' : '1px solid #E6E6E6',
                  borderRadius: '8px',
                  backgroundColor: '#FFFFFF',
                  display: 'flex',
                  flexDirection: 'column',
                  ...clickBorderNone
                }}
                onClick={() => handleClick(obj.alg_name)}
                onKeyDown={(e) => handleEnterPress(e, () => handleClick(obj.alg_name))}
              >
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 'auto',
                    paddingBottom: obj.image?.PADDING_BOTTOM,
                  }}
                >
                  <SvgAllergyIcons type={obj.image?.TITLE} isSelected={obj.value}/>
                </Box>
                <Typography
                  sx={{
                    fontWeight: obj.value ? 700 : 400,
                    fontSize: '16px',
                    lineHeight: '22px',
                    letterSpacing: '-0.03em',
                    color: obj.value ? '#ECA548' : '#666666',
                    paddingBottom: '20px',
                  }}
                >
                  {obj.alg_name}
                </Typography>
              </Box>
            </Grid>
          ))}
        </Grid>
      </Box>
    </Box>
  );
}