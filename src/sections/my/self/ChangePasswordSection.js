import {Container, Fade, IconButton, Typography} from "@mui/material";
import {useRecoilState, useRecoilValue, useSetRecoilState} from "recoil";
import * as React from "react";
import {useEffect, useState} from "react";
import CancelIcon from '@mui/icons-material/Cancel';
import {CHANGE_PASSWORD} from "../../../constant/sign-up/SignUp";
import {authBottomButtonState} from "../../../recoil/atom/sign-up/auth";
import DonotsBottomButton from "../../../components/common/DonotsBottomButton";
import {myOpenChangePwSelector} from "../../../recoil/selector/my/popup/myPopupSelector";
import {Z_INDEX} from "../../../constant/common/ZIndex";
import {authIdSelector} from "../../../recoil/selector/sign-up/signUpSelector";
import {ERRORS, GET_ERROR_CODE, GET_ERROR_MESSAGE} from "../../../constant/common/Error";
import {postChangePasswordMutate} from "../../../api/myApi";
import {MainDialogLayout} from "../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../layouts/my/MyHeaderDialogLayout";
import ChromeHiddenIdPw from "../../../components/common/ChromeHiddenIdPw";
import {isValidateParam, passwordSchema} from "../../../utils/validateUtils";
import TextFieldWrapper from "../../../components/common/TextFieldWrapper";

export default function ChangePasswordSection({memberInfo}) {
  // 8~15자 영문(대문자 or 소문자), 숫자, 특수문자 필수 포함
  const regex = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/
  // Password 정보 state
  const id = useRecoilValue(authIdSelector)
  const [curPw, setCurPw] = useState('')
  const [newPw, setNewPw] = useState('')
  const [newConfirmPw, setNewConfirmPw] = useState('')
  // 입력 상태 state
  const [isCurTrue, setIsCurTrue] = useState(false)
  const [isNewTrue, setIsNewTrue] = useState(false)
  const [isNewConfirmTrue, setIsNewConfirmTrue] = useState(false)
  const [helperText, setHelperText] = useState(CHANGE_PASSWORD.HELPER_TEXT)
  const [isRight, setIsRight] = useState(true)

  const [curPwCancelIcon, setCurPwCancelIcon] = useState(false)
  const [newPwCancelIcon, setNewPwCancelIcon] = useState(false)
  const [newConfirmPwCancelIcon, setNewConfirmPwCancelIcon] = useState(false)

  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  const [openChangePw, setOpenChangePw] = useRecoilState(myOpenChangePwSelector)

  const {mutate: mutatePostChangePassword} = postChangePasswordMutate()

  const handleBackButton = () => {
    setCurPw('')
    setNewPw('')
    setNewConfirmPw('')
    setOpenChangePw(false)
  }

  // 값 변경 제어
  const handleChangeCurrentPw = (e) => setCurPw(e.target.value.substring(0, 15))
  const handleChangeNewPw = (e) => setNewPw(e.target.value.substring(0, 15))
  const handleChangeNewConfirmPw = (e) => setNewConfirmPw(e.target.value.substring(0, 15))

  // Password 정규식 체크
  const checkPasswordValidation = () => {
    // 8~15자 영문(대문자 or 소문자), 숫자, 특수문자 필수 포함
    const regex = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/

    // 모두 비었으면 isRight true
    // 하나라도 비어있으면 isRight false
    // 현재 비밀번호 정규식 검사
    // 새로운 비밀번호 정규식 검사
    // 새로운 비밀번호 확인 일치 검사

    if (curPw.length === 0 && newPw.length === 0 && newConfirmPw.length === 0) {
      setIsRight(true)
      setIsBottomBtnDisabled(true)
    } else {
      isValidateParam(curPw, passwordSchema).then((isVal) => {
        if (isVal) {
          setIsCurTrue(true)
          setHelperText('')
          setIsRight(true)

          isValidateParam(newPw, passwordSchema).then((isVal) => {
            if (isVal) {
              setIsNewTrue(true)
              setHelperText('')
              checkNewConfirmPwValidation(newConfirmPw)
            } else {
              setIsNewTrue(false)
              setHelperText(CHANGE_PASSWORD.HELPER_TEXT)
              setIsRight(false)
              setIsBottomBtnDisabled(true)
            }
          })
        } else {
          setIsCurTrue(false)
          setHelperText(CHANGE_PASSWORD.HELPER_TEXT)
          setIsRight(false)
          setIsBottomBtnDisabled(true)
        }
      })
    }
  }
  const checkNewConfirmPwValidation = (value) => {
    // 8~15자 영문(대문자 or 소문자), 숫자, 특수문자 필수 포함
    const regex = /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/
    // 앞에서 입력한 비밀번호와 일치하는지 확인
    isValidateParam(newPw, passwordSchema).then((isVal) => {
      if (isVal) {
        if (newPw === value) {
          setIsNewConfirmTrue(true)
          setHelperText('')
          setIsRight(true)
          setIsBottomBtnDisabled(false)
        } else {
          setIsNewConfirmTrue(false)
          setHelperText(CHANGE_PASSWORD.WRONG)
          setIsRight(false)
          setIsBottomBtnDisabled(true)
        }
      } else {
        setIsNewConfirmTrue(false)
        setHelperText(CHANGE_PASSWORD.HELPER_TEXT)
        setIsRight(false)
        setIsBottomBtnDisabled(true)
      }
    })
  }

  // Current Password 입력 값 취소 버튼 제어
  const handleFocusCurPw = () => setCurPwCancelIcon(true)
  const handleBlurCurPw = () => setCurPwCancelIcon(false)
  const handleCancelCurPw = () => setCurPw('')
  // New Password 입력 값 취소 버튼 제어
  const handleFocusNewPw = () => setNewPwCancelIcon(true)
  const handleBlurNewPw = () => setNewPwCancelIcon(false)
  const handleCancelNewPw = () => setNewPw('')
  // New Confirm Password 확인 입력 값 취소 버튼 제어
  const handleFocusNewConfirmPw = () => setNewConfirmPwCancelIcon(true)
  const handleBlurNewConfirmPw = () => setNewConfirmPwCancelIcon(false)
  const handleCancelNewConfirmPw = () => setNewConfirmPw('')

  const handleConfirmClick = () => {
    /* API : 비밀번호 변경하기 */
    mutatePostChangePassword({
      id: memberInfo?.id,
      password: curPw,
      newPassword: newPw,
    }, {
      onSuccess: (result) => {
        setCurPw('')
        setNewPw('')
        setNewConfirmPw('')
        setOpenChangePw(false)
      },
      onError: (error) => {
        switch (GET_ERROR_CODE(error)) {
          case ERRORS.PASSWORD_NOT_MATCH:
          case ERRORS.PASSWORD_INCLUDE_PERSONAL_INFORMATION:
            setIsRight(false)
            setHelperText(GET_ERROR_MESSAGE(error))
            break
        }
      }
    })
  }

  useEffect(() => {
    checkPasswordValidation()
  }, [curPw, newPw, newConfirmPw])

  return (
    <MainDialogLayout
      fullScreen
      open={openChangePw}
      onClose={handleBackButton}
      TransitionComponent={Fade}
      sx={{'& .MuiDialog-paper': {mx: 0, height: '100%', backgroundColor: 'white'}, zIndex: Z_INDEX.DIALOG}}
    >
      <MyHeaderDialogLayout
        title={CHANGE_PASSWORD.TITLE}
        onBack={handleBackButton}
      >
        <Container maxWidth={'xs'} disableGutters sx={{px: '20px'}}>
          <ChromeHiddenIdPw/>
          <TextFieldWrapper
            autoComplete='off'
            fullWidth
            value={curPw}
            label={curPw === '' ? CHANGE_PASSWORD.CURRENT : ' '}
            type="password"
            onChange={handleChangeCurrentPw}
            onFocus={handleFocusCurPw}
            onBlur={handleBlurCurPw}
            variant="standard"
            inputProps={{tabIndex: 1}}
            InputProps={{
              endAdornment: (
                curPw === '' ?
                  null
                  :
                  <IconButton onClick={handleCancelCurPw} sx={{p: 0}}>
                    <CancelIcon
                      sx={{fontSize: '19.3px', color: '#CCCCCC', visibility: curPwCancelIcon ? 'visible' : 'hidden'}}/>
                  </IconButton>
              ),
              style: {
                paddingBottom: '11px',
                fontStyle: 'normal',
                fontWeight: 400,
                fontSize: '16px',
                lineHeight: '22px',
                letterSpacing: '-0.03em',
                color: '#222222',
              },
            }}
            InputLabelProps={{
              shrink: false,
              style: {
                fontStyle: 'normal',
                fontWeight: 500,
                fontSize: '16px',
                lineHeight: '22px',
                letterSpacing: '-0.03em',
                color: '#CCCCCC',
              }
            }}
            sx={{
              marginTop: '10px',
              '& .MuiInput-root:before': {
                borderBottom: curPw === '' ? '1px solid #CCCCCC' : isCurTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
              },
              '& .MuiInput-root:hover:before': {
                borderBottom: curPw === '' ? '1px solid #CCCCCC' : isCurTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
              },
              '& .MuiInput-root:hover:after': {
                borderBottom: curPw === '' ? '1px solid #222222' : isCurTrue ? '1px solid #222222' : '1px solid #FF4842',
              },
              '& .MuiInput-root:hover.Mui-disabled:before': {
                borderBottom: curPw === '' ? '1px solid #222222' : isCurTrue ? '1px solid #222222' : '1px solid #FF4842',
              },
              '& .MuiInput-root:after': {
                borderBottom: curPw === '' ? '1px solid #222222' : isCurTrue ? '1px solid #222222' : '1px solid #FF4842',
              },
            }}
          />
          <TextFieldWrapper
            autoComplete='off'
            fullWidth
            value={newPw}
            label={newPw === '' ? CHANGE_PASSWORD.NEW : ' '}
            type="password"
            onChange={handleChangeNewPw}
            onFocus={handleFocusNewPw}
            onBlur={handleBlurNewPw}
            variant="standard"
            inputProps={{tabIndex: 1}}
            InputProps={{
              endAdornment: (
                newPw === '' ?
                  null
                  :
                  <IconButton onClick={handleCancelNewPw} sx={{p: 0}}>
                    <CancelIcon
                      sx={{fontSize: '19.3px', color: '#CCCCCC', visibility: newPwCancelIcon ? 'visible' : 'hidden'}}/>
                  </IconButton>
              ),
              style: {
                paddingBottom: '11px',
                fontStyle: 'normal',
                fontWeight: 400,
                fontSize: '16px',
                lineHeight: '22px',
                letterSpacing: '-0.03em',
                color: '#222222',
              },
            }}
            InputLabelProps={{
              shrink: false,
              style: {
                fontStyle: 'normal',
                fontWeight: 500,
                fontSize: '16px',
                lineHeight: '22px',
                letterSpacing: '-0.03em',
                color: '#CCCCCC',
              }
            }}
            sx={{
              marginTop: '10px',
              '& .MuiInput-root:before': {
                borderBottom: newPw === '' ? '1px solid #CCCCCC' : isNewTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
              },
              '& .MuiInput-root:hover:before': {
                borderBottom: newPw === '' ? '1px solid #CCCCCC' : isNewTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
              },
              '& .MuiInput-root:hover:after': {
                borderBottom: newPw === '' ? '1px solid #222222' : isNewTrue ? '1px solid #222222' : '1px solid #FF4842',
              },
              '& .MuiInput-root:hover.Mui-disabled:before': {
                borderBottom: newPw === '' ? '1px solid #222222' : isNewTrue ? '1px solid #222222' : '1px solid #FF4842',
              },
              '& .MuiInput-root:after': {
                borderBottom: newPw === '' ? '1px solid #222222' : isNewTrue ? '1px solid #222222' : '1px solid #FF4842',
              },
            }}
          />
          <TextFieldWrapper
            autoComplete='off'
            fullWidth
            value={newConfirmPw}
            type="password"
            label={newConfirmPw === '' ? CHANGE_PASSWORD.NEW_CONFIRM : ' '}
            onChange={handleChangeNewConfirmPw}
            onFocus={handleFocusNewConfirmPw}
            onBlur={handleBlurNewConfirmPw}
            variant="standard"
            inputProps={{tabIndex: 1}}
            InputProps={{
              endAdornment: (
                newConfirmPw === '' ?
                  null
                  :
                  <IconButton onClick={handleCancelNewConfirmPw} sx={{p: 0}}>
                    <CancelIcon
                      sx={{
                        fontSize: '19.3px',
                        color: '#CCCCCC',
                        visibility: newConfirmPwCancelIcon ? 'visible' : 'hidden'
                      }}/>
                  </IconButton>
              ),
              style: {
                paddingBottom: '11px',
                fontStyle: 'normal',
                fontWeight: 400,
                fontSize: '16px',
                lineHeight: '22px',
                letterSpacing: '-0.03em',
                color: '#222222',
              },
            }}
            InputLabelProps={{
              shrink: false,
              style: {
                fontStyle: 'normal',
                fontWeight: 500,
                fontSize: '16px',
                lineHeight: '22px',
                letterSpacing: '-0.03em',
                color: '#CCCCCC',
              }
            }}
            sx={{
              marginTop: '10px',
              '& .MuiInput-root:before': {
                borderBottom: newConfirmPw === '' ? '1px solid #CCCCCC' : isNewConfirmTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
              },
              '& .MuiInput-root:hover:before': {
                borderBottom: newConfirmPw === '' ? '1px solid #CCCCCC' : isNewConfirmTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
              },
              '& .MuiInput-root:hover:after': {
                borderBottom: newConfirmPw === '' ? '1px solid #222222' : isNewConfirmTrue ? '1px solid #222222' : '1px solid #FF4842',
              },
              '& .MuiInput-root:hover.Mui-disabled:before': {
                borderBottom: newConfirmPw === '' ? '1px solid #222222' : isNewConfirmTrue ? '1px solid #222222' : '1px solid #FF4842',
              },
              '& .MuiInput-root:after': {
                borderBottom: newConfirmPw === '' ? '1px solid #222222' : isNewConfirmTrue ? '1px solid #222222' : '1px solid #FF4842',
              },
            }}
          />
          <Typography
            sx={{
              mt: '8px',
              fontStyle: 'normal',
              fontWeight: 400,
              fontSize: '12px',
              lineHeight: '18px',
              letterSpacing: '-0.03em',
              color: isRight ? '#888888' : '#FF4842',
            }}
          >
            {helperText}
          </Typography>

          <DonotsBottomButton tabIndex={1} handleClick={handleConfirmClick} buttonText={'확인'}/>

        </Container>
      </MyHeaderDialogLayout>
    </MainDialogLayout>
  );
}
