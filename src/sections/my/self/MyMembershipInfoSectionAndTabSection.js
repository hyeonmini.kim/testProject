import {Box, Divider, IconButton, Skeleton, Stack, Tab, Tabs, Typography} from "@mui/material";
import {MY_COMMON, MY_TABS, RECIPE_STATUS_TEXT} from "../../../constant/my/MyConstants";
import RecipeItem from "../../../components/my/RecipeItem";
import {useRecoilState, useSetRecoilState} from "recoil";
import {
  myActiveTabSelector,
  myIsExpandedAccordionSelector,
  myIsSelectedFilterSelector,
  myScrapListSelector,
  mySelectedFilterSelector,
  myWriteListSelector,
} from "../../../recoil/selector/my/information/myStateSelector";
import {useEffect, useState} from "react";
import {myFilterBottomDialogState,} from "../../../recoil/atom/my/information/myBottomDialog";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {
  getGradeInformation,
  getMyInfo,
  getSearchMyRecipes,
  getSearchMyScrapRecipesMutate,
  getSearchMyWriteRecipesMutate
} from "../../../api/myApi";
import {
  myOpenChildrenSelector,
  myOpenMomStarSelector,
  myOpenMyInformationPopupSelector
} from "../../../recoil/selector/my/popup/myPopupSelector";
import {PAGINATION} from "../../../constant/common/Pagination";
import MyMembershipInfoSection from "./MyMembershipInfoSection";
import usePagination from "../../../hooks/usePagination";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {RECIPE_STATUS} from "../../../constant/recipe/Recipe";
import {STACK} from "../../../constant/common/StackNavigation";
import {getRecipeDetailMutate} from "../../../api/detailApi";
import useStackNavigation from "../../../hooks/useStackNavigation";
import {createTemporaryRecipeSelector} from "../../../recoil/selector/recipe/createSelector";
import {getURIString} from "../../../utils/formatString";
import LoadingScreen from "../../../components/common/LoadingScreen";
import dynamic from "next/dynamic";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {clickBorderNone, TOAST_TYPE} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ERRORS, GET_FRONT_ERROR_MESSAGE} from "../../../constant/common/Error";
import {toastMessageSelector} from "../../../recoil/selector/common/toastSelector";

const MyFilterBottomDialog = dynamic(() =>
    import("./my-information/MyFilterBottomDialog"), {
    ssr: false, loading: () => <LoadingScreen/>
  }
);

export default function MyMembershipInfoSectionAndTabSection({mainRef}) {
  const [selectedFilter, setSelectedFilter] = useRecoilState(mySelectedFilterSelector);

  const [open, setOpen] = useRecoilState(myOpenMyInformationPopupSelector);
  const [openMomStar, setOpenMomStar] = useRecoilState(myOpenMomStarSelector);
  const [openChildren, setOpenChildren] = useRecoilState(myOpenChildrenSelector)

  const {data: userRecipeInfo} = getSearchMyRecipes({
    params: {
      recipe_check_status: selectedFilter
    }
  })

  const {data: userGradeInfo, isLoading: isLoadingUserGradeInfo} = getGradeInformation()

  return (
    <>
      {
        (!open && !openMomStar && !openChildren) &&
        <>
          <MyMembershipInfoSection userGradeInfo={userGradeInfo}/>
          <Divider sx={{px: '0px', mt: '20px', height: '6px', background: '#F5F5F5', borderBottomWidth: 'inherit'}}/>
          <MyTabSection userRecipeInfo={userRecipeInfo} mainRef={mainRef}/>
        </>
      }
      <MyFilterBottomDialog/>
      {isLoadingUserGradeInfo && <LoadingScreen/>}
    </>
  )
}

function MyTabSection({userRecipeInfo, mainRef}) {
  const [tempRecipe, setTempRecipe] = useRecoilState(createTemporaryRecipeSelector)
  const [isExpanded, setIsExpanded] = useRecoilState(myIsExpandedAccordionSelector);
  const [activeTab, setActiveTab] = useRecoilState(myActiveTabSelector);
  const [isOpen, setIsOpen] = useRecoilState(myFilterBottomDialogState);
  const [isSelectedFilter, setIsSelectedFilter] = useRecoilState(myIsSelectedFilterSelector);

  const [isLoading, setIsLoading] = useState(false)

  const setToastMessage = useSetRecoilState(toastMessageSelector)

  const handleFilterClick = () => {
    setIsOpen(true);
  }

  const [selectedFilter, setSelectedFilter] = useRecoilState(mySelectedFilterSelector);

  const {
    param: paramWrite,
    items: itemsWrite,
    setItems: setItemsWrite,
    setTarget: setTargetWrite,
    pageOffset: pageOffsetWrite,
    hasNextOffset: hasNextOffsetWrite,
    initPagination: initPaginationWrite,
    isLoading: isLoadingWrite,
    getPaginationItems: getPaginationItemsWrite,
  } = usePagination(getSearchMyWriteRecipesMutate, PAGINATION.PAGE_SIZE)

  paramWrite.current = {
    params: {
      recipe_check_status: selectedFilter,
      paging_type: MY_COMMON.MY_TAB_WRITE
    }
  }

  const {
    param: paramScrap,
    items: itemsScrap,
    setItems: setItemsScrap,
    setTarget: setTargetScrap,
    pageOffset: pageOffsetScrap,
    hasNextOffset: hasNextOffsetScrap,
    initPagination: initPaginationScrap,
    isLoading: isLoadingScrap,
    getPaginationItems: getPaginationItemsScrap,
  } = usePagination(getSearchMyScrapRecipesMutate, PAGINATION.PAGE_SIZE)

  paramScrap.current = {
    params: {
      paging_type: MY_COMMON.MY_TAB_SCRAP
    }
  }

  const [myWriteList, setMyWriteList] = useRecoilState(myWriteListSelector);
  const [myScrapList, setMyScrapList] = useRecoilState(myScrapListSelector);

  useEffect(() => {
    setMyWriteList(itemsWrite)
    setMyScrapList(itemsScrap)
  }, [itemsWrite, itemsScrap]);


  const {mutate: mutateTempRecipe} = getRecipeDetailMutate()
  const {navigation, preFetch} = useStackNavigation()

  useEffect(() => {
    const fetch = preFetch(STACK.MY_SELF.TYPE)
    if (fetch) {
      if (fetch.activeTab === MY_COMMON.MY_TAB_WRITE) {
        initPaginationWrite(fetch?.myWriteList)
      }
      if (fetch.activeTab === MY_COMMON.MY_TAB_SCRAP) {
        initPaginationScrap(fetch?.myScrapList)
      }
    } else {
      if (activeTab === MY_COMMON.MY_TAB_WRITE) {
        getPaginationItemsWrite()
      }
      if (activeTab === MY_COMMON.MY_TAB_SCRAP) {
        getPaginationItemsScrap()
      }
    }
  }, [activeTab, selectedFilter])

  const handlePushNavigation = (linkTo) => {
    setTimeout(() => {
      setIsLoading(false)
      navigation.push(STACK.MY_SELF.TYPE, linkTo, {
        ...STACK.MY_SELF.DATA,
        activeTab: activeTab,
        isExpanded: isExpanded,
        myWriteList: itemsWrite,
        myScrapList: itemsScrap,
        selectedFilter: selectedFilter,
        isSelectedFilter: isSelectedFilter,
        scrollY: mainRef?.current?.scrollTop
      })
    }, 500);
  }

  const handlePushError = (error) => {
    setIsLoading(false)
    setToastMessage({
      type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
      message: GET_FRONT_ERROR_MESSAGE(ERRORS.ECONNABORTED)
    })
  }

  const handleClick = (recipe_check_status, recipe_user_name, recipe_name, recipe_key) => {
    setIsLoading(true)
    let linkTo = PATH_DONOTS_PAGE.RECIPE.CREATE
    switch (recipe_check_status) {
      case RECIPE_STATUS_TEXT.TEMPORARY:
        mutateTempRecipe({
          recipe_key: recipe_key
        }, {
          onSuccess: (data) => {
            setTempRecipe({
              status: RECIPE_STATUS.TEMPORARY,
              ...data
            })
            handlePushNavigation(linkTo)
          },
          onError: (error) => {
            handlePushError(error)
          }
        })
        break
      case RECIPE_STATUS_TEXT.EXAMINATION:
        mutateTempRecipe({
          recipe_key: recipe_key
        }, {
          onSuccess: (data) => {
            setTempRecipe({
              status: RECIPE_STATUS.EXAMINATION,
              ...data
            })
            handlePushNavigation(linkTo)
          },
          onError: (error) => {
            handlePushError(error)
          }
        })
        break
      case RECIPE_STATUS_TEXT.REJECT:
        mutateTempRecipe({
          recipe_key: recipe_key
        }, {
          onSuccess: (data) => {
            setTempRecipe({
              status: RECIPE_STATUS.REJECT,
              ...data
            })
            handlePushNavigation(linkTo)
          },
          onError: (error) => {
            handlePushError(error)
          }
        })
        break
      case RECIPE_STATUS_TEXT.POSTING:
        linkTo = PATH_DONOTS_PAGE.RECIPE.DETAIL(recipe_user_name, getURIString(recipe_name), recipe_key)
        handlePushNavigation(linkTo)
        break
      default:
        break
    }
  }

  return (
    <>
      <TabSection userRecipeInfo={userRecipeInfo}/>

      <Box sx={{px: '20px', mt: activeTab === MY_COMMON.MY_TAB_WRITE ? '10px' : '20px'}}>
        {activeTab === MY_COMMON.MY_TAB_WRITE &&
          <Filter handleFilterClick={handleFilterClick} isSelectedFilter={isSelectedFilter}/>}

        {(isLoadingWrite || isLoadingScrap || isLoading) && <LoadingScreen/>}

        {
          activeTab === MY_COMMON.MY_TAB_WRITE && (
            itemsWrite === PAGINATION.INIT_DATA
              ? <TabSkeleton/>
              : itemsWrite?.length ? (
                <>
                  <TabSectionItems items={itemsWrite} onClick={handleClick} activeTab={activeTab}/>
                  <Box alignItems={'center'} sx={{mt: '12px', height: '67px',}}/>
                  {hasNextOffsetWrite.current && <Box ref={setTargetWrite}/>}
                </>
              ) : <NoItems activeTab={activeTab} selectedFilter={selectedFilter}/>
          )
        }
        {
          activeTab === MY_COMMON.MY_TAB_SCRAP && (
            itemsScrap === PAGINATION.INIT_DATA
              ? <TabSkeleton/>
              : itemsScrap?.length ? (
                <>
                  <TabSectionItems items={itemsScrap} onClick={handleClick} activeTab={activeTab}/>
                  <Box alignItems={'center'} sx={{mt: '12px', height: '67px',}}/>
                  {hasNextOffsetScrap.current && <Box ref={setTargetScrap}/>}
                </>
              ) : <NoItems activeTab={activeTab}/>
          )
        }
      </Box>
    </>
  )
}

function TabSection({userRecipeInfo}) {
  const [activeTab, setActiveTab] = useRecoilState(myActiveTabSelector);

  const handleTabClick = (value) => {
    if (value === "write") {
      setHackleTrack(HACKLE_TRACK.MY_TAB_RECIPE)
    } else if (value === "scrap") {
      setHackleTrack(HACKLE_TRACK.MY_TAB_SCRAP)
    }
    setActiveTab(value);
  };

  const getCount = (value) => {
    if (value === MY_COMMON.MY_TAB_WRITE) {
      return userRecipeInfo?.recipe_writer_cnt || 0
    } else {
      return userRecipeInfo?.recipe_scrap_cnt || 0
    }
  }

  return (
    <Box sx={{height: '48px', borderBottom: '2px solid #F5F5F5'}}>
      <Tabs
        sx={{'& .MuiTabs-indicator': {backgroundColor: 'black'}, '& .Mui-selected': {color: 'black'}}}
        variant="fullWidth"
        value={activeTab}
        onChange={(event, newValue) => handleTabClick(newValue)}
      >
        {MY_TABS.slice(0, 2).map((tab, index) => (
          <Tab
            tabIndex={7}
            key={tab.value}
            value={tab.value}
            label={
              <Stack direction="row" spacing="8px">
                <Typography fontWeight="400" fontSize="16px" sx={{lineHeight: '16px', color: '#000000'}}>
                  {tab.label}
                </Typography>
                <Typography fontWeight="400" fontSize="16px" sx={{lineHeight: '16px', color: '#888888'}}>
                  {getCount(tab.value)}
                </Typography>
              </Stack>
            }
            sx={{'&:not(:last-of-type)': {marginRight: '0px',}, ...clickBorderNone}}
          />
        ))}
      </Tabs>
    </Box>
  )
}

function TabSkeleton() {
  return (
    <>
      {[...Array(10)].map((item, index) =>
        <SkeletonItem key={index}/>
      )}
    </>
  );
}

function TabSectionItems({items, onClick, activeTab}) {
  return (
    <>
      {
        items?.map((item, index) =>
          <RecipeItem key={index} item={{...item}} isLazy={index < 2 ? false : true} onClick={onClick} activeTab={activeTab}/>
        )
      }
    </>
  )
}


function SkeletonItem() {
  return (
    <Stack direction="row" spacing="12px" sx={{my: '12px'}}>
      <Box sx={{position: 'relative'}}>
        <Skeleton variant="rounded" sx={{borderRadius: '8px', width: '124px', height: '124px'}}/>
      </Box>
      <Box sx={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between', flexGrow: 1}}>
        <Skeleton sx={{height: '24px'}}/>
        <Skeleton sx={{height: '20px'}}/>
        <Box sx={{flexGrow: 1}}/>
        <Stack direction="row" alignItems={'center'}>
          <Skeleton variant="rectangular" sx={{width: '50px', height: '16px', mr: '15px', borderRadius: 1}}/>
          <Skeleton variant="rectangular" sx={{width: '50px', height: '16px', borderRadius: 1}}/>
        </Stack>
      </Box>
    </Stack>
  );
}

function NoItems({activeTab, selectedFilter}) {
  return (
    <Box
      sx={{
        height: getNoItemHeight(activeTab),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        pb: '70px',
      }}
    >
      <Typography align={'center'} sx={{fontSize: '16px', fontWeight: 500, color: '#666666', lineHeight: '24px'}}>
        {getNoItemTextTitle(activeTab, selectedFilter)}
      </Typography>
      <Typography align={'center'} sx={{fontSize: '14px', fontWeight: 400, color: '#888888', lineHeight: '20px'}}>
        {getNoItemTextSub(activeTab)}
      </Typography>
    </Box>
  )
}

function getNoItemTextTitle(activeTab, selectedFilter) {
  if (activeTab === MY_COMMON.MY_TAB_WRITE) {
    if (selectedFilter !== '전체') {
      return MY_COMMON.RECIPE_NO_ITEM_FILTER
    } else {
      return MY_COMMON.RECIPE_NO_ITEM
    }
  } else {
    return MY_COMMON.SCRAP_NO_ITEM
  }
}

function getNoItemTextSub(activeTab) {
  if (activeTab === MY_COMMON.MY_TAB_WRITE) {
    return MY_COMMON.RECIPE_NO_ITEM_SUB
  } else {
    return MY_COMMON.SCRAP_NO_ITEM_SUB
  }
}

function getNoItemHeight(activeTab) {
  if (activeTab === MY_COMMON.MY_TAB_WRITE) {
    return 'calc(100vh - 586px)'
  } else {
    return 'calc(100vh - 536px)'
  }
}

function Filter({handleFilterClick, isSelectedFilter}) {
  return (
    <>
      <Stack
        direction="row"
        onClick={handleFilterClick}
        sx={{
          alignItems: 'center',
          mb: '14px'
        }}
      >
        <IconButton
          tabIndex={-1}
          disableFocusRipple
          sx={{
            m: '-8px',
            backgroundColor: "#FFFFFF",
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: '#FFFFFF',
            },
          }}>
          <SvgCommonIcons
            tabIndex={8}
            type={ICON_TYPE.FILTER}
            sx={{...clickBorderNone}}
            onKeyDown={(e) => handleEnterPress(e, handleFilterClick)}
          />
        </IconButton>
        <Typography fontWeight="400" fontSize="16px" sx={{ml: '8px', lineHeight: '16px', color: '#888888'}}>
          {MY_COMMON.RECIPE_FILTER}
        </Typography>
        {isSelectedFilter &&
          <SvgCommonIcons type={ICON_TYPE.CHECK_FILLED_24PX}
                          sx={{mt: '1px', ml: '2px', width: '20px', height: '20px'}}/>}
      </Stack>
    </>
  )
}