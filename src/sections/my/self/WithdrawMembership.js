import {Box, Button, Container, Fade, FormControlLabel, IconButton, InputAdornment, Radio, RadioGroup, Typography} from "@mui/material";
import * as React from "react";
import {useEffect, useState} from "react";
import {MY_BTN_TEXT, MY_COMMON, MY_WITHDRAW_REASON_LIST} from "../../../constant/my/MyConstants";
import {useRecoilState, useRecoilValue, useResetRecoilState, useSetRecoilState} from "recoil";
import {myInformationFooterBottomButtonTextState} from "../../../recoil/atom/my/information/footer";
import {dialogSelector} from "../../../recoil/selector/common/dialogSelector";
import {clickBorderNone, COMMON_DIALOG_TYPE} from "../../../constant/common/Common";
import {useBefuAuthContext} from "../../../auth/useAuthContext";
import {useSnackbar} from "../../../components/snackbar";
import {myOpenMyInformationPopupSelector, myOpenWithdrawMembershipSelector} from "../../../recoil/selector/my/popup/myPopupSelector";
import {Z_INDEX} from "../../../constant/common/ZIndex";
import {gnbActiveTabSelector} from "../../../recoil/selector/common/gnbSelector";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {authAccountKeySelector} from "../../../recoil/selector/sign-up/authSelector";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {useRouter} from "next/router";
import {deleteWithdrawAccountMutate} from "../../../api/myApi";
import {MainDialogLayout} from "../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../layouts/my/MyHeaderDialogLayout";
import {resetTokens} from "../../../auth/utils";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {accessTokenSelector, refreshTokenSelector} from "../../../recoil/selector/auth/accessTokenSelector";
import TextFieldWrapper from "../../../components/common/TextFieldWrapper";
import {ALT_STRING} from "../../../constant/common/AltString";

export default function WithdrawMembership({userInfo}) {
  const [value, setValue] = useState('');
  const [leaveType, setLeaveType] = useState(0)
  const [etcReason, setEtcReason] = useState('');
  const [isTrue, setIsTrue] = useState(true);
  const [cancelIcon, setCancelIcon] = useState(false);
  const bottomBtnText = useRecoilValue(myInformationFooterBottomButtonTextState)
  const setDialogMessage = useSetRecoilState(dialogSelector)
  const [openWithdrawMembership, setOpenWithdrawMembership] = useRecoilState(myOpenWithdrawMembershipSelector)
  const [open, setOpen] = useRecoilState(myOpenMyInformationPopupSelector);
  const {logout} = useBefuAuthContext()
  const {enqueueSnackbar} = useSnackbar()
  const [activeTab, setActiveTab] = useRecoilState(gnbActiveTabSelector);
  const [accountKey, setAccountKey] = useRecoilState(authAccountKeySelector)
  const [checkedReason, setCheckedReason] = useState([false, false, false, false, false])

  const router = useRouter()
  const {mutate: mutateDeleteWithdrawAccount} = deleteWithdrawAccountMutate()

  const resetAccessToken = useResetRecoilState(accessTokenSelector);
  const resetRefreshToken = useResetRecoilState(refreshTokenSelector);

  useEffect(() => {
    if (!openWithdrawMembership) {
      setLeaveType(0)
      setCheckedReason([false, false, false, false, false])
      setValue('')
      setEtcReason('')
    }
  }, [openWithdrawMembership])

  const handleBackButton = () => {
    setOpenWithdrawMembership(false)
  }

  const handleConfirmClick = () => {
    setDialogMessage({
      type: COMMON_DIALOG_TYPE.TWO_BUTTON,
      message: MY_COMMON.TEXT.WITHDRAW_POPUP_MESSAGE,
      leftButtonProps: {text: MY_BTN_TEXT.CANCEL},
      rightButtonProps: {text: MY_BTN_TEXT.WITHDRAW, onClick: handleClickWithdraw},
    })
    // setValue('')
    // setOpenWithdrawMembership(false)
  }

  // 회원 탈퇴 처리
  const handleClickWithdraw = async () => {
    try {
      mutateDeleteWithdrawAccount({
        accountKey: userInfo?.accountKey,
        body: {
          leaveType: parseInt(leaveType),
          anotherText: etcReason
        }
      }, {
        onSuccess: (result) => {
          resetTokens()
          resetAccessToken()
          resetRefreshToken()
          logout()
          handleBackButton()
          setOpen(false)
          setActiveTab('')

          router.replace({pathname: PATH_DONOTS_PAGE.GUEST})
        }
      })
    } catch (error) {
      console.error(error)
      enqueueSnackbar('Unable to logout!', {variant: 'error'})
    }
  }

  const handleChange = (event) => {
    setLeaveType(event.target.labels[0]?.id)
    setValue(event.target.value)
  };

  const handleChangeValue = (e) => {
    const value = e.target.value.toLowerCase().substring(0, 30);

    setEtcReason(value);
  }

  const handleCheckedReason = (event, index, item) => {
    let reasonArray = [false, false, false, false, false]
    reasonArray[index] = true
    setCheckedReason(reasonArray)

    setLeaveType(index)
    setValue(item)
  }

  useEffect(() => {
    if (value !== MY_WITHDRAW_REASON_LIST[4]) {
      setEtcReason('');
    }
  }, [value])

  const handleFocus = () => setCancelIcon(true);
  const handleBlur = () => setCancelIcon(false);
  const handleCancel = () => setEtcReason('');

  return (
    <MainDialogLayout
      fullScreen
      open={openWithdrawMembership}
      onClose={handleBackButton}
      TransitionComponent={Fade}
      sx={{
        '& .MuiDialog-paper': {
          mx: 0,
          height: '100%',
          backgroundColor: 'white'
        },
        zIndex: Z_INDEX.DIALOG
      }}>
      <MyHeaderDialogLayout
        title={MY_COMMON.TEXT.WITHDRAW}
        onBack={handleBackButton}
      >
        <Container maxWidth={'xs'} disableGutters sx={{px: '20px', mb: '100px'}}>
          <Typography fontWeight="700" fontSize="24px"
                      sx={{mt: '30px', mb: '20px', lineHeight: '32px', color: '#000000', whiteSpace: 'pre-wrap'}}>
            {MY_COMMON.TEXT.WITHDRAW_TITLE}
          </Typography>

          <RadioGroup
            aria-labelledby="demo-controlled-radio-buttons-group"
            name="controlled-radio-buttons-group"
            value={value}
            onChange={handleChange}
          >
            {MY_WITHDRAW_REASON_LIST.slice(0, 5).map((item, index) => (
              <FormControlLabel
                tabIndex={1}
                id={index.toString()}
                key={index}
                sx={{my: '14px', ...clickBorderNone}}
                value={item}
                checked={checkedReason[index]}
                onClick={(event) => handleCheckedReason(event, index, item)}
                onKeyDown={(e) => handleEnterPress(e, (event) => handleCheckedReason(event, index, item))}
                control={
                  <Radio
                    disableFocusRipple
                    sx={{width: '24px', height: '24px', ml: '12px'}}
                    icon={
                      <IconButton
                        tabIndex={-1}
                        disableFocusRipple
                        sx={{
                          m: '-8px',
                          backgroundColor: "#FFFFFF",
                          '&:hover': {
                            boxShadow: 'none',
                            backgroundColor: '#FFFFFF',
                          },
                        }}>
                        <SvgCommonIcons type={ICON_TYPE.CHECK_24PX}/>
                      </IconButton>
                    }
                    checkedIcon={
                      <IconButton
                        tabIndex={-1}
                        disableFocusRipple
                        sx={{
                          m: '-8px',
                          backgroundColor: "#FFFFFF",
                          '&:hover': {
                            boxShadow: 'none',
                            backgroundColor: '#FFFFFF',
                          },
                        }}>
                        <SvgCommonIcons type={ICON_TYPE.CHECK_FILLED_24PX}/>
                      </IconButton>
                    }
                  />
                }
                label={
                  <Typography fontWeight="400" fontSize="16px" sx={{lineHeight: '24px', ml: '8px', color: '#000000'}}>
                    {item}
                  </Typography>
                }
              />
            ))}
            {/*<FormControlLabel value="female" control={<Radio />} label="Female" />*/}
            {/*<FormControlLabel value="male" control={<Radio />} label="Male" />*/}
          </RadioGroup>

          <TextFieldWrapper
            autoComplete='off'
            fullWidth
            value={etcReason}
            placeholder={MY_COMMON.TEXT.WITHDRAW_PLACEHOLDER}
            onChange={handleChangeValue}
            onFocus={handleFocus}
            onBlur={handleBlur}
            disabled={value !== MY_WITHDRAW_REASON_LIST[4]}
            variant="standard"
            inputProps={{tabIndex: 1, style: {padding: 0}, title: ALT_STRING.MY.WITHDRAW}}
            InputProps={{
              endAdornment: (
                etcReason !== '' &&
                <InputAdornment position={'end'}>
                  <SvgCommonIcons type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
                </InputAdornment>
              ),
              style: {
                marginTop: '8px',
                paddingBottom: '13px',
                marginBottom: '26px',
                fontStyle: 'normal',
                fontWeight: 400,
                fontSize: '14px',
                lineHeight: '20px',
                color: '#222222',
              },
            }}
            InputLabelProps={{
              shrink: false,
              style: {
                marginTop: '0px',
                fontStyle: 'normal',
                fontWeight: 500,
                fontSize: '14px',
                lineHeight: '20px',
                color: '#CCCCCC',
              }
            }}
            sx={{
              paddingLeft: '32px',
              '& .MuiInput-root:before': {
                borderBottom: etcReason === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
              },
              '& .MuiInput-root:hover:before': {
                borderBottom: etcReason === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
              },
              '& .MuiInput-root:hover:after': {
                borderBottom: etcReason === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
              },
              '& .MuiInput-root:hover.Mui-disabled:after': {
                borderBottom: etcReason === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
              },
              '& .MuiInput-root:after': {
                borderBottom: etcReason === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
              },
            }}
          />
        </Container>

        <Box
          sx={{
            p: '20px',
            position: 'fixed',
            zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
            bgcolor: 'white',
            bottom: 0,
            left: 0,
            right: 0,
          }}
        >
          <Container maxWidth={'xs'} disableGutters>
            <Container
              disableGutters
              sx={{
                mt: '20px',
                maxWidth: 'xs',
              }}
            >
              <Button
                tabIndex={1}
                disableFocusRipple
                fullWidth
                variant="contained"
                disabled={value === '' || (value === MY_WITHDRAW_REASON_LIST[4] && etcReason === '')}
                onClick={handleConfirmClick}
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  p: '14px 100.5px',
                  height: '52px',
                  fontStyle: 'normal',
                  fontWeight: '700',
                  fontSize: '16px',
                  lineHeight: '24px',
                  textAlign: 'center',
                  color: '#FFFFFF',
                  '&.Mui-disabled': {
                    color: '#B3B3B3',
                    backgroundColor: '#E6E6E6',
                  },
                  '&.Mui-disabled:hover': {
                    backgroundColor: '#E6E6E6',
                  },
                  '&:hover': {
                    boxShadow: 'none',
                    backgroundColor: 'primary.main',
                  },
                  ...clickBorderNone
                }}
              >
                {bottomBtnText}
              </Button>
            </Container>
          </Container>
        </Box>
      </MyHeaderDialogLayout>
    </MainDialogLayout>
  )
}
