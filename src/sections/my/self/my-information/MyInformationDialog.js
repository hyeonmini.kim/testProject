import * as React from 'react';
import {forwardRef, useEffect, useState} from 'react';
import {Avatar, Badge, Button, Container, Slide, Typography,} from '@mui/material';
import {useRecoilState, useRecoilValue, useResetRecoilState, useSetRecoilState} from "recoil";
import {MY_COMMON, MY_GRADE_TYPE} from "../../../../constant/my/MyConstants";
import MyNicknameInputForm from "./input-form/MyNicknameInputForm";
import MyEmailInputForm from "./input-form/MyEmailInputForm";
import MyBirthInputForm from "./input-form/MyBirthInputForm";
import MyPhoneNumberInputForm from "./input-form/MyPhoneNumberInputForm";
import MySnsLinkInputForm from "./input-form/MySnsLinkInputForm";
import MyIntroductionInputForm from "./input-form/MyIntroductionInputForm";
import {
  myInformationFooterBottomButtonState,
  myInformationFooterBottomButtonTextState
} from "../../../../recoil/atom/my/information/footer";
import GalleryContents from "./GalleryContents";
import MyBottomDialog from "./MyBottomDialog";
import {myInformationBottomDialogState} from "../../../../recoil/atom/my/information/myBottomDialog";
import MySnsAccount from "./MySnsAccount";
import ChangePasswordSection from "../ChangePasswordSection";
import ChangeMyPhoneNumberDialog from "./ChangeMyPhoneNumberDialog";
import WithdrawMembership from "../WithdrawMembership";
import {useBefuAuthContext} from "../../../../auth/useAuthContext";
import {useSnackbar} from "../../../../components/snackbar";
import {
  myOpenChangePhoneSelector,
  myOpenChangePwSelector,
  myOpenMyInformationPopupSelector,
  myOpenSnsSelector,
  myOpenWithdrawMembershipSelector
} from "../../../../recoil/selector/my/popup/myPopupSelector";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {gnbActiveTabSelector} from "../../../../recoil/selector/common/gnbSelector";
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import {
  myEmailDuplicatedState,
  myEmailErrorState,
  myEmailHelperTextState,
  myNicknameDuplicatedState,
  myNicknameErrorState,
  myNicknameHelperTextState
} from "../../../../recoil/atom/my/information/myState";
import {SIGN_UP_EMAIL, SIGN_UP_NICKNAME} from "../../../../constant/sign-up/SignUp";
import {postIsDuplicateEmailMutate, postIsDuplicateNicknameMutate} from "../../../../api/authApi";
import {
  isChangePhoneNumberSelector,
  myDataSelector,
  myIsProfilePictureUrlDeletedSelector,
  myKeySelector,
  tempMyBirthDaySelector,
  tempMyBriefBioSelector,
  tempMyDataSelector,
  tempMyEmailSelector,
  tempMyNicknameSelector,
  tempMyPhoneNumberSelector,
  tempMyProfilePictureUrlSelector,
  tempMySocialMediaUrlSelector
} from "../../../../recoil/selector/my/myDataSelector";
import {delMyProfilePictureMutate, patchMyDetailsMutate, putMyProfilePictureMutate} from "../../../../api/myApi";
import {useRouter} from "next/router";
import {PATH_DONOTS_PAGE} from "../../../../routes/paths";
import {useQueryClient} from "react-query";
import {authIdSelector} from "../../../../recoil/selector/sign-up/signUpSelector";
import {fileData} from "../../../../components/file-thumbnail";
import LoadingScreen from "../../../../components/common/LoadingScreen";
import {getHyphenPhoneNumber, getYYMMDDFromHyphen} from "../../../../utils/formatString";
import {authPhoneNumberState} from "../../../../recoil/atom/sign-up/auth";
import {removeCookie} from "../../../../utils/storageUtils";
import {clickBorderNone, COMMON_STR} from "../../../../constant/common/Common";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../layouts/my/MyHeaderDialogLayout";
import {removeHyphenFromPhoneNumber} from "../../../../utils/authUtils";
import {resetTokens} from "../../../../auth/utils";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";
import {accessTokenSelector, refreshTokenSelector} from "../../../../recoil/selector/auth/accessTokenSelector";
import {ALT_STRING} from "../../../../constant/common/AltString";
import {setHackleTrack} from "../../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../../constant/common/Hackle";
import {USER_GRADE} from "../../../../constant/common/User";
import {USER_INFORM} from "../../../../constant/common/UserInform";

const Transition = forwardRef((props, ref) => <Slide direction="up" ref={ref} {...props} />);

export default function MyInformationDialog({userInfo, memberInfo, setShowChoco}) {
  const [myData, setMyData] = useRecoilState(myDataSelector);
  const key = useRecoilValue(myKeySelector);
  const [tempMyData, setTempMyData] = useRecoilState(tempMyDataSelector);
  const [myIsProfilePictureUrlDeleted, setMyIsProfilePictureUrlDeleted] = useRecoilState(myIsProfilePictureUrlDeletedSelector);
  const [open, setOpen] = useRecoilState(myOpenMyInformationPopupSelector);
  const resetTempMyData = useResetRecoilState(tempMyDataSelector)
  const isBottomBtnDisabled = useRecoilValue(myInformationFooterBottomButtonState)
  const bottomBtnText = useRecoilValue(myInformationFooterBottomButtonTextState)
  const [isOpen, setIsOpen] = useRecoilState(myInformationBottomDialogState);
  const [openSns, setOpenSns] = useRecoilState(myOpenSnsSelector);
  const [openChangePw, setOpenChangePw] = useRecoilState(myOpenChangePwSelector)
  const [openChangePhone, setOpenChangePhone] = useRecoilState(myOpenChangePhoneSelector)
  const [openWithdrawMembership, setOpenWithdrawMembership] = useRecoilState(myOpenWithdrawMembershipSelector)
  const [activeTab, setActiveTab] = useRecoilState(gnbActiveTabSelector);

  const id = useRecoilValue(authIdSelector)
  const [email, setEmail] = useState(userInfo?.email || '')
  const [nickname, setNickname] = useState(userInfo?.nickname || '')
  const [birthDay, setBirthDay] = useState(userInfo?.birthDay || '')
  const [phoneNumber, setPhoneNumber] = useState(userInfo?.phoneNumber || '')
  const [briefBio, setBriefBio] = useState(userInfo?.briefBio || '');
  const [socialMediaUrl, setSocialMediaUrl] = useState(userInfo?.socialMediaUrl || '');
  const [profilePictureUrl, setProfilePictureUrl] = useState(userInfo?.profilePictureUrl || '');

  const [tempEmail, setTempEmail] = useRecoilState(tempMyEmailSelector)
  const [tempNickname, setTempNickname] = useRecoilState(tempMyNicknameSelector)
  const [tempBirthDay, setTempBirthDay] = useRecoilState(tempMyBirthDaySelector)
  const [tempPhoneNumber, setTempPhoneNumber] = useRecoilState(tempMyPhoneNumberSelector)
  const [tempBriefBio, setTempBriefBio] = useRecoilState(tempMyBriefBioSelector);
  const [tempSocialMediaUrl, setTempSocialMediaUrl] = useRecoilState(tempMySocialMediaUrlSelector);
  const [tempProfilePictureUrl, setTempProfilePictureUrl] = useRecoilState(tempMyProfilePictureUrlSelector);

  const [phone, setPhone] = useRecoilState(authPhoneNumberState)
  const [isChangePhoneNumber, setIsChangePhoneNumber] = useRecoilState(isChangePhoneNumberSelector)

  const setEmailHelperText = useSetRecoilState(myEmailHelperTextState)
  const [emailTrue, setEmailTrue] = useRecoilState(myEmailErrorState)
  const [emailDuplicated, setEmailDuplicated] = useRecoilState(myEmailDuplicatedState)
  const setNicknameHelperText = useSetRecoilState(myNicknameHelperTextState)
  const [nicknameTrue, setNicknameTrue] = useRecoilState(myNicknameErrorState)
  const [nicknameDuplicated, setNicknameDuplicated] = useRecoilState(myNicknameDuplicatedState)

  const resetAccessToken = useResetRecoilState(accessTokenSelector);
  const resetRefreshToken = useResetRecoilState(refreshTokenSelector);

  const queryClient = useQueryClient()

  const {
    mutate: mutatePostIsDuplicateNickname,
    isLoading: isLoadingGetIsDuplicateNickname
  } = postIsDuplicateNicknameMutate()
  const {mutate: mutatePostIsDuplicateEmail, isLoading: isLoadingPostIsDuplicateEmail} = postIsDuplicateEmailMutate()
  const {mutate: mutatePatchMyDetails, isLoading: isLoadingPatchMyDetails} = patchMyDetailsMutate()
  const {mutate: mutatePutMyProfilePicture, isLoading: isLoadingPutMyProfilePicture} = putMyProfilePictureMutate()
  const {mutate: mutateDelMyProfilePicture, isLoading: isLoadingDelMyProfilePicture} = delMyProfilePictureMutate()

  const {logout} = useBefuAuthContext()
  const router = useRouter()

  const handleGalleryClick = () => {
    setHackleTrack(HACKLE_TRACK.MY_INFO_PHOTO)
    setIsOpen(true);
  }

  useEffect(() => {
    if (open) {
      setProfilePictureUrl(userInfo?.profilePictureUrl || '')
      setEmail(userInfo?.email || '')
      setNickname(userInfo?.nickname || '')
      setBirthDay(userInfo?.birthDay || '')
      setPhoneNumber(userInfo?.phoneNumber || '')
      setBriefBio(userInfo?.briefBio || '')
      setSocialMediaUrl(userInfo?.socialMediaUrl || '')

      setTempProfilePictureUrl(userInfo?.profilePictureUrl || '')
      setTempEmail(userInfo?.email || '')
      setTempNickname(userInfo?.nickname || '')
      setTempBirthDay(userInfo?.birthDay || '')
      setTempPhoneNumber(userInfo?.phoneNumber || '')
      setTempBriefBio(userInfo?.briefBio || '')
      setTempSocialMediaUrl(userInfo?.socialMediaUrl || '')
      setHackleTrack(HACKLE_TRACK.MY_INFO)
    }

    return () => {
      setMyIsProfilePictureUrlDeleted(false)
    }
  }, [open])

  useEffect(() => {
    if (isChangePhoneNumber) {
      setPhoneNumber(phone)
      setIsChangePhoneNumber(false)
      setPhone('')
    }
  }, [isChangePhoneNumber])

  const {enqueueSnackbar} = useSnackbar()

  const handleBackButton = () => {
    setOpen(false);
    setMyIsProfilePictureUrlDeleted(false);
  };

  const handleClickOpenPopupDialog = () => {
    setOpen(false);
  };

  const handleClickOpenSns = () => {
    setOpenSns(true);
  };

  const completeChangeInform = () => {
    if (userInfo?.type === MY_GRADE_TYPE.NORMAL) {
      if (userInfo?.grade === USER_GRADE.LV1.GRADE) {
        setShowChoco(true)
      }
    }
  }

  const handleConfirmClick = () => {
    let isNicknameChanged = false
    let isEmailChanged = false
    let isPhoneNumberChanged = false
    let isBriefBioChanged = false
    let isSocialMediaUrlChanged = false
    let isProfilePictureUrlChanged = false

    if (tempNickname !== nickname)
      isNicknameChanged = true
    if (tempEmail !== email)
      isEmailChanged = true
    if (tempPhoneNumber !== phoneNumber)
      isPhoneNumberChanged = true
    if (tempBriefBio !== briefBio)
      isBriefBioChanged = true
    if (tempSocialMediaUrl !== socialMediaUrl)
      isSocialMediaUrlChanged = true
    if (tempProfilePictureUrl !== profilePictureUrl)
      isProfilePictureUrlChanged = true

    let param = {}
    let isSuccess = true

    if (tempNickname === nickname && tempEmail === email && tempBriefBio === briefBio && tempSocialMediaUrl === socialMediaUrl && tempProfilePictureUrl === profilePictureUrl && tempPhoneNumber === phoneNumber) {
      setOpen(false)
      return
    } else {
      if (isNicknameChanged) {
        param = {
          ...param,
          nickname: nickname
        }
      }
      if (isEmailChanged) {
        param = {
          ...param,
          email: email
        }
      }
      if (isPhoneNumberChanged) {
        const phone = removeHyphenFromPhoneNumber(phoneNumber)
        param = {
          ...param,
          phoneNumber: phone
        }
      }
      if (isBriefBioChanged) {
        param = {
          ...param,
          briefBio: briefBio
        }
      }
      if (isSocialMediaUrlChanged) {
        param = {
          ...param,
          socialMediaUrl: socialMediaUrl
        }
      }
    }

    if (isNicknameChanged && isEmailChanged) {
      mutatePostIsDuplicateNickname({nickname: nickname}, {
        onSuccess: (isDuplicate) => {
          if (isDuplicate) {
            setNicknameDuplicated(true)
            setNicknameHelperText(SIGN_UP_NICKNAME.HELPER_TEXT_DUPLICATION)
            setNicknameTrue(false)
            return
          } else {
            mutatePostIsDuplicateEmail({email: email}, {
              onSuccess: (isDuplicate) => {
                if (isDuplicate) {
                  setEmailDuplicated(true)
                  setEmailHelperText(SIGN_UP_EMAIL.HELPER_TEXT_DUPLICATION)
                  setEmailTrue(false)
                  return
                } else {
                  if (isProfilePictureUrlChanged) {
                    if (myIsProfilePictureUrlDeleted) {
                      mutateDelMyProfilePicture(null, {
                        onSuccess: () => {
                          isSuccess = true
                          mutatePatchMyDetails(param, {
                            onSuccess: () => {
                              isSuccess = true
                              queryClient.invalidateQueries('getMyInfo')
                              queryClient.invalidateQueries('getGradeInformation')
                              setOpen(false)
                              resetTempMyData()
                              setMyIsProfilePictureUrlDeleted(false)
                            },
                            onError: () => {
                              isSuccess = false
                            }
                          })
                        },
                        onError: () => {
                          isSuccess = false
                        }
                      })
                    } else {
                      if (typeof profilePictureUrl === 'object') {
                        const formData = new FormData()
                        formData.append('multipartFile', profilePictureUrl)
                        mutatePutMyProfilePicture(formData, {
                          onSuccess: () => {
                            isSuccess = true
                            mutatePatchMyDetails(param, {
                              onSuccess: () => {
                                isSuccess = true
                                queryClient.invalidateQueries('getMyInfo')
                                queryClient.invalidateQueries('getGradeInformation')
                                resetTempMyData()
                                setMyIsProfilePictureUrlDeleted(false)

                                completeChangeInform()

                                setTimeout(() => {
                                  setOpen(false)
                                }, 300)
                              },
                              onError: () => {
                                isSuccess = false
                              }
                            })
                          },
                          onError: () => {
                            isSuccess = false
                          }
                        })
                      }
                    }
                  } else {
                    mutatePatchMyDetails(param, {
                      onSuccess: () => {
                        isSuccess = true
                        queryClient.invalidateQueries('getMyInfo')
                        queryClient.invalidateQueries('getGradeInformation')
                        setOpen(false)
                        resetTempMyData()
                        setMyIsProfilePictureUrlDeleted(false)
                      },
                      onError: () => {
                        isSuccess = false
                      }
                    })
                  }
                }
              }
            })
          }
        }
      })
    } else {
      if (isNicknameChanged) {
        mutatePostIsDuplicateNickname({nickname: nickname}, {
          onSuccess: (isDuplicate) => {
            if (isDuplicate) {
              setNicknameDuplicated(true)
              setNicknameHelperText(SIGN_UP_NICKNAME.HELPER_TEXT_DUPLICATION)
              setNicknameTrue(false)
              return
            } else {
              if (isProfilePictureUrlChanged) {
                if (myIsProfilePictureUrlDeleted) {
                  mutateDelMyProfilePicture(null, {
                    onSuccess: () => {
                      isSuccess = true
                      mutatePatchMyDetails(param, {
                        onSuccess: () => {
                          isSuccess = true
                          queryClient.invalidateQueries('getMyInfo')
                          queryClient.invalidateQueries('getGradeInformation')
                          setOpen(false)
                          resetTempMyData()
                          setMyIsProfilePictureUrlDeleted(false)
                        },
                        onError: () => {
                          isSuccess = false
                        }
                      })
                    },
                    onError: () => {
                      isSuccess = false
                    }
                  })
                } else {
                  if (typeof profilePictureUrl === 'object') {
                    const formData = new FormData()
                    formData.append('multipartFile', profilePictureUrl)
                    mutatePutMyProfilePicture(formData, {
                      onSuccess: () => {
                        isSuccess = true
                        mutatePatchMyDetails(param, {
                          onSuccess: () => {
                            isSuccess = true
                            queryClient.invalidateQueries('getMyInfo')
                            queryClient.invalidateQueries('getGradeInformation')
                            resetTempMyData()
                            setMyIsProfilePictureUrlDeleted(false)

                            completeChangeInform()

                            setTimeout(() => {
                              setOpen(false)
                            }, 300)
                          },
                          onError: () => {
                            isSuccess = false
                          }
                        })
                      },
                      onError: () => {
                        isSuccess = false
                      }
                    })
                  }
                }
              } else {
                mutatePatchMyDetails(param, {
                  onSuccess: () => {
                    isSuccess = true
                    queryClient.invalidateQueries('getMyInfo')
                    queryClient.invalidateQueries('getGradeInformation')
                    setOpen(false)
                    resetTempMyData()
                    setMyIsProfilePictureUrlDeleted(false)
                  },
                  onError: () => {
                    isSuccess = false
                  }
                })
              }
            }
          }
        })
      } else if (isEmailChanged) {
        mutatePostIsDuplicateEmail({email: email}, {
          onSuccess: (isDuplicate) => {
            if (isDuplicate) {
              setEmailDuplicated(true)
              setEmailHelperText(SIGN_UP_EMAIL.HELPER_TEXT_DUPLICATION)
              setEmailTrue(false)
              return
            } else {
              if (isProfilePictureUrlChanged) {
                if (myIsProfilePictureUrlDeleted) {
                  mutateDelMyProfilePicture(null, {
                    onSuccess: () => {
                      isSuccess = true
                      mutatePatchMyDetails(param, {
                        onSuccess: () => {
                          isSuccess = true
                          queryClient.invalidateQueries('getMyInfo')
                          queryClient.invalidateQueries('getGradeInformation')
                          setOpen(false)
                          resetTempMyData()
                          setMyIsProfilePictureUrlDeleted(false)
                        },
                        onError: () => {
                          isSuccess = false
                        }
                      })
                    },
                    onError: () => {
                      isSuccess = false
                    }
                  })
                } else {
                  if (typeof profilePictureUrl === 'object') {
                    const formData = new FormData()
                    formData.append('multipartFile', profilePictureUrl)
                    mutatePutMyProfilePicture(formData, {
                      onSuccess: () => {
                        isSuccess = true
                        mutatePatchMyDetails(param, {
                          onSuccess: () => {
                            isSuccess = true
                            queryClient.invalidateQueries('getMyInfo')
                            queryClient.invalidateQueries('getGradeInformation')
                            resetTempMyData()
                            setMyIsProfilePictureUrlDeleted(false)

                            completeChangeInform()

                            setTimeout(() => {
                              setOpen(false)
                            }, 300)
                          },
                          onError: () => {
                            isSuccess = false
                          }
                        })
                      },
                      onError: () => {
                        isSuccess = false
                      }
                    })
                  }
                }
              } else {
                mutatePatchMyDetails(param, {
                  onSuccess: () => {
                    isSuccess = true
                    queryClient.invalidateQueries('getMyInfo')
                    queryClient.invalidateQueries('getGradeInformation')
                    setOpen(false)
                    resetTempMyData()
                    setMyIsProfilePictureUrlDeleted(false)
                  },
                  onError: () => {
                    isSuccess = false
                  }
                })
              }
            }
          }
        })
      } else if (isPhoneNumberChanged) {
        mutatePatchMyDetails(param, {
          onSuccess: () => {
            isSuccess = true
            queryClient.invalidateQueries('getMyInfo')
            queryClient.invalidateQueries('getGradeInformation')
            setOpen(false)
            resetTempMyData()
            setMyIsProfilePictureUrlDeleted(false)
          },
          onError: () => {
            isSuccess = false
          }
        })
      } else if (isBriefBioChanged || isSocialMediaUrlChanged) {
        if (isProfilePictureUrlChanged) {
          if (myIsProfilePictureUrlDeleted) {
            mutateDelMyProfilePicture(null, {
              onSuccess: () => {
                isSuccess = true
                mutatePatchMyDetails(param, {
                  onSuccess: () => {
                    isSuccess = true
                    queryClient.invalidateQueries('getMyInfo')
                    queryClient.invalidateQueries('getGradeInformation')
                    setOpen(false)
                    resetTempMyData()
                    setMyIsProfilePictureUrlDeleted(false)
                  },
                  onError: () => {
                    isSuccess = false
                  }
                })
              },
              onError: () => {
                isSuccess = false
              }
            })
          } else {
            if (typeof profilePictureUrl === 'object') {
              const formData = new FormData()
              formData.append('multipartFile', profilePictureUrl)
              mutatePutMyProfilePicture(formData, {
                onSuccess: () => {
                  isSuccess = true
                  mutatePatchMyDetails(param, {
                    onSuccess: () => {
                      isSuccess = true
                      queryClient.invalidateQueries('getMyInfo')
                      queryClient.invalidateQueries('getGradeInformation')
                      resetTempMyData()
                      setMyIsProfilePictureUrlDeleted(false)

                      completeChangeInform()

                      setTimeout(() => {
                        setOpen(false)
                      }, 300)
                    },
                    onError: () => {
                      isSuccess = false
                    }
                  })
                },
                onError: () => {
                  isSuccess = false
                }
              })
            }
          }
        } else {
          mutatePatchMyDetails(param, {
            onSuccess: () => {
              isSuccess = true
              queryClient.invalidateQueries('getMyInfo')
              queryClient.invalidateQueries('getGradeInformation')
              setOpen(false)
              resetTempMyData()
              setMyIsProfilePictureUrlDeleted(false)
            },
            onError: () => {
              isSuccess = false
            }
          })
        }
      } else {
        if (isProfilePictureUrlChanged) {
          if (myIsProfilePictureUrlDeleted) {
            mutateDelMyProfilePicture(null, {
              onSuccess: () => {
                isSuccess = true
                queryClient.invalidateQueries('getMyInfo')
                queryClient.invalidateQueries('getGradeInformation')
                setOpen(false)
                resetTempMyData()
                setMyIsProfilePictureUrlDeleted(false)
              },
              onError: () => {
                isSuccess = false
              }
            })
          } else {
            if (typeof profilePictureUrl === 'object') {
              const formData = new FormData()
              formData.append('multipartFile', profilePictureUrl)
              mutatePutMyProfilePicture(formData, {
                onSuccess: () => {
                  isSuccess = true
                  queryClient.invalidateQueries('getMyInfo')
                  queryClient.invalidateQueries('getGradeInformation')
                  resetTempMyData()
                  setMyIsProfilePictureUrlDeleted(false)

                  completeChangeInform()

                  setTimeout(() => {
                    setOpen(false)
                  }, 300)
                },
                onError: () => {
                  isSuccess = false
                }
              })
            }
          }
        }
      }
    }
  }

  const handleClickLogout = () => {
    try {
      setHackleTrack(HACKLE_TRACK.MY_INFO_LOGOUT)
      removeCookie(COMMON_STR.ACCOUNT_KEY)

      resetTokens()
      resetAccessToken()
      resetRefreshToken()

      logout()
      setOpen(false)
      resetTempMyData()
      setActiveTab('')
      router.replace({pathname: PATH_DONOTS_PAGE.GUEST})
    } catch (error) {
      console.error(error)
      enqueueSnackbar('Unable to logout!', {variant: 'error'})
    }
  }

  const handleClickChangePw = () => setOpenChangePw(true)
  const handleClickChangePhone = () => setOpenChangePhone(true)
  const handleClickWithdrawMembership = () => {
    setOpenWithdrawMembership(true)
    setHackleTrack(HACKLE_TRACK.MY_INFO_UNREGISTER)
  }

  return (
    <>
      <MainDialogLayout
        fullScreen open={open} onClose={handleClickOpenPopupDialog} TransitionComponent={Transition}
        sx={{'& .MuiDialog-paper': {mx: 0, height: '100%', backgroundColor: 'white'}, zIndex: Z_INDEX.DIALOG}}
      >
        <MyHeaderDialogLayout
          title={MY_COMMON.TEXT.INFORMATION}
          onBack={handleBackButton}
        >
          <Container maxWidth={'xs'} disableGutters sx={{px: '20px', mt: '39px', mb: '34px'}}>
            <Badge
              overlap="circular"
              anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
              badgeContent={
                <SvgCommonIcons alt={ALT_STRING.MY.BTN_CHANGE_PROFILE} type={ICON_TYPE.PHOTO_ADD}/>
              }
              onClick={handleGalleryClick}
              sx={{
                width: '88px',
                height: '88px',
                margin: 'auto',
                display: 'flex',
                cursor: 'pointer',
                overflow: 'hidden',
                alignItems: 'center',
                position: 'relative',
                justifyContent: 'center',
                textDecoration: 'none',
                WebkitTapHighlightColor: 'transparent',
              }}
            >
              {profilePictureUrl
                ? (<Avatar
                  alt={ALT_STRING.MY.PROFILE}
                  tabIndex={1}
                  src={fileData(profilePictureUrl)?.preview}
                  sx={{width: '88px', height: '88px'}}
                  onKeyDown={(e) => handleEnterPress(e, handleGalleryClick)}
                />)
                : (<SvgCommonIcons
                  alt={ALT_STRING.MY.BTN_CHANGE_PROFILE}
                  tabIndex={1}
                  type={ICON_TYPE.PROFILE_NO_IMAGE_80PX}
                  sx={{width: '88px', ...clickBorderNone}}
                  onKeyDown={(e) => handleEnterPress(e, handleGalleryClick)}
                />)
              }
            </Badge>
          </Container>

          <Container maxWidth={'xs'} disableGutters sx={{px: '20px',}}>
            <MyNicknameInputForm nickname={nickname} setNickname={setNickname}/>
            <MyEmailInputForm email={email} setEmail={setEmail}/>
            <MyBirthInputForm birthDay={getYYMMDDFromHyphen(birthDay)} setBirthDay={setBirthDay}/>
            <MyPhoneNumberInputForm phoneNumber={getHyphenPhoneNumber(phoneNumber)} setPhoneNumber={setPhoneNumber}
                                    handleClickChangePhone={handleClickChangePhone}/>
            <MySnsLinkInputForm socialMediaUrl={socialMediaUrl} setSocialMediaUrl={setSocialMediaUrl}
                                type={userInfo?.type} grade={userInfo?.grade}/>
            <MyIntroductionInputForm briefBio={briefBio} setBriefBio={setBriefBio}/>
            {memberInfo?.id?.length > 0 && <Typography tabIndex={1} fontWeight="400" fontSize="16px"
                                                       sx={{lineHeight: '24px', color: 'black', mt: '8px', mb: '28px', ...clickBorderNone}}
                                                       onClick={handleClickChangePw}
                                                       onKeyDown={(e) => handleEnterPress(e, handleClickChangePw)}
            >
              {MY_COMMON.TEXT.PASSWORD}
            </Typography>}
            {/*<Typography fontWeight="400" fontSize="16px"*/}
            {/*            sx={{lineHeight: '24px', color: 'black', mt: '8px', mb: '28px'}} onClick={handleClickOpenSns}>*/}
            {/*  {MY_COMMON.TEXT.SNS_ACCOUNT}*/}
            {/*</Typography>*/}
            <Typography tabIndex={1} fontWeight="400" fontSize="16px"
                        sx={{lineHeight: '24px', color: 'black', mt: '8px', mb: '28px', ...clickBorderNone}}
                        onClick={handleClickWithdrawMembership}
                        onKeyDown={(e) => handleEnterPress(e, handleClickWithdrawMembership)}
            >
              {MY_COMMON.TEXT.WITHDRAW}
            </Typography>

            <Container
              disableGutters
              sx={{
                mt: '57px',
                mb: '21px',
                maxWidth: 'xs',
              }}
            >
              <Button
                tabIndex={1}
                disableFocusRipple
                fullWidth
                variant="contained"
                disabled={isBottomBtnDisabled}
                onClick={handleConfirmClick}
                onKeyDown={(e) => handleEnterPress(e, handleConfirmClick)}
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  p: '12px 50px',
                  height: '52px',
                  fontStyle: 'normal',
                  fontWeight: '700',
                  fontSize: '16px',
                  lineHeight: '24px',
                  textAlign: 'center',
                  color: '#FFFFFF',
                  '&.Mui-disabled': {
                    color: '#B3B3B3',
                    backgroundColor: '#E6E6E6',
                  },
                  '&.Mui-disabled:hover': {
                    backgroundColor: '#E6E6E6',
                  },
                  '&:hover': {
                    boxShadow: 'none',
                    backgroundColor: 'primary.main',
                  },
                  ...clickBorderNone
                }}
              >
                {bottomBtnText}
              </Button>
            </Container>

            <Typography
              tabIndex={1}
              fontWeight="400"
              fontSize="14px"
              sx={{
                mt: '21px',
                mb: '3px',
                lineHeight: '20px',
                textAlign: 'center',
                textDecoration: 'underline',
                color: '#888888',
                ...clickBorderNone
              }}
              onClick={handleClickLogout}
              onKeyDown={(e) => handleEnterPress(e, handleClickLogout)}
            >
              {MY_COMMON.TEXT.LOGOUT}
            </Typography>

          </Container>
        </MyHeaderDialogLayout>

      </MainDialogLayout>
      <MyBottomDialog>
        <GalleryContents profilePictureUrl={profilePictureUrl} setProfilePictureUrl={setProfilePictureUrl}/>
      </MyBottomDialog>
      <MySnsAccount/>

      <ChangePasswordSection memberInfo={memberInfo}/>
      <ChangeMyPhoneNumberDialog/>
      <WithdrawMembership userInfo={userInfo}/>

      {(isLoadingGetIsDuplicateNickname || isLoadingPostIsDuplicateEmail || isLoadingPatchMyDetails || isLoadingPutMyProfilePicture || isLoadingDelMyProfilePicture) &&
        <LoadingScreen/>}
    </>
  );
}
