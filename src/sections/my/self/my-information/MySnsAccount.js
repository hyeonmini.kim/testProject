import * as React from 'react';
import {useEffect, useState} from 'react';
// @mui
import {Container, Fade, Stack, Typography,} from '@mui/material';
// components
import {useRecoilState} from "recoil";
import {MY_COMMON} from "../../../../constant/my/MyConstants";
import MySnsAccountDetail from "./MySnsAccountDetail";
import {myOpenSnsDetailSelector, myOpenSnsSelector} from "../../../../recoil/selector/my/popup/myPopupSelector";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {getMySnsList} from "../../../../api/myApi";
import {parseProvider} from "../../../../constant/my/Setting";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../layouts/my/MyHeaderDialogLayout";

// ----------------------------------------------------------------------

export default function MySnsAccount() {
  const [openSns, setOpenSns] = useRecoilState(myOpenSnsSelector);
  const [openSnsDetail, setOpenSnsDetail] = useRecoilState(myOpenSnsDetailSelector);

  const [selectedInform, setSelectedInform] = useState({})
  const {data: mySnsList} = getMySnsList()

  useEffect(() => {
  }, [openSns])

  const handleBackButton = () => {
    setOpenSns(false);
  };

  const handleClickCloseSns = () => {
    setOpenSns(false);
  };

  const handleClickOpenSnsDetail = (inform) => {
    setOpenSnsDetail(true)
    setSelectedInform(inform)
  }

  return (
    <>
      <MainDialogLayout
        fullScreen open={openSns} onClose={handleClickCloseSns} TransitionComponent={Fade}
        sx={{'& .MuiDialog-paper': {mx: 0, height: '100%', backgroundColor: 'white'}, zIndex: Z_INDEX.DIALOG}}
      >
        <MyHeaderDialogLayout
          title={MY_COMMON.TEXT.SNS_ACCOUNT}
          onBack={handleBackButton}
        >
          <Container maxWidth={'xs'} disableGutters sx={{px: '20px', mt: '-10px'}}>
            {/* APPLE 계정 임시 스킵 */}
            {mySnsList?.map((item, index) => (
              item?.providerType !== 'APPLE' && <Stack
                flexWrap="wrap"
                direction="row"
                justifyContent="space-between"
                sx={{
                  my: '40px',
                }}
                key={index}
                onClick={() => handleClickOpenSnsDetail(item)}
              >
                <Typography fontWeight="500" fontSize="16px" sx={{lineHeight: '24px', color: '#222222'}}>
                  {parseProvider(item?.providerType)}
                </Typography>
                <Typography fontWeight="400" fontSize="16px" sx={{lineHeight: '24px', color: '#888888'}}>
                  {item?.isConnectionStatus ? "연결됨" : "연결안됨"}
                </Typography>
              </Stack>))}
          </Container>
        </MyHeaderDialogLayout>

        <MySnsAccountDetail snsList={mySnsList} inform={selectedInform}/>
      </MainDialogLayout>
    </>
  );
}