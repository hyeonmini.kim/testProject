import {useRecoilState, useRecoilValue, useSetRecoilState} from "recoil";
import {
  authBottomButtonState,
  authIdEndNumberState,
  authIdFrontNumberState,
  authNameState,
  authPhoneNumberState,
  authTelecomState
} from "../../../../../recoil/atom/sign-up/auth";
import {useEffect, useRef, useState} from "react";
import {commonBottomDialogState} from "../../../../../recoil/atom/common/bottomDialog";
import {toastMessageSelector} from "../../../../../recoil/selector/common/toastSelector";
import {TOAST_TYPE} from "../../../../../constant/common/Common";
import {AUTH_CODE, CERT_TYPE, SIGN_UP_DIALOG} from "../../../../../constant/sign-up/SignUp";
import {Box, Typography} from "@mui/material";
import WatchLaterIcon from '@mui/icons-material/WatchLater';
import {styled} from "@mui/material/styles";
import Timer from "../../../../../components/auth/Timer";
import {memberInformationSelector} from "../../../../../recoil/selector/auth/userSelector";
import {
  changeBirthFromYYMMDDToYYYYMMDD,
  changeGenderType,
  changeNewsAgencyType,
  checkTransactionSeqNumberByEnv,
  removeHyphenFromPhoneNumber
} from "../../../../../utils/authUtils";
import {postAuthCodeMutate, postCheckIdAndAuthMutate} from "../../../../../api/authApi";
import {authCiSelector} from "../../../../../recoil/selector/sign-up/signUpSelector";
import {isChangePhoneNumberSelector} from "../../../../../recoil/selector/my/myDataSelector";
import {transactionSeqNumberSelector} from "../../../../../recoil/selector/auth/accessTokenSelector";
import {getEnv} from "../../../../../utils/envUtils";
import {ERRORS, GET_ERROR_CODE, GET_ERROR_MESSAGE} from "../../../../../constant/common/Error";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";
import {ALT_STRING} from "../../../../../constant/common/AltString";

const InputAreaStyle = styled('div')(() => ({
  overflowX: 'hidden',
  width: '100%',
  '& .MuiInput-root:before': {
    borderBottom: 'none',
  },
  '& .MuiInput-root:hover:before': {
    borderBottom: 'none',
  },
  '& .MuiInput-root:hover.Mui-disabled:before': {
    borderBottom: 'none',
  },
  '& .MuiInput-root:hover.Mui-disabled:after': {
    borderBottom: 'none',
  },
  '& .MuiInput-root.Mui-disabled:before': {
    borderBottom: 'none',
  },
  '& .MuiInput-root.Mui-disabled:after': {
    borderBottom: 'none',
  },
}))

export default function MyAuthCodeInputForm({setPage, setOpenPhoneDialog}) {
  // 부모 바텀 버튼 제어 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  // 입력 값 제어 state
  const inputRef = useRef([])
  const [current, setCurrent] = useState(0)
  const [inputNumbers, setInputNumbers] = useState('')
  const [isWarningTime, setIsWarningTime] = useState(false)
  const [errorCount, setErrorCount] = useState(0)
  // 입력 칸 스타일 제어 state
  const [numColor, setNumColor] = useState('#222222')
  const [inputBorderBottom, setInputBorderBottom] = useState('1px solid #CCCCCC')
  // 바텀 다이얼로그 제어 state
  const setIsOpen = useSetRecoilState(commonBottomDialogState)
  // 시간 제어 state
  const timeByMs = 1000 * 60 * 3
  const [time, setTimeByMs] = useState(timeByMs)

  const setToastMessage = useSetRecoilState(toastMessageSelector)

  const setCi = useSetRecoilState(authCiSelector)
  const [telecom, setTelecom] = useRecoilState(authTelecomState)
  const [phoneNumber, setPhoneNumber] = useRecoilState(authPhoneNumberState)
  const [name, setName] = useRecoilState(authNameState)
  const [idFront, setIdFront] = useRecoilState(authIdFrontNumberState)
  const [idEnd, setIdEnd] = useRecoilState(authIdEndNumberState)
  const [transactionSeqNumber, setTransactionSeqNumber] = useRecoilState(transactionSeqNumberSelector)

  const memberInfo = useRecoilValue(memberInformationSelector)
  const setIsChangePhoneNumber = useSetRecoilState(isChangePhoneNumberSelector)

  const {mutate: mutatePostCheckIdAndAuth} = postCheckIdAndAuthMutate() // 인증번호 확인하고 ID로 가입 여부 체크
  const {mutate: mutatePostAuthCode} = postAuthCodeMutate()

  const env = getEnv()

  // 인증번호 불일치
  const mismatchAuthCode = () => {
    setErrorCount(errorCount + 1)
    // 입력칸 색상 설정
    setNumColor('#FF4842')
    setInputBorderBottom('1px solid #FF4842')
    // 스낵바 설정
    if ((errorCount + 1) < 5) {
      setToastMessage({
        type: TOAST_TYPE.BOTTOM_DIALOG_HEADER_ERROR,
        message: AUTH_CODE.TOAST_INCORRECT,
      })
    } else {
      setToastMessage({
        type: TOAST_TYPE.BOTTOM_DIALOG_HEADER_ERROR,
        message: AUTH_CODE.TOAST_FIVE_TIMES,
      })

      setTimeout(function () {
        setIsOpen(false)
      }, 500)
    }
  }

  // 입력칸 클릭 이벤트 제어
  const onClickNumber = () => {
    inputRef.current[0].focus()
    // 입력 값 초기화
    Array(6).fill().map((i, index) => {
      inputRef.current[index].value = ''
    })
    setCurrent(0)
    setInputNumbers('')
    // 입력 칸 스타일 초기화
    setNumColor('#222222')
    setInputBorderBottom('1px solid #CCCCCC')
  }
  // 입력칸 체인지 이벤트 제어
  const handleInputAllNumber = (numbers) => {
    // 인증번호 6자리 모두 입력했을 때 처리
    const phone = removeHyphenFromPhoneNumber(phoneNumber)
    if (time !== 0) {
      mutatePostCheckIdAndAuth({
        joiningAccountType: CERT_TYPE.OWN_ACCOUNT,
        param: {
          phoneNumber: phone,
          transactionSeqNumber: transactionSeqNumber,
          authCodeOf6Digits: numbers,
        },
        id: memberInfo?.id,
      }, {
        onSuccess: (result) => {
          if (result) {
            // 인증번호 일치
            // 분류 : 정보 일치 / 정보 불일치
            if (result?.isMatched) {
              // 아이디에 맞는 인증 정보
              // setCookie(COMMON_STR.CI, result?.ci, 1)
              setCi(result?.ci)
              setIsOpen(false)
              setOpenPhoneDialog(false)
              setPage(1)
              setIsBottomBtnDisabled(true)

              setIsChangePhoneNumber(true)
              setName('')
              setTelecom('')
              setIdFront('')
              setIdEnd('')
            } else {
              // 아이디에 맞지 않는 인증 정보
              setErrorCount(errorCount + 1)
              // 입력칸 색상 설정
              setNumColor('#FF4842')
              setInputBorderBottom('1px solid #FF4842')
              // 스낵바 설정
              if ((errorCount + 1) < 5) {
                // 아이디에 맞지 않는 인증 정보
                setIsOpen(false)
                setToastMessage({
                  type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
                  message: SIGN_UP_DIALOG.INCORRECT_INFORM,
                })
              } else {
                setToastMessage({
                  type: TOAST_TYPE.BOTTOM_DIALOG_HEADER_ERROR,
                  message: AUTH_CODE.TOAST_FIVE_TIMES,
                })

                setTimeout(function () {
                  setIsOpen(false)
                }, 500)
              }
            }
          } else {
            // 인증번호 불일치
            mismatchAuthCode()
          }
        },
        onError: async (error) => {
          switch (GET_ERROR_CODE(error)) {
            case ERRORS.AUTH_CODE_NOT_MATCHED:
              setToastMessage({
                type: TOAST_TYPE.BOTTOM_DIALOG_HEADER_ERROR,
                message: GET_ERROR_MESSAGE(error),
              })
              break
            case ERRORS.SERVICE_UNAVAILABLE:
            case ERRORS.INCORRECT_AUTH_CODE_ATTEMPTS_EXCEEDED:
            case ERRORS.AUTH_CODE_RETRANSMISSION_REQUEST_TIMEOUT:
            case ERRORS.MISCELLANEOUS:
              setTimeout(function () {
                setIsOpen(false)

                setToastMessage({
                  type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
                  message: GET_ERROR_MESSAGE(error),
                })
              }, 300)
              break
          }
        }
      })
    } else {
      // 입력칸 색상 설정
      setNumColor('#FF4842')
      setInputBorderBottom('1px solid #FF4842')
      // 스낵바 설정
      setToastMessage({
        type: TOAST_TYPE.BOTTOM_DIALOG_HEADER_ERROR,
        message: AUTH_CODE.TOAST_TIMEOUT,
      })
    }
  }

  // 입력칸 체인지 이벤트 제어
  const onChangeNumber = async (e) => {
    if (!isNaN(e.target.value) && e.target.value.length === 6) {
      const inputNumber = e.target.value
      for (let i = 0; i < inputNumber.length; i++) {
        inputRef.current[i].value = inputNumber.substring(i, i + 1)
      }
      inputRef.current[current].blur()

      // 인증번호 6자리 모두 입력했을 때 처리
      await handleInputAllNumber(e.target.value)
    } else if (!isNaN(e.target.value) && e.target.value.length === 1) {
      let numbers = inputNumbers + e.target.value
      if (e.target.value !== '') {
        setInputNumbers(numbers)
        if (current === 5) {
          inputRef.current[current].blur()

          setTimeout(async () => {
            // 인증번호 6자리 모두 입력했을 때 처리
            await handleInputAllNumber(numbers)
          }, 500)
        } else {
          inputRef.current[current + 1].focus()
          setCurrent(current + 1)
        }
      }
    } else {
      onClickNumber()
    }
  }

  // 키보드 키 제어
  const handleKeyDown = (e) => {
    // 백스페이스 키 처리
    if (e.key === 'Backspace' && current !== 0) {
      inputRef.current[current].blur()
      inputRef.current[current - 1].focus()
      setCurrent(current - 1)
      setInputNumbers(inputNumbers.slice(0, -1))
    }
  }

  const handleKeyUp = (e) => {
    if (e.key === 'Unidentified')
      inputRef.current[current].value = ''
    // if (e.keyCode === 229)
    //   inputRef.current[current].value = ''
  }

  const onReset = () => {
    /* API : 본인인증 입력 정보 검사 및 인증번호 받기 */
    const newsAgencyType = changeNewsAgencyType(telecom)
    const gender = changeGenderType(idEnd)
    const birth = changeBirthFromYYMMDDToYYYYMMDD(idFront, idEnd)
    const phone = removeHyphenFromPhoneNumber(phoneNumber)
    mutatePostAuthCode({
      name: name,
      phoneNumber: phone,
      birthday: birth,
      gender: gender,
      koreaForeignerType: '1',
      newsAgencyType: newsAgencyType,
    }, {
      onSuccess: (result) => {
        /* Further Development */
        setTransactionSeqNumber(result?.transactionSeqNumber)
        // result 값에 따라 달라질 수 있음
        if (checkTransactionSeqNumberByEnv(result?.transactionSeqNumber, env)) {
          // 본인인증 정보 성공
          // 입력시간 초기화
          setTimeByMs(timeByMs)
          setIsWarningTime(false)
          // 스낵바 설정
          setToastMessage({
            type: TOAST_TYPE.BOTTOM_DIALOG_HEADER,
            message: AUTH_CODE.TOAST_RETRY,
          })
          // 입력값 초기화
          onClickNumber()
          setErrorCount(0)
        } else {
          // 본인인증 정보 에러
          setToastMessage({
            type: TOAST_TYPE.BOTTOM_HEADER,
            message: '입력하신 정보가 올바르지 않아요.\n다시 한번 확인 후 입력해 주세요.',
          })
        }
      },
      onError: async (error) => {
        switch (GET_ERROR_CODE(error)) {
          case ERRORS.INVALID_NAME_OR_RESIDENT_REGISTRATION_NUMBER:
          case ERRORS.SERVICE_UNAVAILABLE:
          case ERRORS.MISCELLANEOUS:
            setToastMessage({
              type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
              message: GET_ERROR_MESSAGE(error),
            })
            break
        }
      }
    })
  }

  useEffect(() => {
    if (time <= 1000 * 30) {
      setIsWarningTime(true)
    }
  }, [time])

  return (
    <Box textAlign="-webkit-center">
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 500,
          fontSize: '16px',
          lineHeight: '24px',
          letterSpacing: '-0.02em',
          color: '#000000',
          mb: '13px',
        }}
      >
        {AUTH_CODE.TITLE}
      </Typography>
      <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
        <WatchLaterIcon
          sx={{
            fontSize: '12px',
            color: isWarningTime ? '#FF4842' : '#888888',
            mr: '2px',
            mb: '1px',
          }}
        />
        <Typography
          sx={{
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            letterSpacing: '-0.02em',
            color: isWarningTime ? '#FF4842' : '#888888',
          }}
        >
          <Timer time={time} setTimeByMs={setTimeByMs}/>
        </Typography>
      </Box>
      <Box sx={{display: 'flex', justifyContent: 'center', m: '18px 0 37px 0'}}>
        <InputAreaStyle>
          {Array(6).fill().map((i, index) => {
            return (
              <TextFieldWrapper
                autoComplete='off'
                key={index}
                variant="standard"
                onClick={onClickNumber}
                onChange={onChangeNumber}
                onKeyDown={handleKeyDown}
                onKeyUp={handleKeyUp}
                inputRef={(ref) => (inputRef.current[index] = ref)}
                inputProps={{inputMode: 'decimal'}}
                sx={{
                  height: '40px',
                  width: '24px',
                  mx: '5px',
                  '& input': {
                    p: 0,
                    textAlign: 'center',
                    fontStyle: 'normal',
                    fontWeight: 700,
                    fontSize: '24px',
                    lineHeight: '32px',
                    letterSpacing: '-0.02em',
                    color: numColor,
                    caretColor: 'transparent',
                    borderBottom: inputBorderBottom,
                  },
                }}
                InputProps={{inputProps: {tabIndex: index === 0 ? 0 : -1, inputMode: 'numeric', title: ALT_STRING.SIGN_UP.AUTH_CODE(index)}}}
              />
            );
          })}
        </InputAreaStyle>
      </Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 400,
          fontSize: '12px',
          lineHeight: '18px',
          letterSpacing: '-0.03em',
          color: '#222222',
          textDecoration: 'underline',
          mb: '18px',
          width: 'fit-content',
        }}
        onClick={onReset}
      >
        {AUTH_CODE.RETRY}
      </Typography>
    </Box>
  );
}