import {Box, InputAdornment, Typography} from "@mui/material";
import * as React from "react";
import {useState} from "react";
import {MY_COMMON} from "../../../../../constant/my/MyConstants";
import {ICON_TYPE, SvgCommonIcons} from "../../../../../constant/icons/ImageIcons";
import {ALT_STRING} from "../../../../../constant/common/AltString";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";

export default function MyIntroductionInputForm({briefBio, setBriefBio}) {
  // introduction 정보 state
  // const [briefBio, setBriefBio] = useRecoilState(myBriefBioSelector);
  // 입력 상태 state
  const [cancelIcon, setCancelIcon] = useState(false);
  // introduction 값 변경 제어
  const handleChangeValue = (e) => {
    const value = e.target.value.substring(0, 25);

    setBriefBio(value);
  }
  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true);
  const handleBlur = () => setCancelIcon(false);
  const handleCancel = () => setBriefBio('');

  return (
    <Box>
      <Typography fontWeight="700" fontSize="14px" sx={{lineHeight: '20px', color: '#888888', mt: '14px', mb: '25px'}}>
        {MY_COMMON.TEXT.INTRODUCTION}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={briefBio}
        placeholder={MY_COMMON.TEXT.INTRODUCTION_MESSAGE}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        inputProps={{tabIndex: 1, style: {padding: 0}, title: ALT_STRING.MY.INPUT_INTRODUCE}}
        InputProps={{
          endAdornment: briefBio && cancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
            </InputAdornment>
          ),
          style: {
            paddingBottom: '13px',
            marginBottom: '26px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
          },
        }}
        sx={{
          '& input::placeholder': {
            color: '#CCCCCC'
          }
        }}
      />
    </Box>
  );
}