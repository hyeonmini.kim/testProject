import {styled} from "@mui/material/styles";
import {Box, InputAdornment, Typography} from "@mui/material";
import {FiberManualRecord, Remove} from "@mui/icons-material";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {useRecoilState, useRecoilValue, useSetRecoilState} from "recoil";
import {
  commonBottomDialogPageState,
  commonBottomDialogState,
  commonBottomDialogTitleState
} from "../../../../../recoil/atom/common/bottomDialog";
import {
  authBottomButtonClickState,
  authBottomButtonState,
  authBottomButtonTextState,
  authIdEndNumberState,
  authIdFrontNumberState,
  authInputRefIndexState,
  authNameState,
  authPhoneNumberState,
  authTelecomState
} from "../../../../../recoil/atom/sign-up/auth";
import {SIGN_UP_PHONE, SIGN_UP_TITLE} from "../../../../../constant/sign-up/SignUp";
import FAQDetail from "../../../setting/service/faq/FAQDetail";
import {settingShowFAQDetailSelector} from "../../../../../recoil/selector/my/settingSelector";
import IFrameDialog from "../../../../../components/common/IFrameDialog";
import {PATH_DONOTS_STATIC_PAGE} from "../../../../../routes/paths";
import {MY_SETTING} from "../../../../../constant/my/Setting";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";

const InputAreaStyle = styled('div')(() => ({
  overflowX: 'hidden',
  width: '100%',
  '& .MuiInput-root:before': {
    borderBottom: 'none',
  },
  '& .MuiInput-root:after': {
    borderBottom: 'none',
  },
  '& .MuiInput-root:hover': {
    borderBottom: 'none',
  },
  '& .MuiInput-root:hover:before': {
    borderBottom: 'none',
  },
  '& .MuiInput-root:hover:after': {
    borderBottom: 'none',
  },
  '& .MuiInput-root:hover.Mui-disabled:before': {
    borderBottom: 'none',
  },
  '& .MuiInput-root:hover.Mui-disabled:after': {
    borderBottom: 'none',
  },
  '& .MuiInput-root.Mui-disabled:before': {
    borderBottom: 'none',
  },
  '& .MuiInput-root.Mui-disabled:after': {
    borderBottom: 'none',
  },
  '& .MuiInput-root.Mui-disabled svg': {
    color: '#222222',
  },
}));

export default function ChangeMyPhoneInputForm({setPage}) {
  // 바텀 다이얼로그 state
  const setIsBottomDialogOpen = useSetRecoilState(commonBottomDialogState)
  const setBottomDialogTitle = useSetRecoilState(commonBottomDialogTitleState)
  const setDialogPage = useSetRecoilState(commonBottomDialogPageState)
  // 본인인증 정보 state
  const [name, setName] = useRecoilState(authNameState)
  const [telecom, setTelecom] = useRecoilState(authTelecomState)
  const [phone, setPhone] = useRecoilState(authPhoneNumberState)
  const [idFront, setIdFront] = useRecoilState(authIdFrontNumberState)
  const [idEnd, setIdEnd] = useRecoilState(authIdEndNumberState)
  // 바텀 버튼 state
  const clickBottomButton = useRecoilValue(authBottomButtonClickState)
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  const [bottomButtonText, setBottomButtonText] = useRecoilState(authBottomButtonTextState)
  // 입력란 포커스 state
  const [isFocusName, setIsFocusName] = useState(false)
  const [isFocusPhone, setIsFocusPhone] = useState(false)
  const [isFocusIdNum, setIsFocusIdNum] = useState(false)
  const [isFocusIdEndNum, setIsFocusIdEndNum] = useState(false)
  // 입력 값 제어
  const [index, setIndex] = useRecoilState(authInputRefIndexState)
  const inputRef = useRef([])
  // 입력 값 변경 제어
  const handleNameChange = (e) => setName(e.target.value)
  const handleTelecomChange = (e) => setTelecom(e.target.value)

  const [openFAQ, setOpenFAQ] = useState(false)
  const [showFAQDetail, setShowFAQDetail] = useRecoilState(settingShowFAQDetailSelector)

  const handleClickFAQ = () => setOpenFAQ(true)
  const handleClickBack = () => setOpenFAQ(false)

  const handlePhoneChange = (e) => {
    const regex = /[^0-9]/g
    let value = e.target.value
    let valueNum = value.replace(regex, '')

    // 핸드폰 번호 사이에 하이픈을 넣기 위한 처리
    if (valueNum.length <= 3) {
      value = valueNum
    }
    if (valueNum.length > 3) {
      value = valueNum.slice(0, 3) + "-" + valueNum.slice(3)
    }
    if (valueNum.length > 6) {
      value = valueNum.slice(0, 3) + "-" + valueNum.slice(3, 6) + "-" + valueNum.slice(6)
    }
    if (valueNum.length > 10) {
      valueNum = valueNum.substring(0, 11)
      value = valueNum.slice(0, 3) + "-" + valueNum.slice(3, 7) + "-" + valueNum.slice(7, 11)
    }
    setPhone(value)
  }
  const handleIdFrontNumChange = (e) => {
    let value = e.target.value.substring(0, 6)
    const regex = /[^0-9]/g
    value = value.replace(regex, '')
    setIdFront(value)
  }
  const handleIdEndNumChange = (e) => {
    let value = e.target.value.substring(0, 1)
    const regex = /[^0-9]/g
    value = value.replace(regex, '')
    setIdEnd(value)
  }
  // 통신사 구역 클릭 이벤트
  const handleTelecomClick = () => {
    setTimeout(function () {
      setDialogPage(0)
      setIsBottomDialogOpen(true)
      setBottomDialogTitle(false)
    }, 300)
  }
  // 입력란 포커스 제어
  const handleFocusName = () => {
    setIsFocusName(true)
    setIndex(0)
  }
  const handleFocusPhone = () => {
    setIsFocusPhone(true)
    setIndex(1)
  }
  const handleFocusIdNum = () => {
    setIsFocusIdNum(true)
    setIndex(2)
  }
  const handleFocusIdEndNum = () => {
    setIsFocusIdNum(true)
    setIsFocusIdEndNum(true)
    setIndex(3)
  }
  // 주민번호 뒷자리 Adornment 클릭
  const handleClickIdEnd = () => {
    setIsFocusIdNum(true)
    setIndex(3)
    inputRef.current[3].focus()
  }
  // 입력란 블러 제어
  const handleBlurName = () => {
    setIsFocusName(false)
    setIndex(-1)
  }
  const handleBlurPhone = () => {
    setIsFocusPhone(false)
    setIndex(-1)
  }
  const handleBlurIdNum = () => {
    setIsFocusIdNum(false)
    setIndex(-1)
  }
  const handleBlurIdEndNum = () => {
    setIsFocusIdNum(false)
    setIsFocusIdEndNum(false)
    setIndex(-1)
  }

  useEffect(() => {
    // 이름 2자리 이상
    // 통신사 빈값 아님
    // 휴대폰 번호 11자리
    // 주민번호 앞자리 6자리
    // 주민번호 뒷자리 1자리

    if (index !== -1 && bottomButtonText === '다음') {
      inputRef.current[index].focus()
    }
    // 이름 작성 규칙에 따른 바텀 버튼 처리
    if (name === '' && telecom === '' && phone === '' && idFront === '' && idEnd === '') {
      setIsBottomBtnDisabled(true)
      setBottomButtonText('확인')
      return;
    }
    if (name.length > 1 || telecom !== '' || phone !== '' || idFront !== '' || idEnd !== '') {
      setIsBottomBtnDisabled(false)
      setBottomButtonText('다음')
    }
    if (name.length > 1 && telecom !== '' && phone !== '' && idFront !== '' && idEnd !== '') {
      setIsBottomBtnDisabled(false)
      setBottomButtonText('확인')
    }
  }, [index, clickBottomButton, name, telecom, phone, idFront, idEnd])

  return (
    <Box sx={{marginTop: '28px'}}>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          marginBottom: '20px',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.PHONE}
      </Typography>
      <Box
        position="relative"
        alignItems="center"
        justifyContent="center"
      >
        <InputAreaStyle>
          <TextFieldWrapper
            autoComplete='off'
            fullWidth
            value={name}
            inputRef={(ref) => (inputRef.current[0] = ref)}
            label={name === '' ? SIGN_UP_PHONE.LABEL_NAME : ' '}
            onChange={handleNameChange}
            onFocus={handleFocusName}
            onBlur={handleBlurName}
            variant="standard"
            InputProps={{
              style: {
                marginTop: 0,
                paddingTop: '14px',
                paddingBottom: '14px',
              }
            }}
            InputLabelProps={{
              shrink: false,
              style: {
                fontStyle: 'normal',
                fontWeight: 500,
                fontSize: '16px',
                lineHeight: '24px',
                letterSpacing: '-0.02em',
                color: '#CCCCCC',
                marginTop: '-4px',
              }
            }}
            sx={{
              '& input': {
                fontStyle: 'normal',
                fontWeight: 500,
                fontSize: '16px',
                lineHeight: '24px',
                color: '#222222',
                letterSpacing: '-0.02em',
                paddingTop: '1px',
                paddingBottom: 0,
              },
              '& .MuiInput-root:hover:before': {
                borderBottom: 'none',
              },
            }}
          />
          <Box sx={{
            marginTop: '-1px',
            height: '1px',
            backgroundColor: isFocusName ? '#222222' : '#CCCCCC',
          }}/>
          <Box display="flex">
            <TextFieldWrapper
              autoComplete='off'
              value={telecom}
              label={telecom === '' ? SIGN_UP_PHONE.LABEL_TELECOM : ' '}
              disabled
              onClick={handleTelecomClick}
              onChange={handleTelecomChange}
              onFocus={handleFocusPhone}
              onBlur={handleBlurPhone}
              variant="standard"
              InputProps={{
                endAdornment:
                  <InputAdornment position="end" sx={{m: 0}}>
                    <ExpandMoreIcon/>
                  </InputAdornment>,
                style: {
                  marginTop: 0,
                  paddingTop: '14px',
                  paddingBottom: '14px',
                  width: '130px',
                }
              }}
              InputLabelProps={{
                shrink: false,
                style: {
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '24px',
                  letterSpacing: '-0.02em',
                  color: '#CCCCCC',
                  marginTop: '-4px',
                }
              }}
              sx={{
                '& input': {
                  paddingTop: '1px',
                  paddingBottom: 0,
                },
                "& .MuiInputBase-input.Mui-disabled": {
                  WebkitTextFillColor: '#222222',
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '24px',
                  letterSpacing: '-0.02em',
                },
              }}
            />
            <TextFieldWrapper
              autoComplete='off'
              fullWidth
              value={phone}
              inputRef={(ref) => (inputRef.current[1] = ref)}
              label={phone === '' ? SIGN_UP_PHONE.LABEL_PHONE : ' '}
              onChange={handlePhoneChange}
              onFocus={handleFocusPhone}
              onBlur={handleBlurPhone}
              variant="standard"
              type="tel"
              InputProps={{
                style: {
                  marginTop: 0,
                  paddingTop: '14px',
                  paddingBottom: '14px',
                }
              }}
              InputLabelProps={{
                shrink: false,
                style: {
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '24px',
                  letterSpacing: '-0.02em',
                  color: '#CCCCCC',
                  marginTop: '-4px',
                  marginLeft: '10px',
                }
              }}
              sx={{
                marginInlineStart: '54px',
                '& input': {
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '24px',
                  color: '#222222',
                  letterSpacing: '-0.02em',
                  paddingTop: '1px',
                  paddingBottom: 0,
                  marginLeft: '10px',
                },
                '& .MuiInput-root:hover:before': {
                  borderBottom: 'none',
                },
              }}
            />
          </Box>
          <Box sx={{
            marginTop: '-1px',
            height: '1px',
            backgroundColor: isFocusPhone ? '#222222' : '#CCCCCC',
          }}/>
          <Box display="flex">
            <TextFieldWrapper
              autoComplete='off'
              value={idFront}
              inputRef={(ref) => (inputRef.current[2] = ref)}
              label={idFront === '' ? SIGN_UP_PHONE.LABEL_ID_FRONT_NUM : ' '}
              onChange={handleIdFrontNumChange}
              onFocus={handleFocusIdNum}
              onBlur={handleBlurIdNum}
              variant="standard"
              inputProps={{inputMode: 'decimal'}}
              InputProps={{
                endAdornment:
                  <InputAdornment position="end" sx={{m: 0}}>
                    <Remove/>
                  </InputAdornment>,
                style: {
                  marginTop: 0,
                  paddingTop: '14px',
                  paddingBottom: '14px',
                  width: '130px',
                }
              }}
              InputLabelProps={{
                shrink: false,
                style: {
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '24px',
                  letterSpacing: '-0.02em',
                  color: '#CCCCCC',
                  marginTop: '-4px',
                }
              }}
              sx={{
                '& input': {
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '24px',
                  color: '#222222',
                  letterSpacing: '-0.02em',
                  paddingTop: '1px',
                  paddingBottom: 0,
                },
                '& .MuiInput-root:hover:before': {
                  borderBottom: 'none',
                },
              }}
            />
            <TextFieldWrapper
              autoComplete='off'
              fullWidth
              value={idEnd}
              inputRef={(ref) => (inputRef.current[3] = ref)}
              label={isFocusIdEndNum ? ' ' : idEnd === '' ? <FiberManualRecord sx={{fontSize: '21px'}}/> : ' '}
              onChange={handleIdEndNumChange}
              onFocus={handleFocusIdEndNum}
              onBlur={handleBlurIdEndNum}
              variant="standard"
              inputProps={{inputMode: 'decimal'}}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="start" sx={{mb: '-1px'}} onClick={handleClickIdEnd}>
                    {Array(6).fill().map((i, index) => {
                      return (
                        <FiberManualRecord
                          key={index}
                          sx={{
                            color: (isFocusIdEndNum || idEnd !== '') ? '#222222' : '#CCCCCC',
                            fontSize: '21px'
                          }}
                        />
                      );
                    })}
                  </InputAdornment>
                ),
                style: {
                  marginTop: 0,
                  paddingTop: '14px',
                  paddingBottom: '14px',
                }
              }}
              InputLabelProps={{
                shrink: false,
                style: {
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '24px',
                  letterSpacing: '-0.02em',
                  color: '#CCCCCC',
                  marginLeft: '2px',
                  marginTop: '-4px',
                }
              }}
              sx={{
                marginInlineStart: '60px',
                '& input': {
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '24px',
                  color: '#222222',
                  letterSpacing: '-0.02em',
                  paddingTop: '1px',
                  paddingBottom: 0,
                  width: '13px',
                  marginLeft: '10px',
                },
                '& .MuiInput-root:hover:before': {
                  borderBottom: 'none',
                },
              }}
            />
          </Box>
          <Box sx={{
            marginTop: '-1px',
            height: '1px',
            backgroundColor: isFocusIdNum ? '#222222' : '#CCCCCC',
          }}/>
        </InputAreaStyle>
        <Box display="flex" marginTop="45px">
          <InfoOutlinedIcon
            onClick={handleClickFAQ}
            sx={{
              fontSize: '18px',
              color: '#666666',
            }}/>
          <Typography
            onClick={handleClickFAQ}
            sx={{
              fontStyle: 'normal',
              fontWeight: 400,
              fontSize: '14px',
              lineHeight: '20px',
              letterSpacing: '-0.02em',
              color: '#666666',
              paddingLeft: '3px',
            }}>
            {SIGN_UP_PHONE.HELP_AUTH}
          </Typography>
        </Box>
      </Box>

      <IFrameDialog
        open={openFAQ}
        title={MY_SETTING.FAQ.HEADER_TITLE}
        url={PATH_DONOTS_STATIC_PAGE.FAQ()}
        onBack={handleClickBack}
      />
      {showFAQDetail && <FAQDetail open={showFAQDetail} onBack={() => setShowFAQDetail(false)}/>}

    </Box>
  );
}
