import {Box, InputAdornment, Typography} from "@mui/material";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {useRecoilState, useSetRecoilState} from "recoil";
import {SIGN_UP_EMAIL} from "../../../../../constant/sign-up/SignUp";
import {MY_COMMON} from "../../../../../constant/my/MyConstants";
import {myInformationFooterBottomButtonState} from "../../../../../recoil/atom/my/information/footer";
import {myEmailDuplicatedState, myEmailErrorState, myEmailHelperTextState,} from "../../../../../recoil/atom/my/information/myState";
import {ICON_TYPE, SvgCommonIcons} from "../../../../../constant/icons/ImageIcons";
import {ALT_STRING} from "../../../../../constant/common/AltString";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";

export default function MyEmailInputForm({email, setEmail}) {
  // Email 정보 state
  // const [email, setEmail] = useRecoilState(myEmailSelector);
  // 입력 상태 state
  const [isTrue, setIsTrue] = useRecoilState(myEmailErrorState);
  const [isDuplicated, setIsDuplicated] = useRecoilState(myEmailDuplicatedState);
  const [helperText, setHelperText] = useRecoilState(myEmailHelperTextState);
  // const [helperText, setHelperText] = useState(SIGN_UP_EMAIL.HELPER_TEXT);
  const [cancelIcon, setCancelIcon] = useState(false);
  const emailInput = useRef(null);
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(myInformationFooterBottomButtonState);
  // Email 값 변경 제어
  const handleChangeValue = (e) => setEmail(e.target.value.toLowerCase())
  // Email 정규식 체크
  const checkEmailValidation = () => {
    // @ 와 .이 포함되어야 함
    if (email?.includes('@') && email?.includes('.')) {
      setIsTrue(true);
      setIsBottomBtnDisabled(false);
    } else {
      setIsTrue(false);
      setHelperText(SIGN_UP_EMAIL.HELPER_TEXT);
      setIsBottomBtnDisabled(true);
    }
  }
  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true);
  const handleBlur = () => setCancelIcon(false);
  const handleCancel = () => setEmail('');

  useEffect(() => {
    checkEmailValidation();
  }, [email]);

  useEffect(() => {
    if (isDuplicated) {
      emailInput.current.focus();
      setIsDuplicated(false)
    }
  }, [isDuplicated]);

  return (
    <Box>
      <Typography fontWeight="700" fontSize="14px" sx={{lineHeight: '20px', color: '#888888', mt: '14px', mb: '25px'}}>
        {MY_COMMON.TEXT.EMAIL}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        inputRef={emailInput}
        fullWidth
        value={email}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        helperText={email === '' ? helperText : isTrue ? ' ' : helperText}
        inputProps={{tabIndex: 1, style: {padding: 0}, title: ALT_STRING.MY.INPUT_EMAIL}}
        InputProps={{
          endAdornment: email && cancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
            </InputAdornment>
          ),
          style: {
            paddingBottom: '13px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
          },
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#CCCCCC',
          }
        }}
        FormHelperTextProps={{
          style: {
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            color: email === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          '& .MuiInput-root:before': {
            borderBottom: email === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: email === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: email === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: email === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: email === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  );
}