import {Box, InputAdornment, Typography} from "@mui/material";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {useRecoilState, useSetRecoilState} from "recoil";
import {
  myNicknameDuplicatedState,
  myNicknameErrorState,
  myNicknameHelperTextState,
} from "../../../../../recoil/atom/my/information/myState";
import {SIGN_UP_NICKNAME} from "../../../../../constant/sign-up/SignUp";
import {MY_COMMON} from "../../../../../constant/my/MyConstants";
import {myInformationFooterBottomButtonState} from "../../../../../recoil/atom/my/information/footer";
import {ICON_TYPE, SvgCommonIcons} from "../../../../../constant/icons/ImageIcons";
import {ALT_STRING} from "../../../../../constant/common/AltString";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";

export default function MyNicknameInputForm({nickname, setNickname}) {
  // Nickname 정보 state
  // const [nickname, setNickname] = useRecoilState(myNicknameSelector);
  // 입력 상태 state
  const [isTrue, setIsTrue] = useRecoilState(myNicknameErrorState);
  const [isDuplicated, setIsDuplicated] = useRecoilState(myNicknameDuplicatedState);
  const [helperText, setHelperText] = useRecoilState(myNicknameHelperTextState);
  // const [helperText, setHelperText] = useState(SIGN_UP_NICKNAME.HELPER_TEXT);
  const [cancelIcon, setCancelIcon] = useState(false);
  const nicknameInput = useRef(null);
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(myInformationFooterBottomButtonState);
  // Nickname 값 변경 제어
  const handleChangeValue = (e) => setNickname(e.target.value.substring(0, 10))
  // Nickname 정규식 체크
  const checkNicknameValidation = () => {
    // 영문/한글/숫자 포함 10자리 이내 정규식
    const regex = /^[ㄱ-ㅎ가-힣a-zA-Z0-9]{2,10}$/g;
    if (regex.test(nickname)) {
      setIsTrue(true);
      setIsBottomBtnDisabled(false);
    } else {
      setIsTrue(false);
      setHelperText(SIGN_UP_NICKNAME.HELPER_TEXT);
      setIsBottomBtnDisabled(true);
    }
  }
  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true);
  const handleBlur = () => setCancelIcon(false);
  const handleCancel = () => setNickname('');

  useEffect(() => {
    checkNicknameValidation();
  }, [nickname]);

  useEffect(() => {
    if (isDuplicated) {
      nicknameInput.current.focus();
      setIsDuplicated(false)
    }
  }, [isDuplicated]);

  return (
    <Box>
      <Typography fontWeight="700" fontSize="14px" sx={{lineHeight: '20px', color: '#888888', mt: '14px', mb: '25px'}}>
        {MY_COMMON.TEXT.NAME_NICKNAME}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        inputRef={nicknameInput}
        fullWidth
        value={nickname}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        helperText={nickname === '' ? helperText : isTrue ? ' ' : helperText}
        inputProps={{tabIndex: 1, style: {padding: 0}, title: ALT_STRING.MY.INPUT_NICKNAME}}
        InputProps={{
          endAdornment: nickname && cancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
            </InputAdornment>
          ),
          style: {
            paddingBottom: '13px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
          },
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#CCCCCC',
          }
        }}
        FormHelperTextProps={{
          style: {
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            color: nickname === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          '& .MuiInput-root:before': {
            borderBottom: nickname === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: nickname === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: nickname === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: nickname === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: nickname === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  );
}