import {Box, Typography} from "@mui/material";
import * as React from "react";
import {useEffect, useState} from "react";
import {MY_COMMON} from "../../../../../constant/my/MyConstants";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";
import {ALT_STRING} from "../../../../../constant/common/AltString";

export default function MyPhoneNumberInputForm({phoneNumber, setPhoneNumber, handleClickChangePhone}) {
  // phone 정보 state
  // const [phoneNumber, setPhoneNumber] = useRecoilState(myPhoneNumberSelector);
  const [phoneMaskingStr, setPhoneMaskingStr] = useState("");

  const setMasking = (phoneStr) => {
    let maskingStr = phoneStr;
    if (/-[0-9]{3}-/.test(phoneStr)) { // 1) 00-000-0000
      maskingStr = phoneStr.toString().replace(/-[0-9]{3}-/g, "-***-");
    } else if (/-[0-9]{4}-/.test(phoneStr)) { // 2) 00-0000-0000
      maskingStr = phoneStr.toString().replace(/-[0-9]{4}-/g, "-****-");
    }
    return maskingStr
  }

  useEffect(() => {
    setPhoneMaskingStr(setMasking(phoneNumber));
  }, [phoneNumber])

  return (
    <Box>
      <Typography fontWeight="700" fontSize="14px" sx={{lineHeight: '20px', color: '#888888', mt: '14px', mb: '25px'}}>
        {MY_COMMON.TEXT.PHONE}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={phoneNumber}
        variant="standard"
        inputProps={{style: {padding: 0}, title: ALT_STRING.MY.INPUT_PHONE}}
        InputProps={{
          // endAdornment: (
          //   <Button
          //     disableRipple
          //     sx={{
          //       px: '12px',
          //       py: '8px',
          //       width: '80px',
          //       height: '28px',
          //       background: '#222222',
          //       borderRadius: '24px',
          //       '&:hover': {boxShadow: 'none', backgroundColor: 'black'}
          //     }} onClick={handleClickChangePhone}>
          //     <Typography fontWeight="400" fontSize="12px" sx={{lineHeight: '18px', color: 'white',}}>
          //       {MY_COMMON.TEXT.CERTIFICATION}
          //     </Typography>
          //   </Button>
          // ),
          style: {
            paddingBottom: '13px',
            marginBottom: '26px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#CCCCCC',
          },
          readOnly: true,
        }}
        sx={{
          '& .MuiInput-root:before': {
            borderBottom: '1px solid #CCCCCC',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: '1px solid #CCCCCC',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: '1px solid #CCCCCC',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: '1px solid #CCCCCC',
          },
          '& .MuiInput-root:after': {
            borderBottom: '1px solid #CCCCCC',
          },
        }}
      />
    </Box>
  );
}