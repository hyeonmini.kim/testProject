import {Box, InputAdornment, TextField, Typography} from "@mui/material";
import * as React from "react";
import {useState} from "react";
import {useSetRecoilState} from "recoil";
import {MY_COMMON} from "../../../../../constant/my/MyConstants";
import {authBottomButtonState} from "../../../../../recoil/atom/sign-up/auth";
import {ICON_TYPE, SvgCommonIcons} from "../../../../../constant/icons/ImageIcons";
import {ALT_STRING} from "../../../../../constant/common/AltString";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";

export default function MySnsLinkInputForm({socialMediaUrl, setSocialMediaUrl, type, grade}) {
  // const [grade, setGrade] = useRecoilState(myGradeSelector);
  // const [socialMediaUrl, setSocialMediaUrl] = useRecoilState(mySocialMediaUrlSelector);
  // 입력 상태 state
  const [cancelIcon, setCancelIcon] = useState(false);
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState);
  // snsLink 값 변경 제어
  const handleChangeValue = (e) => {
    const value = e.target.value;

    setSocialMediaUrl(value);
  }
  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true);
  const handleBlur = () => setCancelIcon(false);
  const handleCancel = () => setSocialMediaUrl('');

  return (
    <Box>
      <Typography fontWeight="700" fontSize="14px" sx={{lineHeight: '20px', color: '#888888', mt: '14px', mb: '25px'}}>
        {MY_COMMON.TEXT.SNS_LINK}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={socialMediaUrl}
        placeholder={(type === "NORMAL_MEMBER" && (grade === "LV1" || grade === "LV2")) ? MY_COMMON.TEXT.LEVEL_MESSEAGE : ''}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        inputProps={{style: {padding: 0}, title: ALT_STRING.MY.INPUT_LINK}}
        InputProps={{
          endAdornment: socialMediaUrl && cancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
            </InputAdornment>
          ),
          style: {
            paddingBottom: '13px',
            marginBottom: '26px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
          },
          readOnly: type === "NORMAL_MEMBER" && (grade === "LV1" || grade === "LV2"),
        }}
        sx={{
          '& input::placeholder': {
            color: '#CCCCCC'
          },
          '& .MuiInput-root:before': {
            borderBottom: (type === "NORMAL_MEMBER" && (grade === "LV1" || grade === "LV2")) && '1px solid #CCCCCC',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: (type === "NORMAL_MEMBER" && (grade === "LV1" || grade === "LV2")) && '1px solid #CCCCCC',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: (type === "NORMAL_MEMBER" && (grade === "LV1" || grade === "LV2")) && '1px solid #CCCCCC',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: (type === "NORMAL_MEMBER" && (grade === "LV1" || grade === "LV2")) && '1px solid #CCCCCC',
          },
          '& .MuiInput-root:after': {
            borderBottom: (type === "NORMAL_MEMBER" && (grade === "LV1" || grade === "LV2")) && '1px solid #CCCCCC',
          },
        }}
      />
    </Box>
  );
}