import {Box, Typography} from "@mui/material";
import * as React from "react";
import {MY_COMMON} from "../../../../../constant/my/MyConstants";
import TextFieldWrapper from "../../../../../components/common/TextFieldWrapper";
import {ALT_STRING} from "../../../../../constant/common/AltString";

export default function MyBirthInputForm({birthDay, setBirthDay}) {
  // birth 정보 state
  // const [birthDay, setBirthDay] = useRecoilState(myBirthDaySelector);

  return (
    <Box>
      <Typography fontWeight="700" fontSize="14px" sx={{lineHeight: '20px', color: '#888888', mt: '14px', mb: '25px'}}>
        {MY_COMMON.TEXT.BIRTH}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={birthDay}
        variant="standard"
        inputProps={{style: {padding: 0}, title: ALT_STRING.MY.INPUT_BIRTH}}
        InputProps={{
          style: {
            paddingBottom: '13px',
            marginBottom: '26px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#CCCCCC',
          },
          readOnly: true,
        }}
        sx={{
          '& .MuiInput-root:before': {
            borderBottom: '1px solid #CCCCCC',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: '1px solid #CCCCCC',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: '1px solid #CCCCCC',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: '1px solid #CCCCCC',
          },
          '& .MuiInput-root:after': {
            borderBottom: '1px solid #CCCCCC',
          },
        }}
      />
    </Box>
  );
}