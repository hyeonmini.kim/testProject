import {Box, Divider, Stack, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import * as React from "react";
import {useState} from "react";
import {callOpenNativeGalleryChannel} from "../../../../channels/photoChannel";
import {isNative} from "../../../../utils/envUtils";
import {bytesToImageFile} from "../../../../utils/fileUtils";
import {myChildrenBottomDialogState,} from "../../../../recoil/atom/my/information/myBottomDialog";
import UploadChildrenAvatar from "../../../../components/my/upload/UploadChildrenAvatar";
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import {myBabyIsProfilePictureUrlDeletedSelector} from "../../../../recoil/selector/my/myDataSelector";
import imageCompression from "browser-image-compression";
import {clickBorderNone, IMAGE_COMPRESSION_OPTIONS} from "../../../../constant/common/Common";
import LoadingScreen from "../../../../components/common/LoadingScreen";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";

export default function ChildrenBottomContents({profilePictureUrl, setProfilePictureUrl}) {
  const setIsChildrenBottomOpen = useSetRecoilState(myChildrenBottomDialogState);
  const [myBabyIsProfilePictureUrlDeleted, setMyBabyIsProfilePictureUrlDeleted] = useRecoilState(myBabyIsProfilePictureUrlDeletedSelector);
  const [isCompress, setIsCompress] = useState(false)

  const handleClickDelete = (e) => {
    setIsChildrenBottomOpen(false);
    setProfilePictureUrl("");
    setMyBabyIsProfilePictureUrlDeleted(true);
  }

  const setFileWithImage = async (image) => {
    setIsChildrenBottomOpen(false);
    if (isNative()) {
      image = bytesToImageFile(image)
    } else {
      setIsCompress(true)
      const resizingBlob = await imageCompression(image, IMAGE_COMPRESSION_OPTIONS);
      image = new File([resizingBlob], image.name, {type: image.type});
      setIsCompress(false)
    }
    const newImage = Object.assign(image, {
      preview: URL.createObjectURL(image),
    })
    setProfilePictureUrl(newImage);
  }

  const handleDrop = (images) => {
    setIsChildrenBottomOpen(false);
    setMyBabyIsProfilePictureUrlDeleted(false);
    setFileWithImage(images[0])
  }

  const handleGalleryChannel = () => {
    setIsChildrenBottomOpen(false);
    setMyBabyIsProfilePictureUrlDeleted(false);
    document.activeElement.blur()
    callOpenNativeGalleryChannel(0, 1, (images) => setFileWithImage(images[0]))
  }

  return (
    <>
      <Box sx={{mt: '6px', textAlign: 'start'}}>
        <Typography
          sx={{
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
            paddingY: '20px'
          }}
        >
          프로필 사진 선택하기
        </Typography>
        <Divider sx={{height: '1px', backgroundColor: '#E6E6E6'}}/>
        {isNative()
          ? <UploadChildrenAvatar profilePictureUrl={profilePictureUrl}
                                  onClick={handleGalleryChannel}/>
          : <UploadChildrenAvatar profilePictureUrl={profilePictureUrl}
                                  onDrop={(images) => handleDrop(images)}/>
        }
        <Stack
          tabIndex={1}
          direction="row"
          sx={{
            alignItems: 'center',
            ...clickBorderNone
          }}
          onClick={handleClickDelete}
          onKeyDown={(e) => handleEnterPress(e, handleClickDelete)}
        >
          <SvgCommonIcons type={ICON_TYPE.DELETE_PHOTO}/>
          <Typography
            value="사진 삭제"
            sx={{
              fontStyle: 'normal',
              fontWeight: 500,
              fontSize: '16px',
              lineHeight: '24px',
              color: '#222222',
              paddingX: '10px',
              paddingY: '20px',
            }}
          >
            사진 삭제
          </Typography>
        </Stack>
      </Box>
      {isCompress && <LoadingScreen dialog/>}
    </>
  );
}