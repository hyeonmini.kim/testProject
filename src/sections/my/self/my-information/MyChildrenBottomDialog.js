import {DialogContent, Slide,} from "@mui/material";
import * as React from "react";
import {useRecoilState} from "recoil";
import {myChildrenBottomDialogState,} from "../../../../recoil/atom/my/information/myBottomDialog";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";

const transition = React.forwardRef(
  function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  }
);

export default function MyChildrenBottomDialog({children}) {
  const [isChildrenBottomOpen, setIsChildrenBottomOpen] = useRecoilState(myChildrenBottomDialogState);

  const handleClose = (event, reason) => {
    setIsChildrenBottomOpen(false);
  }

  return (
    <MainDialogLayout
      open={isChildrenBottomOpen}
      onClose={handleClose}
      TransitionComponent={transition}
      disableScrollLock={true}
      fullWidth
      PaperProps={{
        style: {
          margin: 0,
          borderRadius: '16px 16px 0 0',
          position: 'fixed',
          bottom: 0,
          backgroundColor: '#FFFFFF',
        },
      }}
      sx={{
        zIndex: Z_INDEX.BOTTOM_DIALOG, '& .MuiDialog-paper': {mx: 0}
      }}
    >
      <DialogContent sx={{margin: '0 20px 6px 20px', padding: 0, textAlign: '-webkit-center'}}>
        {children}
      </DialogContent>
    </MainDialogLayout>
  );
}