import {Box, Divider, Stack, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import * as React from "react";
import {myFilterBottomDialogState,} from "../../../../recoil/atom/my/information/myBottomDialog";
import {myIsSelectedFilterSelector, mySelectedFilterSelector} from "../../../../recoil/selector/my/information/myStateSelector";
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import {clickBorderNone} from "../../../../constant/common/Common";
import {handleEnterPress} from "../../../../utils/onKeyDownUtils";

export default function FilterContents() {
  const setIsOpen = useSetRecoilState(myFilterBottomDialogState);
  const [selectedFilter, setSelectedFilter] = useRecoilState(mySelectedFilterSelector);
  const [isSelectedFilter, setIsSelectedFilter] = useRecoilState(myIsSelectedFilterSelector);

  const handleClick = (e, text) => {
    e.preventDefault()

    setIsOpen(false);
    if (text === "전체") {
      setIsSelectedFilter(false);
    } else {
      setIsSelectedFilter(true);
    }
    setSelectedFilter(text)
  }

  return (
    <Box sx={{mt: '6px', textAlign: 'start'}}>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 500,
          fontSize: '16px',
          lineHeight: '24px',
          color: '#222222',
          paddingY: '20px'
        }}
      >
        필터
      </Typography>
      <Divider sx={{height: '1px', backgroundColor: '#E6E6E6'}}/>
      <Stack
        tabIndex={9}
        direction="row"
        sx={{
          alignItems: 'center',
          ...clickBorderNone
        }}
        onClick={(e) => handleClick(e, "전체")}
        onKeyDown={(e) => handleEnterPress(e, (e) => handleClick(e, "전체"))}
      >
        <SvgCommonIcons type={ICON_TYPE.FILTER_ALL}/>
        <Typography
          value="전체"
          sx={{
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
            paddingX: '10px',
            paddingY: '20px',
          }}
        >
          전체
        </Typography>
      </Stack>

      <Stack
        tabIndex={9}
        direction="row"
        sx={{
          alignItems: 'center',
          ...clickBorderNone
        }}
        onClick={(e) => handleClick(e, "임시저장")}
        onKeyDown={(e) => handleEnterPress(e, (e) => handleClick(e, "임시저장"))}
      >
        <SvgCommonIcons type={ICON_TYPE.FILTER_TEMP_SAVE}/>
        <Typography
          value="임시저장"
          sx={{
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
            paddingX: '10px',
            paddingY: '20px',
          }}
        >
          임시저장
        </Typography>
      </Stack>

      <Stack
        tabIndex={9}
        direction="row"
        sx={{
          alignItems: 'center',
          ...clickBorderNone
        }}
        onClick={(e) => handleClick(e, "반려")}
        onKeyDown={(e) => handleEnterPress(e, (e) => handleClick(e, "반려"))}
      >
        <SvgCommonIcons type={ICON_TYPE.FILTER_REJECT}/>
        <Typography
          value="반려"
          sx={{
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
            paddingX: '10px',
            paddingY: '20px',
          }}
        >
          반려
        </Typography>
      </Stack>

      <Stack
        tabIndex={9}
        direction="row"
        sx={{
          alignItems: 'center',
          ...clickBorderNone
        }}
        onClick={(e) => handleClick(e, "검수중")}
        onKeyDown={(e) => handleEnterPress(e, (e) => handleClick(e, "검수중"))}
      >
        <SvgCommonIcons type={ICON_TYPE.FILTER_INSPECTING}/>
        <Typography
          value="검수중"
          sx={{
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
            paddingX: '10px',
            paddingY: '20px',
          }}
        >
          검수중
        </Typography>
      </Stack>

      <Stack
        tabIndex={9}
        direction="row"
        sx={{
          alignItems: 'center',
          ...clickBorderNone
        }}
        onClick={(e) => handleClick(e, "게시중")}
        onKeyDown={(e) => handleEnterPress(e, (e) => handleClick(e, "게시중"))}
      >
        <SvgCommonIcons type={ICON_TYPE.FILTER_COMPLETE}/>
        <Typography
          value="게시중"
          sx={{
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
            paddingX: '10px',
            paddingY: '20px',
          }}
        >
          게시중
        </Typography>
      </Stack>
    </Box>
  );
}