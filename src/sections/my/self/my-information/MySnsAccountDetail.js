import {useEffect} from 'react';
// @mui
import {Avatar, Box, Button, Container, Fade, Stack, Typography,} from '@mui/material';
// components
import {useRecoilState, useSetRecoilState} from "recoil";
import {MY_BTN_TEXT, MY_COMMON} from "../../../../constant/my/MyConstants";
import {forMyPageEntrySelector} from "../../../../recoil/selector/auth/userSelector";
import {COMMON_DIALOG_TYPE, COMMON_STR} from "../../../../constant/common/Common";
import {SIGN_UP_DIALOG} from "../../../../constant/sign-up/SignUp";
import {dialogSelector, dialogShowDialogSelector} from "../../../../recoil/selector/common/dialogSelector";
import {myOpenSnsDetailSelector} from "../../../../recoil/selector/my/popup/myPopupSelector";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {ICON_TYPE, SvgCommonIcons} from "../../../../constant/icons/ImageIcons";
import {parseProvider} from "../../../../constant/my/Setting";
import {getEnv} from "../../../../utils/envUtils";
import {useRouter} from "next/router";
import {postGetUuidMutate} from "../../../../api/authApi";
import {deleteRemoveSnsAccountMutate} from "../../../../api/myApi";
import {useQueryClient} from "react-query";
import {toastMessageSelector} from "../../../../recoil/selector/common/toastSelector";
import {setCookie} from "../../../../utils/storageUtils";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import MyHeaderDialogLayout from "../../../../layouts/my/MyHeaderDialogLayout";
import {PATH_DONOTS_API, PATH_DONOTS_PAGE} from "../../../../routes/paths";
import useNativeBackKey from "../../../../hooks/useNativeBackKey";

export default function MySnsAccountDetail({snsList, inform}) {
  const [user, setUser] = useRecoilState(forMyPageEntrySelector);
  const [openSnsDetail, setOpenSnsDetail] = useRecoilState(myOpenSnsDetailSelector);
  const setDialogMessage = useSetRecoilState(dialogSelector)
  const setToastMessage = useSetRecoilState(toastMessageSelector)

  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)

  const router = useRouter()
  const {mutate: mutatePostGetUuid} = postGetUuidMutate()
  const {mutate: mutateDeleteRemoveSnsAccount} = deleteRemoveSnsAccountMutate()

  const useQuery = useQueryClient()

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      if (showDialog) {
        setShowDialog(false)
      }
    }
  }, [handleBackKey])

  const handleBackButton = () => {
    setOpenSnsDetail(false);
  };

  const handleClickCloseSnsDetail = () => {
    setOpenSnsDetail(false);
  };

  const handleConfirmClick = () => {
    if (inform?.isConnectionStatus) {
      let checkConnectedSnsCount = 0;
      snsList?.map((item, index) => {
        if (item?.isConnectionStatus) {
          checkConnectedSnsCount++;
        }
      })

      if (checkConnectedSnsCount <= 1 && inform?.isConnectionStatus) {
        setDialogMessage({
          type: COMMON_DIALOG_TYPE.ONE_BUTTON,
          message: MY_COMMON.TEXT.NOTICE_WITHDRAW,
          subMessage: MY_COMMON.TEXT.NOTICE_WITHDRAW_PATH,
          handleButton1: {text: SIGN_UP_DIALOG.CONFIRM},
        })
      } else {
        // 계정 연동 해제 API
        mutateDeleteRemoveSnsAccount({
          socialAccountConnectionStatusKey: inform?.socialAccountConnectionStatusKey
        }, {
          onSuccess: (result) => {
            useQuery.invalidateQueries('getMySnsList').then(() => setOpenSnsDetail(false))
          }
        })
      }
    } else {
      // 계정 연동 추가 API
      mutatePostGetUuid({}, {
        onSuccess: async (uuid) => {
          setCookie(COMMON_STR.UUID, uuid, 1)
          setCookie(COMMON_STR.CALLBACK_TYPE, COMMON_STR.SNS, 1)

          await router.push(PATH_DONOTS_PAGE.AUTH.SNS(
            getEnv(), {
              type: inform?.providerType?.toLowerCase(),
              redirectUrl: PATH_DONOTS_API.AUTH.SNS_CALLBACK(getEnv()),
              uuid: uuid
            }))
        }
      })
    }
  }

  return (
    <MainDialogLayout
      fullScreen open={openSnsDetail} onClose={handleClickCloseSnsDetail} TransitionComponent={Fade}
      sx={{'& .MuiDialog-paper': {mx: 0, height: '100%', backgroundColor: 'white'}, zIndex: Z_INDEX.DIALOG}}
    >
      <MyHeaderDialogLayout
        title={parseProvider(inform?.providerType)}
        onBack={handleBackButton}
      >

        <Container maxWidth={'xs'} disableGutters sx={{
          px: '20px',
          pb: '98px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%'
        }}>
          <Stack
            alignItems="center"
          >
            {inform?.isConnectionStatus ? (
              <>
                <Avatar alt="avatar" src={user?.profilePictureUrl} sx={{width: '80px', height: '80px', mb: '25px'}}/>
                <Typography fontWeight="500" fontSize="16px" sx={{
                  mb: '12px',
                  lineHeight: '16px',
                  color: '#000000',
                  whiteSpace: 'pre-wrap'
                }}>
                  {inform?.email}
                </Typography>
                <Typography fontWeight="400" fontSize="16px"
                            sx={{lineHeight: '16px', color: '#222222', whiteSpace: 'pre-wrap'}}>
                  {MY_COMMON.TEXT.ACCOUNT_CONNECTED_NOW}
                </Typography>
              </>
            ) : (
              <>
                {user?.profilePictureUrl ? (<Avatar alt="avatar" src={user?.profilePictureUrl}
                                                    sx={{width: '80px', height: '80px'}}/>) : (

                  <SvgCommonIcons type={ICON_TYPE.PROFILE_NO_IMAGE_80PX} sx={{width: '80px'}}/>
                )}
                <Typography fontWeight="500" fontSize="16px" sx={{
                  textAlign: 'center',
                  mt: '20px',
                  mb: '10px',
                  lineHeight: '24px',
                  color: '#222222',
                  whiteSpace: 'pre-wrap'
                }}>
                  {MY_COMMON.TEXT.ACCOUNT_NOT_CONNECTED(parseProvider(inform?.providerType))}
                </Typography>
              </>
            )}
          </Stack>
        </Container>

        <Box
          sx={{
            p: '20px',
            position: 'fixed',
            zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
            bgcolor: 'white',
            bottom: 0,
            left: 0,
            right: 0,
          }}
        >
          <Container maxWidth={'xs'} disableGutters>
            <Container
              disableGutters
              sx={{
                mt: '20px',
                maxWidth: 'xs',
              }}
            >
              <Button
                fullWidth
                variant="contained"
                onClick={handleConfirmClick}
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  p: '14px 100.5px',
                  height: '52px',
                  fontStyle: 'normal',
                  fontWeight: '700',
                  fontSize: '16px',
                  lineHeight: '24px',
                  textAlign: 'center',
                  color: '#FFFFFF',
                  '&.Mui-disabled': {
                    color: '#B3B3B3',
                    backgroundColor: '#E6E6E6',
                  },
                  '&.Mui-disabled:hover': {
                    backgroundColor: '#E6E6E6',
                  },
                  '&:hover': {
                    boxShadow: 'none',
                    backgroundColor: 'primary.main',
                  },
                  whiteSpace: 'nowrap'
                }}
              >
                {inform?.isConnectionStatus ? MY_BTN_TEXT.DELETE_CONNECTED_ACCOUNT : MY_BTN_TEXT.CONNECT_ACCOUNT}
              </Button>
            </Container>
          </Container>
        </Box>
      </MyHeaderDialogLayout>

    </MainDialogLayout>
  );
}
