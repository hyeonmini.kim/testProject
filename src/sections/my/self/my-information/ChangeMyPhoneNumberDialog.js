import {Box, Button, Container, Fade} from "@mui/material";
import ChangeMyPhoneInputForm from "./input-form/ChangeMyPhoneInputForm";
import CloseIcon from '@mui/icons-material/Close';
import {HEADER} from "../../../../config";
import {useRecoilState, useRecoilValue, useResetRecoilState, useSetRecoilState} from "recoil";
import {
  authBottomButtonState,
  authBottomButtonTextState,
  authIdEndNumberState,
  authIdFrontNumberState,
  authInputRefIndexState,
  authNameState,
  authPhoneNumberState,
  authTelecomState
} from "../../../../recoil/atom/sign-up/auth";
import {
  commonBottomDialogPageState,
  commonBottomDialogState,
  commonBottomDialogTitleState
} from "../../../../recoil/atom/common/bottomDialog";
import {TOAST_TYPE} from "../../../../constant/common/Common";
import {toastMessageSelector} from "../../../../recoil/selector/common/toastSelector";
import {useMemo, useState} from "react";
import BottomDialog from "../../../../components/common/BottomDialog";
import TelecomContents from "../../../common/TelecomContents";
import MyAuthCodeInputForm from "./input-form/MyAuthCodeInputForm";
import {myOpenChangePhoneSelector} from "../../../../recoil/selector/my/popup/myPopupSelector";
import {Z_INDEX} from "../../../../constant/common/ZIndex";
import {postAuthCodeMutate} from "../../../../api/authApi";
import {
  changeBirthFromYYMMDDToYYYYMMDD,
  changeGenderType,
  changeNewsAgencyType,
  checkTransactionSeqNumberByEnv,
  removeHyphenFromPhoneNumber
} from "../../../../utils/authUtils";
import {MainDialogLayout} from "../../../../layouts/main/MainLayout";
import {transactionSeqNumberSelector} from "../../../../recoil/selector/auth/accessTokenSelector";
import {getEnv} from "../../../../utils/envUtils";
import {authSelector} from "../../../../recoil/selector/sign-up/authSelector";
import {ERRORS, GET_ERROR_CODE, GET_ERROR_MESSAGE} from "../../../../constant/common/Error";

export default function ChangeMyPhoneNumberDialog() {
  const [page, setPage] = useState(0)

  const bottomBtnText = useRecoilValue(authBottomButtonTextState)
  const isBottomBtnDisabled = useRecoilValue(authBottomButtonState)
  const setDialogPage = useSetRecoilState(commonBottomDialogPageState)
  const dialogPage = useRecoilValue(commonBottomDialogPageState)

  const name = useRecoilValue(authNameState)
  const telecom = useRecoilValue(authTelecomState)
  const phone = useRecoilValue(authPhoneNumberState)
  const idFront = useRecoilValue(authIdFrontNumberState)
  const idEnd = useRecoilValue(authIdEndNumberState)

  const [transactionSeqNumber, setTransactionSeqNumber] = useRecoilState(transactionSeqNumberSelector)

  const setIndex = useSetRecoilState(authInputRefIndexState)
  const setIsBottomDialogOpen = useSetRecoilState(commonBottomDialogState)
  const setBottomDialogTitle = useSetRecoilState(commonBottomDialogTitleState)

  const resetAuthInform = useResetRecoilState(authSelector)

  const setToastMessage = useSetRecoilState(toastMessageSelector)
  const [openChangePhone, setOpenChangePhone] = useRecoilState(myOpenChangePhoneSelector)

  const env = getEnv()
  const {mutate: mutatePostAuthCode} = postAuthCodeMutate()

  const handleClose = () => {
    resetAuthInform()
    setOpenChangePhone(false)
  }
  const handleConfirmClick = () => {
    if (bottomBtnText === '다음') {
      if (name === '') {
        // 이름에 포커스
        setIndex(0)
        return
      }
      if (telecom === '') {
        setIsBottomDialogOpen(true)
        setBottomDialogTitle(false)
        return
      }
      if (phone === '') {
        // 휴대폰 번호에 포커스
        setIndex(1)
        return
      }
      if (idFront === '') {
        // 주민번호 앞자리에 포커스
        setIndex(2)
        return
      }
      if (idEnd === '') {
        // 주민번호 뒷자리에 포커스
        setIndex(3)
      }
    } else if (bottomBtnText === '확인') {
      /* API : 본인인증 입력 정보 검사 및 인증번호 받기 */
      const newsAgencyType = changeNewsAgencyType(telecom)
      const gender = changeGenderType(idEnd)
      const birth = changeBirthFromYYMMDDToYYYYMMDD(idFront, idEnd)
      const phoneNumber = removeHyphenFromPhoneNumber(phone)
      mutatePostAuthCode({
        name: name,
        phoneNumber: phoneNumber,
        birthday: birth,
        gender: gender,
        koreaForeignerType: '1',
        newsAgencyType: newsAgencyType,
      }, {
        onSuccess: (result) => {
          setTransactionSeqNumber(result?.transactionSeqNumber)
          /* Further Development */
          if (checkTransactionSeqNumberByEnv(result?.transactionSeqNumber, env)) {
            // 본인인증 정보 성공
            setDialogPage(1)
            setIsBottomDialogOpen(true)
            setBottomDialogTitle(true)
          } else {
            // 본인인증 정보 에러
            setToastMessage({
              type: TOAST_TYPE.BOTTOM_HEADER,
              message: '입력하신 정보가 올바르지 않아요.\n다시 한번 확인 후 입력해 주세요.',
            })
          }
        },
        onError: async (error) => {
          switch (GET_ERROR_CODE(error)) {
            case ERRORS.INVALID_NAME_OR_RESIDENT_REGISTRATION_NUMBER:
            case ERRORS.SERVICE_UNAVAILABLE:
            case ERRORS.MISCELLANEOUS:
              setToastMessage({
                type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
                message: GET_ERROR_MESSAGE(error),
              })
              break
          }
        }
      })
    }
  }

  return (
    <MainDialogLayout fullScreen open={openChangePhone} onClose={handleClose} TransitionComponent={Fade}
                      sx={{'& .MuiDialog-paper': {mx: 0, height: '100%', backgroundColor: 'white'}, zIndex: Z_INDEX.DIALOG}}>

      <Container maxWidth={'xs'} sx={{height: HEADER.H_MOBILE, textAlign: 'right'}}>
        <Box sx={{paddingY: '15px'}} onClick={handleClose}>
          <CloseIcon/>
        </Box>
      </Container>

      <Container disableGutters maxWidth={'xs'} sx={{px: '20px'}}>
        <ChangeMyPhoneInputForm setPage={setPage}/>
      </Container>

      <Container
        disableGutters
        maxWidth={'xs'}
        sx={{
          position: 'fixed',
          bottom: 0,
          left: 0,
          right: 0,
          paddingX: '20px',
          marginBottom: '20px',
        }}
      >
        <Button
          fullWidth
          variant="contained"
          disabled={isBottomBtnDisabled}
          onClick={handleConfirmClick}
          sx={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            p: '12px 50px',
            height: '52px',
            fontStyle: 'normal',
            fontWeight: '700',
            fontSize: '16px',
            lineHeight: '28px',
            textAlign: 'center',
            color: '#FFFFFF',
            '&.Mui-disabled': {
              color: '#B3B3B3',
              backgroundColor: '#E6E6E6',
            },
            '&.Mui-disabled:hover': {
              backgroundColor: '#E6E6E6',
            },
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: 'primary.main',
            },
          }}
        >
          {bottomBtnText}
        </Button>
      </Container>

      {/* 바텀 다이얼로그 */}
      {useMemo(() => {
        return (
          <BottomDialog>
            {dialogPage === 0 && <TelecomContents/>}
            {dialogPage === 1 && <MyAuthCodeInputForm setPage={setPage} setOpenPhoneDialog={setOpenChangePhone}/>}
          </BottomDialog>
        );
      }, [dialogPage])}

    </MainDialogLayout>
  )
}