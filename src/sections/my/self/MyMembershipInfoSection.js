import {Box, Skeleton, Stack, Typography} from "@mui/material";
import {useRecoilState} from "recoil";
import {myOpenMomStarSelector, myOpenMyInformationPopupSelector} from "../../../recoil/selector/my/popup/myPopupSelector";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {MY_COMMON, MY_GRADE, MY_GRADE_TYPE, MY_MEMBERSHIP} from "../../../constant/my/MyConstants";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {USER_GRADE} from "../../../constant/common/User";
import usePreRecipeCreate from "../../../hooks/usePreRecipeCreate";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";

export default function MyMembershipInfoSection({userGradeInfo}) {
  return (
    <>
      {
        !userGradeInfo?.grade
          ? <MyMembershipInfoSkeleton/>
          : <MyMembershipInfoDataSection userGradeInfo={userGradeInfo}/>
      }
    </>
  )
}

function MyMembershipInfoSkeleton() {
  return (
    <Box sx={{mt: 3, alignItems: 'center', mx: '20px'}}>
      <Skeleton variant="rounded" height="132px" width="100%" sx={{mb: '20px', borderRadius: '8px'}}/>
    </Box>
  )
}

function MyMembershipInfoDataSection({userGradeInfo}) {

  const [openMomStar, setOpenMomStar] = useRecoilState(myOpenMomStarSelector)
  const [openMy, setOpenMy] = useRecoilState(myOpenMyInformationPopupSelector)

  const {handleRecipe} = usePreRecipeCreate()

  let gradeContents = MY_MEMBERSHIP.DOUGH

  if (userGradeInfo?.type === MY_GRADE_TYPE.NORMAL) {
    switch (userGradeInfo?.grade) {
      case USER_GRADE.LV2.GRADE:
        gradeContents = MY_MEMBERSHIP.CHOCO
        break
      case USER_GRADE.LV3.GRADE:
        gradeContents = MY_MEMBERSHIP.CRUNCHY_KING
        break
      default:
        break
    }
  } else {
    gradeContents = MY_MEMBERSHIP.EXPERT
  }

  const handleClickMembership = (event) => {
    event.stopPropagation();
    setHackleTrack(HACKLE_TRACK.MYPAGE_MEMBERSHIP)
    setOpenMomStar(true)
  }

  const handleClickLevelUp = () => {
    if (userGradeInfo?.type === MY_GRADE_TYPE.NORMAL) {
      switch (userGradeInfo?.grade) {
        case USER_GRADE.LV1.GRADE:
          setHackleTrack(HACKLE_TRACK.MY_LEVEL_DOUGH)
          setOpenMy(true)
          break

        case USER_GRADE.LV2.GRADE:
          setHackleTrack(HACKLE_TRACK.MY_LEVEL_CHOCO)
          handleRecipe()
          break

        case USER_GRADE.LV3.GRADE:
          setHackleTrack(HACKLE_TRACK.MY_LEVEL_KING)
          setOpenMy(true)
          break
        default:
          setOpenMy(true)
          break
      }
    } else {
      setOpenMy(true)
    }
  }

  return (
    <Box sx={{mx: '20px', mb: '30px', py: '20px', backgroundColor: '#FAFAFA', borderRadius: '8px'}}>

      <Stack direction={'row'} sx={{mx: '16px', mb: '10px', justifyContent: 'space-between'}}>

        <LevelSection type={userGradeInfo?.type} grade={userGradeInfo?.grade}/>

        <Stack
          tabIndex={5}
          direction={'row'}
          sx={{mr: '7px', alignItems: 'center'}}
          onKeyDown={(e) => handleEnterPress(e, handleClickMembership)}
          onClick={handleClickMembership}
        >
          <Typography variant={'b2_14_r'} sx={{color: '#666666'}}>{MY_COMMON.TEXT.MEMBERSHIP_INFO}</Typography>
          <SvgCommonIcons type={ICON_TYPE?.ARROW_RIGHT_16PX}/>
        </Stack>

      </Stack>

      <Box sx={{mx: '16px', p: '20px', backgroundColor: '#FFF4E5', borderRadius: '4px'}}>

        <Stack direction={'row'} sx={{mb: '17px'}}>
          <Typography variant={'b1_16_b_1l'} sx={{color: '#ECA548'}}>{gradeContents?.TEXT_1}</Typography>
          <Typography variant={'b1_16_b_1l'}>{gradeContents?.TEXT_2}</Typography>
        </Stack>

        <Stack direction={'row'} sx={{alignItems: 'center', justifyContent: 'space-between'}}>
          <Stack direction={'row'}>
            <Typography variant={'b1_16_r_1l'}>{gradeContents?.SUB_1}</Typography>
            {gradeContents?.SUB_2 !== undefined &&
              <Typography variant={'b1_16_b_1l'} sx={{color: '#ECA548'}}>
                {gradeContents?.SUB_2(userGradeInfo?.posted_recipe_numer, userGradeInfo?.posted_recipe_denom)}
              </Typography>}
            {gradeContents?.SUB_3 !== undefined && <Typography variant={'b1_16_r_1l'}>{gradeContents?.SUB_3}</Typography>}
          </Stack>

          {
            ((userGradeInfo?.type === MY_GRADE_TYPE.NORMAL && userGradeInfo?.grade === USER_GRADE.LV3.GRADE) || userGradeInfo?.type === MY_GRADE_TYPE.EXPERT)
            && userGradeInfo?.has_social_media_url
              ? (<Stack direction={'row'} sx={{alignItems: 'center'}}>
                  <SvgCommonIcons type={ICON_TYPE.CHECK_10PX} sx={{mr: '4px'}}/>
                  <Typography variant={'bs_12_r_1l'} sx={{color: '#ECA548'}}>
                    {gradeContents?.BTN_TEXT(true)}
                  </Typography>
                </Stack>
              )
              : (<Typography
                  tabIndex={6}
                  variant={'b2_14_r_1l'}
                  sx={{textDecoration: 'underline'}}
                  onKeyDown={(e) => handleEnterPress(e, handleClickLevelUp)}
                  onClick={handleClickLevelUp}
                >
                  {gradeContents?.BTN_TEXT(false)}
                </Typography>
              )
          }
        </Stack>

      </Box>

    </Box>
  )
}

function LevelSection({type, grade}) {
  let gradeContents = MY_GRADE.LV1

  if (type === MY_GRADE_TYPE.NORMAL) {
    switch (grade) {
      case USER_GRADE.LV2.GRADE:
        gradeContents = MY_GRADE.LV2
        break
      case USER_GRADE.LV3.GRADE:
        gradeContents = MY_GRADE.LV3
        break
      default:
        break
    }
  } else {
    gradeContents = MY_GRADE.LV4
  }

  return (
    <Stack direction={'row'} sx={{alignItems: 'center'}}>
      <img
        alt={gradeContents?.GRADE_TEXT}
        src={gradeContents?.GRADE_IMAGE}
        style={{width: gradeContents?.IMAGE_WIDTH, height: gradeContents?.IMAGE_HEIGHT, marginRight: '8px'}}
      />
      <Typography variant={'b1_16_b'}>{gradeContents?.GRADE_TEXT}</Typography>
    </Stack>
  )
}
