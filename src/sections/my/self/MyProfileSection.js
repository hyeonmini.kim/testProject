import {Avatar, Box, Divider, IconButton, Skeleton, Stack, Typography} from "@mui/material";
import {MY_BTN_TEXT, MY_COMMON} from "../../../constant/my/MyConstants";
import {PATH_DONOTS_PAGE, PATH_DONOTS_STATIC_PAGE} from "../../../routes/paths";
import {FROM_TYPE, SIGN_UP_DIALOG} from "../../../constant/sign-up/SignUp";
import {useRecoilState, useSetRecoilState} from "recoil";
import {DragDropContext, Droppable} from "react-beautiful-dnd";
import {hideScrollbarX} from "../../../utils/cssStyles";
import DragImage from "../../../components/my/drag/DragImage";
import {useRouter} from "next/router";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {
  myOpenAddChildrenSelector,
  myOpenChildrenSelector,
  myOpenMomStarSelector,
  myOpenMyInformationPopupSelector
} from "../../../recoil/selector/my/popup/myPopupSelector";
import {myIsExpandedAccordionSelector} from "../../../recoil/selector/my/information/myStateSelector";
import LongPressable from 'react-longpressable';
import {
  dialogSelector,
  dialogShowDialogSelector,
  showFullScreenMessageDialogSelector
} from "../../../recoil/selector/common/dialogSelector";
import {clickBorderNone, COMMON_DIALOG_TYPE} from "../../../constant/common/Common";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {getMyInfo, postMyChildrenRearrangeMutate, postMyChildrenRemoveMutate} from "../../../api/myApi";
import {
  myBabyIsProfilePictureUrlDeletedSelector,
  myDataSelector,
  myIsProfilePictureUrlDeletedSelector
} from "../../../recoil/selector/my/myDataSelector";
import {MyData} from "../../../constant/my/MyData";
import {STACK} from "../../../constant/common/StackNavigation";
import useStackNavigation from "../../../hooks/useStackNavigation";
import {getHyphenPhoneNumber, getYYMMDDFromOnlyNumber, isCorrectUrl, nvlString} from "../../../utils/formatString";
import {useQueryClient} from "react-query";
import LoadingScreen from "../../../components/common/LoadingScreen";
import {forMyPageEntrySelector} from "../../../recoil/selector/auth/userSelector";
import {UserBadge} from "../../../constant/icons/UserBadge";
import {isNative} from "../../../utils/envUtils";
import {logger} from "../../../utils/loggingUtils";
import dynamic from "next/dynamic";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import MenuPopover from "../../../components/menu-popover";
import {ICON_COLOR, SvgCloseIcon} from "../../../constant/icons/icons";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {ALT_STRING} from "../../../constant/common/AltString";

const MyInformationDialog = dynamic(() =>
    import("./my-information/MyInformationDialog"), {
    ssr: false, loading: () => <LoadingScreen/>
  }
);
const IFrameDialog = dynamic(() =>
    import("../../../components/common/IFrameDialog"), {
    ssr: false, loading: () => <LoadingScreen/>
  }
);
const ChildrenInformationDialog = dynamic(() =>
    import("./children-information/ChildrenInformationDialog"), {
    ssr: false, loading: () => <LoadingScreen/>
  }
);
const ChildrenAddInformationDialog = dynamic(() =>
    import("./children-information/ChildrenAddInformationDialog"), {
    ssr: false, loading: () => <LoadingScreen/>
  }
);
const ChildrenResultAddDialog = dynamic(() =>
    import("./children-information/ChildrenResultAddDialog"), {
    ssr: false, loading: () => <LoadingScreen/>
  }
);
const FullScreenMessageDialog = dynamic(() =>
    import("../../common/FullScreenMessageDialog"), {
    ssr: false, loading: () => <LoadingScreen/>
  }
);

export default function MyProfileSection({mainRef, memberInfo, setShowChoco}) {
  const [myData, setMyData] = useRecoilState(myDataSelector);
  const [user, setUser] = useRecoilState(forMyPageEntrySelector);
  const [myIsProfilePictureUrlDeleted, setMyIsProfilePictureUrlDeleted] = useRecoilState(myIsProfilePictureUrlDeletedSelector);
  const [myBabyIsProfilePictureUrlDeleted, setMyBabyIsProfilePictureUrlDeleted] = useRecoilState(myBabyIsProfilePictureUrlDeletedSelector);
  const [open, setOpen] = useRecoilState(myOpenMyInformationPopupSelector);
  const [openMomStar, setOpenMomStar] = useRecoilState(myOpenMomStarSelector);
  const [openChildren, setOpenChildren] = useRecoilState(myOpenChildrenSelector)
  const [openAddChildren, setOpenAddChildren] = useRecoilState(myOpenAddChildrenSelector)
  const [childIndex, setChildIndex] = useState(0)
  const [isExpanded, setIsExpanded] = useRecoilState(myIsExpandedAccordionSelector);

  const {data: userInfo} = getMyInfo()

  const {navigation, preFetch} = useStackNavigation()
  useEffect(() => {
    const fetch = preFetch(STACK.MY_SELF.TYPE)
    if (!fetch) {
      setMyData({
        ...MyData,
        profilePictureUrl: userInfo?.profilePictureUrl,
        marketingInfoPushNotifSettingModifiedDatetime: userInfo?.marketingInfoPushNotifSettingModifiedDatetime,
        birthDay: getYYMMDDFromOnlyNumber(userInfo?.birthDay),
        profileSelectedBabyKey: userInfo?.profileSelectedBabyKey,
        briefBio: userInfo?.briefBio,
        babies: userInfo?.babies,
        type: userInfo?.type,
        emailReceiveAgreementModifiedDatetime: userInfo?.emailReceiveAgreementModifiedDatetime,
        isTermsCollectingPersonalDataMarketingAgreed: userInfo?.isTermsCollectingPersonalDataMarketingAgreed,
        accountKey: userInfo?.accountKey,
        isEmailReceiveAgreed: userInfo?.isEmailReceiveAgreed,
        phoneNumber: getHyphenPhoneNumber(userInfo?.phoneNumber),
        socialMediaUrl: userInfo?.socialMediaUrl,
        isPostCensorshipResultPushNotifSet: userInfo?.isPostCensorshipResultPushNotifSet,
        grade: userInfo?.grade,
        nickname: userInfo?.nickname,
        textMessageReciveAgreementModifiedDatetime: userInfo?.textMessageReciveAgreementModifiedDatetime,
        isMarketingInfoPushNotifSet: userInfo?.isMarketingInfoPushNotifSet,
        isTextMessageReciveAgreed: userInfo?.isTextMessageReciveAgreed,
        key: userInfo?.key,
        email: userInfo?.email,
      })
      setUser(userInfo)
      setMyIsProfilePictureUrlDeleted(false);
      setMyBabyIsProfilePictureUrlDeleted(false);
    }
  }, [])

  useEffect(() => {
    setUser(userInfo)
  }, [userInfo])

  const handleClickAddInform = () => setOpenAddChildren(true)

  return (
    <>
      {
        (!open && !openMomStar && !openChildren) && <UserInfoSection mainRef={mainRef} userInfo={userInfo} setChildIndex={setChildIndex}/>
      }
      <MyInformationDialog userInfo={userInfo} memberInfo={memberInfo} setShowChoco={setShowChoco}/>
      <IFrameDialog open={openMomStar} title={ALT_STRING.COMMON.MEMBERSHIP_INFO} url={PATH_DONOTS_STATIC_PAGE.MEMBERSHIP()} onClose={() => setOpenMomStar(false)}/>
      {userInfo && <ChildrenInformationDialog userInfo={userInfo} index={childIndex} handleClick={handleClickAddInform}/>}
      <ChildrenAddInformationDialog userInfo={userInfo} index={childIndex}/>
      <ChildrenResultAddDialog/>
    </>
  )
}

function UserInfoSection({mainRef, userInfo, setChildIndex}) {
  return (
    <>
      {
        !userInfo
          ? <UserInfoSkeleton/>
          : <UserInfoDataSection mainRef={mainRef} userInfo={userInfo} setChildIndex={setChildIndex}/>
      }
    </>
  )
}

function UserInfoSkeleton() {
  return (
    <Box sx={{mt: 3, alignItems: 'center', mx: '20px'}}>
      <Skeleton variant="rounded" height="24px" width="172px" sx={{mb: '10px'}}/>
      <Skeleton variant="rounded" height="16px" width="100%" sx={{mb: '26px'}}/>
      <Stack direction="row" sx={{ml: '8px', mt: '27px', mb: '16px', alignItems: 'flex-start', verticalAlign: 'center'}}>
        <Skeleton variant="circular" width="80px" height="80px"/>
        <Divider sx={{height: '44px', ml: '25px', mt: '12px', mr: '10px', width: '1px', background: '#ccc'}}/>
        <Stack direction="row" sx={{...hideScrollbarX}}>
          <Stack direction="column">
            <Skeleton variant="circular" width="56px" height="56px" sx={{mt: '6px', mx: '6px'}}/>
            <Skeleton variant="rounded" height="14px" width="56px" sx={{mt: '8px', alignSelf: 'center'}}/>
          </Stack>
          <Stack direction="column">
            <Skeleton variant="circular" width="56px" height="56px" sx={{mt: '6px', mx: '6px'}}/>
            <Skeleton variant="rounded" height="14px" width="56px" sx={{mt: '8px', alignSelf: 'center'}}/>
          </Stack>
          <Stack direction="column">
            <Skeleton variant="circular" width="56px" height="56px" sx={{mt: '6px', mx: '6px'}}/>
            <Skeleton variant="rounded" height="14px" width="56px" sx={{mt: '8px', alignSelf: 'center'}}/>
          </Stack>
        </Stack>
      </Stack>
    </Box>
  )
}

function UserInfoDataSection({mainRef, userInfo, setChildIndex}) {
  const [open, setOpen] = useRecoilState(myOpenMyInformationPopupSelector);

  const handleClickOpenPopupDialog = () => {
    setOpen(true);
  };

  const handleClickBriefBio = () => {
    setOpen(true);
  }

  return (
    <>
      <Box sx={{px: '20px'}}>
        <Stack direction="row" sx={{mt: '4px', alignItems: 'center', verticalAlign: 'center', mb: '10px'}}>
          <Typography fontWeight="500" fontSize="18px" sx={{lineHeight: '24px', color: '#000000', mr: '6px'}}>
            {nvlString(userInfo?.nickname)}
          </Typography>
          <SNSLinkSection userInfo={userInfo}/>
        </Stack>

        {
          userInfo?.briefBio !== ''
            ? <BriefBioSection onClick={handleClickBriefBio} userInfo={userInfo}/>
            : <NoBriefBioSection onClick={handleClickBriefBio}/>
        }

        <FullScreenMessageDialog
          type="no"
          title={MY_COMMON.TEXT.SNS_LINK_NOT_EXIST_ADD_LINK}
          detail={MY_COMMON.TEXT.SNS_LINK_NOT_EXIST_DETAIL}
        />

        <Stack direction="row" sx={{ml: '8px', mt: '27px', mb: '16px', alignItems: 'flex-start', verticalAlign: 'center'}}>
          {
            userInfo?.profilePictureUrl
              ? <AvatarSection userInfo={userInfo} handleClickOpenPopupDialog={handleClickOpenPopupDialog}/>
              : <SvgCommonIcons
                alt={ALT_STRING.MY.BTN_CHANGE_PROFILE}
                tabIndex={2}
                type={ICON_TYPE.PROFILE_NO_IMAGE_80PX}
                onClick={handleClickOpenPopupDialog}
                onKeyDown={(e) => handleEnterPress(e, handleClickOpenPopupDialog)}
                sx={{width: '80px', ...clickBorderNone}}
              />
          }

          <Divider sx={{height: '44px', ml: '25px', mt: '12px', mr: getMLValue(userInfo), width: '1px', background: '#ccc'}}/>

          <DragChildImageContext mainRef={mainRef} userInfo={userInfo} setChildIndex={setChildIndex}/>
        </Stack>
      </Box>
    </>
  )
}

function SNSLinkSection({userInfo}) {
  return (
    <>
      {
        ((userInfo?.type === "NORMAL_MEMBER" && userInfo?.grade === "LV3") || (userInfo?.type === "EXPERT")) &&
        <SNSLinkShowSection userInfo={userInfo}/>
      }
    </>
  )
}

function SNSLinkShowSection({userInfo}) {
  const [openFullScreenMessageDialog, setOpenFullScreenMessageDialog] = useRecoilState(showFullScreenMessageDialogSelector);

  const handleClick = (socialMediaUrl) => {
    setHackleTrack(HACKLE_TRACK.MY_SNS)
    if (socialMediaUrl !== '' && isNative()) {
      try {
        window.flutter_inappwebview.callHandler('openNativeLinkOpen', isCorrectUrl(socialMediaUrl))
      } catch (e) {
        logger.error(e.message);
      }
    } else {
      setOpenFullScreenMessageDialog(true);
    }
  };

  return (
    <>
      <Divider sx={{height: '14px', ml: '4px', mr: '10px', background: '#CCCCCC', width: '1px', border: 0}}/>

      {
        userInfo?.socialMediaUrl && !isNative()
          ? <LinkSocialMediaUrl socialMediaUrl={userInfo?.socialMediaUrl}/>
          : <NotLinkSocialMediaUrl onClick={() => handleClick(userInfo?.socialMediaUrl)}/>
      }
    </>
  )
}

function LinkSocialMediaUrl({socialMediaUrl}) {
  return (
    <a target='_blank' href={isCorrectUrl(socialMediaUrl)} rel='noreferrer'>
      <Typography
        fontWeight="400"
        fontSize="14px"
        sx={{lineHeight: '20px', color: '#888888', textDecoration: 'underline',}}
      >
        {MY_COMMON.TEXT.SNS_ADDRESS}
      </Typography>
    </a>
  )
}

function NotLinkSocialMediaUrl({onClick}) {
  return (
    <Typography
      fontWeight="400"
      fontSize="14px"
      sx={{lineHeight: '20px', color: '#888888', textDecoration: 'underline',}}
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
    >
      {MY_COMMON.TEXT.SNS_ADDRESS}
    </Typography>
  )
}

function BriefBioSection({onClick, userInfo}) {
  return (
    <Typography onClick={onClick} fontWeight="400" fontSize="16px" sx={{lineHeight: '16px', color: '#000000', width: 'fit-content'}}>
      {userInfo?.briefBio}
    </Typography>
  )
}

function NoBriefBioSection({onClick}) {
  return (
    <Typography onClick={onClick} fontWeight="400" fontSize="16px" sx={{lineHeight: '16px', color: '#888888', width: 'fit-content'}}>
      {MY_COMMON.TEXT.INTRODUCTION_DEFAULT_MESSAGE}
    </Typography>
  )
}

function AvatarSection({userInfo, handleClickOpenPopupDialog}) {
  return (
    <Avatar
      tabIndex={2}
      alt={ALT_STRING.MY.PROFILE}
      src={userInfo?.profilePictureUrl}
      sx={{width: '80px', height: '80px', ...clickBorderNone}}
      onClick={handleClickOpenPopupDialog}
      onKeyDown={(e) => handleEnterPress(e, handleClickOpenPopupDialog)}
    />
  )
}

function getMLValue({userInfo}) {
  if (userInfo?.babies?.length === 0) {
    return '0px'
  } else {
    return '10px'
  }
}

DragChildImageContext.propTypes = {};

function DragChildImageContext({mainRef, userInfo, setChildIndex}) {
  const [myData, setMyData] = useRecoilState(myDataSelector);
  const [openChildren, setOpenChildren] = useRecoilState(myOpenChildrenSelector)
  const [isRemove, setIsRemove] = useState(false)
  const setDialogMessage = useSetRecoilState(dialogSelector)
  const [babies, setBabies] = useState([])
  const [isShowTooltip, setIsShowTooltip] = useState(false)
  const firstChildRef = useRef(null)
  const [customized, setCustomized] = useState(null);

  const isShowDialogRef = useRef(null)

  useEffect(() => {
    setBabies([...userInfo?.babies])
    if (userInfo?.babies?.length < 3)
      setIsShowTooltip(true)
    else
      setIsShowTooltip(false)
  }, [userInfo])

  const queryClient = useQueryClient()

  const {
    mutate: mutatePostMyChildrenRearrange,
    isLoading: isLoadingPostMyChildrenRearrange
  } = postMyChildrenRearrangeMutate()
  const {mutate: mutatePostMyChildrenRemove, isLoading: isLoadingPostMyChildrenRemove} = postMyChildrenRemoveMutate()

  const handleConfirmClick = (index) => {
    if (userInfo?.babies?.length !== 1) {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.TWO_BUTTON,
        message: MY_COMMON.TEXT.DELETE_CHILDREN,
        leftButtonProps: {text: MY_BTN_TEXT.CANCEL},
        rightButtonProps: {text: MY_BTN_TEXT.DELETE, onClick: () => handleClickRemoveChildren(index)},
      })
    } else {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.ONE_BUTTON,
        message: MY_COMMON.TEXT.CANT_DELETE_CHILDREN,
        handleButton1: {text: SIGN_UP_DIALOG.CONFIRM},
      })
    }
  }
  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)
  useEffect(() => {
    if (showDialog) {
      isShowDialogRef.current = true
    } else {
      isShowDialogRef.current = false
    }
  }, [showDialog])

  useEffect(() => {
    if (isRemove) {
      window.addEventListener('mousedown', handleCloseDelete, false);
    }

    return () => {
      window.removeEventListener('mousedown', handleCloseDelete, false);
    }
  }, [isRemove])

  useEffect(() => {
    if (userInfo?.babies?.length > 1 && isShowTooltip) {
      handleOpenCustomized(firstChildRef?.current)
    }
  }, [userInfo?.babies?.length])

  const handleClickRemoveChildren = (index) => {
    if (userInfo?.babies?.length > 1) {
      mutatePostMyChildrenRemove({
        key: userInfo?.babies[index]?.key
      }, {
        onSuccess: () => {
          queryClient.invalidateQueries('getMyInfo')
        }
      })
    }
  };

  const handleDragEndFile = (result) => {
    const {destination, source} = result;
    if (!destination) return;
    if (destination?.droppableId === source?.droppableId && destination?.index === source?.index) return;
    const newArray = [...babies]
    const [removed] = newArray.splice(source.index, 1);
    newArray.splice(destination.index, 0, removed);

    const babiesArray = [];
    newArray?.map((item, index) => {
      const paramBabyItem = {
        key: item?.key,
        profilePictureThumbnailOrder: index
      }
      babiesArray.push(paramBabyItem);
    })
    setBabies([...newArray])
    // setMyData({...myData, babies: newArray})
    mutatePostMyChildrenRearrange({
      profileSelectedBabyKey: babiesArray[0]?.key,
      babies: babiesArray
    }, {
      onSuccess: () => {
        queryClient.invalidateQueries('getMyInfo')
      },
      onError: () => {
        setBabies([...userInfo.babies])
      }
    })
  };

  const handleChildClick = (index) => {
    if (!isRemove) {
      setChildIndex(index)
      setOpenChildren(true)
    }
    setHackleTrack(HACKLE_TRACK.MY_CHILD_INFO,{
      kid_name: userInfo?.babies[index].nickname,
      kid_birth: userInfo?.babies[index].birthdate,
      kid_gender: userInfo?.babies[index].gender,
      kid_weight: userInfo?.babies[index].weight,
      kid_stature: userInfo?.babies[index].height
    })
  }

  const handleLongClick = () => {
    if (!isShowDialogRef?.current) {
      setIsRemove(true);
    }
  }

  const handleCloseDelete = () => {
    if (!isShowDialogRef?.current) {
      setIsRemove(false);
    }
  }

  const handleShortClick = () => {
    // setIsRemove(false);
  }

  const handleOpenCustomized = (currentTarget) => {
    setCustomized(currentTarget);
  };

  const handleCloseCustomized = () => {
    setCustomized(null);
  };

  return (
    <Box sx={{...hideScrollbarX}}>
      <LongPressable onShortPress={handleShortClick} onLongPress={handleLongClick} longPressTime={700}>
        <DragDropContext onDragEnd={handleDragEndFile}>
          <Droppable droppableId="imageId" direction="horizontal" type="column">
            {
              (provided) => (
                <Stack
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  direction="row"
                  alignItems="flex-start"
                  sx={{
                    height: 1,
                    mt: '6px',
                    overflowY: 'hidden',
                    ...hideScrollbarX,
                  }}
                >
                  {
                    babies?.map((item, index) => (
                      <React.Fragment key={index}>
                        {(userInfo?.babies?.length > 1 && index === 0) && <Box ref={firstChildRef}/>}
                        <DragImage
                          dragKey={'image'}
                          index={index}
                          child={item}
                          isRemove={isRemove}
                          profileSelectedBabyKey={babies[0].key}
                          onRemove={() => handleConfirmClick(index)}
                          handleClick={() => handleChildClick(index)}
                        />
                      </React.Fragment>
                    ))
                  }
                  {provided.placeholder}

                  {userInfo?.babies?.length < 3 && <BabyAddSection/>}
                </Stack>
              )}
          </Droppable>
        </DragDropContext>
      </LongPressable>
      {userInfo?.babies?.length > 1 &&
        <ChildrenRearrangeIntroduceTooltip
          headerTitleRef={mainRef}
          customized={customized}
          setCustomized={setCustomized}
          handleCloseCustomized={handleCloseCustomized}
        />
      }
      {(isLoadingPostMyChildrenRearrange || isLoadingPostMyChildrenRemove) && <LoadingScreen/>}
    </Box>
  )
}

function BabyAddSection() {
  const router = useRouter()

  const addChildren = () => {
    setHackleTrack(HACKLE_TRACK.MYPAGE_ADD_CHILD)
    router.push({pathname: PATH_DONOTS_PAGE.MY.ADD_CHILDREN})
  }

  return (
    <Stack tabIndex={4} sx={{...clickBorderNone}} onKeyDown={(e) => handleEnterPress(e, addChildren)}>
      <IconButton
        tabIndex={-1}
        disableFocusRipple
        sx={{
          width: '56px',
          height: '56px',
          backgroundColor: "#FFFFFF",
          '&:hover': {
            boxShadow: 'none',
            backgroundColor: '#FFFFFF',
          },
        }}
        onClick={addChildren}
      >
        <SvgCommonIcons alt={ALT_STRING.MY.ADD_CHILDREN} type={ICON_TYPE.PLUS}/>
      </IconButton>
      <Typography fontWeight="400" fontSize="14px" sx={{mt: '8px', alignSelf: 'center', lineHeight: '20px', color: '#222222'}}>
        아이추가
      </Typography>
    </Stack>
  )
}

function ChildrenRearrangeIntroduceTooltip({headerTitleRef, customized, setCustomized, handleCloseCustomized}) {
  return (
    <MenuPopover
      mainRef={headerTitleRef}
      open={customized}
      setOpen={setCustomized}
      onClose={handleCloseCustomized}
      arrow='bottom-center-my'
      sx={{backgroundColor: 'primary.main', p: 0, borderRadius: '4px', height: '28px', width: '231px', zIndex: 9999,}}
      arrowBottomStyle={{backgroundColor: '#ECA548'}}
    >
      <Stack direction="row" sx={{alignItems: 'center'}}>
        <SvgCloseIcon alt={ALT_STRING.COMMON.BTN_CLOSE} color={ICON_COLOR?.WHITE} onClick={handleCloseCustomized}
                      sx={{m: '0 6px', width: '12px', height: '12px'}}/>
        <Typography fontWeight="500" fontSize="12px"
                    sx={{lineHeight: '25px', color: 'white', whiteSpace: 'pre-wrap'}}>
          2초간 꾹 눌러 대표 아이를 변경해보세요
        </Typography>
      </Stack>
    </MenuPopover>
  )
}