import {Avatar, Box, Divider, Skeleton, Stack, Typography} from "@mui/material";
import {MY_COMMON} from "../../../constant/my/MyConstants";
import {useRecoilState} from "recoil";
import {showFullScreenMessageDialogSelector} from "../../../recoil/selector/common/dialogSelector";
import FullScreenMessageDialog from "../../common/FullScreenMessageDialog";
import * as React from "react";
import {useEffect} from "react";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {getOthersInfo} from "../../../api/myApi";
import useStackNavigation from "../../../hooks/useStackNavigation";
import {STACK} from "../../../constant/common/StackNavigation";
import {MyData} from "../../../constant/my/MyData";
import {isCorrectUrl, nvlString} from "../../../utils/formatString";
import {othersDataSelector} from "../../../recoil/selector/my/othersDataSelector";
import {UserBadge} from "../../../constant/icons/UserBadge";
import {isNative} from "../../../utils/envUtils";
import {logger} from "../../../utils/loggingUtils";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";

MyExpertsProfileSection.propTypes = {};

export default function MyExpertsProfileSection({user_key, setProperty}) {
  const [othersData, setOthersData] = useRecoilState(othersDataSelector);

  const {data: userInfo} = getOthersInfo({
    key: user_key,
  })

  const {navigation, preFetch} = useStackNavigation()
  useEffect(() => {
    const fetch = preFetch(STACK.MY_EXPERT.TYPE)
    if (!fetch) {
      setOthersData({
        ...MyData,
        profilePictureUrl: userInfo?.profilePictureUrl,
        briefBio: userInfo?.briefBio,
        type: userInfo?.type,
        socialMediaUrl: userInfo?.socialMediaUrl,
        grade: userInfo?.grade,
        nickname: userInfo?.nickname,
        key: userInfo?.key,
      })
    }
  }, [])

  useEffect(()=>{
    if(userInfo){
      setHackleTrack(HACKLE_TRACK.MYPAGE_EXPERT,{
        expert_name: userInfo?.nickname
      })
      setProperty(userInfo?.nickname)
    }
  },[userInfo])

  return (
    <>
      {
        !userInfo
          ? <NoUserInfoSkeleton/>
          : <UserProfileMainSection userInfo={userInfo}/>
      }
    </>
  )
}

function NoUserInfoSkeleton() {
  return (
    <Box sx={{mt: 3, alignItems: 'center', mx: '20px'}}>
      <Stack direction="row" sx={{ml: '4px', my: '20px', alignItems: 'center', verticalAlign: 'center'}}>
        <Skeleton variant="circular" width="76px" height="76px"/>
        <Stack>
          <Stack direction="row" sx={{ml: '16px', alignItems: 'center', verticalAlign: 'center', mb: '4px'}}>
            <Skeleton variant="rounded" height="24px" width="82px" sx={{mr: '6px'}}/>
            <Divider sx={{height: '14px', ml: '4px', mr: '10px', background: '#CCCCCC', width: '1px', border: 0}}/>
            <Skeleton variant="rounded" height="14px" width="52px"/>
          </Stack>
          <Skeleton variant="rounded" height="48px" width="225px" sx={{ml: '16px'}}/>
        </Stack>
      </Stack>
    </Box>
  )
}

function UserProfileMainSection({userInfo}) {
  const [openFullScreenMessageDialog, setOpenFullScreenMessageDialog] = useRecoilState(showFullScreenMessageDialogSelector);

  const handleClick = (socialMediaUrl) => {

    setHackleTrack(HACKLE_TRACK.EXPERT_SNS,{
      expert_name: userInfo?.nickname
    })

    if (socialMediaUrl !== '' && isNative()) {
      try {
        window.flutter_inappwebview.callHandler('openNativeLinkOpen', isCorrectUrl(socialMediaUrl))
      } catch (e) {
        logger.error(e.message);
      }
    } else {
      setOpenFullScreenMessageDialog(true);
    }
  };

  return (
    <Box sx={{px: '20px'}}>
      <Stack direction="row" sx={{ml: '4px', my: '20px', alignItems: 'center', verticalAlign: 'center'}}>
        {
          userInfo?.profilePictureUrl
            ? <Avatar alt={userInfo?.nickname} role={'img'} src={userInfo?.profilePictureUrl} sx={{width: '76px', height: '76px'}}/>
            : <SvgCommonIcons alt={userInfo?.nickname} type={ICON_TYPE.PROFILE_NO_IMAGE_80PX} sx={{width: '76px', height: '76px'}}/>
        }

        <Stack>
          <Stack direction="row" sx={{ml: '16px', alignItems: 'center', verticalAlign: 'center', mb: '4px'}}>
            <Typography fontWeight="500" fontSize="18px" sx={{lineHeight: '24px', color: '#000000', mr: '6px'}}>
              {nvlString(userInfo?.nickname)}
            </Typography>
            <UserBadge type={userInfo?.type} grade={userInfo.grade} sx={{width: '24px'}}/>
            {
              userInfo?.briefBio !== ''
                ? <SNSLinkSectionWithBriefBio userInfo={userInfo} handleClick={handleClick}/>
                : <></>
            }
          </Stack>

          {
            userInfo?.briefBio !== ''
              ? <BriefBioSection userInfo={userInfo}/>
              : <SNSLinkSectionWithNoBriefBio userInfo={userInfo} handleClick={handleClick}/>
          }
        </Stack>
      </Stack>

      <FullScreenMessageDialog type="no" title={MY_COMMON.TEXT.SNS_LINK_NOT_EXIST}/>
    </Box>
  )
}

function SNSLinkSectionWithBriefBio({userInfo, handleClick}) {
  return (
    <>
      {
        ((userInfo?.type === "NORMAL_MEMBER" && userInfo?.grade === "LV3") || userInfo?.type === "EXPERT") &&
        <>
          <Divider sx={{height: '14px', ml: '4px', mr: '10px', background: '#CCCCCC', width: '1px', border: 0}}/>

          <SNSLinkSection userInfo={userInfo} handleClick={handleClick}/>
        </>
      }
    </>
  )
}

function SNSLinkSection({userInfo, handleClick, sx}) {
  return (
    <>
      {
        userInfo?.socialMediaUrl && !isNative()
          ? <LinkSocialMediaUrl socialMediaUrl={userInfo?.socialMediaUrl} sx={{...sx}}/>
          : <NotLinkSocialMediaUrl onClick={() => handleClick(userInfo?.socialMediaUrl)} sx={{...sx}}/>
      }
    </>
  )
}

function BriefBioSection({userInfo}) {
  return (
    <Typography fontWeight="400" fontSize="16px" sx={{ml: '16px', lineHeight: '24px', color: '#000000'}}>
      {userInfo?.briefBio}
    </Typography>
  )
}

function SNSLinkSectionWithNoBriefBio({userInfo, handleClick}) {
  return (
    <>
      {
        ((userInfo?.type === "NORMAL_MEMBER" && userInfo?.grade === "LV3") || userInfo?.type === "EXPERT") &&
        <SNSLinkSection userInfo={userInfo} handleClick={handleClick} sx={{ml: '16px'}}/>
      }
    </>
  )
}

function LinkSocialMediaUrl({socialMediaUrl, sx}) {
  return (
    <a target='_blank' href={isCorrectUrl(socialMediaUrl)} rel='noreferrer'>
      <Typography
        tabIndex={2}
        fontWeight="400"
        fontSize="14px"
        sx={{lineHeight: '20px', color: '#888888', textDecoration: 'underline', ...clickBorderNone, ...sx}}
      >
        {MY_COMMON.TEXT.SNS_ADDRESS}
      </Typography>
    </a>
  )
}

function NotLinkSocialMediaUrl({onClick, sx}) {
  return (
    <Typography
      tabIndex={2}
      fontWeight="400"
      fontSize="14px"
      sx={{lineHeight: '20px', color: '#888888', textDecoration: 'underline', ...clickBorderNone, ...sx}}
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
    >
      {MY_COMMON.TEXT.SNS_ADDRESS}
    </Typography>
  )
}