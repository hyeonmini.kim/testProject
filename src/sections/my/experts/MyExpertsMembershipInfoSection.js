import * as React from "react";
import {Box, Divider, Skeleton, Stack, Typography} from "@mui/material";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {MY_COMMON} from "../../../constant/my/MyConstants";

export default function MyExpertsMembershipInfoSection({userRecipeInfo}) {
  return (
    <>
      {
        !userRecipeInfo
          ? <MyExpertsMembershipInfoSkeleton/>
          : <MyExpertsMembershipInfoDataSection userRecipeInfo={userRecipeInfo}/>
      }
    </>
  )
}

function MyExpertsMembershipInfoSkeleton() {
  return (
    <Box sx={{mt: 3, alignItems: 'center', mx: '20px'}}>
      <Skeleton variant="rounded" height="90px" width="100%" sx={{mb: '20px', borderRadius: '8px'}}/>
    </Box>
  )
}

function MyExpertsMembershipInfoDataSection({userRecipeInfo}) {
  return (
    <Box sx={{px: '20px'}}>
      <Stack
        direction="row"
        justifyContent="space-evenly"
        sx={{py: '19px', pb: '14px', backgroundColor: '#F5F5F5', boxShadow: 'none', borderRadius: '8px'}}
      >
        <NumberInfoComponent count={userRecipeInfo?.write_recipe_count} type="write"/>

        <Divider sx={{height: '36px', mt: '21px', width: '1px', border: 0, background: '#E6E6E6'}}/>

        <NumberInfoComponent count={userRecipeInfo?.scrap_recipe_count} type="scrap"/>

        <Divider sx={{height: '36px', mt: '21px', width: '1px', border: 0, background: '#E6E6E6'}}/>

        <NumberInfoComponent count={userRecipeInfo?.review_recipe_count} type="review"/>
      </Stack>
    </Box>
  )
}

function NumberInfoComponent({count, type}) {
  return (
    <Box sx={{position: 'relative', textAlign: 'center'}}>
      <Typography fontWeight="400" fontSize="14px" sx={{lineHeight: '14px', color: '#222222', mb: '15px'}}>
        {getTitleText(type)}
      </Typography>
      <Typography fontWeight="500" fontSize="20px" sx={{ml: getMLValue(count), lineHeight: '20px', color: '#222222'}}>
        {getNumberText(count)}
      </Typography>
      {count > 999 && <SvgCommonIcons type={ICON_TYPE.PLUS_11PX} sx={{position: 'absolute', left: getLeftValue(type), top: '30px'}}/>}
    </Box>
  )
}

function getTitleText(type) {
  if (type === "write") {
    return MY_COMMON.TEXT.DONOTS_MEMBERSHIP_WRITE_COUNT
  } else if (type === "scrap") {
    return MY_COMMON.TEXT.DONOTS_MEMBERSHIP_SCRAP_COUNT
  } else if (type === "review") {
    return MY_COMMON.TEXT.DONOTS_MEMBERSHIP_REVIEW_COUNT
  }
}

function getNumberText(count) {
  if (count > 999) {
    return 999
  } else {
    return count
  }
}

function getMLValue(count) {
  if (count > 999) {
    return '-10px'
  } else {
    return '0px'
  }
}

function getLeftValue(type) {
  if (type === "review") {
    return '35px'
  } else {
    return '41px'
  }
}