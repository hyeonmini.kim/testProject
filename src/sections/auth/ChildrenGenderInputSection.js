import {Box, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import {useEffect} from "react";
import {GENDER, SIGN_UP_CHILD_GENDER, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import {ICON_COLOR, SvgBoyIcon, SvgGirlIcon} from "../../constant/icons/icons";
import {authChildGenderSelector} from "../../recoil/selector/sign-up/signUpSelector";
import {inputChildrenGenderSelector} from "../../recoil/selector/home/inputChildrenInfoSelector";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {handleEnterPress} from "../../utils/onKeyDownUtils";

export default function ChildrenGenderInputSection({isGuest = false}) {
  const [childGender, setChildGender] = useRecoilState(authChildGenderSelector)
  const setInputChildGender = useSetRecoilState(inputChildrenGenderSelector)

  const handleClickBoy = () => {
    setChildGender(GENDER.male)
    if (isGuest) setInputChildGender(GENDER.male)
  }
  const handleClickGirl = () => {
    setChildGender(GENDER.female)
    if (isGuest) setInputChildGender(GENDER.female)
  }

  useEffect(() => {
    if (childGender === '') {
      setChildGender(GENDER.male)
    }
  }, [])

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_KIDS_GENDER)
  }, [])

  return (
    <Box>
      <Typography variant={'h1_24_b'} sx={{color: '#000000'}}>
        {SIGN_UP_TITLE.CHILD_GENDER}
      </Typography>
      <Box sx={{display: 'flex', marginTop: '103px', justifyContent: 'center'}}>
        <Box
          tabIndex={0}
          onClick={handleClickBoy}
          onKeyDown={(e) => handleEnterPress(e, handleClickBoy)}
          sx={{textAlign: 'center', marginX: '24px'}}
        >
          {<Box sx={{height: '96px', width: '96px', mb: '12px', position: 'relative'}}>
            {childGender === GENDER.male &&
              <SvgBoyIcon color={ICON_COLOR.PRIMARY} sx={{borderRadius: '50%', border: '1.4px solid #ECA548'}}/>}
            {childGender === GENDER.female && <SvgBoyIcon color={ICON_COLOR.GREY}/>}
          </Box>}
          {childGender === GENDER.male &&
            <Typography variant={'b1_16_b_1l'} sx={{color: '#000000'}}>
              {SIGN_UP_CHILD_GENDER.BOY}
            </Typography>}
          {childGender === GENDER.female &&
            <Typography variant={'b1_16_r_1l'} sx={{color: '#000000'}}>
              {SIGN_UP_CHILD_GENDER.BOY}
            </Typography>}
        </Box>
        <Box
          tabIndex={0}
          onClick={handleClickGirl}
          onKeyDown={(e) => handleEnterPress(e, handleClickGirl)}
          sx={{textAlign: 'center', marginX: '24px'}}
        >
          {<Box sx={{height: '96px', width: '96px', mb: '12px', position: 'relative'}}>
            {childGender === GENDER.female &&
              <SvgGirlIcon color={ICON_COLOR.PRIMARY} sx={{borderRadius: '50%', border: '1.4px solid #ECA548'}}/>}
            {childGender === GENDER.male && <SvgGirlIcon color={ICON_COLOR.GREY}/>}
          </Box>}
          {childGender === GENDER.female &&
            <Typography variant={'b1_16_b_1l'} sx={{color: '#000000'}}>
              {SIGN_UP_CHILD_GENDER.GIRL}
            </Typography>}
          {childGender === GENDER.male &&
            <Typography variant={'b1_16_r_1l'} sx={{color: '#000000'}}>
              {SIGN_UP_CHILD_GENDER.GIRL}
            </Typography>}
        </Box>
      </Box>
    </Box>
  )
}
