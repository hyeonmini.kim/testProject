import {Box, Typography} from "@mui/material";
import {GET_FIND_ID_QUESTIONS_TEXT, SIGN_UP_EMAIL, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import DonotsTextField from "../../components/common/DonotsTextField";
import {useRecoilState, useSetRecoilState} from "recoil";
import {authBottomButtonState, authFindIdAnswerErrorState, authFindIdAnswerHelperTextState} from "../../recoil/atom/sign-up/auth";
import {authFindIdAnswerSelector, authFindIdQuestionSelector} from "../../recoil/selector/sign-up/signUpSelector";
import {useEffect} from "react";

export default function FindIdAnswerInputForm() {
  // Email 정보 state
  const [findIdAnswer, setFindIdAnswer] = useRecoilState(authFindIdAnswerSelector)
  // 입력 상태 state
  const [isTrue, setIsTrue] = useRecoilState(authFindIdAnswerErrorState)
  const [helperText, setHelperText] = useRecoilState(authFindIdAnswerHelperTextState)
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  const [findIdQuestion, setFindIdQuestion] = useRecoilState(authFindIdQuestionSelector)

  const handleChangeValue = (e) => setFindIdAnswer(e.target.value?.substring(0, 10))

  useEffect(() => {
    if (findIdAnswer === '') {
      setIsBottomBtnDisabled(true)
    } else {
      setIsBottomBtnDisabled(false)
    }

    if(findIdAnswer === '') {
      setHelperText('')
      setIsTrue(true)
    }
  }, [findIdAnswer])

  return (
    <>

      <Box sx={{position: 'relative'}}>

        <Typography variant={'h1_24_b'} sx={{color: '#000000'}}>
          {SIGN_UP_TITLE.INPUT_ANSWER_FIND_ID}
        </Typography>

        <Box sx={{mt: '30px', p: '10px', height: '52px', background: '#F5F5F5', border: '1px solid #E6E6E6', borderRadius: '8px',}}>
          <Box sx={{height: '2px'}}/>
          <Typography variant={'b1_16_m'} sx={{color: '#BFBFBF'}}>
            {GET_FIND_ID_QUESTIONS_TEXT(findIdQuestion)}
          </Typography>
        </Box>

        <DonotsTextField
          tabIndex={0}
          handleChange={handleChangeValue}
          helperText={helperText}
          label={SIGN_UP_EMAIL.LABEL_INPUT_ANSWER}
          type={'text'}
          value={findIdAnswer}
          setValue={setFindIdAnswer}
          isValidate={isTrue}
        />

      </Box>

    </>
  )
}