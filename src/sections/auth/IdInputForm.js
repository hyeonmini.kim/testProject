import {Box, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import {authBottomButtonState, authIdErrorState, authIdHelperTextState, authIdState} from "../../recoil/atom/sign-up/auth";
import {useEffect} from "react";
import {SIGN_UP_ID, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import DonotsTextField from "../../components/common/DonotsTextField";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";

export default function IdInputForm() {
  // ID 정보 state
  const [id, setId] = useRecoilState(authIdState)
  // 입력 상태 state
  const [isTrue, setIsTrue] = useRecoilState(authIdErrorState)
  const [helperText, setHelperText] = useRecoilState(authIdHelperTextState)
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  // ID 값 변경 제어
  const handleChangeValue = (e) => setId(e.target.value.toLowerCase().substring(0, 12))
  // ID 정규식 체크
  const checkIdValidation = () => {
    // 영문 소문자, 숫자, 6~12 자리 정규식
    const regex = /^[a-z0-9]{6,12}$/g
    if (regex.test(id)) {
      setIsTrue(true)
      setIsBottomBtnDisabled(false)
    } else {
      setIsTrue(false)
      setHelperText(SIGN_UP_ID.HELPER_TEXT)
      setIsBottomBtnDisabled(true)
    }
  }

  useEffect(() => {
    checkIdValidation()
  }, [id])

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_ID)
  }, [])

  return (
    <Box>

      <Typography variant={'h1_24_b'} sx={{color: '#000000'}}>
        {SIGN_UP_TITLE.ID}
      </Typography>

      <DonotsTextField
        tabIndex={0}
        handleChange={handleChangeValue}
        helperText={helperText}
        label={SIGN_UP_ID.LABEL}
        type={'text'}
        value={id}
        setValue={setId}
        isValidate={isTrue}
      />

    </Box>
  );
}