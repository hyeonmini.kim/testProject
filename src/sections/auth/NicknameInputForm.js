import {Box, InputAdornment, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import {
  authBottomButtonState,
  authNicknameErrorState,
  authNicknameHelperTextState,
  authNicknameState
} from "../../recoil/atom/sign-up/auth";
import {useEffect, useState} from "react";
import {SIGN_UP_NICKNAME, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {ALT_STRING} from "../../constant/common/AltString";
import TextFieldWrapper from "../../components/common/TextFieldWrapper";

export default function NicknameInputForm() {
  // Nickname 정보 state
  const [nickname, setNickname] = useRecoilState(authNicknameState)
  // 입력 상태 state
  const [isTrue, setIsTrue] = useRecoilState(authNicknameErrorState)
  const [helperText, setHelperText] = useRecoilState(authNicknameHelperTextState)
  const [cancelIcon, setCancelIcon] = useState(false)
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  // Nickname 값 변경 제어
  const handleChangeValue = (e) => setNickname(e.target.value.substring(0, 10))
  // Nickname 정규식 체크
  const checkNicknameValidation = () => {
    // 영문/한글/숫자 포함 10자리 이내 정규식
    const regex = /^[ㄱ-ㅎ가-힣a-zA-Z0-9]{2,10}$/g
    if (regex.test(nickname)) {
      setIsTrue(true)
      setIsBottomBtnDisabled(false)
    } else {
      setIsTrue(false)
      setHelperText(SIGN_UP_NICKNAME.HELPER_TEXT)
      setIsBottomBtnDisabled(true)
    }
  }
  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true)
  const handleBlur = () => setCancelIcon(false)
  const handleCancel = () => setNickname('')

  useEffect(() => {
    checkNicknameValidation()
  }, [nickname])

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_NAME)
  }, [])

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          color: '#000000',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.NICKNAME}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={nickname}
        label={nickname === '' ? SIGN_UP_NICKNAME.LABEL : ' '}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        helperText={nickname === '' ? helperText : isTrue ? ' ' : helperText}
        inputProps={{tabIndex: 0}}
        InputProps={{
          endAdornment: nickname && cancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
            </InputAdornment>
          ),
          style: {
            paddingBottom: '11px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#222222',
          },
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#CCCCCC',
          }
        }}
        FormHelperTextProps={{
          style: {
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            letterSpacing: '-0.03em',
            color: nickname === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          marginTop: '14px',
          '& .MuiInput-root:before': {
            borderBottom: nickname === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: nickname === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: nickname === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: nickname === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: nickname === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  );
}