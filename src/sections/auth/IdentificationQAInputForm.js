import {Box, InputAdornment, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import {authBottomButtonState} from "../../recoil/atom/sign-up/auth";
import {useEffect, useState} from "react";
import {SIGN_UP_IDENTIFICATION, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import DonotsTextField from "../../components/common/DonotsTextField";
import {authIdentificationQASelector} from "../../recoil/selector/sign-up/signUpSelector";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import BottomDialogContents from "../common/BottomDialogContents";
import {commonBottomDialogState} from "../../recoil/atom/common/bottomDialog";
import BottomDialogQA from "../../components/common/BottomDialogQA";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {
  firstAnswerSelector,
  firstQuestionCodeSelector,
  firstQuestionTextSelector,
  questionNumSelector,
  secondAnswerSelector,
  secondQuestionCodeSelector,
  secondQuestionTextSelector
} from "../../recoil/selector/auth/identificationQASelector";
import TextFieldWrapper from "../../components/common/TextFieldWrapper";

export default function IdentificationQAInputForm() {
  const [identificationQA, setIdentificationQA] = useRecoilState(authIdentificationQASelector)
  const [firstAnswerHelperText, setFirstAnswerHelperText] = useState(SIGN_UP_IDENTIFICATION.HELPER_TEXT)
  const [secondAnswerHelperText, setSecondAnswerHelperText] = useState(SIGN_UP_IDENTIFICATION.HELPER_TEXT)
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)

  const [isOpenBottomDialog, setIsOpenBottomDialog] = useRecoilState(commonBottomDialogState)

  const [isTrueFirst, setIsTrueFirst] = useState(true)
  const [isTrueSecond, setIsTrueSecond] = useState(true)

  const [questionNum, setQuestionNum] = useRecoilState(questionNumSelector)
  const [firstQuestionCode, setFirstQuestionCode] = useRecoilState(firstQuestionCodeSelector)
  const [secondQuestionCode, setSecondQuestionCode] = useRecoilState(secondQuestionCodeSelector)
  const [firstQuestionText, setFirstQuestionText] = useRecoilState(firstQuestionTextSelector)
  const [secondQuestionText, setSecondQuestionText] = useRecoilState(secondQuestionTextSelector)
  const [firstAnswer, setFirstAnswer] = useRecoilState(firstAnswerSelector)
  const [secondAnswer, setSecondAnswer] = useRecoilState(secondAnswerSelector)

  const handleChangeFirstQuestion = (e) => {
    setFirstQuestionCode(e.target.id)
    setFirstQuestionText(e.target.innerText)
  }
  const handleChangeSecondQuestion = (e) => {
    setSecondQuestionCode(e.target.id)
    setSecondQuestionText(e.target.innerText)
  }
  const handleChangeFirstValue = (e) => setFirstAnswer(e.target.value.substring(0, 10))
  const handleChangeSecondValue = (e) => setSecondAnswer(e.target.value.substring(0, 10))

  const checkFirstAnswerValidation = (answer) => {
    // 영문, 한글, 숫자, 띄어쓰기 최대 10자리 정규식
    const regex = /^[ㄱ-ㅎ가-힣a-zA-Z0-9 ]{0,10}$/g
    if (regex.test(answer)) {
      setIsTrueFirst(true)
      setFirstAnswerHelperText(SIGN_UP_IDENTIFICATION.HELPER_TEXT)
    } else {
      setIsTrueFirst(false)
      setFirstAnswerHelperText(SIGN_UP_IDENTIFICATION.HELPER_TEXT_INVALID)
    }
  }

  const checkSecondAnswerValidation = (answer) => {
    // 영문, 한글, 숫자, 띄어쓰기 최대 10자리 정규식
    const regex = /^[ㄱ-ㅎ가-힣a-zA-Z0-9 ]{0,10}$/g
    if (regex.test(answer)) {
      setIsTrueSecond(true)
      setSecondAnswerHelperText(SIGN_UP_IDENTIFICATION.HELPER_TEXT)
    } else {
      setIsTrueSecond(false)
      setSecondAnswerHelperText(SIGN_UP_IDENTIFICATION.HELPER_TEXT_INVALID)
    }
  }

  const handleFirstQuestionClick = () => {
    setQuestionNum(SIGN_UP_IDENTIFICATION.FIRST_QUESTION)
    setIsOpenBottomDialog(true)
  }

  const handleSecondQuestionClick = () => {
    setQuestionNum(SIGN_UP_IDENTIFICATION.SECOND_QUESTION)
    setIsOpenBottomDialog(true)
  }

  const setQuestion = (questionCode, questionText) => {
    if (questionNum === SIGN_UP_IDENTIFICATION.FIRST_QUESTION) {
      setFirstQuestionCode(questionCode)
      setFirstQuestionText(questionText)
    } else {
      setSecondQuestionCode(questionCode)
      setSecondQuestionText(questionText)
    }
  }

  useEffect(() => {
    checkFirstAnswerValidation(firstAnswer)
  }, [firstAnswer])

  useEffect(() => {
    checkSecondAnswerValidation(secondAnswer)
  }, [secondAnswer])

  useEffect(() => {
    if (firstQuestionCode && secondQuestionCode && firstAnswer && secondAnswer && isTrueFirst && isTrueSecond) {
      setIsBottomBtnDisabled(false)
    } else {
      setIsBottomBtnDisabled(true)
    }

    setIdentificationQA([
      {
        question: firstQuestionCode,
        answerText: firstAnswer,
      }, {
        question: secondQuestionCode,
        answerText: secondAnswer,
      },
    ])
  }, [firstQuestionCode, secondQuestionCode, firstAnswer, secondAnswer, isTrueFirst, isTrueSecond])

  return (
    <Box sx={{pb: '70px'}}>

      <Typography variant={'h1_24_b'} sx={{color: '#000000'}}>
        {SIGN_UP_TITLE.IDENTIFICATION}
      </Typography>

      <TextFieldWrapper
        fullWidth
        tabIndex={0}
        autoComplete='off'
        value={firstQuestionText}
        label={firstQuestionText === '' ? SIGN_UP_IDENTIFICATION.LABEL_QUESTION : ' '}
        disabled
        onClick={handleFirstQuestionClick}
        onChange={handleChangeFirstQuestion}
        onKeyDown={(e) => handleEnterPress(e, handleFirstQuestionClick)}
        variant="outlined"
        InputProps={{
          endAdornment:
            <InputAdornment position="end" sx={{m: 0}}>
              <SvgCommonIcons type={ICON_TYPE.ARROW_DOWN}/>
            </InputAdornment>,
          style: {
            marginTop: '30px',
            padding: '15px 10px',
          }
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            letterSpacing: '-0.02em',
            color: '#CCCCCC',
            marginTop: '28px',
            zIndex: 0,
          }
        }}
        sx={{
          '& input': {
            paddingTop: '1px',
            paddingBottom: 0,
          },
          "& .MuiInputBase-input.Mui-disabled": {
            WebkitTextFillColor: '#222222',
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            letterSpacing: '-0.02em',
          },
        }}
      />

      <DonotsTextField
        tabIndex={0}
        handleChange={handleChangeFirstValue}
        helperText={firstAnswerHelperText}
        label={SIGN_UP_IDENTIFICATION.LABEL_ANSWER}
        type={'text'}
        value={firstAnswer}
        setValue={setFirstAnswer}
        isValidate={isTrueFirst}
      />

      {firstAnswer && isTrueFirst && <Box sx={{height: '26px'}}/>}

      <TextFieldWrapper
        fullWidth
        tabIndex={0}
        autoComplete='off'
        value={secondQuestionText}
        label={secondQuestionText === '' ? SIGN_UP_IDENTIFICATION.LABEL_QUESTION : ' '}
        disabled
        onClick={handleSecondQuestionClick}
        onChange={handleChangeSecondQuestion}
        onKeyDown={(e) => handleEnterPress(e, handleSecondQuestionClick)}
        variant="outlined"
        InputProps={{
          endAdornment:
            <InputAdornment position="end" sx={{m: 0}}>
              <SvgCommonIcons type={ICON_TYPE.ARROW_DOWN}/>
            </InputAdornment>,
          style: {
            marginTop: '30px',
            padding: '15px 10px',
          }
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            letterSpacing: '-0.02em',
            color: '#CCCCCC',
            marginTop: '28px',
            zIndex: 0,
          }
        }}
        sx={{
          '& input': {
            paddingTop: '1px',
            paddingBottom: 0,
          },
          "& .MuiInputBase-input.Mui-disabled": {
            WebkitTextFillColor: '#222222',
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            letterSpacing: '-0.02em',
          },
        }}
      />

      <DonotsTextField
        tabIndex={0}
        handleChange={handleChangeSecondValue}
        helperText={secondAnswerHelperText}
        label={SIGN_UP_IDENTIFICATION.LABEL_ANSWER}
        type={'text'}
        value={secondAnswer}
        setValue={setSecondAnswer}
        isValidate={isTrueSecond}
      />

      <BottomDialogQA title={SIGN_UP_IDENTIFICATION.BOTTOM_DIALOG_TITLE}>
        <BottomDialogContents setText={setQuestion} first={firstQuestionText} second={secondQuestionText}/>
      </BottomDialogQA>
    </Box>
  )
}
