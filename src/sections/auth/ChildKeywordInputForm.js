import {Box, Grid, Typography} from "@mui/material";
import * as React from "react";
import {useEffect} from "react";
import {useRecoilState, useSetRecoilState} from "recoil";
import {authBottomButtonState} from "../../recoil/atom/sign-up/auth";
import {SIGN_UP_SUBTITLE, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import {authChildKeywordSelector, authKeywordSelector} from "../../recoil/selector/sign-up/signUpSelector";
import {inputChildrenKeywordSelector} from "../../recoil/selector/home/inputChildrenInfoSelector";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {handleEnterPress} from "../../utils/onKeyDownUtils";

export default function ChildKeywordInputForm({isGuest = false}) {
  const [keyword, setKeyword] = useRecoilState(authKeywordSelector)
  const [childKeyword, setChildKeyword] = useRecoilState(authChildKeywordSelector)
  const setInputChildKeyword = useSetRecoilState(inputChildrenKeywordSelector)
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  // Child Keyword 값 변경 제어
  const handleClick = (e) => {
    let count = 0;
    const index = keyword.findIndex((listItem) => listItem.health_name === e.target.id);
    const newList = replaceItemAtIndex(keyword, index, {
      health_name: e.target.id,
      clicked: !keyword[index].clicked,
      health_key: keyword[index].health_key,
    });

    newList.map(obj => {
      if (obj.clicked === true) {
        count = count + 1;
      }
    });

    if (count < 4) {
      setKeyword(newList)

      // 아이 키워드 추가/삭제
      const idx = childKeyword.findIndex((listItem) => listItem.name === e.target.id)
      if (idx >= 0) {
        // 있으면 제거
        setChildKeyword(childKeyword.filter(key => key.name !== e.target.id))
        if (isGuest) setInputChildKeyword(childKeyword.filter(key => key.name !== e.target.id))
      } else {
        // 없으므로 추가
        const newKeyword = {
          name: e.target.id,
          key: newList[index].health_key
        }
        setChildKeyword([...childKeyword, newKeyword])
        if (isGuest) setInputChildKeyword([...childKeyword, newKeyword])
      }
    }
  }

  function replaceItemAtIndex(arr, index, newValue) {
    return [...arr.slice(0, index), newValue, ...arr.slice(index + 1)];
  }

  useEffect(() => {
    const index = keyword.findIndex((listItem) => listItem.clicked === true);
    setIsBottomBtnDisabled(index < 0);
  }, [keyword]);

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_KIDS_KEYWORD)
  }, [])

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          color: '#000000',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.CHILD_KEYWORD}
      </Typography>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 400,
          fontSize: '16px',
          lineHeight: '24px',
          letterSpacing: '-0.05em',
          color: '#666666',
          whiteSpace: 'pre-wrap',
          marginTop: '10px',
        }}
      >
        {SIGN_UP_SUBTITLE.CHILD_KEYWORD}
      </Typography>
      <Box sx={{flexGrow: 1}}>
        <Grid container rowSpacing="20px" columnSpacing="10px" sx={{marginTop: '10px', textAlign: 'center'}}
              columns={{xs: 3}}>
          {keyword.map(function (obj, idx) {
            return (
              <Grid
                item
                key={idx}
                sx={{
                  display: 'table',
                  height: '62px',
                  flexBasis: 'auto',
                  maxWidth: 'none',
                  '& .MuiGrid-root.MuiGrid-item': {
                    padding: 0,
                  }
                }}
              >
                <Box
                  tabIndex={0}
                  id={obj.health_name}
                  clicked={obj.clicked.toString()}
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    height: '100%',
                    border: obj.clicked ? '1.5px solid #ECA548' : '1.5px solid #F6F6F6',
                    borderRadius: '30px',
                    backgroundColor: obj.clicked ? 'primary.main' : '#F6F6F6',
                    fontStyle: 'normal',
                    fontWeight: obj.clicked ? 700 : 500,
                    fontSize: '14px',
                    lineHeight: '22px',
                    letterSpacing: '-0.02em',
                    color: obj.clicked ? '#FFFFFF' : '#666666',
                    paddingX: '20px',
                    justifyContent: 'center'
                  }}
                  onClick={handleClick}
                  onKeyDown={(e) => handleEnterPress(e, handleClick)}
                >
                  {obj.health_name}
                </Box>
              </Grid>
            );
          })}
        </Grid>
      </Box>
    </Box>
  );
}