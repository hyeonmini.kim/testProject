import {Box, InputAdornment, Typography} from "@mui/material";
import {useEffect, useState} from "react";
import {useRecoilState} from "recoil";
import {SIGN_UP_CHILD_NICKNAME, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {authChildNicknameSelector} from "../../recoil/selector/sign-up/signUpSelector";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {ALT_STRING} from "../../constant/common/AltString";
import TextFieldWrapper from "../../components/common/TextFieldWrapper";

export default function ChildrenNicknameInputSection({setIsBottomButtonDisabled}) {
  const [childNickname, setChildNickname] = useRecoilState(authChildNicknameSelector)
  const [isTrue, setIsTrue] = useState(true)
  const [helperText, setHelperText] = useState(SIGN_UP_CHILD_NICKNAME.HELPER_TEXT)
  const [cancelIcon, setCancelIcon] = useState(false)

  const handleChangeValue = (e) => {
    const value = e.target.value?.substring(0, 10)
    checkChildNicknameValidation(value)

    setChildNickname(value)
  }

  const checkChildNicknameValidation = (value) => {
    const regex = /^[ㄱ-ㅎ가-힣a-zA-Z0-9]{2,10}$/g
    if (regex.test(value)) {
      setIsTrue(true)
      setIsBottomButtonDisabled(false)
    } else {
      setIsTrue(false)
      setHelperText(SIGN_UP_CHILD_NICKNAME.HELPER_TEXT)
      setIsBottomButtonDisabled(true)
    }
  }

  const handleFocus = () => setCancelIcon(true)
  const handleBlur = () => setCancelIcon(false)
  const handleCancel = () => setChildNickname('')

  useEffect(() => {
    checkChildNicknameValidation(childNickname)
  }, [childNickname])

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_KIDS_NAME)
  }, [])

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          color: '#000000',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.CHILD_NICKNAME}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={childNickname}
        label={childNickname === '' ? SIGN_UP_CHILD_NICKNAME.LABEL : ' '}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        helperText={childNickname === '' ? helperText : isTrue ? ' ' : helperText}
        inputProps={{tabIndex: 0}}
        InputProps={{
          endAdornment: childNickname && cancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
            </InputAdornment>
          ),
          style: {
            paddingBottom: '11px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#222222',
          },
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#CCCCCC',
          }
        }}
        FormHelperTextProps={{
          style: {
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            letterSpacing: '-0.03em',
            color: childNickname === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          marginTop: '14px',
          '& .MuiInput-root:before': {
            borderBottom: childNickname === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: childNickname === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: childNickname === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: childNickname === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: childNickname === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  )
}
