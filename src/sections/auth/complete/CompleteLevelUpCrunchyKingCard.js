import {Box, Card, Stack, Typography} from "@mui/material";
import {LEVEL_UP_CRUNCH_KING} from "../../../constant/dialog/Complete";
import DonotsTextField from "../../../components/common/DonotsTextField";

export default function CompleteLevelUpCrunchyKingCard({snsUrl, setSnsUrl}) {

  const handleChangeValue = (e) => setSnsUrl(e.target.value?.trim())

  return (
    <Card sx={{
      textAlign: 'start',
      m: '40px 38px 0 38px',
      p: '40px 20px',
      borderRadius: '8px',
      boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.08)',
    }}>

      <Box sx={{textAlign: 'center'}}>
        <Typography variant={'b1_16_r'} sx={{color: '#222222'}}>{LEVEL_UP_CRUNCH_KING.CARD_TEXT}</Typography>
      </Box>

      <Stack sx={{mt: '30px'}}>
        <Typography variant={'b2_14_b_1l'} sx={{color: '#888888'}}>{LEVEL_UP_CRUNCH_KING.INPUT_TITLE}</Typography>
        <DonotsTextField
          tabIndex={0}
          handleChange={handleChangeValue}
          label={LEVEL_UP_CRUNCH_KING.INPUT_LABEL}
          type={'text'}
          value={snsUrl}
          setValue={setSnsUrl}
        />
      </Stack>

    </Card>
  )
}
