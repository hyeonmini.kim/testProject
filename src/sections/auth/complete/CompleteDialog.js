import {MainDialogLayout} from "../../../layouts/main/MainLayout";
import BottomFixedTwoButton from "../../../components/recipe/create/button/BottomFixedTwoButton";
import {Box, Fade, Typography} from "@mui/material";
import BottomFixedOneButton from "../../../components/common/BottomFixedOneButton";
import PropTypes from "prop-types";
import DonotsMembership from "./DonotsMembership";
import {Z_INDEX} from "../../../constant/common/ZIndex";
import {useEffect} from "react";

CompleteDialog.prototype = {
  isOpen: PropTypes.bool,
  title: PropTypes.string,
  grade: PropTypes.string,
  oneButton: PropTypes.shape({
    text: PropTypes.string,
    onClick: PropTypes.func,
  }),
  twoButton: PropTypes.shape({
    text: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
  }),
  children: PropTypes.node,
}
export default function CompleteDialog({isOpen, title, grade, children, oneButton, twoButton, onClick}) {
  useEffect(()=>{
    if(onClick){
      onClick()
    }
  },[])

  return (
    <MainDialogLayout
      fullScreen
      open={isOpen}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}, textAlign: '-webkit-center'}}
    >

      <Box sx={{pb: '30%'}}>

        {/* 화면 별 텍스트 */}
        <Box sx={{mt: '100px'}}>
          <Typography variant={'h2_20_m'}>{title}</Typography>
        </Box>

        {/* 등급 분류 영역 */}
        <Box sx={{m: '40px 38px 0 38px'}}>
          <DonotsMembership active={grade}/>
        </Box>

        {/* 화면 별 설명 카드 */}
        {children}

        {/* 하단 버튼 */}
        {
          !!!twoButton
            ? <BottomFixedOneButton buttonProps={{text: oneButton?.text, onClick: oneButton?.onClick, isDisabled: false}}/>
            : <BottomFixedTwoButton
              leftButtonProps={{
                text: oneButton?.text,
                onClick: oneButton?.onClick,
                isDisabled: false,
              }}
              rightButtonProps={{
                text: twoButton?.text,
                onClick: twoButton?.onClick,
                isDisabled: twoButton?.disabled,
              }}
            />
        }

      </Box>

    </MainDialogLayout>
  )
}
