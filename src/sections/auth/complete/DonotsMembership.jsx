import React from 'react'
import styled from 'styled-components';
import PropTypes from "prop-types";
import DonotsLevelUp from "./DonotsLevelUp";
import {GRADE_STEPS} from "../../../constant/dialog/Complete";

const Wrap = styled.div`
  position: relative;
  width: 100%;
  max-width: 440px;
`

const Backface = styled.div`
  position: absolute;
  z-index: 2;
  top: 10px;
  left: 50%;
  transform: translateX(-71%);
  width: 24px;
  height: 24px;
  background: #fff;
  border-radius: 50%;
`

const Dashline = styled.div`
  position: absolute;
  z-index: 1;
  top: 22px;
  width: calc(100% - 54px);
  height: 1px;
  margin-left: 16px;
  border-top: 1px dashed #E6E6E6;
`

const ContentBox = styled.div`
  position: relative;
  z-index: 3;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

DonotsMembership.propTypes = {
  active: PropTypes.string
}

function DonotsMembership({active}) {
  return (
    <Wrap>
      <Backface/>
      <Dashline/>
      <ContentBox>
        {GRADE_STEPS.map((item, index) => (
          <DonotsLevelUp key={index} isActive={active === item.step} img={item.img} level={item.step} name={item.name}/>
        ))}
      </ContentBox>
    </Wrap>
  );
}

export default DonotsMembership;
