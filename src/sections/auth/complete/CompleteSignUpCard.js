import {useEffect, useState} from "react";
import {Avatar, Badge, Box, Card, DialogContent, Divider, Fade, Stack, Typography} from "@mui/material";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {ALT_STRING} from "../../../constant/common/AltString";
import {clickBorderNone, IMAGE_COMPRESSION_OPTIONS} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {MainDialogLayout} from "../../../layouts/main/MainLayout";
import {Z_INDEX} from "../../../constant/common/ZIndex";
import {isNative} from "../../../utils/envUtils";
import {bytesToImageFile} from "../../../utils/fileUtils";
import imageCompression from "browser-image-compression";
import {callOpenNativeGalleryChannel} from "../../../channels/photoChannel";
import {UploadAvatar} from "../../../components/my/upload";
import LoadingScreen from "../../../components/common/LoadingScreen";
import {fileData} from "../../../components/file-thumbnail";
import {COMPLETE_SIGN_UP} from "../../../constant/dialog/Complete";
import useNativeBackKey from "../../../hooks/useNativeBackKey";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";

export default function CompleteSignUpCard({profilePictureUrl, setProfilePictureUrl}) {
  const [isOpenSelectImage, setIsOpenSelectImage] = useState(false)

  const handleGalleryClick = () => {
    setIsOpenSelectImage(true)
    setHackleTrack(HACKLE_TRACK.SIGNUP_COMPLETE_PHOTO)
  }
  const handleCloseDialog = () => setIsOpenSelectImage(false)

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      if(isOpenSelectImage) {
        setIsOpenSelectImage(false)
      }
    }
  }, [handleBackKey])

  return (
    <>
      <Card sx={{m: '40px 38px 0 38px', p: '30px 43px 27px 43px', borderRadius: '8px', boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.08)'}}>

        <Typography variant={'b1_16_r'}>{COMPLETE_SIGN_UP.CARD_TEXT}</Typography>

        <Badge
          overlap="circular"
          anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
          badgeContent={
            <SvgCommonIcons alt={ALT_STRING.MY.BTN_CHANGE_PROFILE} type={ICON_TYPE.PHOTO_ADD}/>
          }
          onClick={handleGalleryClick}
          sx={{
            width: '88px',
            height: '88px',
            m: '28px 0 11px 0',
            display: 'flex',
            cursor: 'pointer',
            overflow: 'hidden',
            alignItems: 'center',
            position: 'relative',
            justifyContent: 'center',
            textDecoration: 'none',
            WebkitTapHighlightColor: 'transparent',
          }}
        >
          {profilePictureUrl
            ? (<Avatar
              alt={ALT_STRING.MY.PROFILE}
              tabIndex={1}
              src={fileData(profilePictureUrl)?.preview}
              sx={{width: '88px', height: '88px'}}
              onKeyDown={(e) => handleEnterPress(e, handleGalleryClick)}
            />)
            : (<SvgCommonIcons
              alt={ALT_STRING.MY.BTN_CHANGE_PROFILE}
              tabIndex={1}
              type={ICON_TYPE.PROFILE_NO_IMAGE_80PX}
              sx={{width: '88px', ...clickBorderNone}}
              onKeyDown={(e) => handleEnterPress(e, handleGalleryClick)}
            />)
          }
        </Badge>

      </Card>

      <SelectImageDialog isOpen={isOpenSelectImage} handleClose={handleCloseDialog}>
        <SelectImageOrDelete setIsOpen={setIsOpenSelectImage} profilePictureUrl={profilePictureUrl}
                             setProfilePictureUrl={setProfilePictureUrl}/>
      </SelectImageDialog>

    </>
  )
}

function SelectImageDialog({children, isOpen = false, handleClose}) {

  return (
    <MainDialogLayout
      open={isOpen}
      onClose={handleClose}
      TransitionComponent={Fade}
      disableScrollLock={true}
      fullWidth
      PaperProps={{
        style: {
          margin: 0,
          borderRadius: '16px 16px 0 0',
          position: 'fixed',
          bottom: 0,
          backgroundColor: '#FFFFFF',
        },
      }}
      sx={{
        zIndex: Z_INDEX.BOTTOM_DIALOG, '& .MuiDialog-paper': {mx: 0}
      }}
    >
      <DialogContent sx={{margin: '0 20px 6px 20px', padding: 0, textAlign: '-webkit-center'}}>
        {children}
      </DialogContent>
    </MainDialogLayout>
  );
}

function SelectImageOrDelete({setIsOpen, profilePictureUrl, setProfilePictureUrl}) {
  const [isCompress, setIsCompress] = useState(false)

  const handleClickDelete = (e) => {
    setIsOpen(false);
    setProfilePictureUrl("");
  }

  const setFileWithImage = async (image) => {
    setIsOpen(false);
    if (isNative()) {
      image = bytesToImageFile(image)
    } else {
      setIsCompress(true)
      const resizingBlob = await imageCompression(image, IMAGE_COMPRESSION_OPTIONS);
      image = new File([resizingBlob], image.name, {type: image.type});
      setIsCompress(false)
    }
    const newImage = Object.assign(image, {
      preview: URL.createObjectURL(image),
    })
    setProfilePictureUrl(newImage);
  }

  const handleDrop = (images) => {
    setIsOpen(false)
    setFileWithImage(images[0])
  }

  const handleGalleryChannel = () => {
    setIsOpen(false)
    callOpenNativeGalleryChannel(0, 1, (images) => setFileWithImage(images[0]))
  }

  return (
    <>
      <Box sx={{mt: '6px', textAlign: 'start'}}>
        <Typography
          sx={{
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '24px',
            color: '#222222',
            paddingY: '20px'
          }}
        >
          프로필 사진 선택하기
        </Typography>
        <Divider sx={{height: '1px', backgroundColor: '#E6E6E6'}}/>
        {isNative()
          ? <UploadAvatar profilePictureUrl={profilePictureUrl} onClick={handleGalleryChannel}/>
          : <UploadAvatar profilePictureUrl={profilePictureUrl}
                          onDrop={(images) => handleDrop(images)}/>
        }
        <Stack
          tabIndex={1}
          direction="row"
          sx={{
            alignItems: 'center',
            ...clickBorderNone
          }}
          onClick={handleClickDelete}
          onKeyDown={(e) => handleEnterPress(e, handleClickDelete)}
        >
          <SvgCommonIcons type={ICON_TYPE.DELETE_PHOTO}/>
          <Typography
            value="사진 삭제"
            sx={{
              fontStyle: 'normal',
              fontWeight: 500,
              fontSize: '16px',
              lineHeight: '24px',
              color: '#222222',
              paddingX: '10px',
              paddingY: '20px',
            }}
          >
            사진 삭제
          </Typography>
        </Stack>
      </Box>
      {isCompress && <LoadingScreen dialog/>}
    </>
  );
}
