import React from 'react'
import styled from 'styled-components';
import PropTypes from "prop-types";

const Wrap = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`

const Items = styled.div`
  position: relative;

  &:nth-of-type(2):after {
    content: '';
    display: block;
    position: absolute;
    top: 27%;
    left: 2px;
    z-index: 1;
    width: 50%;
    border-top: 1px dashed #E6E6E6;
  }
`

const ImgWrap = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 44px;
  height: 44px;
  margin: 0 auto;
`

const ImgWrap_Img = {
  position: 'relative',
  zIndex: 2,
  width: '24px',
  transition: '0.3s'
}

const ImgWrap_Img_Active = {
  width: '50px',
  transition: '0.3s'
}

const Typo = styled.p`
  margin-top: 10px;
  font-size: 16px;
  font-weight: 500;
  line-height: 10px;
  text-align: center;
  white-space: nowrap;

  & span {
    display: block;
    margin-bottom: 8px;
    font-size: 12px;
    font-weight: 400;
    line-height: 12px;
  }
;
`

const Typo_inner = {
  color: '#ccc',
  '& span': {color: '#ccc'}
}

const Typo_inner_Active = {
  color: '#666',
  '& span': {color: '#666'}
}

DonotsLevelUp.propTypes = {
  isActive: PropTypes.bool,
  level: PropTypes.string,
  name: PropTypes.string,
  img: PropTypes.string,
}

function DonotsLevelUp({isActive, level, name, img}) {
  return (
    <div>
      <Wrap>
        <Items>
          <ImgWrap>
            <img
              src={img}
              style={isActive ? ImgWrap_Img_Active : ImgWrap_Img}
              alt={`${name}등급`}/>
          </ImgWrap>
          <Typo style={isActive ? Typo_inner_Active : Typo_inner}><span>level {level.substring(2, 3)}</span>{name}</Typo>
        </Items>
      </Wrap>
    </div>
  );
}

export default DonotsLevelUp;
