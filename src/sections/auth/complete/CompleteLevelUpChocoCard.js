import {Box, Card, Stack, Typography} from "@mui/material";
import {LEVEL_UP_CHOCO} from "../../../constant/dialog/Complete";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";

export default function CompleteLevelUpChocoCard() {

  return (
    <Card sx={{
      textAlign: 'start',
      m: '40px 38px 0 38px',
      p: '40px 20px',
      borderRadius: '8px',
      boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.08)',
    }}>

      <Box sx={{ml: '10px'}}>
        <Typography variant={'b1_16_r'}>{LEVEL_UP_CHOCO.CARD_TEXT_1}</Typography>
      </Box>

      <Stack direction={'row'} sx={{mt: '20px'}}>
        <SvgCommonIcons type={ICON_TYPE.CHECK_16PX} sx={{m: '4px 6px 0 0'}}/>
        <Box>
          <Typography variant={'b1_16_r'}>{LEVEL_UP_CHOCO.CARD_TEXT_2_1}</Typography>
          <Typography variant={'b1_16_b'}>{LEVEL_UP_CHOCO.CARD_TEXT_2_2}</Typography>
          <Typography variant={'b1_16_r'}>{LEVEL_UP_CHOCO.CARD_TEXT_2_3}</Typography>
        </Box>
      </Stack>

      <Stack direction={'row'} sx={{mt: '20px'}}>
        <SvgCommonIcons type={ICON_TYPE.CHECK_16PX} sx={{m: '4px 6px 0 0'}}/>
        <Box>
          <Typography variant={'b1_16_r'}>{LEVEL_UP_CHOCO.CARD_TEXT_3_1}</Typography>
          <Typography variant={'b1_16_b'}>{LEVEL_UP_CHOCO.CARD_TEXT_3_2}</Typography>
          <Typography variant={'b1_16_r'}>{LEVEL_UP_CHOCO.CARD_TEXT_3_3}</Typography>
        </Box>
      </Stack>

    </Card>
  )
}
