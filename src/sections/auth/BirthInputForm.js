import {Box, InputAdornment, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import {authBirthdayState, authBottomButtonState} from "../../recoil/atom/sign-up/auth";
import {useEffect, useState} from "react";
import {SIGN_UP_BIRTH, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {checkBirthValidation} from "../../utils/customValidation";
import TextFieldWrapper from "../../components/common/TextFieldWrapper";

export default function BirthInputForm() {
  const [birth, setBirth] = useRecoilState(authBirthdayState)

  // 입력 상태 state
  const [isTrue, setIsTrue] = useState(true)
  const [helperText, setHelperText] = useState(SIGN_UP_BIRTH.HELPER_TEXT)
  const [cancelIcon, setCancelIcon] = useState(false)
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  // Birth 값 변경 제어
  const handleChangeValue = (e) => {
    // 숫자 정규식
    const regex = /[^0-9]/g
    let value = e.target.value
    let valueNum = value.replace(regex, '').substring(0, 8)

    checkBirthValidation(valueNum, setIsTrue, setIsBottomBtnDisabled, setHelperText)

    // 생년월일 사이에 콤마를 넣기위한 처리
    if (valueNum.length <= 4) {
      value = valueNum
    }
    if (valueNum.length > 4) {
      value = valueNum.slice(0, 4) + "." + valueNum.slice(4)
    }
    if (valueNum.length > 6) {
      value = valueNum.slice(0, 4) + "." + valueNum.slice(4, 6) + "." + valueNum.slice(6)
    }

    // 생년월일 변환
    value = value.replaceAll('.', '-')

    setBirth(value)
  }

  useEffect(() => {
    if (birth.length === 10) {
      setIsTrue(true)
      setIsBottomBtnDisabled(false)
    }
  }, [])

  useEffect(() => {
    if (birth.length === 0) {
      setIsTrue(true)
      setHelperText(SIGN_UP_BIRTH.HELPER_TEXT)
      setIsBottomBtnDisabled(true)
    }
  }, [birth])

  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true)
  const handleBlur = () => setCancelIcon(false)
  const handleCancel = () => setBirth('')

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          color: '#000000',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.BIRTH}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={birth.replaceAll('-', '.')}
        label={birth === '' ? SIGN_UP_BIRTH.LABEL : ' '}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        type="tel"
        helperText={birth === '' ? helperText : isTrue ? ' ' : helperText}
        inputProps={{tabIndex: 0}}
        InputProps={{
          endAdornment: birth && cancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
            </InputAdornment>
          ),
          style: {
            paddingBottom: '11px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#222222',
          },
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#CCCCCC',
          }
        }}
        FormHelperTextProps={{
          style: {
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            letterSpacing: '-0.03em',
            color: birth === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          marginTop: '14px',
          '& .MuiInput-root:before': {
            borderBottom: birth === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: birth === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: birth === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: birth === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: birth === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  )
}