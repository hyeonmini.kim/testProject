import {Box, Typography} from "@mui/material";
import {GET_FIND_ID_QUESTIONS_TEXT, SIGN_UP_EMAIL, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import DonotsTextField from "../../components/common/DonotsTextField";
import {useRecoilState, useSetRecoilState} from "recoil";
import {
  authBottomButtonState,
  authResetPasswordAnswer1ErrorState,
  authResetPasswordAnswer1HelperTextState,
  authResetPasswordAnswer2ErrorState,
  authResetPasswordAnswer2HelperTextState
} from "../../recoil/atom/sign-up/auth";
import {
  authResetPasswordAnswer1Selector,
  authResetPasswordAnswer2Selector,
  authResetPasswordQuestion1Selector,
  authResetPasswordQuestion2Selector
} from "../../recoil/selector/sign-up/signUpSelector";
import {useEffect} from "react";

export default function ResetPasswordAnswerInputForm() {
  // Email 정보 state
  const [resetPasswordAnswer1, setResetPasswordAnswer1] = useRecoilState(authResetPasswordAnswer1Selector)
  const [resetPasswordAnswer2, setResetPasswordAnswer2] = useRecoilState(authResetPasswordAnswer2Selector)
  // 입력 상태 state
  const [isTrue1, setIsTrue1] = useRecoilState(authResetPasswordAnswer1ErrorState)
  const [isTrue2, setIsTrue2] = useRecoilState(authResetPasswordAnswer2ErrorState)
  const [helperText1, setHelperText1] = useRecoilState(authResetPasswordAnswer1HelperTextState)
  const [helperText2, setHelperText2] = useRecoilState(authResetPasswordAnswer2HelperTextState)
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  const [resetPasswordQuestion1, setResetPasswordQuestion1] = useRecoilState(authResetPasswordQuestion1Selector)
  const [resetPasswordQuestion2, setResetPasswordQuestion2] = useRecoilState(authResetPasswordQuestion2Selector)

  const handleChangeValue1 = (e) => setResetPasswordAnswer1(e.target.value?.substring(0, 10))
  const handleChangeValue2 = (e) => setResetPasswordAnswer2(e.target.value?.substring(0, 10))

  useEffect(() => {
    if (resetPasswordAnswer1 === '' || resetPasswordAnswer2 === '') {
      setIsBottomBtnDisabled(true)
    } else {
      setIsBottomBtnDisabled(false)
    }

    if(resetPasswordAnswer1 === '') {
      setHelperText1('')
      setIsTrue1(true)
    }
    if(resetPasswordAnswer2 === '') {
      setHelperText2('')
      setIsTrue2(true)
    }
  }, [resetPasswordAnswer1, resetPasswordAnswer2])

  return (
    <>

      <Box sx={{position: 'relative', pb: '70px'}}>

        <Typography variant={'h1_24_b'} sx={{color: '#000000'}}>
          {SIGN_UP_TITLE.INPUT_ANSWER_FIND_ID}
        </Typography>

        <Box sx={{mt: '30px', p: '10px', height: '52px', background: '#F5F5F5', border: '1px solid #E6E6E6', borderRadius: '8px',}}>
          <Box sx={{height: '2px'}}/>
          <Typography variant={'b1_16_m'} sx={{color: '#BFBFBF'}}>
            {GET_FIND_ID_QUESTIONS_TEXT(resetPasswordQuestion1)}
          </Typography>
        </Box>

        <DonotsTextField
          tabIndex={0}
          handleChange={handleChangeValue1}
          helperText={helperText1}
          label={SIGN_UP_EMAIL.LABEL_INPUT_ANSWER}
          type={'text'}
          value={resetPasswordAnswer1}
          setValue={setResetPasswordAnswer1}
          isValidate={isTrue1}
        />

        <Box sx={{height: '40px'}}/>

        <Box sx={{mt: '30px', p: '10px', height: '52px', background: '#F5F5F5', border: '1px solid #E6E6E6', borderRadius: '8px',}}>
          <Box sx={{height: '2px'}}/>
          <Typography variant={'b1_16_m'} sx={{color: '#BFBFBF'}}>
            {GET_FIND_ID_QUESTIONS_TEXT(resetPasswordQuestion2)}
          </Typography>
        </Box>

        <DonotsTextField
          tabIndex={0}
          handleChange={handleChangeValue2}
          helperText={helperText2}
          label={SIGN_UP_EMAIL.LABEL_INPUT_ANSWER}
          type={'text'}
          value={resetPasswordAnswer2}
          setValue={setResetPasswordAnswer2}
          isValidate={isTrue2}
        />

      </Box>

    </>
  )
}