import {Box, Typography} from "@mui/material";
import {SIGN_UP_EMAIL, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import DonotsTextField from "../../components/common/DonotsTextField";
import {useRecoilState, useSetRecoilState} from "recoil";
import {
  authBottomButtonState,
  authBottomButtonTextState,
  authEmailFindIdErrorState,
  authEmailFindIdHelperTextState,
  authEmailFindIdState
} from "../../recoil/atom/sign-up/auth";
import {emailSchema, isValidateParam} from "../../utils/validateUtils";
import {useEffect} from "react";

export default function FindIdInputForm() {
  // Email 정보 state
  const [email, setEmail] = useRecoilState(authEmailFindIdState)
  // 입력 상태 state
  const [isTrue, setIsTrue] = useRecoilState(authEmailFindIdErrorState)
  const [helperText, setHelperText] = useRecoilState(authEmailFindIdHelperTextState)
  // 바텀 버튼 state
  const setBottomBtnText = useSetRecoilState(authBottomButtonTextState)
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  // Email 값 변경 제어
  const handleChangeValue = (e) => setEmail(e.target.value.toLowerCase())
  // Email 정규식 체크
  const checkEmailValidation = () => {
    isValidateParam(email, emailSchema).then((isVal) => {
      if (isVal) {
        setIsTrue(true)
        setIsBottomBtnDisabled(email === '')
      } else {
        setIsTrue(false)
        setHelperText(SIGN_UP_EMAIL.HELPER_TEXT_WRONG)
        setIsBottomBtnDisabled(true)
      }
    })
  }

  useEffect(() => {
    setBottomBtnText('확인')
  }, [])

  useEffect(() => {
    checkEmailValidation()
  }, [email])

  return (
    <>

      <Box sx={{position: 'relative'}}>

        <Typography variant={'h1_24_b'} sx={{color: '#000000'}}>
          {SIGN_UP_TITLE.EMAIL_FIND_ID}
        </Typography>

        <DonotsTextField
          tabIndex={0}
          handleChange={handleChangeValue}
          helperText={email ? helperText : ""}
          label={SIGN_UP_EMAIL.LABEL_FIND_ID}
          type={'text'}
          value={email}
          setValue={setEmail}
          isValidate={isTrue}
        />

      </Box>

    </>
  )
}