import {Box, IconButton, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import {authBottomButtonState} from "../../recoil/atom/sign-up/auth";
import {useEffect, useState} from "react";
import {CHILD_WEIGHT, SIGN_UP_CHILD_WEIGHT, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {authChildWeightSelector} from "../../recoil/selector/sign-up/signUpSelector";
import {inputChildrenWeightSelector} from "../../recoil/selector/home/inputChildrenInfoSelector";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {ALT_STRING} from "../../constant/common/AltString";
import TextFieldWrapper from "../../components/common/TextFieldWrapper";

export default function ChildWeightInputForm({isGuest = false}) {
  const [childWeight, setChildWeight] = useRecoilState(authChildWeightSelector)
  const setInputChildWeight = useSetRecoilState(inputChildrenWeightSelector)
  // 입력 상태 state
  const [isTrue, setIsTrue] = useState(true)
  const [helperText, setHelperText] = useState(SIGN_UP_CHILD_WEIGHT.HELPER_TXT)
  const [cancelIcon, setCancelIcon] = useState(false)
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  const [hasDot, setHasDot] = useState(false)
  const [hasDotTwoMore, setHasDotTwoMore] = useState(false)
  // Child Weight 값 변경 제어
  const handleChangeValue = (e) => {
    const weight = (e.target.value.length === 1 && e.target.value === '0') ? '' : e.target.value
    setChildWeight(weight)
    if (isGuest) setInputChildWeight(weight)
  }
  // Child Height 정규식 체크
  const checkChildWeightValidation = () => {
    // 소수점 첫째 자리 정규식
    const regex = /^\d*[.]?$/
    const regexOnlyDot = /^[.]$/
    const regexLastDot = /^\d*[.]$/
    const regexHasDot = /^\d*[.]\d$/

    if (childWeight !== '') {
      if (childWeight > CHILD_WEIGHT.MAX) {
        setErrorHelperText(SIGN_UP_CHILD_WEIGHT.HELPER_TXT_MAX_WEIGHT)
        setIsTrue(false)
        setIsBottomBtnDisabled(true)
      } else if (childWeight < CHILD_WEIGHT.MIN) {
        setErrorHelperText(SIGN_UP_CHILD_WEIGHT.HELPER_TXT_MIN_WEIGHT)
        setIsTrue(false)
        setIsBottomBtnDisabled(true)
      } else {
        if (regex.test(childWeight) && !regexOnlyDot.test(childWeight) && !regexLastDot.test(childWeight)) {
          setErrorHelperText(SIGN_UP_CHILD_WEIGHT.HELPER_TXT)
          setIsTrue(true)
          setHasDot(false)
          setIsBottomBtnDisabled(false)
        } else {
          if (regexHasDot.test(childWeight)) {
            setIsTrue(true)
            setHasDot(true)
            setHasDotTwoMore(false)
            setIsBottomBtnDisabled(false)
          } else {
            setErrorHelperText(SIGN_UP_CHILD_WEIGHT.HELPER_TXT)
            setIsTrue(false)
            setHasDot(true)
            setHasDotTwoMore(true)
            setIsBottomBtnDisabled(true)
          }
        }
      }
    } else {
      setErrorHelperText(SIGN_UP_CHILD_WEIGHT.HELPER_TXT)
      setIsTrue(false)
      setIsBottomBtnDisabled(true)
    }
  }

  const setErrorHelperText = (helperText) => {
    if (helperText === SIGN_UP_CHILD_WEIGHT.HELPER_TXT && !hasDotTwoMore) {
      setIsTrue(true)
      setHelperText(helperText)
      setIsBottomBtnDisabled(false)
    } else {
      setIsTrue(false)
      setHelperText(helperText)
      setIsBottomBtnDisabled(true)
    }
  }

  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true)
  const handleBlur = () => setCancelIcon(false)
  const handleCancel = () => setChildWeight('')

  useEffect(() => {
    checkChildWeightValidation()
  }, [childWeight])

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_KIDS_WEIGHT)
  }, [])

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          color: '#000000',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.CHILD_WEIGHT}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={childWeight}
        label={childWeight === '' ? SIGN_UP_CHILD_WEIGHT.LABEL : ' '}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        helperText={childWeight === '' ? helperText : isTrue && hasDot ? ' ' : helperText}
        inputProps={{inputMode: 'decimal', tabIndex: 0}}
        InputProps={{
          endAdornment: (
            childWeight === '' ?
              <Typography
                sx={{
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '22px',
                  letterSpacing: '-0.03em',
                  color: '#000000',
                  marginRight: '14px',
                }}
              >
                kg
              </Typography>
              :
              <Box display="flex" marginRight="14px">
                {childWeight && cancelIcon && (
                  <IconButton tabIndex={-1} sx={{p: 0, marginRight: '8px'}}>
                    <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
                  </IconButton>
                )}
                <Typography
                  sx={{
                    fontStyle: 'normal',
                    fontWeight: 500,
                    fontSize: '16px',
                    lineHeight: '22px',
                    letterSpacing: '-0.03em',
                    color: '#000000',
                  }}
                >
                  kg
                </Typography>
              </Box>
          ),
          style: {
            paddingBottom: '11px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#222222',
          },
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#CCCCCC',
          }
        }}
        FormHelperTextProps={{
          style: {
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            letterSpacing: '-0.03em',
            color: childWeight === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          marginTop: '14px',
          '& .MuiInput-root:before': {
            borderBottom: childWeight === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: childWeight === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: childWeight === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: childWeight === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: childWeight === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  );
}