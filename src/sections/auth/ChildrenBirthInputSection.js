import {Box, InputAdornment, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import {useEffect, useState} from "react";
import {SIGN_UP_CHILD_BIRTH, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {authChildBirthSelector} from "../../recoil/selector/sign-up/signUpSelector";
import {checkChildBirthValidation} from "../../utils/customValidation";
import {inputChildrenBirthSelector} from "../../recoil/selector/home/inputChildrenInfoSelector";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {ALT_STRING} from "../../constant/common/AltString";
import TextFieldWrapper from "../../components/common/TextFieldWrapper";

export default function ChildrenBirthInputSection({isGuest = false, setIsBottomButtonDisabled}) {
  const [childBirth, setChildBirth] = useRecoilState(authChildBirthSelector)
  const setInputChildBirth = useSetRecoilState(inputChildrenBirthSelector)

  const [isTrue, setIsTrue] = useState(true)
  const [helperText, setHelperText] = useState(SIGN_UP_CHILD_BIRTH.HELPER_TEXT)
  const [cancelIcon, setCancelIcon] = useState(false)

  const handleChangeValue = (e) => {
    const regex = /[^0-9]/g
    let value = e.target.value
    let valueNum = value?.replace(regex, '').substring(0, 6)

    checkChildBirthValidation(valueNum, setIsTrue, setIsBottomButtonDisabled, setHelperText)

    if (valueNum.length <= 2) {
      value = valueNum
    }
    if (valueNum.length > 2) {
      value = valueNum.slice(0, 2) + "." + valueNum.slice(2)
    }
    if (valueNum.length > 4) {
      value = valueNum.slice(0, 2) + "." + valueNum.slice(2, 4) + "." + valueNum.slice(4)
    }

    value = value.replaceAll('.', '-')
    value = value.length > 0 ? "20" + value : value

    setChildBirth(value)
    if (isGuest) setInputChildBirth(value)
  }

  useEffect(() => {
    if (childBirth.length === 10) {
      setIsTrue(true)
      setIsBottomButtonDisabled(false)
    }
  }, [])

  useEffect(() => {
    if (childBirth.length === 0) {
      setIsTrue(true)
      setHelperText(SIGN_UP_CHILD_BIRTH.HELPER_TEXT)
      setIsBottomButtonDisabled(true)
    }
  }, [childBirth])

  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true)
  const handleBlur = () => setCancelIcon(false)
  const handleCancel = () => setChildBirth('')

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_KIDS_BIRTH)
  }, [])

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          color: '#000000',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.CHILD_BIRTH}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={childBirth.replaceAll('-', '.').substring(2, childBirth.length)}
        label={childBirth === '' ? SIGN_UP_CHILD_BIRTH.LABEL : ' '}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        type="tel"
        helperText={childBirth === '' ? helperText : isTrue ? ' ' : helperText}
        inputProps={{tabIndex: 0}}
        InputProps={{
          endAdornment: childBirth && cancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
            </InputAdornment>
          ),
          style: {
            paddingBottom: '11px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#222222',
          },
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#CCCCCC',
          }
        }}
        FormHelperTextProps={{
          style: {
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            letterSpacing: '-0.03em',
            color: childBirth === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          marginTop: '14px',
          '& .MuiInput-root:before': {
            borderBottom: childBirth === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: childBirth === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: childBirth === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: childBirth === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: childBirth === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  )
}
