import {Box, InputAdornment, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import {
  authBottomButtonState,
  authBottomButtonTextState,
  authIdErrorState,
  authIdHelperTextState,
  authIdState
} from "../../recoil/atom/sign-up/auth";
import {useEffect, useState} from "react";
import {RESET_PASSWORD, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import TextFieldWrapper from "../../components/common/TextFieldWrapper";

export default function ResetPasswordInputForm() {
  // ID 정보 state
  const [id, setId] = useRecoilState(authIdState);
  // 입력 상태 state
  const [isTrue, setIsTrue] = useRecoilState(authIdErrorState);
  const [helperText, setHelperText] = useRecoilState(authIdHelperTextState);
  const [cancelIcon, setCancelIcon] = useState(false);
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState);
  const setBottomBtnText = useSetRecoilState(authBottomButtonTextState)
  // ID 값 변경 제어
  const handleChangeValue = (e) => {
    const value = e.target.value.toLowerCase().substring(0, 12);
    checkIdValidation(value);

    setId(value);
  }
  // ID 정규식 체크
  const checkIdValidation = (value) => {
    // 영문 소문자, 숫자, 6~12 자리 정규식
    const regex = /^[a-z0-9]{6,12}$/g;
    if (regex.test(value)) {
      setIsTrue(true);
      setIsBottomBtnDisabled(false);
    } else {
      setIsTrue(false);
      setHelperText(RESET_PASSWORD.HELPER_TEXT);
      setIsBottomBtnDisabled(true);
    }
  }
  // 입력 값 취소 버튼 제어
  const handleFocus = () => setCancelIcon(true);
  const handleBlur = () => setCancelIcon(false);
  const handleCancel = () => setId('');

  useEffect(() => {
    setBottomBtnText('확인')
  }, [])

  useEffect(() => {
    checkIdValidation(id);
  }, [id]);

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          color: '#000000',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.RESET_PASSWORD}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={id}
        label={id === '' ? RESET_PASSWORD.LABEL : ' '}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        helperText={id === '' ? helperText : isTrue ? ' ' : helperText}
        InputProps={{
          endAdornment: id && cancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
            </InputAdornment>
          ),
          style: {
            paddingBottom: '11px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#222222',
          },
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#CCCCCC',
          }
        }}
        FormHelperTextProps={{
          style: {
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            letterSpacing: '-0.03em',
            color: id === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          marginTop: '14px',
          '& .MuiInput-root:before': {
            borderBottom: id === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: id === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: id === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: id === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: id === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  );
}