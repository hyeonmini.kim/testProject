import {Box, InputAdornment, Typography} from "@mui/material";
import * as React from "react";
import {useEffect, useState} from "react";
import {useRecoilState, useSetRecoilState} from "recoil";
import {
  authBottomButtonState,
  authConfirmPasswordState,
  authPasswordErrorState,
  authPasswordHelperTextState,
  authPasswordState
} from "../../recoil/atom/sign-up/auth";
import {SIGN_UP_PASSWORD, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {isValidateParam, passwordSchema} from "../../utils/validateUtils";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {ALT_STRING} from "../../constant/common/AltString";
import ChromeHiddenIdPw from "../../components/common/ChromeHiddenIdPw";
import TextFieldWrapper from "../../components/common/TextFieldWrapper";

export default function PasswordInputForm() {
  // Password 정보 state
  const [password, setPassword] = useRecoilState(authPasswordState);
  const [confirmPw, setConfirmPw] = useRecoilState(authConfirmPasswordState);
  // 입력 상태 state
  const [isTrue, setIsTrue] = useRecoilState(authPasswordErrorState);
  const [isConfirmTrue, setIsConfirmTrue] = useState(true);
  const [helperText, setHelperText] = useRecoilState(authPasswordHelperTextState);
  const [pwCancelIcon, setPwCancelIcon] = useState(false);
  const [confirmPwCancelIcon, setConfirmPwCancelIcon] = useState(false);
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState);
  // Password 값 변경 제어
  const handleChangePw = (e) => {
    const value = e.target.value.substring(0, 15);
    checkPasswordValidation(value);
    setPassword(value);
  }
  // Password 확인 값 변경 제어
  const handleChangeConfirmPw = (e) => {
    const value = e.target.value.substring(0, 15);
    checkConfirmPasswordValidation(value);
    setConfirmPw(value);
  }
  // Password 정규식 체크
  const checkPasswordValidation = (value) => {
    isValidateParam(value, passwordSchema).then((isVal) => {
      if (isVal) {
        setIsTrue(true);
        setHelperText('');
        setIsBottomBtnDisabled(false);
        checkConfirmPasswordValidation(confirmPw);
      } else {
        setIsTrue(false);
        setHelperText(SIGN_UP_PASSWORD.HELPER_TEXT);
        setIsBottomBtnDisabled(true);
      }
    })
  }
  const checkConfirmPasswordValidation = (value) => {
    // 앞에서 입력한 비밀번호와 일치하는지 확인
    if (value !== '') {
      isValidateParam(value, passwordSchema).then((isVal) => {
        if (isVal) {
          if (password === value) {
            setIsConfirmTrue(true);
            setIsBottomBtnDisabled(false);
          } else {
            setIsConfirmTrue(false);
            setHelperText(SIGN_UP_PASSWORD.WRONG);
            setIsBottomBtnDisabled(true);
          }
        } else {
          setIsConfirmTrue(false);
          setHelperText(SIGN_UP_PASSWORD.HELPER_TEXT);
          setIsBottomBtnDisabled(true);
        }
      })
    } else {
      setIsConfirmTrue(false);
      setIsBottomBtnDisabled(true);
    }
  }
  // Password 입력 값 취소 버튼 제어
  const handleFocusPw = () => setPwCancelIcon(true);
  const handleBlurPw = () => setPwCancelIcon(false);
  const handleCancelPw = () => setPassword('');
  // Password 확인 입력 값 취소 버튼 제어
  const handleFocusConfirmPw = () => setConfirmPwCancelIcon(true);
  const handleBlurConfirmPw = () => setConfirmPwCancelIcon(false);
  const handleCancelConfirmPw = () => setConfirmPw('');

  useEffect(() => {
    checkPasswordValidation(password);
    if (isTrue && password !== '' && confirmPw !== '') {
      checkConfirmPasswordValidation(confirmPw);
    }
  }, [password, confirmPw]);

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_PW)
  }, [])

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          color: '#000000',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.PASSWORD}
      </Typography>
      <ChromeHiddenIdPw/>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={password}
        label={password === '' ? SIGN_UP_PASSWORD.LABEL_INPUT : ' '}
        type="password"
        onChange={handleChangePw}
        onFocus={handleFocusPw}
        onBlur={handleBlurPw}
        variant="standard"
        inputProps={{tabIndex: 0}}
        InputProps={{
          endAdornment: password && pwCancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancelPw}/>
            </InputAdornment>
          ),
          style: {
            paddingBottom: '11px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#222222',
          },
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#CCCCCC',
          }
        }}
        FormHelperTextProps={{
          style: {
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            letterSpacing: '-0.03em',
            color: password === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          marginTop: '14px',
          '& .MuiInput-root:before': {
            borderBottom: password === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: password === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: password === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: password === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: password === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={confirmPw}
        type="password"
        label={confirmPw === '' ? SIGN_UP_PASSWORD.LABEL_CONFIRM : ' '}
        onChange={handleChangeConfirmPw}
        onFocus={handleFocusConfirmPw}
        onBlur={handleBlurConfirmPw}
        variant="standard"
        inputProps={{tabIndex: 0}}
        InputProps={{
          endAdornment: confirmPw && confirmPwCancelIcon && (
            <InputAdornment position={'end'}>
              <SvgCommonIcons
                alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT}
                type={ICON_TYPE.DELETE_TEXT}
                onMouseDown={handleCancelConfirmPw}
              />
            </InputAdornment>
          ),
          style: {
            paddingBottom: '11px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#222222',
          },
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#CCCCCC',
          }
        }}
        FormHelperTextProps={{
          style: {
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            letterSpacing: '-0.03em',
            color: password === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          marginTop: '4px',
          '& .MuiInput-root:before': {
            borderBottom: confirmPw === '' ? '1px solid #CCCCCC' : isConfirmTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: confirmPw === '' ? '1px solid #CCCCCC' : isConfirmTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: confirmPw === '' ? '1px solid #222222' : isConfirmTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: confirmPw === '' ? '1px solid #222222' : isConfirmTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: confirmPw === '' ? '1px solid #222222' : isConfirmTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
      <Typography
        sx={{
          mt: '8px',
          fontStyle: 'normal',
          fontWeight: 400,
          fontSize: '12px',
          lineHeight: '18px',
          letterSpacing: '-0.03em',
          color: (password === '' && confirmPw === '') ? '#888888' : isTrue ? isConfirmTrue ? '#888888' : '#FF4842' : '#FF4842',
        }}
      >
        {isTrue && isConfirmTrue ? '' : helperText}
      </Typography>
    </Box>
  );
}
