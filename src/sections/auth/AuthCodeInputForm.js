import {Box, Typography} from "@mui/material";
import Timer from "../../components/auth/Timer";
import {useEffect, useRef, useState} from "react";
import {styled} from "@mui/material/styles";
import {useRecoilState, useRecoilValue, useResetRecoilState, useSetRecoilState} from "recoil";
import {
  authBirthdayState,
  authBottomButtonState,
  authBottomButtonTextState,
  authEmailFindIdState,
  authFindIdAnswerState,
  authFindIdQuestionState,
  authGenderState,
  authIdEndNumberState,
  authIdFrontNumberState,
  authIdState,
  authNameState,
  authPhoneNumberState,
  authSignUpIdExistedSnsState,
  authTelecomState
} from "../../recoil/atom/sign-up/auth";
import {commonBottomDialogState} from "../../recoil/atom/common/bottomDialog";
import {useRouter} from "next/router";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import {AUTH_CODE, CERT_TYPE, CONFIRM_TYPE, FROM_TYPE, SIGN_UP_DIALOG} from "../../constant/sign-up/SignUp";
import {toastMessageSelector} from "../../recoil/selector/common/toastSelector";
import {COMMON_DIALOG_TYPE, TOAST_TYPE} from "../../constant/common/Common";
import {dialogSelector, dialogShowDialogSelector} from "../../recoil/selector/common/dialogSelector";
import {authSelector} from "../../recoil/selector/sign-up/authSelector";
import {ICON_COLOR, SvgWatchRoundIcon} from "../../constant/icons/icons";
import {getCheckAuthMutate, postAddSnsAccountMutate, postAuthCodeMutate, postCheckIdAndAuthMutate} from "../../api/authApi";
import {
  changeBirthFromYYMMDDToYYYYMMDD,
  changeGenderType,
  changeNewsAgencyType,
  checkTransactionSeqNumberByEnv,
  removeHyphenFromPhoneNumber
} from "../../utils/authUtils";
import {authCiSelector} from "../../recoil/selector/sign-up/signUpSelector";
import {callOpenNativeSetSnsProviderChannel} from "../../channels/commonChannel";
import {useBefuAuthContext} from "../../auth/useAuthContext";
import {getItemSnsType} from "../../auth/utils";
import {accessTokenSelector, socialAccountIdSelector, transactionSeqNumberSelector} from "../../recoil/selector/auth/accessTokenSelector";
import {getEnv} from "../../utils/envUtils";
import useNativeBackKey from "../../hooks/useNativeBackKey";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {ERRORS, GET_ERROR_CODE, GET_ERROR_MESSAGE} from "../../constant/common/Error";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import TextFieldWrapper from "../../components/common/TextFieldWrapper";
import {ALT_STRING} from "../../constant/common/AltString";

const InputAreaStyle = styled('div')(() => ({
  overflowX: 'hidden',
  width: '100%',
  '& .MuiInput-root:before': {
    borderBottom: 'none',
  },
  '& .MuiInput-root:hover:before': {
    borderBottom: 'none',
  },
  '& .MuiInput-root:hover.Mui-disabled:before': {
    borderBottom: 'none',
  },
  '& .MuiInput-root:hover.Mui-disabled:after': {
    borderBottom: 'none',
  },
  '& .MuiInput-root.Mui-disabled:before': {
    borderBottom: 'none',
  },
  '& .MuiInput-root.Mui-disabled:after': {
    borderBottom: 'none',
  },
}))

export default function AuthCodeInputForm({setPage}) {
  // 부모 바텀 버튼 제어 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  const setBottomBtnText = useSetRecoilState(authBottomButtonTextState)
  // 입력 값 제어 state
  const inputRef = useRef([])
  const [current, setCurrent] = useState(0)
  const [inputNumbers, setInputNumbers] = useState('')
  const [isWarningTime, setIsWarningTime] = useState(false)
  const [errorCount, setErrorCount] = useState(0)
  const [fullAuthCode, setFullAuthCode] = useState([])
  // 입력 칸 스타일 제어 state
  const [numColor, setNumColor] = useState('#222222')
  const [inputBorderBottom, setInputBorderBottom] = useState('1px solid #CCCCCC')
  // 바텀 다이얼로그 제어 state
  const setIsOpen = useSetRecoilState(commonBottomDialogState)
  // 시간 제어 state
  const timeByMs = 1000 * 60 * 3
  const [time, setTimeByMs] = useState(timeByMs)

  const setSignUpIdExistedSns = useSetRecoilState(authSignUpIdExistedSnsState)

  const [ci, setCi] = useRecoilState(authCiSelector)
  const [id, setId] = useRecoilState(authIdState)
  const setBirthday = useSetRecoilState(authBirthdayState)
  const setGender = useSetRecoilState(authGenderState)

  const name = useRecoilValue(authNameState)
  const telecom = useRecoilValue(authTelecomState)
  const [phone, setPhone] = useRecoilState(authPhoneNumberState)
  const idFront = useRecoilValue(authIdFrontNumberState)
  const idEnd = useRecoilValue(authIdEndNumberState)

  const resetAuthInform = useResetRecoilState(authSelector)
  const resetCi = useResetRecoilState(authCiSelector)
  const resetId = useResetRecoilState(authIdState)
  const [accessToken, setAccessToken] = useRecoilState(accessTokenSelector)
  const [socialAccountId, setSocialAccountId] = useRecoilState(socialAccountIdSelector)
  const [transactionSeqNumber, setTransactionSeqNumber] = useRecoilState(transactionSeqNumberSelector)
  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)

  const setToastMessage = useSetRecoilState(toastMessageSelector)
  const setDialogMessage = useSetRecoilState(dialogSelector)
  const router = useRouter()

  const [tmpCi, setTmpCi] = useState('')
  const [isSignUpSnsExistedId, setIsSignUpSnsExistedId] = useState(false)
  const [isSignUpSnsExistedAnotherSns, setIsSignUpSnsExistedAnotherSns] = useState(false)

  const resetEmailFindId = useResetRecoilState(authEmailFindIdState)
  const resetFindIdQuestion = useResetRecoilState(authFindIdQuestionState)
  const resetFindIdAnswer = useResetRecoilState(authFindIdAnswerState)

  const {mutate: mutateGetCheckAuth} = getCheckAuthMutate() // 인증번호 확인하고 계정 정보 체크
  const {mutate: mutatePostCheckIdAndAuth} = postCheckIdAndAuthMutate() // 인증번호 확인하고 ID로 가입 여부 체크
  const {mutate: mutatePostAddSnsAccount} = postAddSnsAccountMutate() // SNS 연동 계정 추가
  const {mutate: mutatePostAuthCode} = postAuthCodeMutate()

  const {getUser} = useBefuAuthContext()

  const env = getEnv()

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      if (showDialog) {
        setShowDialog(false)
      } else {
        router.back()
      }
    }
  }, [handleBackKey])

  // SNS 회원가입의 경우
  // ID 기가입이거나 다른 SNS 기가입의 경우
  // 처리하는 부분
  useEffect(() => {
    if (isSignUpSnsExistedId || isSignUpSnsExistedAnotherSns) {
      mutatePostAddSnsAccount({
        ci: tmpCi
      }, {
        onSuccess: (ret) => {
          setIsOpen(false)
          // 처리 이후 초기화
          setTmpCi('')
          setIsSignUpSnsExistedId(false)
          setIsSignUpSnsExistedAnotherSns(false)
          // 유저정보 가져오기
          getUser(accessToken)
          // 홈으로 이동
          router.replace({pathname: PATH_DONOTS_PAGE.HOME})
        }
      })
    }
  }, [isSignUpSnsExistedId, isSignUpSnsExistedAnotherSns])

  // 인증번호 불일치
  const mismatchAuthCode = () => {
    setErrorCount(errorCount + 1)
    // 입력칸 색상 설정
    setNumColor('#FF4842')
    setInputBorderBottom('1px solid #FF4842')
    // 스낵바 설정
    if ((errorCount + 1) < 5) {
      setToastMessage({
        type: TOAST_TYPE.BOTTOM_DIALOG_HEADER_ERROR,
        message: AUTH_CODE.TOAST_INCORRECT,
      })
    } else {
      setToastMessage({
        type: TOAST_TYPE.BOTTOM_DIALOG_HEADER_ERROR,
        message: AUTH_CODE.TOAST_FIVE_TIMES,
      })

      setTimeout(function () {
        setIsOpen(false)
      }, 500)
    }
  }

  // 입력칸 클릭 이벤트 제어
  const onClickNumber = () => {
    inputRef.current[0].focus()
    // 입력 값 초기화
    Array(6).fill().map((i, index) => {
      inputRef.current[index].value = ''
    })
    setCurrent(0)
    setInputNumbers('')
    // 입력 칸 스타일 초기화
    setNumColor('#222222')
    setInputBorderBottom('1px solid #CCCCCC')
  }

  // 생년월일 변환 입력
  const parseBirthday = () => {
    const tempIdFront = idFront?.substring(0, 2) + "-" + idFront?.substring(2, 4) + "-" + idFront?.substring(4, 6)
    switch (idEnd) {
      case '1':
      case '2':
        setBirthday('19' + tempIdFront)
        return '19' + tempIdFront
      case '3':
      case '4':
        setBirthday('20' + tempIdFront)
        return '20' + tempIdFront
      default:
        return '99' + tempIdFront
    }
  }

  // 휴대폰번호 변환 입력
  const parsePhoneNumber = () => {
    setPhone(phone.replaceAll('-', ''))
  }

  const handleInputAllNumber = async (numbers) => {
    const fromType = router.query.from

    if (time !== 0) {
      switch (fromType) {
        case FROM_TYPE.SIGN_UP_ID:
        case FROM_TYPE.SIGN_UP_SNS:
          /* API : 본인인증 인증번호 검사 및 가입 여부 체크 */
          let params = {}
          if (fromType === FROM_TYPE.SIGN_UP_ID) {
            params = {
              joiningAccountType: CERT_TYPE.OWN_ACCOUNT,
              param: {
                phoneNumber: phone.replaceAll('-', ''),
                transactionSeqNumber: transactionSeqNumber,
                authCodeOf6Digits: numbers,
              }
            }
          } else {
            params = {
              joiningAccountType: CERT_TYPE.SOCIAL_ACCOUNT,
              param: {
                phoneNumber: phone.replaceAll('-', ''),
                transactionSeqNumber: transactionSeqNumber,
                authCodeOf6Digits: numbers,
                socialAccountId: socialAccountId,
              }
            }
          }

          await mutateGetCheckAuth(params, {
            onSuccess: async (result) => {
              if (result) {
                // 인증번호 일치
                // 분류 : 14세 미만 / 기존 회원 / 신규 회원
                const curAge = new Date().getFullYear() - parseInt(parseBirthday().substring(0, 4)) + 1
                if (curAge < 14) {
                  setIsOpen(false)
                  // 14세 미만 가입 불가
                  router.replace({
                    pathname: PATH_DONOTS_PAGE.AUTH.CONFIRM,
                    query: {
                      from: CONFIRM_TYPE.REJECT,
                    }
                  }).then(() => resetAuthInform())
                } else {
                  if (result?.isExist) {
                    // 기존 회원
                    if (fromType === FROM_TYPE.SIGN_UP_ID) {
                      // ID 회원가입의 경우
                      if (result?.data?.id !== null) {
                        setIsOpen(false)
                        // ID 기가입 / ID and SNS 기가입 -> 회원 정보 찾음 화면 -> 메인으로
                        router.replace({
                          pathname: PATH_DONOTS_PAGE.AUTH.CONFIRM,
                          query: {
                            from: CONFIRM_TYPE.CORRECT,
                            id: result?.data?.id,
                            createdAt: result?.data?.createdAt?.substring(2, 10).replaceAll('-', '.'),
                          }
                        }).then(() => resetAuthInform())
                      } else if (result?.data?.snsInfoDtoList?.length > 0) {
                        setIsOpen(false)
                        // SNS 기가입 -> 아이디, 패스워드 입력만 받기 -> 회원가입 완료 화면 -> 홈으로
                        // setCookie(COMMON_STR.CI, result?.ci, 1)
                        setCi(result?.ci)
                        setSignUpIdExistedSns(true)
                        setPage(1)
                      }
                    } else if (fromType === FROM_TYPE.SIGN_UP_SNS) {
                      // SNS 회원가입의 경우
                      if (result?.data?.id !== null) {
                        // ID 기가입 -> SNS 계정 추가 -> 홈으로
                        setTmpCi(result?.ci)
                        setIsSignUpSnsExistedId(true)
                      } else if (result?.data?.snsInfoDtoList?.length > 0) {
                        const providerInform = result?.data?.snsInfoDtoList.filter(item => item.provider === getItemSnsType().toUpperCase())
                        if (providerInform.length > 0) {
                          setIsOpen(false)
                          // 같은 SNS 기가입 -> 회원 정보 찾음 화면 -> 홈으로
                          getUser(accessToken)
                          router.replace({
                            pathname: PATH_DONOTS_PAGE.AUTH.CONFIRM,
                            query: {
                              from: CONFIRM_TYPE.CORRECT,
                              id: providerInform[0].email,
                              createdAt: result?.data?.createdAt?.substring(2, 10).replaceAll('-', '.'),
                            }
                          }).then(() => resetAuthInform())
                        } else {
                          // 다른 SNS 기가입 -> SNS 계정 추가 -> 홈으로
                          callOpenNativeSetSnsProviderChannel(getItemSnsType())
                          setTmpCi(result?.ci)
                          setIsSignUpSnsExistedAnotherSns(true)
                        }
                      }
                    }
                  } else {
                    setIsOpen(false)

                    // 신규 회원
                    // setCookie(COMMON_STR.CI, result?.ci, 1)
                    setCi(result?.ci)
                    parseBirthday()
                    parsePhoneNumber()
                    setGender(changeGenderType(idEnd))
                    setPage(1)
                    setIsBottomBtnDisabled(true)
                    setBottomBtnText('확인')

                    setHackleTrack(HACKLE_TRACK.SIGNUP_CERTIFICATION_CODE)
                  }
                }
              } else {
                // 인증번호 불일치
                mismatchAuthCode()
              }
            },
            onError: async (error) => {
              switch (GET_ERROR_CODE(error)) {
                case ERRORS.AUTH_CODE_NOT_MATCHED:
                  setToastMessage({
                    type: TOAST_TYPE.BOTTOM_DIALOG_HEADER_ERROR,
                    message: GET_ERROR_MESSAGE(error),
                  })
                  break
                case ERRORS.SERVICE_UNAVAILABLE:
                case ERRORS.INCORRECT_AUTH_CODE_ATTEMPTS_EXCEEDED:
                case ERRORS.AUTH_CODE_RETRANSMISSION_REQUEST_TIMEOUT:
                case ERRORS.MISCELLANEOUS:
                  setTimeout(function () {
                    setIsOpen(false)

                    setToastMessage({
                      type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
                      message: GET_ERROR_MESSAGE(error),
                    })
                  }, 300)
                  break
              }
            }
          })
          break
        case FROM_TYPE.FIND_ID:
          /* API : 본인인증 인증번호 검사 및 가입 여부 체크 */
          mutateGetCheckAuth({
            joiningAccountType: CERT_TYPE.OWN_ACCOUNT,
            param: {
              phoneNumber: phone.replaceAll('-', ''),
              transactionSeqNumber: transactionSeqNumber,
              authCodeOf6Digits: numbers,
            }
          }, {
            onSuccess: (result) => {
              if (result) {
                // 인증번호 일치
                setIsOpen(false)

                // 분류 : 기존 회원 / 회원 아님
                if (result?.isExist) {
                  resetEmailFindId()
                  resetFindIdQuestion()
                  resetFindIdAnswer()
                  // 기존 회원
                  router.replace({
                    pathname: PATH_DONOTS_PAGE.AUTH.CONFIRM,
                    query: {
                      from: CONFIRM_TYPE.CORRECT,
                      id: result?.data?.id,
                      createdAt: result?.data?.createdAt?.substring(2, 10).replaceAll('-', '.'),
                    }
                  }).then(() => resetAuthInform())
                } else {
                  resetEmailFindId()
                  resetFindIdQuestion()
                  resetFindIdAnswer()
                  // 회원 아님
                  router.replace({
                    pathname: PATH_DONOTS_PAGE.AUTH.CONFIRM,
                    query: {
                      from: CONFIRM_TYPE.INCORRECT,
                    }
                  }).then(() => resetAuthInform())
                }
              } else {
                // 인증번호 불일치
                mismatchAuthCode()
              }
            },
            onError: async (error) => {
              switch (GET_ERROR_CODE(error)) {
                case ERRORS.AUTH_CODE_NOT_MATCHED:
                  setToastMessage({
                    type: TOAST_TYPE.BOTTOM_DIALOG_HEADER_ERROR,
                    message: GET_ERROR_MESSAGE(error),
                  })
                  break
                case ERRORS.SERVICE_UNAVAILABLE:
                case ERRORS.INCORRECT_AUTH_CODE_ATTEMPTS_EXCEEDED:
                case ERRORS.AUTH_CODE_RETRANSMISSION_REQUEST_TIMEOUT:
                case ERRORS.MISCELLANEOUS:
                  setTimeout(function () {
                    setIsOpen(false)

                    setToastMessage({
                      type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
                      message: GET_ERROR_MESSAGE(error),
                    })
                  }, 300)
                  break
              }
            }
          })
          break
        case FROM_TYPE.RESET_PASSWORD:
          /* API : 본인인증 인증번호 검사 및 ID로 가입 여부 체크 */
          mutatePostCheckIdAndAuth({
            joiningAccountType: CERT_TYPE.OWN_ACCOUNT,
            param: {
              phoneNumber: phone.replaceAll('-', ''),
              transactionSeqNumber: transactionSeqNumber,
              authCodeOf6Digits: numbers,
            },
            id: id,
          }, {
            onSuccess: (result) => {
              if (result) {
                // 인증번호 일치
                setIsOpen(false)

                // 분류 : 정보 일치 / 정보 불일치
                if (result.isMatched) {
                  // 아이디에 맞는 인증 정보
                  // setCookie(COMMON_STR.CI, result?.ci, 1)
                  setCi(result?.ci)
                  setPage(2)
                  setIsBottomBtnDisabled(true)
                  setBottomBtnText('확인')
                } else {
                  // 아이디에 맞지 않는 인증 정보
                  setDialogMessage({
                    type: COMMON_DIALOG_TYPE.ONE_BUTTON,
                    message: SIGN_UP_DIALOG.INCORRECT_INFORM,
                    handleButton1: {text: SIGN_UP_DIALOG.CONFIRM},
                  })
                }
              } else {
                // 인증번호 불일치
                mismatchAuthCode()
              }
            },
            onError: async (error) => {
              switch (GET_ERROR_CODE(error)) {
                case ERRORS.AUTH_CODE_NOT_MATCHED:
                  setToastMessage({
                    type: TOAST_TYPE.BOTTOM_DIALOG_HEADER_ERROR,
                    message: GET_ERROR_MESSAGE(error),
                  })
                  break
                case ERRORS.SERVICE_UNAVAILABLE:
                case ERRORS.INCORRECT_AUTH_CODE_ATTEMPTS_EXCEEDED:
                case ERRORS.AUTH_CODE_RETRANSMISSION_REQUEST_TIMEOUT:
                case ERRORS.MISCELLANEOUS:
                  setTimeout(function () {
                    setIsOpen(false)

                    setToastMessage({
                      type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
                      message: GET_ERROR_MESSAGE(error),
                    })
                  }, 300)
                  break
              }
            }
          })
          break
      }
    } else {
      // 입력칸 색상 설정
      setNumColor('#FF4842')
      setInputBorderBottom('1px solid #FF4842')
      // 스낵바 설정
      setToastMessage({
        type: TOAST_TYPE.BOTTOM_DIALOG_HEADER_ERROR,
        message: AUTH_CODE.TOAST_TIMEOUT,
      })
    }
  }

  useEffect(() => {
    if (fullAuthCode.length !== 0) {
      let index = 0

      setTimeout(() => {
        fullAuthCode.map((letter) => {
          inputRef.current[index].value = letter
          index = index + 1
        })
      }, 300)

      // 인증번호 6자리 모두 입력했을 때 처리
      setTimeout(async () => {
        // 인증번호 6자리 모두 입력했을 때 처리
        await handleInputAllNumber(fullAuthCode.join(''))
      }, 200)

      setFullAuthCode([])
    }
  }, [fullAuthCode])

  // 입력칸 체인지 이벤트 제어
  const onChangeNumber = async (e) => {
    if (!isNaN(e.target.value) && e.target.value.length === 6) {
      setFullAuthCode(e.target.value.split(''))
      inputRef.current[current].value = ''
      inputRef.current[current].blur()
    } else if (!isNaN(e.target.value) && e.target.value.length === 1) {
      let numbers = inputNumbers + e.target.value
      if (e.target.value !== '') {
        setInputNumbers(numbers)
        if (current === 5) {
          inputRef.current[current].blur()

          setTimeout(async () => {
            // 인증번호 6자리 모두 입력했을 때 처리
            await handleInputAllNumber(numbers)
          }, 500)
        } else {
          inputRef.current[current + 1].focus()
          setCurrent(current + 1)
        }
      }
    } else {
      onClickNumber()
    }
  }
  // 키보드 키 제어
  const handleKeyDown = (e) => {
    // 백스페이스 키 처리
    if (e.key === 'Backspace' && current !== 0) {
      inputRef.current[current].blur()
      inputRef.current[current - 1].focus()
      setCurrent(current - 1)
      setInputNumbers(inputNumbers.slice(0, -1))
    }
  }

  const handleKeyUp = (e) => {
    if (e.key === 'Unidentified')
      inputRef.current[current].value = ''
    // if (e.keyCode === 229)
    //   inputRef.current[current].value = ''
  }

  const onReset = () => {
    /* API : 본인인증 입력 정보 검사 및 인증번호 받기 */
    const newsAgencyType = changeNewsAgencyType(telecom)
    const gender = changeGenderType(idEnd)
    const birth = changeBirthFromYYMMDDToYYYYMMDD(idFront, idEnd)
    const phoneNumber = removeHyphenFromPhoneNumber(phone)
    mutatePostAuthCode({
      name: name,
      phoneNumber: phoneNumber,
      birthday: birth,
      gender: gender,
      koreaForeignerType: '1',
      newsAgencyType: newsAgencyType,
    }, {
      onSuccess: (result) => {
        setTransactionSeqNumber(result?.transactionSeqNumber)
        /* Further Development */
        if (checkTransactionSeqNumberByEnv(result?.transactionSeqNumber, env)) {
          // 본인인증 정보 성공
          // 입력시간 초기화
          setTimeByMs(timeByMs)
          setIsWarningTime(false)
          // 스낵바 설정
          setToastMessage({
            type: TOAST_TYPE.BOTTOM_DIALOG_HEADER,
            message: AUTH_CODE.TOAST_RETRY,
          })
          // 입력값 초기화
          onClickNumber()
          setErrorCount(0)
        } else {
          // 본인인증 정보 에러
          setToastMessage({
            type: TOAST_TYPE.BOTTOM_HEADER,
            message: '입력하신 정보가 올바르지 않아요.\n다시 한번 확인 후 입력해 주세요.',
          })
        }
      },
      onError: async (error) => {
        switch (GET_ERROR_CODE(error)) {
          case ERRORS.INVALID_NAME_OR_RESIDENT_REGISTRATION_NUMBER:
          case ERRORS.SERVICE_UNAVAILABLE:
          case ERRORS.MISCELLANEOUS:
            setToastMessage({
              type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
              message: GET_ERROR_MESSAGE(error),
            })
            break
        }
      }
    })
  }

  useEffect(() => {
    if (time <= 1000 * 30) {
      setIsWarningTime(true)
    }
  }, [time])

  return (
    <Box textAlign="-webkit-center">
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 500,
          fontSize: '16px',
          lineHeight: '24px',
          letterSpacing: '-0.02em',
          color: '#000000',
          mb: '13px',
        }}
      >
        {AUTH_CODE.TITLE}
      </Typography>
      <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
        {
          isWarningTime ?
            <SvgWatchRoundIcon color={ICON_COLOR.RED} sx={{mt: '2px', mr: '2px', mb: '1px'}}/>
            :
            <SvgWatchRoundIcon color={ICON_COLOR.GREY} sx={{mt: '2px', mr: '2px', mb: '1px'}}/>
        }
        <Typography
          sx={{
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            letterSpacing: '-0.02em',
            color: isWarningTime ? '#FF4842' : '#888888',
          }}
        >
          <Timer time={time} setTimeByMs={setTimeByMs}/>
        </Typography>
      </Box>
      <Box sx={{display: 'flex', justifyContent: 'center', m: '18px 0 37px 0'}}>
        <InputAreaStyle>
          {Array(6).fill().map((i, index) => {
            return (
              <TextFieldWrapper
                autoComplete='off'
                key={index}
                variant="standard"
                onClick={onClickNumber}
                onChange={onChangeNumber}
                onKeyDown={handleKeyDown}
                onKeyUp={handleKeyUp}
                inputRef={(ref) => (inputRef.current[index] = ref)}
                sx={{
                  height: '40px',
                  width: '24px',
                  mx: '5px',
                  '& input': {
                    p: 0,
                    textAlign: 'center',
                    fontStyle: 'normal',
                    fontWeight: 700,
                    fontSize: '24px',
                    lineHeight: '32px',
                    letterSpacing: '-0.02em',
                    color: numColor,
                    caretColor: 'transparent',
                    borderBottom: inputBorderBottom,
                  },
                }}
                InputProps={{inputProps: {tabIndex: index === 0 ? 0 : -1, inputMode: 'numeric', title: ALT_STRING.SIGN_UP.AUTH_CODE(index)}}}
              />
            );
          })}
        </InputAreaStyle>
      </Box>
      <Typography
        tabIndex={0}
        sx={{
          fontStyle: 'normal',
          fontWeight: 400,
          fontSize: '12px',
          lineHeight: '18px',
          letterSpacing: '-0.03em',
          color: '#222222',
          textDecoration: 'underline',
          mb: '18px',
          width: 'fit-content',
        }}
        onClick={onReset}
        onKeyDown={(e) => handleEnterPress(e, onReset)}
      >
        {AUTH_CODE.RETRY}
      </Typography>
    </Box>
  );
}