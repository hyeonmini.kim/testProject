import {Box, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import {authBottomButtonState, authEmailErrorState, authEmailHelperTextState, authEmailState} from "../../recoil/atom/sign-up/auth";
import {useEffect} from "react";
import {SIGN_UP_EMAIL, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import DonotsTextField from "../../components/common/DonotsTextField";
import {emailSchema, isValidateParam} from "../../utils/validateUtils";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";

export default function EmailInputForm() {
  // Email 정보 state
  const [email, setEmail] = useRecoilState(authEmailState)
  // 입력 상태 state
  const [isTrue, setIsTrue] = useRecoilState(authEmailErrorState)
  const [helperText, setHelperText] = useRecoilState(authEmailHelperTextState)
  // 바텀 버튼 state
  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  // Email 값 변경 제어
  const handleChangeValue = (e) => setEmail(e.target.value.toLowerCase())
  // Email 정규식 체크
  const checkEmailValidation = () => {
    isValidateParam(email, emailSchema).then((isVal) => {
      if (email) {
        if (isVal) {
          setIsTrue(true)
          setHelperText('')
          setIsBottomBtnDisabled(email === '')
        } else {
          setIsTrue(false)
          setHelperText(SIGN_UP_EMAIL.HELPER_TEXT_WRONG)
          setIsBottomBtnDisabled(true)
        }
      } else {
        setIsTrue(true)
        setHelperText(SIGN_UP_EMAIL.HELPER_TEXT)
        setIsBottomBtnDisabled(true)
      }
    })
  }

  useEffect(() => {
    checkEmailValidation()
  }, [email])

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_EMAIL)
  }, [])

  return (
    <>

      <Box sx={{position: 'relative'}}>

        <Typography variant={'h1_24_b'} sx={{color: '#000000'}}>
          {SIGN_UP_TITLE.EMAIL}
        </Typography>

        <DonotsTextField
          tabIndex={0}
          handleChange={handleChangeValue}
          helperText={helperText}
          label={SIGN_UP_EMAIL.LABEL}
          type={'text'}
          value={email}
          setValue={setEmail}
          isValidate={isTrue}
        />

      </Box>

    </>
  );
}