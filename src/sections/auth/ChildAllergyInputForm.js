import {Box, Grid, Typography} from "@mui/material";
import {useEffect} from "react";
import {useRecoilState, useSetRecoilState} from "recoil";
import {authBottomButtonMarginState, authBottomButtonPositionState, authBottomButtonState} from "../../recoil/atom/sign-up/auth";
import {SIGN_UP_SUBTITLE, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import {SvgAllergyIcons} from "../../constant/icons/AllergyIcons";
import {authAllergySelector, authChildAllergySelector} from "../../recoil/selector/sign-up/signUpSelector";
import {inputChildrenAllergySelector} from "../../recoil/selector/home/inputChildrenInfoSelector";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {handleEnterPress} from "../../utils/onKeyDownUtils";

export default function ChildAllergyInputForm({isGuest = false}) {
  const [allergy, setAllergy] = useRecoilState(authAllergySelector)
  const [childAllergy, setChildAllergy] = useRecoilState(authChildAllergySelector)
  const setInputChildAllergy = useSetRecoilState(inputChildrenAllergySelector)

  const setIsBottomBtnDisabled = useSetRecoilState(authBottomButtonState)
  const setButtonPosition = useSetRecoilState(authBottomButtonPositionState)
  const setButtonMargin = useSetRecoilState(authBottomButtonMarginState)

  const handleClick = (id) => {
    let newList = allergy

    if (id === '없음') {
      let index = 0
      while (index < allergy.length) {
        newList = replaceItemAtIndex(newList, index, {
          alg_name: allergy[index].alg_name,
          value: false,
          image: allergy[index].image,
          alg_key: allergy[index].alg_key,
        })
        index++
      }
      newList = replaceItemAtIndex(newList, 0, {
        alg_name: allergy[0].alg_name,
        value: true,
        image: allergy[0].image,
        alg_key: allergy[0].alg_key,
      })
      // 아이 알러지 초기화
      setChildAllergy([])
      if (isGuest) setInputChildAllergy([])
    } else {
      newList = replaceItemAtIndex(allergy, 0, {
        alg_name: allergy[0].alg_name,
        value: false,
        image: allergy[0].image,
        alg_key: allergy[0].alg_key,
      })
      const index = allergy.findIndex((listItem) => listItem.alg_name === id)
      newList = replaceItemAtIndex(newList, index, {
        alg_name: id,
        value: !newList[index].value,
        image: newList[index].image,
        alg_key: newList[index].alg_key,
      })
      // 아이 알러지 추가/삭제
      const idx = childAllergy.findIndex((listItem) => listItem.name === id)
      if (idx >= 0) {
        // 있으면 제거
        setChildAllergy(childAllergy.filter(alg => alg.name !== id))
        if (isGuest) setInputChildAllergy(childAllergy.filter(alg => alg.name !== id))
      } else {
        // 없으므로 추가
        const newAlg = {
          name: id,
          key: newList[index].alg_key
        }
        setChildAllergy([...childAllergy, newAlg])
        if (isGuest) setInputChildAllergy([...childAllergy, newAlg])
      }
    }

    setAllergy(newList)
  }

  function replaceItemAtIndex(arr, index, newValue) {
    return [...arr.slice(0, index), newValue, ...arr.slice(index + 1)];
  }

  useEffect(() => {
    setChildAllergy([])
    if (isGuest) setInputChildAllergy([])
  }, [])

  useEffect(() => {
    setButtonPosition('initial')
    setButtonMargin('0px')

    return () => {
      setButtonPosition('fixed')
      setButtonMargin('20px')
    }
  }, [])

  useEffect(() => {
    const index = allergy.findIndex((listItem) => listItem.value === true)
    setIsBottomBtnDisabled(index < 0)
  }, [allergy])

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_KIDS_ALERGY)
  }, [])

  return (
    <Box sx={{marginBottom: '40px'}}>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          color: '#000000',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.CHILD_ALLERGY}
      </Typography>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 400,
          fontSize: '16px',
          lineHeight: '24px',
          letterSpacing: '-0.05em',
          color: '#666666',
          whiteSpace: 'pre-wrap',
          marginTop: '10px',
        }}
      >
        {SIGN_UP_SUBTITLE.CHILD_ALLERGY}
      </Typography>
      <Box sx={{flexGrow: 1}}>
        <Grid
          container
          columnSpacing="10px"
          rowSpacing="8px"
          sx={{marginTop: '22px', textAlign: 'center'}}
        >
          {allergy.map((obj, idx) => (
            <Grid item key={idx} xs={4} sx={{height: '150px', '& .MuiGrid-root.MuiGrid-item': {padding: 0}}}>
              <Box
                tabIndex={0}
                sx={{
                  height: '100%',
                  border: obj.value ? '1.4px solid #ECA548' : '1px solid #E6E6E6',
                  borderRadius: '8px',
                  backgroundColor: '#FFFFFF',
                  display: 'flex',
                  flexDirection: 'column',
                }}
                onClick={() => handleClick(obj.alg_name)}
                onKeyDown={(e) => handleEnterPress(e, () => handleClick(obj.alg_name))}
              >
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 'auto',
                    paddingBottom: obj.image?.PADDING_BOTTOM,
                  }}
                >
                  <SvgAllergyIcons type={obj.image?.TITLE} isSelected={obj.value}/>
                </Box>
                <Typography
                  sx={{
                    fontWeight: obj.value ? 700 : 400,
                    fontSize: '16px',
                    lineHeight: '22px',
                    letterSpacing: '-0.03em',
                    color: obj.value ? '#ECA548' : '#666666',
                    paddingBottom: '20px',
                  }}
                >
                  {obj.alg_name}
                </Typography>
              </Box>
            </Grid>
          ))}
        </Grid>
      </Box>
    </Box>
  );
}