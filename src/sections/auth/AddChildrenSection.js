import {ADD_CHILDREN_STEP} from "../../constant/my/AddChildren";
import {useEffect, useState} from "react";
import {sxFixedCenterMainLayout} from "../../layouts/main/MainLayout";
import {Button, Container} from "@mui/material";
import {clickBorderNone, COMMON_TEXT} from "../../constant/common/Common";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import {CONFIRM_TYPE} from "../../constant/sign-up/SignUp";
import {getFloatFixed} from "../../utils/formatNumber";
import {useRouter} from "next/router";
import {useRecoilState, useResetRecoilState} from "recoil";
import {
  addChildrenSelector,
  authAllergySelector,
  authChildHeightSelector,
  authChildWeightSelector,
  authKeywordSelector
} from "../../recoil/selector/sign-up/signUpSelector";
import {postAddChildren} from "../../api/myChildrenApi";
import AddChildrenHeaderLayout from "../../layouts/my/add-children/AddChildrenHeaderLayout";
import ChildrenNicknameInputSection from "./ChildrenNicknameInputSection";
import ChildrenBirthInputSection from "./ChildrenBirthInputSection";
import ChildrenHeightInputSection from "./ChildrenHeightInputSection";
import ChildrenWeightInputSection from "./ChildrenWeightInputSection";
import ChildrenGenderInputSection from "./ChildrenGenderInputSection";
import ChildrenAllergyInputSection from "./ChildrenAllergyInputSection";
import ChildrenKeywordInputSection from "./ChildrenKeywordInputSection";
import useNativeBackKey from "../../hooks/useNativeBackKey";
import {debounce} from "lodash";

export default function AddChildrenSection() {

  const [addChildren, setAddChildren] = useRecoilState(addChildrenSelector)
  const [childHeight, setChildHeight] = useRecoilState(authChildHeightSelector)
  const [childWeight, setChildWeight] = useRecoilState(authChildWeightSelector)

  const resetAddChildrenValue = useResetRecoilState(addChildrenSelector)
  const resetSelectedAllergy = useResetRecoilState(authAllergySelector)
  const resetSelectedKeyword = useResetRecoilState(authKeywordSelector)

  const [step, setStep] = useState(ADD_CHILDREN_STEP.NICKNAME)
  const [isBottomButtonDisabled, setIsBottomButtonDisabled] = useState(true)
  const [isOnClickDisabled, setIsOnClickDisabled] = useState(false)

  const router = useRouter()

  const {mutate: mutateAddChildren} = postAddChildren()

  const handleBackButton = () => {
    switch (step) {
      case ADD_CHILDREN_STEP.NICKNAME:
        router.back()
        break
      default:
        setStep(step - 1)
        break
    }
  }

  const handleClickNextStep = debounce(() => {
    switch (step) {
      case ADD_CHILDREN_STEP.NICKNAME:
      case ADD_CHILDREN_STEP.BIRTH:
      case ADD_CHILDREN_STEP.GENDER:
      case ADD_CHILDREN_STEP.ALLERGY:
        setStep(step + 1)
        break
      case ADD_CHILDREN_STEP.HEIGHT:
        const parseHeight = getFloatFixed(childHeight, 1)
        setChildHeight(parseHeight)
        setStep(step + 1)
        break
      case ADD_CHILDREN_STEP.WEIGHT:
        const parseWeight = getFloatFixed(childWeight, 1)
        setChildWeight(parseWeight)
        setStep(step + 1)
        break
      case ADD_CHILDREN_STEP.KEYWORD:
        setIsOnClickDisabled(true)
        /* API : 아이 추가 */
        mutateAddChildren(addChildren, {
          onSuccess: (result) => {
            if (result !== undefined) {
              /* 아이 추가 완료 페이지로 이동 */
              router.replace(
                {pathname: PATH_DONOTS_PAGE.AUTH.CONFIRM, query: {from: CONFIRM_TYPE.ADD_CHILDREN}},
                undefined,
                {shallow: true}
              )
            }
          }
        })
        break
    }
  }, 300)

  useEffect(() => {
    return () => {
      resetAddChildrenValue()
      resetSelectedAllergy()
      resetSelectedKeyword()

      setIsOnClickDisabled(false)
    }
  }, [])

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      handleBackButton()
    }
  }, [handleBackKey])

  return (
    <AddChildrenHeaderLayout onBack={handleBackButton}>

      <Container disableGutters maxWidth={'xs'} sx={{mt: '10px', px: '20px'}}>

        {/* 아이 정보 입력 스텝 */}
        {step === ADD_CHILDREN_STEP.NICKNAME && <ChildrenNicknameInputSection setIsBottomButtonDisabled={setIsBottomButtonDisabled}/>}
        {step === ADD_CHILDREN_STEP.BIRTH && <ChildrenBirthInputSection setIsBottomButtonDisabled={setIsBottomButtonDisabled}/>}
        {step === ADD_CHILDREN_STEP.HEIGHT && <ChildrenHeightInputSection setIsBottomButtonDisabled={setIsBottomButtonDisabled}/>}
        {step === ADD_CHILDREN_STEP.WEIGHT && <ChildrenWeightInputSection setIsBottomButtonDisabled={setIsBottomButtonDisabled}/>}
        {step === ADD_CHILDREN_STEP.GENDER && <ChildrenGenderInputSection/>}
        {step === ADD_CHILDREN_STEP.ALLERGY && <ChildrenAllergyInputSection setIsBottomButtonDisabled={setIsBottomButtonDisabled}/>}
        {step === ADD_CHILDREN_STEP.KEYWORD && <ChildrenKeywordInputSection setIsBottomButtonDisabled={setIsBottomButtonDisabled}/>}

        {/* 하단 버튼 */}
        <BottomFixedButton isButtonDisabled={isBottomButtonDisabled} isOnClickDisabled={isOnClickDisabled}
                           handleClick={handleClickNextStep}/>

      </Container>

    </AddChildrenHeaderLayout>
  )
}

function BottomFixedButton({isButtonDisabled, isOnClickDisabled, handleClick}) {
  return (
    <Container
      disableGutters
      maxWidth={'xs'}
      sx={{
        position: 'fixed',
        bottom: 0,
        bgcolor: 'white',
        p: '20px',
        ...sxFixedCenterMainLayout
      }}
    >
      <Button
        tabIndex={0}
        fullWidth
        variant="contained"
        disabled={isButtonDisabled}
        onClick={isOnClickDisabled ? () => {
        } : handleClick}
        disableFocusRipple
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          p: '12px 50px',
          height: '52px',
          fontStyle: 'normal',
          fontWeight: '700',
          fontSize: '16px',
          lineHeight: '28px',
          textAlign: 'center',
          color: '#FFFFFF',
          '&.Mui-disabled': {
            color: '#B3B3B3',
            backgroundColor: '#E8E8E8',
          },
          '&.Mui-disabled:hover': {
            backgroundColor: '#E6E6E6',
          },
          '&:hover': {
            boxShadow: 'none',
            backgroundColor: 'primary.main',
          },
          ...clickBorderNone
        }}
      >
        {COMMON_TEXT.CONFIRM}
      </Button>
    </Container>
  )
}
