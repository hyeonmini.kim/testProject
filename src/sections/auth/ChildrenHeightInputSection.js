import {Box, IconButton, Typography} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import {useEffect, useState} from "react";
import {CHILD_HEIGHT, SIGN_UP_CHILD_HEIGHT, SIGN_UP_TITLE} from "../../constant/sign-up/SignUp";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {authChildHeightSelector} from "../../recoil/selector/sign-up/signUpSelector";
import {inputChildrenHeightSelector} from "../../recoil/selector/home/inputChildrenInfoSelector";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {ALT_STRING} from "../../constant/common/AltString";
import TextFieldWrapper from "../../components/common/TextFieldWrapper";

export default function ChildrenHeightInputSection({isGuest = false, setIsBottomButtonDisabled}) {
  const [childHeight, setChildHeight] = useRecoilState(authChildHeightSelector)
  const setInputChildHeight = useSetRecoilState(inputChildrenHeightSelector)
  
  const [isTrue, setIsTrue] = useState(true)
  const [helperText, setHelperText] = useState(SIGN_UP_CHILD_HEIGHT.HELPER_TXT)
  const [cancelIcon, setCancelIcon] = useState(false)
  const [hasDot, setHasDot] = useState(false)
  const [hasDotTwoMore, setHasDotTwoMore] = useState(false)
  
  const handleChangeValue = (e) => {
    const height = (e.target.value?.length === 1 && e.target.value === '0') ? '' : e.target.value
    setChildHeight(height)
    if (isGuest) setInputChildHeight(height)
  }
  
  const checkChildHeightValidation = () => {
    const regex = /^\d*[.]?$/
    const regexOnlyDot = /^[.]$/
    const regexLastDot = /^\d*[.]$/
    const regexHasDot = /^\d*[.]\d$/

    if (childHeight !== '') {
      if (childHeight > CHILD_HEIGHT.MAX) {
        setErrorHelperText(SIGN_UP_CHILD_HEIGHT.HELPER_TXT_MAX_HEIGHT)
        setIsTrue(false)
        setIsBottomButtonDisabled(true)
      } else if (childHeight < CHILD_HEIGHT.MIN) {
        setErrorHelperText(SIGN_UP_CHILD_HEIGHT.HELPER_TXT_MIN_HEIGHT)
        setIsTrue(false)
        setIsBottomButtonDisabled(true)
      } else {
        if (regex.test(childHeight) && !regexOnlyDot.test(childHeight) && !regexLastDot.test(childHeight)) {
          setErrorHelperText(SIGN_UP_CHILD_HEIGHT.HELPER_TXT)
          setIsTrue(true)
          setHasDot(false)
          setIsBottomButtonDisabled(false)
        } else {
          if (regexHasDot.test(childHeight)) {
            setIsTrue(true)
            setHasDot(true)
            setHasDotTwoMore(false)
            setIsBottomButtonDisabled(false)
          } else {
            setErrorHelperText(SIGN_UP_CHILD_HEIGHT.HELPER_TXT)
            setIsTrue(false)
            setHasDot(true)
            setHasDotTwoMore(true)
            setIsBottomButtonDisabled(true)
          }
        }
      }
    } else {
      setErrorHelperText(SIGN_UP_CHILD_HEIGHT.HELPER_TXT)
      setIsTrue(false)
      setIsBottomButtonDisabled(true)
    }
  }

  const setErrorHelperText = (helperText) => {
    if (helperText === SIGN_UP_CHILD_HEIGHT.HELPER_TXT && !hasDotTwoMore) {
      setIsTrue(true)
      setHelperText(helperText)
      setIsBottomButtonDisabled(false)
    } else {
      setIsTrue(false)
      setHelperText(helperText)
      setIsBottomButtonDisabled(true)
    }
  }
  
  const handleFocus = () => setCancelIcon(true)
  const handleBlur = () => setCancelIcon(false)
  const handleCancel = () => setChildHeight('')

  useEffect(() => {
    checkChildHeightValidation()
  }, [childHeight])

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_KIDS_HEIGHT)
  }, [])

  return (
    <Box>
      <Typography
        sx={{
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '24px',
          lineHeight: '32px',
          letterSpacing: '-0.02em',
          color: '#000000',
          whiteSpace: 'pre-wrap',
        }}
      >
        {SIGN_UP_TITLE.CHILD_HEIGHT}
      </Typography>
      <TextFieldWrapper
        autoComplete='off'
        fullWidth
        value={childHeight}
        label={childHeight === '' ? SIGN_UP_CHILD_HEIGHT.LABEL : ' '}
        onChange={handleChangeValue}
        onFocus={handleFocus}
        onBlur={handleBlur}
        variant="standard"
        helperText={childHeight === '' ? helperText : isTrue && hasDot ? ' ' : helperText}
        inputProps={{inputMode: 'decimal', tabIndex: 0}}
        InputProps={{
          endAdornment: (
            childHeight === '' ?
              <Typography
                sx={{
                  fontStyle: 'normal',
                  fontWeight: 500,
                  fontSize: '16px',
                  lineHeight: '22px',
                  letterSpacing: '-0.03em',
                  color: '#000000',
                  marginRight: '14px',
                }}
              >
                cm
              </Typography>
              :
              <Box display="flex" marginRight="14px">
                {childHeight && cancelIcon && (
                  <IconButton tabIndex={-1} sx={{p: 0, marginRight: '8px'}}>
                    <SvgCommonIcons alt={ALT_STRING.COMMON.ICON_CLEAR_INPUT_TEXT} type={ICON_TYPE.DELETE_TEXT} onMouseDown={handleCancel}/>
                  </IconButton>
                )}
                <Typography
                  sx={{
                    fontStyle: 'normal',
                    fontWeight: 500,
                    fontSize: '16px',
                    lineHeight: '22px',
                    letterSpacing: '-0.03em',
                    color: '#000000',
                  }}
                >
                  cm
                </Typography>
              </Box>
          ),
          style: {
            paddingBottom: '11px',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#222222',
          },
        }}
        InputLabelProps={{
          shrink: false,
          style: {
            fontStyle: 'normal',
            fontWeight: 500,
            fontSize: '16px',
            lineHeight: '22px',
            letterSpacing: '-0.03em',
            color: '#CCCCCC',
          }
        }}
        FormHelperTextProps={{
          style: {
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '12px',
            lineHeight: '18px',
            letterSpacing: '-0.03em',
            color: childHeight === '' ? '#888888' : isTrue ? '#888888' : '#FF4842',
          }
        }}
        sx={{
          marginTop: '14px',
          '& .MuiInput-root:before': {
            borderBottom: childHeight === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:before': {
            borderBottom: childHeight === '' ? '1px solid #CCCCCC' : isTrue ? '1px solid #CCCCCC' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover:after': {
            borderBottom: childHeight === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:hover.Mui-disabled:before': {
            borderBottom: childHeight === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
          '& .MuiInput-root:after': {
            borderBottom: childHeight === '' ? '1px solid #222222' : isTrue ? '1px solid #222222' : '1px solid #FF4842',
          },
        }}
      />
    </Box>
  )
}
