import ChildBirthInputForm from "../auth/ChildBirthInputForm";
import ChildHeightInputForm from "../auth/ChildHeightInputForm";
import ChildWeightInputForm from "../auth/ChildWeightInputForm";
import ChildGenderInputForm from "../auth/ChildGenderInputForm";

export default function ChildProfileSectionList({page}) {
  return (
    <>
      {page === 0 && <ChildBirthInputForm isGuest={true}/>}
      {page === 1 && <ChildHeightInputForm isGuest={true}/>}
      {page === 2 && <ChildWeightInputForm isGuest={true}/>}
      {page === 3 && <ChildGenderInputForm isGuest={true}/>}
    </>
  )
}
