import ChildBirthInputForm from "../auth/ChildBirthInputForm";
import ChildHeightInputForm from "../auth/ChildHeightInputForm";
import ChildWeightInputForm from "../auth/ChildWeightInputForm";
import ChildGenderInputForm from "../auth/ChildGenderInputForm";
import ChildNicknameInputForm from "../auth/ChildNicknameInputForm";
import ChildAllergyInputForm from "../auth/ChildAllergyInputForm";
import ChildKeywordInputForm from "../auth/ChildKeywordInputForm";

export default function AddChildrenSectionList({page}) {
  return (
    <>
      {page === 0 && <ChildNicknameInputForm/>}
      {page === 1 && <ChildBirthInputForm/>}
      {page === 2 && <ChildHeightInputForm/>}
      {page === 3 && <ChildWeightInputForm/>}
      {page === 4 && <ChildGenderInputForm/>}
      {page === 5 && <ChildAllergyInputForm/>}
      {page === 6 && <ChildKeywordInputForm/>}
    </>
  )
}
