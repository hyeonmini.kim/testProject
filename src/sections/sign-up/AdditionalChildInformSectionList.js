import ChildAllergyInputForm from "../auth/ChildAllergyInputForm";
import ChildKeywordInputForm from "../auth/ChildKeywordInputForm";

export default function AdditionalChildInformSectionList({page}) {
  return (
    <>
      {page === 0 && <ChildAllergyInputForm/>}
      {page === 1 && <ChildKeywordInputForm/>}
    </>
  )
}
