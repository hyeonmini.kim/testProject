import ResetPasswordInputForm from "../auth/ResetPasswordInputForm";
import PhoneNumberInputForm from "../auth/PhoneNumberInputForm";
import PasswordInputForm from "../auth/PasswordInputForm";
import ResetPasswordAnswerInputForm from "../auth/ResetPasswordAnswerInputForm";

export default function ResetPasswordSectionList({page, setPage}) {
  return (
    <>
      {page === 0 && <ResetPasswordInputForm/>}
      {page === 1 && <PhoneNumberInputForm setPage={setPage}/>}
      {page === 2 && <PasswordInputForm/>}
      {page === 3 && <ResetPasswordAnswerInputForm/>}
    </>
  )
}
