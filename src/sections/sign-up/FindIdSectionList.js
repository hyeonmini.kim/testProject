import PhoneNumberInputForm from "../auth/PhoneNumberInputForm";
import FindIdAnswerInputForm from "../auth/FindIdAnswerInputForm";
import FindIdInputForm from "../auth/FindIdInputForm";

export default function FindIdSectionList({page}) {
  return (
    <>
      {page === 0 && <FindIdInputForm/>}
      {page === 1 && <PhoneNumberInputForm/>}
      {page === 2 && <FindIdAnswerInputForm/>}
    </>
  )
}