import PhoneNumberInputForm from "../auth/PhoneNumberInputForm";
import NicknameInputForm from "../auth/NicknameInputForm";
import ChildNicknameInputForm from "../auth/ChildNicknameInputForm";
import ChildBirthInputForm from "../auth/ChildBirthInputForm";
import ChildHeightInputForm from "../auth/ChildHeightInputForm";
import ChildWeightInputForm from "../auth/ChildWeightInputForm";
import ChildGenderInputForm from "../auth/ChildGenderInputForm";
import ChildKeywordInputForm from "../auth/ChildKeywordInputForm";
import ChildAllergyInputForm from "../auth/ChildAllergyInputForm";
import EmailInputForm from "../auth/EmailInputForm";

export default function SignUpSnsSectionList({page, setPage}) {
  return (
    <>
      {page === 0 && <PhoneNumberInputForm setPage={setPage}/>}
      {page === 1 && <EmailInputForm/>}
      {page === 2 && <NicknameInputForm/>}
      {page === 3 && <ChildNicknameInputForm/>}
      {page === 4 && <ChildBirthInputForm/>}
      {page === 5 && <ChildHeightInputForm/>}
      {page === 6 && <ChildWeightInputForm/>}
      {page === 7 && <ChildGenderInputForm/>}
      {page === 8 && <ChildAllergyInputForm/>}
      {page === 9 && <ChildKeywordInputForm/>}
    </>
  )
}
