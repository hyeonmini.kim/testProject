import PhoneNumberInputForm from "../auth/PhoneNumberInputForm";
import IdInputForm from "../auth/IdInputForm";
import EmailInputForm from "../auth/EmailInputForm";
import PasswordInputForm from "../auth/PasswordInputForm";
import NicknameInputForm from "../auth/NicknameInputForm";
import ChildNicknameInputForm from "../auth/ChildNicknameInputForm";
import ChildBirthInputForm from "../auth/ChildBirthInputForm";
import ChildHeightInputForm from "../auth/ChildHeightInputForm";
import ChildWeightInputForm from "../auth/ChildWeightInputForm";
import ChildGenderInputForm from "../auth/ChildGenderInputForm";
import ChildKeywordInputForm from "../auth/ChildKeywordInputForm";
import ChildAllergyInputForm from "../auth/ChildAllergyInputForm";
import BirthInputForm from "../auth/BirthInputForm";
import IdentificationQAInputForm from "../auth/IdentificationQAInputForm";

export default function SignUpIdIosSectionList({page}) {
  return (
    <>
      {page === 0 && <BirthInputForm/>}
      {page === 1 && <IdInputForm/>}
      {page === 2 && <PasswordInputForm/>}
      {page === 3 && <EmailInputForm/>}
      {page === 4 && <NicknameInputForm/>}
      {page === 5 && <IdentificationQAInputForm/>}
      {page === 6 && <ChildNicknameInputForm/>}
      {page === 7 && <ChildBirthInputForm/>}
      {page === 8 && <ChildHeightInputForm/>}
      {page === 9 && <ChildWeightInputForm/>}
      {page === 10 && <ChildGenderInputForm/>}
      {page === 11 && <ChildAllergyInputForm/>}
      {page === 12 && <ChildKeywordInputForm/>}
    </>
  )
}
