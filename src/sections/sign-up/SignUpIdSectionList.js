import PhoneNumberInputForm from "../auth/PhoneNumberInputForm";
import IdInputForm from "../auth/IdInputForm";
import EmailInputForm from "../auth/EmailInputForm";
import PasswordInputForm from "../auth/PasswordInputForm";
import NicknameInputForm from "../auth/NicknameInputForm";
import ChildNicknameInputForm from "../auth/ChildNicknameInputForm";
import ChildBirthInputForm from "../auth/ChildBirthInputForm";
import ChildHeightInputForm from "../auth/ChildHeightInputForm";
import ChildWeightInputForm from "../auth/ChildWeightInputForm";
import ChildGenderInputForm from "../auth/ChildGenderInputForm";
import ChildKeywordInputForm from "../auth/ChildKeywordInputForm";
import ChildAllergyInputForm from "../auth/ChildAllergyInputForm";

export default function SignUpIdSectionList({page, setPage}) {
  return (
    <>
      {page === 0 && <PhoneNumberInputForm setPage={setPage}/>}
      {page === 1 && <IdInputForm/>}
      {page === 2 && <PasswordInputForm/>}
      {page === 3 && <EmailInputForm/>}
      {page === 4 && <NicknameInputForm/>}
      {page === 5 && <ChildNicknameInputForm/>}
      {page === 6 && <ChildBirthInputForm/>}
      {page === 7 && <ChildHeightInputForm/>}
      {page === 8 && <ChildWeightInputForm/>}
      {page === 9 && <ChildGenderInputForm/>}
      {page === 10 && <ChildAllergyInputForm/>}
      {page === 11 && <ChildKeywordInputForm/>}
    </>
  )
}
