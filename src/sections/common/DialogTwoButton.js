import {Button, DialogActions, Grid} from "@mui/material";
import PropTypes from "prop-types";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../constant/common/Common";


DialogTwoButton.propTypes = {
  handleClose: PropTypes.func,
  dialog: PropTypes.object,
};

export default function DialogTwoButton({handleClose, dialog}) {
  if (!dialog) return

  const handleLeftButton = () => {
    handleClose()
    if (dialog.leftButtonProps?.onClick) {
      dialog.leftButtonProps.onClick()
    }
  }

  const handleRightButton = () => {
    handleClose()
    if (dialog.rightButtonProps?.onClick) {
      dialog.rightButtonProps.onClick()
    }
  }

  return (
    <>
      <DialogActions>
        <Grid container spacing={'10px'}>
          <Grid item xs={6}>
            <Button
              tabIndex={0}
              disableFocusRipple
              variant="outlined"
              size="large"
              fullWidth={true}
              onClick={handleLeftButton}
              onKeyDown={(e) => handleEnterPress(e, () => handleLeftButton)}
              sx={{
                height: '50px',
                px: 0,
                fontSize: '16px',
                fontWeight: 700,
                lineHeight: '24px',
                ...clickBorderNone,
              }}
            >
              {dialog.leftButtonProps?.text}
            </Button>
          </Grid>
          <Grid item xs={6}>
            <Button
              tabIndex={0}
              disableFocusRipple
              variant="contained"
              size="large"
              fullWidth={true}
              onClick={handleRightButton}
              onKeyDown={(e) => handleEnterPress(e, () => handleLeftButton)}
              sx={{
                height: '50px',
                px: 0,
                fontSize: '16px',
                fontWeight: 700,
                lineHeight: '24px',
                ...clickBorderNone,
              }}
            >
              {dialog.rightButtonProps?.text}
            </Button>
          </Grid>
        </Grid>
      </DialogActions>
    </>
  )
}