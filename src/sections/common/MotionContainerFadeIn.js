import {Box} from "@mui/material";
import {m} from "framer-motion";
import getVariant from "../_examples/extra/animate/getVariant";
import {MotionContainer} from "../../components/animate";

export function MotionContainerFadeIn({children}) {
  return (
    <MotionContainer>
      <Box
        component={m.div}
        variants={getVariant('fadeIn')}
      >
        {children}
      </Box>
    </MotionContainer>
  )
}

export function MotionContainerRotateIn({children}) {
  return (
    <MotionContainer>
      <Box
        component={m.div}
        variants={getVariant('rotateIn')}
      >
        {children}
      </Box>
    </MotionContainer>
  )
}

export function MotionContainerFadeOut({children}) {
  return (
    <MotionContainer>
      <Box
        component={m.div}
        variants={getVariant('fadeOut')}
      >
        {children}
      </Box>
    </MotionContainer>
  )
}

export function MotionContainerFadeInDown({children}) {
  return (
    <MotionContainer>
      <Box
        component={m.div}
        variants={getVariant('fadeInDown')}
      >
        {children}
      </Box>
    </MotionContainer>
  )
}