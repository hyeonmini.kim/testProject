import {Box, Typography} from "@mui/material";
import {useSetRecoilState} from "recoil";
import {authInputRefIndexState} from "../../recoil/atom/sign-up/auth";
import {commonBottomDialogState} from "../../recoil/atom/common/bottomDialog";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {SIGN_UP_IDENTIFICATION} from "../../constant/sign-up/SignUp";

export default function BottomDialogContents({setText, first, second}) {
  const setIsOpen = useSetRecoilState(commonBottomDialogState)
  const setIndex = useSetRecoilState(authInputRefIndexState)

  const handleClick = (e) => {
    e.stopPropagation()

    if ((e.target.innerText !== first) && (e.target.innerText !== second)) {
      setText(e.target.id, e.target.innerText)
      setIsOpen(false)
      setIndex(1)
    }
  }

  return (
    <Box sx={{textAlign: 'start', py: '9px'}}>
      {SIGN_UP_IDENTIFICATION.QUESTION.map((question, index) => {
        return (
          <Typography
            tabIndex={0}
            id={question?.code}
            key={index}
            value={question?.text}
            sx={{
              fontStyle: 'normal',
              fontWeight: 500,
              fontSize: '16px',
              lineHeight: '24px',
              letterSpacing: '-0.02em',
              color: ((question?.text === first) || (question?.text === second)) ? '#777777' : '#000000',
              paddingY: '9px',
            }}
            onClick={handleClick}
            onKeyDown={(e) => handleEnterPress(e, handleClick)}
          >
            {question?.text}
          </Typography>
        )
      })}
    </Box>
  )
}
