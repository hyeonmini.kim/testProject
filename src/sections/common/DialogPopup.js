import {Dialog, DialogContent, DialogTitle, Fade, Typography} from '@mui/material';
import PropTypes from "prop-types";
import {COMMON_DIALOG_TYPE} from "../../constant/common/Common";
import DialogTwoButton from "./DialogTwoButton";
import {useRecoilState} from "recoil";
import {dialogShowDialogSelector} from "../../recoil/selector/common/dialogSelector";
import DialogOneButton from "./DialogOneButton";
import {Z_INDEX} from "../../constant/common/ZIndex";
import {sxFixedCenterMainLayout} from "../../layouts/main/MainLayout";

// ----------------------------------------------------------------------
DialogPopup.propTypes = {
  message: PropTypes.object
};
// ----------------------------------------------------------------------

export default function DialogPopup({dialog}) {
  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)

  const handleClose = () => {
    setShowDialog(false)
  }

  return (
    <Dialog
      fullWidth
      maxWidth={'xs'}
      open={showDialog}
      onClose={handleClose}
      TransitionComponent={Fade}
      PaperProps={{style: {margin: '20px'}}}
      sx={{
        zIndex: Z_INDEX.DIALOG,
        '& .MuiDialog-paper .MuiDialogActions-root': {
          padding: '20px',
        },
        '& .MuiBackdrop-root ': {
          background: 'rgba(0,0,0,0.7)',
        },
        '& .MuiPaper-root ': {
          borderRadius: '8px',
        },
        ...sxFixedCenterMainLayout
      }}
    >
      <DialogTitle sx={{py: '15px'}}></DialogTitle>

      <DialogContent sx={{px: '20px', pb: '10px'}}>
        <Typography sx={{
          whiteSpace: 'pre-line',
          textAlign: 'center',
          fontSize: '16px',
          fontWeight: dialog?.isBoldMsg ? '700' : '400',
          lineHeight: '24px',
          color: '#222222'
        }}>
          {dialog?.message}
        </Typography>
      </DialogContent>

      {dialog?.subMessage ? (
        <DialogContent sx={{px: '20px', mt: '5px', mb: '5px'}}>
          <Typography sx={{
            whiteSpace: 'pre-line',
            textAlign: 'center',
            fontSize: '16px',
            fontWeight: '400',
            lineHeight: '24px',
            color: '#666666'
          }}>
            {dialog?.subMessage}
          </Typography>
        </DialogContent>
      ) : (
        <></>
      )}

      {dialog?.children ? (
        <DialogContent sx={{px: '20px', m: '25px 18px 0 18px'}}>
          {dialog?.children}
        </DialogContent>
      ) : (
        <></>
      )}

      {dialog?.type === COMMON_DIALOG_TYPE.ONE_BUTTON && <DialogOneButton dialog={dialog} handleClose={handleClose}/>}
      {dialog?.type === COMMON_DIALOG_TYPE.TWO_BUTTON && <DialogTwoButton dialog={dialog} handleClose={handleClose}/>}
    </Dialog>
  );
}
