import {Box, Button} from "@mui/material";
import PropTypes from "prop-types";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../constant/common/Common";

DialogOneButton.propTypes = {
  handleClose: PropTypes.func,
  dialog: PropTypes.object,
};

export default function DialogOneButton({handleClose, dialog}) {
  if (!dialog) return

  const handleButton = () => {
    handleClose()
    if (dialog.handleButton1?.onClick) {
      dialog.handleButton1.onClick()
    }
  }

  return (
    <Box sx={{padding: '20px'}}>
      <Button
        tabIndex={0}
        disableFocusRipple
        variant={'contained'}
        fullWidth={true}
        onClick={handleButton}
        onKeyDown={(e) => handleEnterPress(e, () => handleButton)}
        sx={{
          height: '50px',
          fontStyle: 'normal',
          fontWeight: 700,
          fontSize: '16px',
          lineHeight: '24px',
          letterSpacing: '-0.02em',
          color: '#FFFFFF',
          ...clickBorderNone,
        }}
      >
        {dialog.handleButton1?.text}
      </Button>
    </Box>
  )
}