// @mui
import {Button, Container, Slide, Stack, Typography,} from '@mui/material';
// components
import {useRecoilState} from "recoil";
import * as React from "react";
import {forwardRef} from "react";
import {Z_INDEX} from "../../constant/common/ZIndex";
import {showFullScreenMessageDialogSelector} from "../../recoil/selector/common/dialogSelector";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {MainDialogLayout} from "../../layouts/main/MainLayout";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {clickBorderNone} from "../../constant/common/Common";

// ----------------------------------------------------------------------
const Transition = forwardRef((props, ref) => <Slide direction="up" ref={ref} {...props} />);
export default function FullScreenMessageDialog({type, title, detail}) {
  const [openFullScreenMessageDialog, setOpenFullScreenMessageDialog] = useRecoilState(showFullScreenMessageDialogSelector);

  const onClick = () => {
    setOpenFullScreenMessageDialog(false);
  }

  return (
    <MainDialogLayout
      fullScreen open={openFullScreenMessageDialog} TransitionComponent={Transition}
      sx={{'& .MuiDialog-paper': {mx: 0, height: '100%', backgroundColor: 'white'}, zIndex: Z_INDEX.DIALOG}}
    >
      <Container maxWidth={'xs'} disableGutters
                 sx={{px: '20px', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100%'}}>
        <Stack
          alignItems="center"
        >
          {type === 'yes' ? <SvgCommonIcons type={ICON_TYPE.COMMON_YES}/> : (type === 'no' ?
            <SvgCommonIcons type={ICON_TYPE.COMMON_NO}/> : <SvgCommonIcons type={ICON_TYPE.COMMON_CHECK}/>)}
          <Typography
            sx={{
              textAlign: 'center',
              marginTop: '30px',
              fontStyle: 'normal',
              fontWeight: 500,
              fontSize: '18px',
              lineHeight: '24px',
              color: '#222222',
              whiteSpace: 'pre-wrap',
            }}
          >
            {title}
          </Typography>
          <Typography
            sx={{
              marginTop: '12px',
              fontStyle: 'normal',
              fontWeight: 400,
              fontSize: '14px',
              lineHeight: '20px',
              color: '#666666',
              whiteSpace: 'pre-wrap',
            }}
          >
            {detail}
          </Typography>
        </Stack>
      </Container>

      {/* Footer */}
      <Container
        maxWidth={'xs'}
        disableGutters
        sx={{
          position: 'fixed',
          bottom: '0',
          left: '0',
          right: '0',
          padding: '20px'
        }}
      >
        <Button
          tabIndex={1}
          disableFocusRipple
          fullWidth
          type="submit"
          variant="contained"
          sx={{
            height: '52px',
            fontSize: '16px',
            fontWeight: '700',
            '&:hover': {
              boxShadow: 'none',
              backgroundColor: 'primary.main',
            },
            ...clickBorderNone
          }}
          onClick={onClick}
          onKeyDown={(e) => handleEnterPress(e, onClick)}
        >
          확인
        </Button>
      </Container>

    </MainDialogLayout>
  );
}
