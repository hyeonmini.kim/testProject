import {Box, Divider, Typography} from "@mui/material";
import {useSetRecoilState} from "recoil";
import {authInputRefIndexState, authTelecomState} from "../../recoil/atom/sign-up/auth";
import {commonBottomDialogState} from "../../recoil/atom/common/bottomDialog";
import {handleEnterPress} from "../../utils/onKeyDownUtils";

export default function TelecomContents() {
  const telecomList = [
    'SKT',
    'KT',
    'LG U+',
    'SKT 알뜰폰',
    'KT 알뜰폰',
    'LG U+ 알뜰폰',
  ];

  const setTelecom = useSetRecoilState(authTelecomState);
  const setIsOpen = useSetRecoilState(commonBottomDialogState);
  const setIndex = useSetRecoilState(authInputRefIndexState);
  const handleClick = (e) => {
    setTelecom(e.target.innerText);
    setIsOpen(false);
    setIndex(1);
  }

  return (
    <Box sx={{mt: '24px', mb: '16px', textAlign: 'start'}}>
      <Typography
        tabIndex={0}
        sx={{
          fontStyle: 'normal',
          fontWeight: 500,
          fontSize: '16px',
          lineHeight: '24px',
          letterSpacing: '-0.02em',
          color: '#000000',
          paddingX: '8px',
          marginBottom: '27px',
        }}
      >
        통신사
      </Typography>
      <Divider sx={{height: '1px', backgroundColor: '#E6E6E6', marginBottom: '14px'}}/>
      {telecomList.map((telecom, index) => {
        return (
          <Typography
            tabIndex={0}
            key={index}
            value={telecom}
            sx={{
              fontStyle: 'normal',
              fontWeight: 500,
              fontSize: '16px',
              lineHeight: '24px',
              letterSpacing: '-0.02em',
              color: '#000000',
              paddingX: '8px',
              paddingY: '9px',
            }}
            onClick={handleClick}
            onKeyDown={(e) => handleEnterPress(e, handleClick)}
          >
            {telecom}
          </Typography>
        );
      })}
    </Box>
  );
}