import {Box, FormControlLabel, FormGroup, Typography} from '@mui/material';
import {HEADER} from "../../config";
import DonotsTextField from "../../components/common/DonotsTextField";
import * as React from "react";
import {useEffect, useState} from "react";
import DonotsBottomButton from "../../components/common/DonotsBottomButton";
import {useRecoilState, useSetRecoilState} from "recoil";
import {
  authBottomButtonMarginState,
  authBottomButtonPositionState,
  authBottomButtonState,
  authLoginErrorMessageState,
  authLoginIdState,
} from "../../recoil/atom/sign-up/auth";
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import Checkbox from '@mui/material/Checkbox';
import {useBefuAuthContext} from "../../auth/useAuthContext";
import {useRouter} from "next/router";
import {FROM_TYPE, LOGIN, MAIN} from "../../constant/sign-up/SignUp";
import {getNativeOpt, setNativeOpt} from "../../channels/commonChannel";
import {MY_SETTING_OPTION} from "../../constant/my/Setting";
import {isNative} from "../../utils/envUtils";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import {idSchema, isValidateParam} from "../../utils/validateUtils";
import {handleEnterPress} from "../../utils/onKeyDownUtils";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {setCookie} from "../../utils/storageUtils";
import {COMMON_STR} from "../../constant/common/Common";
import {postGetUuidMutate} from "../../api/authApi";
import {authUuidSelector, signUpTypeSelector} from "../../recoil/selector/sign-up/signUpSelector";
import {commonAgreeOpenSelector} from "../../recoil/selector/auth/agreeDetailSelector";
import AgreeContents from "../../components/common/agreement/AgreeContents";
import LoadingScreen from "../../components/common/LoadingScreen";

export default function Login({mainRef, openDetail, setOpenDetail, openDetailSert, setOpenDetailSert}) {
  const {login} = useBefuAuthContext()
  const [loginId, setLoginId] = useRecoilState(authLoginIdState)
  const [isValidId, setIsValidId] = useState(true)

  const labelId = "아이디를 입력해주세요"
  const nameId = "id"
  const typeId = "text"
  const handleChangeId = (e) => {
    setWrongMessage('')
    const value = e.target.value.toLowerCase()
    isValidateParam(value, idSchema).then((isVal) => {
      setIsValidId(isVal)
      setLoginId(value.substring(0, 12))
    })
  }
  // 비밀번호 관련
  const [valuePassword, setValuePassword] = useState('')
  const labelPassword = "비밀번호를 입력해주세요"
  const namePassword = "password"
  const typePassword = "password"

  const handleChangePassword = (e) => {
    setWrongMessage('')
    setValuePassword(e.target.value)
  }
  // 입력 정보 관련
  const [wrongMessage, setWrongMessage] = useRecoilState(authLoginErrorMessageState)
  // 버튼 관련
  const setBottomButtonPosition = useSetRecoilState(authBottomButtonPositionState)
  const setBottomButtonMargin = useSetRecoilState(authBottomButtonMarginState)
  const setBottomButtonDisabled = useSetRecoilState(authBottomButtonState)
  const [isAutoLoginFlag, setIsAutoLoginFlag] = useState(true)
  const [isBottomBtnDisabled, setIsBottomBtnDisabled] = useRecoilState(authBottomButtonState)

  const router = useRouter()
  const isApp = isNative()

  useEffect(() => {
    mainRef?.current?.addEventListener('resize', handleClickJoin);

    return () => {
      mainRef?.current?.removeEventListener('resize', () => alert('hihihi'));
    };
  }, []);

  const handleBottomButtonClick = () => {
    if (!isBottomBtnDisabled) {
      if (isValidId) {
        login(loginId, valuePassword)
      } else {
        setWrongMessage(LOGIN.ACCOUNT_NOT_FOUND)
      }
    }
  }

  const [uuid, setUuid] = useRecoilState(authUuidSelector)
  const {mutate: mutatePostGetUuid, isLoading: isLoadingPostGetUuid} = postGetUuidMutate()
  const [signUpType, setSignUpType] = useRecoilState(signUpTypeSelector)
  const [openAgreeDlg, setOpenAgreeDlg] = useRecoilState(commonAgreeOpenSelector)
  const handleClickJoin = () => {
    mutatePostGetUuid({}, {
      onSuccess: (uuid) => {
        setUuid(uuid)
        setCookie(COMMON_STR.UUID, uuid, 1)
        setOpenAgreeDlg(true)
        setSignUpType(COMMON_STR.ID)
      }
    })
  }

  const handleClickFindId = () => {
    router.push({
      pathname: PATH_DONOTS_PAGE.AUTH.SIGNUP,
      query: {from: FROM_TYPE.FIND_ID},
    })
  }

  const handleClickFindPassword = () => {
    router.push({
      pathname: PATH_DONOTS_PAGE.AUTH.SIGNUP,
      query: {from: FROM_TYPE.RESET_PASSWORD},
    })
  }

  const changeAutoLogin = (e) => {
    setIsAutoLoginFlag(e.target.checked)
    setNativeOpt({type: MY_SETTING_OPTION.AUTO_LOGIN, value: String(e.target.checked)})
  }

  useEffect(() => {
    if (isApp) {
      try {
        getNativeOpt(MY_SETTING_OPTION.AUTO_LOGIN, async (result) => {
          setIsAutoLoginFlag(JSON.parse(result?.val))
        })
      } catch (e) {
        setIsAutoLoginFlag(false)
      }
    }
  }, [])

  useEffect(() => {
    setBottomButtonPosition('initial')
    setBottomButtonMargin('0px')
    setHackleTrack(HACKLE_TRACK.LOGIN)

    return () => {
      setBottomButtonPosition('fixed')
      setBottomButtonMargin('20px')
      setWrongMessage('')
      setLoginId('')
    }
  }, [])

  useEffect(() => {
    if (loginId.length !== 0 && valuePassword.length !== 0) {
      setBottomButtonDisabled(false)
    } else {
      setBottomButtonDisabled(true)
    }
  }, [loginId, valuePassword])

  const {isInitialized} = useBefuAuthContext();
  if (!isInitialized) {
    return <LoadingScreen/>
  }

  return (
    <>
      <Box sx={{pt: `${HEADER.H_MOBILE}px`, background: '#FFFFFF', pb: '20px'}}>
        <Box sx={{textAlign: 'left', px: '20px', py: '30px'}}>

          {/* 타이틀 영역 */}
          <Typography variant={'h1_24_b'} sx={{color: '#000000'}}>
            {LOGIN.TITLE}
          </Typography>

          <Box sx={{height: '10px'}}/>

          {/* 콘텐츠 영역 */}
          <DonotsTextField
            tabIndex={2}
            handleChange={handleChangeId}
            label={labelId}
            type={typeId}
            value={loginId}
            setValue={setLoginId}
          />
          <DonotsTextField
            tabIndex={3}
            handleChange={handleChangePassword}
            onEnterKeyDown={handleBottomButtonClick}
            label={labelPassword}
            type={typePassword}
            value={valuePassword}
            setValue={setValuePassword}
          />
          <Typography variant={'bs_12_r'} sx={{color: '#FF4842', marginTop: wrongMessage.length !== 0 ? '8px' : '0px'}}>
            {wrongMessage}
          </Typography>

          <Box sx={{height: '13px'}}/>

          {/* 자동 로그인 체크 */}
          {isNative() && <AutoLogin isAutoLoginFlag={isAutoLoginFlag} changeAutoLogin={changeAutoLogin}/>}

          <Box sx={{height: '13px'}}/>

          {/* 로그인 버튼 */}
          <DonotsBottomButton tabIndex={4} handleClick={handleBottomButtonClick} buttonText={MAIN.LOGIN}/>

          {/* 회원가입, 아이디 찾기, 비밀번호 재설정 */}
          <MemberLink handleClickJoin={handleClickJoin} handleClickFindId={handleClickFindId}
                      handleClickFindPassword={handleClickFindPassword}/>

          {/* 이용약관 팝업 */}
          {openAgreeDlg &&
            <AgreeContents
              isOpen={openAgreeDlg}
              setIsOpen={setOpenAgreeDlg}
              signUpType={signUpType}
              openDetail={openDetail}
              setOpenDetail={setOpenDetail}
              openDetailCert={openDetailSert}
              setOpenDetailCert={setOpenDetailSert}
            />
          }

        </Box>
      </Box>
    </>
  )
}

function AutoLogin({isAutoLoginFlag, changeAutoLogin}) {
  return (
    <FormGroup>
      <FormControlLabel
        control={
          <Checkbox
            checked={isAutoLoginFlag}
            onChange={changeAutoLogin}
            disabled={!isNative()}
            disableRipple
            icon={<CheckCircleIcon htmlColor={'#CCCCCC'}/>}
            checkedIcon={<CheckCircleIcon htmlColor={'primary.main'}/>}
            size={"medium"}
          />
        }
        label={
          <Typography variant={'b2_14_r'} sx={{color: '#000000'}}>
            {LOGIN.AUTO}
          </Typography>
        }
      />
    </FormGroup>
  )
}

function MemberLink({handleClickJoin, handleClickFindId, handleClickFindPassword}) {
  return (
    <Box sx={{display: 'flex', flexDirection: 'column', justifyContent: 'center', mt: '20px'}}>
      <Box sx={{display: 'flex', justifyContent: 'center'}}>
        <Typography
          tabIndex={5}
          variant={'b2_14_m'}
          onClick={handleClickJoin}
          onKeyDown={(e) => handleEnterPress(e, handleClickJoin)}
          sx={{color: '#000000', cursor: 'pointer'}}
        >
          {LOGIN.JOIN}
        </Typography>
        <Typography variant={'b2_14_m'} sx={{color: '#000000', mx: '8px'}}>
          {LOGIN.DIVIDER}
        </Typography>
        <Typography
          tabIndex={5}
          variant={'b2_14_m'}
          onClick={handleClickFindId}
          onKeyDown={(e) => handleEnterPress(e, handleClickFindId)}
          sx={{color: '#000000', cursor: 'pointer'}}
        >
          {LOGIN.FIND_ID}
        </Typography>
        <Typography variant={'b2_14_m'} sx={{color: '#000000', mx: '8px'}}>
          {LOGIN.DIVIDER}
        </Typography>
        <Typography
          tabIndex={6}
          variant={'b2_14_m'}
          onClick={handleClickFindPassword}
          onKeyDown={(e) => handleEnterPress(e, handleClickFindPassword)}
          sx={{color: '#000000', cursor: 'pointer'}}
        >
          {LOGIN.RESET_PASSWORD}
        </Typography>
      </Box>
    </Box>
  )
}