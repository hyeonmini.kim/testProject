import {Avatar, Box, Skeleton, Stack, Typography} from "@mui/material";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {useRecoilState} from "recoil";
import * as React from "react";
import {BellIcon, BellLottie} from "../../../components/home/dynamicIsland/NotificationBell";
import {dynamicIslandExpandSelector} from "../../../recoil/selector/home/dynamicIslandSelector";
import {MotionContainerFadeIn} from "../../common/MotionContainerFadeIn";
import {HOME_PERSONAL_COMMON} from "../../../constant/home/Personalization";
import {ALT_STRING} from "../../../constant/common/AltString";

export default function CloseDynamicSection({info, avatar}) {
  const [expand, setExpand] = useRecoilState(dynamicIslandExpandSelector);

  return (
    <MotionContainerFadeIn>
      <Stack direction="row" alignItems="center" onClick={() => setExpand(!expand)}>

        {/* 아이 아바타 */}
        <BabyAvatarSection avatar={avatar}/>

        {/* 아이 타이틀 & 웰컴 메시지 */}
        <BabyTitleSection
          title={HOME_PERSONAL_COMMON.BABY.STATUS(info?.profile?.statement_status)}
          subTitle={HOME_PERSONAL_COMMON.WELCOME()}
        />

      </Stack>
    </MotionContainerFadeIn>
  )
}

function BabyAvatarSection({avatar}) {
  return (
    avatar
      ? <BabyAvatar avatar={avatar}/>
      : <SvgCommonIcons alt={ALT_STRING.HOME.PROFILE} type={ICON_TYPE.PROFILE_NO_IMAGE_80PX} sx={{width: '44px'}}/>
  )
}

function BabyTitleSection({title, subTitle}) {
  return (
    <Box sx={{ml: '12px', mr: '40px', display: 'flex', flexGrow: 1}}>
      {
        title
          ? <BabyTitle title={title} subTitle={subTitle}/>
          : <BabyTitleSkeleton/>
      }
    </Box>
  )
}

function BabyTitle({title, subTitle}) {
  return (
    <Box>
      <Typography align={'left'} sx={{fontSize: '16px', fontWeight: 500, lineHeight: '16px'}}>
        {title}
      </Typography>
      <Typography align={'left'} sx={{mt: '6px', fontSize: '12px', fontWeight: 400, lineHeight: '12px', color: '#888888'}}>
        {subTitle}
      </Typography>
    </Box>
  )
}

function BabyTitleSkeleton() {
  return (
    <Stack spacing={'4px'} direction="column">
      <Skeleton variant="rectangular" sx={{width: '200px', height: '20px', borderRadius: '5px'}}/>
      <Skeleton variant="rectangular" sx={{width: '160px', height: '12px', borderRadius: '5px'}}/>
    </Stack>
  )
}

function BabyAvatar({avatar}) {
  return (
    <Avatar
      alt={ALT_STRING.HOME.PROFILE}
      src={avatar}
      sx={{
        width: 44,
        height: 44,
        alignItems: 'center'
      }}
    />
  )
}