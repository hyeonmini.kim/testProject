import {Card} from "@mui/material";
import * as React from "react";
import OpenDynamicSection from "./OpenDynamicSection";
import {GUEST_BABY_INFO} from "../../../constant/home/Guest";

export default function GuestDynamicSection({mainRef, sx}) {
  return (
    <Card
      tabIndex={1}
      sx={{
        textAlign: 'center',
        boxShadow: '0px 2px 16px rgba(6, 51, 54, 0.1)',
        border: '1px solid #E9EDF4',
        borderRadius: '16px',
        ...sx
      }}
    >
      {/* 다이나믹 아일랜드 Open */}
      <OpenDynamicSection
        info={GUEST_BABY_INFO}
        avatar={'/assets/images/common/baby_avatar.png'}
        nickName={GUEST_BABY_INFO.nickName}
        guest
        mainRef={mainRef}
      />

    </Card>
  )
}
