import {Box, CardContent, Collapse, Divider, LinearProgress, linearProgressClasses, Skeleton, Stack, Typography,} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import {dynamicIslandExpandSelector} from "../../../recoil/selector/home/dynamicIslandSelector";
import * as React from "react";
import {useEffect} from "react";
import BabyInfoSection from "../../../components/home/dynamicIsland/BabyInfo";
import {HOME_PERSONAL_COMMON} from "../../../constant/home/Personalization";
import {myCallSelector} from "../../../recoil/selector/my/myDataSelector";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {MY_CALL} from "../../../constant/my/MyConstants";
import {useRouter} from "next/router";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import {COMMON_DIALOG_TYPE} from "../../../constant/common/Common";
import {LOGIN_POPUP_TEXT} from "../../../constant/recipe/detail/DetailConstants";
import {dialogSelector} from "../../../recoil/selector/common/dialogSelector";

export default function OpenDynamicSection({info, avatar, nickName, guest, mainRef}) {
  const [expand, setExpand] = useRecoilState(dynamicIslandExpandSelector);

  useEffect(() => {
    if (guest) {
      setExpand(true)
    }
  }, [])

  return (
    <Collapse in={expand}>
      <CardContent
        tabIndex={-1}
        sx={{
          pt: '20px',
          px: '20px',
          '&.MuiCardContent-root': {
            pb: '0px'
          }
        }}
      >
        {/* 아이 프로필 & 백분위수 */}
        <BabyInfoSection nickName={nickName} info={info} avatar={avatar} mainRef={mainRef}/>

        {/* 아이 건강 차트 */}
        <HealthInfoChartSection info={info}/>

        <Divider tabIndex={-1} sx={{mt: '30px'}}/>

        {/* 발달 정보 업데이트 하기 버튼*/}
        <HealthInfoUpdateSection nickName={nickName} guest={guest} sx={{mt: '20px', mb: '30px'}}/>

      </CardContent>
    </Collapse>

  )
}

function HealthInfoChartSection({info}) {
  return (
    <Box sx={{mt: '14px'}}>
      {
        info
          ? <HealthInfoChart info={info}/>
          : <HealthInfoChartSkeleton/>
      }
    </Box>
  )
}

function HealthInfoChart({info}) {
  return (
    <Stack>
      {/* BMI */}
      <HealthInfoChartItem
        title={HOME_PERSONAL_COMMON.BABY.BMI}
        value={info?.profile_bmi?.statement}
      />

      {/* 키 */}
      <HealthInfoChartItem
        title={HOME_PERSONAL_COMMON.BABY.HEIGHT}
        value={info?.profile_height?.value}
        statement={info?.profile_height?.statement}
        percent={info?.profile_height?.percentage}
        sx={{mt: '40px'}}
      />

      {/* 몸무게 */}
      <HealthInfoChartItem
        title={HOME_PERSONAL_COMMON.BABY.WEIGHT}
        value={info?.profile_weight?.value}
        statement={info?.profile_weight?.statement}
        percent={info?.profile_weight?.percentage}
        sx={{mt: '24px'}}
      />
    </Stack>
  )
}

function HealthInfoChartItem({title, value = '', statement, percent, sx}) {
  return (
    <Stack sx={{...sx}}>
      <Stack direction={'row'} spacing={'8px'}>
        <Typography align={'left'} sx={{fontWeight: 700, fontSize: '16px', lineHeight: '16px'}}>
          {title}
        </Typography>
        <Typography align={'left'} sx={{fontWeight: 400, fontSize: '16px', lineHeight: '16px'}}>
          {value}
        </Typography>
        {
          statement && (
            <Typography align={'left'} sx={{fontWeight: 400, fontSize: '14px', lineHeight: '14px'}}>
              {statement}
            </Typography>
          )
        }
      </Stack>
      <Box>
        {
          percent && (
            <LinearProgress
              variant='determinate'
              value={Number(percent)}
              sx={{
                mt: '10px',
                height: '15px',
                bgcolor: '#F5F5F5',
                borderRadius: '99px',
                [`& .${linearProgressClasses.bar}`]: {
                  borderRadius: '99px',
                  background: '#FFD092',
                },
                width: 1,
              }}
            />
          )
        }
      </Box>
    </Stack>
  )
}

function HealthInfoChartSkeleton() {
  return (
    <Stack>

      {/* BMI */}
      <Skeleton variant="rectangular" sx={{width: '100px', height: '16px', borderRadius: '5px'}}/>

      {/* 키 */}
      <Stack spacing={'10px'} sx={{mt: '30px'}}>
        <Skeleton variant="rectangular" sx={{width: '180px', height: '16px', borderRadius: '5px'}}/>
        <Skeleton variant="rectangular" sx={{width: 1, height: '15px', borderRadius: '5px'}}/>
      </Stack>

      {/* 몸무게 */}
      <Stack spacing={'10px'} sx={{mt: '24px'}}>
        <Skeleton variant="rectangular" sx={{width: '180px', height: '16px', borderRadius: '5px'}}/>
        <Skeleton variant="rectangular" sx={{width: 1, height: '15px', borderRadius: '5px'}}/>
      </Stack>

    </Stack>
  )
}

function HealthInfoUpdateSection({nickName, guest, sx}) {
  const [myCall, setMyCall] = useRecoilState(myCallSelector)
  const setDialogMessage = useSetRecoilState(dialogSelector)

  const {push} = useRouter()

  const handleClick = (event) => {
    event.stopPropagation();

    if (guest) {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE?.TWO_BUTTON,
        message: LOGIN_POPUP_TEXT?.TITLE,
        leftButtonProps: {text: LOGIN_POPUP_TEXT?.BUTTON_TEXT?.CLOSE},
        rightButtonProps: {
          text: LOGIN_POPUP_TEXT?.BUTTON_TEXT?.GO_LOGIN, onClick: () => {
            push(PATH_DONOTS_PAGE.AUTH.LOGIN)
          }
        },
      })
      return
    }
    setMyCall({
      type: MY_CALL.CHILDREN_INFORM
    })
    push(PATH_DONOTS_PAGE.MY.SELF)
  }

  return (
    <Box
      tabIndex={4}
      sx={{...sx}}
      onClick={(event) => handleClick(event)}
      onKeyDown={(e) => handleEnterPress(e, (event) => handleClick(event))}
    >
      {
        nickName
          ? <BabyInfoChangeTextButton nickName={nickName}/>
          : <Skeleton variant="rectangular" sx={{m: '0 auto', width: '240px', height: '16px', borderRadius: '5px'}}/>
      }
    </Box>
  )
}

function BabyInfoChangeTextButton({nickName}) {
  return (
    <Typography sx={{fontWeight: 500, fontSize: '16px', lineHeight: '16px'}}>
      {HOME_PERSONAL_COMMON.BABY.UPDATE(nickName)}
    </Typography>
  )
}
