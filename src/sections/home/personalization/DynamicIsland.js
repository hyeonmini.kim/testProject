import {Card} from "@mui/material";
import {useRecoilState} from "recoil";
import {nvlString} from "../../../utils/formatString";
import {dynamicIslandExpandSelector} from "../../../recoil/selector/home/dynamicIslandSelector";
import OpenDynamicSection from "../dynamicIsland/OpenDynamicSection";
import NotificationBell from "../../../components/home/dynamicIsland/NotificationBell";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import CloseDynamicSection from "../dynamicIsland/CloseDynamicSection";
import {getMyInfo} from "../../../api/myApi";
import {isOpenTooltipSelector} from "../../../recoil/selector/home/homeSelector";
import {useEffect} from "react";
import {getUserBabyInfo} from "../../../api/homeApi";

export default function DynamicIsland({mainRef, onClick}) {
  const [expand, setExpand] = useRecoilState(dynamicIslandExpandSelector);
  const [isTooltip, setIsTooltip] = useRecoilState(isOpenTooltipSelector)
  const {data: user} = getMyInfo()
  const {data: info} = getUserBabyInfo()

  const getBabyProfilePictureUrl = () => {
    if (!user?.babies?.length) return
    const babyKey = user?.profileSelectedBabyKey
    let baby
    user?.babies?.some((item) => {
      if (item?.key === babyKey) {
        baby = item
        return true
      }
    })
    return nvlString(baby?.profilePictureUrl)
  }

  const getBabyNickName = () => {
    if (!user?.babies?.length) return
    const babyKey = user?.profileSelectedBabyKey
    let baby
    user?.babies?.some((item) => {
      if (item?.key === babyKey) {
        baby = item
        return true
      }
    })
    return nvlString(baby?.nickname)
  }

  const handleClick = () => {
    setHackleTrack(HACKLE_TRACK.INDEX_DYNAMIC)
    if (!isTooltip)
      setExpand(!expand)
  }

  useEffect(()=>{
    if(!user) return

    if(onClick){
      onClick(user)
    }
  },[user])

  return (
    <Card
      tabIndex={1}
      sx={{
        textAlign: 'center',
        p: expand ? '' : '11px',
        boxShadow: expand ? '0px 2px 16px rgba(6, 51, 54, 0.1)' : '0px 2px 10px rgba(0, 0, 0, 0.1)',
        border: '1px solid #E9EDF4',
        borderRadius: expand ? '16px' : '50px'
      }}
      onClick={handleClick}
      onKeyDown={(e) => handleEnterPress(e, handleClick)}
    >
      {/* 다이나믹 아일랜드 Close */}
      {!expand && <CloseDynamicSection info={info} avatar={getBabyProfilePictureUrl()}/>}

      {/* 다니나믹 아일랜들 Open */}
      <OpenDynamicSection info={info} avatar={getBabyProfilePictureUrl()} nickName={getBabyNickName()} mainRef={mainRef}/>

      {/* 알림 종 아이콘 */}
      <NotificationBell/>

    </Card>
  )
}