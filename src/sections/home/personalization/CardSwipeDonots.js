import {Swiper, SwiperSlide} from "swiper/react";
import {Pagination} from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import {Avatar, Badge, Box, Container, Skeleton, Stack, Typography} from "@mui/material";
import HeaderBreadcrumbs from "../../../components/home/HeaderBreadcrumbs";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import React from "react";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {useRouter} from "next/router";
import {HOME_PERSONAL_COMMON} from "../../../constant/home/Personalization";
import TextMaxLine from "../../../components/text-max-line";
import {UserBadge} from "../../../constant/icons/UserBadge";
import {BADGE_ICON_TYPE, BadgeIcons} from "../../../constant/icons/Badge/BadgeIcons";
import {ALT_STRING} from "../../../constant/common/AltString";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import usePreRecipeCreate from "../../../hooks/usePreRecipeCreate";

export default function CardSwipeDonots({tabIndex, items, onClick, sx}) {
  const {handleRecipe} = usePreRecipeCreate()

  return (
    <Container disableGutters sx={{...sx}}>

      {/* 이달의 HOT 도낫츠 타이틀 */}
      <HeaderBreadcrumbs
        heading={HOME_PERSONAL_COMMON.HOT_DONOTS}
      />

      {/* HOT 도낫츠 스와이퍼*/}
      <Swiper
        slidesPerView={4.3}
        modules={[Pagination]}
        spaceBetween={16}
        style={{
          maxWidth: '440px',
          marginTop: '20px',
          marginLeft: '-20px',
          paddingLeft: '20px',
          marginRight: '-20px',
          paddingRight: '20px',
          backgroundColor: 'white',
        }}
      >
        {(items?.length ? items.filter(item => item?.recipe_writer_info?.recipe_writer_grade && item?.recipe_writer_info?.recipe_writer_type) : [...Array(5)]).map((item, index) =>
          item ? (
            <SwiperSlide key={index}>
              <DonotsItem item={item} tabIndex={tabIndex} onClick={onClick} index={index}/>
            </SwiperSlide>
          ) : (
            <SwiperSlide key={index}>
              <SkeletonItem/>
            </SwiperSlide>
          )
        )}
      </Swiper>

      {/* 이달의 HOT 도낫츠가 되려면? 자세히 보기 */}
      <Stack
        tabIndex={9}
        direction={'row'}
        sx={{
          justifyContent: 'space-between',
          alignItems: 'center',
          backgroundColor: '#F5F5F5',
          mt: '32px',
          p: '10px',
          borderRadius: '8px',
          cursor: 'pointer'
        }}
        onClick={handleRecipe}
        onKeyDown={(e) => handleEnterPress(e, () => handleRecipe)}
      >
        <Stack direction={'row'} sx={{alignItems: 'center'}}>
          <BadgeIcons
            type={BADGE_ICON_TYPE.BADGE_LV3_24PX}
            alt={ALT_STRING.COMMON.BADGE_LV3}
            sx={{height: '24px', width: '24px', mr: '6px'}}
          />
          <Typography variant={'b2_14_m'} sx={{color: '#222222'}}>{HOME_PERSONAL_COMMON.PROPOSE_HOT_DONOTS}</Typography>
        </Stack>
        <SvgCommonIcons alt={ALT_STRING.COMMON.BTN_MOVE} type={ICON_TYPE.ARROW_RIGHT_16PX_999999}/>
      </Stack>
    </Container>
  )
}

function DonotsItem({item, tabIndex, onClick, index}) {
  const {
    banner_link,
    recipe_writer_info,
  } = item;

  if (!banner_link || !recipe_writer_info) {
    return <SkeletonItem/>
  }
  const router = useRouter()
  const handleClick = () => {
    if (onClick) {
      onClick(index, recipe_writer_info?.recipe_writer_nickname)
    }
    router?.push(PATH_DONOTS_PAGE.MY.OTHERS(item?.banner_link))
  }

  return (
    <Stack
      tabIndex={tabIndex}
      alignItems="center"
      sx={{maxWidth: '85px'}}
      onClick={handleClick}
      onKeyDown={(e) => handleEnterPress(e, handleClick)}
    >
      {/* 아바타 */}
      {
        <WriterAvatarWithBadge
          alt={recipe_writer_info?.recipe_writer_nickname}
          avatar={recipe_writer_info?.recipe_writer_thumbnail_path}
          type={recipe_writer_info?.recipe_writer_type}
          grade={recipe_writer_info?.recipe_writer_grade}
        />
      }

      {/* 닉네임 */}
      <TextMaxLine
        open={true}
        line={1}
        sx={{mt: '12px', fontSize: '14px', fontWeight: 500, lineHeight: '20px', color: '#222222'}}
      >
        {recipe_writer_info?.recipe_writer_nickname}
      </TextMaxLine>
    </Stack>
  )
}

function SkeletonItem() {
  return (
    <Stack spacing={'8px'} alignItems={'center'}>
      <Skeleton variant='circular' width={66} height={66}/>
      <Skeleton variant='rectangular' width={'90%'} height={20} sx={{borderRadius: 2}}/>
    </Stack>
  );
}

function WriterAvatarWithBadge({alt, avatar, type, grade}) {
  return (
    <Badge
      overlap="circular"
      anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
      badgeContent={<UserBadge type={type} grade={grade} isAlt={false} sx={{maxWidth: '25px', width: 'calc(100vw/18)', zIndex: 11}}/>}
    >
      <Box>
        <Avatar
          alt={''}
          src={avatar ? avatar : '/assets/images/my/no_avatar_66.png'}
          sx={{width: '100%', height: '100%', alignItems: 'center', aspectRatio: '1/1'}}
        />
      </Box>
    </Badge>
  )
}
