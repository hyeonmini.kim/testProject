import {Box, Card, Skeleton, Stack} from '@mui/material';
import {useSetRecoilState} from "recoil";
import {searchCallSelector} from "../../../recoil/selector/recipe/searchSelector";
import {useRouter} from "next/router";
import {RECIPE_SEARCH_TYPE} from "../../../constant/recipe/Search";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import React from "react";
import {nvlString} from "../../../utils/formatString";
import {HOME_PERSONAL_COMMON} from "../../../constant/home/Personalization";
import HeaderBreadcrumbs from "../../../components/home/HeaderBreadcrumbs";
import RecipeItem from "../../../components/home/RecipeItem";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";

export default function CardPosts({month, ingredient, posts, highLightColor, onClick}) {
  const setSearchCall = useSetRecoilState(searchCallSelector);
  const {push} = useRouter();

  const handleClick = () => {
    if (ingredient) {
      setHackleTrack(HACKLE_TRACK.INDEX_SEASON_MORE)
      setSearchCall({
        type: RECIPE_SEARCH_TYPE.SEASONING,
        search: ingredient
      })
      push(PATH_DONOTS_PAGE.RECIPE.SEARCH)
    }
  }

  return (
    <>
      <HeaderBreadcrumbs
        tabIndex={11}
        heading={HOME_PERSONAL_COMMON.SEASON.TITLE(month, nvlString(ingredient))}
        sx={{mb: '25px'}}
        onClick={handleClick}
      />

      <Box sx={{mb: '30px'}}>
        {
          (posts?.length ? posts.slice(0, 2) : [...Array(2)]).map((post, index) =>
            post ? (
              <Card key={index} sx={{
                boxShadow: '0px 6px 123px rgba(174, 174, 174, 0.11), 0px 2.54258px 54.0057px rgba(174, 174, 174, 0.0625716), 0px 1.41914px 20.0513px rgba(174, 174, 174, 0.0392421)',
                borderRadius: '8px',
                p: '20px',
                border: '1px solid #E9EDF4',
                mb: '20px'
              }}>
                <RecipeItem tabIndex={10} key={post} post={post} onClick={onClick}/>
              </Card>
            ) : (
              <React.Fragment key={index}>
                <Card key={index} sx={{
                  boxShadow: '0px 6px 123px rgba(174, 174, 174, 0.11), 0px 2.54258px 54.0057px rgba(174, 174, 174, 0.0625716), 0px 1.41914px 20.0513px rgba(174, 174, 174, 0.0392421)',
                  borderRadius: '8px',
                  p: '20px',
                  border: '1px solid #E9EDF4',
                  mb: '20px'
                }}>
                  <SkeletonItem/>
                </Card>
              </React.Fragment>
            )
          )
        }
      </Box>
    </>

  );
}

function SkeletonItem() {
  return (
    <Stack spacing={'15px'} direction={'row'} alignItems={'center'}>
      <Skeleton variant="rectangular" width={80} height={80} sx={{borderRadius: 2}}/>
      <Stack direction={'column'} flexGrow={1}>
        <Skeleton variant="rectangular" height={20} sx={{my: '2px', flexGrow: 1, borderRadius: 2}}/>
        <Skeleton variant="rectangular" height={20} sx={{my: '2px', flexGrow: 1, borderRadius: 2}}/>
        <Stack direction={'row'} alignItems={'center'} sx={{mt: '6px'}}>
          <Stack alignItems={'center'} spacing={'11px'} direction={'row'}>
            <Skeleton variant="rectangular" width={80} height={20} sx={{my: '2px', borderRadius: 2}}/>
            <Skeleton variant="rectangular" width={80} height={20} sx={{my: '2px', borderRadius: 2}}/>
          </Stack>
        </Stack>
      </Stack>
    </Stack>
  );
}