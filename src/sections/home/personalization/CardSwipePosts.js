import {Swiper, SwiperSlide} from "swiper/react";
import {Pagination} from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import {Card, Container, Divider, Skeleton, Stack} from "@mui/material";
import HeaderBreadcrumbs from "../../../components/home/HeaderBreadcrumbs";
import HomePostCard from "../../../components/home/HomePostCard";

export default function CardSwipePosts({posts, title, onClick, onClickMore, tooltip, mainRef, sx}) {
  return (
    <Container disableGutters sx={{...sx}}>
      <HeaderBreadcrumbs
        tabIndex={6}
        mainRef={mainRef}
        heading={title}
        tooltip={tooltip}
        onClick={onClickMore}
      />
      <Swiper
        slidesPerView={1.3}
        modules={[Pagination]}
        spaceBetween={15}
        style={{
          paddingTop: '90px',
          marginTop: '-70px',
          marginLeft: '-20px',
          paddingLeft: '20px',
          marginRight: '-20px',
          paddingRight: '20px',
          paddingBottom: '100px',
          marginBottom: '-100px',
        }}
      >
        {(posts?.length ? posts.slice(0, 5) : [...Array(5)]).map((post, index) =>
          post ? (
            <SwiperSlide key={index}>
              <HomePostCard post={post} index={index} onClick={onClick}/>
            </SwiperSlide>
          ) : (
            <SwiperSlide key={index}>
              <SkeletonItem/>
            </SwiperSlide>
          )
        )}
      </Swiper>
    </Container>
  )
}

function SkeletonItem() {
  return (
    <Card sx={{
      p: '10px', borderRadius: '8px', border: '1px solid #F5F5F5',
      boxShadow: '0px 6px 123px rgba(174, 174, 174, 0.11), 0px 2.54258px 54.0057px rgba(174, 174, 174, 0.06), 0px 1.41914px 20.0513px rgba(174, 174, 174, 0.04)',
    }}>
      <Skeleton variant="rectangular" height={220} sx={{flexGrow: 1, borderRadius: 2, mb: '10px'}}/>
      <Stack spacing={'10px'} direction={'column'} alignItems={'center'}>
        <Skeleton variant="rectangular" width={180} height={26} sx={{borderRadius: 2}}/>
        <Skeleton variant="rectangular" width={130} height={22} sx={{borderRadius: 2}}/>
      </Stack>
      <Divider sx={{my: '11px'}}/>
      <Stack spacing={'10px'} direction={'column'} sx={{mb: '10px', alignItems: 'center'}}>
        <Stack alignItems={'center'} spacing={'11px'} direction={'row'}>
          <Skeleton variant="rectangular" width={50} height={37} sx={{borderRadius: 2}}/>
          <Skeleton variant="rectangular" width={50} height={37} sx={{borderRadius: 2}}/>
          <Skeleton variant="rectangular" width={50} height={37} sx={{borderRadius: 2}}/>
        </Stack>
      </Stack>
    </Card>
  );
}