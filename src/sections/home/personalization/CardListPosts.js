import {Box, Container, Skeleton, Stack, Typography} from '@mui/material';
import React from "react";
import HeaderBreadcrumbs from "../../../components/home/HeaderBreadcrumbs";
import RecipeItem from "../../../components/home/RecipeItem";
import {ICON_TYPE, SvgCommonIcons} from "../../../constant/icons/ImageIcons";
import {HOME_PERSONAL_COMMON} from "../../../constant/home/Personalization";
import {clickBorderNone} from "../../../constant/common/Common";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";

export default function CardListPosts({tabIndex, mainRef, title, posts, onClick, onClickMore, tooltip, sx}) {
  return (
    <Container disableGutters sx={{...sx}}>

      {/* 헤더 타이틀 */}
      <HeaderBreadcrumbs mainRef={mainRef} tabIndex={tabIndex} heading={title} tooltip={tooltip}/>

      {/* 레시피 목록 */}
      <RecipeItems tabIndex={tabIndex} posts={posts} onClick={onClick}/>

      {/* 레시피 더보기 버튼 */}
      {onClickMore && posts?.length > 1 && <RecipeMore tabIndex={tabIndex} onClick={onClickMore}/>}

    </Container>
  );
}

function RecipeItems({tabIndex, posts, onClick}) {
  return (
    <Stack spacing={'12px'} sx={{mt: '20px'}}>
      {
        (posts?.length ? posts.slice(0, 3) : [...Array(3)]).map((post, index) =>
          post ? (
            <RecipeItem tabIndex={tabIndex} key={index} post={post} onClick={onClick} index={index}/>
          ) : (
            <SkeletonItem key={index}/>
          )
        )
      }
    </Stack>
  )
}

function RecipeMore({tabIndex, onClick}) {
  return (
    <Stack
      tabIndex={tabIndex}
      direction={'row'}
      spacing={'4px'}
      onClick={onClick}
      onKeyDown={(e) => handleEnterPress(e, onClick)}
      sx={{
        mt: '30px',
        justifyContent: 'center',
        ...clickBorderNone
      }}
    >
      <Typography sx={{fontSize: '14px', fontWeight: 500, lineHeight: '14px', color: '#888888'}}>
        {HOME_PERSONAL_COMMON.RECIPE_MORE}
      </Typography>
      <SvgCommonIcons type={ICON_TYPE.ARROW_RIGHT_14PX}/>
    </Stack>
  )
}

function SkeletonItem() {
  return (
    <Stack spacing={'15px'} direction={'row'}>
      <Skeleton variant="rectangular" width={132} height={132} sx={{borderRadius: 2}}/>
      <Stack spacing={'6px'} sx={{flexGrow: 1}}>
        <Skeleton variant="rectangular" width={80} height={24} sx={{borderRadius: 2}}/>
        <Skeleton variant="rectangular" width={'90%'} height={22} sx={{borderRadius: 2}}/>
        <Box sx={{flexGrow: 1}}> </Box>

        <Stack alignItems={'center'} spacing={'6px'} direction={'row'}>
          <Skeleton variant="rectangular" width={70} height={18} sx={{borderRadius: 2}}/>
          <Skeleton variant="rectangular" width={70} height={18} sx={{borderRadius: 2}}/>
        </Stack>
        <Stack alignItems={'center'} spacing={'6px'} direction={'row'}>
          <Skeleton variant="rectangular" width={70} height={24} sx={{borderRadius: 2}}/>
          <Skeleton variant="rectangular" width={70} height={24} sx={{borderRadius: 2}}/>
        </Stack>
      </Stack>
    </Stack>
  );
}
