import {Button, Skeleton, Stack} from '@mui/material';
import {useSetRecoilState} from "recoil";
import {searchCallSelector} from "../../../recoil/selector/recipe/searchSelector";
import {useRouter} from "next/router";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {clickBorderNone} from "../../../constant/common/Common";
import {keyframes} from "@mui/system";
import * as React from "react";
import {getRandom} from "../../../utils/formatNumber";
import {HOME_PERSONAL_COMMON} from "../../../constant/home/Personalization";

export default function KeywordWave({keywords, onClick, sx}) {

  const upKeywords = keywords?.filter((item, index) => index < Math.round(keywords?.length / 2))
  const downKeywords = keywords?.filter((item, index) => index >= Math.round(keywords?.length / 2))

  return (
    <Stack spacing={'16px'} sx={{...sx, mx: '-20px', overflow: 'hidden'}}>

      {/* 상단 키워드 */}
      <Stack direction={'row'} spacing={'12px'} sx={{whiteSpace: 'nowrap'}}>
        <Marquee keywords={upKeywords} onClick={onClick} position={HOME_PERSONAL_COMMON.POSITION.TOP}/>
        <Marquee keywords={upKeywords} onClick={onClick} position={HOME_PERSONAL_COMMON.POSITION.TOP}/>
      </Stack>

      {/* 하단 키워드 */}
      <Stack direction={'row'} spacing={'12px'} sx={{whiteSpace: 'nowrap'}}>
        <Marquee reverse keywords={downKeywords} onClick={onClick} position={HOME_PERSONAL_COMMON.POSITION.DOWN}/>
        <Marquee reverse keywords={downKeywords} onClick={onClick} position={HOME_PERSONAL_COMMON.POSITION.DOWN}/>
      </Stack>

    </Stack>

  );
}

function Marquee({keywords, reverse = false, onClick, position}) {
  const setSearchCall = useSetRecoilState(searchCallSelector);
  const {push} = useRouter();

  const marquee = keyframes`
    0% {
      transform: translateX(0);
    }
    100% {
      transform: translateX(-100%);
    }
  `;

  const handleClick = (e, item, index) => {
    e.preventDefault()
    if (onClick) {
      onClick(index, item?.title, position)
    }
    setSearchCall({
      type: item?.type,
      search: item?.keyword
    })
    push(PATH_DONOTS_PAGE.RECIPE.SEARCH)
  }

  return (
    <Stack
      direction={'row'}
      spacing={'12px'}
      sx={{
        width: '180%',
        display: 'flex',
        animation: `${marquee} 20s linear infinite`,
        animationDirection: reverse ? 'reverse' : 'normal',
      }}
    >
      {(keywords?.length ? keywords : [...Array(5)]).map((item, index) => (
        item
          ? <ChipButton key={index} keyword={item?.title} onClick={(e) => handleClick(e, item, index)}/>
          : <ChipButtonSkeleton/>
      ))}
    </Stack>
  )
}

function ChipButton({keyword, onClick}) {
  return (
    <Button
      tabIndex={9}
      disableFocusRipple
      variant={'contained'}
      value={keyword}
      onClick={onClick}
      sx={{
        px: '18px',
        py: '14px',
        borderRadius: '12px',
        backgroundColor: '#FFF7EC',
        fontSize: '16px',
        fontWeight: 500,
        lineHeight: '16px',
        color: '#ECA548',
        '&:hover': {
          boxShadow: 'none',
          backgroundColor: '#FFF7EC',
        },
        ...clickBorderNone
      }}
    >
      #{keyword}
    </Button>
  )
}

function ChipButtonSkeleton() {
  const size = getRandom(100) + 70
  return (
    <Skeleton variant="rectangular" width={size} height={54} sx={{borderRadius: '12px'}}/>
  )
}