import CardListPosts from "./personalization/CardListPosts";
import DynamicIsland from "./personalization/DynamicIsland";
import CardSwipePosts from "./personalization/CardSwipePosts";
import {HOME_PERSONAL_COMMON} from "../../constant/home/Personalization";
import {getMainInfo, getRecipeCustomBasedList, getRecipeUserBodyBasedList, getSeasonIngredientBasedRecipeList} from "../../api/homeApi";
import InfoBanner from "../../components/home/banner/InfoBanner";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import KeywordWave from "./personalization/KeywordWave";
import {useRouter} from "next/router";
import {useSetRecoilState} from "recoil";
import {searchCallSelector} from "../../recoil/selector/recipe/searchSelector";
import {RECIPE_SEARCH_TYPE} from "../../constant/recipe/Search";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import CardSwipeDonots from "./personalization/CardSwipeDonots";
import * as React from "react";
import Footer from "../../layouts/common/Footer";

export default function PersonalizationSection({mainRef}) {
  const {data: user} = getRecipeUserBodyBasedList()
  const {data: custom} = getRecipeCustomBasedList()
  const {data: season} = getSeasonIngredientBasedRecipeList()
  const {data: main_info} = getMainInfo()

  const router = useRouter()
  const setSearchCall = useSetRecoilState(searchCallSelector);

  const handleUserMore = () => {
    setSearchCall({
      type: RECIPE_SEARCH_TYPE.USER,
    })
    router?.push(PATH_DONOTS_PAGE.RECIPE.SEARCH)
  }

  const handleCustomMore = () => {
    setHackleTrack(HACKLE_TRACK.INDEX_PERSONAL_MORE)
    setSearchCall({
      type: RECIPE_SEARCH_TYPE.CUSTOM,
    })
    router?.push(PATH_DONOTS_PAGE.RECIPE.SEARCH)
  }

  const handleSeasonMore = () => {
    setHackleTrack(HACKLE_TRACK.INDEX_SEASON_MORE)
    setSearchCall({
      type: RECIPE_SEARCH_TYPE.SEASONING,
      search: main_info?.season_ingredient
    })
    router?.push(PATH_DONOTS_PAGE.RECIPE.SEARCH)
  }

  const onTrackIndex = (userInfo) => {
    if (!userInfo) return

    setHackleTrack(HACKLE_TRACK.INDEX, {
      is_login: true,
      marketing_notification: userInfo?.isMarketingInfoPushNotifSet,
      name: userInfo?.nickname,
      email: userInfo?.email,
      birth: userInfo?.birthDay,
      mobile: userInfo?.phoneNumber
    })
  }

  const onTrackIndexGrow = (index, recipeId) => {
    setHackleTrack(HACKLE_TRACK.INDEX_GROW, {
      index_grow: index + 1,
      recipeid: recipeId
    })
  }

  const onTrackContents = (index, banner_type, banner_subject) => {
    setHackleTrack(HACKLE_TRACK.CONTENTS, {
      index_bn_top: index + 1,
      columeid: banner_type,
      columnname: banner_subject
    })
  }

  const onTrackIndexPersonal = (recipeId, recipeName, index) => {
    setHackleTrack(HACKLE_TRACK.INDEX_PERSONAL, {
      recipeid: recipeId,
      recipename: recipeName,
      index_personal: index + 1
    })
  }

  const onTrackIndexKeyword = (index, title, position) => {
    const property = position === HOME_PERSONAL_COMMON.POSITION.TOP ? {
      index_keword_top: index + 1,
      index_keword_top_name: title,
    } : {
      index_keword_down: index + 1,
      index_keword_down_name: title
    }
    setHackleTrack(HACKLE_TRACK.INDEX_KEYWORD, {...property})
  }

  const onTrackIndexHotDonots = (index, userName) => {
    setHackleTrack(HACKLE_TRACK.INDEX_HOT_DONOTS, {
      index_hot_donots: index + 1,
      user_name: userName,
    })
  }

  const onTrackIndexSeason = (recipeId, recipeName, index) => {
    setHackleTrack(HACKLE_TRACK.INDEX_SEASON, {
      index_season: index + 1,
      recipeid: recipeId,
      recipename: recipeName,
    })
  }

  return (
    <>
      {/* 다이나믹 아일랜드 */}
      <DynamicIsland mainRef={mainRef} onClick={onTrackIndex}/>

      {/* 성장 도움 레시피 */}
      <CardSwipePosts
        posts={user}
        title={HOME_PERSONAL_COMMON.USER.TITLE}
        onClick={onTrackIndexGrow}
        onClickMore={handleUserMore}
        sx={{mt: '40px'}}
      />

      {/* 정보성 배너 */}
      <InfoBanner
        tabIndex={7}
        showBanner
        items={main_info?.banner_info_list}
        onClick={onTrackContents}
        sx={{mt: '30px'}}
      />

      {/* 우리 아이 맞춤 레시피 */}
      <CardListPosts
        tabIndex={8}
        mainRef={mainRef}
        title={HOME_PERSONAL_COMMON.CUSTOM.TITLE}
        posts={custom}
        sx={{mt: '60px'}}
        onClick={onTrackIndexPersonal}
        onClickMore={handleCustomMore}
        tooltip={{
          contents: HOME_PERSONAL_COMMON.CUSTOM.TOOLTIP_CONTENT,
          arrow: 'home-custom'
        }}
      />

      {/* 키워드 */}
      <KeywordWave
        keywords={HOME_PERSONAL_COMMON.WEEKLY.KEYWORDS}
        onClick={onTrackIndexKeyword}
        sx={{mt: '60px'}}
      />

      {/* Hot 도낫츠 */}
      <CardSwipeDonots
        tabIndex={9}
        items={main_info?.banner_spring_list}
        onClick={onTrackIndexHotDonots}
        sx={{mt: '60px'}}
      />

      {/* 재철 재료 레시피 */}
      <CardListPosts
        tabIndex={11}
        title={HOME_PERSONAL_COMMON.SEASON.TITLE(main_info?.season_ingredient)}
        posts={season}
        onClick={onTrackIndexSeason}
        onClickMore={handleSeasonMore}
        sx={{mt: '60px'}}
      />

      {/* 이용약관, 개인정보처리방침 */}
      <Footer/>

    </>
  )
}