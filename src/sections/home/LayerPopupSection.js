import {useRecoilState, useResetRecoilState} from "recoil";
import {useEffect, useState} from "react";
import {layerPopupSelector, showLayerPopupSelector} from "../../recoil/selector/home/layerPopupSelector";
import LayerPopup from "./layerpopup/LayerPopup";

export default function LayerPopupSection() {
  const [layerPopup, setLayerPopup] = useRecoilState(layerPopupSelector)
  const resetLayerPopup = useResetRecoilState(layerPopupSelector)
  const [showLayerPopup, setShowLayerPopup] = useRecoilState(showLayerPopupSelector)
  const [newLayerPopup, setNewLayerPopup] = useState(null)

  useEffect(() => {
    if (layerPopup?.layerPopupList?.length) {
      setNewLayerPopup(layerPopup)
      setShowLayerPopup(true)
      resetLayerPopup()
    }
  }, [layerPopup])

  return (
    <LayerPopup layerPopup={newLayerPopup}/>
  );
}