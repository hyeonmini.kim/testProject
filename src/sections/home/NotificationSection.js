import {Box, Container, Fade, Skeleton, Stack, Typography} from "@mui/material";
import PropTypes from "prop-types";
import * as React from "react";
import {useEffect, useState} from "react";
import {HOME_NOTIFICATION} from "../../constant/home/Notification";
import NotificationItem from "../../components/home/notification/NotificationItem";
import {STACK} from "../../constant/common/StackNavigation";
import useStackNavigation from "../../hooks/useStackNavigation";
import {useRecoilState} from "recoil";
import {notificationHasNewItemSelector, notificationOpenSelector} from "../../recoil/selector/home/notificationSelector";
import {Z_INDEX} from "../../constant/common/ZIndex";
import {ICON_TYPE, SvgCommonIcons} from "../../constant/icons/ImageIcons";
import {MainDialogLayout} from "../../layouts/main/MainLayout";
import MainHeaderTitleLayout from "../../layouts/main/MainHeaderTitleLayout";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import CompleteDialog from "../auth/complete/CompleteDialog";
import CompleteLevelUpChocoCard from "../auth/complete/CompleteLevelUpChocoCard";
import CompleteLevelUpCrunchyKingCard from "../auth/complete/CompleteLevelUpCrunchyKingCard";
import {getMyInfo, patchMyDetailsMutate} from "../../api/myApi";
import {LEVEL_UP_CHOCO, LEVEL_UP_CRUNCH_KING} from "../../constant/dialog/Complete";
import {USER_GRADE} from "../../constant/common/User";
import {useQueryClient} from "react-query";
import LoadingScreen from "../../components/common/LoadingScreen";
import useNativeBackKey from "../../hooks/useNativeBackKey";

NotificationSection.propTypes = {
  open: PropTypes.bool,
  onBack: PropTypes.func,
};

export default function NotificationSection({items, onBack}) {
  const [showNotification, setShowNotification] = useRecoilState(notificationOpenSelector)
  const [hasNewItem, setHasNewItem] = useRecoilState(notificationHasNewItemSelector)
  const [hasNotification, setHasNotification] = useState(undefined)

  useEffect(() => {
    if (!items) {
      setHasNotification(false)
    } else if (items?.new_noti_list.length) {
      setHasNotification(true)
    } else if (items?.today_noti_list.length) {
      setHasNotification(true)
    } else if (items?.yesterday_noti_list.length) {
      setHasNotification(true)
    } else if (items?.before7_noti_list.length) {
      setHasNotification(true)
    } else if (items?.cur_month_noti_list.length) {
      setHasNotification(true)
    } else if (items?.before_noti_list.length) {
      setHasNotification(true)
    } else if (items?.before2_noti_list.length) {
      setHasNotification(true)
    }
  }, [items])

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.ALARM)
  }, [])

  return (
    <MainDialogLayout
      fullScreen
      open={showNotification}
      TransitionComponent={Fade}
      sx={{zIndex: Z_INDEX.DIALOG, '& .MuiDialog-paper': {mx: 0}}}
    >
      <MainHeaderTitleLayout
        title={HOME_NOTIFICATION.TITLE}
        onBack={onBack}
      >
        <Container disableGutters maxWidth={'xs'} sx={{position: 'relative', px: '20px'}}>

          {/* 알림 리스트 */}
          {
            (hasNotification || hasNotification === undefined)
              ? <NotificationItemsSection items={items} setShowNoti={setShowNotification}/>
              : <NotificationNotFound/>
          }
        </Container>
      </MainHeaderTitleLayout>
    </MainDialogLayout>
  )
}

function NotificationItemsSection({items, setShowNoti}) {
  const [showLevelUpChoco, setShowLevelUpChoco] = useState(false)
  const [showLevelUpCrunchyKing, setShowLevelUpCrunchyKing] = useState(false)

  const {mutate: mutateMyDetails, isLoading: isLoadingMyDetails} = patchMyDetailsMutate()
  const {data: user, isLoading: isLoadingGetMyInfo} = getMyInfo()

  const [snsUrl, setSnsUrl] = useState(user?.socialMediaUrl ? user?.socialMediaUrl : '')

  const curSnsUrl = user?.socialMediaUrl ? user?.socialMediaUrl : ''

  const queryClient = useQueryClient()

  const levelUpChocoContents = {
    grade: USER_GRADE.LV2.GRADE,
    oneButton: {
      text: LEVEL_UP_CHOCO.BUTTON_TEXT,
      onClick: (e) => {
        e.stopPropagation()
        setShowLevelUpChoco(false)
      }
    }
  }

  const levelUpCrunchyKingContents = {
    grade: USER_GRADE.LV3.GRADE,
    oneButton: {
      text: LEVEL_UP_CRUNCH_KING.LEFT_BUTTON_TEXT,
      onClick: (e) => {
        e.stopPropagation()
        setSnsUrl(curSnsUrl)
        setShowLevelUpCrunchyKing(false)
      }
    },
    twoButton: {
      text: LEVEL_UP_CRUNCH_KING.RIGHT_BUTTON_TEXT,
      disabled: !!!snsUrl,
      onClick: (e) => {
        e.stopPropagation()
        const param = {
          socialMediaUrl: snsUrl
        }
        mutateMyDetails(param, {
          onSuccess: () => {
            queryClient.invalidateQueries('getMyInfo')
            setShowLevelUpCrunchyKing(false)
          }
        })
      }
    }
  }

  const {navigation} = useStackNavigation()
  const handleClick = (path) => {
    navigation.push(STACK.HOME.TYPE, path, {
      ...STACK.HOME.DATA,
      open: true,
    })
  }

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      if (showLevelUpChoco) {
        setShowLevelUpChoco(false)
      } else if (showLevelUpCrunchyKing) {
        setShowLevelUpCrunchyKing(false)
      } else {
        setShowNoti(false)
      }
    }
  }, [handleBackKey]);

  return (
    <>
      <NotificationItems title={HOME_NOTIFICATION.NEW} items={items?.new_noti_list} isNew
                         onClick={handleClick} setShowChoco={setShowLevelUpChoco} setShowCrunchyKing={setShowLevelUpCrunchyKing}/>
      <NotificationItems title={HOME_NOTIFICATION.TODAY} items={items?.today_noti_list}
                         onClick={handleClick} setShowChoco={setShowLevelUpChoco} setShowCrunchyKing={setShowLevelUpCrunchyKing}/>
      <NotificationItems title={HOME_NOTIFICATION.YESTERDAY} items={items?.yesterday_noti_list}
                         onClick={handleClick} setShowChoco={setShowLevelUpChoco} setShowCrunchyKing={setShowLevelUpCrunchyKing}/>
      <NotificationItems title={HOME_NOTIFICATION.THIS_WEEK} items={items?.before7_noti_list}
                         onClick={handleClick} setShowChoco={setShowLevelUpChoco} setShowCrunchyKing={setShowLevelUpCrunchyKing}/>
      <NotificationItems title={HOME_NOTIFICATION.THIS_MONTH} items={items?.cur_month_noti_list}
                         onClick={handleClick} setShowChoco={setShowLevelUpChoco} setShowCrunchyKing={setShowLevelUpCrunchyKing}/>
      <NotificationItems
        title={items?.before_month ? items.before_month : HOME_NOTIFICATION.BEFORE_MONTH}
        items={items?.before_noti_list}
        onClick={handleClick} setShowChoco={setShowLevelUpChoco} setShowCrunchyKing={setShowLevelUpCrunchyKing}/>
      <NotificationItems
        title={items?.before2_month ? items?.before2_month : HOME_NOTIFICATION.BEFORE2_MONTH}
        items={items?.before2_noti_list}
        onClick={handleClick} setShowChoco={setShowLevelUpChoco} setShowCrunchyKing={setShowLevelUpCrunchyKing}/>

      {/* 등급업 다이얼로그 - 초코 */}
      {showLevelUpChoco &&
        <CompleteDialog isOpen={showLevelUpChoco} title={LEVEL_UP_CHOCO.TITLE} {...levelUpChocoContents}>
          <CompleteLevelUpChocoCard/>
        </CompleteDialog>}

      {/* 등급업 다이얼로그 - 크런치킹 */}
      {showLevelUpCrunchyKing &&
        <CompleteDialog isOpen={showLevelUpCrunchyKing} title={LEVEL_UP_CRUNCH_KING.TITLE} {...levelUpCrunchyKingContents}>
          <CompleteLevelUpCrunchyKingCard snsUrl={snsUrl} setSnsUrl={setSnsUrl}/>
        </CompleteDialog>}

      {(isLoadingGetMyInfo || isLoadingMyDetails) && <LoadingScreen/>}
    </>
  )
}

function NotificationItems({title, items, isNew = false, onClick, setShowChoco, setShowCrunchyKing}) {
  if ((!items || !items.length)) return

  return (
    <>
      <Box sx={{mt: '20px', display: 'flex', alignItems: 'center', height: '24px'}}>
        <Typography sx={{fontSize: '16px', fontWeight: '500', lineHeight: '24px', color: '#000000'}}>
          {title}
        </Typography>
        {isNew && <SvgCommonIcons type={ICON_TYPE.NEW_ITEM_DOT} sx={{ml: '5px', mt: '2px'}}/>}
      </Box>
      <Stack sx={{mt: '12px', rowGap: '15px'}}>
        {
          (items?.length ? items : [...Array(10)]).map((item, index) =>
            item ? (
              <NotificationItem
                key={index} item={item}
                isNew onClick={onClick}
                setShowChoco={setShowChoco}
                setShowCrunchyKing={setShowCrunchyKing}
              />
            ) : (
              <SkeletonItem key={index}/>
            ))
        }
      </Stack>
    </>
  )
}

function SkeletonItem() {
  return (
    <Box sx={{
      flexDirection: 'row',
      height: '50px',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      columnGap: '20px'
    }}
    >
      <Skeleton variant="circular" sx={{width: 40, height: 40}}/>
      <Stack rowGap={'2px'} sx={{flexGrow: 1}}>
        <Skeleton variant="rectangular" height={20} sx={{borderRadius: 1}}/>
        <Skeleton variant="rectangular" height={20} sx={{borderRadius: 1}}/>
      </Stack>
      <Skeleton variant="rectangular" width={50} height={50} sx={{borderRadius: 2}}/>
    </Box>
  )
}

function NotificationNotFound() {
  return (
    <Box
      sx={{
        height: 'calc(100vh - 220px)',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <Typography align={'center'} sx={{fontSize: '16px', fontWeight: 500, color: '#666666', lineHeight: '24px'}}>
        {HOME_NOTIFICATION.NO_SEARCH_NOTICE_TITLE}
      </Typography>
    </Box>
  )
}
