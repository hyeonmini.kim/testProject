import {Container, Dialog, DialogContent, Fade, Skeleton, Stack, Typography} from '@mui/material';
import {useRecoilState} from "recoil";
import {Z_INDEX} from "../../../constant/common/ZIndex";
import {sxFixedCenterMainLayout} from "../../../layouts/main/MainLayout";
import Image from "../../../components/image"
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import Carousel, {CarouselDotsDetail} from "../../../components/recipe/detail/carousel";
import {layerPopupClickedSelector, showLayerPopupSelector} from "../../../recoil/selector/home/layerPopupSelector";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";
import useRedirection, {INFLOW_TYPE, REDIRECTION_TYPE} from "../../../hooks/useRedirection";
import {iFrameUrlSelector} from "../../../recoil/selector/common/iFrameSelector";
import {useRouter} from "next/router";

export default function LayerPopup({layerPopup}) {
  const [showLayerPopup, setShowLayerPopup] = useRecoilState(showLayerPopupSelector)
  const [layerPopupClicked, setLayerPopupClicked] = useRecoilState(layerPopupClickedSelector)
  const [iframeUrl, setIframeUrl] = useRecoilState(iFrameUrlSelector)
  const [isDragging, setIsDragging] = useState(false)

  const {push} = useRouter();
  const carouselRef = useRef(null);

  const handleClose = () => {
    setShowLayerPopup(false)
  }

  const {isRedirection} = useRedirection()
  const handleImageClick = (url) => {
    if (!isDragging) {
      const redirection = isRedirection(INFLOW_TYPE.POPUP, url)
      if (redirection?.type === REDIRECTION_TYPE.D_IFRAME) {
        setIframeUrl(redirection?.url)
      } else if (redirection?.url) {
        push(redirection?.url)
      }
      setShowLayerPopup(false)
      setLayerPopupClicked(true)
    }
    setIsDragging(false)
  }

  const carouselSettings = {
    dots: true,
    arrows: false,
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    edgeFriction: 0,
    // initialSlide: slideIndex,
    beforeChange: (index, nextIndex) => {
      setIsDragging(true)
    },
    afterChange: (index) => {
      // setSlideIndex(index)
      setIsDragging(false)
      // setInfiniteBool(false)
    },
    // rtl: Boolean(theme.direction === 'rtl'),
    ...CarouselDotsDetail({
      rounded: false,
      sx: {
        bottom: 0,
        zIndex: 10,
        width: '100%',
        display: 'flex',
        position: 'absolute',
        marginBottom: '-30px',
        alignItems: 'flex-end',
      },
    }),
  };

  useEffect(() => {
    setTimeout(() => {
      setIsDragging(false)
    }, 500)
  }, [])

  return (
    <Dialog
      fullWidth
      maxWidth={'xs'}
      open={showLayerPopup}
      onClose={(event, reason) => {
        if (reason !== 'backdropClick') {
          handleClose()
        }
      }}
      TransitionComponent={Fade}
      PaperProps={{
        style: {
          paddingLeft: '30px',
          paddingRight: '30px',
          overflowY: 'initial',
          background: 'transparent',
          boxShadow: 'none'
        }
      }}
      sx={{
        zIndex: Z_INDEX.DIALOG,
        '& .MuiDialog-paper .MuiDialogActions-root': {
          padding: '20px',
        },
        '& .MuiBackdrop-root ': {
          background: 'rgba(0,0,0,0.7)',
        },
        '& .MuiPaper-root ': {
          borderRadius: '8px',
        },
        ...sxFixedCenterMainLayout
      }}
    >
      <Stack direction="row" justifyContent="space-between" alignItems="center" sx={{mb: '20px'}}>
        <DialogContent
          tabIndex={0}
          sx={{p: 0, overflowY: 'initial', cursor: 'pointer', flex: 'none'}}
          onKeyDown={(e) => handleEnterPress(e, layerPopup?.handleDialogPaper?.onClick)}
          onClick={layerPopup?.handleDialogPaper?.onClick}
        >
          <Typography sx={{
            whiteSpace: 'pre-line',
            textAlign: 'left',
            fontSize: '15px',
            fontWeight: '400',
            lineHeight: '20px',
            color: 'white',
            // left: '30px',
          }}>
            {layerPopup?.handleDialogPaper?.text}
          </Typography>
        </DialogContent>

        <DialogContent
          tabIndex={0}
          sx={{p: 0, overflowY: 'initial', cursor: 'pointer', flex: 'none'}}
          onKeyDown={(e) => handleEnterPress(e, layerPopup?.handleDialogPaper2?.onClick)}
          onClick={layerPopup?.handleDialogPaper2?.onClick}
        >
          <Typography sx={{
            whiteSpace: 'pre-line',
            textAlign: 'right',
            fontSize: '15px',
            fontWeight: '400',
            lineHeight: '20px',
            color: 'white',
            // right: '30px',
          }}>
            {layerPopup?.handleDialogPaper2?.text}
          </Typography>
        </DialogContent>
      </Stack>

      <DialogContent sx={{p: 0, overflowY: 'initial', cursor: 'pointer'}}>
        <Container maxWidth={'xs'} disableGutters
                   sx={{height: '100%', position: 'relative', zIndex: 99, m: 'auto'}}>
          <Carousel ref={carouselRef} {...carouselSettings}>
            {(!layerPopup?.layerPopupList?.length ? [...Array(5)] : layerPopup?.layerPopupList)?.map((item, index) =>
              item ? (
                <Image
                  alt={item?.popup_subject}
                  key={index}
                  src={item?.popup_url}
                  onClick={() => handleImageClick(item?.popup_link)}
                  sx={{borderRadius: '8px'}}
                  isLazy={false}
                  ratio="15/19"
                />
              ) : (
                <Skeleton variant="rectangular" width="100%" sx={{height: 380, borderRadius: '8px'}} key={index}/>
              ))}
          </Carousel>
        </Container>
      </DialogContent>
    </Dialog>
  );
}
