import {nvlString} from "../utils/formatString";
import {nvlNumber} from "../utils/formatNumber";
import {ENV_TYPE} from "../constant/common/Env";
import {getEnv} from "../utils/envUtils";

function donotsUrlPath(env, path) {
  let host = window.location.origin
  return `${host}${path}`
}

function recipeLabPath(env, path) {
  let host = process.env.RECIPE_PRD_API_URL
  switch (env) {
    case ENV_TYPE.PRODUCT:
      host = process.env.RECIPE_PRD_API_URL
      break;
    case ENV_TYPE.STAGING:
      host = process.env.RECIPE_STG_API_URL
      break;
    case ENV_TYPE.DEVELOP:
      host = process.env.RECIPE_DEV_API_URL
      break;
    case ENV_TYPE.LOCAL:
      host = process.env.RECIPE_LOC_API_URL
      break;
  }
  return `${host}${path}`;
}

function recipeApiPath(env, path) {
  let host = process.env.RECIPE_PRD_API_URL
  switch (env) {
    case ENV_TYPE.PRODUCT:
      host = process.env.RECIPE_PRD_API_URL
      break;
    case ENV_TYPE.STAGING:
      host = process.env.RECIPE_STG_API_URL
      break;
    case ENV_TYPE.DEVELOP:
      host = process.env.RECIPE_DEV_API_URL
      break;
    case ENV_TYPE.LOCAL:
      host = process.env.RECIPE_LOC_API_URL
      break;
  }
  return `${host}${path}`;
}

function memberApiPath(env, path) {
  let host = process.env.MEMBER_PRD_API_URL
  switch (env) {
    case ENV_TYPE.PRODUCT:
      host = process.env.MEMBER_PRD_API_URL
      break;
    case ENV_TYPE.STAGING:
      host = process.env.MEMBER_STG_API_URL
      break;
    case ENV_TYPE.DEVELOP:
      host = process.env.MEMBER_DEV_API_URL
      break;
    case ENV_TYPE.LOCAL:
      host = process.env.MEMBER_LOC_API_URL
      break;
  }
  return `${host}${path}`;
}

const staticCFPath = (path, env = getEnv()) => {
  let host = process.env.STATIC_PRD_CF_URL
  switch (env) {
    case ENV_TYPE.PRODUCT:
      host = process.env.STATIC_PRD_CF_URL
      break;
    case ENV_TYPE.STAGING:
      host = process.env.STATIC_STG_CF_URL
      break;
    case ENV_TYPE.DEVELOP:
      host = process.env.STATIC_DEV_CF_URL
      break;
    case ENV_TYPE.LOCAL:
      host = process.env.STATIC_LOC_CF_URL
      break;
  }
  return `${host}${path}`;
}


export const PATH_DONOTS_STATIC_PAGE = {
  MEMBERSHIP: () => staticCFPath('/membership/membership.html'),
  MEMBERSHIP_HOT_DONOTS: (env) => staticCFPath('/membership/membership.html?d_focus=true', env),
  FAQ: () => staticCFPath('/faq/faq.html'),
  SOURCE: () => staticCFPath('/faq/source.html'),
  ICONS: (path, env) => staticCFPath(path, env),
}

export const PATH_DONOTS_STORE_URL = {
  ANDROID: 'https://play.google.com/store/apps/details?id=com.kyobo.android.donots',
  IOS: 'https://apps.apple.com/kr/app/%EA%B5%90%EB%B3%B4%EC%83%9D%EB%AA%85/id6446616569'
}

export const PATH_DONOTS_API = {
  HOME: {
    BODY: (env) => recipeApiPath(env, '/v1/recipe/main/recipeUserBodyBasedList/N'),
    CUSTOM: (env) => recipeApiPath(env, '/v1/recipe/main/recipeCustomBasedList/N'),
    SEASONING: (env) => recipeApiPath(env, '/v1/recipe/main/seasonIngredientBasedRecipeList/N'),
    MAIN_INFO: (env) => recipeApiPath(env, '/v1/recipe/main/mainInfo'),
    NOTI_LIST: (env) => recipeApiPath(env, '/v1/recipe/main/selectNotiList'),
    NOTI_READ: (env, param) => recipeApiPath(env, `/v1/recipe/main/updateNotiRead/${param?.noti_key}`),
    NOTI_READ_ALL: (env, param) => recipeApiPath(env, `/v1/recipe/main/updateNotiReadAll`),
    BABY_INFO: (env) => recipeApiPath(env, '/v1/recipe/main/userBabyInfo'),
    POPUP_LIST: (env) => recipeApiPath(env, '/v1/recipe/main/popupList'),
  },
  GUEST: {
    BODY: (env, param) => recipeApiPath(env, `/v1/recipe/main/noUserBodyBasedRecipeList/${param?.birthday}/${param?.gender}/${param?.height}/${param?.weight}`),
    ALLERGY: (env) => recipeApiPath(env, '/v1/recipe/main/noUserAllergyBasedRecipeList'),
    HEALTH: (env) => recipeApiPath(env, '/v1/recipe/main/noUserHealthBasedRecipeList'),
  },
  RECIPE: {
    TEMP_CHECK: (env) => recipeApiPath(env, '/v1/recipe/registration/checkRecipeTempSave'),
    TEMP_CREATE: (env) => recipeApiPath(env, '/v1/recipe/registration/recipeRegist'),
    TEMP_DELETE: (env) => recipeApiPath(env, '/v1/recipe/registration/deleteRecipeTempSave'),
    UPLOAD: (env, param) => recipeApiPath(env, `/v1/recipe/registration/recipeUpload/${param?.recipe_key}`),
    IMAGE_UPLOAD: (env) => recipeApiPath(env, '/v1/recipe/registration/recipeImageUpload'),
    UPDATE_STATUS: (env, param) => recipeApiPath(env, `/v1/recipe/registration/updateRecipeStatusTempSave/${param?.recipe_key}`),
  },
  SEARCH: {
    BODY: (env) => recipeApiPath(env, `/v1/recipe/main/recipeUserBodyBasedList/Y`),
    CUSTOM: (env) => recipeApiPath(env, `/v1/recipe/main/recipeCustomBasedList/Y`),
    SEASONING: (env) => recipeApiPath(env, `/v1/recipe/main/seasonIngredientBasedRecipeList/Y`),
    RECIPE: (env) => recipeApiPath(env, `/v1/recipe/search/searchRecipe`),
    INGREDIENT: (env) => recipeApiPath(env, `/v1/recipe/registration/listRecipeIngredient`),
    BASE_INGREDIENT: (env, base_type) => recipeApiPath(env, `/v1/recipe/registration/listRecipeBaseIngredient/${base_type}`),
  },
  RANKING: {
    SPRINKLE: (env) => memberApiPath(env, `/v1/members/NORMAL_MEMBER/todays-member-ranking`),
    EXPERT: (env) => memberApiPath(env, `/v1/members/EXPERT/todays-member-ranking`),
  },
  DETAIL: {
    RECIPE_ALL: (env) => recipeApiPath(env, '/v1/recipe/main/recipeAll'),
    RECIPE_DETAIL: (env, param) => recipeApiPath(env, `/v1/recipe/details/recipeDetail/${param?.recipe_key}`),
    REGIST_RECIPE_SCRAP: (env) => recipeApiPath(env, '/v1/recipe/details/registRecipeScrap'),
    REGIST_RECIPE_REVIEW: (env) => recipeApiPath(env, '/v1/recipe/details/registRecipeReview'),
  },
  MY: {
    SEARCH_USER_RECIPE: (env) => recipeApiPath(env, '/v1/recipe/search/searchUserRecipe'),
    MY_INFO: (env) => memberApiPath(env, `/v1/members/for-mypage-entry`),
    CHECK_GRADE: (env) => memberApiPath(env, '/v1/parent/grade-info'),
    OTHERS_INFO: (env, param) => memberApiPath(env, `/v1/members/${param?.key}/for-others`),
    PATCH_MY_DETAIL_INFO: (env) => memberApiPath(env, `/v1/members/details`),
    MODIFY_MY_PROFILE_PICTURE: (env) => memberApiPath(env, `/v1/members/profile-picture`),
    PATCH_CHILDREN_DETAIL_INFO: (env, param) => memberApiPath(env, `/v1/babies/${param?.key}/details`),
    MODIFY_CHILDREN_PROFILE_PICTURE: (env, param) => memberApiPath(env, `/v1/babies/${param?.key}/profile-picture`),
    PATCH_CHILDREN_ALLERGY_INGREDIENTS_INFO: (env, param) => memberApiPath(env, `/v1/babies/${param?.key}/allergy-ingredients`),
    PATCH_CHILDREN_CONCERNS_INFO: (env, param) => memberApiPath(env, `/v1/babies/${param?.key}/concerns`),
    POST_CHILDREN_REARRANGE: (env) => memberApiPath(env, `/v1/members/babies/rearrange-profile-picture-thumbnail-order`),
    POST_CHILDREN_REMOVE: (env, param) => memberApiPath(env, `/v1/members/babies/${param?.key}/remove`),
    DELETE_SNS_ACCOUNT: (env, param) => memberApiPath(env, `/v1/account/setting/mySocial/${param?.socialAccountConnectionStatusKey}`),
    SNS_LIST: (env) => memberApiPath(env, '/v1/account/setting/mySocial'),
    ADD_CHILDREN: (env) => memberApiPath(env, `/v1/members/babies`),
    CHANGE_PASSWORD: (env) => memberApiPath(env, '/v1/noAuth/id/change/password'),
    DELETE_ACCOUNT: (env, param) => memberApiPath(env, `/v1/account/membership/withdrawal/${param?.accountKey}`),
  },
  SETTING: {
    NOTICE: (env, param) => memberApiPath(env, `/noAuth/v1/notice/post`),
    NOTICE_DETAIL: (env, param) => memberApiPath(env, `/noAuth/v1/notice/post/${param?.noticePostKey}`),
    FAQ: (env, param) => memberApiPath(env, `/v1/faq-posts/by-category/${param?.category}`),
    FAQ_DETAIL: (env, param) => memberApiPath(env, `/v1/faq-posts/${param?.key}`),
    TERMS_OF_USE: (env) => memberApiPath(env, '/v1/terms-of-services'),
    TERMS_OF_USE_KEY: (env, param) => memberApiPath(env, `/v1/terms-of-services/${param?.key}`),
    TERMS_OF_USE_TITLE: (env, param) => memberApiPath(env, `/v1/terms-of-services/by-title/${param?.title}`),
    AGREE_MKT: (env) => memberApiPath(env, `/v1/members/is-terms-collecting-personal-data-marketing-agreed`),
    PUSH_MKT: (env) => memberApiPath(env, `/v1/members/is-marketing-info-push-notif-set`),
    PUSH_CONTENTS: (env) => memberApiPath(env, `/v1/members/is-post-censorship-result-push-notif-set`),
    ONE_ON_ONE_INQUIRIES: (env) => memberApiPath(env, `/v1/one-on-one-inquiry-posts/mine`),
    ONE_ON_ONE_INQUIRY_DETAIL: (env, param) => memberApiPath(env, `/v1/one-on-one-inquiry-posts/${param?.key}`),
    DELETE_ONE_ON_ONE_INQUIRY: (env, param) => memberApiPath(env, `/v1/one-on-one-inquiry-posts/${param?.key}`),
    POST_ONE_ON_ONE_INQUIRY: (env, param) => memberApiPath(env, `/v1/one-on-one-inquiry-posts`),
  },
  AUTH: {
    GET_MEMBER: (env) => memberApiPath(env, '/v1/account/getMember'),
    SIGN_UP_SNS: (env) => memberApiPath(env, '/v1/account/sns/signUp'),
    ADD_SNS_ACCOUNT: (env) => memberApiPath(env, '/v1/account/sns/signUpAdd'),
    REFRESH_TOKEN: (env) => memberApiPath(env, '/token/v1/refreshToken'),
    REFRESH_TOKEN_COOKIE: (env) => memberApiPath(env, '/token/v1/refreshToken/cookie'),
    GET_UUID: (env) => memberApiPath(env, '/token/v1/uuid/publish'),
    GET_TOKENS: (env) => memberApiPath(env, '/token/v1/token/receive'),
    SIGN_IN_ID: (env) => memberApiPath(env, '/v1/noAuth/id/signIn'),
    GET_AUTH_CODE: (env) => memberApiPath(env, '/v1/identity/certified/smsSend'),
    CHECK_AUTH_CODE: (env, param) => memberApiPath(env, `/v1/identity/certified/match/${param.joiningAccountType}`),
    CHECK_EXIST_ID: (env, param) => memberApiPath(env, `/v1/noAuth/id/verification/${param?.id}`),
    RESET_PASSWORD: (env) => memberApiPath(env, '/v1/noAuth/id/reset/password'),
    IS_DUPLICATE_ID: (env, param) => memberApiPath(env, `/v1/noAuth/id/duplication/${param?.id}`),
    IS_DUPLICATE_NICKNAME: (env, param) => memberApiPath(env, `/v1/members/nickname/${param?.nickname}/is-duplicate`),
    IS_DUPLICATE_EMAIL: (env, param) => memberApiPath(env, `/v1/members/email/${param?.email}/is-duplicate`),
    SIGN_UP_ID: (env) => memberApiPath(env, '/v1/noAuth/id/signUp'),
    SIGN_UP_ADD_ID: (env) => memberApiPath(env, '/v1/noAuth/sns/signUpAdd/id'),
    FIND_ID: (env) => memberApiPath(env, `/v1/noAuth/id/find`),
    IS_EXIST_ACCOUNT: (env, param) => memberApiPath(env, `/v1/noAuth/ci/verification/${param?.ci}`),
    CHECK_ID_CI: (env) => memberApiPath(env, '/v1/noAuth/ci/ciAndIdVerification'),
    BY_EMAIL_IDENTIFY_VERIFICATION_QUESTIONS: (env) => memberApiPath(env, '/v1/noAuth/accounts/by-email/identity-verification-questions'),
    BY_ID_IDENTIFY_VERIFICATION_QUESTIONS: (env) => memberApiPath(env, '/v1/noAuth/accounts/by-id/identity-verification-questions'),
    BY_EMAIL_AND_IDENTIFY_VERIFICATION_QAS: (env) => memberApiPath(env, '/v1/noAuth/accounts/by-email-and-identity-verification-qas/id'),
    VERIFY_IDENTIFY_WITH_ID_AND_QAS: (env) => memberApiPath(env, '/v1/noAuth/accounts/verify-identity/with-id-and-qas'),
    SNS_CALLBACK: (env) => donotsUrlPath(env, '/auth/callback'),
  },
  LAB: {
    SURVEY_REGIST: (env) => recipeLabPath(env, '/v1/lab/survey/registSurvey'),
    SURVEY_REGIST_YN: (env, param) => recipeLabPath(env, `/v1/lab/survey/registSurveyYn/${param?.survey_type}`),
    SURVEY_LIST: (env, param) => recipeLabPath(env, `/v1/lab/survey/surveyList/${param?.survey_type}`),
    POST_REGIST: (env) => recipeLabPath(env, '/v1/lab/board/registPost'),
    POST_UPDATE: (env) => recipeLabPath(env, '/v1/lab/board/updatePost'),
    POST_LIST: (env) => recipeLabPath(env, '/v1/lab/board/postList'),
    POST_LIKE: (env) => recipeLabPath(env, '/v1/lab/board/updateGreat'),
    POST_DECLARE: (env) => recipeLabPath(env, '/v1/lab/board/updateDeclare'),
    POST_DETAIL: (env, param) => recipeLabPath(env, `/v1/lab/board/postDetail/${param?.post_key}`),
    COMMENT_REGIST: (env) => recipeLabPath(env, '/v1/lab/board/registComment'),
    COMMENT_UPDATE: (env) => recipeLabPath(env, '/v1/lab/board/updateComment'),
    COMMENT_LIST: (env) => recipeLabPath(env, '/v1/lab/board/commentList'),
    POLL_REGIST_YN: (env) => recipeLabPath(env, '/v1/lab/board/registPollYn'),
    POLL_REGIST: (env) => recipeLabPath(env, '/v1/lab/board/registPoll'),
  }
}

export const PATH_DONOTS_PAGE = {
  MAIN: '/main',
  HOME: '/home',
  GUEST: '/guest',
  RANKING: '/ranking',
  RECIPE: {
    SEARCH: '/recipes/search',
    CREATE: '/recipes/create',
    DETAIL: (name, title, id) => `/recipes/detail/@${nvlString(name)}/${nvlString(title)}/${nvlNumber(id)}`,
  },
  AUTH: {
    LOGIN: '/auth/login',
    SIGNUP: '/auth/sign-up',
    CONFIRM: '/auth/confirm-sign-up',
    SNS: (env, param) => memberApiPath(env, `/oauth2/authorization/${param?.type}?redirect_uri=${param?.redirectUrl}&uuid=${param?.uuid}`),
  },
  MY: {
    SELF: `/my/self`,
    OTHERS: (user_key) => `/my/others?user_key=${nvlString(user_key)}`,
    EXPERTS: (user_key) => `/my/experts?user_key=${nvlString(user_key)}`,
    SETTING: '/my/setting',
    NOTICE: (notice) => `/my/setting?d_notice=${notice}`,
    FAQ: (faq) => `/my/setting?d_faq=${faq}`,
    ADD_CHILDREN: '/my/add-children',
  },
  LAB: {
    ALLERGY: (env) => staticCFPath('/info_banner/info_banner_allergy.html', env),
    BABY_FOOD: '/lab/babyfood',
    NUTRITION: '/lab/nutrition',
    GUEST: '/static/lab/index.html',
    COMMENT_DETAIL: (post, comment) => {
      if (comment) {
        return `/lab?d_post=${post}&d_comment=${comment}`
      }
      return `/lab?d_post=${post}`
    }
  },
  ERROR: {
    PAGE_NOT_FOUND: '/404',
    INTERNAL_SERVER: '/500',
  }
}
