import {PATH_DONOTS_PAGE} from '../routes/paths';
import {isNative} from "../utils/envUtils";
import {callOpenNativeSetTokensChannel} from "../channels/permissionChannel";
import {setLocalStorage} from "../utils/storageUtils";
import {NO_TOKEN_MSG} from "../constant/sign-up/SignUp";
import {logger} from "../utils/loggingUtils";

function jwtDecode(token) {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  const jsonPayload = decodeURIComponent(
    window
      .atob(base64)
      .split('')
      .map((c) => `%${`00${c.charCodeAt(0).toString(16)}`.slice(-2)}`)
      .join('')
  );

  return JSON.parse(jsonPayload);
}

const isValidToken = (token) => {
  if (!token || token === NO_TOKEN_MSG || token === 'null') {
    logger.dump("isValidToken", "token is null")
    return false
  }
  const decoded = jwtDecode(token)
  const currentTime = parseInt(Date.now().toString().substring(0, 10))
  logger.dump("isValidToken", {
    currentTime: currentTime,
    exp: decoded?.exp
  })
  return decoded?.exp > currentTime
}

export const isValidTokens = async (tokens, isMock = false) => {
  if (isMock) {
    return {isValid: true, isUpdateTokens: false}
  }

  // access token 검증
  if (isValidToken(tokens?.accessToken)) {
    logger.dump("isValidTokens", "valid AccessTokens")
    // access token 유효, 유저 프로파일 가져오기 진행
    return {isValid: true, isUpdateTokens: false}
  } else {
    // access token 만료, refresh token 검증
    if (isValidToken(tokens?.refreshToken)) {
      logger.dump("isValidTokens", "valid RefreshTokens")
      // refresh token 유효, access token 재발급
      return {isValid: true, isUpdateTokens: true}
    } else {
      // refresh token 만료, 메인으로 이동
      logger.dump("isValidTokens", "invalid Access/RefreshTokens")
      return {isValid: false, isUpdateTokens: false}
    }
  }
};

export const checkExpired = (exp) => {
  let expiredTimer;
  const currentTime = Date.now();
  const timeLeft = exp - currentTime;

  clearTimeout(expiredTimer);

  expiredTimer = setTimeout(() => {
    alert('인증 정보가 만료되었습니다. 다시 로그인해 주세요.')
    localStorage.removeItem('accessToken')
    window.location.href = PATH_DONOTS_PAGE.AUTH.LOGIN
  }, timeLeft)
}

export const setSession = (accessToken, isMock = false) => {
  if (accessToken) {
    const {exp} = jwtDecode(accessToken);

    if (isMock) {
      return
    }
    checkExpired(exp);
  } else {
    if (!isNative()) {
      setLocalStorage('webSignIn', false)
    }
  }
};

export const setTokens = (accessToken, refreshToken) => {
  if (isNative()) {
    try {
      callOpenNativeSetTokensChannel(accessToken, refreshToken)
    } catch {
      // TODO. 토큰 저장 실패 시 다시 로그인해라 by 끼룩 at 2022.02.20 13:38
    }
  } else {
    setLocalStorage('webSignIn', true)
  }
}

export const resetTokens = () => {
  if (isNative()) {
    try {
      callOpenNativeSetTokensChannel('', '')
    } catch {
      // TODO. 토큰 저장 실패 시 다시 로그인해라 by 끼룩 at 2022.02.20 13:38
    }
  } else {
    setLocalStorage('webSignIn', false)
  }
}

export const setItemSnsType = (snsType) => localStorage.setItem('snsType', snsType)
export const getItemSnsType = () => localStorage.getItem('snsType')
export const removeItemSnsType = () => localStorage.removeItem('snsType')
