import PropTypes from 'prop-types';
import {useEffect} from 'react';
import {useRouter} from 'next/router';
import {PATH_DONOTS_PAGE} from '../routes/paths';
import {useBefuAuthContext} from './useAuthContext';
import {getLocalStorage, removeLocalStorage} from "../utils/storageUtils";

GuestGuard.propTypes = {
  children: PropTypes.node,
};

export default function GuestGuard({children}) {
  const {push} = useRouter();

  const {isAuthenticated, isInitialized} = useBefuAuthContext();

  useEffect(() => {
    if (isAuthenticated) {
      const signInCallBack = getLocalStorage('signInCallback')
      if (signInCallBack) {
        removeLocalStorage('signInCallback')
        push(signInCallBack)
      } else {
        push(PATH_DONOTS_PAGE.HOME);
      }
    }
  }, [isAuthenticated]);

  return (
    <>
      {children}
    </>
  )
}
