import {useEffect, useState} from 'react';
import {useRouter} from 'next/router';
import LoadingScreen from '../components/common/LoadingScreen';
import {useBefuAuthContext} from './useAuthContext';
import {PATH_DONOTS_PAGE} from "../routes/paths";

// import dynamic from "next/dynamic";
// const LoadingScreen = dynamic(() =>
//   import('../components/common/LoadingScreen'), {
//   ssr: false
// })

export default function AuthGuard({children}) {
  const {isAuthenticated, isInitialized} = useBefuAuthContext();
  const [requestedLocation, setRequestedLocation] = useState(null);
  const {pathname, push, replace} = useRouter();

  useEffect(() => {
    if (requestedLocation && pathname !== requestedLocation) {
      push(requestedLocation);
    }
    if (isAuthenticated) {
      setRequestedLocation(null);
    }
  }, [isAuthenticated, pathname, push, requestedLocation]);

  if (!isInitialized) {
    return <LoadingScreen/>
  }

  if (!isAuthenticated) {
    if (pathname !== requestedLocation) {
      setRequestedLocation(pathname);
    }
    replace(PATH_DONOTS_PAGE.GUEST)
  } else {
    return (
      <>
        {children}
      </>
    )
  }
}