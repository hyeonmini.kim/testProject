import {createContext, useCallback, useEffect, useRef, useState} from 'react';
import {setSession, setTokens} from './utils';
import {useRecoilState, useResetRecoilState, useSetRecoilState} from "recoil";
import {authenticateSelector} from "../recoil/selector/auth/authenticateSelector";
import {forMyPageEntrySelector, memberInformationSelector} from "../recoil/selector/auth/userSelector";
import {initAxiosSelector, initEnvSelector, initializeSelector} from "../recoil/selector/auth/initializeSelector";
import {
  getMemberMutate,
  getMyInformAddTokenMutate,
  getRefreshTokenMutate,
  getRefreshTokenMutateByCookie,
  postSignInByIdMutate
} from "../api/authApi";
import {authMemberKeySelector} from "../recoil/selector/sign-up/authSelector";
import {authLoginErrorMessageState} from "../recoil/atom/sign-up/auth";
import {ERRORS, GET_ERROR_CODE, GET_ERROR_MESSAGE} from "../constant/common/Error";
import {isMockApi, isNative} from "../utils/envUtils";
import {callOpenNativeGetTokensChannel} from "../channels/permissionChannel";
import {accessTokenSelector, refreshTokenSelector} from "../recoil/selector/auth/accessTokenSelector";
import {getLocalStorage, removeLocalStorage, setCookie} from "../utils/storageUtils";
import {getNativeOpt, setNativeOpt} from "../channels/commonChannel";
import {MY_SETTING_OPTION} from "../constant/my/Setting";
import {FROM_TYPE, LOGIN, NO_TOKEN_MSG} from "../constant/sign-up/SignUp";
import {COMMON_DIALOG_TYPE, COMMON_STR} from "../constant/common/Common";
import {logger} from "../utils/loggingUtils";
import {dialogSelector} from "../recoil/selector/common/dialogSelector";
import {PATH_DONOTS_PAGE} from "../routes/paths";
import {useRouter} from "next/router";
import LoadingScreen from "../components/common/LoadingScreen";
import {setHackleUserId} from "../utils/hackleUtils";
import {layerPopupClickedSelector} from "../recoil/selector/home/layerPopupSelector";

export const BefuAuthContext = createContext(null);

export function AuthProvider({client, children}) {
  const [isAuthenticated, setIsAuthenticated] = useRecoilState(authenticateSelector);
  const [isInitialized, setIsInitialized] = useRecoilState(initializeSelector);

  const [user, setUser] = useRecoilState(forMyPageEntrySelector)
  const [memberInfo, setMemberInfo] = useRecoilState(memberInformationSelector)
  const [memberKey, setMemberKey] = useRecoilState(authMemberKeySelector)

  const setWrongMessage = useSetRecoilState(authLoginErrorMessageState)
  const resetLayerPopupClicked = useResetRecoilState(layerPopupClickedSelector)

  const {mutate: mutatePostSignInById, isLoading} = postSignInByIdMutate()
  const {mutateAsync: mutateGetMember} = getMemberMutate()
  const {mutateAsync: mutateGetMyInformAddToken} = getMyInformAddTokenMutate()
  const {mutate: mutateGetAccessTokenByCookie} = getRefreshTokenMutateByCookie()
  const {mutate: mutateGetRefreshToken} = getRefreshTokenMutate()

  const [accessToken, setAccessToken] = useRecoilState(accessTokenSelector)
  const [refreshToken, setRefreshToken] = useRecoilState(refreshTokenSelector)
  const refreshTokenRef = useRef(null)

  const [isInitAxios, setIsInitAxios] = useRecoilState(initAxiosSelector);
  const [isInitEnv, setIsInitEnv] = useRecoilState(initEnvSelector);

  const setDialogMessage = useSetRecoilState(dialogSelector)

  const [getAccessToken, setGetAccessToken] = useState(false)

  const isMock = isMockApi()

  const router = useRouter()

  const initialize = useCallback(async () => {
    if (isNative()) {
      try {
        await getNativeOpt(MY_SETTING_OPTION.AUTO_LOGIN, async (result) => {
          if (!result?.val) {
            await setAuthState({type: 'INITIAL', authenticated: false, user: null})
          }
          if (JSON.parse(result?.val)) {
            await initializeNative()
          } else {
            await setAuthState({type: 'INITIAL', authenticated: false, user: null})
          }
        })
      } catch (error) {
        await setAuthState({type: 'INITIAL', authenticated: false, user: null})
      }
    } else {
      const isWebSignIn = getLocalStorage('webSignIn')
      if (isWebSignIn) {
        mutateGetAccessTokenByCookie(null, {
          onSuccess: async (result) => {
            if (result?.token) {
              await setPreAuthState(result?.token, null)
            } else {
              await setAuthState({type: 'INITIAL', authenticated: false, user: null})
            }
          },
          onError: async (error) => {
            await setAuthState({type: 'INITIAL', authenticated: false, user: null})
          }
        })
      } else {
        await setAuthState({type: 'INITIAL', authenticated: false, user: null})
      }
    }
  }, []);

  useEffect(() => {
    if (isInitEnv && isInitAxios) {
      initialize();
    }
  }, [isInitEnv, isInitAxios]);

  const setInitializedAndAuth = (type, authenticated) => {
    if (type === 'INITIAL') {
      setIsInitialized(true);
    }

    setIsAuthenticated(authenticated);
  }

  const setAuthState = async ({type, authenticated, user, token = ''}) => {
    setInitializedAndAuth(type, authenticated)

    if (authenticated && user) {
      logger.dump("### setAuthState ForMyEntry Api + valid token")
      try {
        await mutateGetMyInformAddToken({token: token}, {
          onSuccess: (result) => {
            setUser(result)
            setHackleUserId(result?.key)
          },
          onError: (error) => {
          }
        })
      } catch (e) {
        logger.dump("### setAuthState ForMyEntry Api + valid token Error")
      }
    }
  }

  const setPreAuthState = async (accessToken, refreshToken) => {
    if (accessToken !== NO_TOKEN_MSG) {
      setAccessToken(accessToken)
      if (refreshToken) {
        setRefreshToken(refreshToken)
      }
      await setAuthState({
        type: 'INITIAL',
        authenticated: true,
        user: true,
        token: accessToken
      })
    } else {
      logger.dump("### SetPreAuthState No Token Error, logout")
      await setAuthState({type: 'INITIAL', authenticated: false, user: null})
    }
  }

  const initializeNative = async () => {
    await callOpenNativeGetTokensChannel(async (tokens) => {
      if (tokens?.accessToken && tokens?.refreshToken) {
        await setPreAuthState(tokens?.accessToken, tokens?.refreshToken)
      } else {
        logger.dump("callOpenNativeGetTokensChannel", "result is invalid")
        await setAuthState({type: 'INITIAL', authenticated: false, user: null})
      }
    })
  }

  // LOGIN - 도낫츠
  function login(email, password) {
    const loginInform = {
      id: email,
      password: password
    }

    /* API : ID/PW 로그인 */
    mutatePostSignInById(loginInform, {
      onSuccess: async (result) => {
        if (result?.token && result?.refreshToken) {
          setAccessToken(result?.token)
          setRefreshToken(result?.refreshToken)
          await setTokens(result?.token, result?.refreshToken)

          await setAuthState({
            type: 'LOGIN',
            authenticated: true,
            user: true,
            token: result?.token
          })
        } else {
          setWrongMessage('아이디와 비밀번호를 다시 한번 확인해주세요')
        }
      },
      onError: async (error) => {
        switch (GET_ERROR_CODE(error)) {
          case ERRORS.ACCOUNT_NOT_FOUND:
          case ERRORS.PASSWORD_INCORRECT_ONCE:
          case ERRORS.PASSWORD_INCORRECT_TWICE:
          case ERRORS.PASSWORD_INCORRECT_THREE_TIMES:
          case ERRORS.PASSWORD_INCORRECT_FOUR_TIMES:
            setWrongMessage(GET_ERROR_MESSAGE(error))
            break
          case ERRORS.ACCOUNT_BLOCKED:
            setDialogMessage({
              type: COMMON_DIALOG_TYPE.ONE_BUTTON,
              message: GET_ERROR_MESSAGE(error),
              handleButton1: {
                text: LOGIN.RESET_PASSWORD,
                onClick: () => {
                  router.push({
                    pathname: PATH_DONOTS_PAGE.AUTH.SIGNUP,
                    query: {from: FROM_TYPE.RESET_PASSWORD},
                  })
                }
              },
            })
            break
        }
      }
    })
  }

  /* API : 멤버 정보 가져오기 */
  async function getMemberKey(accessToken) {
    try {
      await mutateGetMember({token: accessToken}, {
        onSuccess: async (getMemberResult) => {
          // setCookie(COMMON_STR.CI, getMemberResult?.ci, 1)
          setCookie(COMMON_STR.ACCOUNT_KEY, getMemberResult?.accountKey, 1)
          setMemberInfo(getMemberResult)
          setMemberKey(getMemberResult?.memberKey)

          await setAuthState({
            type: 'LOGIN',
            authenticated: true,
            user: true,
            token: accessToken
          })
        },
        onError: (error) => {
          setAuthState({type: 'INITIAL', authenticated: false, user: null})
        }
      })
    } catch (e) {
      await setAuthState({type: 'INITIAL', authenticated: false, user: null})
    }

    return memberKey
  }

  async function getUser(accessToken = '') {
    await setAuthState({
      type: 'LOGIN',
      authenticated: true,
      user: true,
      token: accessToken
    })
  }

  // LOGOUT
  const logout = async () => {
    if (isNative()) {
      await setNativeOpt({type: MY_SETTING_OPTION.AUTO_LOGIN, value: String(true)})
    }

    // 로컬스토리지 제거
    removeLocalStorage('isLayerPopupNotOpenOneDay')
    removeLocalStorage('isLabBottomDialogNotOpenOneDay')
    resetLayerPopupClicked()

    setSession(null);
    await setAuthState({type: 'LOGOUT', authenticated: false, user: null})
    client.clear()
  };

  return (
    <BefuAuthContext.Provider
      value={{
        isAuthenticated,
        isInitialized,
        user,
        method: 'jwt',
        login,
        logout,
        getMemberKey,
        getUser,
      }}
    >
      {isLoading && <LoadingScreen dialog/>}
      {children}
    </BefuAuthContext.Provider>
  );
}
