import {useContext} from 'react';
import {BefuAuthContext} from './JwtContext';

// ----------------------------------------------------------------------

export const useBefuAuthContext = () => {
  const context = useContext(BefuAuthContext)

  if (!context) throw new Error('useAuthContext context must be use inside AuthProvider');

  return context;

}