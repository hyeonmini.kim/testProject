import * as React from "react";
import {useEffect, useState} from "react";
import {PATH_DONOTS_PAGE} from "../routes/paths";
import {useBefuAuthContext} from "../auth/useAuthContext";
import Head from "next/head";
import LoadingScreen from "../components/common/LoadingScreen";
import {useRouter} from "next/router";
import {callOpenNativeIsWebLoadChannel} from "../channels/commonChannel";
import {isNative} from "../utils/envUtils";
import {MAIN_META, MAIN_TITLE} from "../constant/main/Main";
import useInflow, {INFLOW_PAGE} from "../hooks/useInflow";
import useRedirection, {REDIRECTION_TYPE} from "../hooks/useRedirection";
import IFrameDialog from "../components/common/IFrameDialog";
import useNativeBackKey from "../hooks/useNativeBackKey";

export default function HomePage() {
  const [url, setUrl] = useState(null)

  const {inflow} = useInflow()
  const {isReady, push} = useRouter();
  const {isRedirection} = useRedirection()
  const {isAuthenticated, isInitialized} = useBefuAuthContext();

  useEffect(() => {
    if (isReady && isInitialized) {
      inflow(INFLOW_PAGE.ROOT)
      const redirection = isRedirection()
      if (redirection?.type === REDIRECTION_TYPE.D_IFRAME) {
        setUrl(redirection?.url)
      } else if (redirection?.url) {
        push(redirection?.url)
      } else if (isAuthenticated) {
        push(PATH_DONOTS_PAGE.HOME)
      } else {
        push(PATH_DONOTS_PAGE.GUEST)
      }
    }
  }, [isReady, isInitialized]);

  useEffect(() => {
    if (isNative()) {
      callOpenNativeIsWebLoadChannel()
    }
  }, [])

  const handleClickBack = () => {
    if (isAuthenticated) {
      push(PATH_DONOTS_PAGE.HOME)
    } else {
      push(PATH_DONOTS_PAGE.GUEST)
    }
  }

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
     if (!!url) {
       handleClickBack()
      }
    }
  }, [handleBackKey])

  return (
    <>
      <Head>
        <title>
          {MAIN_TITLE.HEADER()}
        </title>
        <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta property="og:title" content={MAIN_META.TITLE}/>
        <meta property="og:description" content={MAIN_META.DESC}/>
        <meta property="og:image" content={MAIN_META.IMAGE}/>
      </Head>

      {/* 로딩 스크린*/}
      {!url && <LoadingScreen/>}

      {/* 아이프레임 다이얼로그 */}
      <IFrameDialog
        open={!!url}
        url={url}
        onBack={handleClickBack}
      />
    </>
  )
}