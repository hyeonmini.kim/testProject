import * as React from "react";
import {useEffect} from "react";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import {useBefuAuthContext} from "../../auth/useAuthContext";
import {useRouter} from "next/router";


export default function Main() {
  const {push, isReady} = useRouter();
  const {isAuthenticated, isInitialized} = useBefuAuthContext();

  useEffect(() => {
    if (isReady && isInitialized) {
      if (isAuthenticated) {
        push(PATH_DONOTS_PAGE.HOME)
      } else {
        push(PATH_DONOTS_PAGE.GUEST)
      }
    }
  }, [isReady, isInitialized]);
}