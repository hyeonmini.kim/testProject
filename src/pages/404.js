import NextLink from 'next/link';
import {Box, Button, Container, Typography} from '@mui/material';
import MainLayout from "../layouts/main/MainLayout";
import * as React from "react";
import Image from "../components/image";
import {keyframes} from "@mui/system";

export default function Page500() {

  const rotate = keyframes`
    0% {
      transform: translateX(-50%) rotateY(0deg);
    }
    50% {
      transform: translateX(-50%) rotateY(360deg);
    }
    100% {
      transform: translateX(-50%) rotateY(0deg);
    }
  `;

  const floating = keyframes`
    0% {
      transform: translate(-50%, 0);
    }
    50% {
      transform: translate(-50%, -30px);
    }
    100% {
      transform: translate(-50%, 0);
    }
  `;

  return (
    <MainLayout>
      {/* wrap */}
      <Container
        disableGutters
        maxWidth={'xs'}
        sx={{
          height: '100%',
          position: 'relative',
          background: '#ffffff'
        }}
      >
        {/* error_pg */}
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: '100%'
          }}
        >
          {/* error_ani */}
          <Box
            sx={{
              position: 'relative',
              width: '94px',
              height: '80px',
              margin: '0 auto'
            }}
          >
            {/* polygon */}
            <Box
              sx={{
                position: 'absolute',
                top: '0%',
                left: '50%',
                transform: 'translateX(-50%)',
                transformStyle: 'preserve-3d',
                zIndex: 1,
                width: '94px',
                animation: `${rotate} 2.8s ease infinite`
              }}
            >
              {/* items */}
              <Box
                sx={{
                  position: 'absolute',
                  width: '100%',
                  backfaceVisibility: 'hidden',
                }}
              >
                <Image alt={''} src={'/assets/error/error_img_polygon.png'}/>
              </Box>
              {/* items black*/}
              <Box
                sx={{
                  position: 'absolute',
                  width: '100%',
                  backfaceVisibility: 'hidden',
                  transform: 'rotateY(180deg)'
                }}
              >
                <Image alt={''} src={'/assets/error/error_img_polygon2.png'}/>
              </Box>
            </Box>
            <Image
              sx={{
                position: 'absolute',
                top: '35%',
                left: '50%',
                transform: 'translateX(-50%)',
                zIndex: 2,
                width: '7px',
                transformOrigin: 'bottom 50%',
                animation: `${floating} 1.4s cubic-bezier(0.165, 0.840, 0.440, 1.000) infinite`
              }}
              src={'/assets/error/error_img_inner.png'}/>
          </Box>
          <Typography sx={{
            mt: '20px',
            mb: '12px',
            color: '#222',
            fontSize: '18px',
            fontWeight: 500,
            textAlign: 'center',
            lineHeight: '24px'
          }}>
            페이지를 찾을 수 없어요.
          </Typography>
          <Typography
            sx={{
              color: '#666',
              fontSize: '14px',
              textAlign: 'center',
              fontWeight: 400,
              lineHeight: '14px',
            }}
          >
            불편을 드려 죄송합니다.
          </Typography>
        </Box>
        <NextLink href="/" passHref>
          {/* btn_wrap */}
          <Box
            sx={{
              position: 'absolute',
              bottom: 0,
              left: 0,
              width: '100%',
              padding: '20px',
              display: 'flex',
              justifyContent: 'space-between'
            }}>
            <Button
              fullWidth
              variant="contained"
              sx={{
                height: '52px',
                fontSize: '16px',
                fontWeight: '700',
                lineHeight: '24px',
                backgroundColor: 'primary.main',
              }}>
              홈으로 가기
            </Button>
          </Box>
        </NextLink>
      </Container>
    </MainLayout>
  );
}
