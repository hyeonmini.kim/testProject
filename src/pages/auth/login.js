import {useRouter} from "next/router";
import GuestGuard from '../../auth/GuestGuard';
import CommonHeader from "../../layouts/common/CommonHeader";
import Login from "../../sections/login/Login";
import {Container} from "@mui/material";
import useNativeBackKey from "../../hooks/useNativeBackKey";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import MainLayout from "../../layouts/main/MainLayout";
import {useBefuAuthContext} from "../../auth/useAuthContext";
import LoadingScreen from "../../components/common/LoadingScreen";
import {useRecoilState} from "recoil";
import {commonAgreeOpenSelector} from "../../recoil/selector/auth/agreeDetailSelector";

export default function LoginPage() {
  const [openAgreeDlg, setOpenAgreeDlg] = useRecoilState(commonAgreeOpenSelector)

  const [openDetail, setOpenDetail] = useState(false)
  const [openDetailCert, setOpenDetailCert] = useState(false)

  const router = useRouter()
  const mainRef = useRef(null)
  const {handleBackKey} = useNativeBackKey()

  const {isAuthenticated} = useBefuAuthContext();

  const handleClick = () => router.replace('/')

  useEffect(() => {
    if (handleBackKey) {
      if (openDetail) {
        setOpenDetail(false)
      } else if (openDetailCert) {
        setOpenDetailCert(false)
      } else if (openAgreeDlg) {
        setOpenAgreeDlg(false)
      } else {
        router.back()
      }
    }
  }, [handleBackKey]);

  return (
    <MainLayout mainRef={mainRef}>
      <GuestGuard>
        {
          !isAuthenticated ? (
            <Container maxWidth={'xs'} disableGutters>
              <CommonHeader backTabIndex={1} handleClick={handleClick}/>
              <Login
                mainRef={mainRef}
                openDetail={openDetail}
                setOpenDetail={setOpenDetail}
                openDetailSert={openDetailCert}
                setOpenDetailSert={setOpenDetailCert}
              />
            </Container>
          ) : (
            <LoadingScreen/>
          )
        }
      </GuestGuard>
    </MainLayout>
  )
}
