import {Box, Button, Container} from "@mui/material";
import {useRouter} from "next/router";
import {useEffect, useRef, useState} from "react";
import {useRecoilState, useRecoilValue, useResetRecoilState, useSetRecoilState} from "recoil";
import {
  authBirthdayState,
  authBottomButtonState,
  authBottomButtonTextState,
  authChildAllergyState,
  authChildBirthState,
  authChildGenderState,
  authChildHeightState,
  authChildKeywordState,
  authChildWeightState,
  authConfirmPasswordState,
  authEmailErrorState,
  authEmailFindIdErrorState,
  authEmailFindIdHelperTextState,
  authEmailFindIdState,
  authEmailHelperTextState,
  authEmailState,
  authFindIdAnswerErrorState,
  authFindIdAnswerHelperTextState,
  authFindIdAnswerState,
  authFindIdQuestionState,
  authIdEndNumberState,
  authIdErrorState,
  authIdFrontNumberState,
  authIdHelperTextState,
  authIdState,
  authInputRefIndexState,
  authLoginIdState,
  authNameState,
  authNicknameErrorState,
  authNicknameHelperTextState,
  authNicknameState,
  authPasswordErrorState,
  authPasswordHelperTextState,
  authPasswordState,
  authPhoneNumberState,
  authResetPasswordAnswer1ErrorState,
  authResetPasswordAnswer1HelperTextState,
  authResetPasswordAnswer1State,
  authResetPasswordAnswer2ErrorState,
  authResetPasswordAnswer2HelperTextState,
  authResetPasswordAnswer2State,
  authResetPasswordQuestion1State,
  authResetPasswordQuestion2State,
  authSignUpIdExistedSnsState,
  authTelecomState
} from "../../recoil/atom/sign-up/auth";
import {commonBottomDialogPageState, commonBottomDialogState, commonBottomDialogTitleState} from "../../recoil/atom/common/bottomDialog";
import {HEADER} from "../../config";
import ChildAllergyInputForm from "../../sections/auth/ChildAllergyInputForm";
import ChildKeywordInputForm from "../../sections/auth/ChildKeywordInputForm";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import {clickBorderNone, COMMON_DIALOG_TYPE, COMMON_STR, TOAST_TYPE} from "../../constant/common/Common";
import {
  CONFIRM_TYPE,
  FROM_TYPE,
  SIGN_UP_BIRTH,
  SIGN_UP_EMAIL,
  SIGN_UP_ID,
  SIGN_UP_IDENTIFICATION,
  SIGN_UP_NICKNAME,
  SIGN_UP_PASSWORD
} from "../../constant/sign-up/SignUp";
import {
  authAllergySelector,
  authChildAllergySelector,
  authChildBirthSelector,
  authChildGenderSelector,
  authChildHeightSelector,
  authChildKeywordSelector,
  authChildWeightSelector,
  authCiSelector,
  authFindIdAnswerSelector,
  authFindIdQuestionSelector,
  authGenderSelector,
  authKeywordSelector,
  authResetPasswordAnswer1Selector,
  authResetPasswordAnswer2Selector,
  authResetPasswordQuestion1Selector,
  authResetPasswordQuestion2Selector,
  signUpIdSelector,
  signUpSnsSelector
} from "../../recoil/selector/sign-up/signUpSelector";
import SignUpIdSectionList from "../../sections/sign-up/SignUpIdSectionList";
import ResetPasswordSectionList from "../../sections/sign-up/ResetPasswordSectionList";
import ChildProfileSectionList from "../../sections/sign-up/ChildProfileSectionList";
import AdditionalChildInformSectionList from "../../sections/sign-up/AdditionalChildInformSectionList";
import SignUpSnsSectionList from "../../sections/sign-up/SignUpSnsSectionList";
import {toastMessageSelector} from "../../recoil/selector/common/toastSelector";
import {memberInformationSelector} from "../../recoil/selector/auth/userSelector";
import useNativeBackKey from "../../hooks/useNativeBackKey";
import {authMemberKeySelector, authSelector} from "../../recoil/selector/sign-up/authSelector";
import {commonAgreeOpenSelector, commonIsLoadedSelector} from "../../recoil/selector/auth/agreeDetailSelector";
import {
  getIsDuplicateIdMutate,
  postAuthCodeMutate,
  postByEmailAndIdentityVerificationQasMutate,
  postByIdIdentityVerificationQuestionsMutate,
  postIsDuplicateEmailMutate,
  postIsDuplicateNicknameMutate,
  postMyEmailIdentityVerificationQuestionsMutate,
  postResetPasswordMutate,
  postSignUpAddIdMutate,
  postSignUpIdMutate,
  postSignUpSnsMutate,
  postVerifyIdentifyWithIdAndQasMutate
} from "../../api/authApi";
import {
  changeBirthFromYYMMDDToYYYYMMDD,
  changeGenderType,
  changeNewsAgencyType,
  checkTransactionSeqNumberByEnv,
  removeHyphenFromPhoneNumber
} from "../../utils/authUtils";
import {getItemSnsType, setTokens} from "../../auth/utils";
import {useBefuAuthContext} from "../../auth/useAuthContext";
import {callOpenNativeSetSnsProviderChannel} from "../../channels/commonChannel";
import {accessTokenSelector, refreshTokenSelector, transactionSeqNumberSelector} from "../../recoil/selector/auth/accessTokenSelector";
import {
  inputChildrenAllergyListSelector,
  inputChildrenAllergySelector,
  inputChildrenBirthSelector,
  inputChildrenGenderSelector,
  inputChildrenHeightSelector,
  inputChildrenKeywordListSelector,
  inputChildrenKeywordSelector,
  inputChildrenWeightSelector
} from "../../recoil/selector/home/inputChildrenInfoSelector";
import {getLocalStorage, removeLocalStorage, setCookie} from "../../utils/storageUtils";
import {ERRORS, GET_ERROR_CODE, GET_ERROR_MESSAGE} from "../../constant/common/Error";
import MainLayout, {sxFixedCenterMainLayout} from "../../layouts/main/MainLayout";
import HeaderTitleLayout from "../../layouts/auth/HeaderTitleLayout";
import {SNACKBAR_MESSAGE} from "../../constant/recipe/detail/DetailConstants";
import {getEnv} from "../../utils/envUtils";
import {debounce} from "lodash";
import LoadingScreen from "../../components/common/LoadingScreen";
import SignUpIdIosSectionList from "../../sections/sign-up/SignUpIdIosSectionList";
import FindIdSectionList from "../../sections/sign-up/FindIdSectionList";
import {
  firstAnswerSelector,
  firstQuestionTextSelector,
  identificationQASelector,
  secondAnswerSelector,
  secondQuestionTextSelector
} from "../../recoil/selector/auth/identificationQASelector";
import {dialogSelector} from "../../recoil/selector/common/dialogSelector";
import {getFloatFixed} from "../../utils/formatNumber";
import CompleteDialog from "../../sections/auth/complete/CompleteDialog";
import {COMPLETE_SIGN_UP, LEVEL_UP_CHOCO} from "../../constant/dialog/Complete";
import CompleteSignUpCard from "../../sections/auth/complete/CompleteSignUpCard";
import CompleteLevelUpChocoCard from "../../sections/auth/complete/CompleteLevelUpChocoCard";
import {USER_GRADE} from "../../constant/common/User";
import {putMyProfilePictureMutate} from "../../api/myApi";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {INFLOW_PAGE} from "../../hooks/useInflow";

export default function SignUp() {
  // 페이지 및 바텀 다이얼로그 섹션 페이지 관리 state
  const [page, setPage] = useState(0)
  const setDialogPage = useSetRecoilState(commonBottomDialogPageState)
  // 바텀 버튼 state
  const isBottomBtnDisabled = useRecoilValue(authBottomButtonState)
  const bottomBtnText = useRecoilValue(authBottomButtonTextState)
  // 바텀 다이얼로그 state
  const [isBottomDialogOpen, setIsBottomDialogOpen] = useRecoilState(commonBottomDialogState)
  const setBottomDialogTitle = useSetRecoilState(commonBottomDialogTitleState)
  // 본인인증 정보 화면 state
  const [name, setName] = useRecoilState(authNameState)
  const telecom = useRecoilValue(authTelecomState)
  const [phone, setPhone] = useRecoilState(authPhoneNumberState)
  const idFront = useRecoilValue(authIdFrontNumberState)
  const idEnd = useRecoilValue(authIdEndNumberState)
  const [gender, setGender] = useRecoilState(authGenderSelector)
  const [birth, setBirth] = useRecoilState(authBirthdayState)
  // 회원정보 입력 화면 state reset
  const resetAuthInform = useResetRecoilState(authSelector)
  const resetSignUpIdInform = useResetRecoilState(signUpIdSelector)
  const resetSignUpSnsInform = useResetRecoilState(signUpSnsSelector)
  const resetIdentificationQA = useResetRecoilState(identificationQASelector)

  const resetId = useResetRecoilState(authIdState)
  const resetPassword = useResetRecoilState(authPasswordState)
  const resetConfirmPassword = useResetRecoilState(authConfirmPasswordState)

  const resetChildBirth = useResetRecoilState(authChildBirthState)
  const resetChildHeight = useResetRecoilState(authChildHeightState)
  const resetChildWeight = useResetRecoilState(authChildWeightState)
  const resetChildGender = useResetRecoilState(authChildGenderState)
  const resetChildAllergy = useResetRecoilState(authChildAllergyState)
  const resetSelectedAllergy = useResetRecoilState(authAllergySelector)
  const resetChildKeyword = useResetRecoilState(authChildKeywordState)
  const resetSelectedKeyword = useResetRecoilState(authKeywordSelector)
  const resetInputChildAllergy = useResetRecoilState(inputChildrenAllergySelector)
  const resetInputChildKeyword = useResetRecoilState(inputChildrenKeywordSelector)

  const [inputChildBirth, setInputChildBirth] = useRecoilState(inputChildrenBirthSelector)
  const [inputChildHeight, setInputChildHeight] = useRecoilState(inputChildrenHeightSelector)
  const [inputChildWeight, setInputChildWeight] = useRecoilState(inputChildrenWeightSelector)
  const [inputChildGender, setInputChildGender] = useRecoilState(inputChildrenGenderSelector)
  const [inputChildAllergy, setInputChildAllergy] = useRecoilState(inputChildrenAllergySelector)
  const [inputChildAllergyList, setInputChildAllergyList] = useRecoilState(inputChildrenAllergyListSelector)
  const [inputChildKeyword, setInputChildKeyword] = useRecoilState(inputChildrenKeywordSelector)
  const [inputChildKeywordList, setInputChildKeywordList] = useRecoilState(inputChildrenKeywordListSelector)

  const [childBirth, setChildBirth] = useRecoilState(authChildBirthSelector)
  const [childHeight, setChildHeight] = useRecoilState(authChildHeightSelector)
  const [childWeight, setChildWeight] = useRecoilState(authChildWeightSelector)
  const [childGender, setChildGender] = useRecoilState(authChildGenderSelector)
  const [childAllergy, setChildAllergy] = useRecoilState(authChildAllergySelector)
  const [allergyList, setAllergyList] = useRecoilState(authAllergySelector)
  const [childKeyword, setChildKeyword] = useRecoilState(authChildKeywordSelector)
  const [keywordList, setKeywordList] = useRecoilState(authKeywordSelector)

  const [ci, setCi] = useRecoilState(authCiSelector)
  const id = useRecoilValue(authIdState)
  const email = useRecoilValue(authEmailState)
  const emailFindId = useRecoilValue(authEmailFindIdState)
  const resetEmailFindId = useResetRecoilState(authEmailFindIdState)
  const password = useRecoilValue(authPasswordState)
  const nickname = useRecoilValue(authNicknameState)

  const [memberInfo, setMemberInfo] = useRecoilState(memberInformationSelector)
  const [memberKey, setMemberKey] = useRecoilState(authMemberKeySelector)

  const signUpIdInform = useRecoilValue(signUpIdSelector)
  const signUpSnsInform = useRecoilValue(signUpSnsSelector)

  const setIdHelperText = useSetRecoilState(authIdHelperTextState)
  const setIdTrue = useSetRecoilState(authIdErrorState)
  const setPwHelperText = useSetRecoilState(authPasswordHelperTextState)
  const setPwTrue = useSetRecoilState(authPasswordErrorState)
  const setEmailHelperText = useSetRecoilState(authEmailHelperTextState)
  const setEmailFindIdHelperText = useSetRecoilState(authEmailFindIdHelperTextState)
  const setFindIdAnswerHelperText = useSetRecoilState(authFindIdAnswerHelperTextState)
  const setResetPasswordAnswer1HelperText = useSetRecoilState(authResetPasswordAnswer1HelperTextState)
  const setResetPasswordAnswer2HelperText = useSetRecoilState(authResetPasswordAnswer2HelperTextState)
  const setEmailTrue = useSetRecoilState(authEmailErrorState)
  const setEmailFindIdTrue = useSetRecoilState(authEmailFindIdErrorState)
  const setFindIdAnswerTrue = useSetRecoilState(authFindIdAnswerErrorState)
  const setResetPasswordAnswer1True = useSetRecoilState(authResetPasswordAnswer1ErrorState)
  const setResetPasswordAnswer2True = useSetRecoilState(authResetPasswordAnswer2ErrorState)
  const setNicknameHelperText = useSetRecoilState(authNicknameHelperTextState)
  const setNicknameTrue = useSetRecoilState(authNicknameErrorState)

  const setLoginId = useSetRecoilState(authLoginIdState)
  const [accessToken, setAccessToken] = useRecoilState(accessTokenSelector)
  const setRefreshToken = useSetRecoilState(refreshTokenSelector)
  const [transactionSeqNumber, setTransactionSeqNumber] = useRecoilState(transactionSeqNumberSelector)

  // 본인인증 입력칸 포커스 state
  const setIndex = useSetRecoilState(authInputRefIndexState)

  const setToastMessage = useSetRecoilState(toastMessageSelector)
  const setDialogMessage = useSetRecoilState(dialogSelector)

  const [isLoaded, setIsLoaded] = useRecoilState(commonIsLoadedSelector)
  const [openAgreeDlg, setOpenAgreeDlg] = useRecoilState(commonAgreeOpenSelector)

  const [signUpIdExistedSns, setSignUpIdExistedSns] = useRecoilState(authSignUpIdExistedSnsState)
  const [findIdQuestion, setFindIdQuestion] = useRecoilState(authFindIdQuestionSelector)
  const [resetPasswordQuestion1, setResetPasswordQuestion1] = useRecoilState(authResetPasswordQuestion1Selector)
  const [resetPasswordQuestion2, setResetPasswordQuestion2] = useRecoilState(authResetPasswordQuestion2Selector)
  const resetFindIdQuestion = useResetRecoilState(authFindIdQuestionState)
  const resetResetPasswordQuestion1 = useResetRecoilState(authResetPasswordQuestion1State)
  const resetResetPasswordQuestion2 = useResetRecoilState(authResetPasswordQuestion2State)
  const [findIdAnswer, setFindIdAnswer] = useRecoilState(authFindIdAnswerSelector)
  const [resetPasswordAnswer1, setResetPasswordAnswer1] = useRecoilState(authResetPasswordAnswer1Selector)
  const [resetPasswordAnswer2, setResetPasswordAnswer2] = useRecoilState(authResetPasswordAnswer2Selector)
  const resetFindIdAnswer = useResetRecoilState(authFindIdAnswerState)
  const resetResetPasswordAnswer1 = useResetRecoilState(authResetPasswordAnswer1State)
  const resetResetPasswordAnswer2 = useResetRecoilState(authResetPasswordAnswer2State)

  const [firstQuestionText, setFirstQuestionText] = useRecoilState(firstQuestionTextSelector)
  const [secondQuestionText, setSecondQuestionText] = useRecoilState(secondQuestionTextSelector)
  const [firstAnswer, setFirstAnswer] = useRecoilState(firstAnswerSelector)
  const [secondAnswer, setSecondAnswer] = useRecoilState(secondAnswerSelector)

  const [isPhoneIdentityVerificationUsageAgreed, setIsPhoneIdentityVerificationUsageAgreed] = useState(undefined)
  const [showCompleteSignUp, setShowCompleteSignUp] = useState(false)
  const [showLevelUpChoco, setShowLevelUpChoco] = useState(false)
  const [profilePictureUrl, setProfilePictureUrl] = useState('')

  const bottomRef = useRef(null)
  const topRef = useRef(null)

  const {getUser} = useBefuAuthContext()

  const env = getEnv()

  useEffect(() => {
    setIsLoaded(false)
    setOpenAgreeDlg(false)

    // 아이 정보 임시값 있으면 추가
    if (inputChildBirth.length > 0) setChildBirth(inputChildBirth)
    if (inputChildHeight.length > 0) setChildHeight(inputChildHeight)
    if (inputChildWeight.length > 0) setChildWeight(inputChildWeight)
    if (inputChildGender.length > 0) setChildGender(inputChildGender)
    if (inputChildAllergy !== undefined) setChildAllergy(inputChildAllergy)
    if (inputChildAllergy !== undefined) setAllergyList(inputChildAllergyList)
    if (inputChildKeyword.length > 0) setChildKeyword(inputChildKeyword)
    if (inputChildKeyword.length > 0) setKeywordList(inputChildKeywordList)
  }, [])

  const router = useRouter()
  const {handleBackKey} = useNativeBackKey()

  const fromType = router.query.from

  const {mutate: mutateGetIsDuplicateId} = getIsDuplicateIdMutate()
  const {mutate: mutatePostIsDuplicateNickname} = postIsDuplicateNicknameMutate()
  const {mutate: mutatePostIsDuplicateEmail} = postIsDuplicateEmailMutate()
  const {mutate: mutatePostAuthCode} = postAuthCodeMutate()
  const {mutate: mutatePostResetPw} = postResetPasswordMutate()
  const {mutate: mutatePostSignUpId, isLoading: isLoadingSignUpId} = postSignUpIdMutate()
  const {mutate: mutatePostSignUpSns} = postSignUpSnsMutate()
  const {mutate: mutatePostSignUpAddId} = postSignUpAddIdMutate()
  const {mutate: MutatePostMyEmailIdentityVerificationQuestions} = postMyEmailIdentityVerificationQuestionsMutate()
  const {mutate: MutatePostMyIdIdentityVerificationQuestions} = postByIdIdentityVerificationQuestionsMutate()
  const {mutate: mutatePostByEmailAndIdentityVerificationQas} = postByEmailAndIdentityVerificationQasMutate()
  const {mutate: mutatePostVerifyIdentifyWithIdAndQas} = postVerifyIdentifyWithIdAndQasMutate()
  const {mutate: mutatePutMyProfilePicture, isLoading: isLoadingPutMyProfilePicture} = putMyProfilePictureMutate()

  const backButtonControl = () => {
    if (page > 0) {
      switch (fromType) {
        case FROM_TYPE.SIGN_UP_ID:
          if (page === 1) {
            resetAuthInform()
            resetSignUpIdInform()
            resetSelectedAllergy()
            resetSelectedKeyword()
            resetConfirmPassword()
            router.back()
          } else if (page === 12) {
            router.replace(PATH_DONOTS_PAGE.HOME).then(() => {
              setShowCompleteSignUp(false)
              setShowLevelUpChoco(false)
            })
          } else {
            setPage(page - 1)
          }
          break
        case FROM_TYPE.SIGN_UP_SNS:
          if (page === 1) {
            resetSignUpSnsInform()
            router.back()
          } else {
            setPage(page - 1)
          }
          break
        case FROM_TYPE.FIND_ID:
          if (page === 1) {
            resetEmailFindId()
            resetFindIdQuestion()
            resetFindIdAnswer()
            setEmailFindIdHelperText('')
            setEmailFindIdTrue(true)
            setFindIdAnswerHelperText('')
            setFindIdAnswerTrue(true)
            resetAuthInform()
            setIndex(-1)
            router.back()
          } else if (page === 2) {
            resetAuthInform()
            setFindIdAnswerHelperText('')
            setFindIdAnswerTrue(true)
            setIndex(-1)
            setPage(0)
          } else if (isBottomDialogOpen) {
            setIsBottomDialogOpen(false)
          } else {
            setPage(page - 1)
          }
          break
        case FROM_TYPE.RESET_PASSWORD:
          if (page === 3) {
            setResetPasswordAnswer1HelperText('')
            setResetPasswordAnswer1True(true)
            setResetPasswordAnswer2HelperText('')
            setResetPasswordAnswer2True(true)
            setPage(0)
          } else if (page === 2 && isPhoneIdentityVerificationUsageAgreed === true) {
            setPage(1)
          } else if (page === 2 && isPhoneIdentityVerificationUsageAgreed === false) {
            setPage(3)
          } else if (isBottomDialogOpen) {
            setIsBottomDialogOpen(false)
          } else {
            resetResetPasswordQuestion1()
            resetResetPasswordQuestion2()
            resetResetPasswordAnswer1()
            resetResetPasswordAnswer2()
            setIdHelperText('')
            setIdTrue(true)
            setResetPasswordAnswer1HelperText('')
            setResetPasswordAnswer1True(true)
            setResetPasswordAnswer2HelperText('')
            setResetPasswordAnswer2True(true)
            resetAuthInform()
            resetId()
            resetPassword()
            resetConfirmPassword()
            router.back()
          }
          break
        default:
          setPage(page - 1)
          break
      }
    } else {
      switch (fromType) {
        case FROM_TYPE.SIGN_UP_ID:
          if (isBottomDialogOpen) {
            setIsBottomDialogOpen(false)
          } else {
            resetAuthInform()
            resetSignUpIdInform()
            resetSelectedAllergy()
            resetSelectedKeyword()
            setIndex(-1)
            router.back()
          }
          break
        case FROM_TYPE.SIGN_UP_ID_IOS:
          resetSignUpIdInform()
          setBirth('')
          resetConfirmPassword()
          resetIdentificationQA()
          resetSelectedAllergy()
          resetSelectedKeyword()
          setIndex(-1)
          router.back()
          break
        case FROM_TYPE.SIGN_UP_SNS:
          if (isBottomDialogOpen) {
            setIsBottomDialogOpen(false)
          } else {
            resetSignUpSnsInform()
            setIndex(-1)
            router.back()
          }
          break
        case FROM_TYPE.FIND_ID:
          resetEmailFindId()
          resetFindIdQuestion()
          resetFindIdAnswer()
          setEmailFindIdHelperText('')
          setEmailFindIdTrue(true)
          setFindIdAnswerHelperText('')
          setFindIdAnswerTrue(true)
          resetAuthInform()
          setIndex(-1)
          router.back()
          break
        case FROM_TYPE.RESET_PASSWORD:
          if (isBottomDialogOpen) {
            setIsBottomDialogOpen(false)
          } else {
            resetResetPasswordQuestion1()
            resetResetPasswordQuestion2()
            resetResetPasswordAnswer1()
            resetResetPasswordAnswer2()
            setIdHelperText('')
            setIdTrue(true)
            setResetPasswordAnswer1HelperText('')
            setResetPasswordAnswer1True(true)
            setResetPasswordAnswer2HelperText('')
            setResetPasswordAnswer2True(true)
            resetId()
            resetPassword()
            resetConfirmPassword()
            setIndex(-1)
            router.back()
          }
          break
        case FROM_TYPE.ADDITIONAL_CHILD_INFORM:
          break
        case FROM_TYPE.GUEST_CHILD_PROFILE:
          resetChildBirth()
          resetChildHeight()
          resetChildWeight()
          resetChildGender()
          setInputChildBirth('')
          setInputChildHeight('')
          setInputChildWeight('')
          setInputChildGender('')
          router.back()
          break
        case FROM_TYPE.GUEST_CHILD_ALLERGY:
          resetChildAllergy()
          resetSelectedAllergy()
          resetInputChildAllergy()
          router.back()
          break
        case FROM_TYPE.GUEST_CHILD_KEYWORD:
          resetChildKeyword()
          resetSelectedKeyword()
          resetInputChildKeyword()
          router.back()
          break
      }
    }
  }

  const handleCloseClick = () => {
    backButtonControl()
  }

  const handleConfirmClick = debounce(async () => {
    // 본인인증 화면 바텀 버튼 컨트롤
    if (bottomBtnText === '다음') {
      if (name === '') {
        // 이름에 포커스
        setIndex(0)
        return
      }
      if (telecom === '') {
        setIsBottomDialogOpen(true)
        setBottomDialogTitle(false)
        return
      }
      if (phone === '') {
        // 휴대폰 번호에 포커스
        setIndex(1)
        return
      }
      if (idFront === '') {
        // 주민번호 앞자리에 포커스
        setIndex(2)
        return
      }
      if (idEnd === '') {
        // 주민번호 뒷자리에 포커스
        setIndex(3)
      }
    } else if (bottomBtnText === '확인') {
      switch (fromType) {
        case FROM_TYPE.SIGN_UP_ID:
          if (page === 0) {
            openBottomDialog()
          } else if (page === 1) {
            /* API : 아이디 중복 검사 */
            mutateGetIsDuplicateId({id: id}, {
              onSuccess: (isDuplicate) => {
                if (isDuplicate) {
                  setIdTrue(false)
                  setIdHelperText(SIGN_UP_ID.HELPER_TEXT_DUPLICATION)
                } else {
                  setPage(page + 1)
                }
              }
            })
          } else if (page === 2) {
            // 비밀번호에 아이디, 핸드폰번호, 생년월일 포함 여부 체크
            const birthEightDigits = changeBirthFromYYMMDDToYYYYMMDD(idFront, idEnd)
            if (password.includes(id) || password.includes(phone) || password.includes(idFront) || password.includes(birthEightDigits)) {
              setPwTrue(false)
              setPwHelperText(SIGN_UP_PASSWORD.INVALID)
            } else {
              if (signUpIdExistedSns) {
                const param = {
                  ci: ci,
                  id: id,
                  password: password,
                }
                mutatePostSignUpAddId(param, {
                  onSuccess: (result) => {
                    // 토큰 저장
                    setAccessToken(result?.token)
                    setRefreshToken(result?.refreshToken)
                    setTokens(result?.token, result?.refreshToken)

                    setMemberInfo(result)
                    setMemberKey(result?.memberKey)
                    setCookie(COMMON_STR.ACCOUNT_KEY, result?.accountKey, 1)

                    getUser(result?.token)

                    /* 입력 정보 초기화 */
                    setIndex(-1)
                    resetAuthInform()
                    resetSignUpIdInform()
                    resetConfirmPassword()
                    setSignUpIdExistedSns(false)
                    /* 가입 완료 페이지로 이동 */
                    router.replace(
                      {pathname: PATH_DONOTS_PAGE.AUTH.CONFIRM, query: {from: CONFIRM_TYPE.SUCCESS}},
                      undefined,
                      {shallow: true}
                    )
                  },
                  onError: (error) => {
                    switch (GET_ERROR_CODE(error)) {
                      case ERRORS.PASSWORD_INCLUDE_PERSONAL_INFORMATION:
                        setPwHelperText(GET_ERROR_MESSAGE(error))
                        setPwTrue(false)
                        break
                    }
                  }
                })
              } else {
                setPage(page + 1)
              }
            }
          } else if (page === 3) {
            /* API : 이메일 중복 검사 */
            mutatePostIsDuplicateEmail({email: email}, {
              onSuccess: (isDuplicate) => {
                if (isDuplicate) {
                  setEmailTrue(false)
                  setEmailHelperText(SIGN_UP_EMAIL.HELPER_TEXT_DUPLICATION)
                } else {
                  setPage(page + 1)
                }
              }
            })
          } else if (page === 4) {
            /* API : 닉네임 중복 검사 */
            mutatePostIsDuplicateNickname({nickname: nickname}, {
              onSuccess: (isDuplicate) => {
                if (isDuplicate) {
                  setNicknameTrue(false)
                  setNicknameHelperText(SIGN_UP_NICKNAME.HELPER_TEXT_DUPLICATION)
                } else {
                  setPage(page + 1)
                }
              }
            })
          } else if (page === 11) {
            /* API : ID 회원가입 완료 처리 */
            mutatePostSignUpId(signUpIdInform, {
              onSuccess: (result) => {
                // 토큰 저장
                setAccessToken(result?.token)
                setRefreshToken(result?.refreshToken)
                setTokens(result?.token, result?.refreshToken)

                setMemberInfo(result)
                setMemberKey(result?.memberKey)
                setCookie(COMMON_STR.ACCOUNT_KEY, result?.accountKey, 1)

                getUser(result?.token)

                /* 입력 정보 초기화 */
                setIndex(-1)
                resetAuthInform()
                resetSignUpIdInform()
                resetConfirmPassword()
                resetSelectedAllergy()
                resetSelectedKeyword()

                /* 가입 완료 페이지로 이동 */
                setShowCompleteSignUp(true)
                setPage(page + 1)

                // 가입 유입 경로
                const inflow = getLocalStorage('inflow')
                if (inflow) {
                  removeLocalStorage(inflow)
                  setHackleTrack(HACKLE_TRACK.INFLOW, {
                    page: INFLOW_PAGE.JOIN,
                    inflow: inflow
                  })
                }
              }
            })
          } else if (page === 7) {
            const parseHeight = getFloatFixed(childHeight, 1)
            setChildHeight(parseHeight)
            setPage(page + 1)
          } else if (page === 8) {
            const parseWeight = getFloatFixed(childWeight, 1)
            setChildWeight(parseWeight)
            setPage(page + 1)
          } else {
            setPage(page + 1)
          }
          break
        case FROM_TYPE.SIGN_UP_SNS:
          if (page === 0) {
            openBottomDialog()
          } else if (page === 1) {
            /* API : 이메일 중복 검사 */
            mutatePostIsDuplicateEmail({email: email}, {
              onSuccess: (isDuplicate) => {
                if (isDuplicate) {
                  setEmailTrue(false)
                  setEmailHelperText(SIGN_UP_EMAIL.HELPER_TEXT_DUPLICATION)
                } else {
                  setPage(page + 1)
                }
              }
            })
          } else if (page === 2) {
            /* API : 닉네임 중복 검사 */
            mutatePostIsDuplicateNickname({nickname: nickname}, {
              onSuccess: (isDuplicate) => {
                if (isDuplicate) {
                  setNicknameTrue(false)
                  setNicknameHelperText(SIGN_UP_NICKNAME.HELPER_TEXT_DUPLICATION)
                } else {
                  setPage(page + 1)
                }
              }
            })
          } else if (page === 9) {
            /* API : SNS 회원가입 완료 처리 */
            mutatePostSignUpSns(signUpSnsInform, {
              onSuccess: (result) => {
                /* 입력 정보 초기화 */
                setMemberInfo(result)
                setMemberKey(result?.memberKey)
                setCookie(COMMON_STR.ACCOUNT_KEY, result?.accountKey, 1)

                getUser(accessToken)

                setIndex(-1)
                resetAuthInform()
                resetSignUpSnsInform()
                callOpenNativeSetSnsProviderChannel(getItemSnsType())

                /* 가입 완료 페이지로 이동 */
                router.replace(
                  {pathname: PATH_DONOTS_PAGE.AUTH.CONFIRM, query: {from: CONFIRM_TYPE.SUCCESS}},
                  undefined,
                  {shallow: true}
                )
              }
            })
          } else {
            setPage(page + 1)
          }
          break
        case FROM_TYPE.SIGN_UP_ID_IOS:
          if (page === 0) {
            const curAge = new Date().getFullYear() - parseInt(birth.substring(0, 4)) + 1
            if (curAge < 14) {
              // 14세 미만 가입 불가
              router.replace({
                pathname: PATH_DONOTS_PAGE.AUTH.CONFIRM,
                query: {
                  from: CONFIRM_TYPE.REJECT,
                }
              }).then(() => setBirth(''))
            } else {
              // 본인인증을 태우지 않는 경우 들어가는 임의 값
              setName(SIGN_UP_BIRTH.TEMP_NAME)
              setPhone(SIGN_UP_BIRTH.TEMP_PHONE)
              setGender(SIGN_UP_BIRTH.TEMP_GENDER)
              setCi(SIGN_UP_BIRTH.TEMP_CI)

              setPage(page + 1)
            }
          } else if (page === 1) {
            /* API : 아이디 중복 검사 */
            mutateGetIsDuplicateId({id: id}, {
              onSuccess: (isDuplicate) => {
                if (isDuplicate) {
                  setIdTrue(false)
                  setIdHelperText(SIGN_UP_ID.HELPER_TEXT_DUPLICATION)
                } else {
                  setPage(page + 1)
                }
              }
            })
          } else if (page === 2) {
            // 비밀번호에 아이디, 생년월일 포함 여부 체크
            if (password.includes(id) || password.includes(birth)) {
              setPwTrue(false)
              setPwHelperText(SIGN_UP_PASSWORD.INVALID)
            } else {
              setPage(page + 1)
            }
          } else if (page === 3) {
            /* API : 이메일 중복 검사 */
            mutatePostIsDuplicateEmail({email: email}, {
              onSuccess: (isDuplicate) => {
                if (isDuplicate) {
                  setEmailTrue(false)
                  setEmailHelperText(SIGN_UP_EMAIL.HELPER_TEXT_DUPLICATION)
                } else {
                  setPage(page + 1)
                }
              }
            })
          } else if (page === 4) {
            /* API : 닉네임 중복 검사 */
            mutatePostIsDuplicateNickname({nickname: nickname}, {
              onSuccess: (isDuplicate) => {
                if (isDuplicate) {
                  setNicknameTrue(false)
                  setNicknameHelperText(SIGN_UP_NICKNAME.HELPER_TEXT_DUPLICATION)
                } else {
                  setPage(page + 1)
                }
              }
            })
          } else if (page === 5) {
            setDialogMessage({
              type: COMMON_DIALOG_TYPE.TWO_BUTTON,
              message: SIGN_UP_IDENTIFICATION.POPUP_MESSAGE,
              subMessage: `${firstQuestionText}\n${firstAnswer}\n${secondQuestionText}\n${secondAnswer}`,
              leftButtonProps: {text: SIGN_UP_IDENTIFICATION.POPUP_CANCEL},
              rightButtonProps: {text: SIGN_UP_IDENTIFICATION.POPUP_OK, onClick: () => setPage(page + 1)},
            })
          } else if (page === 12) {
            /* API : ID - iOS 회원가입 완료 처리 */
            console.log('signUpIdInform', signUpIdInform)
            mutatePostSignUpId(signUpIdInform, {
              onSuccess: (result) => {
                // 토큰 저장
                setAccessToken(result?.token)
                setRefreshToken(result?.refreshToken)
                setTokens(result?.token, result?.refreshToken)

                setMemberInfo(result)
                setMemberKey(result?.memberKey)
                setCookie(COMMON_STR.ACCOUNT_KEY, result?.accountKey, 1)

                getUser(result?.token)

                /* 입력 정보 초기화 */
                setIndex(-1)
                resetAuthInform()
                resetSignUpIdInform()
                resetConfirmPassword()
                resetIdentificationQA()
                resetSelectedAllergy()
                resetSelectedKeyword()

                /* 가입 완료 페이지로 이동 */
                setShowCompleteSignUp(true)
                setPage(page + 1)

                // 가입 유입 경로
                const inflow = getLocalStorage('inflow')
                if (inflow) {
                  removeLocalStorage(inflow)
                  setHackleTrack(HACKLE_TRACK.INFLOW, {
                    page: INFLOW_PAGE.JOIN,
                    inflow: inflow
                  })
                }
              }
            })
          } else if (page === 8) {
            const parseHeight = getFloatFixed(childHeight, 1)
            setChildHeight(parseHeight)
            setPage(page + 1)
          } else if (page === 9) {
            const parseWeight = getFloatFixed(childWeight, 1)
            setChildWeight(parseWeight)
            setPage(page + 1)
          } else {
            setPage(page + 1)
          }
          break
        case FROM_TYPE.FIND_ID:
          if (page === 0) {
            MutatePostMyEmailIdentityVerificationQuestions({email: emailFindId}, {
              onSuccess: (result) => {
                if (result?.isPhoneIdentityVerificationUsageAgreed) {
                  setIsPhoneIdentityVerificationUsageAgreed(true)
                  setPage(1)
                } else {
                  setIsPhoneIdentityVerificationUsageAgreed(false)
                  // setFindIdQuestion('FAVORITE_FOOD')
                  setFindIdQuestion(result?.identityVerificationQuestions[0])
                  setPage(2)
                }
              },
              onError: (error) => {
                switch (GET_ERROR_CODE(error)) {
                  case ERRORS.DATA_NOT_FOUND:
                    setEmailFindIdHelperText('일치하는 회원정보가 없습니다')
                    setEmailFindIdTrue(false)
                    break
                }
              }
            })
          } else if (page === 1) {
            openBottomDialog()
          } else if (page === 2) {
            mutatePostByEmailAndIdentityVerificationQas({
              email: emailFindId,
              identityVerificationQas: [{question: findIdQuestion, answerText: findIdAnswer}]
            }, {
              onSuccess: (result) => {
                resetEmailFindId()
                resetFindIdQuestion()
                resetFindIdAnswer()
                setEmailFindIdHelperText('')
                setEmailFindIdTrue(true)
                setFindIdAnswerHelperText('')
                setFindIdAnswerTrue(true)

                router.replace({
                  pathname: PATH_DONOTS_PAGE.AUTH.CONFIRM,
                  query: {
                    from: CONFIRM_TYPE.CORRECT,
                    id: result?.id,
                    createdAt: result?.createdAt?.substring(2, 10).replaceAll('-', '.'),
                  }
                })
              },
              onError: (error) => {
                switch (GET_ERROR_CODE(error)) {
                  case ERRORS.QA_NOT_MATCHED:
                    setFindIdAnswerHelperText(GET_ERROR_MESSAGE(error))
                    setFindIdAnswerTrue(false)
                    break
                  case ERRORS.REQUEST_BODY_IS_EMPTY:
                    resetEmailFindId()
                    resetFindIdQuestion()
                    resetFindIdAnswer()
                    setEmailFindIdHelperText('')
                    setEmailFindIdTrue(true)
                    setFindIdAnswerHelperText('')
                    setFindIdAnswerTrue(true)

                    router.replace({
                      pathname: PATH_DONOTS_PAGE.AUTH.CONFIRM,
                      query: {
                        from: CONFIRM_TYPE.INCORRECT,
                      }
                    })
                    break
                }
              }
            })
          }
          // openBottomDialog()
          break
        case FROM_TYPE.RESET_PASSWORD:
          if (page === 0) {
            MutatePostMyIdIdentityVerificationQuestions({id: id}, {
              onSuccess: (result) => {
                if (result?.isPhoneIdentityVerificationUsageAgreed) {
                  setIsPhoneIdentityVerificationUsageAgreed(true)
                  setPage(1)
                } else {
                  setIsPhoneIdentityVerificationUsageAgreed(false)
                  // setResetPasswordQuestion1('FAVORITE_FOOD')
                  // setResetPasswordQuestion2('FAVORITE_SONG_TITLE')
                  setResetPasswordQuestion1(result?.identityVerificationQuestions[0])
                  setResetPasswordQuestion2(result?.identityVerificationQuestions[1])
                  setPage(3)
                }
              },
              onError: (error) => {
                switch (GET_ERROR_CODE(error)) {
                  case ERRORS.DATA_NOT_FOUND:
                    setIdHelperText('일치하는 회원정보가 없습니다')
                    setIdTrue(false)
                    break
                }
              }
            })
          } else if (page === 1) {
            openBottomDialog()
          } else if (page === 2) {
            /* API : 비밀번호 재설정 */
            /* Further Development */
            /* 비밀번호에 핸드폰번호, 생년월일 포함 체크 */
            mutatePostResetPw({
              ci: ci,
              id: id,
              newPassword: password
            }, {
              onSuccess: (result) => {
                if (result === '') {
                  setToastMessage({
                    type: TOAST_TYPE.BOTTOM_HEADER,
                    message: SNACKBAR_MESSAGE.RESET_PASSWORD,
                  })

                  resetResetPasswordQuestion1()
                  resetResetPasswordQuestion2()
                  resetResetPasswordAnswer1()
                  resetResetPasswordAnswer2()
                  setIdHelperText('')
                  setIdTrue(true)
                  setResetPasswordAnswer1HelperText('')
                  setResetPasswordAnswer1True(true)
                  setResetPasswordAnswer2HelperText('')
                  setResetPasswordAnswer2True(true)
                  setLoginId(id)
                  resetAuthInform()
                  resetId()
                  resetPassword()
                  resetConfirmPassword()
                  router.back()
                } else {
                  setPwTrue(false)
                  setPwHelperText(SIGN_UP_PASSWORD.INVALID)
                }
              },
              onError: (error) => {
                switch (GET_ERROR_CODE(error)) {
                  case ERRORS.PASSWORD_INCLUDE_PERSONAL_INFORMATION:
                    setPwHelperText(GET_ERROR_MESSAGE(error))
                    setPwTrue(false)
                    break
                }
              }
            })
          } else if (page === 3) {
            mutatePostVerifyIdentifyWithIdAndQas({
              id: id,
              identityVerificationQas: [{
                question: resetPasswordQuestion1,
                answerText: resetPasswordAnswer1
              }, {question: resetPasswordQuestion2, answerText: resetPasswordAnswer2}]
            }, {
              onSuccess: (result) => {
                if (result?.ci) {
                  setResetPasswordAnswer1HelperText('')
                  setResetPasswordAnswer1True(true)
                  setResetPasswordAnswer2HelperText('')
                  setResetPasswordAnswer2True(true)
                  setCi(result?.ci)
                  setPage(2)
                } else if (result?.questionsOfIncorrectAnswers?.length > 0) {
                  let isQuestion1NotMatched = false
                  let isQuestion2NotMatched = false
                  result?.questionsOfIncorrectAnswers?.map((item, index) => {
                    if (resetPasswordQuestion1 === item) {
                      isQuestion1NotMatched = true
                    }
                    if (resetPasswordQuestion2 === item) {
                      isQuestion2NotMatched = true
                    }
                  })
                  if (isQuestion1NotMatched) {
                    setResetPasswordAnswer1HelperText(GET_ERROR_MESSAGE(ERRORS.QA_NOT_MATCHED))
                    setResetPasswordAnswer1True(false)
                  } else {
                    setResetPasswordAnswer1HelperText('')
                    setResetPasswordAnswer1True(true)
                  }
                  if (isQuestion2NotMatched) {
                    setResetPasswordAnswer2HelperText(GET_ERROR_MESSAGE(ERRORS.QA_NOT_MATCHED))
                    setResetPasswordAnswer2True(false)
                  } else {
                    setResetPasswordAnswer2HelperText('')
                    setResetPasswordAnswer2True(true)
                  }
                }
              },
              onError: (error) => {
              }
            })
          }
          break
        case FROM_TYPE.GUEST_CHILD_PROFILE:
          if (page === 3) {
            setInputChildBirth(childBirth)
            setInputChildHeight(childHeight)
            setInputChildWeight(childWeight)
            setInputChildGender(childGender)
            router.back()
          } else if (page === 1) {
            const parseHeight = getFloatFixed(childHeight, 1)
            setChildHeight(parseHeight)
            setPage(page + 1)
          } else if (page === 2) {
            const parseWeight = getFloatFixed(childWeight, 1)
            setChildWeight(parseWeight)
            setPage(page + 1)
          } else {
            setPage(page + 1)
          }
          break
        case FROM_TYPE.GUEST_CHILD_ALLERGY:
          setInputChildAllergy(childAllergy)
          setInputChildAllergyList(allergyList)
          router.back()
          break
        case FROM_TYPE.GUEST_CHILD_KEYWORD:
          setInputChildKeyword(childKeyword)
          setInputChildKeywordList(keywordList)
          router.back()
          break
        default:
          setPage(page + 1)
          break
      }
    }
  }, 300)

  const openBottomDialog = () => {
    if (parseInt(idEnd) > 0 && parseInt(idEnd) < 5) {
      const newsAgencyType = changeNewsAgencyType(telecom)
      const gender = changeGenderType(idEnd)
      const birth = changeBirthFromYYMMDDToYYYYMMDD(idFront, idEnd)
      const phoneNumber = removeHyphenFromPhoneNumber(phone)
      mutatePostAuthCode({
        name: name,
        phoneNumber: phoneNumber,
        birthday: birth,
        gender: gender,
        koreaForeignerType: '1',
        newsAgencyType: newsAgencyType,
      }, {
        onSuccess: (result) => {
          setTransactionSeqNumber(result?.transactionSeqNumber)

          if (checkTransactionSeqNumberByEnv(result?.transactionSeqNumber, env)) {
            // 본인인증 정보 성공
            setDialogPage(1)
            setIsBottomDialogOpen(true)
            setBottomDialogTitle(true)
          } else {
            // 본인인증 정보 에러
            setToastMessage({
              type: TOAST_TYPE.BOTTOM_HEADER,
              message: '입력하신 정보가 올바르지 않아요.\n다시 한번 확인 후 입력해 주세요.',
            })
          }
        },
        onError: async (error) => {
          switch (GET_ERROR_CODE(error)) {
            case ERRORS.INVALID_NAME_OR_RESIDENT_REGISTRATION_NUMBER:
            case ERRORS.SERVICE_UNAVAILABLE:
            case ERRORS.MISCELLANEOUS:
              setToastMessage({
                type: TOAST_TYPE.BOTTOM_SYSTEM_ERROR,
                message: GET_ERROR_MESSAGE(error),
              })
              break
          }
        }
      })
    } else {
      // 본인인증 정보 에러
      setToastMessage({
        type: TOAST_TYPE.BOTTOM_HEADER,
        message: '입력하신 정보가 올바르지 않아요.\n다시 한번 확인 후 입력해 주세요.',
      })
    }
  }

  const completeSignUpContents = {
    grade: USER_GRADE.LV1.GRADE,
    oneButton: {
      text: COMPLETE_SIGN_UP.LEFT_BUTTON_TEXT,
      onClick: () => {
        setHackleTrack(HACKLE_TRACK.SIGNUP_COMPLETE_LATER)
        router.replace(PATH_DONOTS_PAGE.HOME).then(() => setShowCompleteSignUp(false))
      }
    },
    twoButton: {
      text: COMPLETE_SIGN_UP.RIGHT_BUTTON_TEXT,
      disabled: !!!profilePictureUrl,
      onClick: () => {
        const formData = new FormData()
        formData.append('multipartFile', profilePictureUrl)
        mutatePutMyProfilePicture(formData, {
          onSuccess: () => {
            setHackleTrack(HACKLE_TRACK.SIGNUP_COMPLETE_PROFILE_COMPLETE)
            setShowCompleteSignUp(false)
            setShowLevelUpChoco(true)
          }
        })
      },
    }
  }

  const levelUpChocoContents = {
    grade: USER_GRADE.LV2.GRADE,
    oneButton: {
      text: LEVEL_UP_CHOCO.BUTTON_TEXT,
      onClick: () => {
        router.replace(PATH_DONOTS_PAGE.HOME).then(() => setShowLevelUpChoco(false))
      }
    }
  }

  const onTrackSignUpComplete = () => {
    setHackleTrack(HACKLE_TRACK.SIGNUP_COMPLETE)
  }

  useEffect(() => {
    if (handleBackKey) {
      backButtonControl()
    }
  }, [handleBackKey])

  useEffect(() => {
    topRef?.current?.focus()
  }, [page])

  return (
    <MainLayout>
      <Container disableGutters sx={{px: '20px'}}>
        {/* 헤더 */}
        <HeaderTitleLayout topRef={topRef} backTabIndex={0} closeTabIndex={0} handleClick={handleCloseClick}/>

        {/* 콘텐츠 */}
        <Box sx={{pt: `${HEADER.H_MOBILE}px`, pb: '30px'}}>

          <Box sx={{textAlign: 'left', py: '30px'}}>

            {/* 회원 가입 - ID 가입 */}
            {fromType === FROM_TYPE.SIGN_UP_ID && <SignUpIdSectionList page={page} setPage={setPage}/>}

            {/* 회원 가입 - ID 가입 - iOS */}
            {fromType === FROM_TYPE.SIGN_UP_ID_IOS && <SignUpIdIosSectionList page={page}/>}

            {/* 회원 가입 - SNS 가입 */}
            {fromType === FROM_TYPE.SIGN_UP_SNS && <SignUpSnsSectionList page={page} setPage={setPage}/>}

            {/* 아이디 찾기 */}
            {fromType === FROM_TYPE.FIND_ID && <FindIdSectionList page={page}/>}

            {/* 비밀번호 재설정 */}
            {fromType === FROM_TYPE.RESET_PASSWORD && <ResetPasswordSectionList page={page} setPage={setPage}/>}

            {/* 둘러보기 - 아이 프로필 입력 */}
            {fromType === FROM_TYPE.GUEST_CHILD_PROFILE && <ChildProfileSectionList page={page}/>}

            {/* 둘러보기 - 알레르기 정보 입력 */}
            {fromType === FROM_TYPE.GUEST_CHILD_ALLERGY && <ChildAllergyInputForm isGuest={true}/>}

            {/* 둘러보기 - 키워드 정보 입력 */}
            {fromType === FROM_TYPE.GUEST_CHILD_KEYWORD && <ChildKeywordInputForm isGuest={true}/>}

            {/* 아이 추가 정보 입력 */}
            {fromType === FROM_TYPE.ADDITIONAL_CHILD_INFORM && <AdditionalChildInformSectionList page={page}/>}

          </Box>

          {/* 푸터 */}
          <Container
            disableGutters
            maxWidth={'xs'}
            sx={{
              position: 'fixed',
              bottom: 0,
              bgcolor: 'white',
              p: '20px',
              ...sxFixedCenterMainLayout
            }}
          >
            <Button
              ref={bottomRef}
              tabIndex={0}
              fullWidth
              variant="contained"
              disabled={isBottomBtnDisabled}
              onClick={handleConfirmClick}
              disableFocusRipple
              sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                p: '12px 50px',
                height: '52px',
                fontStyle: 'normal',
                fontWeight: '700',
                fontSize: '16px',
                lineHeight: '28px',
                textAlign: 'center',
                color: '#FFFFFF',
                '&.Mui-disabled': {
                  color: '#B3B3B3',
                  backgroundColor: '#E8E8E8',
                },
                '&.Mui-disabled:hover': {
                  backgroundColor: '#E6E6E6',
                },
                '&:hover': {
                  boxShadow: 'none',
                  backgroundColor: 'primary.main',
                },
                ...clickBorderNone
              }}
            >
              {bottomBtnText}
            </Button>
          </Container>
        </Box>
      </Container>

      {/* 회원가입 완료 다이얼로그 */}
      {showCompleteSignUp &&
        <CompleteDialog isOpen={showCompleteSignUp} title={COMPLETE_SIGN_UP.TITLE}
                        onClick={onTrackSignUpComplete} {...completeSignUpContents}>
          <CompleteSignUpCard profilePictureUrl={profilePictureUrl} setProfilePictureUrl={setProfilePictureUrl}/>
        </CompleteDialog>}

      {/* 등급업 다이얼로그 - 초코 */}
      {showLevelUpChoco &&
        <CompleteDialog isOpen={showLevelUpChoco} title={LEVEL_UP_CHOCO.TITLE} {...levelUpChocoContents}>
          <CompleteLevelUpChocoCard/>
        </CompleteDialog>}

      {isLoadingSignUpId || isLoadingPutMyProfilePicture && <LoadingScreen/>}
    </MainLayout>
  )
}
