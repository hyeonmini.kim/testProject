import {Box, Button, Container, Typography} from "@mui/material";
import * as React from "react";
import {useEffect, useState} from "react";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import {useRouter} from "next/router";
import {CONFIRM_SIGN_UP, CONFIRM_TYPE} from "../../constant/sign-up/SignUp";
import useNativeBackKey from "../../hooks/useNativeBackKey";
import Image from "../../components/image";
import MainLayout, {sxFixedCenterMainLayout} from "../../layouts/main/MainLayout";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {clickBorderNone} from "../../constant/common/Common";

export default function ConfirmSignUp() {
  const router = useRouter()
  const {handleBackKey} = useNativeBackKey()

  const [image, setImage] = useState('')
  const [title, setTitle] = useState('')
  const [detail, setDetail] = useState('')

  const url = PATH_DONOTS_PAGE.HOME
  const from = router.query.from
  const id = router.query.id
  const createdAt = router.query.createdAt

  useEffect(() => {
    switch (from) {
      case CONFIRM_TYPE.CORRECT:
        setImage(CONFIRM_SIGN_UP.IMAGE_CHECK)
        setTitle(CONFIRM_SIGN_UP.CORRECT.title)
        setDetail(`${id}\n가입일자 ${createdAt}`)
        break
      case CONFIRM_TYPE.INCORRECT:
        setImage(CONFIRM_SIGN_UP.IMAGE_NO)
        setTitle(CONFIRM_SIGN_UP.INCORRECT.title)
        setDetail(CONFIRM_SIGN_UP.INCORRECT.detail)
        break
      case CONFIRM_TYPE.REJECT:
        setImage(CONFIRM_SIGN_UP.IMAGE_NO)
        setTitle(CONFIRM_SIGN_UP.REJECT)
        break
      case CONFIRM_TYPE.SUCCESS:
        setImage(CONFIRM_SIGN_UP.IMAGE_CHECK)
        setTitle(CONFIRM_SIGN_UP.SUCCESS.title)
        setDetail(CONFIRM_SIGN_UP.SUCCESS.detail)
        setHackleTrack(HACKLE_TRACK.SIGNUP_COMPLETE)
        break
      case CONFIRM_TYPE.ADD_CHILDREN:
        setImage(CONFIRM_SIGN_UP.IMAGE_CHECK)
        setTitle(CONFIRM_SIGN_UP.ADD_CHILDREN)
        break
      default:
        break
    }
  }, [])

  const onClick = () => {
    switch (from) {
      case CONFIRM_TYPE.INCORRECT:
      case CONFIRM_TYPE.SUCCESS:
        router.replace(url)
        break
      case CONFIRM_TYPE.CORRECT:
      case CONFIRM_TYPE.REJECT:
      case CONFIRM_TYPE.ADD_CHILDREN:
        router.back()
        break
      default:
        break
    }
  }

  useEffect(() => {
    if (handleBackKey) {
      onClick()
    }
  }, [handleBackKey])

  return (
    <MainLayout>
      <Container maxWidth={'xs'} sx={{display: 'flex', height: '100%'}}>
        {/* Contents */}
        <Box sx={{px: '20px', margin: 'auto', textAlign: '-webkit-center'}}>
          <Image alt={''} src={image} sx={{width: '80px', height: '80px'}}/>
          <Box sx={{marginTop: '30px', color: '#222222'}}>
            <Typography variant={'b_18_m_1l'}>
              {title}
            </Typography>
          </Box>
          <Box sx={{marginTop: '12px', color: '#666666'}}>
            <Typography variant={'b2_14_r'}>
              {detail}
            </Typography>
          </Box>
        </Box>

        {/* Footer */}
        <Container
          maxWidth={'xs'}
          disableGutters
          sx={{
            position: 'fixed',
            bottom: '0',
            padding: '20px',
            ...sxFixedCenterMainLayout
          }}
        >
          <Button
            tabIndex={0}
            disableFocusRipple
            fullWidth
            type="submit"
            variant="contained"
            sx={{
              height: '52px',
              fontSize: '16px',
              fontWeight: '700',
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: 'primary.main',
              },
              ...clickBorderNone
            }}
            onClick={onClick}
          >
            확인
          </Button>
        </Container>
      </Container>
    </MainLayout>
  );
}