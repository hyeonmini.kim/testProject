import {useEffect} from "react";
import {useRecoilState, useSetRecoilState} from "recoil";
import {useRouter} from "next/router";
import {postGetTokensMutate} from "../../api/authApi";
import {setTokens} from "../../auth/utils";
import {accessTokenSelector, refreshTokenSelector, socialAccountIdSelector} from "../../recoil/selector/auth/accessTokenSelector";
import {postAddSnsAccountInMyMutate} from "../../api/myApi";
import {myCallbackAddSnsSelector} from "../../recoil/selector/my/popup/myPopupSelector";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import {useBefuAuthContext} from "../../auth/useAuthContext";
import LoadingScreen from "../../components/common/LoadingScreen";
import {getCookie, removeCookie} from "../../utils/storageUtils";
import {COMMON_STR} from "../../constant/common/Common";
import {commonAgreeOpenSelector} from "../../recoil/selector/auth/agreeDetailSelector";
import {signUpTypeSelector} from "../../recoil/selector/sign-up/signUpSelector";

export default function Callback() {
  const setAccessToken = useSetRecoilState(accessTokenSelector)
  const setRefreshToken = useSetRecoilState(refreshTokenSelector)
  const setMyCallbackAddSns = useSetRecoilState(myCallbackAddSnsSelector)
  const [socialAccountId, setSocialAccountId] = useRecoilState(socialAccountIdSelector)

  const [signUpType, setSignUpType] = useRecoilState(signUpTypeSelector)
  const [openAgreeDlg, setOpenAgreeDlg] = useRecoilState(commonAgreeOpenSelector)

  const router = useRouter()
  const {getMemberKey} = useBefuAuthContext()
  const {getUser} = useBefuAuthContext()

  const {mutate: mutatePostGetTokens} = postGetTokensMutate()
  const {mutateAsync: mutatePostAddSnsAccountInMy} = postAddSnsAccountInMyMutate()

  useEffect(() => {
    if (router.query.result === COMMON_STR.OK) {
      const uuid = getCookie(COMMON_STR.UUID)
      const ci = getCookie(COMMON_STR.CI)
      const callbackType = getCookie(COMMON_STR.CALLBACK_TYPE)

      removeCookie(COMMON_STR.CALLBACK_TYPE)

      // TODO. getMember API로 CI값 받아와서 처리해야함.

      mutatePostGetTokens({
        uuid: uuid
      }, {
        onSuccess: async (result) => {
          setAccessToken(result?.token)
          setRefreshToken(result?.refreshToken)
          setSocialAccountId(result?.socialAccountId)
          setTokens(result?.token, result?.refreshToken)

          if (callbackType === COMMON_STR.ID) {
            // MAIN - SNS 계정 회원가입 또는 로그인
            if (result?.isAlreadyConnectedSocialAccount) {
              // SNS 기존 회원
              getUser(result?.token)

              await router.replace({pathname: PATH_DONOTS_PAGE.HOME}, undefined, {shallow: true})
            } else {
              // SNS 신규
              await router.replace({query: null})

              setOpenAgreeDlg(true)
              setSignUpType(COMMON_STR.SNS)
              removeCookie(COMMON_STR.UUID)
            }
          } else {
            // MY - SNS 계정 연동
            const param = {
              ciValue: {
                ci: ci
              },
              token: result?.token
            }

            await mutatePostAddSnsAccountInMy(param, {
              onSuccess: async (result) => {
                removeCookie(COMMON_STR.UUID)
                removeCookie(COMMON_STR.CI)

                await getMemberKey(result?.token).then(() => {
                  setMyCallbackAddSns(true)
                  router.replace(PATH_DONOTS_PAGE.MY.SELF)
                })
              }
            })
          }
        }
      })
    }
  }, [router])

  return (
    <>
      <LoadingScreen/>
    </>
  );
}
