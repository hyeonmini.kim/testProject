import * as React from "react";
import {useEffect, useRef} from "react";
import {Container} from "@mui/material";
import HomeLayout from "../../layouts/home/HomeLayout";
import {getNotiList, getPopupList, updateNotiReadAll} from "../../api/homeApi";
import useStackNavigation from "../../hooks/useStackNavigation";
import {STACK} from "../../constant/common/StackNavigation";
import useNativeBackKey from "../../hooks/useNativeBackKey";
import {useRecoilState, useResetRecoilState} from "recoil";
import {notificationHasNewItemSelector, notificationOpenSelector} from "../../recoil/selector/home/notificationSelector";
import PersonalizationSection from "../../sections/home/PersonalizationSection";
import {dynamicIslandExpandSelector} from "../../recoil/selector/home/dynamicIslandSelector";
import MainLayout from "../../layouts/main/MainLayout";
import {homeShowInfoBannerSelector} from "../../recoil/selector/home/homeSelector";

import {dialogShowDialogSelector, showMembershipDialogSelector} from "../../recoil/selector/common/dialogSelector";
import NotificationSection from "../../sections/home/NotificationSection";
import LayerPopupSection from "../../sections/home/LayerPopupSection";
import {useQueryClient} from "react-query";
import {labCommentShowCommentModifySelector} from "../../recoil/selector/lab/labSelector";
import {getLocalStorage, setLocalStorage} from "../../utils/storageUtils";
import {TIME} from "../../constant/common/Time";
import {layerPopupClickedSelector, layerPopupSelector, showLayerPopupSelector} from "../../recoil/selector/home/layerPopupSelector";
import {LAYER_POPUP_BUTTON_TEXT} from "../../constant/home/LayerPopup";
import {iFrameUrlSelector} from "../../recoil/selector/common/iFrameSelector";
import IFrameDialog from "../../components/common/IFrameDialog";
import {settingShowPersonalTermsSelector, settingShowServiceTermsSelector} from "../../recoil/selector/my/settingSelector";

HomePage.getLayout = (page) => <HomeLayout>{page}</HomeLayout>;

export default function HomePage() {
  const [expand, setExpand] = useRecoilState(dynamicIslandExpandSelector);
  const {navigation, preFetch} = useStackNavigation()

  const [showNotification, setShowNotification] = useRecoilState(notificationOpenSelector)
  const [showInfoBanner, setShowInfoBanner] = useRecoilState(homeShowInfoBannerSelector)
  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)
  const [showLayerPopup, setShowLayerPopup] = useRecoilState(showLayerPopupSelector)
  const [showModify, setShowModify] = useRecoilState(labCommentShowCommentModifySelector)
  const [showMembership, setShowMemberShip] = useRecoilState(showMembershipDialogSelector)
  const [showServiceTerms, setShowServiceTerms] = useRecoilState(settingShowServiceTermsSelector)
  const [showPersonalTerms, setShowPersonalTerms] = useRecoilState(settingShowPersonalTermsSelector)

  const [iframeUrl, setIframeUrl] = useRecoilState(iFrameUrlSelector)
  const [hasNewItem, setHasNewItem] = useRecoilState(notificationHasNewItemSelector)
  const [layerPopup, setLayerPopup] = useRecoilState(layerPopupSelector)
  const resetLayerPopup = useResetRecoilState(layerPopupSelector)
  const [layerPopupClicked, setLayerPopupClicked] = useRecoilState(layerPopupClickedSelector)

  const {data: dataNotification} = getNotiList()
  const {mutate: mutateUpdateNotiReadAll} = updateNotiReadAll()
  const {data: dataPopupList} = getPopupList()

  const mainRef = useRef(null)
  const queryClient = useQueryClient()

  useEffect(() => {
    if (dataPopupList?.length && !getLocalStorage('isLayerPopupNotOpenOneDay') && !layerPopupClicked) {
      setLayerPopup({
        layerPopupList: dataPopupList,
        handleDialogPaper: {text: LAYER_POPUP_BUTTON_TEXT.NO_MORE_SHOW_TODAY, onClick: noMoreShowToday},
        handleDialogPaper2: {text: LAYER_POPUP_BUTTON_TEXT.CLOSE, onClick: onClose},
      })
    }
  }, [dataPopupList])

  const noMoreShowToday = () => {
    setLocalStorage('isLayerPopupNotOpenOneDay', true, TIME.MS_ONE_DAY)
    setShowLayerPopup(false)
  }

  const onClose = () => {
    setShowLayerPopup(false)
    setLayerPopupClicked(true)
  }

  useEffect(() => {
    const fetch = preFetch(STACK.HOME.TYPE)
    if (fetch) {
      if (fetch?.open) {
        setShowNotification(fetch?.open)
      } else if (fetch?.showNotification) {
        setShowNotification(fetch?.showNotification)
      }
    }
    return () => {
      setShowNotification(false)
      if (mainRef?.current) {
        window.removeEventListener('scroll', () => {
        })
      }
    }
  }, [])

  useEffect(() => {
    if (mainRef?.current) {
      let lastScrollY = 0;
      mainRef?.current?.addEventListener('scroll', () => {
        const scrollY = mainRef?.current?.scrollTop;
        const direction = scrollY > lastScrollY ? "Down" : "Up"
        lastScrollY = scrollY;
        if (direction === "Down") {
          setExpand(false)
        }
      });
    }
  }, [mainRef?.current]);

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      // 다이얼로그
      if (showDialog) {
        setShowDialog(false)
      }
      // 아이프레임 다이얼로그
      else if (!!iframeUrl) {
        setIframeUrl('')
      }
      // 레이어팝업
      else if (showLayerPopup) {
        setShowLayerPopup(false)
        setLayerPopupClicked(true)
      }
      // 알림 목록 화면
      else if (showNotification) {
        handleCloseNotification()
      }
      // 배너 화면
      else if (showInfoBanner) {
        setShowInfoBanner(false)
      }
      // 멤버십 다이얼로그
      else if (showMembership) {
        setShowMemberShip(false)
      }
      // 이용약관 다이얼로그
      else if (showServiceTerms) {
        setShowServiceTerms(false)
      }
      // 개인정보처리방침 다이얼로그
      else if (showPersonalTerms) {
        setShowPersonalTerms(false)
      }
      // 홈 화면
      else {
        try {
          window.flutter_inappwebview.callHandler('openNativeAppTerminate')
            .then(async function (result) {
            })
        } catch (e) {
        }
      }
    }
  }, [handleBackKey])

  useEffect(() => {
    if (dataNotification?.new_noti_list?.length) {
      setHasNewItem(true)
    } else {
      setHasNewItem(false)
    }
  }, [dataNotification])

  const handleCloseNotification = () => {
    mutateUpdateNotiReadAll(null, {
      onSettled: () => {
        queryClient.removeQueries('getNotiList')
      }
    })
    setShowNotification(false)
  }

  return (
    <>
      <MainLayout mainRef={mainRef}>
        <Container maxWidth={'xs'} disableGutters sx={{pt: '20px', px: '20px', pb: '64px'}}>

          {/* 홈 & 알림*/}
          {showNotification
            ? <NotificationSection items={dataNotification} onBack={handleCloseNotification}/>
            : <PersonalizationSection mainRef={mainRef}/>
          }

          {/* 레이어 팝업 */}
          <LayerPopupSection/>

          {/* 아이프레임 다이얼로그 */}
          <IFrameDialog
            open={!!iframeUrl}
            url={iframeUrl}
            onBack={() => setIframeUrl('')}
          />

        </Container>
      </MainLayout>

    </>
  )
}