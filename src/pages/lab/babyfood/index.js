import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {Box, Container, Divider} from "@mui/material";
import LabLayout from "../../../layouts/lab/LabLayout";
import MainLayout from "../../../layouts/main/MainLayout";
import MainSection from "../../../sections/lab/MainSection";
import RankingSection from "../../../sections/lab/RankingSection";
import CommentSection from "../../../sections/lab/CommentSection";
import ServeySection from "../../../sections/lab/ServeySection";
import {BABYFOOD_ACTIVE_STEP, LAB_TYPE, SURVEY_TYPE} from "../../../constant/lab/LabCommon";
import RankingScreenSection from "../../../sections/lab/RankingScreenSection";
import {getPollRegistYn, getRegistSurveyYn, postPollRegistMutate} from "../../../api/labApi";
import useNativeBackKey from "../../../hooks/useNativeBackKey";
import {useRecoilState, useResetRecoilState} from "recoil";
import {
  labBabyFoodActiveStepSelector,
  labCommentCheckSelector,
  labCommentFetchSelector,
  labCommentOrderSelector,
  labCommentShowCommentModifySelector,
  labCommentShowCommentViewSelector,
  labIsItemSelectedSelector,
  labRecommendListSelector,
  labScoreSelector
} from "../../../recoil/selector/lab/labSelector";
import useStackNavigation from "../../../hooks/useStackNavigation";
import {dialogShowDialogSelector} from "../../../recoil/selector/common/dialogSelector";
import BottomDialog from "../../../layouts/lab/BottomDialog";
import LabRecommendContents from "../../../components/lab/LabRecommendContents";
import LoadingScreen from "../../../components/common/LoadingScreen";
import {useQueryClient} from "react-query";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {STACK} from "../../../constant/common/StackNavigation";
import {INIT} from "../../../constant/common/Common";
import {getLocalStorage} from "../../../utils/storageUtils";
import LabRankingCardSkeleton from "../../../components/lab/LabRankingCardSkeleton";

BabyfoodPage.getLayout = (page) => <LabLayout>{page}</LabLayout>;

export default function BabyfoodPage() {
  const mainRef = useRef(null)

  const [fetchData, setFetchData] = useRecoilState(labCommentFetchSelector)
  const resetFetchData = useResetRecoilState(labCommentFetchSelector)
  const [check, setCheck] = useRecoilState(labCommentCheckSelector)
  const resetCheck = useResetRecoilState(labCommentCheckSelector)
  const [order, setOrder] = useRecoilState(labCommentOrderSelector)
  const resetOrder = useResetRecoilState(labCommentOrderSelector)

  const [showSurvey, setShowSurvey] = useState(false)
  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)
  const [showModify, setShowModify] = useRecoilState(labCommentShowCommentModifySelector)
  const [showView, setShowView] = useRecoilState(labCommentShowCommentViewSelector)

  const [labBabyFoodActiveStep, setLabBabyFoodActiveStep] = useRecoilState(labBabyFoodActiveStepSelector)

  const resetLabRecommendList = useResetRecoilState(labRecommendListSelector)
  const [labIsItemSelected, setLabIsItemSelected] = useRecoilState(labIsItemSelectedSelector)
  const resetLabIsItemSelected = useResetRecoilState(labIsItemSelectedSelector)

  const [score, setScore] = useRecoilState(labScoreSelector)
  const resetScore = useResetRecoilState(labScoreSelector)

  const [openBottomDialog, setOpenBottomDialog] = useState(false)
  const [isShow, setIsShow] = useState(true)
  const [bottomDialogOpened, setBottomDialogOpened] = useState(true)

  const queryClient = useQueryClient()

  const {navigation, preFetch} = useStackNavigation()
  useEffect(() => {
    const fetch = preFetch(STACK.LAB.TYPE)
    if (fetch) {
      setFetchData(fetch)
      setCheck(fetch?.check)
      setOrder(fetch?.order)
    } else {
      setFetchData(null)
    }
    setHackleTrack(HACKLE_TRACK.BABYFOOD_MAIN)

    return () => {
      resetCheck()
      resetOrder()
      resetFetchData()
    }
  }, [])

  const handleShowRanking = () => {
    setHackleTrack(HACKLE_TRACK.BABYFOOD_BTN)
    setShowSurvey(LAB_TYPE.BABY_FOOD)
  }

  const handleBackButton = () => {
    if (labBabyFoodActiveStep === BABYFOOD_ACTIVE_STEP.BRAND) {
      setShowSurvey(false)
    } else {
      setLabBabyFoodActiveStep((prevActiveStep) => prevActiveStep - 1);
    }
  }

  const backButtonClick = () => {
    if (showBottomDialog === "N" && isShow && bottomDialogOpened) {
      setOpenBottomDialog(true)
      setBottomDialogOpened(false)
    } else {
      navigation?.back();
    }
  }

  const handleClose = () => {
    resetLabRecommendList()
    resetLabIsItemSelected()
    resetScore()
    setOpenBottomDialog(false)
  }

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      // 다이얼로그
      if (showDialog) {
        setShowDialog(false)
      }
      // 설문 화면
      else if (showSurvey) {
        handleBackButton()
      }
      // 댓글 수정 화면
      else if (showModify) {
        setShowModify(false)
      }
      // 기타
      else {
        if (openBottomDialog) {
          handleClose()
        } else {
          backButtonClick()
        }
      }
    }
  }, [handleBackKey])

  useEffect(() => {

    if (openBottomDialog) {
      setHackleTrack(HACKLE_TRACK.ETC_POPUP)
    }

    resetLabRecommendList()
    resetLabIsItemSelected()
    resetScore()
    setBottomDialogOpened(false)

    return () => {
      resetLabRecommendList()
      resetLabIsItemSelected()
      resetScore()
    }
  }, [openBottomDialog])

  useEffect(() => {
    setBottomDialogOpened(true)
    if (getLocalStorage('isLabBottomDialogNotOpenOneDay')) {
      setIsShow(false)
    } else {
      setIsShow(true)
    }

    return () => {
      setOpenBottomDialog(false)
      setBottomDialogOpened(true)
    }
  }, [])

  const {data: showRanking} = getRegistSurveyYn({survey_type: SURVEY_TYPE.SUR02.code})
  const {data: showBottomDialog} = getPollRegistYn({survey_type: SURVEY_TYPE.SUR02.code})

  const {mutate: mutatePostPollRegist, isLoading} = postPollRegistMutate()

  const handleButtonClick = () => {
    setHackleTrack(HACKLE_TRACK.ETC_BTN)

    mutatePostPollRegist({
      survey_type: SURVEY_TYPE.SUR02.code,
      score: score,
    }, {
      onSuccess: () => {
        queryClient?.invalidateQueries('getPollRegistYn')
        handleClose()
        navigation.back()
      }
    })
  }

  const showComment = () => {
    if (fetchData !== INIT && !showSurvey) return true
    return false
  }

  return (
    <MainLayout mainRef={mainRef}>
      <Container maxWidth={'xs'} disableGutters sx={{mb: '71px'}}>

        {/* 메인 섹션 */}
        <MainSection type={LAB_TYPE.BABY_FOOD} backButtonClick={backButtonClick}/>

        {/* 랭킹 섹션 */}
        <Box sx={{position: 'relative'}}>
          {showRanking === 'Y' && <RankingSection type={LAB_TYPE.BABY_FOOD} surveyTypeParam={SURVEY_TYPE.SUR02.code}/>}
          {showRanking === 'N' && <RankingScreenSection type={LAB_TYPE.BABY_FOOD} onClick={handleShowRanking}/>}
          {!showRanking && <LabRankingCardSkeleton type={LAB_TYPE.BABY_FOOD}/>}
        </Box>

        {(!showRanking || showRanking === 'Y') && <Divider sx={{mt: '60px', mb: '50px', height: '1px', background: '#E6E6E6'}}/>}

        {/* 댓글 섹션 */}
        {showComment() && <CommentSection mainRef={mainRef} type={LAB_TYPE.BABY_FOOD}/>}

        {/* 설문 다이얼로그 섹션*/}
        <ServeySection open={showSurvey} onClose={() => setShowSurvey(false)} handleBackButton={handleBackButton}/>

      </Container>

      {/* 추천 다이얼로그 */}
      <BottomDialog open={openBottomDialog} onClose={handleClose}>
        <LabRecommendContents onClose={handleClose} handleButtonClick={handleButtonClick}/>
      </BottomDialog>

      {isLoading && <LoadingScreen/>}
    </MainLayout>
  )
}