import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import LabLayout from "../../layouts/lab/LabLayout";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import CommentView from "../../sections/lab/comment/CommentView";
import useNativeBackKey from "../../hooks/useNativeBackKey";
import useStackNavigation from "../../hooks/useStackNavigation";
import {CALLBACK_TYPE} from "../../constant/lab/LabCommon";
import {useRecoilState} from "recoil";
import {
  labCommentCommentKeySelector,
  labCommentItemSelector,
  labCommentPostKeySelector,
  labCommentShowCommentModifySelector
} from "../../recoil/selector/lab/labSelector";
import MainLayout from "../../layouts/main/MainLayout";

LabPage.getLayout = (page) => <LabLayout>{page}</LabLayout>;

export default function LabPage() {
  const [showDetail, setShowDetail] = useState(false)

  const [item, setItem] = useRecoilState(labCommentItemSelector)
  const [postKey, setPostKey] = useRecoilState(labCommentPostKeySelector)
  const [commentKey, setCommentKey] = useRecoilState(labCommentCommentKeySelector)
  const [showModify, setShowModify] = useRecoilState(labCommentShowCommentModifySelector)

  const {query, isReady, push} = useRouter();
  const {navigation, preFetch} = useStackNavigation()

  useEffect(() => {
    if (isReady) {
      const {d_post, d_comment} = query
      if (d_post) {
        setShowDetail(true)
        setPostKey(d_post)
        setCommentKey(d_comment)
      } else {
        push(PATH_DONOTS_PAGE.HOME);
      }
    }
  }, [isReady]);

  useEffect(() => {
    return () => {
      setItem(null)
      setPostKey(null)
      setCommentKey(null)
    }
  }, [])

  // 안드로이드 백버튼 처리
  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      // 수정 화면
      if (showModify) {
        setShowModify(false)
      }
      // 상세 화면
      else if (showDetail) {
        handleBack({
          type: CALLBACK_TYPE.CHANGE,
          item: item,
        })
      }
    }
  }, [handleBackKey]);

  const handleBack = (callback) => {
    navigation.back(callback)
  }

  return (
    <MainLayout>
      {showDetail && <CommentView onBack={handleBack}/>}
    </MainLayout>
  )
}