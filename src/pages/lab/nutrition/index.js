import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {Box, Button, Container, Divider, Grid, Typography} from "@mui/material";
import LabLayout from "../../../layouts/lab/LabLayout";
import MainLayout from "../../../layouts/main/MainLayout";
import MainSection from "../../../sections/lab/MainSection";
import RankingSection from "../../../sections/lab/RankingSection";
import CommentSection from "../../../sections/lab/CommentSection";
import ServeySection from "../../../sections/lab/ServeySection";
import {LAB_NUTRITION_CATEGORY_ITEM, LAB_TYPE, SURVEY_TYPE} from "../../../constant/lab/LabCommon";
import RankingScreenSection from "../../../sections/lab/RankingScreenSection";
import {getPollRegistYn, getRegistSurveyYn, postPollRegistMutate} from "../../../api/labApi";
import {
  labCommentCheckSelector,
  labCommentFetchSelector,
  labCommentOrderSelector,
  labCommentShowCommentModifySelector,
  labCommentShowCommentViewSelector,
  labIsItemSelectedSelector,
  labIsNutritionSUR03EtcSelectedSelector,
  labIsNutritionSUR04EtcSelectedSelector,
  labIsNutritionSUR05EtcSelectedSelector,
  labIsSUR03SelectedSelector,
  labIsSUR04SelectedSelector,
  labIsSUR05SelectedSelector,
  labNutritionActiveStepListSelector,
  labNutritionActiveStepSelector,
  labRecommendListSelector,
  labScoreSelector,
  labSelectedSurveyTypeSelector
} from "../../../recoil/selector/lab/labSelector";
import {useRecoilState, useResetRecoilState} from "recoil";
import useStackNavigation from "../../../hooks/useStackNavigation";
import useNativeBackKey from "../../../hooks/useNativeBackKey";
import LabRecommendContents from "../../../components/lab/LabRecommendContents";
import BottomDialog from "../../../layouts/lab/BottomDialog";
import LoadingScreen from "../../../components/common/LoadingScreen";
import {useQueryClient} from "react-query";
import {STACK} from "../../../constant/common/StackNavigation";
import {clickBorderNone, INIT} from "../../../constant/common/Common";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {getLocalStorage} from "../../../utils/storageUtils";
import LabRankingCardSkeleton from "../../../components/lab/LabRankingCardSkeleton";
import {dialogShowDialogSelector} from "../../../recoil/selector/common/dialogSelector";
import {handleEnterPress} from "../../../utils/onKeyDownUtils";

NutritionPage.getLayout = (page) => <LabLayout>{page}</LabLayout>;

export default function NutritionPage() {
  const mainRef = useRef(null)

  const [fetchData, setFetchData] = useRecoilState(labCommentFetchSelector)
  const resetFetchData = useResetRecoilState(labCommentFetchSelector)
  const [check, setCheck] = useRecoilState(labCommentCheckSelector)
  const resetCheck = useResetRecoilState(labCommentCheckSelector)
  const [order, setOrder] = useRecoilState(labCommentOrderSelector)
  const resetOrder = useResetRecoilState(labCommentOrderSelector)

  const [showSurvey, setShowSurvey] = useState(false)
  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)
  const [showModify, setShowModify] = useRecoilState(labCommentShowCommentModifySelector)
  const [showView, setShowView] = useRecoilState(labCommentShowCommentViewSelector)

  const [selectedSurveyType, setSelectedSurveyType] = useRecoilState(labSelectedSurveyTypeSelector);
  const [labNutritionActiveStep, setLabNutritionActiveStep] = useRecoilState(labNutritionActiveStepSelector)
  const [labNutritionActiveStepList, setLabNutritionActiveStepList] = useRecoilState(labNutritionActiveStepListSelector)

  const [labIsSUR03Selected, setLabIsSUR03Selected] = useRecoilState(labIsSUR03SelectedSelector)
  const [labIsSUR04Selected, setLabIsSUR04Selected] = useRecoilState(labIsSUR04SelectedSelector)
  const [labIsSUR05Selected, setLabIsSUR05Selected] = useRecoilState(labIsSUR05SelectedSelector)

  const [labIsNutritionSUR03EtcSelected, setLabIsNutritionSUR03EtcSelected] = useRecoilState(labIsNutritionSUR03EtcSelectedSelector)
  const [labIsNutritionSUR04EtcSelected, setLabIsNutritionSUR04EtcSelected] = useRecoilState(labIsNutritionSUR04EtcSelectedSelector)
  const [labIsNutritionSUR05EtcSelected, setLabIsNutritionSUR05EtcSelected] = useRecoilState(labIsNutritionSUR05EtcSelectedSelector)

  const resetLabRecommendList = useResetRecoilState(labRecommendListSelector)
  const [labIsItemSelected, setLabIsItemSelected] = useRecoilState(labIsItemSelectedSelector)
  const resetLabIsItemSelected = useResetRecoilState(labIsItemSelectedSelector)

  const [score, setScore] = useRecoilState(labScoreSelector)
  const resetScore = useResetRecoilState(labScoreSelector)

  const [openBottomDialog, setOpenBottomDialog] = useState(false)
  const [isShow, setIsShow] = useState(true)
  const [bottomDialogOpened, setBottomDialogOpened] = useState(true)

  const queryClient = useQueryClient()

  const {navigation, preFetch} = useStackNavigation()
  useEffect(() => {
    const fetch = preFetch(STACK.LAB.TYPE)
    if (fetch) {
      setFetchData(fetch)
      setCheck(fetch?.check)
      setOrder(fetch?.order)
    } else {
      setFetchData(null)
    }
    setSelectedSurveyType(SURVEY_TYPE.SUR03)
    setHackleTrack(HACKLE_TRACK.NUTRIENTS_MAIN)

    return () => {
      resetCheck()
      resetOrder()
      resetFetchData()
      setSelectedSurveyType(SURVEY_TYPE.SUR03)
    }
  }, [])

  const handleShowRanking = () => {
    setHackleTrack(HACKLE_TRACK.NUTRIENTS_BTN)
    setShowSurvey(LAB_TYPE.NUTRITION)
  }

  const handleBackButton = () => {
    if (labNutritionActiveStep === labNutritionActiveStepList.SELECT) {
      setShowSurvey(false)
    } else {
      switch (labNutritionActiveStep) {
        case labNutritionActiveStepList.SUR03_BRAND :
          setLabNutritionActiveStep(labNutritionActiveStepList.SELECT)
          break;
        case labNutritionActiveStepList.SUR03_ETC_REASON :
          setLabNutritionActiveStep(labNutritionActiveStepList.SUR03_BRAND)
          break;
        case labNutritionActiveStepList.SUR03_REASON :
          if (labIsNutritionSUR03EtcSelected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR03_ETC_REASON)
          } else {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR03_BRAND)
          }
          break;
        case labNutritionActiveStepList.SUR04_BRAND :
          if (labIsSUR03Selected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR03_REASON)
          } else {
            setLabNutritionActiveStep(labNutritionActiveStepList.SELECT)
          }
          break;
        case labNutritionActiveStepList.SUR04_ETC_REASON :
          setLabNutritionActiveStep(labNutritionActiveStepList.SUR04_BRAND)
          break;
        case labNutritionActiveStepList.SUR04_REASON :
          if (labIsNutritionSUR04EtcSelected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR04_ETC_REASON)
          } else {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR04_BRAND)
          }
          break;
        case labNutritionActiveStepList.SUR05_BRAND :
          if (labIsSUR04Selected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR04_REASON)
          } else if (labIsSUR03Selected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR03_REASON)
          } else {
            setLabNutritionActiveStep(labNutritionActiveStepList.SELECT)
          }
          break;
        case labNutritionActiveStepList.SUR05_ETC_REASON :
          setLabNutritionActiveStep(labNutritionActiveStepList.SUR05_BRAND)
          break;
        case labNutritionActiveStepList.SUR05_REASON :
          if (labIsNutritionSUR05EtcSelected) {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR05_ETC_REASON)
          } else {
            setLabNutritionActiveStep(labNutritionActiveStepList.SUR05_BRAND)
          }
          break;
      }
    }
  }

  const backButtonClick = () => {
    if (showBottomDialog === "N" && isShow && bottomDialogOpened) {
      setOpenBottomDialog(true)
      setBottomDialogOpened(false)
    } else {
      navigation?.back();
    }
  }

  const handleClose = () => {
    resetLabRecommendList()
    resetLabIsItemSelected()
    resetScore()
    setOpenBottomDialog(false)
  }

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      // 다이얼로그
      if (showDialog) {
        setShowDialog(false)
      }
      // 설문 화면
      if (showSurvey) {
        handleBackButton()
      }
      // 댓글 수정 화면
      else if (showModify) {
        setShowModify(false)
      }
      // 기타
      else {
        if (openBottomDialog) {
          handleClose()
        } else {
          backButtonClick()
        }
      }
    }
  }, [handleBackKey])

  useEffect(() => {
    if (openBottomDialog) {
      setHackleTrack(HACKLE_TRACK.ETC_POPUP)
    }

    resetLabRecommendList()
    resetLabIsItemSelected()
    resetScore()
    setBottomDialogOpened(false)

    return () => {
      resetLabRecommendList()
      resetLabIsItemSelected()
      resetScore()
    }
  }, [openBottomDialog])

  useEffect(() => {
    setBottomDialogOpened(true)
    if (getLocalStorage('isLabBottomDialogNotOpenOneDay')) {
      setIsShow(false)
    } else {
      setIsShow(true)
    }

    return () => {
      setOpenBottomDialog(false)
      setBottomDialogOpened(true)
    }
  }, [])

  const {data: showRanking} = getRegistSurveyYn({survey_type: SURVEY_TYPE.SUR01.code})
  const {data: showBottomDialog} = getPollRegistYn({survey_type: SURVEY_TYPE.SUR01.code})

  const {mutate: mutatePostPollRegist, isLoading} = postPollRegistMutate()

  const handleButtonClick = () => {
    setHackleTrack(HACKLE_TRACK.ETC_BTN)
    mutatePostPollRegist({
      survey_type: SURVEY_TYPE.SUR01.code,
      score: score,
    }, {
      onSuccess: () => {
        queryClient?.invalidateQueries('getPollRegistYn')
        handleClose()
        navigation.back()
      }
    })
  }

  const showComment = () => {
    if (fetchData !== INIT && !showSurvey) return true
    return false
  }

  return (
    <MainLayout mainRef={mainRef}>
      <Container maxWidth={'xs'} disableGutters sx={{mb: '71px'}}>

        {/* 메인 섹션 */}
        <MainSection type={LAB_TYPE.NUTRITION} backButtonClick={backButtonClick}/>

        {/* 랭킹 섹션 */}
        <Box sx={{position: 'relative'}}>
          <NutritionWrapGroupButton/>
          {showRanking === 'Y' && <RankingSection type={LAB_TYPE.NUTRITION} surveyTypeParam={selectedSurveyType?.code}/>}
          {showRanking === 'N' && <RankingScreenSection type={LAB_TYPE.NUTRITION} onClick={handleShowRanking}/>}
          {!showRanking && <LabRankingCardSkeleton type={LAB_TYPE.NUTRITION}/>}
        </Box>

        {(!showRanking || showRanking === 'Y') && <Divider sx={{mt: '60px', mb: '50px', height: '1px', background: '#E6E6E6'}}/>}

        {/* 댓글 섹션 */}
        {showComment() && <CommentSection mainRef={mainRef} type={LAB_TYPE.NUTRITION}/>}

        {/* 설문 다이얼로그 섹션*/}
        <ServeySection open={showSurvey} onClose={() => setShowSurvey(false)} handleBackButton={handleBackButton}/>

      </Container>

      <BottomDialog open={openBottomDialog} onClose={handleClose}>
        <LabRecommendContents onClose={handleClose} handleButtonClick={handleButtonClick}/>
      </BottomDialog>

      {isLoading && <LoadingScreen/>}
    </MainLayout>
  )
}

export function NutritionWrapGroupButton() {
  const [selectedSurveyType, setSelectedSurveyType] = useRecoilState(labSelectedSurveyTypeSelector);

  const handleCategoryClick = (item) => {
    if (selectedSurveyType === item) return

    switch (item?.code) {
      case SURVEY_TYPE.SUR03.code:
        setHackleTrack(HACKLE_TRACK.NUTRIENTS_MAIN_FILTER_LOCTO)
        break;
      case SURVEY_TYPE.SUR04.code:
        setHackleTrack(HACKLE_TRACK.NUTRIENTS_MAIN_FILTER_VITAMIN)
        break;
      case SURVEY_TYPE.SUR05.code:
        setHackleTrack(HACKLE_TRACK.NUTRIENTS_MAIN_FILTER_IRON)
        break;
    }

    setSelectedSurveyType(item)
  }

  return (
    <Container disableGutters maxWidth={'xs'} sx={{mt: '30px', px: '20px'}}>
      <Grid container direction="row" spacing={1.5}>
        {LAB_NUTRITION_CATEGORY_ITEM?.map((item, index) => (
          <Grid item xs={4} justifyContent="flex-end" key={index}>
            <Button
              tabIndex={0}
              disableFocusRipple
              fullWidth
              variant={'contained'}
              value={item?.code}
              onKeyDown={(e) => handleEnterPress(e, () => handleCategoryClick(item))}
              onClick={() => handleCategoryClick(item)}
              sx={{
                justifyContent: 'center',
                p: '10px',
                height: '42px',
                lineHeight: '26px',
                color: item?.code === selectedSurveyType?.code ? '#FFFFFF' : '#666666',
                border: 1,
                borderColor: item?.code === selectedSurveyType?.code ? 'primary.main' : '#E6E6E6',
                borderRadius: '99px',
                backgroundColor: item?.code === selectedSurveyType?.code ? 'primary.main' : '#FFFFFF',
                '&:hover': {
                  boxShadow: 'none',
                  backgroundColor: item?.code === selectedSurveyType?.code ? 'primary.main' : '#FFFFFF',
                },
                ...clickBorderNone,
              }}
            >
              <Typography noWrap sx={{fontSize: '14px', fontWeight: item?.code === selectedSurveyType?.code ? 800 : 500}}>
                {item?.text}
              </Typography>
            </Button>
          </Grid>
        ))}
      </Grid>
    </Container>
  )
}