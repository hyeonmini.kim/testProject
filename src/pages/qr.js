import {isIos} from "../utils/envUtils";
import {PATH_DONOTS_STORE_URL} from "../routes/paths";
import {useRouter} from "next/router";
import {useEffect} from "react";

export default function QrCode() {
  const router = useRouter()

  useEffect(() => {
    if (isIos()) {
      router.push(PATH_DONOTS_STORE_URL.IOS)
    } else {
      router.push(PATH_DONOTS_STORE_URL.ANDROID)
    }
  }, [])
}