import '../fonts/index.css';
import 'simplebar/src/simplebar.css';
import './recipes/detail/styles.css'
import 'react-image-lightbox/style.css';
import 'react-quill/dist/quill.snow.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'react-lazy-load-image-component/src/effects/blur.css';
import * as React from "react";
import {useEffect, useState} from "react";
import {Hydrate, QueryClient, QueryClientProvider} from "react-query";
import {CacheProvider} from '@emotion/react';
import {RecoilRoot} from 'recoil';

import createEmotionCache from '../utils/createEmotionCache';
import Head from 'next/head';
import ThemeProvider from '../theme';
import ProgressBar from '../components/progress-bar';
import SnackbarProvider from '../components/snackbar';
import {MotionLazyContainer} from '../components/animate';
import {SettingsProvider, ThemeSettings} from '../components/settings';
import {AuthProvider} from "../auth/JwtContext";
import AxiosProvider from "../layouts/common/AxiosProvider";
import {ENV_TYPE} from "../constant/common/Env";
import {ENV} from "../config";
import NativeProvider from "../layouts/common/NativeProvider";
import DialogProvider from "../layouts/common/DialogProvider";
import ErrorProvider from "../layouts/common/ErrorProvider";
import ToastProvider from "../layouts/common/ToastProvider";
import {HackleProvider} from "@hackler/react-sdk";
import {getHackleClient} from "../utils/hackleUtils";

const clientSideEmotionCache = createEmotionCache();

export default function MyApp(props) {
  const {Component, pageProps, emotionCache = clientSideEmotionCache} = props;
  const getLayout = Component.getLayout ?? ((page) => page);
  const [queryClient] = useState(() => new QueryClient());

  useEffect(() => {
    if (ENV === ENV_TYPE.PRODUCT) {
      console.log = function no_console() {
      };
      console.info = function no_console() {
      };
      console.warn = function no_console() {
      };
      // console.debug = function no_console() {
      // };
    }
  }, [])


  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <link rel="icon" type="image/png" href="/favicon/donots.png"/>
      </Head>
      <script type="text/javascript" src="/static/action.js"></script>
      <RecoilRoot>
        <QueryClientProvider client={queryClient}>
          <NativeProvider>
            <AxiosProvider>
              <AuthProvider client={queryClient}>
                {/*<LocalizationProvider dateAdapter={AdapterDateFns}>*/}
                <SettingsProvider>
                  <MotionLazyContainer>
                    <ThemeProvider>
                      <ThemeSettings>
                        {/*<ThemeLocalization>*/}
                        <SnackbarProvider>
                          <ProgressBar/>
                          <Hydrate state={pageProps.dehydratedState}>
                            <DialogProvider/>
                            <ToastProvider/>
                            <ErrorProvider/>
                            <HackleProvider hackleClient={getHackleClient()} supportSSR>
                              {getLayout(<Component {...pageProps} />)}
                            </HackleProvider>
                          </Hydrate>
                        </SnackbarProvider>
                        {/*</ThemeLocalization>*/}
                      </ThemeSettings>
                    </ThemeProvider>
                  </MotionLazyContainer>
                </SettingsProvider>
                {/*</LocalizationProvider>*/}
              </AuthProvider>
            </AxiosProvider>
          </NativeProvider>
        </QueryClientProvider>
      </RecoilRoot>
    </CacheProvider>
  );
}
