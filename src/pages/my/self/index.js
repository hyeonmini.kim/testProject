import {useEffect, useRef, useState} from 'react';
import {Container,} from '@mui/material';
import MyLayout from "../../../layouts/my/MyLayout";
import MyHeaderTitleLayout from "../../../layouts/my/MyHeaderTitleLayout";
import {MY_CALL, MY_COMMON} from "../../../constant/my/MyConstants";
import MyMembershipInfoSectionAndTabSection from "../../../sections/my/self/MyMembershipInfoSectionAndTabSection";
import MyProfileSection from "../../../sections/my/self/MyProfileSection";
import useNativeBackKey from "../../../hooks/useNativeBackKey";
import {useRecoilState, useResetRecoilState} from "recoil";
import {
  myCallbackAddSnsSelector,
  myOpenAddChildrenBtnMarginSelector,
  myOpenAddChildrenBtnPositionSelector,
  myOpenAddChildrenBtnTextSelector,
  myOpenAddChildrenPageSelector,
  myOpenAddChildrenSelector,
  myOpenChangePhoneSelector,
  myOpenChangePwSelector,
  myOpenChildrenSelector,
  myOpenMomStarSelector,
  myOpenMyInformationPopupSelector,
  myOpenSnsDetailSelector,
  myOpenSnsSelector,
  myOpenWithdrawMembershipSelector
} from "../../../recoil/selector/my/popup/myPopupSelector";
import {
  myChildrenBottomDialogState,
  myFilterBottomDialogState,
  myInformationBottomDialogState
} from "../../../recoil/atom/my/information/myBottomDialog";
import {dialogShowDialogSelector, showFullScreenMessageDialogSelector} from "../../../recoil/selector/common/dialogSelector";
import {commonBottomDialogState} from "../../../recoil/atom/common/bottomDialog";
import useStackNavigation from "../../../hooks/useStackNavigation";
import {STACK} from "../../../constant/common/StackNavigation";
import {
  myActiveTabSelector,
  myIsExpandedAccordionSelector,
  myIsSelectedFilterSelector,
  myScrapListSelector,
  mySelectedFilterSelector,
  myWriteListSelector
} from "../../../recoil/selector/my/information/myStateSelector";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {
  myBabyIsProfilePictureUrlDeletedSelector,
  myCallSelector,
  myDataSelector,
  myIsProfilePictureUrlDeletedSelector,
  tempMyDataSelector
} from "../../../recoil/selector/my/myDataSelector";
import {
  childrenAllergyIngredientsListSelector,
  childrenAllergyIngredientsSelector,
  childrenConcernsListSelector,
  childrenConcernsSelector
} from "../../../recoil/selector/my/childrenDataSelector";
import MainLayout from "../../../layouts/main/MainLayout";
import {setHackleTrack} from "../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../constant/common/Hackle";
import {getMember, getMemberMutate} from "../../../api/authApi";
import CompleteDialog from "../../../sections/auth/complete/CompleteDialog";
import {LEVEL_UP_CHOCO} from "../../../constant/dialog/Complete";
import CompleteLevelUpChocoCard from "../../../sections/auth/complete/CompleteLevelUpChocoCard";
import {USER_GRADE} from "../../../constant/common/User";
import {useRouter} from "next/router";
import {useBefuAuthContext} from "../../../auth/useAuthContext";

MyPage.getLayout = (page) => <MyLayout>{page}</MyLayout>;

export default function MyPage() {
  const [activeTab, setActiveTab] = useRecoilState(myActiveTabSelector);
  const [myWriteList, setMyWriteList] = useRecoilState(myWriteListSelector);
  const [myScrapList, setMyScrapList] = useRecoilState(myScrapListSelector);
  const [isExpanded, setIsExpanded] = useRecoilState(myIsExpandedAccordionSelector);

  const [myData, setMyData] = useRecoilState(myDataSelector);
  const [open, setOpen] = useRecoilState(myOpenMyInformationPopupSelector);
  const [tempMydata, setTempMydata] = useRecoilState(tempMyDataSelector);
  const [myIsProfilePictureUrlDeleted, setMyIsProfilePictureUrlDeleted] = useRecoilState(myIsProfilePictureUrlDeletedSelector);
  const [myBabyIsProfilePictureUrlDeleted, setMyBabyIsProfilePictureUrlDeleted] = useRecoilState(myBabyIsProfilePictureUrlDeletedSelector);
  const [openMomStar, setOpenMomStar] = useRecoilState(myOpenMomStarSelector);
  const [openChildren, setOpenChildren] = useRecoilState(myOpenChildrenSelector)
  const [openAddChildren, setOpenAddChildren] = useRecoilState(myOpenAddChildrenSelector)
  const [page, setPage] = useRecoilState(myOpenAddChildrenPageSelector)
  const [btnPosition, setBtnPosition] = useRecoilState(myOpenAddChildrenBtnPositionSelector)
  const [btnMargin, setBtnMargin] = useRecoilState(myOpenAddChildrenBtnMarginSelector)
  const [btnText, setBtnText] = useRecoilState(myOpenAddChildrenBtnTextSelector)
  const resetChildrenAllergyIngredients = useResetRecoilState(childrenAllergyIngredientsSelector)
  const resetChildrenAllergyIngredientsList = useResetRecoilState(childrenAllergyIngredientsListSelector)
  const resetChildrenConcerns = useResetRecoilState(childrenConcernsSelector)
  const resetChildrenConcernsList = useResetRecoilState(childrenConcernsListSelector)
  const [openFullScreenMessageDialog, setOpenFullScreenMessageDialog] = useRecoilState(showFullScreenMessageDialogSelector);

  const [isOpen, setIsOpen] = useRecoilState(myInformationBottomDialogState);
  const [openSns, setOpenSns] = useRecoilState(myOpenSnsSelector);
  const [openChangePw, setOpenChangePw] = useRecoilState(myOpenChangePwSelector)
  const [openChangePhone, setOpenChangePhone] = useRecoilState(myOpenChangePhoneSelector)
  const [openWithdrawMembership, setOpenWithdrawMembership] = useRecoilState(myOpenWithdrawMembershipSelector)

  const [openSnsDetail, setOpenSnsDetail] = useRecoilState(myOpenSnsDetailSelector);
  const [isOpenCommonBottomDialogState, setIsOpenCommonBottomDialogState] = useRecoilState(commonBottomDialogState);

  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)

  const [selectedFilter, setSelectedFilter] = useRecoilState(mySelectedFilterSelector);
  const [filterOpen, setFilterOpen] = useRecoilState(myFilterBottomDialogState);

  const [isChildrenBottomOpen, setIsChildrenBottomOpen] = useRecoilState(myChildrenBottomDialogState);
  const [isSelectedFilter, setIsSelectedFilter] = useRecoilState(myIsSelectedFilterSelector);

  const [myCallbackAddSns, setMyCallbackAddSns] = useRecoilState(myCallbackAddSnsSelector)
  const [myCall, setMyCall] = useRecoilState(myCallSelector)

  const [showLevelUpChoco, setShowLevelUpChoco] = useState(false)

  const {mutate: mutateGetMember} = getMemberMutate()
  const {data: memberInfo} = getMember({token: ''})

  const mainRef = useRef(null)
  const {navigation, preFetch} = useStackNavigation()
  const {asPath, push} = useRouter();
  const {isAuthenticated} = useBefuAuthContext();

  useEffect(() => {
    const fetch = preFetch(STACK.MY_SELF.TYPE)
    if (fetch) {
      setActiveTab(fetch?.activeTab)
      setIsExpanded(fetch?.isExpanded)
      setMyWriteList(fetch?.myWriteList)
      setSelectedFilter(fetch?.selectedFilter)
      setIsSelectedFilter(fetch?.isSelectedFilter)
      setMyScrapList(fetch?.myScrapList)
      setTimeout(() => {
        mainRef?.current?.scrollTo(0, fetch?.scrollY)
      }, 100)
    } else if (myCallbackAddSns) {
      setOpen(true)
      setOpenSns(true)
      setMyCallbackAddSns(false)
    } else if (myCall?.type) {
      switch (myCall?.type) {
        case MY_CALL.CHILDREN_INFORM:
          setOpenChildren(true)
          break
      }
    }

    return () => {
      setIsOpen(false)
      setShowDialog(false)
      setOpenSnsDetail(false)
      setOpenSns(false)
      setOpenChangePw(false)
      setIsOpenCommonBottomDialogState(false)
      setOpenChangePhone(false)
      setOpenWithdrawMembership(false)
      setOpen(false)
      setOpenMomStar(false)
      setOpenAddChildren(false)
      setOpenChildren(false)

      setActiveTab('write')
      setIsExpanded(false)
      setMyWriteList([])
      setMyScrapList([])
      setSelectedFilter('전체')
      setIsSelectedFilter(false)
      setMyCall(null)
    }
  }, [])

  // 안드로이드 백버튼 처리
  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    // 백버튼 이벤트 받았을 때
    setTimeout(() => {
      handleNativeBackKey()
    }, 1)
  }, [handleBackKey]);

  const handleNativeBackKey = () => {
    if (handleBackKey) {
      if (open) { // 나의 정보 화면 오픈인 상태
        // 아바타 선택하여 프로필 사진 선택하기 BottomDialog 오픈인 상태
        if (isOpen) {
          setIsOpen(false);
        } else if (openSns) {  // SNS 연동계정관리 화면 오픈인 상태
          if (openSnsDetail) { // 특정 SNS 연동 정보 화면 오픈인 상태
            if (showDialog) {  // 연동 계정 한 개일 때, 삭제 선택하여 안내 팝업 오픈인 상태
              setShowDialog(false);
            } else {
              setOpenSnsDetail(false);
            }
          } else {
            setOpenSns(false);
          }
        } else if (openChangePw) { // 비밀번호 변경 화면 오픈인 경우
          setOpenChangePw(false);
        } else if (openChangePhone) {  // 휴대전화 본인인증 화면 오픈인 경우
          if (isOpenCommonBottomDialogState) { // 통신사 선택 Bottom Dialog 오픈인 경우
            setIsOpenCommonBottomDialogState(false)
          } else {
            setOpenChangePhone(false);
          }
        } else if (openWithdrawMembership) { // 회원탈퇴 화면 오픈인 경우
          if (showDialog) {  // 회원 탈퇴 확인 팝업 오픈인 경우
            setShowDialog(false)
          } else {
            setOpenWithdrawMembership(false);
          }
        } else {
          setOpen(false);
          setMyData(tempMydata);
          setMyIsProfilePictureUrlDeleted(false);
        }
      } else if (openMomStar) {  // 맘스타정책팝업 오픈인 경우
        setOpenMomStar(false);
      } else if (openChildren) { // 아이 정보 화면 오픈인 경우
        // 아이 아바타 선택하여 프로필 사진 선택하기 BottomDialog 오픈인 상태
        if (myCall?.type) {
          navigation.back()
        } else if (isChildrenBottomOpen) {
          setIsChildrenBottomOpen(false);
        } else if (openAddChildren) {  // 아이 추가 화면 오픈인 경우
          if (page === 0) {
            setOpenAddChildren(false)

            setTimeout(() => {
              resetChildrenAllergyIngredientsList()
              resetChildrenAllergyIngredients()
              resetChildrenConcernsList()
              resetChildrenConcerns()
            }, 300)
          } else {
            setPage(0)

            setBtnText('다음')
            setBtnPosition('initial')
            setBtnMargin('')

            setTimeout(() => {
              resetChildrenConcernsList()
              resetChildrenConcerns()
            }, 300)
          }
        } else {
          setOpenChildren(false);
          setMyBabyIsProfilePictureUrlDeleted(false);
        }
      } else if (showDialog) { // 아이 삭제 선택 팝업 오픈인 경우
        setShowDialog(false);
      } else if (filterOpen) { // 필터 Bottom Dialog 오픈인 경우
        setFilterOpen(false);
      } else if (openFullScreenMessageDialog) { // SNS 연동 링크 없음 팝업 Dialog 오픈인 경우
        setOpenFullScreenMessageDialog(false);
      } else if (showLevelUpChoco) { // 초코 레렙업 다이얼로그 오픈인 경우
        setShowLevelUpChoco(false)
      } else {
        handleBackButton()
      }
    }
  }

  const handleBackButton = () => {
    if (!isAuthenticated && asPath?.includes('d_inflow')) {
      push(PATH_DONOTS_PAGE?.GUEST);
    } else if (isAuthenticated && asPath?.includes('d_inflow')) {
      push(PATH_DONOTS_PAGE?.HOME);
    } else {
      navigation?.back();
    }
  }

  const handleShowSetting = () => {
    navigation.push(STACK.MY_SELF.TYPE, PATH_DONOTS_PAGE.MY.SETTING, {
      ...STACK.MY_SELF.DATA,
      activeTab: activeTab,
      isExpanded: isExpanded,
      myWriteList: myWriteList,
      myScrapList: myScrapList,
      selectedFilter: selectedFilter,
      isSelectedFilter: isSelectedFilter,
      scrollY: mainRef?.current?.scrollTop
    })
  }

  useEffect(() => {
    setHackleTrack(HACKLE_TRACK.MYPAGE_MY_RECIPE)
  }, [])

  const levelUpChocoContents = {
    grade: USER_GRADE.LV2.GRADE,
    oneButton: {
      text: LEVEL_UP_CHOCO.BUTTON_TEXT,
      onClick: () => {
        setShowLevelUpChoco(false)
      }
    }
  }

  return (
    <MainLayout mainRef={mainRef}>
      <MyHeaderTitleLayout title={MY_COMMON.TEXT.TITLE(myCall?.type)} showSetting onSetting={handleShowSetting} isHeadTitle={true}>
        <Container maxWidth={'xs'} disableGutters>
          <MyProfileSection mainRef={mainRef} memberInfo={memberInfo} setShowChoco={setShowLevelUpChoco}/>
          <MyMembershipInfoSectionAndTabSection mainRef={mainRef}/>
        </Container>
      </MyHeaderTitleLayout>

      {/* 등급업 다이얼로그 - 초코 */}
      {showLevelUpChoco &&
        <CompleteDialog isOpen={showLevelUpChoco} title={LEVEL_UP_CHOCO.TITLE} {...levelUpChocoContents}>
          <CompleteLevelUpChocoCard/>
        </CompleteDialog>}
    </MainLayout>
  );
}