import * as React from 'react';
import {useEffect, useRef, useState} from 'react';
import {Box, Container, Divider, Skeleton, Stack, Tab, Tabs, Typography,} from '@mui/material';
import MyHeaderTitleLayout from "../../../layouts/my/MyHeaderTitleLayout";
import {MY_COMMON, MY_TABS} from "../../../constant/my/MyConstants";
import useNativeBackKey from "../../../hooks/useNativeBackKey";
import useStackNavigation from "../../../hooks/useStackNavigation";
import {STACK} from "../../../constant/common/StackNavigation";
import {useRecoilState} from "recoil";
import MyExpertsProfileSection from "../../../sections/my/experts/MyExpertsProfileSection";
import MyExpertsMembershipInfoSectionAndTabSection from "../../../sections/my/experts/MyExpertsMembershipInfoSectionAndTabSection";
import MyLayoutExperts from "../../../layouts/my/MyLayoutExperts";
import {showFullScreenMessageDialogSelector} from "../../../recoil/selector/common/dialogSelector";
import {
  othersActiveTabSelector,
  othersScrapListSelector,
  othersWriteListSelector
} from "../../../recoil/selector/my/information/othersStateSelector";
import {useRouter} from "next/router";
import MainLayout from "../../../layouts/main/MainLayout";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {useBefuAuthContext} from "../../../auth/useAuthContext";

MyPageExperts.getLayout = (page) => <MyLayoutExperts>{page}</MyLayoutExperts>;

export default function MyPageExperts() {
  const [activeTab, setActiveTab] = useRecoilState(othersActiveTabSelector);
  const [othersWriteList, setOthersWriteList] = useRecoilState(othersWriteListSelector);
  const [othersScrapList, setOthersScrapList] = useRecoilState(othersScrapListSelector);
  const [openFullScreenMessageDialog, setOpenFullScreenMessageDialog] = useRecoilState(showFullScreenMessageDialogSelector);
  const [hackleProperty, setHackleProperty] = useState('')

  const {query, asPath, push} = useRouter();
  const {isAuthenticated, isInitialized} = useBefuAuthContext();

  const mainRef = useRef(null)
  const {navigation, preFetch} = useStackNavigation()

  useEffect(() => {
    const fetch = preFetch(STACK.MY_EXPERT.TYPE)
    if (fetch) {
      setActiveTab(fetch?.activeTab)
      setOthersWriteList(fetch?.othersWriteList)
      setOthersScrapList(fetch?.othersScrapList)
      setTimeout(() => {
        mainRef?.current?.scrollTo(0, fetch?.scrollY)
      }, 100)
    }

    return () => {
      setActiveTab('write')
      setOthersWriteList([])
      setOthersScrapList([])
      setOpenFullScreenMessageDialog(false)
    }
  }, [])

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      if (openFullScreenMessageDialog) {
        setOpenFullScreenMessageDialog(false)
      } else {
        handleBackButton()
      }
    }
  }, [handleBackKey]);

  const handleBackButton = () => {
    if (!isAuthenticated && asPath?.includes('d_inflow')) {
      push(PATH_DONOTS_PAGE?.GUEST);
    } else if (isAuthenticated && asPath?.includes('d_inflow')) {
      push(PATH_DONOTS_PAGE?.HOME);
    } else {
      navigation?.back();
    }
  }

  return (
    <MainLayout mainRef={mainRef}>
      <MyHeaderTitleLayout
        title={MY_COMMON.TEXT.TITLE_OTHER}
        onBack={handleBackButton}
      >
        <Container maxWidth={'xs'} disableGutters>
          {
            !query?.user_key
              ? <MyExpertsSkeleton/>
              : <MyExpertsDataSection user_key={query?.user_key} mainRef={mainRef} setProperty={setHackleProperty}
                                      hackleProperty={hackleProperty}/>
          }
        </Container>
      </MyHeaderTitleLayout>
    </MainLayout>
  );
}

function MyExpertsDataSection({user_key, mainRef, setProperty, hackleProperty}) {
  return (
    <>
      <MyExpertsProfileSection user_key={user_key} setProperty={setProperty}/>
      <MyExpertsMembershipInfoSectionAndTabSection user_key={user_key} mainRef={mainRef} hackleProperty={hackleProperty}/>
    </>
  )
}

function MyExpertsSkeleton() {
  const [activeTab, setActiveTab] = useRecoilState(othersActiveTabSelector);

  function getCount(value) {
    if (value === "write") {
      return 0
    } else {
      return 0
    }
  }

  return (
    <>
      <Box sx={{mt: 3, alignItems: 'center', mx: '20px'}}>
        <Stack direction="row" sx={{ml: '4px', my: '20px', alignItems: 'center', verticalAlign: 'center'}}>
          <Skeleton variant="circular" width="76px" height="76px"/>
          <Stack>
            <Stack direction="row" sx={{ml: '16px', alignItems: 'center', verticalAlign: 'center', mb: '4px'}}>
              <Skeleton variant="rounded" height="24px" width="82px" sx={{mr: '6px'}}/>
              <Divider sx={{height: '14px', ml: '4px', mr: '10px', background: '#CCCCCC', width: '1px', border: 0}}/>
              <Skeleton variant="rounded" height="14px" width="52px"/>
            </Stack>
            <Skeleton variant="rounded" height="48px" width="225px" sx={{ml: '16px'}}/>
          </Stack>
        </Stack>
      </Box>

      <Box sx={{mt: 3, alignItems: 'center', mx: '20px'}}>
        <Skeleton variant="rounded" height="90px" width="100%" sx={{mb: '20px', borderRadius: '8px'}}/>
      </Box>

      <Box sx={{height: '48px', borderBottom: '2px solid #F5F5F5'}}>
        <Tabs
          sx={{'& .MuiTabs-indicator': {backgroundColor: 'black'}, '& .Mui-selected': {color: 'black'}}}
          variant="fullWidth"
          value={activeTab}
          onChange={(event, newValue) => handleTabClick(newValue)}
        >
          {MY_TABS.slice(0, 2).map((tab, index) => (
            <Tab
              key={tab.value}
              value={tab.value}
              label={
                <Stack direction="row" spacing="8px">
                  <Typography fontWeight="400" fontSize="16px" sx={{lineHeight: '24px', color: '#000000'}}>
                    {tab.label}
                  </Typography>
                  <Typography fontWeight="400" fontSize="16px" sx={{lineHeight: '24px', color: '#888888'}}>
                    {getCount(tab.value)}
                  </Typography>
                </Stack>
              }
              sx={{'&:not(:last-of-type)': {marginRight: '0px',}}}
            />
          ))}
        </Tabs>
      </Box>

      <Box sx={{mt: 3, alignItems: 'center', mx: '20px'}}>
        {[...Array(10)].map((item, index) =>
          <SkeletonItem key={index}/>
        )}
      </Box>
    </>
  )
}

function SkeletonItem() {
  return (
    <Stack direction="row" spacing="8px" sx={{my: '12px'}}>
      <Box sx={{position: 'relative'}}>
        <Skeleton variant="rounded" sx={{borderRadius: '8px', width: '100px', height: '100px'}}/>
      </Box>
      <Box sx={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between', flexGrow: 1}}>
        <Skeleton sx={{height: '24px'}}>
        </Skeleton>
        <Skeleton sx={{height: '20px'}}>
        </Skeleton>
        <Box sx={{flexGrow: 1}}/>
        <Stack direction="row" alignItems={'center'}>
          <Skeleton variant="rectangular" sx={{width: '50px', height: '16px', mr: '15px', borderRadius: 1}}/>
          <Skeleton variant="rectangular" sx={{width: '50px', height: '16px', borderRadius: 1}}/>
        </Stack>
      </Box>
    </Stack>
  );
}