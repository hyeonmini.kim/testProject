import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {Container, Divider} from "@mui/material";
import {useRecoilState, useSetRecoilState} from "recoil";
import {
  licenseUrlSelector,
  settingShowAgreeTermsSelector,
  settingShowFAQDetailSelector,
  settingShowFAQSelector,
  settingShowLicenseSelector,
  settingShowNoticeDetailSelector,
  settingShowNoticeSelector,
  settingShowOneOnOneInquiriesDetailSelector,
  settingShowOneOnOneInquiriesPopupSelector,
  settingShowOneOnOneInquiriesSelector,
  settingShowPersonalTermsSelector,
  settingShowPushSelector,
  settingShowRegistOneOnOneInquirySelector,
  settingShowServiceTermsSelector,
  settingShowTermsSelector,
  settingShowVersionSelector
} from "../../../recoil/selector/my/settingSelector";
import MyHeaderTitleLayout from "../../../layouts/my/MyHeaderTitleLayout";
import PushSection from "../../../sections/my/setting/app/PushSection";
import NoticeSection from "../../../sections/my/setting/service/NoticeSection";
import TermsSection from "../../../sections/my/setting/service/TermsSection";
import FAQSection from "../../../sections/my/setting/service/FAQSection";
import VersionSection from "../../../sections/my/setting/service/VersionSection";
import useStackNavigation from "../../../hooks/useStackNavigation";
import useNativeBackKey from "../../../hooks/useNativeBackKey";
import {useRouter} from "next/router";
import NoticeDetail from "../../../sections/my/setting/service/notice/NoticeDetail";
import {MY_SETTING} from "../../../constant/my/Setting";
import SettingAppSection from "../../../sections/my/setting/SettingAppSection";
import SettingInfoSection from "../../../sections/my/setting/SettingInfoSection";
import FAQDetail from "../../../sections/my/setting/service/faq/FAQDetail";
import OneOnOneInquiriesSection from "../../../sections/my/setting/service/OneOnOneInquiriesSection";
import RegistOneOnOneInquirySection from "../../../sections/my/setting/service/one-on-one-inquiries/RegistOneOnOneInquirySection";
import OneOnOneInquiriesDetail from "../../../sections/my/setting/service/one-on-one-inquiries/OneOnOneInquiriesDetail";
import {myInformationBottomDialogState} from "../../../recoil/atom/my/information/myBottomDialog";
import {
  settingOneOnOneInquiryAttachmentFileSelector,
  settingOneOnOneInquiryBodySelector,
  settingOneOnOneInquiryCategorySelector,
  settingOneOnOneInquiryTitleSelector
} from "../../../recoil/selector/my/setting/one-on-one-inquiry/oneOnOneInquirySelector";
import {dialogSelector, dialogShowDialogSelector} from "../../../recoil/selector/common/dialogSelector";
import {COMMON_DIALOG_TYPE} from "../../../constant/common/Common";
import {MY_BTN_TEXT} from "../../../constant/my/MyConstants";
import {isNative} from "../../../utils/envUtils";
import MySettingLayout from "../../../layouts/my/MySettingLayout";
import MainLayout from "../../../layouts/main/MainLayout";
import IFrameDialog, {TRANSITION_TYPE} from "../../../components/common/IFrameDialog";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {useBefuAuthContext} from "../../../auth/useAuthContext";
import {ALT_STRING} from "../../../constant/common/AltString";

MySetting.getLayout = (page) => <MySettingLayout>{page}</MySettingLayout>;

export default function MySetting() {
  const [isCall, setIsCall] = useState(undefined)
  const [showPush, setShowPush] = useRecoilState(settingShowPushSelector);
  const [showNotice, setShowNotice] = useRecoilState(settingShowNoticeSelector);
  const [showTerms, setShowTerms] = useRecoilState(settingShowTermsSelector);
  const [showLicense, setLicense] = useRecoilState(settingShowLicenseSelector);
  const [showFAQ, setShowFAQ] = useRecoilState(settingShowFAQSelector);
  const [showFAQDetail, setShowFAQDetail] = useRecoilState(settingShowFAQDetailSelector)
  const [showVersion, setShowVersion] = useRecoilState(settingShowVersionSelector);
  const [showNoticeDetail, setShowNoticeDetail] = useRecoilState(settingShowNoticeDetailSelector)
  const [showServiceTerms, setShowServiceTerms] = useRecoilState(settingShowServiceTermsSelector)
  const [showPersonalTerms, setShowPersonalTerms] = useRecoilState(settingShowPersonalTermsSelector)
  const [showAgreeTerms, setShowAgreeTerms] = useRecoilState(settingShowAgreeTermsSelector)
  const [showOneOnOneInquiries, setShowOneOnOneInquiries] = useRecoilState(settingShowOneOnOneInquiriesSelector);
  const [showRegistOneOnOneInquiry, setShowRegistOneOnOneInquiry] = useRecoilState(settingShowRegistOneOnOneInquirySelector)
  const [showOneOnOneInquiriesDetail, setShowOneOnOneInquiriesDetail] = useRecoilState(settingShowOneOnOneInquiriesDetailSelector)
  const [showOneOnOneInquiriesPopup, setShowOneOnOneInquiriesPopup] = useRecoilState(settingShowOneOnOneInquiriesPopupSelector)
  const [isOpenBottomDialog, setIsOpenBottomDialog] = useRecoilState(myInformationBottomDialogState);
  const setDialogMessage = useSetRecoilState(dialogSelector)
  const [dialogShowDialog, setDialogShowDialog] = useRecoilState(dialogShowDialogSelector)
  const [category, setCategory] = useRecoilState(settingOneOnOneInquiryCategorySelector)
  const [title, setTitle] = useRecoilState(settingOneOnOneInquiryTitleSelector)
  const [body, setBody] = useRecoilState(settingOneOnOneInquiryBodySelector)
  const [attachmentFile, setAttachmentFile] = useRecoilState(settingOneOnOneInquiryAttachmentFileSelector)
  const [licenseUrl, setLicenseUrl] = useRecoilState(licenseUrlSelector)

  const mainRef = useRef(null)
  const {query, isReady, asPath, push} = useRouter();
  const {navigation, preFetch} = useStackNavigation()
  const {isAuthenticated, isInitialized} = useBefuAuthContext();

  useEffect(() => {
    if (isReady) {
      const {d_notice, d_faq} = query
      if (d_notice) {
        setIsCall(true)
        setShowNoticeDetail(d_notice)
      } else if (d_faq) {
        setIsCall(true)
        setShowFAQDetail(d_faq)
      } else {
        setIsCall(false)
      }
    }
  }, [isReady]);

  useEffect(() => {
    return () => {
      setShowPush(false)
      setShowNotice(false)
      setShowTerms(false)
      setLicense(false)
      setShowFAQ(false)
      setShowVersion(false)
      setShowNoticeDetail(false)
      setShowFAQDetail(false)
      setShowOneOnOneInquiries(false)
      setShowRegistOneOnOneInquiry(false)
      setShowOneOnOneInquiriesDetail(false)
      setShowOneOnOneInquiriesPopup(false)
    }
  }, [])

  // 안드로이드 백버튼 처리
  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      if (isCall) {
        isCallBackButton()
      } else if (showPush) {
        setShowPush(false)
      } else if (showNotice) {
        if (showNoticeDetail) {
          setShowNoticeDetail(false)
        } else {
          setShowNotice(false)
        }
      } else if (showTerms) {
        if (showServiceTerms) {
          setShowServiceTerms(false)
        } else if (showPersonalTerms) {
          setShowPersonalTerms(false)
        } else if (showAgreeTerms) {
          setShowAgreeTerms(false)
        } else {
          setShowTerms(false)
        }
      } else if (showLicense) {
        setLicense(false)
      } else if (showFAQ) {
        if (showFAQDetail) {
          setShowFAQDetail(false)
        } else {
          setShowFAQ(false)
        }
      } else if (showVersion) {
        setShowVersion(false)
      } else if (showOneOnOneInquiries) {
        if (showRegistOneOnOneInquiry) {
          if (isOpenBottomDialog) {
            setIsOpenBottomDialog(false)
          } else if (showOneOnOneInquiriesPopup) {
            setShowOneOnOneInquiriesPopup(false)
          } else if (dialogShowDialog) {
            setDialogShowDialog(false)
          } else {
            onBackHandleRegistOneOnOneInquiry()
          }
        } else if (showOneOnOneInquiriesDetail) {
          if (showOneOnOneInquiriesPopup) {
            setShowOneOnOneInquiriesPopup(false)
          } else if (dialogShowDialog) {
            setDialogShowDialog(false)
          } else {
            setShowOneOnOneInquiriesDetail(false)
          }
        } else {
          setShowOneOnOneInquiries(false)
        }
      } else {
        navigation.back()
      }
    }
  }, [handleBackKey]);

  const isCallBackButton = () => {
    if (!isAuthenticated && asPath?.includes('d_inflow')) {
      push(PATH_DONOTS_PAGE?.GUEST);
    } else if (isAuthenticated && asPath?.includes('d_inflow')) {
      push(PATH_DONOTS_PAGE?.HOME);
    } else {
      navigation?.back();
    }
  }

  const onBackHandleRegistOneOnOneInquiry = () => {
    if (category !== '' || title !== '' || body !== '' || attachmentFile !== null) {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE.TWO_BUTTON,
        message: MY_SETTING.POPUP_COMMENT.BACK_REGIST_INQUIRY,
        leftButtonProps: {text: MY_BTN_TEXT.CANCEL},
        rightButtonProps: {text: MY_BTN_TEXT.CONFIRM, onClick: () => setShowRegistOneOnOneInquiry(false)},
      })
    } else {
      setShowRegistOneOnOneInquiry(false)
    }
  }

  if (isCall) {
    return (
      <MainLayout>
        {showNoticeDetail && <NoticeDetail open={showNoticeDetail} onBack={isCallBackButton}/>}
        {showFAQDetail && <FAQDetail open={showFAQDetail} onBack={isCallBackButton}/>}
      </MainLayout>
    )
  }
  if (isCall === false) {
    return (
      <MainLayout mainRef={mainRef}>
        <MyHeaderTitleLayout
          title={MY_SETTING.TITLE}
          onBack={() => navigation.back()}
          isDialog={true}
        >
          <Container disableGutters maxWidth={'xs'} sx={{position: 'relative', px: '20px'}}>
            {isNative() && <SettingAppSection/>}
            {isNative() &&
              <Divider sx={{mx: '-20px', height: '6px', background: '#F5F5F5', borderBottomWidth: 'inherit'}}/>}
            <SettingInfoSection/>
          </Container>
        </MyHeaderTitleLayout>
        {showPush && <PushSection open={showPush} onBack={() => setShowPush(false)}/>}
        {showNotice && <NoticeSection open={showNotice} onBack={() => setShowNotice(false)}/>}
        {showNoticeDetail && <NoticeDetail open={showNoticeDetail} onBack={() => setShowNoticeDetail(false)}/>}
        {showTerms && <TermsSection open={showTerms} onBack={() => setShowTerms(false)}/>}
        {showLicense &&
          <IFrameDialog title={ALT_STRING.SETTING.LICENSE} open={showLicense} transition={TRANSITION_TYPE.FADE} url={licenseUrl}
                        onBack={() => setLicense(false)}/>}
        {showFAQ && <FAQSection open={showFAQ} onBack={() => setShowFAQ(false)}/>}
        {showFAQDetail && <FAQDetail open={showFAQDetail} onBack={() => setShowFAQDetail(false)}/>}
        {showVersion && <VersionSection open={showVersion} onBack={() => setShowVersion(false)}/>}
        {showOneOnOneInquiries &&
          <OneOnOneInquiriesSection open={showOneOnOneInquiries} onBack={() => setShowOneOnOneInquiries(false)}/>}
        {showRegistOneOnOneInquiry && <RegistOneOnOneInquirySection open={showRegistOneOnOneInquiry}
                                                                    onBack={() => onBackHandleRegistOneOnOneInquiry(false)}/>}
        {showOneOnOneInquiriesDetail && <OneOnOneInquiriesDetail open={showOneOnOneInquiriesDetail}
                                                                 onBack={() => setShowOneOnOneInquiriesDetail(false)}/>}
      </MainLayout>
    )
  }
  return
}