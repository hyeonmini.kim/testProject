import AddChildrenLayout from "../../../layouts/my/add-children/AddChildrenLayout";
import MainLayout from "../../../layouts/main/MainLayout";
import AddChildrenSection from "../../../sections/auth/AddChildrenSection";

AddChildrenPage.getLayout = (page) => <AddChildrenLayout>{page}</AddChildrenLayout>

export default function AddChildrenPage() {

  return (
    <MainLayout>
      <AddChildrenSection/>
    </MainLayout>
  )
}
