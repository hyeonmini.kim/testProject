import {useEffect, useRef} from "react";
import RecipeSearchLayout from "../../../layouts/recipe/search/RecipeSearchLayout";
import HeaderSearchLayout from "../../../layouts/recipe/search/HeaderSearchLayout";
import HomeSection from "../../../sections/recipe/search/HomeSection";
import SearchResultSection from "../../../sections/recipe/search/SearchResultSection";
import SearchPopularSection from "../../../sections/recipe/search/SearchPopularSection";
import {useRecoilState, useResetRecoilState} from "recoil";
import {
  searchActiveTabSelector,
  searchCallSelector,
  searchCategorySelector,
  searchIsCallSelector,
  searchIsSearchSelector,
  searchIsTitleSelector,
  searchRecipeResultSelector,
  searchRecipeSearchSelector,
  searchRecipeSelector,
  searchShowFilterSelector
} from "../../../recoil/selector/recipe/searchSelector";
import useStackNavigation from "../../../hooks/useStackNavigation";
import {STACK} from "../../../constant/common/StackNavigation";
import useNativeBackKey from "../../../hooks/useNativeBackKey";
import GlobalNavigation from "../../../layouts/common/GlobalNavigation";
import {
  RECIPE_RESULT_STATUS,
  RECIPE_SEARCH_COMMON,
  RECIPE_SEARCH_HOME_CATEGORY,
  RECIPE_SEARCH_HOME_HEALTH,
  RECIPE_SEARCH_HOME_MATERIAL,
  RECIPE_SEARCH_TYPE
} from "../../../constant/recipe/Search";
import MainLayout from "../../../layouts/main/MainLayout";
import {useBefuAuthContext} from "../../../auth/useAuthContext";
import {dialogShowDialogSelector} from "../../../recoil/selector/common/dialogSelector";
import {PATH_DONOTS_PAGE} from "../../../routes/paths";
import {useRouter} from "next/router";

RecipeSearchPage.getLayout = (page) => <RecipeSearchLayout>{page}</RecipeSearchLayout>;

export default function RecipeSearchPage() {
  const {navigation, preFetch} = useStackNavigation()
  const [searchCall, setSearchCall] = useRecoilState(searchCallSelector);
  const resetSearchCall = useResetRecoilState(searchCallSelector);
  const [searchRecipe, setSearchRecipe] = useRecoilState(searchRecipeSelector);
  const resetSearchRecipe = useResetRecoilState(searchRecipeSelector);

  const [isCall, setIsCall] = useRecoilState(searchIsCallSelector)
  const [isTitle, setIsTitle] = useRecoilState(searchIsTitleSelector);
  const [isSearch, setIsSearch] = useRecoilState(searchIsSearchSelector);
  const resetIsSearch = useResetRecoilState(searchIsSearchSelector);
  const [category, setCategory] = useRecoilState(searchCategorySelector);

  const [result, setResult] = useRecoilState(searchRecipeResultSelector);
  const [search, setSearch] = useRecoilState(searchRecipeSearchSelector);
  const resetSearch = useResetRecoilState(searchRecipeSearchSelector);

  const [activeTab, setActiveTab] = useRecoilState(searchActiveTabSelector);
  const [showFilter, setShowFilter] = useRecoilState(searchShowFilterSelector)
  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)

  const {asPath, push} = useRouter();
  const {isAuthenticated, isInitialized} = useBefuAuthContext();
  const mainRef = useRef(null)

  useEffect(() => {
    const fetch = preFetch(STACK.RECIPE_SEARCH.TYPE)
    if (fetch) {
      setSearchRecipe(fetch)
      setTimeout(() => {
        mainRef?.current?.scrollTo(0, fetch?.scrollY)
      }, 100)
    } else if (searchCall?.type) {
      setIsCall(searchCall?.type)
      setIsTitle(true)
      switch (searchCall?.type) {
        case RECIPE_SEARCH_TYPE.USER:
          setShowFilter(false)
          setSearch(RECIPE_SEARCH_COMMON.SEARCH_TYPE_USER)
          setResult(RECIPE_SEARCH_COMMON.SEARCH_TYPE_USER)
          break
        case RECIPE_SEARCH_TYPE.CUSTOM:
          setShowFilter(false)
          setSearch(RECIPE_SEARCH_COMMON.SEARCH_TYPE_CUSTOM)
          setResult(RECIPE_SEARCH_COMMON.SEARCH_TYPE_CUSTOM)
          break
        case RECIPE_SEARCH_TYPE.SEASONING:
          setShowFilter(false)
          setSearch(searchCall?.search)
          setResult(searchCall?.search)
          break
        case RECIPE_SEARCH_TYPE.CATEGORY:
          setSearch(searchCall?.search)
          setResult(searchCall?.search)
          setCategory(RECIPE_SEARCH_HOME_CATEGORY.NAME)
          break;
        case RECIPE_SEARCH_TYPE.KEYWORD:
          setSearch(searchCall?.search)
          setResult(searchCall?.search)
          setCategory(RECIPE_SEARCH_HOME_HEALTH.NAME)
          break;
        case RECIPE_SEARCH_TYPE.MATERIAL:
          setSearch(searchCall?.search)
          setResult(searchCall?.search)
          setCategory(RECIPE_SEARCH_HOME_MATERIAL.NAME)
          break;
      }
      resetSearchCall()
    } else {
      setResult(RECIPE_RESULT_STATUS.NONE)
    }
    return () => {
      resetSearchRecipe()
    }
  }, [])

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      if (showDialog) {
        setShowDialog(false)
      } else {
        handleBack()
      }
    }
  }, [handleBackKey]);

  const handleBack = () => {
    if (isCall) {
      navigation.back()
    } else if (result) {
      const activeTab = searchRecipe?.activeTab
      resetSearchRecipe()
      setResult(RECIPE_RESULT_STATUS.NONE)
      setActiveTab(activeTab)
    } else if (isSearch) {
      resetSearch()
      resetIsSearch()
    } else {
      handleBackButton()
    }
  }

  const handleBackButton = () => {
    if (!isAuthenticated && asPath?.includes('d_inflow')) {
      push(PATH_DONOTS_PAGE?.GUEST);
    } else if (isAuthenticated && asPath?.includes('d_inflow')) {
      push(PATH_DONOTS_PAGE?.HOME);
    } else {
      navigation?.back();
    }
  }

  // 검색 화면 진입 초기화
  if (isInitialized && result === RECIPE_RESULT_STATUS.INIT) {
    return (
      <MainLayout>
        <HeaderSearchLayout/>
      </MainLayout>
    )
  }

  // 검색 입력 화면
  if (isSearch && !result) {
    return (
      <MainLayout>
        <HeaderSearchLayout onBack={handleBack}>
          <SearchPopularSection/>
        </HeaderSearchLayout>
      </MainLayout>
    )
  }

  // 검색 결과 화면
  if (result) {
    return (
      <MainLayout mainRef={mainRef}>
        <HeaderSearchLayout onBack={handleBack}>
          <SearchResultSection mainRef={mainRef}/>
        </HeaderSearchLayout>
      </MainLayout>
    )
  }

  // 홈 화면
  return (
    <GlobalNavigation>
      <MainLayout>
        <HeaderSearchLayout onBack={handleBack}>
          <HomeSection/>
        </HeaderSearchLayout>
      </MainLayout>
    </GlobalNavigation>
  )
}