import {useRecoilState, useResetRecoilState} from "recoil";
import {
  createEmptySelector,
  createRecipeSelector,
  createShowMaterialAddSelector,
  createShowMaterialEditSelector,
  createShowMaterialInputSelector,
  createShowMeasurementUnitSelector,
  createShowPreviewSelector,
  createShowReturnSelector,
  createTemporaryRecipeSelector,
} from "../../../recoil/selector/recipe/createSelector";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import RecipeCreateLayout from "../../../layouts/recipe/create/RecipeCreateLayout";
import {RECIPE_STATUS} from "../../../constant/recipe/Recipe";
import ExaminationSection from "../../../sections/recipe/create/ExaminationSection";
import CreateStepSection from "../../../sections/recipe/create/CreateStepSection";
import useNativeBackKey from "../../../hooks/useNativeBackKey";
import useStackNavigation from "../../../hooks/useStackNavigation";
import MainLayout from "../../../layouts/main/MainLayout";

RecipeCreatePage.getLayout = (page) => <RecipeCreateLayout>{page}</RecipeCreateLayout>;

export default function RecipeCreatePage() {
  const [showCreateStep, setShowCreateStep] = useState(false)
  const [showExamination, setShowExamination] = useState(false)
  const [empty, setEmpty] = useRecoilState(createEmptySelector)
  const [recipe, setRecipe] = useRecoilState(createRecipeSelector)
  const resetCreateRecipe = useResetRecoilState(createRecipeSelector)

  const [tempRecipe, setTempRecipe] = useRecoilState(createTemporaryRecipeSelector)
  const resetTempRecipe = useResetRecoilState(createTemporaryRecipeSelector)

  const [showReturn, setShowReturn] = useRecoilState(createShowReturnSelector)
  const [showPreview, setShowPreview] = useRecoilState(createShowPreviewSelector)
  const [showMaterialEdit, setShowMaterialEdit] = useRecoilState(createShowMaterialEditSelector)
  const [showMaterialAdd, setShowMaterialAdd] = useRecoilState(createShowMaterialAddSelector)
  const [showMaterialInput, setShowMaterialInput] = useRecoilState(createShowMaterialInputSelector)
  const [showMeasurementUnit, setShowMeasurementUnit] = useRecoilState(createShowMeasurementUnitSelector)

  const mainRef = useRef(null)

  const {handleBackKey} = useNativeBackKey()
  const {navigation} = useStackNavigation()
  useEffect(() => {
    if (handleBackKey) {
      if (showExamination) {
        handleBack()
      }
    }
  }, [handleBackKey]);

  const handleBack = () => {
    navigation.back()
  }

  useEffect(() => {
    setEmpty(recipe)
    setShowPreview(false)
    if (tempRecipe?.status) {
      switch (tempRecipe.status) {
        case RECIPE_STATUS.TEMPORARY:
        case RECIPE_STATUS.REJECT:
          setRecipe(tempRecipe)
          resetTempRecipe()
          setShowCreateStep(true)
          break
        case RECIPE_STATUS.EXAMINATION:
          setRecipe(tempRecipe)
          resetTempRecipe()
          setShowExamination(true)
          break
      }
    } else {
      setShowCreateStep(true)
    }
    return () => {
      resetCreateRecipe()
      setShowPreview(false)
      setShowReturn(false)
      setShowMaterialEdit(false)
      setShowMaterialAdd(false)
      setShowMaterialInput(false)
      setShowMeasurementUnit(false)
    }
  }, []);

  const handleModify = () => {
    setShowCreateStep(true)
    setShowExamination(false)
  };

  return (
    <MainLayout mainRef={mainRef}>
      {/* 레시피 작성*/}
      {showCreateStep && <CreateStepSection mainRef={mainRef} onBackKey={handleBackKey}/>}

      {/* 검수 */}
      <ExaminationSection open={showExamination} onModify={handleModify} onClose={handleBack}/>
    </MainLayout>
  );
}