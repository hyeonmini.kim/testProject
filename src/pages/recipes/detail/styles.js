// @mui
import {useTheme} from '@mui/material/styles';
import {GlobalStyles} from '@mui/material';

// ----------------------------------------------------------------------

export default function StyledNotistackDetail() {
  const theme = useTheme();

  const isLight = theme.palette.mode === 'light';

  const inputGlobalStyles = (
    <GlobalStyles
      styles={{
        '#__next': {
          '.SnackbarContainer-bottom': {
            bottom: '92px'
          },
          '.SnackbarContainer-root': {
            maxWidth: 'calc(100% - 40px)',
          },
          '.MuiCollapse-wrapper': {
            padding: '0px'
          },
          '.SnackbarContent-root': {
            width: '100%',
            margin: '10px 0px 0px 0px',
            boxShadow: theme.customShadows.z8,
            borderRadius: theme.shape.borderRadius,
            color: 'white',
            backgroundColor: '#222222',
            '&.SnackbarItem-variantSuccess, &.SnackbarItem-variantError, &.SnackbarItem-variantWarning, &.SnackbarItem-variantInfo':
              {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.background.paper,
              },
            [theme.breakpoints.up('md')]: {
              minWidth: 240,
            },
          },
          '.SnackbarItem-message': {
            height: '48px',
            padding: '0 !important',
            fontWeight: 400,
            fontSize: '14px',
          },
          '.SnackbarItem-contentRoot': {
            padding: '0px',
            paddingLeft: '16px'
          },
          '.SnackbarItem-action': {
            marginRight: 0,
            color: theme.palette.action.active,

            '& svg': {
              width: 20,
              height: 20,
            },
          },
        },
      }}
    />
  );

  return inputGlobalStyles;
}
