import * as React from 'react';
import {useEffect, useRef, useState} from 'react';
import {Box, Button, Container, Divider, Grid, IconButton, Skeleton, Stack, Tab, Tabs, Typography,} from '@mui/material';
import {getURIString, nvlString} from "../../../../../utils/formatString";
import Head from "next/head";
import CarouselDetail from "../../../../../sections/recipe/detail/carousel/CarouselDetail";
import useSticky from "../../../../../hooks/useSticky";
import classNames from "classnames";
import NutrientsInformationSection from "../../../../../sections/recipe/detail/NutrientsInformationSection";
import IngredientsBasicsSection from "../../../../../sections/recipe/detail/IngredientsBasicsSection";
import MakeStepsSection from "../../../../../sections/recipe/detail/MakeStepsSection";
import ReviewSection from "../../../../../sections/recipe/detail/ReviewSection";
import {
  BUTTON_TEXT,
  COUNT_TITLE,
  GRADE_POPUP_TEXT,
  GUEST_POPUP_TEXT,
  LOGIN_POPUP_TEXT,
  SNACKBAR_MESSAGE,
  TABS,
  VIEW_NUTRIENTS_PROPERTIES,
  VIEW_NUTRIENTS_SOURCE
} from "../../../../../constant/recipe/detail/DetailConstants";
import TopMainSection from "../../../../../sections/recipe/detail/TopMainSection";
import {useRecoilState, useRecoilValue, useSetRecoilState} from "recoil";
import {toastMessageSelector} from "../../../../../recoil/selector/common/toastSelector";
import {clickBorderNone, COMMON_DIALOG, COMMON_DIALOG_TYPE, TOAST_TYPE} from "../../../../../constant/common/Common";
import DetailLayout from "../../../../../layouts/recipe/detail/DetailLayout";
import PopupDialog from "../../../../../sections/recipe/detail/PopupDialog";
import {
  recipeDetailPopupDialogOpenStatusSelector,
  recipeDetailPopupDialogTypeSelector
} from "../../../../../recoil/selector/recipe/detail/detailSelector";
import {isNative} from "../../../../../utils/envUtils";
import {getRecipeDetail, getRecipeDetailFetch, getRecipeDetailWithEnabled, postRegistRecipeScrapMutate} from "../../../../../api/detailApi";
import useNativeBackKey from "../../../../../hooks/useNativeBackKey";
import useStackNavigation from "../../../../../hooks/useStackNavigation";
import {dehydrate, QueryClient, useQueryClient} from "react-query";
import IngredientsSeasoningsSection from "../../../../../sections/recipe/detail/IngredientsSeasoningsSection";
import {ICON_COLOR, SvgBackIcon} from "../../../../../constant/icons/icons";
import {ICON_TYPE, SvgCommonIcons} from "../../../../../constant/icons/ImageIcons";
import {useRouter} from "next/router";
import {STACK} from "../../../../../constant/common/StackNavigation";
import BookmarkScreen from "../../../../../components/detail/bookmark-screen";
import LoadingScreen from "../../../../../components/common/LoadingScreen";
import Carousel from "../../../../../components/recipe/detail/carousel";
import NutrientsCarouselDotsDetail from "../../../../../components/recipe/detail/carousel/NutrientsCarouselDotsDetail";
import {nvlNumber} from "../../../../../utils/formatNumber";
import {PATH_DONOTS_PAGE, PATH_DONOTS_STATIC_PAGE} from "../../../../../routes/paths";
import {dialogSelector, dialogShowDialogSelector} from "../../../../../recoil/selector/common/dialogSelector";
import {MAIN_TITLE} from "../../../../../constant/main/Main";
import {ERRORS, GET_FRONT_ERROR_MESSAGE} from "../../../../../constant/common/Error";
import {Z_INDEX} from "../../../../../constant/common/ZIndex";
import {logger} from "../../../../../utils/loggingUtils";
import {debounce} from "lodash";
import {getAllRecipes} from "../../../../../api/recipeApi";
import {setLocalStorage} from "../../../../../utils/storageUtils";
import {TIME} from "../../../../../constant/common/Time";
import {useBefuAuthContext} from "../../../../../auth/useAuthContext";
import {forMyPageEntrySelector} from "../../../../../recoil/selector/auth/userSelector";
import IFrameDialog from "../../../../../components/common/IFrameDialog";
import {myOpenMomStarSelector} from "../../../../../recoil/selector/my/popup/myPopupSelector";
import {setHackleTrack} from "../../../../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../../../../constant/common/Hackle";
import {handleEnterPress} from "../../../../../utils/onKeyDownUtils";
import MainLayout, {sxFixedCenterMainLayout} from "../../../../../layouts/main/MainLayout";
import {ALT_STRING} from "../../../../../constant/common/AltString";
import {MY_SETTING} from "../../../../../constant/my/Setting";
import DonotsMembership from "../../../../../sections/auth/complete/DonotsMembership";
import {GNB_TYPE} from "../../../../../constant/common/GNB";
import {gnbActiveTabSelector} from "../../../../../recoil/selector/common/gnbSelector";
import useInflow, {INFLOW_PAGE} from "../../../../../hooks/useInflow";

RecipeDetail.getLayout = (page) => <DetailLayout>{page}</DetailLayout>;

export default function RecipeDetail({recipe_key}) {
  const {isAuthenticated, isInitialized} = useBefuAuthContext();
  const userInfo = useRecoilValue(forMyPageEntrySelector)
  const [openMomStar, setOpenMomStar] = useRecoilState(myOpenMomStarSelector);
  const [activeTab, setActiveTab] = useRecoilState(gnbActiveTabSelector)
  const nutrientsRef = useRef(null);
  const howToMakeTopRef = useRef(null);
  const howToMakeBottomRef = useRef(null);
  const reviewRef = useRef(null);

  const [currentTab, setCurrentTab] = useState('nutrients');
  const returnTab = useRef(undefined);
  const [isSeasonings, setIsSeasonings] = useState(false);
  const [isHealthTag, setIsHealthTag] = useState(false);
  const [lottie, setLottie] = useState(false);

  const {navigation, preFetch} = useStackNavigation()

  const [open, setOpen] = useState(false);
  const [openSource, setOpenSource] = useState(false)
  const [openPopupDialog, setOpenPopupDialog] = useRecoilState(recipeDetailPopupDialogOpenStatusSelector);
  const [popupDialogType, setPopupDialogType] = useRecoilState(recipeDetailPopupDialogTypeSelector);
  const setDialogMessage = useSetRecoilState(dialogSelector)
  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)

  const {asPath, isReady} = useRouter();
  const router = useRouter()

  const {mutate: registRecipeScrap, isLoading} = postRegistRecipeScrapMutate()
  const {data: csr} = getRecipeDetailWithEnabled({
    recipe_key: recipe_key,
  }, (asPath?.includes('d_share') ? isInitialized : true))

  useEffect(() => {
    let observerNutrients;
    let observerHowToMakeTop;
    // let observerHowToMakeBottom;
    let observerReview;

    if (nutrientsRef?.current) {
      observerNutrients = new IntersectionObserver(onIntersectNutrients, {
        threshold: 0.01
      })
      observerNutrients?.observe(nutrientsRef?.current)
    }

    // if (howToMakeTopRef?.current) {
    //   observerHowToMakeTop = new IntersectionObserver(onIntersectHowToMakeTop, {
    //     threshold: 0.8
    //     // rootMargin: '0px 0px 100px 0px'
    //   })
    //   observerHowToMakeTop?.observe(howToMakeTopRef?.current)
    // }

    // if (howToMakeBottomRef?.current) {
    //   observerHowToMakeBottom = new IntersectionObserver(onIntersectHowToMakeBottom, {
    //     threshold: 0.1,
    //     // rootMargin: '0px 0px 100px 0px'
    //   })
    //   observerHowToMakeBottom?.observe(howToMakeBottomRef?.current)
    // }

    if (reviewRef?.current) {
      observerReview = new IntersectionObserver(onIntersectReview, {
        threshold: 0.99
      })
      observerReview?.observe(reviewRef?.current)
    }
  }, [csr, isAuthenticated])

  const handlePushLoginPage = () => {
    setDialogMessage({
      type: COMMON_DIALOG_TYPE?.TWO_BUTTON,
      message: LOGIN_POPUP_TEXT?.TITLE,
      leftButtonProps: {text: LOGIN_POPUP_TEXT?.BUTTON_TEXT?.CLOSE},
      rightButtonProps: {
        text: LOGIN_POPUP_TEXT?.BUTTON_TEXT?.GO_LOGIN, onClick: () => {
          const callbackUrl = PATH_DONOTS_PAGE?.RECIPE?.DETAIL(csr?.recipe_writer_info?.recipe_writer_nickname, getURIString(csr?.recipe_name), csr?.recipe_key)
          setLocalStorage('signInCallback', callbackUrl, TIME?.MS_FIVE_MINUTE)
          router?.push(PATH_DONOTS_PAGE.AUTH.LOGIN)
        }
      },
    })
  }

  const handleShowGradePopup = () => {
    setDialogMessage({
      type: COMMON_DIALOG_TYPE?.TWO_BUTTON,
      message: GRADE_POPUP_TEXT?.TITLE,
      isBoldMsg: true,
      subMessage: GRADE_POPUP_TEXT?.SUB_TITLE,
      children: COMMON_DIALOG.children(<DonotsMembership active={'LV1'}/>),
      leftButtonProps: {text: GRADE_POPUP_TEXT?.BUTTON_TEXT?.CLOSE},
      rightButtonProps: {
        text: GRADE_POPUP_TEXT?.BUTTON_TEXT?.SHOW_BENEFIT, onClick: () => {
          setActiveTab(GNB_TYPE.MY)
          router.replace(PATH_DONOTS_PAGE.MY.SELF)
        }
      },
    })
  }

  const {
    tabSticky,
    tabStickyChildren,
    bottomSticky,
    tabStickyRef,
    bottomStickyRef,
    carouselRef,
    isShowHeader,
    isShowGoToTop,
    loginRef,
    imageRef,
    mainRef,
    lastScrollY,
    tabStickyOffsetRef
  } = useSticky(csr, handlePushLoginPage, handleShowGradePopup);

  const onIntersectNutrients = async ([entry], observerNutrients) => {
    if ((isAuthenticated && !((userInfo?.type === "NORMAL_MEMBER" && userInfo?.grade === "LV1") && csr?.recipe_writer_info?.recipe_writer_type === "EXPERT")) || (!isAuthenticated && csr?.recipe_writer_info?.recipe_writer_type !== "EXPERT")) {
      if (entry?.isIntersecting) {
        setCurrentTab('nutrients')
        returnTab.current = 'nutrients'
      }

      if (!entry?.isIntersecting) {
        if (returnTab.current === 'nutrients' && (lastScrollY?.current > (tabStickyOffsetRef?.current - window?.innerHeight))) {
          setCurrentTab('howToMake')
          returnTab.current = 'howtoMake'
        }
      }
    }
  }

  // const onIntersectHowToMakeTop = async ([entry], observerHowToMake) => {
  //   if (entry?.isIntersecting) {
  //     setCurrentTab('howToMake')
  //     returnTab.current = 'howToMake'
  //   }
  // }

  // const onIntersectHowToMakeBottom = async ([entry], observerHowToMake) => {
  //   if (entry?.isIntersecting) {
  //     setCurrentTab('howToMake')
  //   }
  // }

  const onIntersectReview = async ([entry], observerReview) => {
    if ((isAuthenticated && !((userInfo?.type === "NORMAL_MEMBER" && userInfo?.grade === "LV1") && csr?.recipe_writer_info?.recipe_writer_type === "EXPERT")) || (!isAuthenticated && csr?.recipe_writer_info?.recipe_writer_type !== "EXPERT")) {
      if (entry?.isIntersecting) {
        setCurrentTab('review')
        returnTab.current = 'review'
      }

      if (!entry?.isIntersecting) {
        if (returnTab.current === 'review') {
          setCurrentTab('howToMake')
          returnTab.current = 'howtoMake'
        }
      }
    }
  }

  //-------------------------------------------------

  const handleTabClick = (value) => {
    if ((isAuthenticated && !((userInfo?.type === "NORMAL_MEMBER" && userInfo?.grade === "LV1") && csr?.recipe_writer_info?.recipe_writer_type === "EXPERT")) || (!isAuthenticated && csr?.recipe_writer_info?.recipe_writer_type !== "EXPERT")) {
      switch (value) {
        case 'nutrients' :
          setHackleTrack(HACKLE_TRACK.RECIPE_DETAIL_TAB_NUTRITION, {
            recipeid: recipe_key,
            recipename: csr?.recipe_name
          })
          nutrientsRef?.current?.scrollIntoView({behavior: 'smooth'});
          break;
        case 'howToMake' :
          setHackleTrack(HACKLE_TRACK.RECIPE_DETAIL_TAB_RECIPE, {
            recipeid: recipe_key,
            recipename: csr?.recipe_name
          })
          howToMakeTopRef.current.style.scrollMargin = '50px';
          if (mainRef?.current?.scrollTop < howToMakeTopRef?.current?.getBoundingClientRect()?.top + mainRef?.current?.scrollTop)
            howToMakeTopRef.current.style.scrollMargin = '0px';
          howToMakeTopRef?.current?.scrollIntoView({behavior: 'smooth'});
          break;
        case 'review' :
          setHackleTrack(HACKLE_TRACK.RECIPE_DETAIL_TAB_REVIEW, {
            recipeid: recipe_key,
            recipename: csr?.recipe_name
          })
          reviewRef?.current?.scrollIntoView({behavior: 'smooth'});
          break;
      }
    } else {
      if (!isAuthenticated) {
        handlePushLoginPage()
      } else {
        handleShowGradePopup()
      }
    }
  };

  const backButtonClick = () => {
    if (!isNative() && asPath?.includes('d_share')) {
      router?.push(PATH_DONOTS_PAGE?.GUEST);
    } else if (asPath?.includes('d_share')) {
      router?.push(PATH_DONOTS_PAGE?.HOME);
    } else if (!isAuthenticated && asPath?.includes('d_inflow')) {
      router?.push(PATH_DONOTS_PAGE?.GUEST);
    } else if (isAuthenticated && asPath?.includes('d_inflow')) {
      router?.push(PATH_DONOTS_PAGE?.HOME);
    } else {
      loginRef.current = null
      navigation?.back();
    }
  }

  const handleClickOpenPopupDialog = (value) => {
    if (value === 'nutrients') {
      setHackleTrack(HACKLE_TRACK.RECIPE_DETAIL_NUTRITION, {
        recipeid: csr?.recipe_key,
        recipename: csr?.recipe_name
      })
    }
    setOpenPopupDialog(true);
    setPopupDialogType(value);
  };

  const handleClosePopupDialog = () => {
    setOpenPopupDialog(false);
    setPopupDialogType(null);
  };

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      if (open) {
        setOpen(false)
      } else if (openPopupDialog) {
        setOpenPopupDialog(false)
      } else if (openMomStar) {
        setOpenMomStar(false)
      } else if (openSource) {
        setOpenSource(false)
      } else if (showDialog) {
        setShowDialog(false)
      } else {
        backButtonClick()
      }
    }
  }, [handleBackKey]);

  useEffect(() => {
    const fetch = preFetch(STACK?.RECIPE_DETAIL?.TYPE)
    if (fetch) {
      setTimeout(() => {
        mainRef?.current?.scrollTo(0, fetch?.scrollY)
      }, 100)
    } else {
      mainRef?.current?.scrollTo(0, 0)
    }

    csr?.recipe_ingredient_list?.map((item) => {
      item?.recipe_ingredient_category === "양념재료" && setIsSeasonings(true);
    })
    csr?.recipe_health_tag !== "" && setIsHealthTag(true);
  }, [])

  useEffect(() => {
    if (isInitialized && csr) {
      setHackleTrack(HACKLE_TRACK.RECIPE, {
        recipeid: recipe_key,
        recipename: csr?.recipe_name
      })
    }
  }, [])

  const {inflow} = useInflow()
  useEffect(() => {
    if (isReady && isInitialized) {
      inflow(INFLOW_PAGE.RECIPE)
    }
  }, [isReady, isInitialized]);


  if (!csr) {
    return (
      <MainLayout>
        <SkeletonDetail backButtonClick={backButtonClick}/>
      </MainLayout>
    )
  }

  return (
    <MainLayout mainRef={mainRef}>
      <Head>
        <title>{MAIN_TITLE?.HEADER(csr?.recipe_name)}</title>
        <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta property="og:title" content={csr?.recipe_name}/>
        <meta property="og:description" content={csr?.recipe_desc}/>
        <meta property="og:image" content={csr?.recipe_main_img_path}/>
      </Head>

      <Container maxWidth={'xs'} disableGutters sx={{px: '0px', position: 'absolute', left: 0, top: 0, zIndex: 999}}>
        <Box sx={{width: '48px', height: '48px', background: 'transparent', pl: '20px', pr: '48px', pb: '68px'}}
             onClick={backButtonClick}>
          <SvgBackIcon
            alt={ALT_STRING.COMMON.BTN_BACK}
            tabIndex={1}
            onKeyDown={(e) => handleEnterPress(e, backButtonClick)}
            color={ICON_COLOR?.WHITE}
            sx={{position: 'absolute', mt: '17px', zIndex: 99, ...clickBorderNone}}/>
        </Box>
      </Container>

      <Box maxWidth={'xs'} sx={{px: '0px',}} ref={carouselRef}>
        <CarouselDetail csr={csr} open={open} setOpen={setOpen} handlePushLoginPage={handlePushLoginPage}
                        handleShowGradePopup={handleShowGradePopup}/>
      </Box>

      <Box maxWidth={'xs'} sx={{
        mt: '-34px',
        width: '100%',
        position: 'absolute',
        backgroundColor: 'white',
        borderRadius: '30px',
        zIndex: 99
      }}>

        <Container maxWidth={'xs'} disableGutters sx={{px: '20px'}}>
          <Stack
            direction="row"
            sx={{
              mt: '34px',
            }}
          ></Stack>
        </Container>

        {isHealthTag && <HealthTagSection csr={csr}/>}

        <TopMainSection csr={csr}/>

        <CountSection csr={csr}/>

        <Box
          ref={bottomStickyRef}
          sx={[bottomSticky && {
            position: 'fixed',
            bottom: '0px',
            bgcolor: 'transparent',
            zIndex: Z_INDEX?.BOTTOM_FIXED_BUTTON,
            ...sxFixedCenterMainLayout,
          }]
          }
        >
          <BottomSticky isShowGoToTop={isShowGoToTop} csr={csr} recipe_key={recipe_key} mainRef={mainRef} setLottie={setLottie}
                        registRecipeScrap={registRecipeScrap} handlePushLoginPage={handlePushLoginPage}
                        handleShowGradePopup={handleShowGradePopup}/>
        </Box>

        <Box sx={{mb: '30px'}}/>

        <Box
          ref={tabStickyRef}
          sx={[tabSticky && {
            position: 'fixed',
            top: '0px',
            bgcolor: 'white',
            zIndex: Z_INDEX?.BOTTOM_FIXED_BUTTON,
            boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.16)',
            ...sxFixedCenterMainLayout
          }]
          }
        >
          <TabSticky csr={csr} tabSticky={tabSticky} isShowHeader={isShowHeader} backButtonClick={backButtonClick}
                     currentTab={currentTab} handleTabClick={handleTabClick}/>
        </Box>

        <Box className={classNames({tabStickyChildren})}>
          <Box ref={nutrientsRef}>
            <NutrientsInformationSection csr={csr} mainRef={mainRef}/>
          </Box>

          <PopupDialog csr={csr}/>

          {
            (!isInitialized || (isInitialized && !isAuthenticated) || (isAuthenticated && (userInfo?.type === "NORMAL_MEMBER" && userInfo?.grade === "LV1") && csr?.recipe_writer_info?.recipe_writer_type === "EXPERT")) &&
            <Box ref={imageRef} sx={{position: 'relative'}}>
              <img src='/assets/images/detail/blur.jpg' alt={ALT_STRING.RECIPE_DETAIL.RECIPE_PREVIEW}/>

              {(!isInitialized || (isInitialized && !isAuthenticated && csr?.recipe_writer_info?.recipe_writer_type !== "EXPERT")) &&
                <GuestAbsoluteDialog/>
              }
            </Box>
          }

          <Box ref={loginRef}/>

          {(!isInitialized || (isInitialized && (isAuthenticated && !((userInfo?.type === "NORMAL_MEMBER" && userInfo?.grade === "LV1") && csr?.recipe_writer_info?.recipe_writer_type === "EXPERT")) || (!isAuthenticated && csr?.recipe_writer_info?.recipe_writer_type !== "EXPERT"))) &&
            <Box>
              {csr?.recipe_nutrient_list?.length > 0 &&
                <>
                  <Container maxWidth={'xs'} disableGutters sx={{px: '20px'}}>
                    <Box sx={{height: '40px'}}/>
                    <Divider sx={{maxWidth: "xs", mt: '40px'}}/>
                    <Stack direction={'row'} sx={{justifyContent: 'space-between', my: '10px'}}>
                      <Typography
                        tabIndex={9}
                        variant={'b2_14_r_1l'}
                        sx={{
                          textDecoration: 'underline',
                          color: '#666666',
                        }}
                        onClick={() => setOpenSource(true)}
                        onKeyDown={(e) => handleEnterPress(e, () => setOpenSource(true))}
                      >
                        {VIEW_NUTRIENTS_SOURCE}
                      </Typography>
                      <Typography
                        tabIndex={9}
                        variant={'b2_14_r_1l'}
                        sx={{
                          textDecoration: 'underline',
                          color: '#666666',
                        }}
                        onClick={() => handleClickOpenPopupDialog("nutrients")}
                        onKeyDown={(e) => handleEnterPress(e, () => handleClickOpenPopupDialog("nutrients"))}
                      >
                        {VIEW_NUTRIENTS_PROPERTIES}
                      </Typography>
                    </Stack>
                  </Container>

                  <Divider sx={{maxWidth: "xs", px: '0px', mt: '50px'}}/>
                </>
              }

              <Box ref={howToMakeTopRef}>
                <Container maxWidth={'xs'} disableGutters sx={{px: '20px', pt: '100px', mt: '-50px'}} id="howToMake">
                  <IngredientsBasicsSection csr={csr}/>
                  {isSeasonings && <IngredientsSeasoningsSection csr={csr}/>}
                </Container>
              </Box>

              <Divider sx={{maxWidth: "xs", px: '0px', mt: '50px', mb: '20px'}}/>

              <Box>
                <Container maxWidth={'xs'} disableGutters sx={{px: '0px', mt: '20px'}}>
                  <MakeStepsSection csr={csr}/>
                </Container>
              </Box>

              {csr?.recipe_tag_list?.length > 0 && <RecipeTagSection tags={csr?.recipe_tag_list}/>}

              <Divider sx={{maxWidth: "xs", px: '0px', mt: '20px'}}/>

              <Box ref={reviewRef}>
                <Container maxWidth={'xs'} disableGutters sx={{px: '20px', pt: '100px', mt: '-50px'}}>
                  <ReviewSection csr={csr} handlePushLoginPage={handlePushLoginPage}/>
                </Container>
              </Box>
            </Box>
          }
        </Box>
      </Box>
      <IFrameDialog
        backTabIndex={0}
        open={openMomStar}
        url={PATH_DONOTS_STATIC_PAGE?.MEMBERSHIP()}
        onClose={() => setOpenMomStar(false)}
      />

      <IFrameDialog
        backTabIndex={0}
        open={openSource}
        title={MY_SETTING.FAQ.HEADER_TITLE}
        url={PATH_DONOTS_STATIC_PAGE.SOURCE()}
        onBack={() => setOpenSource(false)}
      />

      {isLoading && <LoadingScreen/>}
      {lottie && <BookmarkScreen/>}
    </MainLayout>
  );
}

function GuestAbsoluteDialog() {
  const router = useRouter()

  return (
    <Box sx={{position: 'absolute', top: 0, left: 0, backgroundColor: 'rgba(0, 0, 0, 0.16)', width: '100%', height: '100%'}}>
      <Box
        sx={{
          position: 'absolute',
          alignItems: 'center',
          justifyContent: 'center',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          background: 'white',
          width: 'calc(100% - 40px)',
          height: '178px',
          borderRadius: '8px',
          padding: '20px'
          // padding: '10px'
        }}
      >
        <Box sx={{mt: '10px', justifyContent: 'center'}}>
          <Typography
            sx={{
              whiteSpace: 'pre-line',
              textAlign: 'center',
              fontSize: '16px',
              fontWeight: '400',
              lineHeight: '24px',
              color: '#222222'
            }}
          >
            {GUEST_POPUP_TEXT?.TITLE}
          </Typography>

          <Button
            fullWidth
            variant="contained"
            onClick={() => router?.push(PATH_DONOTS_PAGE.AUTH.LOGIN)}
            sx={{
              mt: '30px',
              height: '50px',
              fontSize: '16px',
              fontWeight: '700',
              lineHeight: '28px',
              backgroundColor: 'primary.main',
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: 'primary.main',
              },
            }}
          >
            {GUEST_POPUP_TEXT?.BUTTON_TEXT?.GO_LOGIN}
          </Button>
        </Box>
      </Box>
    </Box>
  )
}

function RecipeTagSection({tags}) {
  return (
    <Container maxWidth={'xs'} disableGutters sx={{pt: '20px', pb: '30px', px: '20px'}}>
      <Typography variant={'b2_14_r'} sx={{color: '#888888'}}>
        {tags && tags?.map((tag, index) => (
          tag && (
            '#' + nvlString(tag?.recipe_tag_desc) + (index < tags?.recipe_tag_list?.length - 1 ? ',' : '')
          )
        ))}
      </Typography>
    </Container>
  )
}

function HealthTagSection({csr}) {
  const [healthTagList, setHealthTagList] = useState([]);
  const [expanded, setExpanded] = useState(false);

  useEffect(() => {
    csr?.recipe_health_tag && (setHealthTagList(csr?.recipe_health_tag?.split(',')))
  }, [])

  const handleClick = () => {
    setHackleTrack(HACKLE_TRACK.RECIPE_DETAIL_HEALTHTAG)
    setExpanded(true)
  }

  const handleCloseClick = () => {
    setExpanded(false)
  }

  return (
    <Container maxWidth={'xs'} disableGutters sx={{px: '20px'}}>
      <Stack
        direction="row"
        flexWrap="wrap"
        sx={{
          mb: '10px',
          alignItems: 'center',
        }}
      >
        {healthTagList ? (healthTagList?.length <= 2 ? (
          healthTagList?.map((item, index) => item ? (
              <Box sx={{
                height: '32px',
                padding: '4px 10px',
                alignItems: 'center',
                backgroundColor: '#EFFAEC',
                borderRadius: '8px',
                mb: '10px',
                mr: '10px'
              }} key={index}>
                <Typography variant={'b2_14_r_1l'} sx={{color: '#3D6A4A', lineHeight: '24px'}}>
                  {nvlString(item)}
                </Typography>
              </Box>
            ) : (<Skeleton variant="rectangular" sx={{height: '32px', borderRadius: '8px',}}/>)
          )) : (
          healthTagList?.map((item, index) => item ? (
            !expanded && index < 2 ? (
              <Box sx={{
                height: '32px',
                padding: '4px 10px',
                alignItems: 'center',
                backgroundColor: '#EFFAEC',
                borderRadius: '8px',
                mb: '10px',
                mr: '10px'
              }} key={index}>
                <Typography variant={'b2_14_r_1l'} sx={{color: '#3D6A4A', lineHeight: '24px'}}>
                  {nvlString(item)}
                </Typography>
              </Box>
            ) : (expanded && (
              <Box sx={{
                height: '32px',
                padding: '4px 10px',
                alignItems: 'center',
                backgroundColor: '#EFFAEC',
                borderRadius: '8px',
                mb: '10px',
                mr: '10px'
              }} key={index}>
                <Typography variant={'b2_14_r_1l'} sx={{color: '#3D6A4A', lineHeight: '24px'}}>
                  {nvlString(item)}
                </Typography>
              </Box>))) : (<Skeleton variant="rectangular" sx={{height: '32px', borderRadius: '8px',}}/>)))) : (
          <Skeleton variant="rectangular" width="100%" sx={{height: '32px', borderRadius: '8px',}}/>
        )}
        {!expanded && healthTagList?.length > 2 &&
          <Box
            tabIndex={3}
            sx={{
              display: "flex",
              width: '30px',
              height: '30px',
              mb: '10px',
              mr: '10px',
              alignItems: 'center',
              justifyContent: 'center',
              border: '1px solid #E6E6E6',
              borderRadius: '6px',
              ...clickBorderNone
            }}
            onClick={handleClick}
            onKeyDown={(e) => handleEnterPress(e, handleClick)}
          >
            <Stack
              direction="row"
              sx={{
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <SvgCommonIcons type={ICON_TYPE?.ELLIPSE} sx={{mr: '3px'}}/>
              <SvgCommonIcons type={ICON_TYPE?.ELLIPSE}/>
              <SvgCommonIcons type={ICON_TYPE?.ELLIPSE} sx={{ml: '3px'}}/>
            </Stack>
          </Box>}
        {expanded && healthTagList?.length > 2 &&
          <Box
            tabIndex={3}
            sx={{
              display: "flex",
              width: '60px',
              height: '32px',
              mb: '10px',
              mr: '10px',
              alignItems: 'center',
              justifyContent: 'center',
              border: '1px solid #E6E6E6',
              borderRadius: '8px',
              ...clickBorderNone
            }}
            onClick={handleCloseClick}
            onKeyDown={(e) => handleEnterPress(e, handleCloseClick)}
          >
            <Stack
              direction="row"
              sx={{
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Typography variant={'bs_12_r_1l'} sx={{color: '#666666', mr: '2px'}}>
                {BUTTON_TEXT?.FOLD}
              </Typography>
              <IconButton sx={{
                p: 0,
                backgroundColor: "#FFFFFF",
                '&:hover': {
                  boxShadow: 'none',
                  backgroundColor: '#FFFFFF',
                },
              }}>
                <SvgCommonIcons type={ICON_TYPE?.ARROW_UP_XSMALL_16PX}/>
              </IconButton>
            </Stack>
          </Box>}
      </Stack>
    </Container>
  );
}

function CountSection({csr}) {
  return (
    <Container maxWidth={'xs'} disableGutters sx={{px: '20px'}}>
      <Stack
        direction="row"
        sx={{
          mb: '10px',
        }}
      >
        <Typography variant={'b2_14_r_1l'} sx={{mr: '2px', color: '#888888'}}>
          {COUNT_TITLE?.VIEWS}
        </Typography>
        <Typography variant={'b2_14_r_1l'} sx={{mr: '16px', color: '#888888'}}>
          {nvlNumber(csr?.recipe_view_cnt)}
        </Typography>
        <Typography variant={'b2_14_r_1l'} sx={{mr: '2px', color: '#888888'}}>
          {COUNT_TITLE?.SCRAP}
        </Typography>
        <Typography variant={'b2_14_r_1l'} sx={{mr: '16px', color: '#888888'}}>
          {nvlNumber(csr?.recipe_scrap_cnt)}
        </Typography>
      </Stack>
    </Container>
  );
}

function BottomSticky({isShowGoToTop, csr, recipe_key, mainRef, setLottie, registRecipeScrap, handlePushLoginPage, handleShowGradePopup}) {
  const {isAuthenticated, isInitialized} = useBefuAuthContext();
  const userInfo = useRecoilValue(forMyPageEntrySelector)
  const [recipe_scrap_yn, setRecipe_scrap_yn] = useState('');
  const queryClient = useQueryClient()
  const setToastMessage = useSetRecoilState(toastMessageSelector);
  const setDialogMessage = useSetRecoilState(dialogSelector)
  const router = useRouter()

  useEffect(() => {
    setRecipe_scrap_yn(csr?.recipe_scrap_yn || 'N')
  },)

  const handleClickScrap = debounce(() => {
    if (isAuthenticated) {
      if (!((userInfo?.type === "NORMAL_MEMBER" && userInfo?.grade === "LV1") && csr?.recipe_writer_info?.recipe_writer_type === "EXPERT")) {
        if (recipe_scrap_yn === 'N') {
          setHackleTrack(HACKLE_TRACK.RECIPE_DETAIL_SCRAP, {
            recipeid: recipe_key,
            recipename: csr?.recipe_name
          })
        }
        registRecipeScrap({
          recipe_key: recipe_key,
          recipe_scrap_yn: recipe_scrap_yn === "Y" ? "N" : "Y"
        }, {
          onSuccess: (data) => {
            queryClient?.invalidateQueries('getRecipeDetail')

            setToastMessage({
              type: TOAST_TYPE?.BOTTOM_HEADER,
              message: recipe_scrap_yn === "Y" ? SNACKBAR_MESSAGE?.SCRAP_OFF : SNACKBAR_MESSAGE?.SCRAP_ON
            })

            if (recipe_scrap_yn === "N") {
              setLottie(true)

              setTimeout(() => {
                setLottie(false)
              }, 750);
            }
          }
        })
      } else {
        handleShowGradePopup()
      }
    } else {
      handlePushLoginPage()
    }
  }, 300);

  const handleClickShare = debounce(() => {
    if (isAuthenticated) {
      if (!((userInfo?.type === "NORMAL_MEMBER" && userInfo?.grade === "LV1") && csr?.recipe_writer_info?.recipe_writer_type === "EXPERT")) {
        setHackleTrack(HACKLE_TRACK.RECIPE_DETAIL_SHARE, {
          recipeid: recipe_key,
          recipename: csr?.recipe_name
        })
        if (isNative()) {
          try {
            window?.flutter_inappwebview?.callHandler('openNativeShare');
          } catch (err) {
            logger?.error(err?.message);
          }
        } else {
          if (navigator?.share) {
            navigator?.share({
              text: decodeURI(window?.location?.href),
            })
              ?.then((e) => {
                logger?.error(e);
              })
              ?.catch((error) => logger?.error('공유 실패', error));
          } else {
            setToastMessage({
              type: TOAST_TYPE?.BOTTOM_SYSTEM_ERROR,
              message: GET_FRONT_ERROR_MESSAGE(ERRORS?.INVALID_ENVIRONMENT),
            })
          }
        }
      } else {
        handleShowGradePopup()
      }
    } else {
      handlePushLoginPage()
    }
  }, 300);

  return (
    <>
      {isShowGoToTop &&
        <Container
          maxWidth={'xs'}
          disableGutters
          sx={{
            position: 'fixed',
            pr: '20px',
            bottom: '20px',
            zIndex: Z_INDEX.BOTTOM_FIXED_BUTTON,
            '&.MuiContainer-root': {background: 'none',},
          }}
        >
          <IconButton
            tabIndex={-1}
            disableFocusRipple
            onClick={() => mainRef?.current?.scrollTo({top: 0, left: 0, behavior: 'smooth'})}
            sx={{
              position: 'fixed',
              bottom: '92px',
              right: '20px',
              m: '-8px',
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: 'transparent'
              },
            }}
          >
            <SvgCommonIcons
              alt={ALT_STRING.RECIPE_DETAIL.BTN_MOVE_UP}
              tabIndex={11}
              onKeyDown={(e) => handleEnterPress(e, () => mainRef?.current?.scrollTo({top: 0, left: 0, behavior: 'smooth'}))}
              type={ICON_TYPE?.BTN_TOP}
              sx={{...clickBorderNone}}/>
          </IconButton>
        </Container>
      }

      <Container maxWidth={'xs'} disableGutters sx={{px: '20px', pt: '20px', backgroundColor: 'white'}}>
        <Stack
          direction="row"
          spacing={'10px'}
        >
          {/*<StyledNotistackDetail />*/}

          <Button tabIndex={18} disableFocusRipple scraped="false" fullWidth variant="contained" startIcon={
            recipe_scrap_yn === "Y" ? (
              <SvgCommonIcons type={ICON_TYPE?.SCRAP_ON}/>
            ) : (
              <SvgCommonIcons type={ICON_TYPE?.SCRAP_OFF}/>
            )}
                  sx={{
                    height: '52px',
                    '& .MuiButton-startIcon': {marginLeft: 0, marginRight: '2px',},
                    ...clickBorderNone
                  }}
                  onClick={() => {
                    handleClickScrap()
                  }}>
            <Typography variant={'btn'} sx={{display: 'contents', color: '#FFFFFF'}}>
              {BUTTON_TEXT?.SCRAP}
            </Typography>
          </Button>
          <Button
            tabIndex={19}
            disableFocusRipple
            variant="outlined"
            sx={{
              backgroundColor: 'white',
              height: '52px',
              width: '52px',
              minWidth: '52px',
              '&:hover': {backgroundColor: 'white'},
              ...clickBorderNone
            }}
            onClick={() => {
              // setToastMessage({type: TOAST_TYPE?.BOTTOM_HEADER, message: SNACKBAR_MESSAGE?.SHARE})
              handleClickShare()
            }}
          >
            <SvgCommonIcons alt={ALT_STRING.RECIPE_DETAIL.BTN_SHARE} type={ICON_TYPE?.SHARE}/>
          </Button>
        </Stack>
        <Box sx={{height: '20px'}}/>
      </Container>
    </>
  );
}

function TabSticky({csr, tabSticky, isShowHeader, backButtonClick, currentTab, handleTabClick}) {
  return (
    <>
      {isShowHeader && (
        <Container maxWidth={'xs'} disableGutters sx={{px: '20px', height: '54px', backgroundColor: 'white'}}>
          <Stack
            direction="row"
            sx={{
              position: 'fixed',
              top: '0px',
              backgroundColor: 'white',
              zIndex: '99998',
              my: '19px',
              alignItems: 'center',
              width: '100%',
              maxWidth: '420px'
            }}
          >
            <IconButton tabIndex={-1} onClick={backButtonClick} sx={{
              m: '-8px',
              backgroundColor: "#FFFFFF",
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: '#FFFFFF',
              },
            }}>
              <SvgBackIcon alt={ALT_STRING.COMMON.BTN_BACK} color={ICON_COLOR?.BLACK}/>
            </IconButton>
            <Typography variant={'b_18_b'} sx={{
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              display: '-webkit-box',
              WebkitLineClamp: 1,
              WebkitBoxOrient: 'vertical',
              height: '24px',
              pr: '60px',
              color: '#000000',
              ml: '10px',
              width: '100%',
              textAlign: 'center',
            }}>
              {nvlString(csr?.recipe_name)}
            </Typography>
          </Stack>
        </Container>
      )
      }

      <Container maxWidth={'xs'} disableGutters sx={{px: '0px'}}>
        <Box sx={{
          borderTop: isShowHeader || tabSticky ? 'none' : '1px solid #E6E6E6',
          height: isShowHeader || tabSticky ? 'inherit' : '50px',
          borderBottom: '1px solid #E6E6E6',
        }}>
          <Tabs sx={{
            '& .MuiTabs-indicator': {backgroundColor: 'primary.main'},
            '& .MuiTab-root': {color: 'primary.main'},
            '& .Mui-selected': {color: 'primary.main'},
            '& .MuiButtonBase-root.MuiTab-root.Mui-selected': {borderBottom: '1px solid primary.main'},
          }} variant="fullWidth" value={currentTab}>
            {TABS?.slice(0, 3)?.map((tab, index) => (
              <Tab
                tabIndex={5}
                key={tab?.value}
                value={tab?.value}
                label={tab?.label}
                sx={{
                  typography: 'b1_16_b_1l',
                  '&:not(:last-of-type)': {marginRight: '0px',},
                  '&:not(.Mui-selected)': {color: '#666666',},
                  ...clickBorderNone
                }}
                onClick={(value) => handleTabClick(tab?.value)}
                onKeyDown={(e) => handleEnterPress(e, () => handleTabClick(tab?.value))}
              />
            ))}
          </Tabs>
        </Box>
      </Container>
    </>
  );
}

function SkeletonDetail({backButtonClick}) {
  const carouselSettings = {
    dots: true,
    arrows: false,
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    edgeFriction: 0,
    // rtl: Boolean(theme.direction === 'rtl'),
    ...NutrientsCarouselDotsDetail({
      rounded: false,
      sx: {
        bottom: 0,
        mt: '10px',
        zIndex: 10,
        width: '100%',
        display: 'flex',
        position: 'relative',
        alignItems: 'flex-end',
      },
    }),
  };

  return (
    <>
      <Container maxWidth={'xs'} disableGutters sx={{position: 'relative',}}>
        <Box maxWidth={'xs'} sx={{px: '0px',}}>
          <Skeleton variant="rectangular" width="100%" sx={{height: 360}}/>
        </Box>
        <Container maxWidth={'xs'} disableGutters sx={{px: '0px', position: 'absolute', left: 0, top: 0, zIndex: 99}}>
          <Box sx={{width: '48px', height: '48px', background: 'transparent', pl: '20px', pr: '48px', pb: '68px'}}
               onClick={backButtonClick}>
            <SvgBackIcon alt={ALT_STRING.COMMON.BTN_BACK} color={ICON_COLOR?.BLACK} sx={{position: 'absolute', mt: '17px', zIndex: 99,}}/>
          </Box>
        </Container>
        <Box maxWidth={'xs'} sx={{
          mt: '-34px',
          width: '100%',
          position: 'absolute',
          backgroundColor: 'white',
          borderRadius: '30px',
          zIndex: 99
        }}>

          <Container maxWidth={'xs'} disableGutters sx={{px: '20px'}}>
            <Stack
              direction="row"
              sx={{
                mt: '34px',
              }}
            ></Stack>
          </Container>

          <Container maxWidth={'xs'} disableGutters sx={{px: '20px'}}>
            <Stack
              direction="row"
              flexWrap="wrap"
              sx={{
                mb: '10px',
                alignItems: 'center',
              }}
            >
              {[...Array(2)]?.map((item, index) => (
                <Skeleton variant="rectangular" width="100px" sx={{
                  height: '32px',
                  borderRadius: '8px',
                  mb: '10px',
                  mr: '10px'
                }} key={index}/>))}
            </Stack>
          </Container>

          <Container maxWidth={'xs'} disableGutters sx={{px: '20px', display: 'grid'}}>
            <Skeleton variant="rectangular" width="136px" sx={{height: '24px'}}/>
            <Skeleton variant="rectangular" width="233px" sx={{mt: '8px', height: '16px'}}/>

            <Stack
              direction="row"
              alignItems="center"
              sx={{mt: '16px'}}
            >
              <SvgCommonIcons type={ICON_TYPE?.COOK_TIME} sx={{mr: '3px'}}/>
              <Skeleton variant="rectangular" width="54px" sx={{height: '14px'}}/>

              <SvgCommonIcons type={ICON_TYPE?.DOT} sx={{mx: '10px'}}/>

              <SvgCommonIcons type={ICON_TYPE?.DIFFICULTY} sx={{mr: '3px'}}/>
              <Skeleton variant="rectangular" width="39px" sx={{height: '14px'}}/>

              <SvgCommonIcons type={ICON_TYPE?.DOT} sx={{mx: '10px'}}/>

              <SvgCommonIcons type={ICON_TYPE?.BABY_FOOD} sx={{mr: '3px'}}/>
              <Skeleton variant="rectangular" width="79px" sx={{height: '14px'}}/>
            </Stack>

            <Skeleton variant="rectangular" width="320px" sx={{
              height: '154px',
              mt: '40px',
              pt: '25px',
              pb: '20px',
              px: '20px',
              backgroundColor: '#FAFAFA',
              borderRadius: '8px'
            }}/>
          </Container>

          <Container maxWidth={'xs'} disableGutters sx={{px: '20px'}}>
            <Stack
              direction="row"
              sx={{
                mt: '20px',
                mb: '10px',
              }}
            >
              <Skeleton variant="rectangular" width="38px" sx={{height: '14px', mr: '2px'}}/>
              <Skeleton variant="rectangular" width="16px" sx={{height: '14px', mr: '16px'}}/>
              <Skeleton variant="rectangular" width="38px" sx={{height: '14px', mr: '2px'}}/>
              <Skeleton variant="rectangular" width="16px" sx={{height: '14px', mr: '16px'}}/>
            </Stack>
          </Container>

          <Container maxWidth={'xs'} disableGutters sx={{px: '20px', pt: '20px', backgroundColor: 'white'}}>
            <Stack
              direction="row"
              spacing={'10px'}
            >
              {/*<StyledNotistackDetail />*/}

              <Skeleton variant="rounded" sx={{flexGrow: 1, height: '52px'}}/>
              <Skeleton variant="rounded" width="52px" sx={{height: '52px'}}/>
            </Stack>
            <Box sx={{height: '20px'}}/>
          </Container>

          <Box sx={{mb: '30px'}}/>

          <Container maxWidth={'xs'} disableGutters sx={{px: '0px',}}>
            <Box sx={{
              borderTop: '1px solid #E6E6E6',
              height: '50px',
              borderBottom: '1px solid #E6E6E6',
            }}>
              <Tabs sx={{
                '& .MuiTabs-indicator': {backgroundColor: 'primary.main'},
                '& .MuiTab-root': {color: 'primary.main'},
                '& .Mui-selected': {color: 'primary.main'},
                '& .MuiButtonBase-root.MuiTab-root.Mui-selected': {borderBottom: '1px solid primary.main'},
              }} variant="fullWidth" value={TABS[0]?.value}>
                {TABS?.slice(0, 3)?.map((tab, index) => (
                  <Tab
                    key={tab?.value}
                    value={tab?.value}
                    label={tab?.label}
                    sx={{
                      typography: 'b1_16_b_1l',
                      '&:not(:last-of-type)': {marginRight: '0px',},
                      '&:not(.Mui-selected)': {color: '#666666',},
                    }}
                  />
                ))}
              </Tabs>
            </Box>
          </Container>

          <Container maxWidth={'xs'} disableGutters sx={{px: '20px', pt: '101px', mt: '-51px'}}>
            <Box sx={{mb: '20px'}}>
              <Skeleton variant="rectangular" width="188px" sx={{height: '18px', mr: '16px'}}/>
            </Box>

            <Stack
              flexWrap="wrap"
              direction="row"
              sx={{
                alignItems: 'center',
                width: 'fit-content'
              }}
            >
              <Skeleton variant="rectangular" width="93px" sx={{height: '16px', mr: '4px'}}/>
              <Skeleton variant="rectangular" width="10px" sx={{height: '16px', mr: '4px'}}/>
              <IconButton sx={{
                p: 0,
                backgroundColor: "#FFFFFF",
                '&:hover': {
                  boxShadow: 'none',
                  backgroundColor: '#FFFFFF',
                },
              }}>
                <SvgCommonIcons type={ICON_TYPE?.ARROW_RIGHT_16PX}/>
              </IconButton>
            </Stack>

            <Stack
              overflow="auto"
              direction="row"
              justifyContent="flex-start"
              sx={{
                mt: '20px',
                pl: '20px',
                alignItems: 'center',
                "::-webkit-scrollbar": {display: 'none'},
              }}
            >
              {
                [...Array(5)]?.map((item, index) =>
                  (<Stack
                    direction="column"
                    alignItems="center"
                    key={index}
                    sx={{position: 'relative', mr: '10px', mb: '30px'}}
                  >
                    <Skeleton variant="circular" width="50px" sx={{height: '50px', mb: '2px'}}/>
                    <Skeleton variant="rectangular" width="50px" sx={{height: '14px'}}/>
                  </Stack>))}
            </Stack>

            <Stack
              flexWrap="wrap"
              direction="row"
              sx={{
                alignItems: 'center',
                width: 'fit-content'
              }}
            >
              <Skeleton variant="rectangular" width="126px" sx={{height: '16px', mr: '4px'}}/>
              <Skeleton variant="rectangular" width="10px" sx={{height: '16px', mr: '4px'}}/>
              <IconButton sx={{
                p: 0,
                backgroundColor: "#FFFFFF",
                '&:hover': {
                  boxShadow: 'none',
                  backgroundColor: '#FFFFFF',
                },
              }}>
                <SvgCommonIcons type={ICON_TYPE?.ARROW_RIGHT_16PX}/>
              </IconButton>
            </Stack>

            <Stack
              overflow="auto"
              direction="row"
              justifyContent="flex-start"
              sx={{
                mt: '20px',
                pl: '20px',
                alignItems: 'center',
                "::-webkit-scrollbar": {display: 'none'},
              }}
            >
              {
                [...Array(5)]?.map((item, index) =>
                  (<Stack
                    direction="column"
                    alignItems="center"
                    key={index}
                    sx={{position: 'relative', mr: '10px', mb: '30px'}}
                  >
                    <Skeleton variant="circular" width="50px" sx={{height: '50px', mb: '2px'}}/>
                    <Skeleton variant="rectangular" width="50px" sx={{height: '14px'}}/>
                  </Stack>))}
            </Stack>

            <Stack
              sx={{
                mb: '20px'
              }}
            />
          </Container>
          <Container maxWidth={'xs'} disableGutters sx={{px: '0px',}}>
            <Divider sx={{maxWidth: "xs"}}/>
          </Container>

          <Container maxWidth={'xs'} disableGutters sx={{px: '0px', pt: '101px', mt: '-51px'}}>
            <Container maxWidth={'xs'} disableGutters sx={{px: '20px',}}>
              <Skeleton variant="rectangular" width="221px" sx={{height: '28px'}}/>
            </Container>

            <Container maxWidth={'xs'} disableGutters sx={{px: '20px',}}>
              <Stack
                direction="row"
                sx={{
                  mt: '30px',
                  alignItems: 'center',
                }}
              >
                <Skeleton variant="rectangular" width="76px" sx={{height: '16px'}}/>
                <SvgCommonIcons type={ICON_TYPE?.ALERT_16PX} sx={{ml: '4px'}}/>
              </Stack>
            </Container>

            <Carousel {...carouselSettings}>
              {[...Array(3)]?.map((nameList, index) =>
                <Box key={index}>
                  <Stack spacing="32px" sx={{my: '20px', mx: '20px'}}>
                    <Stack
                      direction="row"
                      sx={{
                        alignItems: 'center',
                      }}
                    >
                      <Skeleton variant="rectangular" width="64px" sx={{height: '14px', mr: '20px'}}/>
                      <Skeleton variant="rounded" sx={{flexGrow: 0.8, height: '10px', borderRadius: '99px',}}/>
                      <Skeleton variant="rectangular" width="36px" sx={{height: '14px', ml: '20px'}}/>
                    </Stack>
                  </Stack>
                  <Stack spacing="32px" sx={{my: '20px', mx: '20px'}}>
                    <Stack
                      direction="row"
                      sx={{
                        alignItems: 'center',
                      }}
                    >
                      <Skeleton variant="rectangular" width="64px" sx={{height: '14px', mr: '20px'}}/>
                      <Skeleton variant="rounded" sx={{flexGrow: 0.8, height: '10px', borderRadius: '99px',}}/>
                      <Skeleton variant="rectangular" width="36px" sx={{height: '14px', ml: '20px'}}/>
                    </Stack>
                  </Stack>
                  <Stack spacing="32px" sx={{my: '20px', mx: '20px'}}>
                    <Stack
                      direction="row"
                      sx={{
                        alignItems: 'center',
                      }}
                    >
                      <Skeleton variant="rectangular" width="64px" sx={{height: '14px', mr: '20px'}}/>
                      <Skeleton variant="rounded" sx={{flexGrow: 0.8, height: '10px', borderRadius: '99px',}}/>
                      <Skeleton variant="rectangular" width="36px" sx={{height: '14px', ml: '20px'}}/>
                    </Stack>
                  </Stack>
                  <Stack spacing="32px" sx={{my: '20px', mx: '20px'}}>
                    <Stack
                      direction="row"
                      sx={{
                        alignItems: 'center',
                      }}
                    >
                      <Skeleton variant="rectangular" width="64px" sx={{height: '14px', mr: '20px'}}/>
                      <Skeleton variant="rounded" sx={{flexGrow: 0.8, height: '10px', borderRadius: '99px',}}/>
                      <Skeleton variant="rectangular" width="36px" sx={{height: '14px', ml: '20px'}}/>
                    </Stack>
                  </Stack>
                  <Stack spacing="32px" sx={{my: '20px', mx: '20px'}}>
                    <Stack
                      direction="row"
                      sx={{
                        alignItems: 'center',
                      }}
                    >
                      <Skeleton variant="rectangular" width="64px" sx={{height: '14px', mr: '20px'}}/>
                      <Skeleton variant="rounded" sx={{flexGrow: 0.8, height: '10px', borderRadius: '99px',}}/>
                      <Skeleton variant="rectangular" width="36px" sx={{height: '14px', ml: '20px'}}/>
                    </Stack>
                  </Stack>
                  {index !== 2 &&
                    <Stack spacing="32px" sx={{my: '20px', mx: '20px'}}>
                      <Stack
                        direction="row"
                        sx={{
                          alignItems: 'center',
                        }}
                      >
                        <Skeleton variant="rectangular" width="64px" sx={{height: '14px', mr: '20px'}}/>
                        <Skeleton variant="rounded" sx={{flexGrow: 0.8, height: '10px', borderRadius: '99px',}}/>
                        <Skeleton variant="rectangular" width="36px" sx={{height: '14px', ml: '20px'}}/>
                      </Stack>
                    </Stack>
                  }
                </Box>
              )}
            </Carousel>

            <Container maxWidth={'xs'} disableGutters sx={{px: '20px',}}>
              <Box sx={{mt: '40px', mb: '20px'}}>
                <Skeleton variant="rectangular" width="76px" sx={{height: '16px'}}/>
              </Box>
            </Container>

            <Stack spacing="32px" sx={{my: '20px', mx: '20px'}}>
              <Stack
                direction="row"
                sx={{
                  alignItems: 'center',
                }}
              >
                <Skeleton variant="rectangular" width="64px" sx={{height: '14px', mr: '20px'}}/>
                <Skeleton variant="rounded" sx={{flexGrow: 0.8, height: '10px', borderRadius: '99px',}}/>
                <Skeleton variant="rectangular" width="36px" sx={{height: '14px', ml: '20px'}}/>
              </Stack>
            </Stack>
            <Stack spacing="32px" sx={{my: '20px', mx: '20px'}}>
              <Stack
                direction="row"
                sx={{
                  alignItems: 'center',
                }}
              >
                <Skeleton variant="rectangular" width="64px" sx={{height: '14px', mr: '20px'}}/>
                <Skeleton variant="rounded" sx={{flexGrow: 0.8, height: '10px', borderRadius: '99px',}}/>
                <Skeleton variant="rectangular" width="36px" sx={{height: '14px', ml: '20px'}}/>
              </Stack>
            </Stack>
          </Container>

          <Container maxWidth={'xs'} disableGutters sx={{px: '20px',}}>
            <Divider sx={{maxWidth: "xs", mt: '50px'}}/>
            <Stack direction={'row'} sx={{justifyContent: 'space-between', my: '10px'}}>
              <Typography
                variant={'b2_14_r_1l'}
                sx={{
                  textDecoration: 'underline',
                  color: '#666666',
                }}
              >
                {VIEW_NUTRIENTS_SOURCE}
              </Typography>
              <Typography
                variant={'b2_14_r_1l'}
                sx={{
                  textDecoration: 'underline',
                  color: '#666666',
                }}
              >
                {VIEW_NUTRIENTS_PROPERTIES}
              </Typography>
            </Stack>
          </Container>

          <Divider sx={{maxWidth: "xs", px: '0px', mt: '50px'}}/>

          <Box>
            <Container maxWidth={'xs'} disableGutters sx={{px: '20px', pt: '100px', mt: '-50px'}} id="howToMake">
              <Box sx={{display: 'grid'}}>
                <Skeleton variant="rectangular" width="201px" sx={{height: '36px'}}/>

                <Box sx={{mt: '30px', mb: '20px'}}>
                  <Skeleton variant="rectangular" width="58px" sx={{height: '16px'}}/>
                </Box>

                <Grid container>
                  <Grid item xs={6.3} sx={{mb: '15px'}}>
                    <Skeleton variant="rectangular" width="30px" sx={{height: '16px'}}/>
                  </Grid>
                  <Grid item xs={0.7}/>
                  <Grid item xs={5}>
                    <Skeleton variant="rectangular" width="30px" sx={{height: '16px'}}/>
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={6.3} sx={{mb: '15px'}}>
                    <Skeleton variant="rectangular" width="30px" sx={{height: '16px'}}/>
                  </Grid>
                  <Grid item xs={0.7}/>
                  <Grid item xs={5}>
                    <Skeleton variant="rectangular" width="30px" sx={{height: '16px'}}/>
                  </Grid>
                </Grid>
              </Box>

              <Divider sx={{maxWidth: "md", mt: '30px'}}/>

              <Box sx={{mt: '30px', mb: '20px'}}>
                <Skeleton variant="rectangular" width="58px" sx={{height: '16px'}}/>
              </Box>

              <Grid container>
                <Grid item xs={6.3} sx={{mb: '15px'}}>
                  <Skeleton variant="rectangular" width="30px" sx={{height: '16px'}}/>
                </Grid>
                <Grid item xs={0.7}/>
                <Grid item xs={5}>
                  <Skeleton variant="rectangular" width="30px" sx={{height: '16px'}}/>
                </Grid>
              </Grid>
              <Grid container>
                <Grid item xs={6.3} sx={{mb: '15px'}}>
                  <Skeleton variant="rectangular" width="30px" sx={{height: '16px'}}/>
                </Grid>
                <Grid item xs={0.7}/>
                <Grid item xs={5}>
                  <Skeleton variant="rectangular" width="30px" sx={{height: '16px'}}/>
                </Grid>
              </Grid>

            </Container>
          </Box>

          <Divider sx={{maxWidth: "xs", px: '0px', mt: '50px', mb: '20px'}}/>

          <Box>
            <Container maxWidth={'xs'} disableGutters sx={{px: '0px', mt: '20px'}}>
              <Stack
                flexWrap="wrap"
                direction="column"
                justifyContent="center"
                sx={{
                  py: '30px',
                }}
              >
                <Container maxWidth={'md'} disableGutters sx={{px: '20px',}}>
                  <Skeleton variant="rectangular" width="58px" sx={{height: '16px'}}/>
                </Container>
                <Skeleton variant="rectangular" width="100%"
                          sx={{height: '270px', marginTop: '10px', marginBottom: '10px'}}/>
                <Container maxWidth={'md'} disableGutters sx={{px: '20px',}}>
                  <Skeleton variant="rectangular" width="100%" sx={{height: '32px'}}/>
                </Container>
              </Stack>

              <Stack
                flexWrap="wrap"
                direction="column"
                justifyContent="center"
                sx={{
                  py: '30px',
                }}
              >
                <Container maxWidth={'md'} disableGutters sx={{px: '20px',}}>
                  <Skeleton variant="rectangular" width="58px" sx={{height: '16px'}}/>
                </Container>
                <Skeleton variant="rectangular" width="100%"
                          sx={{height: '270px', marginTop: '10px', marginBottom: '10px'}}/>
                <Container maxWidth={'md'} disableGutters sx={{px: '20px',}}>
                  <Skeleton variant="rectangular" width="100%" sx={{height: '32px'}}/>
                </Container>
              </Stack>
            </Container>
          </Box>

          <Container maxWidth={'xs'} disableGutters sx={{pt: '20px', pb: '30px', px: '20px'}}>
            <Skeleton variant="rectangular" width="100%" sx={{height: '28px'}}/>
          </Container>

          <Divider sx={{maxWidth: "xs", px: '0px', mt: '20px'}}/>

          <Box>
            <Container maxWidth={'xs'} disableGutters sx={{px: '20px', pt: '100px', mt: '-50px'}}>
              <Box sx={{mb: '20px'}}>
                <Skeleton variant="rectangular" width="148px" sx={{height: '18px'}}/>
              </Box>

              <Stack
                direction="row"
                justifyContent="space-evenly"
              >
                {[...Array(4)]?.map((item, index) =>
                  <Stack
                    direction="column"
                    key={index}
                    sx={{
                      mx: '14.5px',
                      alignItems: 'center',
                    }}
                  >
                    <Skeleton variant="circular" width="30px" sx={{height: '30px'}}/>
                    <Skeleton variant="rectangular" width="51px" sx={{height: '14px', mt: '8px', mb: '4px'}}/>
                    <Skeleton variant="rectangular" width="51px" sx={{height: '30px'}}/>
                  </Stack>
                )}
              </Stack>

              <Container maxWidth={'xs'} disableGutters sx={{pb: '160px'}}>
              </Container>
            </Container>
          </Box>
        </Box>
      </Container>
    </>
  );
}

export async function getStaticPaths() {
  const data = await getAllRecipes()
  const paths = data?.filter((item, index) => index < 100).map((item) => ({
    params: {
      recipe_key: item?.recipe_key?.toString(),
      recipe_user_name: `@` + item?.recipe_user_name,
      recipe_name: getURIString(item?.recipe_name)
    },
  }));
  return {paths, fallback: 'blocking'}
}

export async function getStaticProps({params}) {
  let data
  try {
    data = await getRecipeDetailFetch({
      recipe_key: params?.recipe_key
    })
    if (data?.recipe_check_status !== '게시중') {
      logger?.error('## Recipe status is invalid')
      return {
        notFound: true,
      }
    }
  } catch (e) {
    console?.log("## Recipe not found")
    return {
      notFound: true,
    }
  }

  const queryClient = new QueryClient()
  queryClient?.setQueryData(['getRecipeDetail', params?.recipe_key], data)
  return {
    props: {
      dehydratedState: dehydrate(queryClient),
      recipe_key: params?.recipe_key,
    },
  }
}