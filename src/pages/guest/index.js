import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {Box, Container} from "@mui/material";
import GuestLayout from "../../layouts/home/GuestLayout";
import {getMainInfo, getSeasonIngredientBasedRecipeList} from "../../api/homeApi";
import useNativeBackKey from "../../hooks/useNativeBackKey";
import MainLayout from "../../layouts/main/MainLayout";
import {HOME_PERSONAL_COMMON} from "../../constant/home/Personalization";
import {logger} from "../../utils/loggingUtils";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import KeywordWave from "../../sections/home/personalization/KeywordWave";
import GuestDynamicSection from "../../sections/home/dynamicIsland/GuestDynamicSection";
import CardListPosts from "../../sections/home/personalization/CardListPosts";
import CardSwipeDonots from "../../sections/home/personalization/CardSwipeDonots";
import {useRecoilState, useResetRecoilState, useSetRecoilState} from "recoil";
import {dialogShowDialogSelector} from "../../recoil/selector/common/dialogSelector";
import CardSwipePosts from "../../sections/home/personalization/CardSwipePosts";
import {GUEST_CUSTOM_BASED, GUEST_USER_BODY, HOME_GUEST_COMMON} from "../../constant/home/Guest";
import InfoBanner from "../../components/home/banner/InfoBanner";
import {useRouter} from "next/router";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import {TOAST_TYPE} from "../../constant/common/Common";
import {accessTokenSelector, refreshTokenSelector} from "../../recoil/selector/auth/accessTokenSelector";
import {useBefuAuthContext} from "../../auth/useAuthContext";
import {toastMessageSelector} from "../../recoil/selector/common/toastSelector";
import {isNative} from "../../utils/envUtils";
import {callOpenNativeFirstPermission, PERMISSION_JSON} from "../../channels/permissionChannel";
import {callOpenNativeGetSnsProviderChannel} from "../../channels/commonChannel";
import {parseProvider} from "../../constant/my/Setting";
import {MAIN} from "../../constant/sign-up/SignUp";
import Footer from "../../layouts/common/Footer";
import {settingShowPersonalTermsSelector, settingShowServiceTermsSelector} from "../../recoil/selector/my/settingSelector";
import PermissionDialog from "../../components/auth/PermissionDialog";
import LoadingScreen from "../../components/common/LoadingScreen";

GuestPage.getLayout = (page) => <GuestLayout>{page}</GuestLayout>;

export default function GuestPage() {
  const mainRef = useRef(null)

  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)
  const [openPermissionDlg, setOpenPermissionDlg] = useState(false)
  const [showServiceTerms, setShowServiceTerms] = useRecoilState(settingShowServiceTermsSelector)
  const [showPersonalTerms, setShowPersonalTerms] = useRecoilState(settingShowPersonalTermsSelector)

  const resetAccessToken = useResetRecoilState(accessTokenSelector);
  const resetRefreshToken = useResetRecoilState(refreshTokenSelector);
  const setToastMessage = useSetRecoilState(toastMessageSelector)

  const {logout} = useBefuAuthContext()
  const {handleBackKey} = useNativeBackKey()
  const {isInitialized} = useBefuAuthContext();

  useEffect(() => {
    if (handleBackKey) {
      if (showDialog) {
        setShowDialog(false)
      } else if (openPermissionDlg) {
        setOpenPermissionDlg(false)
      } else if (showServiceTerms) {
        setShowServiceTerms(false)
      } else if (showPersonalTerms) {
        setShowPersonalTerms(false)
      } else {
        try {
          window.flutter_inappwebview.callHandler('openNativeAppTerminate')
            .then(async function (result) {
            })
        } catch (e) {
          logger.error(e.message)
        }
      }
    }
  }, [handleBackKey])

  useEffect(() => {
    if (isNative()) {
      showPermission()
      recentlySnsLoginHistory()
    }
    setHackleTrack(HACKLE_TRACK.INDEX_PUBLIC)

    return () => {
      handleBack()
    }
  }, [])

  const showPermission = () => {
    callOpenNativeFirstPermission(() => setOpenPermissionDlg(true))
  }

  const recentlySnsLoginHistory = () => {
    callOpenNativeGetSnsProviderChannel((result) => {
      if (result !== '' || result !== undefined) {
        const provider = parseProvider(result?.toUpperCase())
        setToastMessage({
          type: TOAST_TYPE.BOTTOM_DIALOG_HEADER,
          message: MAIN.RESENT_MESSAGE(provider),
        })
      }
    })
  }

  const handleBack = () => {
    setShowServiceTerms(false)
    setShowPersonalTerms(false)
  }

  if (!isInitialized) {
    return <LoadingScreen/>
  }

  return (
    <>
      <MainLayout mainRef={mainRef}>
        <Container disableGutters sx={{pt: '20px', px: '20px', pb: '64px'}}>

          {/* 둘러보기 */}
          <GuestSection mainRef={mainRef}/>

          {/* 퍼미션 팝업 */}
          {openPermissionDlg &&
            <PermissionDialog open={openPermissionDlg} setOpen={setOpenPermissionDlg} permissionJson={PERMISSION_JSON}/>}

        </Container>
      </MainLayout>
    </>
  );
}

function GuestSection({mainRef}) {
  const [iframe, setIframe] = useState(null)

  const router = useRouter()
  const {data: season} = getSeasonIngredientBasedRecipeList()
  const {data: main_info} = getMainInfo()

  return (
    <>

      {/* 로그인 타이틀 메시지 */}
      <Box
        sx={{
          ml: '10px',
          fontSize: '18px',
          fontWeight: 500,
          lineHeight: '26x',
          color: '#222222',
          whiteSpace: 'pre-wrap',
          cursor: 'pointer'
        }}
        onClick={() => router?.push(PATH_DONOTS_PAGE.AUTH.LOGIN)}
      >
        <Box sx={{display: 'inline', textDecoration: 'underline'}}>
          {HOME_GUEST_COMMON.SERVICE_MESSAGE.LOGIN}
        </Box>
        {HOME_GUEST_COMMON.SERVICE_MESSAGE.TITLE}
      </Box>

      {/* 다이나믹 아일랜드 */}
      <GuestDynamicSection mainRef={mainRef} sx={{mt: '20px'}}/>

      {/* 성장 도움 레시피 */}
      <CardSwipePosts
        mainRef={mainRef}
        posts={GUEST_USER_BODY}
        title={HOME_PERSONAL_COMMON.USER.TITLE}
        sx={{mt: '40px'}}
        tooltip={{
          contents: HOME_GUEST_COMMON.USER.TOOLTIP,
          arrow: 'guest-user'
        }}
        onClick={(url) => setIframe(url)}
      />

      {/* 정보성 배너 */}
      <InfoBanner
        tabIndex={7}
        showBanner
        guest
        items={main_info?.banner_info_list}
        sx={{mt: '30px'}}
      />

      {/* 우리 아이 맞춤 레시피 */}
      <CardListPosts
        tabIndex={8}
        mainRef={mainRef}
        title={HOME_PERSONAL_COMMON.CUSTOM.TITLE}
        posts={GUEST_CUSTOM_BASED}
        sx={{mt: '60px'}}
        tooltip={{
          contents: HOME_GUEST_COMMON.CUSTOM.TOOLTIP,
          arrow: 'guest-custom'
        }}
        onClick={(url) => setIframe(url)}
      />

      {/* 키워드 */}
      <KeywordWave
        keywords={HOME_PERSONAL_COMMON.WEEKLY.KEYWORDS}
        onClick={() => setHackleTrack(HACKLE_TRACK.INDEX_LOGOUT_KEYWORD)}
        sx={{pt: '40px'}}
      />

      {/* Hot 도낫츠 */}
      <CardSwipeDonots
        tabIndex={9}
        items={main_info?.banner_spring_list}
        onClick={() => setHackleTrack(HACKLE_TRACK.INDEX_LOGOUT_BN_HOT)}
        sx={{mt: '60px'}}
      />

      {/* 재철 재료 레시피 */}
      <CardListPosts
        tabIndex={11}
        title={HOME_PERSONAL_COMMON.SEASON.TITLE(main_info?.season_ingredient)}
        posts={season}
        onClick={() => setHackleTrack(HACKLE_TRACK.INDEX_LOGOUT_SEASON)}
        sx={{mt: '60px'}}
      />

      {/* 이용약관, 개인정보처리방침 */}
      <Footer/>

    </>
  )
}