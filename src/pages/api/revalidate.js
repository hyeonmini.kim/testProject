export default async function handler(req, res) {
  const {method, query, body} = req;

  if (method !== 'POST') {
    return res
      .status(400)
      .json({error: 'Invalid HTTP method. Only Post method is allowed'});
  }

  // Token 체크 ( 추후 상세화 )
  if (query.secret !== process.env.SECRET_REVALIDATE_TOKEN) {
    console.log('secret code: ', query.secret);
    console.log(
      'SECRET_REVALIDATE_TOKEN: ',
      process.env.SECRET_REVALIDATE_TOKEN,
    );
    return res.status(401).json({message: 'Invalid token'});
  }

  try {
    // body 값에 id를 넣어 특정 화면을 reGenerate 할 때 사용
    // if(!body){
    //   res.status(400).send("Bad Request ( No Body )");
    //   return
    // }

    // const idToRevalidate = body.id
    // if(idToRevalidate){
    // await res.revalidate(`/isr/${idToRevalidate}`)
    return res.json({revalidated: true});
    // }
  } catch (err) {
    return res.status(500).send(err.toString());
  }
}
