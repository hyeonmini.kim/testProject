import {Container} from '@mui/material';
import HeaderTabLayout from "../../layouts/momstar/HeaderTabLayout";
import MomStarLayout from "../../layouts/momstar/MomStarLayout";
import TodayMomStars from "../../sections/momstar/TodayMomStars";
import {momStarActiveTabSelector, momStarIsScrollSelector,} from "../../recoil/selector/momstar/momstarSelector";
import {useRecoilState, useResetRecoilState} from "recoil";
import {MOMSTAR_COMMON} from "../../constant/momstar/Momstar";
import TodayExperts from "../../sections/momstar/TodayExperts";
import * as React from "react";
import {useEffect, useRef, useState} from "react";
import useStackNavigation from "../../hooks/useStackNavigation";
import {STACK} from "../../constant/common/StackNavigation";
import useNativeBackKey from "../../hooks/useNativeBackKey";
import {getExpertList, getSprinkleList} from "../../api/rankingApi";
import {RANKING_PAGE_PARAMS} from "../../constant/common/Pagination";
import MainLayout from "../../layouts/main/MainLayout";
import {dialogShowDialogSelector} from "../../recoil/selector/common/dialogSelector";
import {setHackleTrack} from "../../utils/hackleUtils";
import {HACKLE_TRACK} from "../../constant/common/Hackle";
import {PATH_DONOTS_PAGE} from "../../routes/paths";
import {useRouter} from "next/router";
import {useBefuAuthContext} from "../../auth/useAuthContext";

MomStarPage.getLayout = (page) => <MomStarLayout>{page}</MomStarLayout>;

export default function MomStarPage() {
  const {navigation, preFetch} = useStackNavigation()
  const [activeTab, setActiveTab] = useRecoilState(momStarActiveTabSelector)
  const resetActiveTab = useResetRecoilState(momStarActiveTabSelector)

  const {data: dataSprinkle} = getSprinkleList(RANKING_PAGE_PARAMS)
  const {data: dataExpert} = getExpertList(RANKING_PAGE_PARAMS)

  const [itemSprinkle, setItemSprinkle] = useState([])
  const [itemExpert, setItemExpert] = useState([])

  const mainRef = useRef(null)
  const [isScroll, setIsScroll] = useRecoilState(momStarIsScrollSelector)
  const [showDialog, setShowDialog] = useRecoilState(dialogShowDialogSelector)

  const {asPath, push} = useRouter();
  const {isAuthenticated} = useBefuAuthContext();

  useEffect(() => {
    if (!itemSprinkle?.length && dataSprinkle) {
      setItemSprinkle(dataSprinkle)
    }
  }, [dataSprinkle]);

  useEffect(() => {
    if (!itemExpert?.length && dataExpert) {
      setItemExpert(dataExpert)
    }
  }, [dataExpert]);

  useEffect(() => {
    const fetch = preFetch(STACK.MOMSTAR.TYPE)
    if (fetch) {
      setActiveTab(fetch?.activeTab)
      setIsScroll(fetch?.isScroll)
      if (fetch?.activeTab === MOMSTAR_COMMON.MOMSTAR.TAB) {
        setItemSprinkle(fetch?.itemSprinkle)
      } else {
        setItemExpert(fetch?.itemExpert)
      }
      setTimeout(() => {
        mainRef?.current?.scrollTo(0, fetch?.scrollY)
      }, 100)
    }

    setHackleTrack(HACKLE_TRACK.RANKING)

    return () => {
      resetActiveTab()
      handleInit()
    }
  }, [])


  const handleInit = () => {
    mainRef?.current?.scrollTo(0, 0)
    setIsScroll(false)
  }

  const handleTapClick = (tab) => {
    if (activeTab === tab) return
    setItemSprinkle(dataSprinkle)
    setItemExpert(dataExpert)
    setActiveTab(tab)
    handleInit()
  };

  const {handleBackKey} = useNativeBackKey()
  useEffect(() => {
    if (handleBackKey) {
      if(showDialog) {
        setShowDialog(false)
      } else {
        handleBackButton()
      }
    }
  }, [handleBackKey]);

  const handleBackButton = () => {
    if (!isAuthenticated && asPath?.includes('d_inflow')) {
      push(PATH_DONOTS_PAGE?.GUEST);
    } else if (isAuthenticated && asPath?.includes('d_inflow')) {
      push(PATH_DONOTS_PAGE?.HOME);
    } else {
      navigation?.back();
    }
  }

  return (
    <MainLayout mainRef={mainRef}>
      <HeaderTabLayout handleTapClick={handleTapClick}>
        <Container disableGutters maxWidth={'xs'} sx={{mb: '70px', px: '20px'}}>
          {activeTab === MOMSTAR_COMMON.MOMSTAR.TAB && <TodayMomStars mainRef={mainRef} fetchItems={itemSprinkle}/>}
          {activeTab === MOMSTAR_COMMON.EXPERT.TAB && <TodayExperts mainRef={mainRef} fetchItems={itemExpert}/>}
        </Container>
      </HeaderTabLayout>
    </MainLayout>
  );
}