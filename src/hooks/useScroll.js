import {useEffect, useState} from "react";

export default function useScroll(handleScroll, unHandleScroll) {
  const [target, setTarget] = useState(null)

  useEffect(() => {
    let observer
    if (target) {
      observer = new IntersectionObserver(onIntersect, {
        rootMargin: '-65px 0px 0px 0px'
      })
      observer.observe(target)
    }
    return () => observer && observer.disconnect();
  }, [target])

  const onIntersect = ([entry], observer) => {
    if (entry.isIntersecting) {
      handleScroll()
    } else {
      unHandleScroll()
    }
  }

  return {
    setTarget,
  };
}