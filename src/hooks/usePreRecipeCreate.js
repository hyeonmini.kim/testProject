import {useRecoilValue, useSetRecoilState} from "recoil";
import {dialogSelector} from "../recoil/selector/common/dialogSelector";
import {createTemporaryRecipeSelector} from "../recoil/selector/recipe/createSelector";
import {useRouter} from "next/router";
import {PATH_DONOTS_PAGE} from "../routes/paths";
import {RECIPE_STATUS} from "../constant/recipe/Recipe";
import {isAuthenticated} from "../recoil/atom/auth/isAuthenticated";
import {deleteTempRecipeKeyMutate, getTempRecipeKeyMutate} from "../api/recipeApi";
import {getRecipeDetailMutate} from "../api/detailApi";
import {COMMON_DIALOG_TYPE} from "../constant/common/Common";
import {RECIPE_CREATE_COMMON} from "../constant/recipe/Create";
import {GUEST_LOGIN_POPUP_TEXT} from "../constant/home/Gnb";

export default function usePreRecipeCreate() {
  const isAuth = useRecoilValue(isAuthenticated)
  const setDialogMessage = useSetRecoilState(dialogSelector)
  const setTempRecipe = useSetRecoilState(createTemporaryRecipeSelector)
  const router = useRouter()

  const {mutate: mutateGetTempRecipeKey} = getTempRecipeKeyMutate()
  const {mutate: mutateGetTempRecipeDetail} = getRecipeDetailMutate()
  const {mutate: mutateDeleteTempRecipeKey} = deleteTempRecipeKeyMutate()

  const handleNew = () => {
    router.push(PATH_DONOTS_PAGE.RECIPE.CREATE);
  }

  const handleDeleteTempAndNew = (tempRecipeKey) => {
    mutateDeleteTempRecipeKey(tempRecipeKey, {
      onSettled: () => router.push(PATH_DONOTS_PAGE.RECIPE.CREATE)
    })
  }

  const handleContinue = (tempRecipeKey) => {
    mutateGetTempRecipeDetail({
      recipe_key: tempRecipeKey
    }, {
      onSuccess: (data) => {
        setTempRecipe({
          status: RECIPE_STATUS.TEMPORARY,
          ...data
        })
        router.push(PATH_DONOTS_PAGE.RECIPE.CREATE);
      }
    })
  }

  const handleRecipe = () => {
    if (isAuth) {
      mutateGetTempRecipeKey(null, {
        onSuccess: (tempRecipeKey) => {
          if (tempRecipeKey) {
            setDialogMessage({
              type: COMMON_DIALOG_TYPE.TWO_BUTTON,
              message: RECIPE_CREATE_COMMON.TEXT.CONTINUE_CONFIRM,
              leftButtonProps: {
                text: RECIPE_CREATE_COMMON.BUTTON.NEW,
                onClick: () => handleDeleteTempAndNew(tempRecipeKey)
              },
              rightButtonProps: {
                text: RECIPE_CREATE_COMMON.BUTTON.CONTINUE,
                onClick: () => handleContinue(tempRecipeKey)
              },
            })
          } else {
            handleNew()
          }
        },
        onError: (error) => {
          handleNew()
        }
      })
    } else {
      setDialogMessage({
        type: COMMON_DIALOG_TYPE?.TWO_BUTTON,
        message: GUEST_LOGIN_POPUP_TEXT?.TITLE,
        leftButtonProps: {text: GUEST_LOGIN_POPUP_TEXT?.BUTTON_TEXT?.CLOSE},
        rightButtonProps: {
          text: GUEST_LOGIN_POPUP_TEXT?.BUTTON_TEXT?.GO_LOGIN, onClick: () => {
            router.push(PATH_DONOTS_PAGE.AUTH.LOGIN);
          }
        },
      })
    }
  }
  return {handleRecipe};
}