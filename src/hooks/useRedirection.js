import {useRouter} from "next/router";
import * as React from "react";
import {PATH_DONOTS_PAGE} from "../routes/paths";
import {getURIString} from "../utils/formatString";

export const REDIRECTION_TYPE = {
  D_IFRAME: 'd_iframe',
  D_RECIPE: 'd_recipe',
  D_HOME: 'd_home',
  D_RANKING: 'd_ranking',
  D_CREATE: 'd_create',
  D_SEARCH: 'd_search',
  D_MY: 'd_my',
  D_NOTICE: 'd_notice',
  D_FAQ: 'd_faq',
  D_OTHERS: 'd_others',
  D_EXPERT: 'd_expert',
  D_EVENT: 'd_event',
}

const REDIRECTION_LIST = [
  REDIRECTION_TYPE.D_IFRAME,
  REDIRECTION_TYPE.D_RECIPE,
  REDIRECTION_TYPE.D_HOME,
  REDIRECTION_TYPE.D_RANKING,
  REDIRECTION_TYPE.D_CREATE,
  REDIRECTION_TYPE.D_SEARCH,
  REDIRECTION_TYPE.D_MY,
  REDIRECTION_TYPE.D_NOTICE,
  REDIRECTION_TYPE.D_FAQ,
  REDIRECTION_TYPE.D_OTHERS,
  REDIRECTION_TYPE.D_EXPERT,
  REDIRECTION_TYPE.D_EVENT,
]

export const INFLOW_TYPE = {
  PUSH: 'push',
  POPUP: 'popup'
}

const ADD_INFLOW_PUSH = '?d_inflow=push'
const APPEND_INFLOW_PUSH = '&d_inflow=push'
const ADD_INFLOW_POPUP = '?d_inflow=popup'

export default function useRedirection() {
  const {query} = useRouter();
  let redirection = {
    type: null,
    url: null
  }

  const isRedirection = (inflow = INFLOW_TYPE.PUSH, url) => {
    const params = new URLSearchParams(url ? url : query);

    REDIRECTION_LIST.some((type) => {
      if (params?.has(type)) {
        redirection = {
          type: type,
          url: getUrl(type, params?.get(type), inflow)
        }
        return
      }
    })
    return redirection
  }

  const getUrl = (type, params, inflow) => {
    switch (type) {
      case REDIRECTION_TYPE.D_IFRAME:
        return params
      case REDIRECTION_TYPE.D_RECIPE:
        const paths = params?.split('/');
        if (paths?.length >= 3) {
          switch (inflow) {
            case INFLOW_TYPE.PUSH:
              return PATH_DONOTS_PAGE.RECIPE.DETAIL(paths[0]?.slice(1), getURIString(paths[1]), paths[2]) + ADD_INFLOW_PUSH
            default:
              return PATH_DONOTS_PAGE.RECIPE.DETAIL(paths[0]?.slice(1), getURIString(paths[1]), paths[2]) + ADD_INFLOW_POPUP
          }
        }
        return null
      case REDIRECTION_TYPE.D_HOME:
        return PATH_DONOTS_PAGE.HOME
      case REDIRECTION_TYPE.D_RANKING:
        return PATH_DONOTS_PAGE.RANKING + ADD_INFLOW_PUSH
      case REDIRECTION_TYPE.D_CREATE:
        switch (inflow) {
          case INFLOW_TYPE.PUSH:
            return PATH_DONOTS_PAGE.RECIPE.CREATE + ADD_INFLOW_PUSH
          default:
            return PATH_DONOTS_PAGE.RECIPE.CREATE
        }
      case REDIRECTION_TYPE.D_SEARCH:
        return PATH_DONOTS_PAGE.RECIPE.SEARCH + ADD_INFLOW_PUSH
      case REDIRECTION_TYPE.D_MY:
        return PATH_DONOTS_PAGE.MY.SELF + ADD_INFLOW_POPUP
      case REDIRECTION_TYPE.D_NOTICE:
        switch (inflow) {
          case INFLOW_TYPE.PUSH:
            return PATH_DONOTS_PAGE.MY.NOTICE(params) + APPEND_INFLOW_PUSH
          default:
            return PATH_DONOTS_PAGE.MY.NOTICE(params)
        }
      case REDIRECTION_TYPE.D_FAQ:
        switch (inflow) {
          case INFLOW_TYPE.PUSH:
            return PATH_DONOTS_PAGE.MY.FAQ(params) + APPEND_INFLOW_PUSH
          default:
            return PATH_DONOTS_PAGE.MY.FAQ(params)
        }
      case REDIRECTION_TYPE.D_OTHERS:
        switch (inflow) {
          case INFLOW_TYPE.PUSH:
            return PATH_DONOTS_PAGE.MY.OTHERS(params) + APPEND_INFLOW_PUSH
          default:
            return PATH_DONOTS_PAGE.MY.OTHERS(params)
        }
      case REDIRECTION_TYPE.D_EXPERT:
        switch (inflow) {
          case INFLOW_TYPE.PUSH:
            return PATH_DONOTS_PAGE.MY.EXPERTS(params) + APPEND_INFLOW_PUSH
          default:
            return PATH_DONOTS_PAGE.MY.EXPERTS(params)
        }
      case REDIRECTION_TYPE.D_EVENT:
      default:
        return null
    }
  }

  return {isRedirection}
}