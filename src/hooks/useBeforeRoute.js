import {useEffect} from "react";
import Router from "next/router";


/**
 * [ 사용법 ]
 * useBeforeRoute('/momstar/',()=>{...})
 *
 */
const useBeforeRoute = (to, handler) => {

  useEffect(() => {
    Router.events.on('routeChangeStart', (url) => {
      if (url === to) {
        if (handler()) {
          Router.events.emit('routeChangeError');
          throw 'routeChange aborted.';
        }
      }
    })

    return () => {
      Router.events.off('routeChangeStart', () => {
        console.log(' useBeforeRoute end')
      });
    };
  }, [])

};

export {useBeforeRoute};