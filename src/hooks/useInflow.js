import {useRouter} from "next/router";
import * as React from "react";
import {setHackleTrack} from "../utils/hackleUtils";
import {HACKLE_TRACK} from "../constant/common/Hackle";
import {setLocalStorage} from "../utils/storageUtils";
import {TIME} from "../constant/common/Time";

export const INFLOW_PAGE = {
  ROOT: 'root',
  RECIPE: 'recipe',
  JOIN: 'join',
}

export const INFLOW_TYPE = {
  UNKNOWN: 'unknown',
  GOOGLE: 'google',
  GOOGLE_AD: 'google_ad',
  NAVER: 'naver',
  NAVER_BLOG: 'naver_blog',
  NAVER_AD: 'naver_ad',
  DAUM: 'daum',
  DAUM_BLOG: 'daum_blog',
  NATE: 'nate',
  INSTAGRAM: 'instagram',
  INSTAGRAM_AD: 'instagram_ad',
  FACEBOOK: 'facebook',
  FACEBOOK_AD: 'facebook_ad',
  KAKAO: 'kakao_ad',
  TISTORY: 'tistory',
  AD: 'ad',
  SHARE: 'share',
  FRIEND: 'friend',
  PUSH: 'push',
}

const INFLOW_LIST = [
  INFLOW_TYPE.GOOGLE,
  INFLOW_TYPE.GOOGLE_AD,
  INFLOW_TYPE.NAVER,
  INFLOW_TYPE.NAVER_BLOG,
  INFLOW_TYPE.NAVER_AD,
  INFLOW_TYPE.DAUM,
  INFLOW_TYPE.DAUM_BLOG,
  INFLOW_TYPE.NATE,
  INFLOW_TYPE.INSTAGRAM,
  INFLOW_TYPE.INSTAGRAM_AD,
  INFLOW_TYPE.FACEBOOK,
  INFLOW_TYPE.FACEBOOK_AD,
  INFLOW_TYPE.KAKAO,
  INFLOW_TYPE.TISTORY,
  INFLOW_TYPE.SHARE,
  INFLOW_TYPE.FRIEND,
  INFLOW_TYPE.PUSH,
]

export default function useInflow() {
  const {query} = useRouter();

  const inflow = (page = INFLOW_PAGE.ROOT) => {
    let {d_inflow, d_share} = query
    if (d_share !== undefined) {
      setLocalStorage('inflow', INFLOW_TYPE.SHARE, TIME.MS_ONE_DAY)
      setHackleTrack(HACKLE_TRACK.INFLOW, {
        page: INFLOW_PAGE.RECIPE,
        inflow: INFLOW_TYPE.SHARE
      })
    } else if (d_inflow) {
      if (!INFLOW_LIST.includes(d_inflow)) {
        d_inflow = INFLOW_TYPE.UNKNOWN
      }

      if (d_inflow !== INFLOW_TYPE.PUSH) {
        setLocalStorage('inflow', d_inflow, TIME.MS_ONE_DAY)
      }

      setHackleTrack(HACKLE_TRACK.INFLOW, {
        page: page,
        inflow: d_inflow
      })
    }

    return !!d_inflow
  }

  return {inflow}
}