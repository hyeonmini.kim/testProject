import {useEffect, useState} from "react";

export default function useNativeBackKey() {
  const [handleBackKey, setHandleBackKey] = useState(false)

  useEffect(() => {
    callNativeBackkey = function () {
      setHandleBackKey(true)
    }
    return () => {
      setHandleBackKey(false)
    }
  })

  return {handleBackKey}
}