import Router from "next/router";

/**
 * [ 사용법 ]
 * useCheckGoBack(()=>{console.log('뒤로가기 진입')});
 *
 * @returns {string}
 */
const useCheckGoBack = (handler) => {

  Router.beforePopState(() => {

    handler()
    return true
  })

};

export {useCheckGoBack};