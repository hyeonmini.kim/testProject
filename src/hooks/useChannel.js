import {isNative} from "../utils/envUtils";
import {useRecoilState} from "recoil";
import {envSelector, isMockSelector, versionCurrentSelector, versionNewSelector} from "../recoil/selector/common/versionSelector";
import {ENV_TYPE} from "../constant/common/Env";
import {initEnvSelector} from "../recoil/selector/auth/initializeSelector";
import {logger} from "../utils/loggingUtils";

export default function useChannel() {
  const [env, setEnv] = useRecoilState(envSelector)
  const [isMock, setIsMock] = useRecoilState(isMockSelector)
  const [version, setVersion] = useRecoilState(versionCurrentSelector)
  const [newVersion, setNewVersion] = useRecoilState(versionNewSelector)

  const [isInitEnv, setIsInitEnv] = useRecoilState(initEnvSelector);

  const getNativeEnv = () => {
    return {
      version: version,
      newVersion: newVersion,
      env: env,
      isMock: isMock,
    }
  }

  const openNativeInit = async () => {
    if (!isNative()) {
      setIsInitEnv(true)
      return
    }

    try {
      await window.flutter_inappwebview.callHandler('openNativeAppVerGet')
        .then(async function (result) {
          setVersion(result?.current)
          setNewVersion(result?.newest)
          setNativeEnv(result?.current)
        })
    } catch (e) {
      logger.dump('openNativeAppVerGet', e.message)
    } finally {
      setIsInitEnv(true)
    }
  }

  const setNativeEnv = (version) => {
    // 버전정보가 없으면 프로덕션으로 셋팅 (사고 방지)
    if (!version || version?.length < 2) {
      setEnv(ENV_TYPE.PRODUCT)
      setIsMock(false)
      return
    }

    const env = version?.charAt(version?.length - 2)
    const mock = version?.charAt(version?.length - 1)

    if (env !== ENV_TYPE.PRODUCT) {
      setEnv(env)
      if (mock === ENV_TYPE.MOCK) {
        setIsMock(true)
      } else if (mock === ENV_TYPE.API) {
        setIsMock(false)
      }
    } else {
      setEnv(ENV_TYPE.PRODUCT)
      setIsMock(false)
    }
  }

  const openNativeAppUpdate = () => {
    if (!isNative()) return
    try {
      window.flutter_inappwebview.callHandler('openNativeAppUpdate')
        .then(async function (result) {
        })
    } catch (e) {
    }
  }

  return {getNativeEnv, openNativeInit, openNativeAppUpdate}
}