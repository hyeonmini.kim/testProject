import {useEffect, useRef, useState} from "react";
import {useRecoilValue} from "recoil";
import {useBefuAuthContext} from "../auth/useAuthContext";
import {forMyPageEntrySelector} from "../recoil/selector/auth/userSelector";

const useSticky = (csr, handlePushLoginPage, handleShowGradePopup) => {
  const tabStickyRef = useRef(null);
  const bottomStickyRef = useRef(null);
  const carouselRef = useRef(null);
  const loginRef = useRef(null);
  const imageRef = useRef(null);
  const mainRef = useRef(null);
  const [tabSticky, setTabSticky] = useState(false);
  const [isShowHeader, setIsShowHeader] = useState(false);
  const [isShowGoToTop, setIsShowGoToTop] = useState(false);
  const [bottomSticky, setBottomSticky] = useState(undefined);
  const [tabStickyChildren, setTabStickyChildren] = useState(false);
  const tabStickyOffsetRef = useRef(0);
  const bottomStickyOffsetRef = useRef(0);
  const loginOffsetRef = useRef(0);
  const isLoginOffsetRef = useRef(true);
  const lastScrollY = useRef(0)
  const {isAuthenticated, isInitialized} = useBefuAuthContext();
  const userInfo = useRecoilValue(forMyPageEntrySelector)
  const isAuthRef = useRef(undefined)
  const isGlazedRef = useRef(undefined)
  const isExpertRef = useRef(undefined)

  useEffect(() => {
    if (csr) {
      if (isAuthenticated && carouselRef?.current?.clientHeight > 10 && tabStickyRef?.current?.offsetTop > 10) {
        if (!((userInfo?.type === "NORMAL_MEMBER" && userInfo?.grade === "LV1") && csr?.recipe_writer_info?.recipe_writer_type === "EXPERT")) {
          isAuthRef.current = true
          isGlazedRef.current = false
          tabStickyOffsetRef.current = tabStickyRef?.current?.offsetTop + carouselRef?.current?.clientHeight + 44 - bottomStickyRef?.current?.clientHeight

          if (bottomSticky === undefined) {
            bottomStickyOffsetRef.current = bottomStickyRef?.current?.offsetTop + carouselRef?.current?.clientHeight - 39 + bottomStickyRef?.current?.clientHeight - window?.innerHeight
          }
          if (bottomStickyOffsetRef?.current < 0) {
            setBottomSticky(true)
            tabStickyOffsetRef.current = tabStickyRef?.current?.offsetTop + carouselRef?.current?.clientHeight - 34
          }
        } else {
          isAuthRef.current = true
          isGlazedRef.current = true
          loginOffsetRef.current = loginRef?.current?.offsetTop + carouselRef?.current?.clientHeight - imageRef?.current?.clientHeight / 2 - bottomStickyRef?.current?.clientHeight - window?.innerHeight
        }
      } else if (isAuthenticated === false) {
        if (csr?.recipe_writer_info?.recipe_writer_type === "EXPERT") {
          isAuthRef.current = false
          isExpertRef.current = true
          loginOffsetRef.current = loginRef?.current?.offsetTop + carouselRef?.current?.clientHeight - imageRef?.current?.clientHeight / 2 - bottomStickyRef?.current?.clientHeight - window?.innerHeight
        } else {
          isAuthRef.current = false
          isExpertRef.current = false
          tabStickyOffsetRef.current = tabStickyRef?.current?.offsetTop + carouselRef?.current?.clientHeight + 44 - bottomStickyRef?.current?.clientHeight

          if (bottomSticky === undefined) {
            bottomStickyOffsetRef.current = bottomStickyRef?.current?.offsetTop + carouselRef?.current?.clientHeight - 39 + bottomStickyRef?.current?.clientHeight - window?.innerHeight
          }
          if (bottomStickyOffsetRef?.current < 0) {
            setBottomSticky(true)
            tabStickyOffsetRef.current = tabStickyRef?.current?.offsetTop + carouselRef?.current?.clientHeight - 34
          }
        }
      }
    }
    // }
  }, [csr, isAuthenticated, carouselRef?.current, tabStickyRef?.current]);

  useEffect(() => {
    if (mainRef?.current) {
      mainRef?.current?.addEventListener("scroll", handleScroll, {passive: true});
    }
    return () => mainRef?.current?.removeEventListener("scroll", handleScroll);
  }, [mainRef?.current])


  const handleScroll = () => {
    if (!mainRef?.current) {
      return;
    }
    if (!tabStickyRef?.current) {
      return;
    }
    if (!bottomStickyRef?.current) {
      return;
    }
    if (!loginRef?.current) {
      return;
    }

    const shouldBeTabSticky = mainRef?.current?.scrollTop > tabStickyOffsetRef?.current;

    const shouldBeLoginPopup = mainRef?.current?.scrollTop > loginOffsetRef?.current && isLoginOffsetRef.current;

    const shouldBeBottomSticky = mainRef?.current?.scrollTop > bottomStickyOffsetRef?.current

    if ((isAuthRef?.current && !isGlazedRef?.current) || (!isAuthRef?.current && !isExpertRef.current)) setBottomSticky(shouldBeBottomSticky);
    if (((isAuthRef?.current && !isGlazedRef?.current) || (!isAuthRef?.current && !isExpertRef.current)) && shouldBeBottomSticky && mainRef?.current?.scrollTop !== 0) {
      setIsShowGoToTop(true);
    } else {
      setIsShowGoToTop(false);
    }

    const scrollY = mainRef?.current?.scrollTop;
    const direction = scrollY > lastScrollY?.current ? "Down" : "Up"

    lastScrollY.current = scrollY;

    if (((isAuthRef?.current && !isGlazedRef?.current) || (!isAuthRef?.current && !isExpertRef.current)) && direction === "Up" && shouldBeTabSticky) {
      setIsShowHeader(true);
    } else if (((isAuthRef?.current && !isGlazedRef?.current) || (!isAuthRef?.current && !isExpertRef.current)) && direction === "Down" || !shouldBeTabSticky) {
      setIsShowHeader(false);
    }
    if (isAuthRef?.current === false && isExpertRef.current && direction === "Down" && shouldBeLoginPopup) {
      setTimeout(() => {
        isLoginOffsetRef.current = false
        handlePushLoginPage()
      }, 100)
    }
    if (isAuthRef?.current && isGlazedRef?.current && direction === "Down" && shouldBeLoginPopup) {
      setTimeout(() => {
        isLoginOffsetRef.current = false
        handleShowGradePopup()
      }, 100)
    }

    if ((isAuthRef?.current && !isGlazedRef?.current) || (!isAuthRef?.current && !isExpertRef.current)) setTabSticky(shouldBeTabSticky);
    if ((isAuthRef?.current && !isGlazedRef?.current) || (!isAuthRef?.current && !isExpertRef.current)) setTabStickyChildren(shouldBeTabSticky);
  };

  return {
    isShowGoToTop,
    isShowHeader,
    carouselRef,
    bottomStickyRef,
    tabStickyRef,
    bottomSticky,
    tabStickyChildren,
    tabSticky,
    loginRef,
    imageRef,
    mainRef,
    lastScrollY,
    tabStickyOffsetRef
  };
};

export default useSticky;
