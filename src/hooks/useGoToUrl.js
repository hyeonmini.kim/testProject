import {URL_TYPE} from "../constant/common/UrlType";
import {PATH_DONOTS_PAGE} from "../routes/paths";
import {getURIString} from "../utils/formatString";
import {useRouter} from "next/router";
import usePreRecipeCreate from "./usePreRecipeCreate";
import * as React from "react";
import {useRecoilState} from "recoil";
import {iFrameUrlSelector} from "../recoil/selector/common/iFrameSelector";

export default function useGoToUrl() {
  const router = useRouter()
  const [iframeUrl, setIframeUrl] = useRecoilState(iFrameUrlSelector)
  const {handleRecipe} = usePreRecipeCreate()

  const parseUrlQuery = (url) => {
    const d_iframe = url?.includes(URL_TYPE.D_IFRAME)
    const d_recipe = url?.includes(URL_TYPE.D_RECIPE)
    const d_home = url?.includes(URL_TYPE.D_HOME)
    const d_ranking = url?.includes(URL_TYPE.D_RANKING)
    const d_create = url?.includes(URL_TYPE.D_CREATE)
    const d_search = url?.includes(URL_TYPE.D_SEARCH)
    const d_my = url?.includes(URL_TYPE.D_MY)
    const d_notice = url?.includes(URL_TYPE.D_NOTICE)
    const d_faq = url?.includes(URL_TYPE.D_FAQ)
    const d_others = url?.includes(URL_TYPE.D_OTHERS)
    const d_expert = url?.includes(URL_TYPE.D_EXPERT)
    const d_event = url?.includes(URL_TYPE.D_EVENT)

    if (d_iframe) {
      const params = new URLSearchParams(url);
      const d_iframe = params?.get(URL_TYPE.D_IFRAME);
      setIframeUrl(d_iframe)
    } else if (d_recipe) {
      const params = new URLSearchParams(url);
      const d_recipe = params?.get(URL_TYPE.D_RECIPE);
      const str = d_recipe?.split("/");
      let recipe_user_name = '';
      if (str?.length > 0)
        recipe_user_name = str[0]?.replace("@", '')
      router?.push(PATH_DONOTS_PAGE.RECIPE.DETAIL(recipe_user_name, getURIString(str[1]), str[2]))
    } else if (d_home) {
      router?.push(PATH_DONOTS_PAGE.HOME)
    } else if (d_ranking) {
      router?.push(PATH_DONOTS_PAGE.RANKING)
    } else if (d_create) {
      handleRecipe()
    } else if (d_search) {
      router?.push(PATH_DONOTS_PAGE.RECIPE.SEARCH)
    } else if (d_my) {
      router?.push(PATH_DONOTS_PAGE.MY.SELF)
    } else if (d_notice) {
      const params = new URLSearchParams(url);
      const d_notice = params?.get(URL_TYPE.D_NOTICE);
      router?.push(PATH_DONOTS_PAGE.MY.NOTICE(d_notice))
    } else if (d_faq) {
      const params = new URLSearchParams(url);
      const d_faq = params?.get(URL_TYPE.D_FAQ);
      router?.push(PATH_DONOTS_PAGE.MY.FAQ(d_faq))
    } else if (d_others) {
      const params = new URLSearchParams(url);
      const d_others = params?.get(URL_TYPE.D_OTHERS);
      router?.push(PATH_DONOTS_PAGE.MY.OTHERS(d_others))
    } else if (d_expert) {
      const params = new URLSearchParams(url);
      const d_expert = params?.get(URL_TYPE.D_EXPERT);
      router?.push(PATH_DONOTS_PAGE.MY.EXPERTS(d_expert))
    } else if (d_event) {
      const params = new URLSearchParams(url);
      const d_event = params?.get(URL_TYPE.D_EVENT);
    }
  }

  return {parseUrlQuery}
}