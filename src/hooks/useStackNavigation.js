import {useRecoilState, useResetRecoilState} from "recoil";
import {useRouter} from "next/router";
import {stackNavigationSelector, stackPreFetchingSelector} from "../recoil/selector/common/stackNavigationSelector";
import {STACK, STACK_PRE_FETCHING} from "../constant/common/StackNavigation";
import {logger} from "../utils/loggingUtils";

export default function useStackNavigation() {
  const router = useRouter()
  const [stack, setStack] = useRecoilState(stackNavigationSelector)
  const [fetch, setFetch] = useRecoilState(stackPreFetchingSelector)
  const resetStack = useResetRecoilState(stackNavigationSelector)
  const resetFetch = useResetRecoilState(stackPreFetchingSelector)

  const stackPush = (data) => {
    const newStack = [...stack]
    newStack.push(data)
    newStack[stack.length] = data
    setStack(newStack)
    // logger.info('## Stack Push: ', newStack)
  }

  const stackPop = () => {
    const newStack = [...stack]
    if (stack.length > 0) {
      const pop = newStack.splice(stack.length - 1, 1)
      setStack(newStack)
      // logger.info('## Stack Pop: ', pop[0])
      // logger.info('## Stack Refresh: ', newStack)
      return pop[0]
    }
    return
  }

  const handlePush = (from, to, data) => {
    const preFetchingData = {
      ...STACK_PRE_FETCHING,
      from: from,
      path: router.asPath,
      data: data,
    }
    stackPush(preFetchingData)
    router.push(to)
  }

  const handleBack = (callback) => {
    const pop = stackPop()
    if (pop) {
      if (callback) {
        setFetch({
          ...pop,
          data: {
            ...pop?.data,
            ...callback,
          }
        })
      } else {
        setFetch(pop)
      }
    }
    router.back()
  }

  const handleClearBack = (from) => {
    const pop = stackPop()
    if (!from || from !== pop?.from) {
      setFetch(pop)
    }
    router.back()
  }

  const preFetch = (type) => {
    const preFetch = {...fetch}
    resetFetch()
    if (!preFetch?.from) return
    else if (preFetch?.from === type) {
      return preFetch.data
    } else if (preFetch?.from !== STACK.NONE.TYPE) {
      logger.error("## Stack Navigation Pre Fetch Miss: " + preFetch?.from + " -> " + type)
    }
  }

  const navigation = {
    push: handlePush,
    back: handleBack,
    clearBack: handleClearBack,
  }

  const clearStack = () => {
    resetStack()
    resetFetch()
  }

  return {navigation, preFetch, clearStack};
}
