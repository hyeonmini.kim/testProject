import {useEffect, useRef, useState} from "react";
import {PAGINATION} from "../constant/common/Pagination";

export default function usePagination(apiMutate, pageSize = PAGINATION.PAGE_SIZE) {
  const [items, setItems] = useState(PAGINATION.INIT_DATA)
  const [target, setTarget] = useState(null)
  const lastKey = useRef('')
  const pageOffset = useRef(0)
  const hasNextOffset = useRef(true)
  const param = useRef({})
  const {mutateAsync, isLoading} = apiMutate()

  useEffect(() => {
    let observer
    if (target) {
      observer = new IntersectionObserver(onIntersect, {
        threshold: 0.3
      })
      observer.observe(target)
    }
    return () => observer && observer.disconnect();
  }, [target])

  const onIntersect = async ([entry], observer) => {
    if (entry.isIntersecting) {
      observer.unobserve(entry.target)
      try {
        await mutateAsync(getVariables(), getOptions())
      } catch (e) {

      }
      observer.observe(entry.target)
    }
  }

  const handleResponse = (data, isInit) => {
    hasNextOffset.current = false
    if (!data) {
      return
    }

    if (isInit) {
      if (data?.length) {
        setItems(data)
        pageOffset.current = data?.length
      } else {
        setItems(PAGINATION.NO_DATA)
      }
    } else {
      if (data?.length) {
        setItems((prevState) => prevState?.concat(data))
        pageOffset.current += data?.length
      }
    }
    if (data?.length) {
      const lastItem = data[data?.length - 1]
      lastKey.current = lastItem?.key
    }
    hasNextOffset.current = getHasNextOffset(data)
  }

  const handleError = (error) => {
    hasNextOffset.current = false
  }

  const getVariables = () => {
    return {
      ...param.current,
      params: {
        key: lastKey.current,
        page: pageOffset.current,
        size: pageSize,
        ...param.current?.params,
      }
    }
  }

  const getOptions = (isInit = false) => {
    return {
      onSuccess: (data) => {
        if (data) {
          handleResponse(data, isInit)
        }
      },
      onError: (error) => {
        handleError(error)
      }
    }
  }

  const getHasNextOffset = (items) => {
    if (!items?.length) return false
    if (parseInt(items.length / pageSize) > 0 && items.length % pageSize === 0) {
      return true
    }
    return false
  }

  const getPaginationItems = async () => {
    initPagination()
    try {
      await mutateAsync(getVariables(), getOptions(true))
    } catch (e) {
    }
  }

  const initPagination = (data = PAGINATION.INIT_DATA) => {
    setItems(data)
    if (data === PAGINATION.INIT_DATA) {
      lastKey.current = ''
      pageOffset.current = 0
      hasNextOffset.current = false
    } else {
      const lastItem = data[data?.length - 1]
      lastKey.current = lastItem?.key
      pageOffset.current = data?.length
      hasNextOffset.current = getHasNextOffset(data)
    }
  }

  return {
    param,
    items,
    setItems,
    setTarget,
    pageOffset,
    hasNextOffset,
    mutateAsync,
    initPagination,
    isLoading,
    getPaginationItems
  };
}