const withTM = require('next-transpile-modules')([
  '@fullcalendar/common',
  '@fullcalendar/daygrid',
  '@fullcalendar/interaction',
  '@fullcalendar/list',
  '@fullcalendar/react',
  '@fullcalendar/timegrid',
  '@fullcalendar/timeline',
]);

// const withBundleAnalyzer = require('@next/bundle-analyzer')({
//   enabled: process.env.ANALYZE === 'true',
// })
//
// module.exports = withBundleAnalyzer({
//   compress: true,
//   env: {
//     ENV: process.env.ENV,
//     IS_MOCK: false,
//     RECIPE_LOC_API_URL: 'https://dev-recipe.kyobo-donots.com',
//     RECIPE_DEV_API_URL: 'https://dev-recipe.kyobo-donots.com',
//     RECIPE_STG_API_URL: 'https://stg-recipe.donots.co.kr',
//     RECIPE_PRD_API_URL: 'https://recipe.donots.co.kr',
//     MEMBER_LOC_API_URL: 'https://dev-member.kyobo-donots.com',
//     MEMBER_DEV_API_URL: 'https://dev-member.kyobo-donots.com',
//     MEMBER_STG_API_URL: 'https://stg-member.donots.co.kr',
//     MEMBER_PRD_API_URL: 'https://member.donots.co.kr',
//   },
//   webpack(config, {webpack}) {
//     const prod = true
//     const plugins = [...config.plugins];
//     return{
//       ...config,
//       mode: prod ? 'production' : 'development',
//       devtool: prod ? 'hidden-source-map' : 'eval',
//       plugins,
//     };
//   },
// })

module.exports = withTM({
  i18n: {
    defaultLocale: 'ko',
    locales: ['ko']
  },
  swcMinify: false,
  trailingSlash: true,
  output: 'standalone',
  env: {
    ENV: process.env.ENV,
    IS_MOCK: false,
    VERSION: '1.3.1.02',
    RECIPE_LOC_API_URL: 'https://dev-recipe.donots.co.kr',
    RECIPE_DEV_API_URL: 'https://dev-recipe.donots.co.kr',
    RECIPE_STG_API_URL: 'https://stg-recipe.donots.co.kr',
    RECIPE_PRD_API_URL: 'https://recipe.donots.co.kr',
    MEMBER_LOC_API_URL: 'https://dev-member.donots.co.kr',
    MEMBER_DEV_API_URL: 'https://dev-member.donots.co.kr',
    MEMBER_STG_API_URL: 'https://stg-member.donots.co.kr',
    MEMBER_PRD_API_URL: 'https://member.donots.co.kr',
    STATIC_LOC_CF_URL: 'https://d1jqpeehmfugex.cloudfront.net',
    STATIC_DEV_CF_URL: 'https://d1jqpeehmfugex.cloudfront.net',
    STATIC_STG_CF_URL: 'https://d1jqpeehmfugex.cloudfront.net',
    STATIC_PRD_CF_URL: 'https://d1f1anino86jvg.cloudfront.net',
    KEY_DEV_HACKLE: 'G0JqaknW6C2WB9Fb2eHRuB0A3wJnRZLF',
    KEY_PRD_HACKLE: 'Vm8InCe0cMhgL8vt9UYuew5fctB8dluH',
  },
  async redirects() {
    return [
      // {
      //   source: '/auth/sign-up/:path*',
      //   destination: '/',
      //   permanent: true,
      // },
      {
        source: '/home/:path*',
        destination: '/',
        permanent: true,
      },
      {
        source: '/ranking/:path*',
        destination: '/',
        permanent: true,
      },
      {
        source: '/recipes/create/:path*',
        destination: '/',
        permanent: true,
      },
      {
        source: '/recipes/search/:path*',
        destination: '/',
        permanent: true,
      },
      {
        source: '/lab/:path*',
        destination: '/',
        permanent: true,
      },
      {
        source: '/my/:path*',
        destination: '/',
        permanent: true,
      },
      {
        source: '/my/setting/:path*',
        destination: '/',
        permanent: true,
      }
    ];
  },
});